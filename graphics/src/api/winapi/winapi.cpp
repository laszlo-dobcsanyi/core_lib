#include "graphics/api/winapi/winapi.h"

////////////////////////////////////////////////////////////////////////////////
// i winapi
////////////////////////////////////////////////////////////////////////////////

namespace graphics { namespace api { namespace winapi {

bool Initialize() {
  LOG_WINAPI(STAGE, "Initialize..\n");
  if (!keyboard::Initialize()) {
    LOG_WINAPI(ERROR, "Failed to initialize keyboard!\n");
    return false;
  }
  LOG_WINAPI(VERBOSE, "Initialized!\n");
  return true;
}

bool IsInitialized() { return keyboard::IsInitialized(); }

void Finalize() {
  if (IsInitialized()) {
    LOG_WINAPI(STAGE, "Finalize..\n");
    keyboard::Finalize();
    LOG_WINAPI(VERBOSE, "Finalized!\n");
  }
}

}}}
