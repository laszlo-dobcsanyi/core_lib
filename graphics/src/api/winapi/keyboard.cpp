#include "graphics/api/winapi/winapi.h"

////////////////////////////////////////////////////////////////////////////////
// i keyboard
////////////////////////////////////////////////////////////////////////////////

namespace graphics { namespace api { namespace winapi { namespace keyboard {

Key scancode_map[512];

bool is_valid_scancode(int _scancode) { return _scancode < 512; }

Key key_from_scancode(int _scancode) {
  if (!is_valid_scancode(_scancode)) {
    return Key::Unknown;
  }
  return scancode_map[_scancode];
}

bool Initialize() {
  LOG_WINAPI(STAGE, "Initializing..\n");
  for (int scancode = 0; scancode < 512; ++scancode) {
    scancode_map[scancode] = Key::Unknown;
  }
  scancode_map[0x00B] = Key::k0;
  scancode_map[0x002] = Key::k1;
  scancode_map[0x003] = Key::k2;
  scancode_map[0x004] = Key::k3;
  scancode_map[0x005] = Key::k4;
  scancode_map[0x006] = Key::k5;
  scancode_map[0x007] = Key::k6;
  scancode_map[0x008] = Key::k7;
  scancode_map[0x009] = Key::k8;
  scancode_map[0x00A] = Key::k9;

  scancode_map[0x01E] = Key::kA;
  scancode_map[0x030] = Key::kB;
  scancode_map[0x02E] = Key::kC;
  scancode_map[0x020] = Key::kD;
  scancode_map[0x012] = Key::kE;
  scancode_map[0x021] = Key::kF;
  scancode_map[0x022] = Key::kG;
  scancode_map[0x023] = Key::kH;
  scancode_map[0x017] = Key::kI;
  scancode_map[0x024] = Key::kJ;
  scancode_map[0x025] = Key::kK;
  scancode_map[0x026] = Key::kL;
  scancode_map[0x032] = Key::kM;
  scancode_map[0x031] = Key::kN;
  scancode_map[0x018] = Key::kO;
  scancode_map[0x019] = Key::kP;
  scancode_map[0x010] = Key::kQ;
  scancode_map[0x013] = Key::kR;
  scancode_map[0x01F] = Key::kS;
  scancode_map[0x014] = Key::kT;
  scancode_map[0x016] = Key::kU;
  scancode_map[0x02F] = Key::kV;
  scancode_map[0x011] = Key::kW;
  scancode_map[0x02D] = Key::kX;
  scancode_map[0x015] = Key::kY;
  scancode_map[0x02C] = Key::kZ;

  scancode_map[0x039] = Key::kSpace;
  scancode_map[0x028] = Key::kApostrophe;
  scancode_map[0x033] = Key::kComma;
  scancode_map[0x00C] = Key::kMinus;
  scancode_map[0x034] = Key::kPeriod;
  scancode_map[0x035] = Key::kSlash;
  scancode_map[0x02B] = Key::kBackslash;
  scancode_map[0x027] = Key::kSemicolon;
  scancode_map[0x00D] = Key::kEqual;
  scancode_map[0x01A] = Key::kLBracket;
  scancode_map[0x01B] = Key::kRBracket;
  scancode_map[0x029] = Key::kGraveAccent;

  scancode_map[0x001] = Key::kEscape;
  scancode_map[0x01C] = Key::kEnter;
  scancode_map[0x00F] = Key::kTab;
  scancode_map[0x00E] = Key::kBackspace;
  scancode_map[0x152] = Key::kInsert;
  scancode_map[0x153] = Key::kDelete;
  scancode_map[0x14D] = Key::kRight;
  scancode_map[0x14B] = Key::kLeft;
  scancode_map[0x150] = Key::kDown;
  scancode_map[0x148] = Key::kUp;
  scancode_map[0x149] = Key::kPageUp;
  scancode_map[0x151] = Key::kPageDown;
  scancode_map[0x147] = Key::kHome;
  scancode_map[0x14F] = Key::kEnd;
  scancode_map[0x03A] = Key::kCapsLock;
  scancode_map[0x046] = Key::kScrollLock;
  scancode_map[0x145] = Key::kNumLock;
  scancode_map[0x137] = Key::kPrintScreen;
  scancode_map[0x045] = Key::kPause;
  scancode_map[0x146] = Key::kPause;
  scancode_map[0x02A] = Key::kLShift;
  scancode_map[0x036] = Key::kRShift;
  scancode_map[0x01D] = Key::kLControl;
  scancode_map[0x11D] = Key::kRControl;
  scancode_map[0x038] = Key::kLAlt;
  scancode_map[0x138] = Key::kRAlt;
  scancode_map[0x15B] = Key::kLSuper;
  scancode_map[0x15C] = Key::kRSuper;
  scancode_map[0x15D] = Key::kMenu;

  scancode_map[0x03B] = Key::kF1;
  scancode_map[0x03C] = Key::kF2;
  scancode_map[0x03D] = Key::kF3;
  scancode_map[0x03E] = Key::kF4;
  scancode_map[0x03F] = Key::kF5;
  scancode_map[0x040] = Key::kF6;
  scancode_map[0x041] = Key::kF7;
  scancode_map[0x042] = Key::kF8;
  scancode_map[0x043] = Key::kF9;
  scancode_map[0x044] = Key::kF10;
  scancode_map[0x057] = Key::kF11;
  scancode_map[0x058] = Key::kF12;

  scancode_map[0x052] = Key::kKP_0;
  scancode_map[0x04F] = Key::kKP_1;
  scancode_map[0x050] = Key::kKP_2;
  scancode_map[0x051] = Key::kKP_3;
  scancode_map[0x04B] = Key::kKP_4;
  scancode_map[0x04C] = Key::kKP_5;
  scancode_map[0x04D] = Key::kKP_6;
  scancode_map[0x047] = Key::kKP_7;
  scancode_map[0x048] = Key::kKP_8;
  scancode_map[0x049] = Key::kKP_9;
  scancode_map[0x053] = Key::kKP_Decimal;
  scancode_map[0x135] = Key::kKP_Divide;
  scancode_map[0x037] = Key::kKP_Multiply;
  scancode_map[0x04A] = Key::kKP_Subtract;
  scancode_map[0x04E] = Key::kKP_Add;
  scancode_map[0x11C] = Key::kKP_Enter;
  // TODO: Key::kKP_Equal on Windows!
  // scancode_map[ 0 ] = Key::kKP_Equal;
  LOG_WINAPI(VERBOSE, "Initialized!\n");
  return true;
}

bool IsInitialized() { return true; }

void Finalize() {
  LOG_WINAPI(STAGE, "Finalizing..\n");
  LOG_WINAPI(VERBOSE, "Finalized!\n");
}

}}}}
