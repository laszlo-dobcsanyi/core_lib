#include "graphics/base.hpp"
#include "graphics/api.h"

#define LOG_GRAPHICS_CONTEXT(__severity__, ...)                                \
  LOG_GRAPHICS(__severity__, __VA_ARGS__)

////////////////////////////////////////////////////////////////////////////////
// graphics api
////////////////////////////////////////////////////////////////////////////////

namespace graphics { namespace api {

/*******************************************************************************
** Context
*******************************************************************************/

Mutex Context::state_mutex;
uint32 Context::state_counter = 0u;

Context::~Context() { Finalize(); }

bool Context::Initialize() {
  if (!has_state) {
    UniqueLock<Mutex> state_lock(state_mutex);
    state_counter++;
    has_state = true;

    if (state_counter == 1) {
      LOG_GRAPHICS_CONTEXT(MESSAGE, "Initialize..\n");
      if (!api::Initialize()) {
        LOG_GRAPHICS_CONTEXT(ERROR, "Failed to initialize adapter!\n");
        return false;
      }
      LOG_GRAPHICS_CONTEXT(VERBOSE, "Initialized!\n");
      return true;
    }
  }
  return IsInitialized();
}

bool Context::IsInitialized() { return api::IsInitialized(); }

Context::operator bool() { return IsInitialized(); }

void Context::Finalize() {
  if (has_state) {
    UniqueLock<Mutex> state_lock(state_mutex);
    has_state = false;
    state_counter--;

    if (state_counter == 0) {
      LOG_GRAPHICS_CONTEXT(MESSAGE, "Finalize..\n");
      api::Finalize();
    }
  }
}

}}

#undef LOG_GRAPHICS_CONTEXT