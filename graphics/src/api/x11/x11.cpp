#include "graphics/api/x11/x11.h"

////////////////////////////////////////////////////////////////////////////////
// graphics
////////////////////////////////////////////////////////////////////////////////

namespace graphics {

#define X11_FUNCTION(NAME) core::LibraryFunction<decltype(&::NAME)> NAME
#include "graphics/api/x11/symbols.inl"

}

////////////////////////////////////////////////////////////////////////////////
// i x11
////////////////////////////////////////////////////////////////////////////////

namespace graphics { namespace api { namespace x11 {

core::Library x11_library;

#if defined(CORE_LIB_CONFIGURATION_DEBUG)
/*******************************************************************************
** xerrorhandler
*******************************************************************************/

int xerrorhandler(Display * _display, XErrorEvent * error) {
  char errorstring[128];
  XGetErrorText(_display, error->error_code, errorstring, 128);

  CORE_LIB_LOG(FATAL, "X11 error: %s\n", errorstring);
  return 0;
}

/*******************************************************************************
** xioerrorhandler
*******************************************************************************/

int xioerrorhandler(Display * _display) {
  CORE_LIB_LOG(FATAL, "X11 io error!\n");
  return 0;
}
#endif

/*******************************************************************************
** Initialize
*******************************************************************************/

bool Initialize() {
  LOG_X11(STAGE, "Initializing..\n");
  CORE_LIB_ASSERT(!IsInitialized());

  static const const_string library_file("libX11.so");
  LOG_X11(MESSAGE, "Loading library file '%s'..\n", cstr(library_file));
  if (!x11_library.Load(library_file)) {
    return false;
  }

#define X11_FUNCTION(NAME)                                                     \
  LOG_X11(VERBOSE, "Loading function %s..\n", CORE_LIB_TO_STRING(NAME));       \
  if (!(NAME).Load(x11_library, const_string(CORE_LIB_TO_STRING(NAME))))       \
  return false
#include "graphics/api/x11/symbols.inl"

#if defined(CORE_LIB_CONFIGURATION_DEBUG)
  LOG_X11(MESSAGE, "Setting up error handler..\n");
  XSetErrorHandler(xerrorhandler);
  XSetIOErrorHandler(xioerrorhandler);
#endif

  LOG_X11(VERBOSE, "Initialized!\n");
  return true;
}

/*******************************************************************************
** IsInitialized
*******************************************************************************/

bool IsInitialized() {
  if (!x11_library.IsLoaded())
    return false;

#define X11_FUNCTION(NAME)                                                     \
  if (!(NAME).IsLoaded())                                                      \
  return false
#include "graphics/api/x11/symbols.inl"

  return true;
}

/*******************************************************************************
** Finalize
*******************************************************************************/

void Finalize() {
  if (IsInitialized()) {
    LOG_X11(STAGE, "Finalizing..\n");

#define X11_FUNCTION(NAME) (NAME).Unload()
#include "graphics/api/x11/symbols.inl"

    x11_library.Unload();

    LOG_X11(VERBOSE, "Finalized!\n");
  }
}

}}}

#undef LOG_X11
