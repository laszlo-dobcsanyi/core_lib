#include "graphics/api/x11/x11.h"

////////////////////////////////////////////////////////////////////////////////
// i x11
////////////////////////////////////////////////////////////////////////////////

namespace graphics { namespace api { namespace x11 {

/*******************************************************************************
** is_valid_scancode
*******************************************************************************/

bool is_valid_scancode(unsigned int _scancode) {
  return 7 < _scancode && _scancode < 256;
}

/*******************************************************************************
** Context
*******************************************************************************/

Key Context::key_from_scancode(unsigned int _scancode) {
  if (!is_valid_scancode(_scancode)) {
    return Key::Unknown;
  }
  return scancode_map[_scancode];
}

Key Context::scancode_to_key(unsigned int _scancode) {
  if (!is_valid_scancode(_scancode)) {
    return Key::Unknown;
  }
  int keysyms_count;
  KeySym * keysyms = XGetKeyboardMapping(display, _scancode, 1, &keysyms_count);
  auto keysym = keysyms[0u];
  XFree(keysyms);
  switch (keysym) {
    case XK_0:
      return Key::k0;
    case XK_1:
      return Key::k1;
    case XK_2:
      return Key::k2;
    case XK_3:
      return Key::k3;
    case XK_4:
      return Key::k4;
    case XK_5:
      return Key::k5;
    case XK_6:
      return Key::k6;
    case XK_7:
      return Key::k7;
    case XK_8:
      return Key::k8;
    case XK_9:
      return Key::k9;

    case XK_a:
      return Key::kA;
    case XK_b:
      return Key::kB;
    case XK_c:
      return Key::kC;
    case XK_d:
      return Key::kD;
    case XK_e:
      return Key::kE;
    case XK_f:
      return Key::kF;
    case XK_g:
      return Key::kG;
    case XK_h:
      return Key::kH;
    case XK_i:
      return Key::kI;
    case XK_j:
      return Key::kJ;
    case XK_k:
      return Key::kK;
    case XK_l:
      return Key::kL;
    case XK_m:
      return Key::kM;
    case XK_n:
      return Key::kN;
    case XK_o:
      return Key::kO;
    case XK_p:
      return Key::kP;
    case XK_q:
      return Key::kQ;
    case XK_r:
      return Key::kR;
    case XK_s:
      return Key::kS;
    case XK_t:
      return Key::kT;
    case XK_u:
      return Key::kU;
    case XK_v:
      return Key::kV;
    case XK_w:
      return Key::kW;
    case XK_x:
      return Key::kX;
    case XK_y:
      return Key::kY;
    case XK_z:
      return Key::kZ;

    case XK_space:
      return Key::kSpace;
    case XK_apostrophe:
      return Key::kApostrophe;
    case XK_comma:
      return Key::kComma;
    case XK_minus:
      return Key::kMinus;
    case XK_period:
      return Key::kPeriod;
    case XK_slash:
      return Key::kSlash;
    case XK_backslash:
      return Key::kBackslash;
    case XK_semicolon:
      return Key::kSemicolon;
    case XK_equal:
      return Key::kEqual;
    case XK_bracketleft:
      return Key::kLBracket;
    case XK_bracketright:
      return Key::kRBracket;
    case XK_grave:
      return Key::kGraveAccent;

    case XK_Escape:
      return Key::kEscape;
    case XK_Return:
      return Key::kEnter;
    case XK_Tab:
      return Key::kTab;
    case XK_BackSpace:
      return Key::kBackspace;
    case XK_Insert:
      return Key::kInsert;
    case XK_Delete:
      return Key::kDelete;
    case XK_Right:
      return Key::kRight;
    case XK_Left:
      return Key::kLeft;
    case XK_Down:
      return Key::kDown;
    case XK_Up:
      return Key::kUp;
    case XK_Page_Up:
      return Key::kPageUp;
    case XK_Page_Down:
      return Key::kPageDown;
    case XK_Home:
      return Key::kHome;
    case XK_End:
      return Key::kEnd;
    case XK_Caps_Lock:
      return Key::kCapsLock;
    case XK_Scroll_Lock:
      return Key::kScrollLock;
    case XK_Num_Lock:
      return Key::kNumLock;
    case XK_Print:
      return Key::kPrintScreen;
    case XK_Pause:
      return Key::kPause;
    case XK_Shift_L:
      return Key::kLShift;
    case XK_Shift_R:
      return Key::kRShift;
    case XK_Control_L:
      return Key::kLControl;
    case XK_Control_R:
      return Key::kRControl;
    case XK_Meta_L:
    case XK_Alt_L:
      return Key::kLAlt;
    case XK_Meta_R:
    case XK_Alt_R:
      return Key::kRAlt;
    case XK_Super_L:
      return Key::kLSuper;
    case XK_Super_R:
      return Key::kRSuper;
    case XK_Menu:
      return Key::kMenu;

    case XK_F1:
      return Key::kF1;
    case XK_F2:
      return Key::kF2;
    case XK_F3:
      return Key::kF3;
    case XK_F4:
      return Key::kF4;
    case XK_F5:
      return Key::kF5;
    case XK_F6:
      return Key::kF6;
    case XK_F7:
      return Key::kF7;
    case XK_F8:
      return Key::kF8;
    case XK_F9:
      return Key::kF9;
    case XK_F10:
      return Key::kF10;
    case XK_F11:
      return Key::kF11;
    case XK_F12:
      return Key::kF12;

    case XK_KP_Insert:
      return Key::kKP_0;
    case XK_KP_End:
      return Key::kKP_1;
    case XK_KP_Down:
      return Key::kKP_2;
    case XK_KP_Page_Down:
      return Key::kKP_3;
    case XK_KP_Left:
      return Key::kKP_4;
    case XK_KP_Right:
      return Key::kKP_6;
    case XK_KP_Home:
      return Key::kKP_7;
    case XK_KP_Up:
      return Key::kKP_8;
    case XK_KP_Page_Up:
      return Key::kKP_9;
    case XK_KP_Delete:
      return Key::kKP_Decimal;
    case XK_KP_Divide:
      return Key::kKP_Divide;
    case XK_KP_Multiply:
      return Key::kKP_Multiply;
    case XK_KP_Subtract:
      return Key::kKP_Subtract;
    case XK_KP_Add:
      return Key::kKP_Add;
    case XK_KP_Enter:
      return Key::kKP_Enter;
    case XK_KP_Equal:
      return Key::kKP_Equal;
  }
  return Key::Unknown;
}

/*******************************************************************************
** create_context
*******************************************************************************/

core::Optional<Context> create_context() {
  Context context;
  LOG_X11(MESSAGE, "Opening display..\n");
  context.display = XOpenDisplay(nullptr);
  if (!context.display) {
    LOG_X11(ERROR, "No display!\n");
    return core::nullopt;
  }

  LOG_X11(MESSAGE, "Getting default root window..\n");
  context.root_window = DefaultRootWindow(context.display);
  if (!context.root_window) {
    LOG_X11(ERROR, "No default root window!\n");
    return core::nullopt;
  }

  LOG_X11(MESSAGE, "Getting default screen..\n");
  context.screen = DefaultScreenOfDisplay(context.display);
  if (!context.screen) {
    LOG_X11(ERROR, "No default screen found!\n");
    return core::nullopt;
  }

  context.screen_id = DefaultScreen(context.display);
  context.wm_protocols_atom =
      XInternAtom(context.display, "WM_PROTOCOLS", False);
  context.wm_delete_window_atom =
      XInternAtom(context.display, "WM_DELETE_WINDOW", False);

  for (auto scancode = 0u; scancode < 256u; ++scancode) {
    context.scancode_map[scancode] =
        context.scancode_to_key(static_cast<unsigned char>(scancode));
  }
  return context;
}

/*******************************************************************************
** destroy
*******************************************************************************/

void destroy(Context & _context) {
  if (_context.display) {
    LOG_X11(MESSAGE, "Closing display..\n");
    XCloseDisplay(_context.display);
    _context.display = nullptr;
  }
}

}}}
