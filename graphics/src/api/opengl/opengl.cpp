#include "graphics/api/opengl/opengl.h"

////////////////////////////////////////////////////////////////////////////////
// i opengl
////////////////////////////////////////////////////////////////////////////////

namespace graphics { namespace api { namespace opengl {

bool Initialize() {
  LOG_GRAPHICS_OPENGL(STAGE, "Initialize..\n");
#if defined(CORE_GRAPHICS_GLX)
  if (!glx::Initialize()) {
    LOG_GRAPHICS_OPENGL(ERROR, "Failed to initialize glx!\n");
    return false;
  }
#elif defined(CORE_GRAPHICS_WGL)
  if (!wgl::Initialize()) {
    LOG_GRAPHICS_OPENGL(ERROR, "Failed to initialize wgl!\n");
    return false;
  }
#endif
  LOG_GRAPHICS_OPENGL(VERBOSE, "Initialized!\n");
  return true;
}

bool IsInitialized() {
#if defined(CORE_GRAPHICS_GLX)
  if (!glx::IsInitialized()) {
    return false;
  }
#elif defined(CORE_GRAPHICS_WGL)
  if (!wgl::IsInitialized()) {
    return false;
  }
#endif
  return true;
}

void Finalize() {
  if (IsInitialized()) {
    LOG_GRAPHICS_OPENGL(STAGE, "Finalizing..\n");
#if defined(CORE_GRAPHICS_GLX)
    glx::Finalize();
#elif defined(CORE_GRAPHICS_WGL)
    wgl::Finalize();
#endif
    LOG_GRAPHICS_OPENGL(VERBOSE, "Finalized!\n");
  }
}

}}}
