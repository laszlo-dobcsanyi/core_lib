#include "graphics/api/opengl/opengl.h"
#include "graphics/api/x11/x11.h"

#define LOG_GRAPHICS_GLX(__severity__, ...)                                    \
  LOG_GRAPHICS_OPENGL(__severity__, __VA_ARGS__)

////////////////////////////////////////////////////////////////////////////////
// graphics
////////////////////////////////////////////////////////////////////////////////

namespace graphics {

core::Library opengl_library;

#define GLX_EXPORTED_FUNCTION(NAME)                                            \
  core::LibraryFunction<decltype(&::NAME)> NAME
#define GLX_ARB_LOADED_FUNCTION(NAME, PTR)                                     \
  core::LibraryFunction<PFN##PTR##PROC> NAME
#include "graphics/api/opengl/glx/symbols.inl"

}

////////////////////////////////////////////////////////////////////////////////
// x window
////////////////////////////////////////////////////////////////////////////////

namespace graphics { namespace api { namespace opengl { namespace glx {
namespace window {

/*******************************************************************************
** create
*******************************************************************************/

core::Optional<Context>
create_context(WindowParameters const & _window_parameters,
               RenderContextParameters const & _context_parameters) {
  using namespace graphics::api::x11;

  LOG_GRAPHICS_GLX(STAGE, "Creating glx window..\n");

  x11::Context x11_context;
  if (auto context = api::x11::create_context()) {
    x11_context = *context;
  } else {
    LOG_GRAPHICS_GLX(ERROR, "Failed to create x11 context!\n");
    return core::nullopt;
  }

  LOG_GRAPHICS_GLX(VERBOSE, "Getting frame buffer configs..\n");
  GLint frame_buffer_attributes[] = {GLX_X_RENDERABLE,
                                     True,
                                     GLX_DRAWABLE_TYPE,
                                     GLX_WINDOW_BIT,
                                     GLX_RENDER_TYPE,
                                     GLX_RGBA_BIT,
                                     GLX_X_VISUAL_TYPE,
                                     GLX_TRUE_COLOR,
                                     GLX_RED_SIZE,
                                     8,
                                     GLX_GREEN_SIZE,
                                     8,
                                     GLX_BLUE_SIZE,
                                     8,
                                     GLX_ALPHA_SIZE,
                                     8,
                                     GLX_DEPTH_SIZE,
                                     24,
                                     GLX_STENCIL_SIZE,
                                     8,
                                     GLX_DOUBLEBUFFER,
                                     True,
                                     0};
  int frame_buffer_count;
  GLXFBConfig * frame_buffer_configs =
      glXChooseFBConfig(x11_context.display, x11_context.screen_id,
                        frame_buffer_attributes, &frame_buffer_count);
  if (!frame_buffer_configs) {
    LOG_GRAPHICS_GLX(ERROR, "Failed to get frame buffer configs!\n");
    return core::nullopt;
  }

  LOG_GRAPHICS_GLX(VERBOSE, "Choosing best frame buffer config..\n");
  int best_config = -1;
  int best_samples = -1;
  for (int current_config = 0; current_config < frame_buffer_count;
       ++current_config) {
    XVisualInfo * visual_info = glXGetVisualFromFBConfig(
        x11_context.display, frame_buffer_configs[current_config]);
    if (visual_info) {
      int sample_buffers;
      int samples;
      glXGetFBConfigAttrib(x11_context.display,
                           frame_buffer_configs[current_config],
                           GLX_SAMPLE_BUFFERS, &sample_buffers);
      glXGetFBConfigAttrib(x11_context.display,
                           frame_buffer_configs[current_config], GLX_SAMPLES,
                           &samples);
      if (best_config < 0 || (sample_buffers && best_samples < samples)) {
        best_config = current_config;
        best_samples = samples;
      }
      LOG_GRAPHICS_GLX(VERBOSE,
                       "%s Config %d matches: Visual Id: 0x%2x, "
                       "sample_buffers: %d, samples: %d\n",
                       (best_config == current_config ? "*" : " "),
                       current_config, visual_info->visualid, sample_buffers,
                       samples);
    }
    XFree(visual_info);
  }
  if (best_config == -1) {
    LOG_GRAPHICS_GLX(ERROR, "No matching config!\n");
    return core::nullopt;
  }

  LOG_GRAPHICS_GLX(VERBOSE, "Getting visual info..\n");
  GLXFBConfig frame_buffer_config = frame_buffer_configs[best_config];
  XFree(frame_buffer_configs);
  XVisualInfo * visual_info =
      glXGetVisualFromFBConfig(x11_context.display, frame_buffer_config);
  if (!visual_info) {
    LOG_GRAPHICS_GLX(ERROR, "No visual info for %d. frame buffer config!\n",
                     best_config);
    return core::nullopt;
  }
  Colormap colormap =
      XCreateColormap(x11_context.display, x11_context.root_window,
                      visual_info->visual, AllocNone);
  XSetWindowAttributes window_attributes;
  window_attributes.colormap = colormap;
  window_attributes.event_mask =
      StructureNotifyMask | KeyPressMask | KeyReleaseMask | PointerMotionMask |
      ButtonPressMask | ButtonReleaseMask | ExposureMask | FocusChangeMask |
      VisibilityChangeMask | EnterWindowMask | LeaveWindowMask |
      PropertyChangeMask;
  // window_attributes.event_mask = ExposureMask /* | KeyPressMask */ | StructureNotifyMask;
  LOG_GRAPHICS_GLX(MESSAGE, "Creating window..\n");
  ::Window window = XCreateWindow(
      x11_context.display, x11_context.root_window, 0, 0,
      _window_parameters.desired_size.x, _window_parameters.desired_size.y, 0,
      visual_info->depth, InputOutput, visual_info->visual,
      CWColormap | CWEventMask, &window_attributes);
  XFree(visual_info);
  if (!window) {
    LOG_GRAPHICS_GLX(ERROR, "Failed to create window!\n");
    return core::nullopt;
  }
  XMapWindow(x11_context.display, window);
  XStoreName(x11_context.display, window, cstr(_window_parameters.title));
  XSetWMProtocols(x11_context.display, window, &x11_context.wm_protocols_atom,
                  1);
  XSetWMProtocols(x11_context.display, window,
                  &x11_context.wm_delete_window_atom, 1);

  ::GLXContext glx_context = nullptr;
  {
    // TODO: OpenGL version should be shared
    const int requested_major_version = 3;
    const int requested_minor_version = 3;
    GLint context_attribs[] = {GLX_CONTEXT_MAJOR_VERSION_ARB,
                               requested_major_version,
                               GLX_CONTEXT_MINOR_VERSION_ARB,
                               requested_minor_version,
                               GLX_CONTEXT_FLAGS_ARB,
                               GLX_CONTEXT_FORWARD_COMPATIBLE_BIT_ARB,
                               0};
    LOG_GRAPHICS_GLX(MESSAGE, "Creating glx context of version %d.%d..\n",
                     requested_major_version, requested_minor_version);
    glx_context = glXCreateContextAttribsARB(
        x11_context.display, frame_buffer_config, 0, true, context_attribs);

    if (!glx_context) {
      LOG_GRAPHICS_GLX(ERROR, "Failed to create glx %d.%d context!\n",
                       requested_major_version, requested_minor_version);
      XDestroyWindow(x11_context.display, window);
      return core::nullopt;
    }

    glXMakeCurrent(x11_context.display, window, glx_context);

    // Check OpenGL context
    {
      GLint actual_major_version = 0;
      glGetIntegerv(GL_MAJOR_VERSION, &actual_major_version);
      GLint actual_minor_version = 0;
      glGetIntegerv(GL_MINOR_VERSION, &actual_minor_version);
      LOG_GRAPHICS_GLX(MESSAGE, "Got glx context with version %d.%d!\n",
                       actual_major_version, actual_minor_version);
    }
  }

  Context result;
  result.window = window;
  result.glx_context = glx_context;
  result.x11_context = x11_context;
  return result;
}

/*******************************************************************************
** destroy
*******************************************************************************/

void destroy(Context & _context) {
  if (_context.window) {
    LOG_GRAPHICS_GLX(MESSAGE, "Destroying window..\n");
    XDestroyWindow(_context.x11_context.display, _context.window);
    _context.window = 0;
  }

  if (_context.glx_context) {
    LOG_GRAPHICS_GLX(MESSAGE, "Destroying render context..\n");
    glXMakeCurrent(_context.x11_context.display, 0, NULL);
    glXDestroyContext(_context.x11_context.display, _context.glx_context);
    _context.glx_context = nullptr;
  }

  x11::destroy(_context.x11_context);
}

}}}}}

////////////////////////////////////////////////////////////////////////////////
// l glx
////////////////////////////////////////////////////////////////////////////////

namespace graphics { namespace api { namespace opengl { namespace glx {

core::LibraryFunction<decltype(&::glXGetProcAddress)> glXGetProcAddress;
core::LibraryFunction<decltype(&::glXGetProcAddressARB)> glXGetProcAddressARB;
core::LibraryFunction<decltype(&::glGetError)> glGetError;

using opengl_process_stub = void (*)();

/*******************************************************************************
** get_process
*******************************************************************************/

opengl_process_stub get_process(char const * _symbol) {
  return glXGetProcAddress(reinterpret_cast<unsigned char const *>(_symbol));
}

/*******************************************************************************
** get_process_arb
*******************************************************************************/

opengl_process_stub get_process_arb(char const * _symbol) {
  return glXGetProcAddressARB(reinterpret_cast<unsigned char const *>(_symbol));
}

/*******************************************************************************
** CheckErrors
*******************************************************************************/

void CheckErrors() {
  CORE_LIB_ASSERT_TEXT(IsInitialized(), "glx api not initialized!\n");
  CORE_LIB_ASSERT_TEXT(glXGetCurrentContext(), "No glx context!\n");
  for (GLenum error = glGetError(); error != GL_NO_ERROR;
       error = glGetError()) {
    CORE_LIB_ASSERT_TEXT(error == GL_NO_ERROR, "OpenGL error: (%#04x)\n",
                         error);
  }
}

/*******************************************************************************
** Initialize
*******************************************************************************/

bool Initialize() {
  LOG_GRAPHICS_GLX(STAGE, "Initializing..\n");
  // Load library
  {
    static const const_string library_file("libGL.so");
    CORE_LIB_ASSERT(!opengl_library.IsLoaded());
    LOG_GRAPHICS_OPENGL(MESSAGE, "Loading library file '%s'..\n",
                        cstr(library_file));
    if (!opengl_library.Load(library_file)) {
      LOG_GRAPHICS_OPENGL(ERROR, "Failed to load library file!\n");
      return false;
    }
  }

  if (!glXGetProcAddress.Load(opengl_library,
                              const_string("glXGetProcAddress"))) {
    LOG_GRAPHICS_OPENGL(ERROR, "Failed to load 'glXGetProcAddress'!\n");
    return false;
  }

  if (!glXGetProcAddressARB.Load(opengl_library,
                                 const_string("glXGetProcAddressARB"))) {
    LOG_GRAPHICS_OPENGL(ERROR, "Failed to load 'glXGetProcAddressARB'!\n");
    return false;
  }

  if (!glGetError.Load(opengl_library, const_string("glGetError"))) {
    LOG_GRAPHICS_OPENGL(ERROR, "Failed to load 'glGetError'!\n");
    return false;
  }

#define GLX_EXPORTED_FUNCTION(NAME)                                            \
  LOG_GRAPHICS_OPENGL(VERBOSE, "Loading exported function '%s'..\n",           \
                      CORE_LIB_TO_STRING(NAME));                               \
  if (!(NAME).Load(opengl_library, const_string(CORE_LIB_TO_STRING(NAME))))    \
  return false
#define GLX_ARB_LOADED_FUNCTION(NAME, PTR)                                     \
  LOG_GRAPHICS_OPENGL(VERBOSE, "Loading symbol '%s'..\n",                      \
                      CORE_LIB_TO_STRING(NAME));                               \
  (NAME) = (PFN##PTR##PROC)get_process_arb(CORE_LIB_TO_STRING(NAME));          \
  if (!(NAME)) {                                                               \
    LOG_GRAPHICS_OPENGL(ERROR, "Failed to load symbol '%s'!\n",                \
                        CORE_LIB_TO_STRING(NAME));                             \
    return false;                                                              \
  }
#include "graphics/api/opengl/glx/symbols.inl"

#define LOAD_SYMBOLS
#include "graphics/api/opengl/symbols.inl"
#undef LOAD_SYMBOLS

  LOG_GRAPHICS_GLX(VERBOSE, "Initialized!\n");
  return true;
}

/*******************************************************************************
** IsInitialized
*******************************************************************************/

bool IsInitialized() {
  return opengl_library.IsLoaded() && glXGetProcAddress.IsLoaded() &&
         glXGetProcAddressARB.IsLoaded() && glGetError.IsLoaded();
}

/*******************************************************************************
** Finalize
*******************************************************************************/

void Finalize() {
  LOG_GRAPHICS_GLX(STAGE, "Finalizing..\n");

#define UNLOAD_SYMBOLS
#include "graphics/api/opengl/symbols.inl"
#undef UNLOAD_SYMBOLS

#define GLX_EXPORTED_FUNCTION(NAME) (NAME).Unload();
#define GLX_ARB_LOADED_FUNCTION(NAME, PTR) (NAME).Unload();
#include "graphics/api/opengl/glx/symbols.inl"

  glGetError.Unload();
  glXGetProcAddressARB.Unload();
  glXGetProcAddress.Unload();

  opengl_library.Unload();

  LOG_GRAPHICS_GLX(VERBOSE, "Finalized!\n");
}

}}}}
