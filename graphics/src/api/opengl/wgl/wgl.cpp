#include "graphics/api/opengl/opengl.h"
#include "graphics/api/winapi/winapi.h"

#define LOG_GRAPHICS_WGL(__severity__, ...)                                    \
  LOG_GRAPHICS_OPENGL(__severity__, __VA_ARGS__)

////////////////////////////////////////////////////////////////////////////////
//                                                                                                //
////////////////////////////////////////////////////////////////////////////////

namespace {

HWND helper_window;
HGLRC helper_window_render_context;

constexpr LPCWSTR helper_window_class_name() {
  return L"core_graphics_helper_window";
}
constexpr LPCWSTR window_delegate_property_name() { return L"window_delegate"; }
constexpr LPCWSTR window_class_name() { return L"core_graphics_window"; }

LRESULT CALLBACK helper_window_procedure(HWND _hwnd, UINT _umsg, WPARAM _wparam,
                                         LPARAM _lparam) {
  return ::DefWindowProcW(_hwnd, _umsg, _wparam, _lparam);
}

LRESULT CALLBACK window_procedure(HWND _hwnd, UINT _umsg, WPARAM _wparam,
                                  LPARAM _lparam) {
  using WindowDelegate = graphics::api::opengl::wgl::window::WindowDelegate;

  if (auto * window_delegate = reinterpret_cast<WindowDelegate *>(
          ::GetPropW(_hwnd, window_delegate_property_name()))) {
    return (*window_delegate)(_hwnd, _umsg, _wparam, _lparam);
  }
  return ::DefWindowProcW(_hwnd, _umsg, _wparam, _lparam);
}

core::UniqueArray<WCHAR> wide_string_from_utf8(char const * _cstr) {
  int length = ::MultiByteToWideChar(CP_UTF8, 0, _cstr, -1, NULL, 0);
  if (!length) {
    return nullptr;
  }
  core::UniqueArray<WCHAR> result = core::make_unique_array<WCHAR>(length);
  ::memset(result.Get(), 0, length * sizeof(WCHAR));
  if (!::MultiByteToWideChar(CP_UTF8, 0, _cstr, -1, result.Get(), length)) {
    return nullptr;
  }
  return result;
}

DWORD get_window_style(bool _monitor, bool _decorated, bool _resizable) {
  DWORD style = WS_CLIPSIBLINGS | WS_CLIPCHILDREN;
  if (_monitor) {
    style |= WS_POPUP;
  } else {
    style |= WS_SYSMENU | WS_MINIMIZEBOX;

    if (_decorated) {
      style |= WS_CAPTION;

      if (_resizable) {
        style |= WS_MAXIMIZEBOX | WS_THICKFRAME;
      }
    } else {
      style |= WS_POPUP;
    }
  }
  return style;
}

DWORD get_window_extended_style(bool _monitor, bool _floating) {
  DWORD style = WS_EX_APPWINDOW;
  if (!_floating) {
    style |= WS_EX_TOPMOST;
  }
  return style;
}

graphics::WindowSize get_window_size(DWORD _style, DWORD _extended_style,
                                     graphics::WindowSize _client_size) {
  RECT rect = {0, 0, _client_size.x, _client_size.z};
  ::AdjustWindowRectEx(&rect, _style, FALSE, _extended_style);
  return graphics::WindowSize(rect.right - rect.left, rect.bottom - rect.top);
}

#if 0
inline BOOL is_windows_version_or_greater( WORD _major, WORD _minor, WORD _sp ) {
  OSVERSIONINFOEXW osvi = { sizeof( osvi ), _major, _minor, 0, 0,{ 0 }, _sp };
  DWORD mask = VER_MAJORVERSION | VER_MINORVERSION | VER_SERVICEPACKMAJOR;
  ULONGLONG cond = ::VerSetConditionMask( 0, VER_MAJORVERSION, VER_GREATER_EQUAL );
  cond = ::VerSetConditionMask( cond, VER_MINORVERSION, VER_GREATER_EQUAL );
  cond = ::VerSetConditionMask( cond, VER_SERVICEPACKMAJOR, VER_GREATER_EQUAL );
  return ::VerifyVersionInfoW( &osvi, mask, cond );
}

inline BOOL is_windows7_or_greater() {
  return is_windows_version_or_greater( HIBYTE( _WIN32_WINNT_WIN7 )
                                      , LOBYTE( _WIN32_WINNT_WIN7 ), 0 );
}
#endif

// TODO: Just return a PIXELFORMATDESCRIPTOR?
inline int choose_pixel_format(HDC _device_context) {
  // TODO: Use 'wglGetPixelFormatAttribivARB' if possible!
  int count = ::DescribePixelFormat(_device_context, 1,
                                    sizeof(PIXELFORMATDESCRIPTOR), NULL);
  int result = 0;
  for (auto current = 0; current < count; ++current) {
    const int format = current + 1;
    PIXELFORMATDESCRIPTOR descriptor;

    if (!::DescribePixelFormat(_device_context, format,
                               sizeof(PIXELFORMATDESCRIPTOR), &descriptor)) {
      continue;
    }

    if (!(descriptor.dwFlags & PFD_DRAW_TO_WINDOW) ||
        !(descriptor.dwFlags & PFD_SUPPORT_OPENGL)) {
      continue;
    }

    if (!(descriptor.dwFlags & PFD_GENERIC_ACCELERATED) &&
        (descriptor.dwFlags & PFD_GENERIC_FORMAT)) {
      continue;
    }

    if (descriptor.iPixelType != PFD_TYPE_RGBA) {
      continue;
    }

    if (descriptor.cDepthBits != U8(24)) {
      continue;
    }

    result = format;
  }

  // TODO: Find most suitable descriptor!
  if (!result) {
    LOG_GRAPHICS_WGL(ERROR, "The driver does not appear to support OpenGL!\n");
  }
  return result;
}

}

////////////////////////////////////////////////////////////////////////////////
// graphics
////////////////////////////////////////////////////////////////////////////////

namespace graphics {

core::Library opengl_library;

#define WGL_EXPORTED_FUNCTION(NAME)                                            \
  core::LibraryFunction<decltype(&::NAME)> NAME
#define WGL_ARB_LOADED_FUNCTION(NAME, PTR)                                     \
  core::LibraryFunction<PFN##PTR##PROC> NAME
#include "graphics/api/opengl/wgl/symbols.inl"

}

////////////////////////////////////////////////////////////////////////////////
// l window
////////////////////////////////////////////////////////////////////////////////

namespace graphics { namespace api { namespace opengl { namespace wgl {
namespace window {

/*******************************************************************************
** create
*******************************************************************************/

HWND create(WindowParameters const & _window_parameters,
            RenderContextParameters const & _context_parameters,
            WindowDelegate & _delegate) {
  CORE_LIB_ASSERT(IsInitialized());

  HWND window_handle = nullptr;
  // Window
  {
    DWORD style = get_window_style(_window_parameters.monitor,
                                   _window_parameters.decorated,
                                   _window_parameters.resizable);
    DWORD extended_style = get_window_extended_style(
        _window_parameters.monitor, _window_parameters.floating);

    WindowSize size;
    WindowPosition position;
    if (_window_parameters.monitor) {
      // TODO: Port this!
      //GLFWvidmode mode;

      //// NOTE: This window placement is temporary and approximate, as the
      ////       correct position and size cannot be known until the monitor
      ////       video mode has been picked in _glfwSetVideoModeWin32
      //_glfwPlatformGetMonitorPos( window->monitor, &xpos, &ypos );
      //_glfwPlatformGetVideoMode( window->monitor, &mode );
      //fullWidth = mode.width;
      //fullHeight = mode.height;
      CORE_LIB_UNIMPLEMENTED;
    } else {
      position = WindowPosition(CW_USEDEFAULT, CW_USEDEFAULT);
      if (_window_parameters.maximized) {
        style |= WS_MAXIMIZE;
      }
      size = get_window_size(style, extended_style,
                             _window_parameters.desired_size);
    }

    core::UniqueArray<WCHAR> title =
        wide_string_from_utf8(cstr(_window_parameters.title));
    if (!title) {
      LOG_GRAPHICS_WGL(ERROR, "Conversion failed from UTF-8 to wide string!\n");
      return nullptr;
    }

    window_handle = ::CreateWindowExW(
        extended_style, window_class_name(), title.Get(), style, position.x,
        position.z, size.x, size.z, NULL // No parent window
        ,
        NULL // No window menu
        ,
        ::GetModuleHandleW(NULL), NULL);
    if (!window_handle) {
      LOG_GRAPHICS_WGL(ERROR, "CreateWindowExW failed!\n");
      return nullptr;
    }

    ::ShowWindow(window_handle, SW_SHOWNA);
    ::BringWindowToTop(window_handle);
    ::SetForegroundWindow(window_handle);
    ::SetFocus(window_handle);
    ::SetPropW(window_handle, window_delegate_property_name(), &_delegate);
  }

  HDC device_context = ::GetDC(window_handle);
  if (!device_context) {
    LOG_GRAPHICS_WGL(ERROR, "GetDC failed!\n");
    // TODO: Destroy window!
    return nullptr;
  }

  // Pixel Format
  {
    int format = choose_pixel_format(device_context);
    if (!format) {
      LOG_GRAPHICS_WGL(ERROR, "Failed to choose pixel format!\n");
      // TODO: Destroy window!
      return nullptr;
    }

    PIXELFORMATDESCRIPTOR descriptor;
    if (!::DescribePixelFormat(device_context, format, sizeof(descriptor),
                               &descriptor)) {
      LOG_GRAPHICS_WGL(ERROR, "DescribePixelFormat failed!\n");
      // TODO: Destroy window!
      return nullptr;
    }

    if (!::SetPixelFormat(device_context, format, &descriptor)) {
      LOG_GRAPHICS_WGL(ERROR, "SetPixelFormat failed!\n");
      // TODO: Destroy window!
      return nullptr;
    }
  }

  // RenderContext
  {
    // TODO: OpenGL version should be shared
    const int requested_major_version = 3;
    const int requested_minor_version = 3;
    int attributes[] = {
        WGL_CONTEXT_MAJOR_VERSION_ARB,
        requested_major_version,
        WGL_CONTEXT_MINOR_VERSION_ARB,
        requested_minor_version,
        WGL_CONTEXT_PROFILE_MASK_ARB,
        WGL_CONTEXT_CORE_PROFILE_BIT_ARB,
        0,
    };
    LOG_GRAPHICS_WGL(MESSAGE, "Creating wgl context of version %d.%d..\n",
                     requested_major_version, requested_minor_version);
    HGLRC render_context =
        wglCreateContextAttribsARB(device_context, 0, attributes);
    if (!render_context) {
      LOG_GRAPHICS_WGL(ERROR, "Failed to create wgl %d.%d context!\n",
                       requested_major_version, requested_minor_version);
      return nullptr;
    }

    wglMakeCurrent(device_context, render_context);

    // Check OpenGL context
    {
      GLint actual_major_version = 0;
      glGetIntegerv(GL_MAJOR_VERSION, &actual_major_version);
      GLint actual_minor_version = 0;
      glGetIntegerv(GL_MINOR_VERSION, &actual_minor_version);
      LOG_GRAPHICS_WGL(MESSAGE, "Got wgl context with version %d.%d!\n",
                       actual_major_version, actual_minor_version);
    }
  }
  return window_handle;
}

/*******************************************************************************
** destroy
*******************************************************************************/

void destroy(HWND _window_handle) {
  CORE_LIB_ASSERT(IsInitialized());

  if (HGLRC render_context = wglGetCurrentContext()) {
    LOG_GRAPHICS_WGL(MESSAGE, "Destroying render context..\n");
    wglDeleteContext(render_context);
  }

  LOG_GRAPHICS_WGL(MESSAGE, "Releasing device context..\n");
  if (HDC device_context = ::GetDC(_window_handle)) {
    if (!::ReleaseDC(_window_handle, device_context)) {
      LOG_GRAPHICS_WGL(ERROR, "::ReleaseDC failed!\n");
    }
  }

  LOG_GRAPHICS_WGL(MESSAGE, "Destroying window..\n");
  if (!::DestroyWindow(_window_handle)) {
    LOG_GRAPHICS_WGL(ERROR, "::DestroyWindow failed!\n");
  }
}

}}}}}

////////////////////////////////////////////////////////////////////////////////
// l wgl
////////////////////////////////////////////////////////////////////////////////

namespace graphics { namespace api { namespace opengl { namespace wgl {

core::LibraryFunction<decltype(&::wglGetProcAddress)> wglGetProcAddress;
core::LibraryFunction<decltype(&::glGetError)> glGetError;

using opengl_process_stub = void (*)();

/*******************************************************************************
** get_process
*******************************************************************************/

opengl_process_stub get_process(char const * _symbol) {
  auto address = reinterpret_cast<intptr_t>(wglGetProcAddress(_symbol));
  if ((address == 0) || (address == 1) || (address == 2) || (address == 3) ||
      (address == -1)) {
    LOG_GRAPHICS_WGL(WARNING, "wglGetProcAddress for '%s' returned %ld!\n",
                     _symbol, address);
    address = reinterpret_cast<intptr_t>(
        ::GetProcAddress(opengl_library.NativeHandle(), _symbol));
  }
  auto fptr = reinterpret_cast<opengl_process_stub>(address);
  if (fptr == nullptr) {
    LOG_GRAPHICS_WGL(WARNING, "Error while loading symbol '%s': %ld!\n",
                     _symbol, GetLastError());
  }
  return fptr;
}

/*******************************************************************************
** get_process_arb
*******************************************************************************/

opengl_process_stub get_process_arb(char const * _symbol) {
  return get_process(_symbol);
}

/*******************************************************************************
** CheckErrors
*******************************************************************************/

void CheckErrors() {
  CORE_LIB_ASSERT_TEXT(IsInitialized(), "wgl api not initialized!\n");
  CORE_LIB_ASSERT_TEXT(wglGetCurrentContext(), "No wgl context!\n");
  for (GLenum error = glGetError(); error != GL_NO_ERROR;
       error = glGetError()) {
    CORE_LIB_ASSERT_TEXT(error == GL_NO_ERROR, "OpenGL error: (%#04x)\n",
                         error);
  }
}

/*******************************************************************************
** Initialize
*******************************************************************************/

bool Initialize() {
  LOG_GRAPHICS_WGL(STAGE, "Initializing..\n");

  // Load library
  {
    static const const_string library_file("opengl32.dll");
    CORE_LIB_ASSERT(!opengl_library.IsLoaded());
    LOG_GRAPHICS_WGL(MESSAGE, "Loading library file '%s'..\n",
                     cstr(library_file));
    if (!opengl_library.Load(library_file)) {
      LOG_GRAPHICS_WGL(ERROR, "Failed to load library file!\n");
      return false;
    }
  }

  if (!wglGetProcAddress.Load(opengl_library,
                              const_string("wglGetProcAddress"))) {
    LOG_GRAPHICS_WGL(ERROR, "Failed to load 'wglGetProcAddress'!\n");
    return false;
  }

  if (!glGetError.Load(opengl_library, const_string("glGetError"))) {
    LOG_GRAPHICS_WGL(ERROR, "Failed to load 'glGetError'!\n");
    return false;
  }

#define WGL_EXPORTED_FUNCTION(NAME)                                            \
  LOG_GRAPHICS_OPENGL(VERBOSE, "Loading exported function '%s'..\n",           \
                      CORE_LIB_TO_STRING(NAME));                               \
  if (!(NAME).Load(opengl_library, const_string(CORE_LIB_TO_STRING(NAME))))    \
  return false
#include "graphics/api/opengl/wgl/symbols.inl"

  WNDCLASSEXW window_class;
  ZeroMemory(&window_class, sizeof(window_class));
  window_class.cbSize = sizeof(window_class);
  window_class.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
  window_class.lpfnWndProc = (WNDPROC)window_procedure;
  window_class.hInstance = ::GetModuleHandleW(NULL);
  window_class.hIcon = ::LoadIcon(NULL, IDI_WINLOGO);
  window_class.hCursor = ::LoadCursor(NULL, IDC_ARROW);
  window_class.lpszClassName = window_class_name();

  if (!::RegisterClassExW(&window_class)) {
    LOG_GRAPHICS_WGL(ERROR, "Failed to register window class!\n");
    return false;
  }

  // Helper window
  {
    WNDCLASSEXW window_class;
    ZeroMemory(&window_class, sizeof(window_class));
    window_class.cbSize = sizeof(window_class);
    window_class.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
    window_class.lpfnWndProc = (WNDPROC)helper_window_procedure;
    window_class.hInstance = ::GetModuleHandleW(NULL);
    window_class.hIcon = ::LoadIcon(NULL, IDI_WINLOGO);
    window_class.hCursor = ::LoadCursor(NULL, IDC_ARROW);
    window_class.lpszClassName = helper_window_class_name();

    if (!::RegisterClassExW(&window_class)) {
      LOG_GRAPHICS_WGL(ERROR, "Failed to register helper window class!\n");
      return false;
    }

    helper_window = ::CreateWindowExW(
        WS_EX_OVERLAPPEDWINDOW, helper_window_class_name(),
        L"core_graphics message window", WS_CLIPSIBLINGS | WS_CLIPCHILDREN, 0,
        0, 1, 1, NULL, NULL, ::GetModuleHandleW(NULL), NULL);
    if (!helper_window) {
      LOG_GRAPHICS_WGL(ERROR, "Failed to create helper window: '%ld'!\n",
                       ::GetLastError());
      return nullptr;
    }

    ::ShowWindow(helper_window, SW_HIDE);

    PIXELFORMATDESCRIPTOR pixel_format_descriptor;
    ZeroMemory(&pixel_format_descriptor, sizeof(pixel_format_descriptor));
    pixel_format_descriptor.nSize = sizeof(pixel_format_descriptor);
    pixel_format_descriptor.nVersion = 1;
    pixel_format_descriptor.dwFlags =
        PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
    pixel_format_descriptor.iPixelType = PFD_TYPE_RGBA;
    pixel_format_descriptor.cColorBits = 24;

    // TODO: Use something RAII for ReleaseDC!
    HDC device_context = ::GetDC(helper_window);
    int pixel_format =
        ::ChoosePixelFormat(device_context, &pixel_format_descriptor);
    if (!::SetPixelFormat(device_context, pixel_format,
                          &pixel_format_descriptor)) {
      LOG_GRAPHICS_WGL(ERROR, "Failed to set pixel format!\n");
      ::ReleaseDC(helper_window, device_context);
      return false;
    }

    helper_window_render_context = wglCreateContext(device_context);
    if (!helper_window_render_context) {
      LOG_GRAPHICS_WGL(ERROR,
                       "Failed to create helper window render context!\n");
      ::ReleaseDC(helper_window, device_context);
      return false;
    }

    HDC previous_device_context = wglGetCurrentDC();
    HGLRC previous_render_context = wglGetCurrentContext();
    if (!wglMakeCurrent(device_context, helper_window_render_context)) {
      LOG_GRAPHICS_WGL(ERROR,
                       "Failed to make default render context current!\n");
      wglMakeCurrent(previous_device_context, previous_render_context);
      wglDeleteContext(helper_window_render_context);
      ::ReleaseDC(helper_window, device_context);
      return false;
    }

    ::ReleaseDC(helper_window, device_context);
  }

#define LOAD_SYMBOLS
#include "graphics/api/opengl/symbols.inl"
#undef LOAD_SYMBOLS

#define WGL_ARB_LOADED_FUNCTION(NAME, PTR)                                     \
  LOG_GRAPHICS_OPENGL(VERBOSE, "Loading symbol '%s'..\n",                      \
                      CORE_LIB_TO_STRING(NAME));                               \
  (NAME) = (PFN##PTR##PROC)get_process_arb(CORE_LIB_TO_STRING(NAME));          \
  if (!(NAME)) {                                                               \
    LOG_GRAPHICS_OPENGL(ERROR, "Failed to load symbol '%s'!\n",                \
                        CORE_LIB_TO_STRING(NAME));                             \
    return false;                                                              \
  }
#include "graphics/api/opengl/wgl/symbols.inl"

  LOG_GRAPHICS_WGL(VERBOSE, "Initialized!\n");
  return true;
}

/*******************************************************************************
** IsInitialized
*******************************************************************************/

bool IsInitialized() {
  // TODO: Add logic to check symbols!
  return opengl_library.IsLoaded() && wglGetProcAddress.IsLoaded() &&
         glGetError.IsLoaded();
}

/*******************************************************************************
** Finalize
*******************************************************************************/

void Finalize() {
  LOG_GRAPHICS_WGL(STAGE, "Finalizing..\n");

#define WGL_ARB_LOADED_FUNCTION(NAME, PTR) (NAME).Unload();
#include "graphics/api/opengl/wgl/symbols.inl"

#define UNLOAD_SYMBOLS
#include "graphics/api/opengl/symbols.inl"
#undef UNLOAD_SYMBOLS

  wglDeleteContext(helper_window_render_context);

  if (helper_window) {
    ::DestroyWindow(helper_window);
  }

  if (!::UnregisterClassW(helper_window_class_name(),
                          ::GetModuleHandleW(NULL))) {
    LOG_GRAPHICS_WGL(ERROR, "Failed to unregister helper window class!\n");
  }

  if (!::UnregisterClassW(window_class_name(), ::GetModuleHandleW(NULL))) {
    LOG_GRAPHICS_WGL(ERROR, "Failed to unregister window class!\n");
  }

#define WGL_EXPORTED_FUNCTION(NAME) (NAME).Unload();
#include "graphics/api/opengl/wgl/symbols.inl"

  glGetError.Unload();
  wglGetProcAddress.Unload();

  opengl_library.Unload();

  LOG_GRAPHICS_WGL(VERBOSE, "Finalized!\n");
}

}}}}
