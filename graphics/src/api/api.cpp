#include "graphics/api/api.h"

////////////////////////////////////////////////////////////////////////////////
// graphics api
////////////////////////////////////////////////////////////////////////////////

namespace graphics { namespace api {

/*******************************************************************************
** Initialize
*******************************************************************************/

bool Initialize() {
#if defined(CORE_GRAPHICS_X11)
  if (!x11::Initialize()) {
    return false;
  }
#elif defined(CORE_GRAPHICS_WINAPI)
  if (!winapi::Initialize()) {
    return false;
  }
#endif

#if defined(CORE_GRAPHICS_OPENGL)
  if (!opengl::Initialize()) {
    return false;
  }
#elif defined(CORE_GRAPHICS_DIRECTX)
  if (!directx::Initialize()) {
    return false;
  }
#endif

  return true;
}

/*******************************************************************************
** IsInitialized
*******************************************************************************/

bool IsInitialized() {
#if defined(CORE_GRAPHICS_X11)
  if (!x11::IsInitialized()) {
    return false;
  }
#elif defined(CORE_GRAPHICS_WINAPI)
  if (!winapi::IsInitialized()) {
    return false;
  }
#endif

#if defined(CORE_GRAPHICS_OPENGL)
  if (!opengl::IsInitialized()) {
    return false;
  }
#elif defined(CORE_GRAPHICS_DIRECTX)
  if (!directx::IsInitialized()) {
    return false;
  }
#endif

  return true;
}

/*******************************************************************************
** Finalize
*******************************************************************************/

void Finalize() {
#if defined(CORE_GRAPHICS_OPENGL)
  opengl::Finalize();
#elif defined(CORE_GRAPHICS_DIRECTX)
  directx::Finalize();
#endif

#if defined(CORE_GRAPHICS_X11)
  x11::Finalize();
#elif defined(CORE_GRAPHICS_WINAPI)
  winapi::Finalize();
#endif
}

}}
