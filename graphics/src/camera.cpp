#include "graphics/camera.h"

////////////////////////////////////////////////////////////////////////////////
// graphics
////////////////////////////////////////////////////////////////////////////////

namespace graphics {

/*******************************************************************************
** Camera
*******************************************************************************/

Camera::Camera()
    : position(0.f, 0.f, 0.f)
    , projection(core::nullopt)
    , view(core::nullopt)
    , view_projection(core::nullopt) {}

void Camera::SetOrtho() { SetOrtho(-1.f, 1.f, -1.f, 1.f, 0.1f, 10000.f); }

void Camera::SetOrtho(float _left, float _right, float _bottom, float _top,
                      float _near, float _far) {
  if (!projection) {
    projection = core::Optional<TransformationMatrix>(in_place);
  }
  set_ortho(projection.Value(), _left, _right, _bottom, _top, _near, _far);
  UpdateViewProjection();
}

void Camera::SetPerspective() {
  SetPerspective(45.f, 16.f / 9.f, 0.1f, 10000.f);
}

void Camera::SetPerspective(float _fov, float _aspect, float _near,
                            float _far) {
  if (!projection) {
    projection = core::Optional<TransformationMatrix>(in_place);
  }
  set_perspective(projection.Value(), _fov, _aspect, _near, _far);
  UpdateViewProjection();
}

void Camera::Set(numerics::Vector3f const & _position,
                 numerics::Vector3f const & _target) {
  Set(_position, _target, numerics::Vector3f(0.f, 1.f, 0.f));
}

void Camera::Set(numerics::Vector3f const & _position,
                 numerics::Vector3f const & _target,
                 numerics::Vector3f const & _up) {
  position = _position;
  if (!view) {
    view = core::Optional<TransformationMatrix>(in_place);
  }
  set_look_at(view.Value(), position, _target, _up);
  UpdateViewProjection();
}

void Camera::UpdateViewProjection() {
  if (projection && view) {
    if (!view_projection) {
      view_projection = core::Optional<TransformationMatrix>(in_place);
    }
    view_projection.Value() = projection.Value() * view.Value();
  }
}

}
