#include "graphics/implementation/gl/glx/window.h"

#define LOG_GRAPHICS_GLX_WINDOW(__severity__, ...)                             \
  LOG_GRAPHICS(__severity__, __VA_ARGS__)

////////////////////////////////////////////////////////////////////////////////
// graphics glx
////////////////////////////////////////////////////////////////////////////////

namespace graphics { namespace glx {

/*******************************************************************************
** Window
*******************************************************************************/

Window::~Window() {
  Destroy();

  if (context.window != 0u) {
    api::opengl::glx::window::destroy(context);
  }
}

bool Window::Create(const_string _title) {
  WindowParameters parameters;
  parameters.desired_size = graphics::WindowSize(1280, 720);
  parameters.title = move(_title);
  return Create(parameters);
}

bool Window::Create(WindowParameters const & _parameters) {
  if (auto window_context = api::opengl::glx::window::create_context(
          _parameters, RenderContextParameters())) {
    context = *window_context;
  } else {
    return false;
  }

  LOG_GRAPHICS_GLX_WINDOW(MESSAGE, "Created!\n");

  WindowSize window_size;
  {
    XWindowAttributes attributes;
    Status result = XGetWindowAttributes(context.x11_context.display,
                                         context.window, &attributes);
    if (!result) {
      LOG_GRAPHICS_GLX_WINDOW(ERROR, "Failed to get window attributes!\n");
      return false;
    }

    window_size = WindowSize(attributes.width, attributes.height);
    // TODO: This should be a shared code somewhere!
    glViewport(0, 0, window_size[0], window_size[1]);
  }

  WindowBase::OnCreatedEvent(window_size);

  return true;
}

void Window::Run() {
  WindowBase::StartFrame();

#if 0
  // TODO: Handle focus event via WindowBase!
  ::Window focused_window;
  int revert_to;
  XGetInputFocus( context.x11_context.display, &focused_window, &revert_to );
  if ( focused_window == window ) {
    LOG_GRAPHICS_GLX_WINDOW( LOG, "In focus!\n" );
  }
#endif

  UpdateMousePosition();

  CORE_LIB_ASSERT(!IsDestroyed());
  int event_count = XPending(context.x11_context.display);
  XEvent xevent;
  while (event_count--) {
    XNextEvent(context.x11_context.display, &xevent);
    switch (xevent.type) {
      case Expose: {
        LOG_GRAPHICS_GLX_WINDOW(VERBOSE, "Exposed event!\n");
      } break;
      case ConfigureNotify: {
        LOG_GRAPHICS_GLX_WINDOW(VERBOSE, "Configure event!\n");
        Resized(WindowSize(xevent.xconfigure.width, xevent.xconfigure.height));
      } break;
      case DestroyNotify: {
        LOG_GRAPHICS_GLX_WINDOW(VERBOSE, "Destroyed event!\n");
        Destroy();
        return;
      } break;
      case ClientMessage: {
        LOG_GRAPHICS_GLX_WINDOW(VERBOSE, "Client message event!\n");
        if (xevent.xclient.message_type ==
            context.x11_context.wm_protocols_atom) {
          Atom const protocol = xevent.xclient.data.l[0];
          if (protocol == 0) {
            break;
          } else if (protocol == context.x11_context.wm_delete_window_atom) {
            Destroy();
            return;
          }
        }
      } break;

      case KeyPress: {
        LOG_GRAPHICS_GLX_WINDOW(VERBOSE, "Key press event!\n");
        Key key = context.x11_context.key_from_scancode(xevent.xkey.keycode);
        if (key != Key::Unknown) {
          WindowBase::OnKeyPressedEvent(key);
        }
      } break;
      case KeyRelease: {
        LOG_GRAPHICS_GLX_WINDOW(VERBOSE, "Key release event!\n");
        Key key = context.x11_context.key_from_scancode(xevent.xkey.keycode);
        if (key != Key::Unknown) {
          WindowBase::OnKeyReleasedEvent(key);
        }
      } break;

      case ButtonPress: {
        switch (xevent.xbutton.button) {
          case Button1: {
            WindowBase::OnMousePressedEvent(Button::Left);
          } break;
          case Button2: {
            WindowBase::OnMousePressedEvent(Button::Middle);
          } break;
          case Button3: {
            WindowBase::OnMousePressedEvent(Button::Right);
          } break;
          case Button4: {
            WindowBase::OnMouseWheelUpEvent();
          } break;
          case Button5: {
            WindowBase::OnMouseWheelDownEvent();
          } break;
        }
      } break;
      case ButtonRelease: {
        switch (xevent.xbutton.button) {
          case Button1: {
            WindowBase::OnMouseReleasedEvent(Button::Left);
          } break;
          case Button2: {
            WindowBase::OnMouseReleasedEvent(Button::Middle);
          } break;
          case Button3: {
            WindowBase::OnMouseReleasedEvent(Button::Right);
          } break;
          case Button4:
          case Button5: {
          } break;
        }
      } break;

      default: {
        // LOG_GRAPHICS_GLX_WINDOW( MESSAGE, "Uncaught event: %i\n", xevent.type );
      } break;
    }
  }
  XFlush(context.x11_context.display);
}

void Window::Destroy() {
  if (IsDestroyed()) {
    return;
  }
  destroyed = true;
  WindowBase::OnDestroyEvent();
}

bool Window::IsDestroyed() const { return destroyed; }

void Window::Clear() {
  glClearColor(0.2f, 0.3f, 0.3f, 1.f);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void Window::Present() {
  glXSwapBuffers(context.x11_context.display, context.window);
}

void Window::UpdateMousePosition() {
  {
    ::Window focused_window;
    int revert_to;
    XGetInputFocus(context.x11_context.display, &focused_window, &revert_to);
    if (focused_window != context.window) {
      return;
    }
  }
  ::Window window_result;
  WindowPosition root_position;
  WindowPosition window_position;
  unsigned int mask;
  const bool on_screen =
      XQueryPointer(context.x11_context.display, context.window, &window_result,
                    &window_result, &root_position.x, &root_position.y,
                    &window_position.x, &window_position.y, &mask);
  if (on_screen) {
    WindowPosition current_position(window_position[0u], window_position[1u]);
    core::Optional<WindowPosition> previous_position;
    if (WindowBase::IsGrabbingMouse()) {
      previous_position = WindowBase::Size() / 2;
    } else {
      previous_position = WindowBase::MousePosition();
    }

    WindowBase::SetMousePosition(current_position);

    if (WindowBase::IsGrabbingMouse()) {
      WindowPosition delta = current_position - *previous_position;
      OnMouseMovedEvent(delta.x, delta.y);
    }

    if (WindowBase::IsGrabbingMouse()) {
      XWarpPointer(context.x11_context.display, context.window, context.window,
                   0, 0, 0, 0, WindowBase::Width() / 2,
                   WindowBase::Height() / 2);
    }
  } else {
    LOG_GRAPHICS_GLX_WINDOW(VERBOSE, "Mouse not in client area!\n");
    WindowBase::ClearMousePosition();
    OnMouseMovedEvent(0, 0);
  }
}

void Window::Resized(WindowSize _size) {
  LOG_GRAPHICS_GLX_WINDOW(VERBOSE, "\tDimension: %dx%d\n", _size[0], _size[1]);
  glViewport(0, 0, _size[0], _size[1]);
  WindowBase::OnResizedEvent(_size);
}

}}
