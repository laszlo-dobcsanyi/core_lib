#include "graphics/implementation/gl/texture.h"

////////////////////////////////////////////////////////////////////////////////
// graphics opengl
////////////////////////////////////////////////////////////////////////////////

namespace graphics { namespace opengl {

/*******************************************************************************
** Texture
*******************************************************************************/

Texture::~Texture() { Destroy(); }

Texture::operator bool() const { return IsCreated(); }

bool Texture::IsCreated() const { return object.HasValue(); }

void Texture::Destroy() {
  if (IsCreated()) {
    graphics::glDeleteTextures(1, &object);
    object.Reset();
  }
  CORE_LIB_ASSERT(!IsCreated());
}

/*******************************************************************************
** create_texture
*******************************************************************************/

Texture create_texture(uint32 _width, uint32 _height, ColorFormat _format,
                       core::span<byte const> _data,
                       TextureSettings const & _settings) {
  Texture texture;
  graphics::glGenTextures(1, &texture.object);
  graphics::glBindTexture(GL_TEXTURE_2D, texture.object);

  GLenum internal_format;
  GLenum format;
  GLenum type;
  switch (_format) {
    case ColorFormat::RGB: {
      internal_format = GL_RGB;
      format = GL_RGB;
      type = GL_UNSIGNED_BYTE;
    } break;
    case ColorFormat::RGBA: {
      internal_format = GL_RGBA;
      format = GL_RGBA;
      type = GL_UNSIGNED_BYTE;
    } break;
    case ColorFormat::RGB8UI: {
      internal_format = GL_RGB8;
      format = GL_RGB;
      type = GL_UNSIGNED_BYTE;
    } break;
    case ColorFormat::RGBA8UI: {
      internal_format = GL_RGBA8UI;
      format = GL_RGBA;
      type = GL_UNSIGNED_BYTE;
    } break;
    case ColorFormat::BGR: {
      internal_format = GL_BGR;
      format = GL_BGR;
      type = GL_UNSIGNED_BYTE;
    } break;
    case ColorFormat::BGRA: {
      internal_format = GL_BGRA;
      format = GL_BGRA;
      type = GL_UNSIGNED_BYTE;
    } break;
    case ColorFormat::BGR8UI: {
      internal_format = GL_RGB8;
      format = GL_BGR;
      type = GL_UNSIGNED_BYTE;
    } break;
    case ColorFormat::ABGR8UI: {
      internal_format = GL_RGBA8;
      format = GL_BGRA;
      type = GL_UNSIGNED_BYTE;
    } break;
    default: {
      CORE_LIB_ASSERT_TEXT(false, "Unknown color format!\n");
      CORE_LIB_UNREACHABLE;
    } break;
  }

  if (!_settings.generate_mipmap) {
    graphics::glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_BASE_LEVEL, 0);
    graphics::glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, 0);
  }

  graphics::glTexImage2D(GL_TEXTURE_2D, 0, internal_format, _width, _height, 0,
                         format, type, _data.data());

  if (_settings.generate_mipmap) {
    graphics::glGenerateMipmap(GL_TEXTURE_2D);
  }

  graphics::glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S,
                            GlTextureWrapMode(_settings.wrap_u));
  graphics::glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T,
                            GlTextureWrapMode(_settings.wrap_v));
  graphics::glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_R,
                            GlTextureWrapMode(_settings.wrap_w));
  graphics::glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,
                            GlTextureFilterMode(_settings.filter_minify));
  graphics::glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,
                            GlTextureFilterMode(_settings.filter_magnify));
  graphics::glBindTexture(GL_TEXTURE_2D, 0);

  return move(texture);
}

Texture create_texture(resource::ImageData const & _image_data,
                       TextureSettings const & _settings) {
  return create_texture(_image_data.Width(), _image_data.Height(),
                        _image_data.Format(), _image_data.GetBytes(),
                        _settings);
}

Texture create_texture(resource::ImageData const & _image_data) {
  return create_texture(_image_data, TextureSettings());
}

}}
