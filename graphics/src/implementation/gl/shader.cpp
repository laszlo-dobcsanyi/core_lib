#include "graphics/implementation/gl/shader.h"

#ifndef CORE_GRAPHICS_OPENGL
#  error
#endif

#define LOG_SHADER(__severity__, ...) CORE_LIB_LOG(__severity__, __VA_ARGS__)

////////////////////////////////////////////////////////////////////////////////
// l detail
////////////////////////////////////////////////////////////////////////////////

namespace graphics { namespace opengl { namespace detail {

ShaderHandle create_shader(char const * const _shader_data,
                           GLenum _shader_type) {
  ShaderHandle shader = glCreateShader(_shader_type);
  if (!shader) {
    LOG_SHADER(ERROR, "Error while creating shader!\n");
    return ShaderHandle();
  }
  glShaderSource(shader, 1, &_shader_data, NULL);
  glCompileShader(shader);
  GLint result;
  glGetShaderiv(shader, GL_COMPILE_STATUS, &result);
  if (result != GL_TRUE) {
    GLint info_log_length;
    glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &info_log_length);
    static const GLsizei error_message_size = 512u;
    char error_message_buffer[error_message_size];
    glGetShaderInfoLog(shader, error_message_size, NULL, error_message_buffer);
    LOG_SHADER(ERROR, "Error while compiling shader: %i: %s!\n",
               info_log_length, error_message_buffer);
    return ShaderHandle();
  }
  return shader;
}

ShaderProgram create_program(ShaderHandle _vertex_shader,
                             ShaderHandle _fragment_shader) {
  ShaderObjectType program = glCreateProgram();
  if (program) {
    glAttachShader(program, _vertex_shader);
    glAttachShader(program, _fragment_shader);
    glLinkProgram(program);
    glDetachShader(program, _vertex_shader);
    glDetachShader(program, _fragment_shader);
    GLint result;
    glGetProgramiv(program, GL_LINK_STATUS, &result);
    if (result != GL_TRUE) {
      static const GLsizei error_message_size = 512u;
      char error_message[error_message_size];
      glGetProgramInfoLog(program, error_message_size, NULL, error_message);
      CORE_LIB_LOG(ERROR, "Error while linking program: %s!\n", error_message);
      return ShaderProgram();
    }
    return ShaderProgram(program);
  }
  CORE_LIB_LOG(ERROR, "Error while creating program!\n");
  return ShaderProgram();
}

}}}

////////////////////////////////////////////////////////////////////////////////
// graphics opengl
////////////////////////////////////////////////////////////////////////////////

namespace graphics { namespace opengl {

/*******************************************************************************
** ShaderHandle
*******************************************************************************/

ShaderHandle::ShaderHandle(typename ShaderObjectType::ValueType _value)
    : ShaderObjectType(_value) {}

ShaderHandle::~ShaderHandle() { Destroy(); }

void ShaderHandle::Destroy() {
  if (HasValue()) {
    glDeleteShader(value);
    Reset();
  }
}

/*******************************************************************************
** ShaderProgram
*******************************************************************************/

ShaderProgram::ShaderProgram(typename ShaderObjectType::ValueType _value)
    : ShaderObjectType(_value) {}

ShaderProgram::~ShaderProgram() { Destroy(); }

void ShaderProgram::Use() const {
  CORE_LIB_ASSERT(HasValue());
  glUseProgram(value);
}

void ShaderProgram::Destroy() {
  if (HasValue()) {
    glDeleteProgram(value);
    Reset();
  }
}

/*******************************************************************************
** Shader
*******************************************************************************/

Shader::Shader(ShaderProgram && _base)
    : ShaderProgram(move(_base)) {}

bool Shader::Create(const_string const & _vertex_shader,
                    const_string const & _fragment_shader) {
  LOG_SHADER(MESSAGE, "Creating Vertex shader..\n");
  auto vertex_shader =
      detail::create_shader(cstr(_vertex_shader), GL_VERTEX_SHADER);
  if (vertex_shader) {
    LOG_SHADER(MESSAGE, "Creating Fragment shader..\n");
    auto fragment_shader =
        detail::create_shader(cstr(_fragment_shader), GL_FRAGMENT_SHADER);
    if (fragment_shader) {
      LOG_SHADER(MESSAGE, "Creating Program..\n");
      *this =
          detail::create_program(move(vertex_shader), move(fragment_shader));
    }
  }
  return HasValue();
}

UniformLocationType Shader::GetUniformLocation(const char * _name) const {
  auto location = glGetUniformLocation(value, _name);
  CORE_LIB_ASSERT_TEXT(location != detail::InvalidLocation,
                       "Failed to GetUniformLocation for '%s'!", _name);
  return location;
}

}}
