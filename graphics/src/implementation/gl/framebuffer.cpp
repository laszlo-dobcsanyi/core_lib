#include "graphics/implementation/gl/framebuffer.h"
#include "graphics/implementation/gl/texture.h"
#include "graphics/implementation/gl/renderbuffer.h"

////////////////////////////////////////////////////////////////////////////////
// graphics opengl
////////////////////////////////////////////////////////////////////////////////

namespace graphics { namespace opengl {

/*******************************************************************************
** FrameBuffer
*******************************************************************************/

FrameBuffer::~FrameBuffer() { Destroy(); }

bool FrameBuffer::Create() {
  CORE_LIB_ASSERT(!IsCreated());
  graphics::glGenFramebuffers(1, &object);
  // TODO( Silent ): Color attachment parameter
  // GLenum DrawBuffers[ 1 ] = { GL_COLOR_ATTACHMENT0 };
  // graphics::glDrawBuffers( 1u, DrawBuffers );
  return IsCreated();
}

void FrameBuffer::Destroy() {
  if (IsCreated()) {
    graphics::glDeleteFramebuffers(1, &object);
    object.Reset();
  }
  CORE_LIB_ASSERT(!IsCreated());
}

FrameBuffer::operator bool() const { return IsCreated(); }

bool FrameBuffer::IsCreated() const { return object.HasValue(); }

void FrameBuffer::Bind() {
  CORE_LIB_ASSERT(IsCreated());
  graphics::glBindFramebuffer(GL_FRAMEBUFFER, object);
}

void FrameBuffer::Unbind() {
  CORE_LIB_ASSERT(IsCreated());
  graphics::glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void FrameBuffer::Attach(uint32 _attachment_index, Texture const & _texture) {
  graphics::glFramebufferTexture2D(GL_FRAMEBUFFER,
                                   GL_COLOR_ATTACHMENT0 + _attachment_index,
                                   GL_TEXTURE_2D, _texture.object, 0);
  CORE_LIB_ASSERT(graphics::glCheckFramebufferStatus(GL_FRAMEBUFFER) ==
                  GL_FRAMEBUFFER_COMPLETE);
}

void FrameBuffer::Attach(RenderBuffer const & _render_buffer) {
  graphics::glFramebufferRenderbuffer(GL_FRAMEBUFFER,
                                      GL_DEPTH_STENCIL_ATTACHMENT,
                                      GL_RENDERBUFFER, _render_buffer.object);
  CORE_LIB_ASSERT(graphics::glCheckFramebufferStatus(GL_FRAMEBUFFER) ==
                  GL_FRAMEBUFFER_COMPLETE);
}

}}