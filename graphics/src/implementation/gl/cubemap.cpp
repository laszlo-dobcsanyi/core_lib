#include "graphics/implementation/gl/cubemap.h"

////////////////////////////////////////////////////////////////////////////////
// graphics opengl
////////////////////////////////////////////////////////////////////////////////

namespace graphics { namespace opengl {

/*******************************************************************************
** Cubemap
*******************************************************************************/

Cubemap::~Cubemap() { Destroy(); }

Cubemap::operator bool() const { return IsCreated(); }

bool Cubemap::IsCreated() const { return object.HasValue(); }

void Cubemap::Destroy() {
  if (IsCreated()) {
    glDeleteTextures(1, &object);
    object.Reset();
  }
  CORE_LIB_ASSERT(!IsCreated());
}

/*******************************************************************************
** create_cubemap
*******************************************************************************/

core::Optional<Cubemap>
create_cubemap(core::span<resource::ImageData> _image_data) {
  CORE_LIB_ASSERT(_image_data.size() == 6u);

  Cubemap cubemap;
  glGenTextures(1, &cubemap.object);
  glBindTexture(GL_TEXTURE_CUBE_MAP, cubemap.object);

  ColorFormat image_data_format = _image_data[0u].Format();
  uint32 width = _image_data[0u].Width();
  uint32 height = _image_data[0u].Height();
#if defined(CORE_LIB_CONFIGURATION_DEBUG)
  for (resource::ImageData const & image_data : _image_data) {
    CORE_LIB_ASSERT(image_data.Format() == image_data_format);
    CORE_LIB_ASSERT(image_data.Width() == width);
    CORE_LIB_ASSERT(image_data.Height() == height);
  }
#endif
  GLenum internal_format;
  GLenum format;
  GLenum type;
  switch (image_data_format) {
    case ColorFormat::RGB: {
      internal_format = GL_RGB;
      format = GL_RGB;
      type = GL_UNSIGNED_BYTE;
    } break;
    case ColorFormat::RGBA: {
      internal_format = GL_RGBA;
      format = GL_RGBA;
      type = GL_UNSIGNED_BYTE;
    } break;
    case ColorFormat::RGB8UI: {
      internal_format = GL_RGB8;
      format = GL_RGB;
      type = GL_UNSIGNED_BYTE;
    } break;
    case ColorFormat::RGBA8UI: {
      internal_format = GL_RGBA8UI;
      format = GL_RGBA;
      type = GL_UNSIGNED_BYTE;
    } break;
    case ColorFormat::BGR: {
      internal_format = GL_BGR;
      format = GL_BGR;
      type = GL_UNSIGNED_BYTE;
    } break;
    case ColorFormat::BGRA: {
      internal_format = GL_BGRA;
      format = GL_BGRA;
      type = GL_UNSIGNED_BYTE;
    } break;
    case ColorFormat::BGR8UI: {
      internal_format = GL_RGB8;
      format = GL_BGR;
      type = GL_UNSIGNED_BYTE;
    } break;
    case ColorFormat::ABGR8UI: {
      internal_format = GL_RGBA8;
      format = GL_BGRA;
      type = GL_UNSIGNED_BYTE;
    } break;
    default: {
      CORE_LIB_ASSERT_TEXT(false, "Unknown color format!\n");
      CORE_LIB_UNREACHABLE;
    } break;
  }
  for (auto current = 0u; current < _image_data.size(); ++current) {
    glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + current, 0, internal_format,
                 width, height, 0, format, type,
                 _image_data[current].GetBytes().data());
  }
  return core::Optional<Cubemap>(move(cubemap));
}

}}