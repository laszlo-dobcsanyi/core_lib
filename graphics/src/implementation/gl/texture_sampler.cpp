#include "graphics/implementation/gl/texture_sampler.h"
#include "graphics/implementation/gl/texture.h"
#include "graphics/implementation/gl/sampler.h"
#include "graphics/implementation/gl/cubemap.h"

#define LOG_GL_TEXTURE_SAMPLER(__severity__, ...)                              \
  LOG_GRAPHICS(__severity__, __VA_ARGS__)

////////////////////////////////////////////////////////////////////////////////
// graphics opengl
////////////////////////////////////////////////////////////////////////////////

namespace graphics { namespace opengl {

/*******************************************************************************
** create
*******************************************************************************/

bool create(TextureSampler & /* _texture_sampler */) { return true; }

/*******************************************************************************
** create_texture_sampler
*******************************************************************************/

core::Optional<TextureSampler> create_texture_sampler() {
  TextureSampler texture_sampler;
  if (create(texture_sampler)) {
    return core::Optional<TextureSampler>(move(texture_sampler));
  }
  return core::nullopt;
}

/*******************************************************************************
** TextureSampler
*******************************************************************************/

TextureSampler::operator bool() const { return IsBound(); }

bool TextureSampler::IsBound() const {
  return texture_unit.HasValue() && sampler.HasValue();
}

int current_texture_unit() {
  GLint current_texture_unit;
  glGetIntegerv(GL_ACTIVE_TEXTURE, &current_texture_unit);
  current_texture_unit = current_texture_unit - GL_TEXTURE0;
  return static_cast<int>(current_texture_unit);
}

void TextureSampler::Bind(TextureUnit _texture_unit) {
  texture_unit = _texture_unit;
  glActiveTexture(GL_TEXTURE0 + texture_unit);
  CORE_LIB_ASSERT(current_texture_unit() == texture_unit);
}

void TextureSampler::Bind(core::Ref<Sampler> _sampler_ref) {
  CORE_LIB_ASSERT(_sampler_ref->IsCreated());
  sampler = move(_sampler_ref);

  CORE_LIB_ASSERT(texture_unit.HasValue());
  CORE_LIB_ASSERT(current_texture_unit() == texture_unit);
  glBindSampler(texture_unit, (*sampler)->object);
}

void TextureSampler::Activate(Texture const & _texture) const {
  LOG_GRAPHICS(VERBOSE, "Activating %d..\n", static_cast<int>(texture_unit));
  CORE_LIB_ASSERT(texture_unit.HasValue());
  glActiveTexture(GL_TEXTURE0 + texture_unit);
  glBindTexture(GL_TEXTURE_2D, _texture.object);
  if (sampler.HasValue()) {
    CORE_LIB_ASSERT((*sampler)->IsCreated());
    glBindSampler(texture_unit, (*sampler)->object);
  }
}

void TextureSampler::Activate(Cubemap const & _cubemap) const {
  LOG_GRAPHICS(VERBOSE, "Activating %d..\n", static_cast<int>(texture_unit));
  CORE_LIB_ASSERT(texture_unit.HasValue());
  glActiveTexture(GL_TEXTURE0 + texture_unit);
  glBindTexture(GL_TEXTURE_CUBE_MAP, _cubemap.object);
  if (sampler.HasValue()) {
    CORE_LIB_ASSERT((*sampler)->IsCreated());
    glBindSampler(texture_unit, (*sampler)->object);
  }
}

}}