#include "graphics/implementation/gl/renderbuffer.h"

////////////////////////////////////////////////////////////////////////////////
// graphics opengl
////////////////////////////////////////////////////////////////////////////////

namespace graphics { namespace opengl {

/*******************************************************************************
** RenderBuffer
*******************************************************************************/

RenderBuffer::~RenderBuffer() { Destroy(); }

bool RenderBuffer::Create(RenderbufferFormat _format, uint32 _width,
                          uint32 _height) {
  CORE_LIB_ASSERT(!IsCreated());
  graphics::glGenRenderbuffers(1, &object);
  if (IsCreated()) {
    graphics::glBindRenderbuffer(GL_RENDERBUFFER, object);
    GLenum internal_format;
    switch (_format) {
      case RenderbufferFormat::Depth32: {
        internal_format = GL_DEPTH_COMPONENT32F;
      } break;
      case RenderbufferFormat::Depth24Stencil8: {
        internal_format = GL_DEPTH24_STENCIL8;
      } break;
      default: {
        CORE_LIB_ASSERT_TEXT(false, "Unknown renderbuffer format!\n");
        CORE_LIB_UNREACHABLE;
      } break;
    }
    graphics::glRenderbufferStorage(GL_RENDERBUFFER, internal_format, _width,
                                    _height);
    graphics::glBindRenderbuffer(GL_RENDERBUFFER, 0);
  }
  return IsCreated();
}

void RenderBuffer::Destroy() {
  if (IsCreated()) {
    graphics::glDeleteRenderbuffers(1, &object);
    object.Reset();
  }
  CORE_LIB_ASSERT(!IsCreated());
}

RenderBuffer::operator bool() const { return IsCreated(); }

bool RenderBuffer::IsCreated() const { return object.HasValue(); }

void RenderBuffer::Bind() {
  CORE_LIB_ASSERT(IsCreated());
  graphics::glBindRenderbuffer(GL_RENDERBUFFER, object);
}

void RenderBuffer::Unbind() {
  CORE_LIB_ASSERT(IsCreated());
  graphics::glBindRenderbuffer(GL_RENDERBUFFER, 0);
}

}}