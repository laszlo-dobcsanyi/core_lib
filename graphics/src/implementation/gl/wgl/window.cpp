#include "graphics/implementation/gl/wgl/window.h"

#define LOG_GRAPHICS_WINGL_WINDOW(__severity__, ...)                           \
  LOG_GRAPHICS(__severity__, __VA_ARGS__)

////////////////////////////////////////////////////////////////////////////////
// graphics wgl
////////////////////////////////////////////////////////////////////////////////

namespace graphics { namespace wgl {

/*******************************************************************************
** Window
*******************************************************************************/

Window::Window() {
  window_delegate = WindowDelegate::From<Window, &Window::Process>(this);
}

Window::~Window() {
  Destroy();

  if (window) {
    api::opengl::wgl::window::destroy(window);
  }
}

bool Window::Create(const_string _title) {
  WindowParameters parameters;
  parameters.desired_size = graphics::WindowSize(1280, 720);
  parameters.title = move(_title);
  return Create(parameters);
}

bool Window::Create(WindowParameters const & _parameters) {
  window = api::opengl::wgl::window::create(
      _parameters, RenderContextParameters(), window_delegate);
  if (!window) {
    LOG_GRAPHICS_WINGL_WINDOW(ERROR, "Failed to create window!\n");
    return false;
  }

  device_context = ::GetDC(window);
  if (!device_context) {
    LOG_GRAPHICS_WINGL_WINDOW(ERROR, "Failed to get device context!\n");
    return false;
  }

#if 0
  RAWINPUTDEVICE Rid;
  Rid.usUsagePage = ((USHORT)0x01); // generic
  Rid.usUsage = ((USHORT)0x02); // mouse
  Rid.dwFlags = 0; // RIDEV_INPUTSINK;
  Rid.hwndTarget = window;
  ::RegisterRawInputDevices( &Rid, 1, sizeof( Rid ) );
#endif

  LOG_GRAPHICS_WINGL_WINDOW(MESSAGE, "Created!\n");

  WindowSize size;
  {
    RECT client_size;
    if (!::GetClientRect(window, &client_size)) {
      LOG_GRAPHICS_WINGL_WINDOW(ERROR, "Failed to get client rectangle!\n");
      return false;
    }

    size = WindowSize(client_size.right, client_size.bottom);
    // TODO: This should be a shared code somewhere!
    glViewport(0, 0, size[0], size[1]);
  }

  WindowBase::OnCreatedEvent(size);

  return true;
}

void Window::Run() {
  WindowBase::StartFrame();

  UpdateMousePosition();

  MSG message;
  while (::PeekMessageW(&message, NULL, 0, 0, PM_REMOVE)) {
    if (message.message != WM_QUIT) {
      ::TranslateMessage(&message);
      ::DispatchMessageW(&message);
    } else {
      Destroy();
      return;
    }
  }

  // Update mouse wheel
  {
    while (WHEEL_DELTA < mouse_wheel_delta) {
      OnMouseWheelUpEvent();
      mouse_wheel_delta -= WHEEL_DELTA;
    }
    while (mouse_wheel_delta < -WHEEL_DELTA) {
      OnMouseWheelDownEvent();
      mouse_wheel_delta += WHEEL_DELTA;
    }
  }
}

void Window::Destroy() {
  if (IsDestroyed()) {
    return;
  }
  destroyed = true;
  WindowBase::OnDestroyEvent();
}

bool Window::IsDestroyed() const { return destroyed; }

void Window::Clear() {
  glClearColor(0.2f, 0.3f, 0.3f, 1.f);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void Window::Present() { ::SwapBuffers(device_context); }

LRESULT Window::Process(HWND _hwnd, UINT _umsg, WPARAM _wparam,
                        LPARAM _lparam) {
  using namespace graphics::api::winapi;

  switch (_umsg) {
    case WM_SIZE: {
      Resized(WindowSize(LOWORD(_lparam), HIWORD(_lparam)));
    } break;
    case WM_CLOSE: {
      ::PostQuitMessage(0);
    } break;

#if 0
    case WM_INPUT: {
      UINT buffer_size = 40;
      BYTE buffer[ 40 ];
      ::GetRawInputData( (HRAWINPUT)_lparam, RID_INPUT, buffer, &buffer_size, sizeof( RAWINPUTHEADER ) );
      RAWINPUT raw_input = *(RAWINPUT*)buffer;
      if ( raw_input.header.dwType == RIM_TYPEMOUSE ) {
        int32 delta_x = raw_input.data.mouse.lLastX;
        int32 delta_y = raw_input.data.mouse.lLastY;
        OnMouseMovedEvent( delta_x, delta_y );
      }
    } break;
#endif

    case WM_KEYDOWN: {
      const int scancode = (_lparam >> 16) & 0x1ff;
      Key key = keyboard::key_from_scancode(scancode);
      if (key != Key::Unknown) {
        OnKeyPressedEvent(key);
      }
    } break;
    case WM_KEYUP: {
      const int scancode = (_lparam >> 16) & 0x1ff;
      Key key = keyboard::key_from_scancode(scancode);
      if (key != Key::Unknown) {
        OnKeyReleasedEvent(key);
      }
    } break;

    case WM_LBUTTONDOWN: {
      OnMousePressedEvent(Button::Left);
    } break;
    case WM_MBUTTONDOWN: {
      OnMousePressedEvent(Button::Middle);
    } break;
    case WM_RBUTTONDOWN: {
      OnMousePressedEvent(Button::Right);
    } break;

    case WM_LBUTTONUP: {
      OnMouseReleasedEvent(Button::Left);
    } break;
    case WM_MBUTTONUP: {
      OnMouseReleasedEvent(Button::Middle);
    } break;
    case WM_RBUTTONUP: {
      OnMouseReleasedEvent(Button::Right);
    } break;

    case WM_MOUSEWHEEL: {
      auto delta = GET_WHEEL_DELTA_WPARAM(_wparam);
      mouse_wheel_delta += delta;
    } break;

    default: {
      return ::DefWindowProcW(_hwnd, _umsg, _wparam, _lparam);
    }
  }
  return 0;
}

void Window::UpdateMousePosition() {
  if (::GetFocus() != window) {
    return;
  }
  POINT position;
  if (::GetCursorPos(&position)) {
    if (::ScreenToClient(window, &position)) {
      RECT client_size;
      if (::GetClientRect(window, &client_size)) {
        if (0 <= position.x && 0 <= position.y &&
            position.x <= client_size.right &&
            position.y <= client_size.bottom) {
          LOG_GRAPHICS_WINGL_WINDOW(VERBOSE, "Mouse position: (%d, %d)\n",
                                    position.x, position.y);
          WindowPosition current_position(position.x, position.y);
          core::Optional<WindowPosition> previous_position;
          if (WindowBase::IsGrabbingMouse()) {
            previous_position = WindowBase::Size() / 2;
          } else {
            previous_position = WindowBase::MousePosition();
          }

          WindowBase::SetMousePosition(current_position);

          if (previous_position) {
            WindowPosition delta = current_position - *previous_position;
            OnMouseMovedEvent(delta.x, delta.z);
          }
          if (WindowBase::IsGrabbingMouse()) {
            POINT middle;
            middle.x = previous_position->x;
            middle.y = previous_position->z;
            if (::ClientToScreen(window, &middle)) {
              ::SetCursorPos(middle.x, middle.y);
            }
          }
          return;
        }
      }
    }
  }
  LOG_GRAPHICS_WINGL_WINDOW(VERBOSE, "Mouse not in client area!\n");
  WindowBase::ClearMousePosition();
  OnMouseMovedEvent(0, 0);
}

void Window::Resized(WindowSize _size) {
  LOG_GRAPHICS_WINGL_WINDOW(VERBOSE, "\tDimension: %dx%d\n", _size[0],
                            _size[1]);
  glViewport(0, 0, _size[0], _size[1]);
  WindowBase::OnResizedEvent(_size[0], _size[1]);
}

}}
