#include "graphics/implementation/gl/sampler.h"

#define LOG_GL_SAMPLER(__severity__, ...)                                      \
  LOG_GRAPHICS(__severity__, __VA_ARGS__)

////////////////////////////////////////////////////////////////////////////////
// graphics opengl
////////////////////////////////////////////////////////////////////////////////

namespace graphics { namespace opengl {

/*******************************************************************************
** Sampler
*******************************************************************************/

Sampler::~Sampler() { Destroy(); }

Sampler::operator bool() const { return IsCreated(); }

bool Sampler::IsCreated() const { return object.HasValue(); }

void Sampler::Set(TextureWrap _wrap, TextureWrapMode _mode) {
  CORE_LIB_ASSERT(IsCreated());
  GLenum parameter_name = GlTextureWrap(_wrap);
  GLint parameter_value = GlTextureWrapMode(_mode);
  glSamplerParameteri(object, parameter_name, parameter_value);
}

void Sampler::Set(TextureFilter _filter, TextureFilterMode _mode) {
  CORE_LIB_ASSERT(IsCreated());
  GLenum parameter_name = GlTextureFilter(_filter);
  GLint parameter_value = GlTextureFilterMode(_mode);
  glSamplerParameteri(object, parameter_name, parameter_value);
}

void Sampler::Destroy() {
  if (IsCreated()) {
    glDeleteSamplers(1, &object);
    object.Reset();
  }
  CORE_LIB_ASSERT(!IsCreated());
}

/*******************************************************************************
** create
*******************************************************************************/

bool create(Sampler & _sampler) {
  CORE_LIB_ASSERT(!_sampler.IsCreated());
  SamplerObjectType sampler_object;
  glGenSamplers(1, &sampler_object);
  _sampler.object = move(sampler_object);
  return true;
}

/*******************************************************************************
** create_sampler
*******************************************************************************/

core::Optional<Sampler> create_sampler() {
  Sampler sampler;
  if (create(sampler)) {
    return core::Optional<Sampler>(move(sampler));
  }
  return core::nullopt;
}

}}