#include "graphics/resource/load_model.h"
#include "graphics/create_mesh.hpp"
#include "graphics/resource/model_data_from_file.h"
#include "graphics/resource/vertex_data_from_mesh_data.hpp"
#include "graphics/resource/texture_from_file.hpp"

////////////////////////////////////////////////////////////////////////////////
// graphics resource
////////////////////////////////////////////////////////////////////////////////

namespace graphics { namespace resource {

/*******************************************************************************
** load_static_model
*******************************************************************************/

core::Optional<StaticModel>
load_static_model(core::fs::Directory _model_directory) {
  LOG_GRAPHICS(STAGE, "Loading StaticModel from '%s'!\n",
               cstr(_model_directory));

  // Model
  StaticModelData static_model_data;
  {
    auto model_file_path = _model_directory / "model.fbx";
    LOG_GRAPHICS(MESSAGE, "Loading '%s'!\n", cstr(model_file_path));
    auto data = model_data_from_file(model_file_path);
    if (!data) {
      LOG_GRAPHICS(ERROR, "Failed to load model '%s'!\n",
                   cstr(model_file_path));
      return core::nullopt;
    }
    if (!data->Is<StaticModelData>()) {
      LOG_GRAPHICS(ERROR, "Loaded model data is not a static model '%s'!\n",
                   cstr(model_file_path));
      return core::nullopt;
    }
    static_model_data = move(data->As<StaticModelData>());
  }

  // Diffuse Texture
  Texture diffuse_texture;
  {
    auto texture_file_path = _model_directory / "textures" / "diffuse.tga";
    LOG_GRAPHICS(MESSAGE, "Loading '%s'!\n", cstr(texture_file_path));
    diffuse_texture =
        resource::texture_from_file(texture_file_path, TextureSettings());
    if (!diffuse_texture) {
      LOG_GRAPHICS(ERROR, "Failed to load diffuse texture!\n");
      return core::nullopt;
    }
  }

  // StaticModel
  StaticModel static_model;
  static_model.mesh = create_mesh(
      vertex_data_from_mesh_data(move(static_model_data.mesh_data)));
  static_model.diffuse_texture = move(diffuse_texture);
  return static_model;
}

/*******************************************************************************
** load_animated_model
*******************************************************************************/

core::Optional<AnimatedModel>
load_animated_model(core::fs::Directory _model_directory) {
  LOG_GRAPHICS(STAGE, "Loading AnimatedModel from '%s'!\n",
               cstr(_model_directory));

  // Model
  AnimatedModelData animated_model_data;
  {
    auto model_file_path = _model_directory / "model.fbx";
    LOG_GRAPHICS(MESSAGE, "Loading '%s'!\n", cstr(model_file_path));
    auto data = model_data_from_file(model_file_path);
    if (!data) {
      LOG_GRAPHICS(ERROR, "Failed to load model '%s'!\n",
                   cstr(model_file_path));
      return core::nullopt;
    }
    if (!data->Is<AnimatedModelData>()) {
      LOG_GRAPHICS(ERROR, "Loaded model data is not an animated model '%s'!\n",
                   cstr(model_file_path));
      return core::nullopt;
    }
    if (!data->As<AnimatedModelData>().animation_data.IsEmpty()) {
      LOG_GRAPHICS(WARNING, "Loaded data has animations '%s'!\n",
                   cstr(model_file_path));
    }
    animated_model_data = move(data->As<AnimatedModelData>());
  }

  // Animations
  container::Array<core::fs::FilePath> animations;
  {
    if (core::Optional<core::fs::Directory> animations_directory =
            _model_directory.GetSubDirectory("animations")) {
      for (auto entry : *animations_directory) {
        if (!entry.Is<core::fs::FilePath>())
          continue;

        animations.Create(move(entry.As<core::fs::FilePath>()));
      }
    } else {
      LOG_GRAPHICS(WARNING, "Missing animations directory!\n");
    }
  }

  animated_model_data.animation_data =
      core::make_unique_array<AnimationData>(animations.Size());
  for (auto animation = 0u; animation < animations.Size(); ++animation) {
    auto & animation_file_path = animations[animation];
    LOG_GRAPHICS(MESSAGE, "Loading '%s'!\n", cstr(animation_file_path));
    auto data = model_data_from_file(animation_file_path);
    if (!data) {
      LOG_GRAPHICS(ERROR, "Failed to load animation data '%s'!\n",
                   cstr(animation_file_path));
      return core::nullopt;
    }
    if (!data->Is<AnimatedModelData>()) {
      LOG_GRAPHICS(ERROR,
                   "Loaded animation data is not an animated model '%s'!\n",
                   cstr(animation_file_path));
      return core::nullopt;
    }
    auto & animation_data = data->As<AnimatedModelData>();
    if (animation_data.animation_data.IsEmpty()) {
      LOG_GRAPHICS(ERROR, "Loaded animation data has no animation '%s'!\n",
                   cstr(animation_file_path));
      return core::nullopt;
    }
    if (1u < animation_data.animation_data.Size()) {
      LOG_GRAPHICS(ERROR,
                   "Loaded animation data has too many animations '%s'!\n",
                   cstr(animation_file_path));
      // return core::nullopt;
    }
    if (animation_data.skeleton_data.bones.Size() !=
        animated_model_data.skeleton_data.bones.Size()) {
      LOG_GRAPHICS(ERROR, "Loaded animation data has invalid skeleton '%s'!\n",
                   cstr(animation_file_path));
      return core::nullopt;
    }
    for (auto current = 0u; current < animation_data.skeleton_data.bones.Size();
         ++current) {
      if (animation_data.skeleton_data.bones[current].name !=
          animated_model_data.skeleton_data.bones[current].name) {
        bool found = false;
        for (auto other = current + 1u;
             other < animated_model_data.skeleton_data.bones.Size(); ++other) {
          if (animation_data.skeleton_data.bones[other].name ==
              animated_model_data.skeleton_data.bones[current].name) {
            core::swap(animation_data.skeleton_data.bones[current],
                       animation_data.skeleton_data.bones[other]);
            core::swap(animation_data.animation_data[0u].channels[current],
                       animation_data.animation_data[0u].channels[other]);
            found = true;
            break;
          }
        }
        if (!found) {
          LOG_GRAPHICS(
              ERROR, "Failed to map bone '%s' in '%s'!\n",
              cstr(animated_model_data.skeleton_data.bones[current].name),
              cstr(animation_file_path));
          return core::nullopt;
        }
      }
    }
    animation_data.animation_data[0u].name = animation_file_path.Name();
    animated_model_data.animation_data[animation] =
        move(animation_data.animation_data[0u]);
  }

  // Diffuse Texture
  Texture diffuse_texture;
  {
    auto texture_file_path = _model_directory / "textures" / "diffuse.tga";
    LOG_GRAPHICS(MESSAGE, "Loading '%s'!\n", cstr(texture_file_path));
    diffuse_texture =
        resource::texture_from_file(texture_file_path, TextureSettings());
    if (!diffuse_texture) {
      LOG_GRAPHICS(ERROR, "Failed to load diffuse texture!\n");
      return core::nullopt;
    }
  }

  // Normal Texture
  Texture normal_texture;
  {
    auto texture_file_path = _model_directory / "textures" / "normal.tga";
    LOG_GRAPHICS(MESSAGE, "Loading '%s'!\n", cstr(texture_file_path));
    normal_texture =
        resource::texture_from_file(texture_file_path, TextureSettings());
    if (!normal_texture) {
      LOG_GRAPHICS(ERROR, "Failed to load normal texture!\n");
      return core::nullopt;
    }
  }

  // Specular Texture
  Texture specular_texture;
  {
    auto texture_file_path = _model_directory / "textures" / "specular.tga";
    LOG_GRAPHICS(MESSAGE, "Loading '%s'!\n", cstr(texture_file_path));
    specular_texture =
        resource::texture_from_file(texture_file_path, TextureSettings());
    if (!specular_texture) {
      LOG_GRAPHICS(ERROR, "Failed to load specular texture!\n");
      return core::nullopt;
    }
  }

  // AnimatedModel
  AnimatedModel animated_model;
  animated_model.mesh = create_mesh(
      vertex_data_from_mesh_data(move(animated_model_data.mesh_data)));
  animated_model.skeleton_data = move(animated_model_data.skeleton_data);
  animated_model.animation_data = move(animated_model_data.animation_data);
  animated_model.diffuse_texture = move(diffuse_texture);
  animated_model.normal_texture = move(normal_texture);
  animated_model.specular_texture = move(specular_texture);
  return animated_model;
}

}}