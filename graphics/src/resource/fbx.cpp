#include "graphics/resource/fbx.h"

#define LOG_FBX(__severity__, ...) CORE_LIB_LOG(__severity__, __VA_ARGS__)

#define GRAPHICS_FBX
#include "graphics/resource/fbx/base.hpp"
#include "graphics/resource/fbx/io.hpp"
#include "graphics/resource/fbx/scene.hpp"
#include "graphics/resource/fbx/convert.hpp"
#undef GRAPHICS_FBX

#undef LOG_FBX

////////////////////////////////////////////////////////////////////////////////
// graphics fbx
////////////////////////////////////////////////////////////////////////////////

namespace graphics { namespace fbx {

/*******************************************************************************
** load
*******************************************************************************/

core::Optional<resource::ModelData>
load(core::fs::FilePath const & _file_path) {
  if (auto scene = scene::load(_file_path)) {
    return build(converter::convert(move(scene.Value())));
  }
  return core::nullopt;
}

}}