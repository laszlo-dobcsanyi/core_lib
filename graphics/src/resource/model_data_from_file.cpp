#include "graphics/resource/model_data_from_file.h"
#include "graphics/resource/cgm.hpp"
#include "graphics/resource/obj.hpp"
#include "graphics/resource/fbx.h"

////////////////////////////////////////////////////////////////////////////////
// graphics resource
////////////////////////////////////////////////////////////////////////////////

namespace graphics { namespace resource {

/*******************************************************************************
** model_data_from_file
*******************************************************************************/

core::Optional<ModelData>
model_data_from_file(core::fs::FilePath const & _file_path) {
  auto const extension = _file_path.Extension();
  if (extension == "cgm") {
    return cgm::load(_file_path);
  } else if (extension == "obj") {
    return obj::convert(_file_path);
  } else if (extension == "fbx") {
    return fbx::load(_file_path);
  } else {
    LOG_GRAPHICS(WARNING, "Unknown extension '%s'!\n", cstr(extension));
    return core::nullopt;
  }
}

}}
