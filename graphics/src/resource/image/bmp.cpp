#include "graphics/resource/image/bmp.h"
#include "core/byte_stream.hpp"
#include "core/fs/mapped_file.hpp"
#include "graphics/resource/image/image_data.h"

#define LOG_BMP(__severity__, ...) CORE_LIB_LOG(LOG, __VA_ARGS__)

////////////////////////////////////////////////////////////////////////////////
// e bmp
////////////////////////////////////////////////////////////////////////////////

namespace graphics { namespace resource { namespace bmp {

/*******************************************************************************
** Stream
*******************************************************************************/

using Stream = core::LittleEndianByteStreamReader;

/*******************************************************************************
** ParserState
*******************************************************************************/

struct ParserState {
  uint32 file_size;
  uint32 data_offset;
  int32 width;
  int32 height;
  uint16 bpp;
};

/*******************************************************************************
** parse_bitmap_header
*******************************************************************************/

bool parse_bitmap_header(Stream & _stream, ParserState & _state) {
  LOG_BMP(STAGE, "Parsing bitmap header..\n");
  uint16 header;
  if (_stream.Read(header))
    if (header == U16(0x424D))
      if (_stream.Read(_state.file_size))
        if (4u < _stream.Available()) {
          _stream.Advance(4u);
          return true;
        }
  LOG_BMP(ERROR, "Failed to parse bitmap header!\n");
  return false;
}

/*******************************************************************************
** parse_dib_header
*******************************************************************************/

bool parse_dib_header(Stream & _stream, ParserState & /* _state */) {
  uint32 header_size;
  if (_stream.Read(header_size) && header_size == U32(40)) {
    uint32 width;
    uint32 height;
    uint16 colour_panes;
    uint16 bpp;
    uint32 compression;
    uint32 size;
    uint32 horizontal_resolution;
    uint32 vertical_resolution;
    uint32 colours_in_palette;
    uint32 important_colours;
    if (_stream.Read(width))
      if (_stream.Read(height))
        if (_stream.Read(colour_panes))
          if (_stream.Read(bpp))
            if (_stream.Read(compression))
              if (_stream.Read(size))
                if (_stream.Read(horizontal_resolution))
                  if (_stream.Read(vertical_resolution))
                    if (_stream.Read(colours_in_palette))
                      if (_stream.Read(important_colours)) {
                        if (colour_panes == U16(1))
                          if (compression == U16(0)) {
                            return true;
                          }
                      }
  }
  LOG_BMP(ERROR, "Failed to parse DIB header!\n");
  return false;
}

/*******************************************************************************
** load
*******************************************************************************/

core::Optional<ImageData> load(Stream & _stream) {
  // TODO: Finish this!
  CORE_LIB_UNIMPLEMENTED;
  LOG_BMP(STAGE, "Loading from memory..\n");
  ParserState state;
  if (parse_bitmap_header(_stream, state))
    if (parse_dib_header(_stream, state)) {
      return core::nullopt;
    }
  LOG_BMP(ERROR, "Failed to load from memory!\n");
  return core::nullopt;
}

core::Optional<ImageData> load(core::fs::FilePath const & _file_path) {
  LOG_BMP(STAGE, "Loading BMP image from '%s'..\n", cstr(_file_path));
  auto mapping_reader = core::fs::mapped_file_reader(_file_path);
  if (!mapping_reader) {
    LOG_BMP(ERROR, "Failed to map '%s' into memory!\n", cstr(_file_path));
    return core::nullopt;
  }
  Stream stream(mapping_reader->Bytes());
  return load(stream);
}

}}}
