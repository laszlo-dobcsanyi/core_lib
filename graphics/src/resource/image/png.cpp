#include "graphics/resource/image/png.h"
#include "graphics/base.hpp"
#include "core/fs/mapped_file.hpp"
#include "graphics/base.hpp"

#define LOG_PNG(__severity__, ...) CORE_LIB_LOG(__severity__, __VA_ARGS__)

////////////////////////////////////////////////////////////////////////////////
// e png
////////////////////////////////////////////////////////////////////////////////

namespace graphics { namespace resource { namespace png {

using Stream = core::BigEndianByteStreamReader;

enum ColourType {
  Greyscale = 0,
  TrueColour = 2,
  IndexedColour = 3,
  GreyscaleAlpha = 4,
  TrueColourAlpha = 6,
};

constexpr size_t channel_count(ColourType _colour) {
  switch (_colour) {
    case Greyscale:
      return 1u;
    case TrueColour:
      return 3u;
    case IndexedColour:
      return 1u;
    case GreyscaleAlpha:
      return 2u;
    case TrueColourAlpha:
      return 4u;
  }
  return 0u;
}

/*******************************************************************************
** BitStream
*******************************************************************************/

class BitStream {
  uint32 cache = 0u;
  uint32 cached_bits = 0u;
  uint8 * allocation = nullptr;
  uint8 * allocation_end = nullptr;
  uint8 * buffer = nullptr;
  uint8 * buffer_end = nullptr;

  uint8 ReadByte() {
    if (buffer < buffer_end) {
      return *buffer++;
    }
    return 0u;
  }

public:
  BitStream() = default;
  BitStream(BitStream &&) = default;
  BitStream(BitStream const &) = default;
  BitStream & operator=(BitStream &&) = default;
  BitStream & operator=(BitStream const &) = default;
  ~BitStream() {
    if (IsAllocated()) {
      Deallocate();
    }
  }

  bool IsAllocated() const { return allocation != nullptr; }

  void Allocate(size_t _size) {
    CORE_LIB_ASSERT(!IsAllocated());
    allocation = reinterpret_cast<uint8 *>(core::memory::alloc(_size));
    CORE_LIB_ASSERT(allocation);
    allocation_end = allocation + _size;
    buffer = allocation;
    buffer_end = buffer;
  }

  void Deallocate() {
    CORE_LIB_ASSERT(IsAllocated());
    core::memory::free(reinterpret_cast<uintptr_t>(allocation));
    allocation = nullptr;
  }

  size_t Free() const {
    CORE_LIB_ASSERT(IsAllocated());
    return static_cast<size_t>(allocation_end - buffer_end);
  }

  void Copy(core::span<byte const> _bytes) {
    CORE_LIB_ASSERT(IsAllocated());
    CORE_LIB_ASSERT(_bytes.size() <= Free());
    ::memcpy(buffer_end, _bytes.data(), _bytes.size());
    buffer_end += _bytes.size();
  }

  size_t Available() const {
    CORE_LIB_ASSERT(IsAllocated());
    return static_cast<size_t>(buffer_end - buffer);
  }

  uint32 Read(uint8 _bits) {
    CORE_LIB_ASSERT(IsAllocated());
    CORE_LIB_ASSERT(_bits <= sizeof(cache) * 8u);
    // Fill cache.
    if (cached_bits < _bits) {
      do {
        const uint32 next_byte = ReadByte();
        cache |= next_byte << cached_bits;
        cached_bits += 8u;
      } while (cached_bits <= (sizeof(cache) - 1u) * 8u);
    }
    // Mask result.
    const uint32 mask = (1u << _bits) - 1u;
    const uint32 result = cache & mask;
    // Update cache.
    cache >>= _bits;
    cached_bits -= _bits;
    return result;
  }
};

struct ParserState {
  uint32 width;
  uint32 height;
  uint8 bit_depth;
  ColourType colour;
  uint32 data_size = 0u;
  BitStream bit_stream;
};

/*******************************************************************************
** Chunk
*******************************************************************************/

constexpr uint32 chunk_type(char _c1, char _c2, char _c3, char _c4) {
  return (((_c1 << 24u) | (_c2 << 16u)) | (_c3 << 8u)) | _c4;
}

struct Chunk {
  uint32 type_value;
  uint32 length;
};

bool scan_chunk(Stream & _stream, Chunk & _chunk) {
  if (!_stream.Read(_chunk.length)) {
    LOG_PNG(ERROR, "Invalid chunk data!\n");
    return false;
  }
  if (!_stream.Read(_chunk.type_value)) {
    LOG_PNG(ERROR, "Invalid chunk data!\n");
    return false;
  }
  LOG_PNG(MESSAGE, "Scanning %c%c%c%c chunk of size %" PRIu32 "..\n",
          (_chunk.type_value & (0xFF << 24u)) >> 24u,
          (_chunk.type_value & (0xFF << 16u)) >> 16u,
          (_chunk.type_value & (0xFF << 8u)) >> 8u, (_chunk.type_value & 0xFF),
          _chunk.length);
  return true;
}

bool check_chunk(Stream & _stream, uint32 _length) {
  if (_length + 4u <= _stream.Available()) {
    // TODO: Check CRC!
    _stream.Advance(_length);
    _stream.Advance(4u); // CRC
    return true;
  }
  return false;
}

bool skip_chunk(Stream & _stream, uint32 _length) {
  _stream.Advance(_length);
  _stream.Advance(4u); // CRC
  return true;
}

/*******************************************************************************
** IHDR
*******************************************************************************/

bool is_valid_bit_depth(uint8 _bit_depth) {
  switch (_bit_depth) {
    case 1u:
    case 2u:
    case 4u:
    case 8u:
    case 16u:
      return true;

    default:
      return false;
  }
}

bool parse_IHDR(Stream & _stream, ParserState & _state, uint32 _length) {
  struct IHDR {
    uint32 width;
    uint32 height;
    uint8 bit_depth;
    uint8 colour;
    uint8 compression;
    uint8 filter;
    uint8 interlace;
  } ihdr;

  if (!_stream.Read(ihdr.width)) {
    LOG_PNG(ERROR, "Failed to parse IHDR width!\n");
    return false;
  }
  if (!_stream.Read(ihdr.height)) {
    LOG_PNG(ERROR, "Failed to parse IHDR height!\n");
    return false;
  }
  if (!_stream.Read(ihdr.bit_depth)) {
    LOG_PNG(ERROR, "Failed to parse IHDR bit_depth!\n");
    return false;
  }
  if (!_stream.Read(ihdr.colour)) {
    LOG_PNG(ERROR, "Failed to parse IHDR colour!\n");
    return false;
  }
  if (!_stream.Read(ihdr.compression)) {
    LOG_PNG(ERROR, "Failed to parse IHDR compression!\n");
    return false;
  }
  if (!_stream.Read(ihdr.filter)) {
    LOG_PNG(ERROR, "Failed to parse IHDR filter!\n");
    return false;
  }
  if (!_stream.Read(ihdr.interlace)) {
    LOG_PNG(ERROR, "Failed to parse IHDR interlace!\n");
    return false;
  }

  LOG_PNG(MESSAGE,
          "Width: %" PRIu32 "\tHeight: %" PRIu32 "\tBit Depth: %" PRIu8
          "\tColour: %" PRIu8 "\tCompression: %" PRIu8 "\tFilter: %" PRIu8
          "\tInterlace: %" PRIu8 "\n",
          ihdr.width, ihdr.height, ihdr.bit_depth, ihdr.colour,
          ihdr.compression, ihdr.filter, ihdr.interlace);

  uint32 crc;
  if (!_stream.Read(crc)) {
    LOG_PNG(ERROR, "Failed to parse IHDR width!\n");
    return false;
  }
  if (!is_valid_bit_depth(ihdr.bit_depth)) {
    LOG_PNG(ERROR, "Invalid bit depth!\n");
    return false;
  }
  if (ihdr.compression != 0u) {
    LOG_PNG(ERROR, "Invalid compression!\n");
    return false;
  }
  if (ihdr.filter != 0u) {
    LOG_PNG(ERROR, "Invalid filter!\n");
    return false;
  }
  if (ihdr.interlace != 0u) {
    LOG_PNG(ERROR, "Invalid interlace!\n");
    return false;
  }
  _state.width = ihdr.width;
  _state.height = ihdr.height;
  _state.bit_depth = ihdr.bit_depth;
  _state.colour = static_cast<ColourType>(ihdr.colour);
  return true;
}

/*******************************************************************************
** IDAT
*******************************************************************************/

bool check_IDAT(Stream & _stream, ParserState & _state, uint32 _length) {
  LOG_PNG(MESSAGE, "Checking IDAT chunk..\n");
  _state.data_size += _length;
  check_chunk(_stream, _length);
  return true;
}

bool parse_IDAT(Stream & _stream, ParserState & _state, uint32 _length) {
  LOG_PNG(MESSAGE, "Parsing IDAT chunk..\n");
  _state.bit_stream.Copy(_stream.RemainingBytes(_length));
  skip_chunk(_stream, _length);
  return true;
}

/*******************************************************************************
** IEND
*******************************************************************************/

bool parse_IEND(Stream & _stream, ParserState & /* _state */, uint32 _length) {
  if (_length == 0u) {
    uint32 crc;
    _stream.Read(crc);
    // TODO: Check CRC!
    return true;
  }
  return false;
}

/*******************************************************************************
** scan
*******************************************************************************/

bool scan(Stream & _stream, ParserState & _state) {
  // Check header.
  LOG_PNG(STAGE, "Checking..\n");
  uint64 header;
  if (_stream.Read(header))
    if (header == U64(0x89504E470D0A1A0A)) {
      bool scanning = true;
      // Parse IHDR.
      Chunk chunk;
      if (scan_chunk(_stream, chunk))
        if (chunk.type_value == chunk_type('I', 'H', 'D', 'R'))
          if (parse_IHDR(_stream, _state, chunk.length)) {
            // Scan chunks.
            while (scanning && scan_chunk(_stream, chunk)) {
              switch (chunk.type_value) {
                case chunk_type('I', 'D', 'A', 'T'): {
                  if (!check_IDAT(_stream, _state, chunk.length)) {
                    return false;
                  }
                } break;
                case chunk_type('I', 'E', 'N', 'D'): {
                  if (parse_IEND(_stream, _state, chunk.length)) {
                    scanning = false;
                  } else {
                    return false;
                  }
                } break;
                case chunk_type('P', 'L', 'T', 'E'): {
                  LOG_PNG(ERROR, "Palettes are not supported!\n");
                  return false;
                } break;
                default: {
                  LOG_PNG(MESSAGE, "Checking unknown chunk..\n");
                  check_chunk(_stream, chunk.length);
                }
              }
            }

            if (_state.data_size == 0u) {
              return false;
            }

            LOG_PNG(MESSAGE, "Allocating %" PRIu32 " bytes for data..\n",
                    _state.data_size);
            _state.bit_stream.Allocate(_state.data_size);

            LOG_PNG(STAGE, "Parsing..\n");
            // Parse data.
            _stream.Reset();
            _stream.Advance(
                8u +
                (8u + 13u + 4u)); // = header + IHDR ( header + data + CRC )
            while (scan_chunk(_stream, chunk)) {
              switch (chunk.type_value) {
                case chunk_type('I', 'D', 'A', 'T'): {
                  if (!parse_IDAT(_stream, _state, chunk.length)) {
                    return false;
                  }
                } break;
                case chunk_type('I', 'E', 'N', 'D'): {
                  return true;
                } break;
                default: {
                  LOG_PNG(MESSAGE, "Skipping unknown chunk..\n");
                  skip_chunk(_stream, chunk.length);
                }
              }
            }

            CORE_LIB_ASSERT(_state.bit_stream.Free() == 0u);
          }
    }
  return false;
}

/*******************************************************************************
** load
*******************************************************************************/

core::Optional<ImageData> load(Stream & _stream) {
  // TODO: Finish this!
  ParserState state;
  LOG_PNG(STAGE, "Scanning..\n");
  if (!scan(_stream, state)) {
    LOG_PNG(ERROR, "Invalid type header!\n");
    return core::nullopt;
  }

  // auto const bits_per_pixel = channel_count(state.colour) * state.bit_depth;
  // LOG_PNG(MESSAGE, "Allocating image of size %ldx%ld with %ld bpp..\n", state.width, state.height,
  //        bits_per_pixel);

  // TODO: Padding with low bpp?
  // auto const bytes_per_pixel = bits_per_pixel / 8u;

  LOG_PNG(STAGE, "Decoding..\n");
  return core::nullopt;
}

core::Optional<ImageData> load(core::fs::FilePath const & _file_path) {
  LOG_PNG(STAGE, "Loading PNG image from '%s'..\n", cstr(_file_path));
  auto mapping_reader = core::fs::mapped_file_reader(_file_path);
  if (!mapping_reader) {
    LOG_PNG(ERROR, "Failed to map '%s' into memory!\n", cstr(_file_path));
    return core::nullopt;
  }
  png::Stream stream(mapping_reader->Bytes());
  return png::load(stream);
}

}}}
