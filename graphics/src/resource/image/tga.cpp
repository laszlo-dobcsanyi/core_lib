#include "graphics/resource/image/tga.h"
#include "core/byte_stream.hpp"
#include "core/fs/mapped_file.hpp"

#define LOG_TGA(__severity__, ...) LOG_GRAPHICS(__severity__, __VA_ARGS__)

////////////////////////////////////////////////////////////////////////////////
// e tga
////////////////////////////////////////////////////////////////////////////////

namespace graphics { namespace resource { namespace tga {

/*******************************************************************************
** Parameters
*******************************************************************************/

struct Parameters {
  uint32 row_alignment;
};

/*******************************************************************************
** Stream
*******************************************************************************/

using Stream = core::LittleEndianByteStreamReader;

/*******************************************************************************
** Header
*******************************************************************************/

struct Header {
  uint8 id_length;
  uint8 color_map_type;
  uint8 image_type;
  uint16 map_start;
  uint16 map_length;
  uint8 map_depth;
  uint16 x_offset;
  uint16 y_offset;
  uint16 width;
  uint16 height;
  uint8 pixel_depth;
  uint8 image_descriptor;
};

/*******************************************************************************
** State
*******************************************************************************/

struct State {
  Header header;
  Parameters parameters;
};

/*******************************************************************************
** ImageType
*******************************************************************************/

enum class ImageType {
  NoData = 0,
  UncompressedMapped = 1,
  UncompressedRGB = 2,
  EncodedMapped = 9,
  EncodedRGB = 10,
};

/*******************************************************************************
** parse_header
*******************************************************************************/

bool parse_header(Stream & _stream, Header & _header) {
  LOG_TGA(VERBOSE, "Parsing header..\n");
  if (!_stream.Read(_header.id_length)) {
    LOG_TGA(ERROR, "Failed to parse header id_length!\n");
    return false;
  }
  if (!_stream.Read(_header.color_map_type)) {
    LOG_TGA(ERROR, "Failed to parse header color_map_type!\n");
    return false;
  }
  if (!_stream.Read(_header.image_type)) {
    LOG_TGA(ERROR, "Failed to parse header image_type!\n");
    return false;
  }
  if (!_stream.Read(_header.map_start)) {
    LOG_TGA(ERROR, "Failed to parse header map_start!\n");
    return false;
  }
  if (!_stream.Read(_header.map_length)) {
    LOG_TGA(ERROR, "Failed to parse header map_length!\n");
    return false;
  }
  if (!_stream.Read(_header.map_depth)) {
    LOG_TGA(ERROR, "Failed to parse header map_depth!\n");
    return false;
  }
  if (!_stream.Read(_header.x_offset)) {
    LOG_TGA(ERROR, "Failed to parse header x_offset!\n");
    return false;
  }
  if (!_stream.Read(_header.y_offset)) {
    LOG_TGA(ERROR, "Failed to parse header y_offset!\n");
    return false;
  }
  if (!_stream.Read(_header.width)) {
    LOG_TGA(ERROR, "Failed to parse header width!\n");
    return false;
  }
  if (!_stream.Read(_header.height)) {
    LOG_TGA(ERROR, "Failed to parse header height!\n");
    return false;
  }
  if (!_stream.Read(_header.pixel_depth)) {
    LOG_TGA(ERROR, "Failed to parse header pixel_depth!\n");
    return false;
  }
  if (!_stream.Read(_header.image_descriptor)) {
    LOG_TGA(ERROR, "Failed to parse header image_descriptor!\n");
    return false;
  }
  return true;
}

/*******************************************************************************
** parse_no_data
*******************************************************************************/

core::Optional<ImageData> parse_no_data(Stream &, State &) {
  LOG_TGA(VERBOSE, "No data tga!\n");
  return core::nullopt;
}

/*******************************************************************************
** parse_uncompressed_mapped
*******************************************************************************/

core::Optional<ImageData> parse_uncompressed_mapped(Stream &, State &) {
  // TODO: Finish this!
  CORE_LIB_UNIMPLEMENTED;
  return core::nullopt;
}

/*******************************************************************************
** parse_uncompressed_rgb
*******************************************************************************/

core::Optional<ImageData> parse_uncompressed_rgb(Stream & _stream,
                                                 State & _state) {
  LOG_TGA(VERBOSE, "Parsing uncompressed image..\n");
  ColorFormat format;
  switch (_state.header.pixel_depth) {
    case 24u: {
      format = ColorFormat::BGR8UI;
    } break;
    case 32u: {
      format = ColorFormat::ABGR8UI;
    } break;
    default: {
      LOG_TGA(ERROR,
              "Images other than 24 or 32 bit depth are not supported! (%" PRIu8
              ")\n",
              _state.header.pixel_depth);
      return core::nullopt;
    } break;
  }
  ImageData image(_state.header.width, _state.header.height, format,
                  _state.parameters.row_alignment);
  auto const bpp = format_bpp(format) / 8u;
  auto const row_size = bpp * image.Width();
  auto const data_size = row_size * image.Height();
  core::span<byte const> data_bytes = _stream.RemainingBytes(data_size);
  if (data_bytes.size() != data_size) {
    LOG_TGA(ERROR, "Invalid image size %ld (got %ld!)\n", data_size,
            data_bytes.size());
    return core::nullopt;
  }
  if (image.RowAlignment() == 1u) {
    auto image_bytes = image.GetBytes();
    CORE_LIB_ASSERT(image_bytes.size() == data_bytes.size());
    ::memcpy(image_bytes.data(), data_bytes.data(), data_bytes.size());
  } else {
    auto data_memory = reinterpret_cast<uintptr_t>(data_bytes.data());
    for (auto row = 0u; row < image.Height(); ++row) {
      auto image_row_bytes = image.GetRowBytes(row);
      CORE_LIB_ASSERT(image_row_bytes.size() == row_size);
      ::memcpy(image_row_bytes.data(),
               reinterpret_cast<void const *>(data_memory), row_size);
      data_memory += row_size;
    }
  }
  // TODO: Check eof!
  return core::Optional<ImageData>(move(image));
}

/*******************************************************************************
** parse_encoded_mapped
*******************************************************************************/

core::Optional<ImageData> parse_encoded_mapped(Stream &, State &) {
  // TODO: Finish this!
  CORE_LIB_UNIMPLEMENTED;
  return core::nullopt;
}

/*******************************************************************************
** parse_encoded_rgb
*******************************************************************************/

core::Optional<ImageData> parse_encoded_rgb(Stream &, State &) {
  // TODO: Finish this!
  CORE_LIB_UNIMPLEMENTED;
  return core::nullopt;
}

/*******************************************************************************
** load
*******************************************************************************/

core::Optional<ImageData> load(Stream & _stream, State & _state) {
  if (!parse_header(_stream, _state.header)) {
    LOG_TGA(ERROR, "Failed to parse header!\n");
    return core::nullopt;
  }
  auto const image_type = static_cast<ImageType>(_state.header.image_type);
  switch (image_type) {
    case ImageType::NoData:
      return parse_no_data(_stream, _state);
    case ImageType::UncompressedMapped:
      return parse_uncompressed_mapped(_stream, _state);
    case ImageType::UncompressedRGB:
      return parse_uncompressed_rgb(_stream, _state);
    case ImageType::EncodedMapped:
      return parse_encoded_mapped(_stream, _state);
    case ImageType::EncodedRGB:
      return parse_encoded_rgb(_stream, _state);

    default: {
      LOG_TGA(WARNING, "Unknown tga data type (%" PRIu8 ")!\n",
              _state.header.image_type);
      return core::nullopt;
    }
  }
}

core::Optional<ImageData> load(Stream & _stream) {
  State state;
  state.parameters.row_alignment = 4u;
  return load(_stream, state);
}

core::Optional<ImageData> load(core::fs::FilePath const & _file_path) {
  LOG_TGA(STAGE, "Loading TGA image from '%s'..\n", cstr(_file_path));
  auto mapping_reader = core::fs::mapped_file_reader(_file_path);
  if (!mapping_reader) {
    LOG_TGA(ERROR, "Failed to map '%s' into memory!\n", cstr(_file_path));
    return core::nullopt;
  }
  Stream stream(mapping_reader->Bytes());
  return load(stream);
}

}}}
