#include "graphics/resource/image/image_data_from_file.h"
#include "graphics/resource/image/tga.h"
#include "graphics/resource/image/bmp.h"
#include "graphics/resource/image/png.h"

////////////////////////////////////////////////////////////////////////////////
// graphics resource
////////////////////////////////////////////////////////////////////////////////

namespace graphics { namespace resource {

/*******************************************************************************
** image_data_from_file
*******************************************************************************/

core::Optional<ImageData>
image_data_from_file(core::fs::FilePath const & _file_path) {
  const_string extension = _file_path.Extension();
  if (extension == "tga") {
    return tga::load(_file_path);
  } else if (extension == "bmp") {
    return bmp::load(_file_path);
  } else if (extension == "png") {
    return png::load(_file_path);
  }
  LOG_GRAPHICS(WARNING, "Unknown extension '%s'!\n", cstr(extension));
  return core::nullopt;
}

}}