#ifndef _GRAPHICS_RESOURCE_IMAGE_DATA_HPP_
#define _GRAPHICS_RESOURCE_IMAGE_DATA_HPP_

#include "graphics/resource/image/image_data.h"

////////////////////////////////////////////////////////////////////////////////
// graphics resource
////////////////////////////////////////////////////////////////////////////////

namespace graphics { namespace resource {

/*******************************************************************************
** ImageData
*******************************************************************************/

ImageData::ImageData(uint32 _width, uint32 _height, ColorFormat _format)
    : ImageData(_width, _height, _format, 4u) {}

ImageData::ImageData(uint32 _width, uint32 _height, ColorFormat _format,
                     uint32 _row_alignment)
    : width(_width)
    , height(_height)
    , row_alignment(_row_alignment)
    , format(_format) {
  data = core::make_aligned_memory<4u>(Size());
}

void ImageData::SetFormat(ColorFormat _format) {
  CORE_LIB_ASSERT(format_bpp(_format) == format_bpp(format));
  format = _format;
}

uint32 ImageData::Size() const { return RowSize() * height; }

core::span<byte> ImageData::GetRowBytes(uint32 _row) {
  CORE_LIB_ASSERT(_row < height);
  auto row_bytes = reinterpret_cast<byte *>(data.Get()) + _row * RowSize();
  return core::make_span(row_bytes, RowSize());
}

core::span<byte const> ImageData::GetRowBytes(uint32 _row) const {
  CORE_LIB_ASSERT(_row < height);
  auto row_bytes =
      reinterpret_cast<byte const *>(data.Get()) + _row * RowSize();
  return core::make_span(row_bytes, RowSize());
}

uint32 ImageData::RowSize() const {
  auto const byte_per_pixel = format_bpp(format) / 8u;
  auto const row_data_size = byte_per_pixel * width;
  auto const row_data_padding =
      static_cast<uint32>(core::memory::padding(row_data_size, row_alignment));
  return row_data_size + row_data_padding;
}

}}

#endif
