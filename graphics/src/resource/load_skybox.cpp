#include "graphics/resource/load_skybox.h"
#include "graphics/create_mesh.hpp"
#include "graphics/resource/cubemap_from_file.hpp"

////////////////////////////////////////////////////////////////////////////////
// graphics resource
////////////////////////////////////////////////////////////////////////////////

namespace graphics { namespace resource {

/*******************************************************************************
** load_skybox
*******************************************************************************/

core::Optional<Skybox> load_skybox(core::fs::Directory _skybox_directory) {
  LOG_GRAPHICS(STAGE, "Loading Skybox from '%s'!\n", cstr(_skybox_directory));

  Skybox skybox;
  // Mesh
  {
    container::InlineArray<PositionVertex, 8u> vertices;
    vertices[0u].position = numerics::Vector3f(-100.f, 100.f, 100.f);
    vertices[1u].position = numerics::Vector3f(-100.f, 100.f, -100.f);
    vertices[2u].position = numerics::Vector3f(100.f, 100.f, 100.f);
    vertices[3u].position = numerics::Vector3f(100.f, 100.f, -100.f);
    vertices[4u].position = numerics::Vector3f(-100.f, -100.f, 100.f);
    vertices[5u].position = numerics::Vector3f(-100.f, -100.f, -100.f);
    vertices[6u].position = numerics::Vector3f(100.f, -100.f, 100.f);
    vertices[7u].position = numerics::Vector3f(100.f, -100.f, -100.f);

    container::InlineArray<uint32, 6u * 2u * 3u> indices;
    indices[0u] = 0u;
    indices[1u] = 1u;
    indices[2u] = 2u;
    indices[3u] = 3u;
    indices[4u] = 2u;
    indices[5u] = 1u;
    indices[6u] = 4u;
    indices[7u] = 5u;
    indices[8u] = 0u;
    indices[9u] = 1u;
    indices[10u] = 0u;
    indices[11u] = 5u;
    indices[12u] = 5u;
    indices[13u] = 7u;
    indices[14u] = 1u;
    indices[15u] = 3u;
    indices[16u] = 1u;
    indices[17u] = 7u;
    indices[18u] = 7u;
    indices[19u] = 6u;
    indices[20u] = 3u;
    indices[21u] = 2u;
    indices[22u] = 3u;
    indices[23u] = 6u;
    indices[24u] = 6u;
    indices[25u] = 4u;
    indices[26u] = 2u;
    indices[27u] = 0u;
    indices[28u] = 2u;
    indices[29u] = 4u;
    indices[30u] = 4u;
    indices[31u] = 5u;
    indices[32u] = 6u;
    indices[33u] = 7u;
    indices[34u] = 6u;
    indices[35u] = 5u;

    skybox.mesh = create_mesh(IndexedVertexDataView<PositionVertex>(
        core::make_span(vertices), core::make_span(indices)));
  }

  // Cubemap
  {
    container::InlineArray<core::fs::FilePath, 6u> file_paths;
    file_paths[0u] = _skybox_directory / "right.tga";
    file_paths[1u] = _skybox_directory / "left.tga";
    file_paths[2u] = _skybox_directory / "top.tga";
    file_paths[3u] = _skybox_directory / "bottom.tga";
    file_paths[4u] = _skybox_directory / "front.tga";
    file_paths[5u] = _skybox_directory / "back.tga";

    if (auto cubemap_result =
            resource::cubemap_from_file(core::make_span(file_paths))) {
      skybox.cubemap = move(*cubemap_result);
    } else {
      LOG_GRAPHICS(ERROR, "Failed to load cubemap!\n");
      return core::nullopt;
    }
  }
  return skybox;
}

}}
