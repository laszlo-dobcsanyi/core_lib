#include "graphics/renderers/forward_animated_model_renderer.h"

////////////////////////////////////////////////////////////////////////////////
// graphics
////////////////////////////////////////////////////////////////////////////////

namespace graphics {

/*******************************************************************************
** create_forward_animated_model_renderer
*******************************************************************************/

core::Optional<ForwardAnimatedModelRenderer>
create_forward_animated_model_renderer() {
  ForwardAnimatedModelRenderer renderer;

// Shader
#if defined(CORE_GRAPHICS_OPENGL)
  // clang-format off
  const char * vertex_shader =
    "#version 330 core\n"
    "layout (location = 0) in vec3 position;\n"
    "layout (location = 1) in vec3 normal;\n"
    "layout (location = 2) in vec2 texture_coord;\n"
    "layout (location = 3) in uint bone_indices;\n"
    "layout (location = 4) in vec4 bone_weights;\n"
    "out vec2 texture_coordinate;\n"
    "out vec3 fragment_position;\n"
    "uniform mat4 transformation;\n"
    "uniform mat4 bones[ 128 ];\n"
    "void main() {\n"
    "  uint bone_count = bone_indices & 0x0000000Fu;\n"
    "  uint bone_index_0 = ( bone_indices & 0x000007F0u ) >> 4u;\n"
    "  vec3 animated_position = bone_weights[ 0 ] * "
    "(bones[bone_index_0] * vec4(position, 1.0f)).xyz;\n"
    "  if ( 1u < bone_count ) {\n"
    "    uint bone_index_1 = ( bone_indices & 0x0003F800u ) >> 11u;\n"
    "    animated_position += bone_weights[ 1 ] * (bones[ bone_index_1 "
    "] * vec4(position, 1.f)).xyz;\n"
    "  }\n"
    "  if ( 2u < bone_count ) {\n"
    "    uint bone_index_2 = ( bone_indices & 0x01FC0000u ) >> 18u;\n"
    "    animated_position += bone_weights[ 2 ] * (bones[ bone_index_2 "
    "] * vec4(position, 1.f)).xyz;\n"
    "  }\n"
    "  if ( 3u < bone_count ) {\n"
    "    uint bone_index_3 = ( bone_indices & 0xFE000000u ) >> 25u;\n"
    "    animated_position += bone_weights[ 3 ] * (bones[ bone_index_3 "
    "] * vec4(position, 1.f)).xyz;\n"
    "  }\n"
    "  texture_coordinate = texture_coord;\n"
    "  fragment_position = position;\n"
    "  gl_Position = transformation * vec4(animated_position, 1.f);\n"
    "}\n";
  const char * fragment_shader =
    "#version 330 core\n"
    "in vec2 texture_coordinate;\n"
    "in vec3 fragment_position;\n"
    "out vec4 color;\n"
    "uniform vec3 view_position;\n"
    "uniform sampler2D texture1; // diffuse\n"
    "uniform sampler2D texture2; // normal\n"
    "uniform sampler2D texture3; // specular\n"
    "void main() {\n"
    "  // light\n"
    "  vec3 light_position = vec3( 500.f, 0.f, 0.f );\n"
    "  vec3 light_direction = normalize( light_position - fragment_position );\n"
    "  vec3 light_color = vec3( 1.f, 1.0f, 1.0f );\n"
    "\n"
    "  // material\n"
    "  vec3  material_ambient = vec3( 0.f, 0.f, 0.f );\n"
    "  vec3  material_diffuse = texture( texture1, texture_coordinate ).xyz;\n"
    "  vec3  material_normal = normalize( texture( texture2, texture_coordinate ).xyz );\n"
    "  vec3  material_specular = texture( texture3, texture_coordinate ).xyz;\n"
    "  float material_shininess = 32.f;\n"
    "\n"
    "  // ambient\n"
    "  vec3 ambient = light_color * material_ambient;\n"
    "\n"
    "  // diffuse\n"
    "  float diff = max( 1.f, max( dot( material_normal, light_direction ), 0.f ) );\n"
    "  vec3 diffuse = light_color * ( diff * material_diffuse );\n"
    "\n"
    "  // specular\n"
    "  vec3 view_direction = normalize( view_position - fragment_position );\n"
    "  vec3 reflect_direction = reflect( -light_direction, material_normal );\n"
    "  float spec = max( 1.f, pow( max( dot( view_direction, reflect_direction ), 0.f ), "
    "material_shininess ) );\n"
    "  vec3 specular = light_color * ( spec * 0.1f * material_specular );\n"
    "\n"
    "  color = vec4( ambient + diffuse + specular, 1.f );\n"
    "}\n";
  // clang-format on

  if (!renderer.shader.Create(vertex_shader, fragment_shader)) {
    LOG_GRAPHICS(ERROR, "Failed to create renderer shader!\n");
    return core::nullopt;
  }
  opengl::generate_uniform_locations(renderer);
#else
#  error
#endif

  // Sampler
  if (create(renderer.sampler)) {
    renderer.sampler.Set(TextureWrap::U, TextureWrapMode::Repeat);
    renderer.sampler.Set(TextureWrap::V, TextureWrapMode::Repeat);
    renderer.sampler.Set(TextureFilter::Minify,
                         TextureFilterMode::LinearMipmapLinear);
    renderer.sampler.Set(TextureFilter::Magnify, TextureFilterMode::Linear);
  } else {
    LOG_GRAPHICS(ERROR, "Failed to create sampler!\n");
    return core::nullopt;
  }

  // DiffuseSampler
  if (!create(renderer.diffuse_sampler)) {
    LOG_GRAPHICS(ERROR, "Failed to create diffuse texture sampler!\n");
    return core::nullopt;
  }

  // NormalSampler
  if (!create(renderer.normal_sampler)) {
    LOG_GRAPHICS(ERROR, "Failed to create normal texture sampler!\n");
    return core::nullopt;
  }

  // SpecularSampler
  if (!create(renderer.specular_sampler)) {
    LOG_GRAPHICS(ERROR, "Failed to create specular texture sampler!\n");
    return core::nullopt;
  }

  return renderer;
}

/*******************************************************************************
** bind
*******************************************************************************/

void tag_invoke(bind_t, ForwardAnimatedModelRenderer & _this) {
  _this.diffuse_sampler.Bind(TextureUnit(0));
  _this.diffuse_sampler.Bind(_this.sampler);
  _this.normal_sampler.Bind(TextureUnit(1));
  _this.normal_sampler.Bind(_this.sampler);
  _this.specular_sampler.Bind(TextureUnit(2));
  _this.specular_sampler.Bind(_this.sampler);
  _this.shader.Use();
}

/*******************************************************************************
** render
*******************************************************************************/

void tag_invoke(render_t, ForwardAnimatedModelRenderer const & _this,
                AnimatedModel const & _model,
                TransformationMatrix const & _transformation,
                core::span<TransformationMatrix const> _transformations,
                Camera const & _camera) {
  _this.diffuse_sampler.Activate(_model.diffuse_texture);
  _this.normal_sampler.Activate(_model.normal_texture);
  _this.specular_sampler.Activate(_model.specular_texture);

  ForwardAnimatedModelRendererUniforms uniforms;
  uniforms.transformation = _camera.GetTransformation() * _transformation;
  uniforms.bones = _transformations;
  uniforms.view_position = _camera.GetPosition();
  uniforms.diffuse_sampler = &_this.diffuse_sampler;
  uniforms.normal_sampler = &_this.normal_sampler;
  uniforms.specular_sampler = &_this.specular_sampler;
  render_mesh(_this, uniforms, _model.mesh, graphics::DrawMode::Triangles);
}

}
