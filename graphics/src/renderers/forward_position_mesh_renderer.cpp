#include "graphics/renderers/forward_position_mesh_renderer.h"

////////////////////////////////////////////////////////////////////////////////
// graphics
////////////////////////////////////////////////////////////////////////////////

namespace graphics {

/*******************************************************************************
** create_forward_position_mesh_renderer
*******************************************************************************/

core::Optional<ForwardPositionMeshRenderer>
create_forward_position_mesh_renderer() {
  ForwardPositionMeshRenderer renderer;

// Shader
#if defined(CORE_GRAPHICS_OPENGL)
  // clang-format off
  const char * vertex_shader =
    "#version 330 core\n"
    "layout (location = 0) in vec3 position;\n"
    "uniform mat4 transformation;\n"
    "void main() {\n"
    "  gl_Position = transformation * vec4( position, 1.0f );\n"
    "}\n";
  const char * fragment_shader =
    "#version 330 core\n"
    "uniform vec4 fragment_color;\n"
    "out vec4 color;\n"
    "void main() {\n"
    "  color = fragment_color;\n"
    "}\n";
  // clang-format on

  if (!renderer.shader.Create(vertex_shader, fragment_shader)) {
    LOG_GRAPHICS(ERROR, "Failed to create renderer shader!\n");
    return core::nullopt;
  }
  opengl::generate_uniform_locations(renderer);
#else
#  error
#endif
  return renderer;
}

/*******************************************************************************
** bind
*******************************************************************************/

void tag_invoke(bind_t, ForwardPositionMeshRenderer & _this) {
  _this.shader.Use();
}

/*******************************************************************************
** render
*******************************************************************************/

void tag_invoke(render_t, ForwardPositionMeshRenderer const & _this,
                IndexedMesh<PositionVertex> const & _mesh,
                TransformationMatrix const & _transformation,
                numerics::Vector4f _color, Camera const & _camera) {
  ForwardPositionMeshRendererUniforms uniforms;
  uniforms.transformation = _camera.GetTransformation() * _transformation;
  uniforms.color = _color;
  render_mesh(_this, uniforms, _mesh, graphics::DrawMode::Triangles);
}

void tag_invoke(render_t, ForwardPositionMeshRenderer const & _this,
                ArrayMesh<PositionVertex> const & _mesh,
                TransformationMatrix const & _transformation,
                numerics::Vector4f _color, Camera const & _camera) {
  ForwardPositionMeshRendererUniforms uniforms;
  uniforms.transformation = _camera.GetTransformation() * _transformation;
  uniforms.color = _color;
  render_mesh(_this, uniforms, _mesh, graphics::DrawMode::Triangles);
}

}
