#include "graphics/renderers/forward_static_model_renderer.h"

////////////////////////////////////////////////////////////////////////////////
// graphics
////////////////////////////////////////////////////////////////////////////////

namespace graphics {

/*******************************************************************************
** create_forward_static_model_renderer
*******************************************************************************/

core::Optional<ForwardStaticModelRenderer>
create_forward_static_model_renderer() {
  ForwardStaticModelRenderer renderer;

// Shader
#if defined(CORE_GRAPHICS_OPENGL)
  // clang-format off
  const char * vertex_shader =
    "#version 330 core\n"
    "layout (location = 0) in vec3 position;\n"
    "layout (location = 1) in vec3 normal;\n"
    "layout (location = 2) in vec2 texture_coord;\n"
    "out vec2 texture_coordinate;\n"
    "uniform mat4 transformation;\n"
    "void main() {\n"
    "  gl_Position = transformation * vec4( position, 1.0f );\n"
    "  texture_coordinate = texture_coord;\n"
    "}\n";
  const char * fragment_shader =
    "#version 330 core\n"
    "in vec2 texture_coordinate;\n"
    "out vec4 color;\n"
    "uniform sampler2D texture1; //diffuse\n"
    "void main() {\n"
    "  color = texture( texture1, texture_coordinate );\n"
    "}\n";
  // clang-format on

  if (!renderer.shader.Create(vertex_shader, fragment_shader)) {
    LOG_GRAPHICS(ERROR, "Failed to create renderer shader!\n");
    return core::nullopt;
  }
  opengl::generate_uniform_locations(renderer);
#else
#  error
#endif

  // Sampler
  if (create(renderer.sampler)) {
    renderer.sampler.Set(TextureWrap::U, TextureWrapMode::Repeat);
    renderer.sampler.Set(TextureWrap::V, TextureWrapMode::Repeat);
    renderer.sampler.Set(TextureFilter::Minify,
                         TextureFilterMode::LinearMipmapLinear);
    renderer.sampler.Set(TextureFilter::Magnify, TextureFilterMode::Linear);
  } else {
    LOG_GRAPHICS(ERROR, "Failed to create sampler!\n");
    return core::nullopt;
  }

  // DiffuseSampler
  if (!create(renderer.diffuse_sampler)) {
    LOG_GRAPHICS(ERROR, "Failed to create diffuse texture sampler!\n");
    return core::nullopt;
  }

  return renderer;
}

/*******************************************************************************
** bind
*******************************************************************************/

void tag_invoke(bind_t, ForwardStaticModelRenderer & _this) {
  _this.diffuse_sampler.Bind(TextureUnit(0));
  _this.diffuse_sampler.Bind(_this.sampler);
  _this.shader.Use();
}

/*******************************************************************************
** render
*******************************************************************************/

void tag_invoke(render_t, ForwardStaticModelRenderer const & _this,
                StaticModel const & _model,
                TransformationMatrix const & _transformation,
                Camera const & _camera) {
  _this.diffuse_sampler.Activate(_model.diffuse_texture);

  ForwardStaticModelRendererUniforms uniforms;
  uniforms.transformation = _camera.GetTransformation() * _transformation;
  uniforms.diffuse_sampler = &_this.diffuse_sampler;
  render_mesh(_this, uniforms, _model.mesh, graphics::DrawMode::Triangles);
}

}
