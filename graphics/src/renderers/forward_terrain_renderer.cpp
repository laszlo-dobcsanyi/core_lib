#include "graphics/renderers/forward_terrain_renderer.h"

////////////////////////////////////////////////////////////////////////////////
// graphics
////////////////////////////////////////////////////////////////////////////////

namespace graphics {

/*******************************************************************************
** create_forward_terrain_renderer
*******************************************************************************/

core::Optional<ForwardTerrainRenderer> create_forward_terrain_renderer() {
  ForwardTerrainRenderer renderer;

// Shader
#if defined(CORE_GRAPHICS_OPENGL)
  // clang-format off
  const char * vertex_shader =
    "#version 330\n"
    "layout (location = 0) in vec3 position;\n"
    "layout (location = 1) in vec3 normal;\n"
    "layout (location = 2) in vec2 texture_coord;\n"
    "uniform mat4 projection;\n"
    "uniform mat4 view;\n"
    "uniform vec3 view_position;\n"
    "out float distance;\n"
    "out vec2 texture_coordinate;\n"
    "void main() {\n"
    "  gl_Position = projection * view * vec4( position, 1.0f );\n"
    "  distance = clamp((length(vec3(projection * vec4( position, 1.0f )) - vec3(projection * "
    "vec4( view_position, 1.0f ))) - 16384.f)/ 32768.f, 0.f, 0.8f);\n"
    "  texture_coordinate = texture_coord;\n"
    "}\n";
  const char * fragment_shader =
    "#version 330\n"
    "in float distance;\n"
    "in vec2 texture_coordinate;\n"
    "uniform float blend_size;\n"
    "uniform sampler2D blend;\n"
    "uniform sampler2D texture1;\n"
    "uniform sampler2D texture2;\n"
    "uniform sampler2D texture3;\n"
    "uniform sampler2D texture4;\n"
    "\n"
    "out vec4 color;\n"
    "\n"
    "vec3 blend_layers( vec3 l1, vec3 l2, vec3 l3, vec3 l4, vec4 b ) {\n"
    "  float depth = 0.2f;\n"
    "  float height = max( max( max( b.x, b.y ), b.z ), b.w ) - depth;\n"
    "  float w1 = max( b.x - height, 0.f );\n"
    "  float w2 = max( b.y - height, 0.f );\n"
    "  float w3 = max( b.z - height, 0.f );\n"
    "  float w4 = max( b.w - height, 0.f );\n"
    "  return ( l1 * w1 + l2 * w2 + l3 * w3 + l4 * w4 ) / ( w1 + w2 + w3 + w4 );\n"
    "}\n"
    "\n"
    "void main() {\n"
    "  vec2 blend_pos = texture_coordinate / blend_size;\n"
    "  vec4 blend_vec = texture( blend, blend_pos );\n"
    "  vec3 layer1 = texture( texture1, texture_coordinate ).xyz;\n"
    "  vec3 layer2 = texture( texture2, texture_coordinate ).xyz;\n"
    "  vec3 layer3 = texture( texture3, texture_coordinate ).xyz;\n"
    "  vec3 layer4 = texture( texture4, texture_coordinate ).xyz;\n"
    "  color = vec4( blend_layers( layer1, layer2, layer3, layer4, blend_vec ), 1.f );\n"
    "  color = mix(color, vec4(0.9f, 0.9f, 0.9f, 1.0f), distance);\n"
    "}\n";
  // clang-format on

  if (!renderer.shader.Create(vertex_shader, fragment_shader)) {
    LOG_GRAPHICS(ERROR, "Failed to create renderer shader!\n");
    return core::nullopt;
  }
  opengl::generate_uniform_locations(renderer);
#else
#  error
#endif

  // Sampler
  if (create(renderer.sampler)) {
    renderer.sampler.Set(TextureWrap::U, TextureWrapMode::Repeat);
    renderer.sampler.Set(TextureWrap::V, TextureWrapMode::Repeat);
    renderer.sampler.Set(TextureFilter::Minify,
                         TextureFilterMode::LinearMipmapNearest);
    renderer.sampler.Set(TextureFilter::Magnify, TextureFilterMode::Linear);
  } else {
    LOG_GRAPHICS(ERROR, "Failed to create sampler!\n");
    return core::nullopt;
  }

  // BlendSampler
  if (!create(renderer.blend_sampler)) {
    LOG_GRAPHICS(ERROR, "Failed to create blend sampler!\n");
    return core::nullopt;
  }

  // TextureSampler1
  if (!create(renderer.texture_sampler1)) {
    LOG_GRAPHICS(ERROR, "Failed to create texture sampler 1!\n");
    return core::nullopt;
  }
  // TextureSampler2
  if (!create(renderer.texture_sampler2)) {
    LOG_GRAPHICS(ERROR, "Failed to create texture sampler 2!\n");
    return core::nullopt;
  }
  // TextureSampler3
  if (!create(renderer.texture_sampler3)) {
    LOG_GRAPHICS(ERROR, "Failed to create texture sampler 3!\n");
    return core::nullopt;
  }
  // TextureSampler4
  if (!create(renderer.texture_sampler4)) {
    LOG_GRAPHICS(ERROR, "Failed to create texture sampler 4!\n");
    return core::nullopt;
  }

  return renderer;
}

/*******************************************************************************
** bind ( ForwardTerrainRenderer )
*******************************************************************************/

void tag_invoke(bind_t, ForwardTerrainRenderer & _this) {
  _this.blend_sampler.Bind(TextureUnit(0));
  _this.blend_sampler.Bind(_this.sampler);
  _this.texture_sampler1.Bind(TextureUnit(1));
  _this.texture_sampler1.Bind(_this.sampler);
  _this.texture_sampler2.Bind(TextureUnit(2));
  _this.texture_sampler2.Bind(_this.sampler);
  _this.texture_sampler3.Bind(TextureUnit(3));
  _this.texture_sampler3.Bind(_this.sampler);
  _this.texture_sampler4.Bind(TextureUnit(4));
  _this.texture_sampler4.Bind(_this.sampler);
  _this.shader.Use();
}

/*******************************************************************************
** render ( ForwardTerrainRenderer )
*******************************************************************************/

void tag_invoke(render_t, ForwardTerrainRenderer const & _this,
                Terrain const & _terrain, Camera const & _camera) {
  CORE_LIB_ASSERT(_terrain.layer_textures.Size() == 4u);
  _this.blend_sampler.Activate(_terrain.blend_map);
  _this.texture_sampler1.Activate(_terrain.layer_textures[0u]);
  _this.texture_sampler2.Activate(_terrain.layer_textures[1u]);
  _this.texture_sampler3.Activate(_terrain.layer_textures[2u]);
  _this.texture_sampler4.Activate(_terrain.layer_textures[3u]);

  ForwardTerrainRendererUniforms uniforms;
  uniforms.projection = _camera.GetProjection();
  uniforms.view = _camera.GetView();
  uniforms.view_position = _camera.GetPosition();
  uniforms.blend_size = 256.f;
  uniforms.blend = &_this.blend_sampler;
  uniforms.layer_1 = &_this.texture_sampler1;
  uniforms.layer_2 = &_this.texture_sampler2;
  uniforms.layer_3 = &_this.texture_sampler3;
  uniforms.layer_4 = &_this.texture_sampler4;

  render_mesh(_this, uniforms, _terrain.mesh, graphics::DrawMode::Triangles);
}

}
