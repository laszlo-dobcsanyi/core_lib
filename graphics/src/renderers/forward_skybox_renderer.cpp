#include "graphics/renderers/forward_skybox_renderer.h"

////////////////////////////////////////////////////////////////////////////////
// graphics
////////////////////////////////////////////////////////////////////////////////

namespace graphics {

/*******************************************************************************
** create_forward_skybox_renderer
*******************************************************************************/

core::Optional<ForwardSkyboxRenderer> create_forward_skybox_renderer() {
  ForwardSkyboxRenderer renderer;

// Shader
#if defined(CORE_GRAPHICS_OPENGL)
  // clang-format off
  const char * vertex_shader =
    "#version 330 core\n"
    "layout (location = 0) in vec3 position;\n"
    "uniform mat4 projection;\n"
    "uniform mat4 view;\n"
    "out float height;\n"
    "out vec3 texture_coordinate;\n"
    "void main() {\n"
    "  texture_coordinate = position;\n"
    "  vec4 world_position = projection * view * vec4(position, 1.0);\n"
    "  height = vec3(projection * vec4(position, 1.0)).y;\n"
    "  gl_Position = world_position.xyww;\n"
    "}\n";
  const char * fragment_shader =
    "#version 330 core\n"
    "in float height;\n"
    "in vec3 texture_coordinate;\n"
    "out vec4 color;\n"
    "uniform samplerCube texture1; // skybox\n"
    "void main() {\n"
    "  color = texture(texture1, texture_coordinate);\n"
    "  color = mix(vec4(0.9f, 0.9f, 0.9f, 1.0f), color, clamp(height / 32.f, 0.f, 1.f));\n"
    "}";
  // clang-format on

  if (!renderer.shader.Create(vertex_shader, fragment_shader)) {
    LOG_GRAPHICS(ERROR, "Failed to create renderer shader!\n");
    return core::nullopt;
  }
  opengl::generate_uniform_locations(renderer);
#else
#  error
#endif

  // Sampler
  if (create(renderer.sampler)) {
    renderer.sampler.Set(TextureWrap::U, TextureWrapMode::Clamp);
    renderer.sampler.Set(TextureWrap::V, TextureWrapMode::Clamp);
    renderer.sampler.Set(TextureWrap::W, TextureWrapMode::Clamp);
    renderer.sampler.Set(TextureFilter::Minify, TextureFilterMode::Linear);
    renderer.sampler.Set(TextureFilter::Magnify, TextureFilterMode::Linear);
  } else {
    LOG_GRAPHICS(ERROR, "Failed to create sampler!\n");
    return core::nullopt;
  }

  // DiffuseSampler
  if (!create(renderer.diffuse_sampler)) {
    LOG_GRAPHICS(ERROR, "Failed to create diffuse texture sampler!\n");
    return core::nullopt;
  }

  return renderer;
}

/*******************************************************************************
** bind
*******************************************************************************/

void tag_invoke(bind_t, ForwardSkyboxRenderer & _this) {
  _this.diffuse_sampler.Bind(TextureUnit(0));
  _this.diffuse_sampler.Bind(_this.sampler);
  _this.shader.Use();
}

/*******************************************************************************
** render
*******************************************************************************/

void tag_invoke(render_t, ForwardSkyboxRenderer const & _this,
                Skybox const & _skybox, Camera const & _camera) {
  _this.diffuse_sampler.Activate(_skybox.cubemap);

  ForwardSkyboxRendererUniforms uniforms;
  uniforms.projection = _camera.GetProjection();
  TransformationMatrix view = _camera.GetView();
  numerics::set_translation(view, numerics::Vector3f(0.f, 0.f, 0.f));
  uniforms.view = view;
  uniforms.diffuse_sampler = &_this.diffuse_sampler;
  render_mesh(_this, uniforms, _skybox.mesh, graphics::DrawMode::Triangles);
}

}
