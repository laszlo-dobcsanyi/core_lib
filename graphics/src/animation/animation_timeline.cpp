#include "graphics/animation/animation_timeline.h"

////////////////////////////////////////////////////////////////////////////////
// graphics animation
////////////////////////////////////////////////////////////////////////////////

namespace graphics { namespace animation {

/*******************************************************************************
** AnimationTimeline
*******************************************************************************/

AnimationTimeline::AnimationTimeline(
    core::Ref<resource::SkeletonData> _skeleton_data,
    core::Ref<resource::AnimationData> _animation_data)
    : time(0.f)
    , duration(0.f)
    , skeleton_data(_skeleton_data)
    , animation_data(_animation_data)
    , bone_transformations(_skeleton_data) {
  channels = core::make_unique_array<AnimationChannel>(
      animation_data->channels.Size());
  for (graphics::resource::AnimationChannel & channel :
       animation_data->channels) {
    if (!channel.translation.IsEmpty())
      duration = core::max(
          duration, channel.translation[channel.translation.Size() - 1u].time);
    if (!channel.rotation.IsEmpty())
      duration = core::max(duration,
                           channel.rotation[channel.rotation.Size() - 1u].time);
    if (!channel.scale.IsEmpty())
      duration =
          core::max(duration, channel.scale[channel.scale.Size() - 1u].time);
  }
  Reset();
}

void AnimationTimeline::Update(float _delta) {
  time += _delta;
  for (auto current = 0u; current < channels.Size(); ++current)
    channels[current].Update(time, animation_data->channels[current]);
}

void AnimationTimeline::Reset() {
  if (time != 0.f) {
    time = 0.f;
    for (auto current = 0u; current < channels.Size(); ++current)
      channels[current].Reset(animation_data->channels[current]);
    bone_transformations.Reset();
  }
}

void AnimationTimeline::Animate() {
  TransformationMatrix identity;
  numerics::set_identity(identity);
  AnimateBone(0u, identity);
}

void AnimationTimeline::AnimateBone(
    size_t _index, TransformationMatrix const & _parent_transformation) {
  auto & bones = skeleton_data->bones;

  TransformationMatrix translate;
  numerics::set_translate(translate, channels[_index].translation.value);
  TransformationMatrix rotate;
  numerics::set_rotate(rotate,
                       numerics::to_rotation(channels[_index].rotation.value));
  bone_transformations[_index] = _parent_transformation *
                                 bones[_index].transformation * translate *
                                 rotate * bones[_index].offset;

  size_t child = bones[_index].child;
  while (child != 0u) {
    AnimateBone(child, bone_transformations[_index]);
    child = bones[child].next;
  }
}

}}
