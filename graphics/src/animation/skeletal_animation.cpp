#include "graphics/animation/skeletal_animation.h"

////////////////////////////////////////////////////////////////////////////////
// graphics animation
////////////////////////////////////////////////////////////////////////////////

namespace graphics { namespace animation {

/*******************************************************************************
** SkeletalAnimation
*******************************************************************************/

SkeletalAnimation::SkeletalAnimation(
    resource::SkeletonData & _skeleton_data,
    core::span<resource::AnimationData> _animation_data)
    : skeleton_data(_skeleton_data) {
  for (auto current = 0u; current < _animation_data.size(); ++current) {
    states.Create(_skeleton_data, _animation_data[current]);
  }
  bone_transformations.Create(_skeleton_data.bones.Size());
}

void SkeletalAnimation::Start(resource::AnimationData const & _animation) {
  Start(_animation, false, 1.f);
}

void SkeletalAnimation::Start(AnimationState & _animation_state) {
  Start(_animation_state, false, 1.f);
}

void SkeletalAnimation::Start(resource::AnimationData const & _animation,
                              bool _loop, float _rate) {
  AnimationState * state = FindAnimation(_animation);
  CORE_LIB_ASSERT(state != nullptr);
  Start(*state, _loop, _rate);
}

void SkeletalAnimation::Start(AnimationState & _animation_state, bool _loop,
                              float _rate) {
  _animation_state.SetLooping(_loop);
  _animation_state.SetRate(_rate);
  if (!_animation_state.IsActive()) {
    active_animations.push_front(_animation_state);
    CORE_LIB_ASSERT(_animation_state.IsActive());
  }
  _animation_state.Reset();
}

void SkeletalAnimation::Stop(resource::AnimationData const & _animation) {
  AnimationState * state = FindAnimation(_animation);
  CORE_LIB_ASSERT(state != nullptr);
  Stop(*state);
}

void SkeletalAnimation::Stop(AnimationState & _animation_state) {
  if (_animation_state.IsActive()) {
    active_animations.erase(_animation_state);
  }
}

void SkeletalAnimation::StopAnimations() {
  for (AnimationState & state : active_animations) {
    active_animations.erase(state);
  }
}

void SkeletalAnimation::Update(float _delta) {
  for (AnimationState & state : active_animations) {
    float delta_seconds = _delta * state.GetRate();
    if (state.IsLooping()) {
      if (state.GetDuration() <= state.GetTime() + delta_seconds) {
        delta_seconds -= state.GetDuration() - state.GetTime();
        state.Reset();
      }
    }
    state.Update(delta_seconds);
  }
}

void SkeletalAnimation::Animate() {
  bone_transformations.Reset();
  float weight = 0.f;
  for (AnimationState & state : active_animations) {
    float state_weight =
        state.GetWeight(); // core::min( state.GetWeight(), 1.f - weight );
    if (numerics::is_nearly_zero(state_weight)) {
      continue;
    }
    // CORE_LIB_LOG( LOG, "Animation '%s' weight: %.6f, rate: %.6f\n", cstr( state.GetAnimationData()->name ), state_weight, state.GetRate() );
    weight += state_weight;
    state.Animate();
    bone_transformations.Blend(state_weight,
                               core::make_span(state.GetTransformations()));
  }
  // CORE_LIB_ASSERT( numerics::are_nearly_equal( 1.f, weight ) );
  auto & transformations = bone_transformations.GetTransformations();
  for (auto current = 0u; current < transformations.Size(); ++current) {
    transformations[current] =
        GetSkeletonData().bones[0u].offset * transformations[current];
  }
}

AnimationState * SkeletalAnimation::FindAnimation(
    resource::AnimationData const & _animation_data) {
  for (auto current = 0u; current < states.Size(); ++current) {
    if (&states[current].GetAnimationData() == &_animation_data)
      return &states[current];
  }
  return nullptr;
}

}}
