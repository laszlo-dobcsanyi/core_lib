#include "graphics/animation/bone_transformations.h"

////////////////////////////////////////////////////////////////////////////////
// graphics animation
////////////////////////////////////////////////////////////////////////////////

namespace graphics { namespace animation {

/*******************************************************************************
** BoneTransformations
*******************************************************************************/

BoneTransformations::BoneTransformations(
    core::Ref<resource::SkeletonData> _skeleton_data) {
  Create(_skeleton_data->bones.Size());
}

void BoneTransformations::Create(size_t _bone_count) {
  transformations = core::make_unique_array<TransformationMatrix>(_bone_count);
  Reset();
}

void BoneTransformations::Blend(float _weight,
                                BoneTransformations const & _other) {
  Blend(_weight, core::make_span(_other.GetTransformations()));
}

void BoneTransformations::Blend(
    float _weight, core::span<TransformationMatrix const> _transformations) {
  // CORE_LIB_ASSERT( 0.f <= _weight && _weight <= 1.f );
  CORE_LIB_ASSERT(transformations.Size() == _transformations.size());
  for (auto current = 0u; current < transformations.Size(); ++current) {
    transformations[current] =
        transformations[current] + _transformations[current] * _weight;
  }
}

void BoneTransformations::Reset() {
  for (auto current = 0u; current < transformations.Size(); ++current) {
    numerics::set_zero(transformations[current]);
  }
}

}}