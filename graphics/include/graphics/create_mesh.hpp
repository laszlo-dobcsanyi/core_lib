#ifndef _GRAPHICS_CREATE_MESH_HPP_
#define _GRAPHICS_CREATE_MESH_HPP_

#include "graphics/mesh.hpp"

#if defined(CORE_GRAPHICS_OPENGL)
#  include "graphics/implementation/gl/create_mesh.hpp"
#elif defined(CORE_GRAPHICS_DIRECTX)
#  error
#else
#  error
#endif

////////////////////////////////////////////////////////////////////////////////
// graphics
////////////////////////////////////////////////////////////////////////////////

namespace graphics {

/*******************************************************************************
** create_mesh
*******************************************************************************/

#if defined(CORE_GRAPHICS_OPENGL)
using opengl::create_mesh;
using opengl::set_vertex_data;
#elif defined(CORE_GRAPHICS_DIRECTX)
using directx::create_mesh;
using directx::set_vertex_data;
#else
#  error
#endif

}

#endif
