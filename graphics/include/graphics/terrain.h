#ifndef _GRAPHICS_TERRAIN_H_
#define _GRAPHICS_TERRAIN_H_

#include "graphics/mesh.hpp"
#include "graphics/texture.h"
#include "graphics/resource/map_data.hpp"

////////////////////////////////////////////////////////////////////////////////
// graphics
////////////////////////////////////////////////////////////////////////////////

namespace graphics {

/*******************************************************************************
** Terrain
*******************************************************************************/

struct Terrain {
  IndexedMesh<TexturedVertex> mesh;
  Texture blend_map;
  core::UniqueArray<Texture> layer_textures;
  uint32 size;
};

/*******************************************************************************
** Map
*******************************************************************************/

struct Map {
  const_string name;
  resource::MapData map_data;
  Terrain terrain;
};

}

#endif