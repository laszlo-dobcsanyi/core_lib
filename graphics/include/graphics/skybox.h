#ifndef _GRAPHICS_SKYBOX_H_
#define _GRAPHICS_SKYBOX_H_

#include "graphics/mesh.hpp"
#include "graphics/cubemap.h"

////////////////////////////////////////////////////////////////////////////////
// graphics
////////////////////////////////////////////////////////////////////////////////

namespace graphics {

/*******************************************************************************
** Skybox
*******************************************************************************/

struct Skybox {
  IndexedMesh<PositionVertex> mesh;
  Cubemap cubemap;
};

}

#endif