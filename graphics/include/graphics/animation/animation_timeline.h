#ifndef _GRAPHICS_ANIMATION_ANIMATION_TIMELINE_H_
#define _GRAPHICS_ANIMATION_ANIMATION_TIMELINE_H_

#include "graphics/animation/animation_channel.h"
#include "graphics/animation/bone_transformations.h"

////////////////////////////////////////////////////////////////////////////////
// graphics animation
////////////////////////////////////////////////////////////////////////////////

namespace graphics { namespace animation {

/*******************************************************************************
** AnimationTimeline
*******************************************************************************/

class AnimationTimeline {
public:
  AnimationTimeline(core::Ref<resource::SkeletonData> _skeleton_data,
                    core::Ref<resource::AnimationData> _animation_data);
  AnimationTimeline(AnimationTimeline &&) = default;
  AnimationTimeline(AnimationTimeline const &) = delete;
  AnimationTimeline & operator=(AnimationTimeline &&) = default;
  AnimationTimeline & operator=(AnimationTimeline const &) = delete;
  ~AnimationTimeline() = default;

  void Update(float _delta);

  void Reset();

  void Animate();

  float GetTime() const { return time; }
  float GetDuration() const { return duration; }
  resource::SkeletonData const & GetSkeletonData() const {
    return *skeleton_data;
  }
  resource::AnimationData const & GetAnimationData() const {
    return *animation_data;
  }
  core::UniqueArray<TransformationMatrix> const & GetTransformations() const {
    return bone_transformations.GetTransformations();
  }

private:
  float time;
  float duration;
  core::Ref<resource::SkeletonData> skeleton_data;
  core::Ref<resource::AnimationData> animation_data;
  core::UniqueArray<AnimationChannel> channels;
  BoneTransformations bone_transformations;

  void AnimateBone(size_t _index,
                   TransformationMatrix const & _parent_transformation);
};

}}

#endif
