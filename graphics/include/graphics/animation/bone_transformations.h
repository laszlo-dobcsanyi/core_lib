#ifndef _GRAPHICS_ANIMATION_BONE_TRANSFORMATIONS_H_
#define _GRAPHICS_ANIMATION_BONE_TRANSFORMATIONS_H_

#include "graphics/resource/model_data.hpp"

////////////////////////////////////////////////////////////////////////////////
// graphics animation
////////////////////////////////////////////////////////////////////////////////

namespace graphics { namespace animation {

/*******************************************************************************
** BoneTransformations
*******************************************************************************/

class BoneTransformations {
public:
  BoneTransformations() = default;
  BoneTransformations(core::Ref<resource::SkeletonData> _skeleton_data);
  BoneTransformations(BoneTransformations const & _other) {}
  BoneTransformations(BoneTransformations &&) = default;
  BoneTransformations & operator=(BoneTransformations const & _other) {
    return *this;
  }
  BoneTransformations & operator=(BoneTransformations &&) = default;
  ~BoneTransformations() = default;

  void Create(size_t _bone_count);

  void Blend(float _weight, BoneTransformations const & _other);
  void Blend(float _weight,
             core::span<TransformationMatrix const> _transformations);

  void Reset();

  TransformationMatrix & operator[](size_t _index) {
    return transformations[_index];
  }
  TransformationMatrix const & operator[](size_t _index) const {
    return transformations[_index];
  }

  core::UniqueArray<TransformationMatrix> & GetTransformations() {
    return transformations;
  }
  core::UniqueArray<TransformationMatrix> const & GetTransformations() const {
    return transformations;
  }

private:
  core::UniqueArray<TransformationMatrix> transformations;
};

}}

#endif
