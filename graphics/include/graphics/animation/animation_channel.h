#ifndef _GRAPHICS_ANIMATION_ANIMATION_CHANNEL_H_
#define _GRAPHICS_ANIMATION_ANIMATION_CHANNEL_H_

#include "graphics/resource/model_data.hpp"

////////////////////////////////////////////////////////////////////////////////
// graphics animation
////////////////////////////////////////////////////////////////////////////////

namespace graphics { namespace animation {

/*******************************************************************************
** AnimationValue
*******************************************************************************/

template<typename ValueType,
         ValueType(Interpolate)(ValueType const &, ValueType const &, float)>
class AnimationValue {
public:
  ValueType value;

  AnimationValue(ValueType _default_value)
      : value(_default_value)
      , index(0u) {}

  void Update(float _time,
              core::span<resource::AnimationValue const> _animation_values) {
    if (_animation_values.size() != 0u) {
      while (index < _animation_values.size() - 1u) {
        auto & current_value = _animation_values[index];
        CORE_LIB_ASSERT(current_value.time <= _time);
        auto & next_value = _animation_values[index + 1u];
        if (_time < next_value.time) {
          // No need to update index, we can interpolate
          auto duration = next_value.time - current_value.time;
          auto elapsed = _time - current_value.time;
          auto delta = elapsed / duration;
          value = Interpolate(current_value.value, next_value.value, delta);
          return;
        } else {
          // The next animation value is still behind
          ++index;
          continue;
        }
      }
      // We are behind the last animation value, so just use it
      value = _animation_values[index].value;
    }
  }

  void Reset(core::span<resource::AnimationValue const> _animation_values) {
    index = 0u;
    if (_animation_values.size() != 0u) {
      value = _animation_values[index].value;
    }
  }

private:
  size_t index;
};

/*******************************************************************************
** AnimationChannel
*******************************************************************************/

class AnimationChannel {
public:
  AnimationValue<numerics::Vector3f, numerics::lerp> translation;
  AnimationValue<numerics::Vector3f, numerics::lerp> rotation;
  AnimationValue<numerics::Vector3f, numerics::lerp> scale;

  AnimationChannel()
      : translation(numerics::Vector3f(0.f, 0.f, 0.f))
      , rotation(numerics::Vector3f(0.f, 0.f, 0.f))
      , scale(numerics::Vector3f(1.f, 1.f, 1.f)) {}

  void Update(float _time, resource::AnimationChannel const & _channel) {
    translation.Update(_time, core::make_span(_channel.translation));
    rotation.Update(_time, core::make_span(_channel.rotation));
    scale.Update(_time, core::make_span(_channel.scale));
  }

  void Reset(resource::AnimationChannel const & _channel) {
    translation.Reset(core::make_span(_channel.translation));
    rotation.Reset(core::make_span(_channel.rotation));
    scale.Reset(core::make_span(_channel.scale));
  }
};

}}

#endif