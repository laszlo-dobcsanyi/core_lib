#ifndef _GRAPHICS_ANIMATION_ANIMATION_STATE_H_
#define _GRAPHICS_ANIMATION_ANIMATION_STATE_H_

#include "core/collections/intrusive_list.hpp"
#include "graphics/animation/animation_timeline.h"

////////////////////////////////////////////////////////////////////////////////
// graphics animation
////////////////////////////////////////////////////////////////////////////////

namespace graphics { namespace animation {

/*******************************************************************************
** AnimationState
*******************************************************************************/

class AnimationState
    : public AnimationTimeline
    , public core::intrusive_list_node {
public:
  AnimationState(core::Ref<resource::SkeletonData> _skeleton_data,
                 core::Ref<resource::AnimationData> _animation_data)
      : AnimationTimeline(move(_skeleton_data), move(_animation_data))
      , rate(0.f)
      , weight(0.f)
      , loop(false) {}
  AnimationState(AnimationState &&) = default;
  AnimationState(AnimationState const &) = delete;
  AnimationState & operator=(AnimationState &&) = default;
  AnimationState & operator=(AnimationState const &) = delete;
  ~AnimationState() = default;

  bool IsActive() const { return is_linked(); }

  float GetRate() const { return rate; }
  void SetRate(float _rate) { rate = _rate; }

  float GetWeight() const { return weight; }
  void SetWeight(float _weight) { weight = _weight; }

  bool IsLooping() const { return loop; }
  void SetLooping(bool _loop) { loop = _loop; }

private:
  float rate;
  float weight;
  bool loop;
};

}}

#endif
