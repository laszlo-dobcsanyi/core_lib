#ifndef _GRAPHICS_ANIMATION_SKELETAL_ANIMATION_H_
#define _GRAPHICS_ANIMATION_SKELETAL_ANIMATION_H_

#include "core/collections/intrusive_list.hpp"
#include "graphics/animation/animation_state.h"

////////////////////////////////////////////////////////////////////////////////
// graphics animation
////////////////////////////////////////////////////////////////////////////////

namespace graphics { namespace animation {

/*******************************************************************************
** SkeletalAnimation
*******************************************************************************/

class SkeletalAnimation {
public:
  SkeletalAnimation(resource::SkeletonData & _skeleton_data,
                    core::span<resource::AnimationData> _animation_data);
  SkeletalAnimation(SkeletalAnimation &&) = default;
  SkeletalAnimation(SkeletalAnimation const &) = delete;
  SkeletalAnimation & operator=(SkeletalAnimation &&) = default;
  SkeletalAnimation & operator=(SkeletalAnimation const &) = delete;
  ~SkeletalAnimation() = default;

  void Start(resource::AnimationData const & _animation);
  void Start(AnimationState & _animation_state);
  void Start(resource::AnimationData const & _animation, bool _loop,
             float _rate);
  void Start(AnimationState & _animation_state, bool _loop, float _rate);

  void Stop(resource::AnimationData const & _animation);
  void Stop(AnimationState & _animation_state);

  void StopAnimations();

  void Update(float _delta);

  void Animate();

  resource::SkeletonData & GetSkeletonData() { return *skeleton_data; }
  resource::SkeletonData const & GetSkeletonData() const {
    return *skeleton_data;
  }

  core::UniqueArray<TransformationMatrix> const & GetTransformations() const {
    return bone_transformations.GetTransformations();
  }

  container::Array<AnimationState> & GetAnimationStates() { return states; }
  container::Array<AnimationState> const & GetAnimationStates() const {
    return states;
  }

private:
  core::Ref<resource::SkeletonData> skeleton_data;
  container::Array<AnimationState> states;
  core::intrusive_list<AnimationState> active_animations;
  BoneTransformations bone_transformations;

  AnimationState *
  FindAnimation(resource::AnimationData const & _animation_data);
};

}}

#endif