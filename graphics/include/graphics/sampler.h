#ifndef _GRAPHICS_SAMPLER_H_
#define _GRAPHICS_SAMPLER_H_

#include "graphics/base.hpp"

#if defined(CORE_GRAPHICS_OPENGL)
#  include "graphics/implementation/gl/sampler.h"
#elif defined(CORE_GRAPHICS_DIRECTX)
#  error
#  include "graphics/implementation/dx/sampler.h"
#else
#  error
#endif

////////////////////////////////////////////////////////////////////////////////
// graphics
////////////////////////////////////////////////////////////////////////////////

namespace graphics {

/*******************************************************************************
** Sampler
*******************************************************************************/

#if defined(CORE_GRAPHICS_OPENGL)
using Sampler = opengl::Sampler;
using opengl::create_sampler;
#elif defined(CORE_GRAPHICS_DIRECTX)
#  error
using Sampler = directx::Sampler;
using directx::create_sampler;
#else
#  error
#endif

}

#endif
