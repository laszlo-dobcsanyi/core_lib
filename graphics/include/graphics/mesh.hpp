#ifndef _GRAPHICS_MESH_HPP_
#define _GRAPHICS_MESH_HPP_

#include "graphics/base.hpp"

#if defined(CORE_GRAPHICS_OPENGL)
#  include "graphics/implementation/gl/mesh.hpp"
#elif defined(CORE_GRAPHICS_DIRECTX)
#  include "graphics/implementation/dx/mesh.hpp"
#else
#  error
#endif

////////////////////////////////////////////////////////////////////////////////
// graphics
////////////////////////////////////////////////////////////////////////////////

namespace graphics {

/*******************************************************************************
** Mesh
*******************************************************************************/

#if defined(CORE_GRAPHICS_OPENGL)
template<typename Vertex, VertexDataType DataType>
using Mesh = opengl::Mesh<Vertex, DataType>;
#elif defined(CORE_GRAPHICS_DIRECTX)
#  error
template<typename Vertex, VertexDataType DataType>
using Mesh = directx::Mesh<Vertex, DataType>;
#else
#  error
#endif

template<typename Vertex>
using ArrayMesh = Mesh<Vertex, VertexDataType::Array>;
template<typename Vertex>
using IndexedMesh = Mesh<Vertex, VertexDataType::Indexed>;

}

#endif
