#ifndef _GRAPHICS_CAMERA_H_
#define _GRAPHICS_CAMERA_H_

#include "graphics/base.hpp"

////////////////////////////////////////////////////////////////////////////////
// graphics
////////////////////////////////////////////////////////////////////////////////

namespace graphics {

/*******************************************************************************
** Camera
*******************************************************************************/

class Camera : public Resource<Camera> {
public:
  Camera();
  Camera(Camera &&) = default;
  Camera(Camera const &) = default;
  Camera & operator=(Camera &&) = default;
  Camera & operator=(Camera const &) = default;
  ~Camera() = default;

  void SetOrtho();
  void SetOrtho(float _left, float _right, float _bottom, float _top,
                float _near, float _far);
  void SetPerspective();
  void SetPerspective(float _fov, float _aspect, float _near, float _far);

  void Set(numerics::Vector3f const & _position,
           numerics::Vector3f const & _target);
  void Set(numerics::Vector3f const & _position,
           numerics::Vector3f const & _target, numerics::Vector3f const & _up);

  numerics::Vector3f const & GetPosition() const { return position; }

  TransformationMatrix & GetProjection() { return projection.Value(); }
  TransformationMatrix const & GetProjection() const {
    return projection.Value();
  }

  TransformationMatrix & GetView() { return view.Value(); }
  TransformationMatrix const & GetView() const { return view.Value(); }

  TransformationMatrix & GetTransformation() { return view_projection.Value(); }
  TransformationMatrix const & GetTransformation() const {
    return view_projection.Value();
  }

private:
  numerics::Vector3f position;
  core::Optional<TransformationMatrix> projection;
  core::Optional<TransformationMatrix> view;
  core::Optional<TransformationMatrix> view_projection;

  void UpdateViewProjection();
};

}

#endif
