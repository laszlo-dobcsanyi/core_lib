#ifndef _GRAPHICS_WINDOW_H_
#define _GRAPHICS_WINDOW_H_

#include "graphics/base.hpp"

#if defined(CORE_GRAPHICS_GLX)
#  include "graphics/implementation/gl/glx/window.h"
#elif defined(CORE_GRAPHICS_WGL)
#  include "graphics/implementation/gl/wgl/window.h"
#elif defined(CORE_GRAPHICS_DIRECTX)
#  include "graphics/implementation/dx/window.h"
#endif

////////////////////////////////////////////////////////////////////////////////
// graphics
////////////////////////////////////////////////////////////////////////////////

namespace graphics {

/*******************************************************************************
** Window
*******************************************************************************/

#if defined(CORE_GRAPHICS_GLX)
using Window = glx::Window;
#elif defined(CORE_GRAPHICS_WGL)
using Window = wgl::Window;
#elif defined(CORE_GRAPHICS_DIRECTX)
using Window = dx::Window;
#endif

}

#endif
