#ifndef _GRAPHICS_REFLECT_UNIFORMS_HPP_
#define _GRAPHICS_REFLECT_UNIFORMS_HPP_

#include "graphics/base.hpp"

////////////////////////////////////////////////////////////////////////////////
// graphics
////////////////////////////////////////////////////////////////////////////////

namespace graphics {

/*******************************************************************************
** reflect_uniforms
*******************************************************************************/

struct reflect_uniforms_t {
  template<typename Uniforms, typename UniformContext>
  auto operator()(mpl::type<Uniforms>, UniformContext & _context) const {
    return tag_invoke(*this, mpl::type<Uniforms>(), _context);
  }
};
constexpr reflect_uniforms_t reflect_uniforms;

}

#if defined(CORE_GRAPHICS_OPENGL)
#  include "graphics/implementation/gl/uniforms.h"
#  include "graphics/implementation/gl/draw.hpp"
#elif defined(CORE_GRAPHICS_DIRECTX)
#  include "graphics/implementation/dx/render_mesh.h"
#  include "graphics/implementation/dx/draw.hpp"
#else
#  error
#endif

////////////////////////////////////////////////////////////////////////////////
// graphics implementation
////////////////////////////////////////////////////////////////////////////////

namespace graphics { namespace implementation {

#if defined(CORE_GRAPHICS_OPENGL)
template<typename Uniforms>
using UniformLocations = opengl::UniformLocations<Uniforms>;
using opengl::draw;
using opengl::generate_uniform_locations;
using opengl::set_uniforms;
#elif defined(CORE_GRAPHICS_DIRECTX)
template<typename Uniforms>
using UniformLocations = directx::UniformLocations<Uniforms>;
using directx::draw;
using directx::generate_uniform_locations;
using directx::set_uniforms;
#else
#  error
#endif

}}

#endif