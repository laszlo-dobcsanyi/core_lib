#ifndef _GRAPHICS_FRAMEBUFFER_HPP_
#define _GRAPHICS_FRAMEBUFFER_HPP_

#include "graphics/base.hpp"

#if defined(CORE_GRAPHICS_OPENGL)
#  include "graphics/implementation/gl/framebuffer.h"
#elif defined(CORE_GRAPHICS_DIRECTX)
#  error
#  include "graphics/implementation/dx/framebuffer.h"
#else
#  error
#endif

////////////////////////////////////////////////////////////////////////////////
// graphics
////////////////////////////////////////////////////////////////////////////////

namespace graphics {

/*******************************************************************************
** FrameBuffer
*******************************************************************************/

#if defined(CORE_GRAPHICS_OPENGL)
using FrameBuffer = opengl::FrameBuffer;
#elif defined(CORE_GRAPHICS_DIRECTX)
#  error
using FrameBuffer = directx::FrameBuffer;
#else
#  error
#endif

}

#endif
