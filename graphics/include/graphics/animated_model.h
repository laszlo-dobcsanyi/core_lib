#ifndef _GRAPHICS_ANIMATED_MODEL_H_
#define _GRAPHICS_ANIMATED_MODEL_H_

#include "graphics/texture.h"
#include "graphics/mesh.hpp"
#include "graphics/resource/model_data.hpp"

////////////////////////////////////////////////////////////////////////////////
// graphics
////////////////////////////////////////////////////////////////////////////////

namespace graphics {

/*******************************************************************************
** SkinnedMesh
*******************************************************************************/

using SkinnedMesh = IndexedMesh<SkinnedVertex>;

/*******************************************************************************
** AnimatedModel
*******************************************************************************/

struct AnimatedModel {
  const_string name;
  SkinnedMesh mesh;
  resource::SkeletonData skeleton_data;
  core::UniqueArray<resource::AnimationData> animation_data;
  Texture diffuse_texture;
  Texture normal_texture;
  Texture specular_texture;
};

}

#endif
