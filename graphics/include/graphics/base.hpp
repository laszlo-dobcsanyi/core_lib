#ifndef _GRAPHICS_BASE_HPP_
#define _GRAPHICS_BASE_HPP_

#include "core/base.h"
#include "core/string.hpp"
#include "core/numerics.hpp"
#include "core/memory/object.hpp"
#include "core/optional.hpp"

#define LOG_GRAPHICS(__severity__, ...) CORE_LIB_LOG(__severity__, __VA_ARGS__)

#define CORE_GRAPHICS_BASE
#include "graphics/base/definitions.hh"
#include "graphics/base/vertex_types.hpp"
#include "graphics/base/vertex_data.hpp"
#undef CORE_GRAPHICS_BASE

////////////////////////////////////////////////////////////////////////////////
// graphics
////////////////////////////////////////////////////////////////////////////////

namespace graphics {

/*******************************************************************************
** Resource
*******************************************************************************/

template<typename ResourceType>
class Resource : public core::WeakCounter<ResourceType> {};

/*******************************************************************************
** ColorFormat
*******************************************************************************/

enum class ColorFormat {
  RGB,
  RGBA,
  RGB8UI,
  RGBA8UI,

  BGR,
  BGRA,
  BGR8UI,
  ABGR8UI,
};

constexpr uint32 format_bpp(ColorFormat _format) {
  switch (_format) {
    case ColorFormat::RGB:
      return 24u;
    case ColorFormat::RGBA:
      return 32u;
    case ColorFormat::RGB8UI:
      return 24u;
    case ColorFormat::RGBA8UI:
      return 32u;

    case ColorFormat::BGR:
      return 24u;
    case ColorFormat::BGRA:
      return 32u;
    case ColorFormat::BGR8UI:
      return 24u;
    case ColorFormat::ABGR8UI:
      return 32u;

    default:
      CORE_LIB_ASSERT(false);
      return 0;
  }
}

constexpr uint32 rgba(uint8 _r, uint8 _g, uint8 _b, uint8 _a) {
  return (_r << 24u) | (_g << 16u) | (_b << 8u) | _a;
}

constexpr uint32 abgr(uint8 _a, uint8 _b, uint8 _g, uint8 _r) {
  return (_a << 24u) | (_b << 16u) | (_g << 8u) | _r;
}

constexpr uint32 bgra(uint8 _b, uint8 _g, uint8 _r, uint8 _a) {
  return (_b << 24u) | (_g << 16u) | (_r << 8u) | _a;
}

/*******************************************************************************
** RenderbufferFormat
*******************************************************************************/

enum class RenderbufferFormat {
  Depth32,
  Depth24Stencil8,
};

/*******************************************************************************
** TransformationMAtrix
*******************************************************************************/

#if defined(CORE_GRAPHICS_OPENGL)
using TransformationMatrix = numerics::ColumnMajorMatrix4x4f;
#elif defined(CORE_GRAPHICS_DIRECTX)
using TransformationMatrix = numerics::RowMajorMatrix4x4f;
#else
#  error
#endif

/*******************************************************************************
** DrawMode
*******************************************************************************/

enum class DrawMode {
  Points,
  Lines,
  LineStrip,
  LineLoop,
  Triangles,
  TriangleStrip,
  TriangleFan,
};

/*******************************************************************************
** UniformId
*******************************************************************************/

enum class UniformId {
  Projection,
  View,
  Transformation,
  FragmentColor,
  Bones,
  ViewPosition,
  BlendSize,
  Blend,
  Texture1,
  Texture2,
  Texture3,
  Texture4,
  Time,
  TimeDelta,
  Resolution,
};

constexpr char const * UniformIdName(UniformId _id) {
  switch (_id) {
    case UniformId::Projection:
      return "projection";
    case UniformId::View:
      return "view";
    case UniformId::Transformation:
      return "transformation";
    case UniformId::FragmentColor:
      return "fragment_color";
    case UniformId::Bones:
      return "bones";
    case UniformId::ViewPosition:
      return "view_position";
    case UniformId::BlendSize:
      return "blend_size";
    case UniformId::Blend:
      return "blend";
    case UniformId::Texture1:
      return "texture1";
    case UniformId::Texture2:
      return "texture2";
    case UniformId::Texture3:
      return "texture3";
    case UniformId::Texture4:
      return "texture4";
    case UniformId::Time:
      return "time";
    case UniformId::TimeDelta:
      return "time_delta";
    case UniformId::Resolution:
      return "resolution";
    default:
      CORE_LIB_ASSERT(false);
      return "unknown";
  }
}

/*******************************************************************************
** TextureWrap
*******************************************************************************/

enum class TextureWrap {
  U,
  V,
  W,
};

/*******************************************************************************
** TextureWrapMode
*******************************************************************************/

enum class TextureWrapMode {
  Clamp,
  Mirror,
  Repeat,
};

/*******************************************************************************
** TextureFilter
*******************************************************************************/

enum class TextureFilter {
  Minify,
  Magnify,
};

/*******************************************************************************
** TextureFilterMode
*******************************************************************************/

enum class TextureFilterMode {
  Nearest,
  Linear,
  NearestMipmapNearest,
  NearestMipmapLinear,
  LinearMipmapNearest,
  LinearMipmapLinear,
};

/*******************************************************************************
** TextureSettings
*******************************************************************************/

struct TextureSettings {
  bool generate_mipmap = false;
  TextureWrapMode wrap_u = TextureWrapMode::Repeat;
  TextureWrapMode wrap_v = TextureWrapMode::Repeat;
  TextureWrapMode wrap_w = TextureWrapMode::Repeat;
  TextureFilterMode filter_minify = TextureFilterMode::Nearest;
  TextureFilterMode filter_magnify = TextureFilterMode::Nearest;
};

/*******************************************************************************
** KeyState
*******************************************************************************/

enum class KeyState {
  Pressed,
  Released,
};

/*******************************************************************************
** Button
*******************************************************************************/

enum class Button {
  Left,
  Right,
  Middle,

  Count
};

constexpr size_t ButtonCount() { return static_cast<size_t>(Button::Count); }

/*******************************************************************************
** Key
*******************************************************************************/

enum class Key {
  Unknown = -1

  ,
  k0,
  k1,
  k2,
  k3,
  k4,
  k5,
  k6,
  k7,
  k8,
  k9

  ,
  kA,
  kB,
  kC,
  kD,
  kE,
  kF,
  kG,
  kH,
  kI,
  kJ,
  kK,
  kL,
  kM,
  kN,
  kO,
  kP,
  kQ,
  kR,
  kS,
  kT,
  kU,
  kV,
  kW,
  kX,
  kY,
  kZ

  ,
  kSpace,
  kApostrophe,
  kComma,
  kMinus,
  kPeriod,
  kSlash,
  kBackslash,
  kSemicolon,
  kEqual,
  kLBracket,
  kRBracket,
  kGraveAccent

  ,
  kEscape,
  kEnter,
  kTab,
  kBackspace,
  kInsert,
  kDelete,
  kRight,
  kLeft,
  kDown,
  kUp,
  kPageUp,
  kPageDown,
  kHome,
  kEnd,
  kCapsLock,
  kScrollLock,
  kNumLock,
  kPrintScreen,
  kPause,
  kLShift,
  kRShift,
  kLControl,
  kRControl,
  kLAlt,
  kRAlt,
  kLSuper,
  kRSuper,
  kMenu

  ,
  kF1,
  kF2,
  kF3,
  kF4,
  kF5,
  kF6,
  kF7,
  kF8,
  kF9,
  kF10,
  kF11,
  kF12

  ,
  kKP_0,
  kKP_1,
  kKP_2,
  kKP_3,
  kKP_4,
  kKP_5,
  kKP_6,
  kKP_7,
  kKP_8,
  kKP_9,
  kKP_Decimal,
  kKP_Divide,
  kKP_Multiply,
  kKP_Subtract,
  kKP_Add,
  kKP_Enter,
  kKP_Equal

  ,
  Count
};

constexpr size_t KeyCount() { return static_cast<size_t>(Key::Count); }

/*******************************************************************************
** WindowPosition
*******************************************************************************/

using WindowPosition = numerics::Vector2<int32>;

/*******************************************************************************
** WindowSize
*******************************************************************************/

using WindowSize = numerics::Vector2<int32>;

/*******************************************************************************
** WindowParameters
*******************************************************************************/

struct WindowParameters {
  bool monitor = false;
  bool decorated = true;
  bool resizable = true;
  bool floating = true;
  bool maximized = false;
  WindowSize desired_size = graphics::WindowSize(640, 480);
  const_string title;
};

/*******************************************************************************
** RenderContextParameters
*******************************************************************************/

struct RenderContextParameters {
  ColorFormat color_format = ColorFormat::RGBA;
  uint32 depth_bits = 24u;
  uint32 stencil_bits = 8u;
};

}

#endif
