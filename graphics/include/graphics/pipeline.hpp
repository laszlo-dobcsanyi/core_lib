#ifndef _GRAPHICS_PIPELINE_HPP_
#define _GRAPHICS_PIPELINE_HPP_

#include "graphics/renderer.hpp"

////////////////////////////////////////////////////////////////////////////////
// graphics
////////////////////////////////////////////////////////////////////////////////

namespace graphics {

namespace detail {

template<class>
struct tuple_size;

template<class... Types>
struct tuple_size<std::tuple<Types...> &>
    : public mpl::IntegralConstant<size_t, sizeof...(Types)> {};

template<class Tuple, class F, size_t... Indices, class... Args>
void for_each_impl(Tuple && _tuple, F && _f, mpl::Sequence<Indices...>,
                   Args &&... _args) {
  int fold[] = {
      (_f(std::get<Indices>(::forward<Tuple>(_tuple)), forward<Args>(_args)...),
       0)...};
}

template<class Tuple, class F, class... Args>
void for_each(Tuple && _tuple, F && _f, Args &&... _args) {
  constexpr size_t Size = tuple_size<Tuple>::value;
  for_each_impl(::forward<Tuple>(_tuple), forward<F>(_f),
                mpl::GenerateSequence<tuple_size<Tuple>::value>(),
                forward<Args>(_args)...);
}

}

template<class Renderer, class Adapter>
struct RendererAdapter {
  RendererAdapter(Renderer && _renderer, Adapter && _adapter)
      : renderer(forward<Renderer>(_renderer))
      , adapter(forward<Adapter>(_adapter)) {}

private:
  Renderer renderer;
  Adapter adapter;

  friend void tag_invoke(bind_t, RendererAdapter<Renderer, Adapter> & _this) {
    bind(_this.renderer);
  }

  template<class PipelineState, class ModelType>
  friend void tag_invoke(render_t, RendererAdapter<Renderer, Adapter> & _this,
                         PipelineState & _pipeline_state,
                         ModelType const & _model) {
    _this.adapter(_this.renderer, _pipeline_state, _model);
  }
};

/*******************************************************************************
** adapt_renderer
*******************************************************************************/

template<class Renderer, class Adapter>
auto adapt_renderer(Renderer && _renderer, Adapter && _adapter) {
  return RendererAdapter<Renderer, Adapter>(forward<Renderer>(_renderer),
                                            forward<Adapter>(_adapter));
}

template<class SceneIterator, class Renderer>
struct ObjectRenderer {
  ObjectRenderer(SceneIterator && _scene_iterator, Renderer && _renderer)
      : scene_iterator(_scene_iterator)
      , renderer(forward<Renderer>(_renderer)) {}

private:
  SceneIterator scene_iterator;
  Renderer renderer;

  template<class PipelineState, class SceneType>
  friend void
  tag_invoke(render_t, ObjectRenderer<SceneIterator, Renderer> & _this,
             PipelineState & _pipeline_state, SceneType const & _scene) {
    bool first = true;
    for (const auto & object : _this.scene_iterator(_scene)) {
      if (first) {
        first = false;
        bind(_this.renderer);
      }
      render(_this.renderer, _pipeline_state, object);
    }
  }
};

/*******************************************************************************
** object_renderer
*******************************************************************************/

template<class SceneIterator, class Renderer>
auto object_renderer(SceneIterator && _scene_iterator, Renderer && _renderer) {
  return ObjectRenderer<SceneIterator, Renderer>(
      forward<SceneIterator>(_scene_iterator), forward<Renderer>(_renderer));
}

template<class... ObjectRenderers>
struct Pass {
  Pass(ObjectRenderers &&... _object_renderers)
      : object_renderers(forward<ObjectRenderers>(_object_renderers)...) {}

private:
  std::tuple<ObjectRenderers...> object_renderers;

  template<class PipelineState, class SceneType>
  friend void tag_invoke(render_t, Pass<ObjectRenderers...> & _this,
                         PipelineState & _pipeline_state,
                         SceneType const & _scene) {
    detail::for_each(_this.object_renderers, render, _pipeline_state, _scene);
  }
};

/*******************************************************************************
** pass
*******************************************************************************/

template<class... ObjectRenderers>
auto pass(ObjectRenderers &&... _object_renderers) {
  return Pass<ObjectRenderers...>(
      forward<ObjectRenderers>(_object_renderers)...);
}

template<class... Passes>
struct Pipeline {
  Pipeline(Passes &&... _passes)
      : passes(forward<Passes>(_passes)...) {}

private:
  std::tuple<Passes...> passes;

  template<class PipelineState, class SceneType>
  friend void tag_invoke(render_t, Pipeline<Passes...> & _this,
                         PipelineState && _pipeline_state,
                         SceneType const & _scene) {
    detail::for_each(_this.passes, render, _pipeline_state, _scene);
  }
};

/*******************************************************************************
** pipeline
*******************************************************************************/

template<class... Passes>
auto pipeline(Passes &&... _passes) {
  return Pipeline<Passes...>(forward<Passes>(_passes)...);
}

}

#endif
