#ifndef _GRAPHICS_RENDERERS_FORWARD_SKYBOX_RENDERER_H_
#define _GRAPHICS_RENDERERS_FORWARD_SKYBOX_RENDERER_H_

#include "graphics/camera.h"
#include "graphics/texture.h"
#include "graphics/sampler.h"
#include "graphics/texture_sampler.h"
#include "graphics/renderer.hpp"
#include "graphics/skybox.h"

////////////////////////////////////////////////////////////////////////////////
// graphics
////////////////////////////////////////////////////////////////////////////////

namespace graphics {

/*******************************************************************************
** ForwardSkyboxRendererUniforms
*******************************************************************************/

struct ForwardSkyboxRendererUniforms {
  TransformationMatrix projection;
  TransformationMatrix view;
  TextureSampler const * diffuse_sampler = nullptr;

private:
  template<typename UniformContext>
  friend auto tag_invoke(graphics::reflect_uniforms_t,
                         mpl::type<ForwardSkyboxRendererUniforms>,
                         UniformContext & _context) {
    return _context
        .Member(graphics::UniformId::Projection,
                &ForwardSkyboxRendererUniforms::projection)
        .Member(graphics::UniformId::View, &ForwardSkyboxRendererUniforms::view)
        .Member(graphics::UniformId::Texture1,
                &ForwardSkyboxRendererUniforms::diffuse_sampler);
    ;
  }
};

/*******************************************************************************
** ForwardSkyboxRenderer
*******************************************************************************/

struct ForwardSkyboxRenderer
    : public Resource<ForwardSkyboxRenderer>
    , public RendererBase<ForwardSkyboxRendererUniforms> {
  Sampler sampler;
  TextureSampler diffuse_sampler;

private:
  friend void tag_invoke(bind_t, ForwardSkyboxRenderer & _this);
  friend void tag_invoke(render_t, ForwardSkyboxRenderer const & _this,
                         Skybox const & _skybox, Camera const & _camera);
};

/*******************************************************************************
** create_forward_skybox_renderer
*******************************************************************************/

core::Optional<ForwardSkyboxRenderer> create_forward_skybox_renderer();

}

#endif
