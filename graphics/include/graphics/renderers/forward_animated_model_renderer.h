#ifndef _GRAPHICS_RENDERERS_FORWARD_ANIMATED_MODEL_RENDERER_H_
#define _GRAPHICS_RENDERERS_FORWARD_ANIMATED_MODEL_RENDERER_H_

#include "graphics/camera.h"
#include "graphics/texture.h"
#include "graphics/sampler.h"
#include "graphics/texture_sampler.h"
#include "graphics/renderer.hpp"
#include "graphics/animated_model.h"

////////////////////////////////////////////////////////////////////////////////
// graphics
////////////////////////////////////////////////////////////////////////////////

namespace graphics {

/*******************************************************************************
** ForwardAnimatedModelRendererUniforms
*******************************************************************************/

struct ForwardAnimatedModelRendererUniforms {
  TransformationMatrix transformation;
  core::span<TransformationMatrix const> bones;
  numerics::Vector3f view_position;
  TextureSampler const * diffuse_sampler = nullptr;
  TextureSampler const * normal_sampler = nullptr;
  TextureSampler const * specular_sampler = nullptr;

private:
  template<typename UniformContext>
  friend auto tag_invoke(graphics::reflect_uniforms_t,
                         mpl::type<ForwardAnimatedModelRendererUniforms>,
                         UniformContext & _context) {
    return _context
        .Member(graphics::UniformId::Transformation,
                &ForwardAnimatedModelRendererUniforms::transformation)
        .Member(graphics::UniformId::Bones,
                &ForwardAnimatedModelRendererUniforms::bones)
        .Member(graphics::UniformId::ViewPosition,
                &ForwardAnimatedModelRendererUniforms::view_position)
        .Member(graphics::UniformId::Texture1,
                &ForwardAnimatedModelRendererUniforms::diffuse_sampler)
        .Member(graphics::UniformId::Texture2,
                &ForwardAnimatedModelRendererUniforms::normal_sampler)
        .Member(graphics::UniformId::Texture3,
                &ForwardAnimatedModelRendererUniforms::specular_sampler);
  }
};

/*******************************************************************************
** ForwardAnimatedModelRenderer
*******************************************************************************/

struct ForwardAnimatedModelRenderer
    : public Resource<ForwardAnimatedModelRenderer>
    , public RendererBase<ForwardAnimatedModelRendererUniforms> {
  Sampler sampler;
  TextureSampler diffuse_sampler;
  TextureSampler normal_sampler;
  TextureSampler specular_sampler;

private:
  friend void tag_invoke(bind_t, ForwardAnimatedModelRenderer & _this);
  friend void
  tag_invoke(render_t, ForwardAnimatedModelRenderer const & _this,
             AnimatedModel const & _model,
             TransformationMatrix const & _transformation,
             core::span<TransformationMatrix const> _transformations,
             Camera const & _camera);
};

/*******************************************************************************
** create_forward_animated_model_renderer
*******************************************************************************/

core::Optional<ForwardAnimatedModelRenderer>
create_forward_animated_model_renderer();

}

#endif
