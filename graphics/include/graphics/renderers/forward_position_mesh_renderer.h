#ifndef _GRAPHICS_RENDERERS_FORWARD_POSITION_MESH_RENDERER_HPP_
#define _GRAPHICS_RENDERERS_FORWARD_POSITION_MESH_RENDERER_HPP_

#include "graphics/camera.h"
#include "graphics/renderer.hpp"
#include "graphics/sampler.h"
#include "graphics/texture_sampler.h"

////////////////////////////////////////////////////////////////////////////////
// graphics
////////////////////////////////////////////////////////////////////////////////

namespace graphics {

/*******************************************************************************
** ForwardPositionMeshRendererUniforms
*******************************************************************************/

struct ForwardPositionMeshRendererUniforms {
  TransformationMatrix transformation;
  numerics::Vector4f color;

private:
  template<typename UniformContext>
  friend auto tag_invoke(graphics::reflect_uniforms_t,
                         mpl::type<ForwardPositionMeshRendererUniforms>,
                         UniformContext & _context) {
    return _context
        .Member(graphics::UniformId::Transformation,
                &ForwardPositionMeshRendererUniforms::transformation)
        .Member(graphics::UniformId::FragmentColor,
                &ForwardPositionMeshRendererUniforms::color);
  }
};

/*******************************************************************************
** ForwardPositionMeshRenderer
*******************************************************************************/

struct ForwardPositionMeshRenderer
    : public RendererBase<ForwardPositionMeshRendererUniforms> {
private:
  friend void tag_invoke(bind_t, ForwardPositionMeshRenderer & _this);
  friend void tag_invoke(render_t, ForwardPositionMeshRenderer const & _this,
                         IndexedMesh<PositionVertex> const & _mesh,
                         TransformationMatrix const & _transformation,
                         numerics::Vector4f _color, Camera const & _camera);
  friend void tag_invoke(render_t, ForwardPositionMeshRenderer const & _this,
                         ArrayMesh<PositionVertex> const & _mesh,
                         TransformationMatrix const & _transformation,
                         numerics::Vector4f _color, Camera const & _camera);
};

/*******************************************************************************
** create_forward_position_mesh_renderer
*******************************************************************************/

core::Optional<ForwardPositionMeshRenderer>
create_forward_position_mesh_renderer();

}

#endif
