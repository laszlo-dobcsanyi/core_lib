#ifndef _GRAPHICS_RENDERERS_FORWARD_STATIC_MODEL_RENDERER_H_
#define _GRAPHICS_RENDERERS_FORWARD_STATIC_MODEL_RENDERER_H_

#include "graphics/camera.h"
#include "graphics/texture.h"
#include "graphics/sampler.h"
#include "graphics/texture_sampler.h"
#include "graphics/renderer.hpp"
#include "graphics/static_model.h"

////////////////////////////////////////////////////////////////////////////////
// graphics
////////////////////////////////////////////////////////////////////////////////

namespace graphics {

/*******************************************************************************
** ForwardStaticModelRendererUniforms
*******************************************************************************/

struct ForwardStaticModelRendererUniforms {
  TransformationMatrix transformation;
  TextureSampler const * diffuse_sampler = nullptr;

private:
  template<typename UniformContext>
  friend auto tag_invoke(graphics::reflect_uniforms_t,
                         mpl::type<ForwardStaticModelRendererUniforms>,
                         UniformContext & _context) {
    return _context
        .Member(graphics::UniformId::Transformation,
                &ForwardStaticModelRendererUniforms::transformation)
        .Member(graphics::UniformId::Texture1,
                &ForwardStaticModelRendererUniforms::diffuse_sampler);
  }
};

/*******************************************************************************
** ForwardStaticModelRenderer
*******************************************************************************/

struct ForwardStaticModelRenderer
    : public Resource<ForwardStaticModelRenderer>
    , public RendererBase<ForwardStaticModelRendererUniforms> {
  Sampler sampler;
  TextureSampler diffuse_sampler;

private:
  friend void tag_invoke(bind_t, ForwardStaticModelRenderer & _this);
  friend void tag_invoke(render_t, ForwardStaticModelRenderer const & _this,
                         StaticModel const & _model,
                         TransformationMatrix const & _transformation,
                         Camera const & _camera);
};

/*******************************************************************************
** create_forward_static_model_renderer
*******************************************************************************/

core::Optional<ForwardStaticModelRenderer>
create_forward_static_model_renderer();

}

#endif
