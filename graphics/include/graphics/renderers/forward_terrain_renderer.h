#ifndef _GRAPHICS_RENDERERS_TERRAIN_RENDERER_H_
#define _GRAPHICS_RENDERERS_TERRAIN_RENDERER_H_

#include "graphics/camera.h"
#include "graphics/texture.h"
#include "graphics/sampler.h"
#include "graphics/texture_sampler.h"
#include "graphics/renderer.hpp"
#include "graphics/terrain.h"

////////////////////////////////////////////////////////////////////////////////
// graphics
////////////////////////////////////////////////////////////////////////////////

namespace graphics {

/*******************************************************************************
** ForwardTerrainRendererUniforms
*******************************************************************************/

struct ForwardTerrainRendererUniforms {
  TransformationMatrix projection;
  TransformationMatrix view;
  numerics::Vector3f view_position;
  float blend_size;
  TextureSampler const * blend = nullptr;
  TextureSampler const * layer_1 = nullptr;
  TextureSampler const * layer_2 = nullptr;
  TextureSampler const * layer_3 = nullptr;
  TextureSampler const * layer_4 = nullptr;

private:
  template<typename UniformContext>
  friend auto tag_invoke(graphics::reflect_uniforms_t,
                         mpl::type<ForwardTerrainRendererUniforms>,
                         UniformContext & _context) {
    return _context
        .Member(graphics::UniformId::Projection,
                &ForwardTerrainRendererUniforms::projection)
        .Member(graphics::UniformId::View,
                &ForwardTerrainRendererUniforms::view)
        .Member(graphics::UniformId::ViewPosition,
                &ForwardTerrainRendererUniforms::view_position)
        .Member(graphics::UniformId::BlendSize,
                &ForwardTerrainRendererUniforms::blend_size)
        .Member(graphics::UniformId::Blend,
                &ForwardTerrainRendererUniforms::blend)
        .Member(graphics::UniformId::Texture1,
                &ForwardTerrainRendererUniforms::layer_1)
        .Member(graphics::UniformId::Texture2,
                &ForwardTerrainRendererUniforms::layer_2)
        .Member(graphics::UniformId::Texture3,
                &ForwardTerrainRendererUniforms::layer_3)
        .Member(graphics::UniformId::Texture4,
                &ForwardTerrainRendererUniforms::layer_4);
  }
};

/*******************************************************************************
** ForwardTerrainRenderer
*******************************************************************************/

struct ForwardTerrainRenderer
    : public Resource<ForwardTerrainRenderer>
    , public RendererBase<ForwardTerrainRendererUniforms> {
  Sampler sampler;
  TextureSampler blend_sampler;
  TextureSampler texture_sampler1;
  TextureSampler texture_sampler2;
  TextureSampler texture_sampler3;
  TextureSampler texture_sampler4;

private:
  friend void tag_invoke(bind_t, ForwardTerrainRenderer & _this);
  friend void tag_invoke(render_t, ForwardTerrainRenderer const & _this,
                         Terrain const & _terrain, Camera const & _camera);
};

/*******************************************************************************
** create_forward_terrain_renderer
*******************************************************************************/

core::Optional<ForwardTerrainRenderer> create_forward_terrain_renderer();

}

#endif
