#ifndef _GRAPHICS_RENDERBUFFER_H_
#define _GRAPHICS_RENDERBUFFER_H_

#include "graphics/base.hpp"

#if defined(CORE_GRAPHICS_OPENGL)
#  include "graphics/implementation/gl/renderbuffer.h"
#elif defined(CORE_GRAPHICS_DIRECTX)
#  error
#  include "graphics/implementation/dx/renderbuffer.h"
#else
#  error
#endif

////////////////////////////////////////////////////////////////////////////////
// graphics
////////////////////////////////////////////////////////////////////////////////

namespace graphics {

/*******************************************************************************
** RenderBuffer
*******************************************************************************/

#if defined(CORE_GRAPHICS_OPENGL)
using RenderBuffer = opengl::RenderBuffer;
#elif defined(CORE_GRAPHICS_DIRECTX)
#  error
using RenderBuffer = directx::RenderBuffer;
#else
#  error
#endif

}

#endif
