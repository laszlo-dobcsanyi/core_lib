#ifndef _GRAPHICS_IMPLEMENTATION_WINDOW_WINDOW_EVENTS_HPP_
#define _GRAPHICS_IMPLEMENTATION_WINDOW_WINDOW_EVENTS_HPP_

#include "core/signal/signal.hpp"

////////////////////////////////////////////////////////////////////////////////
// graphics window
////////////////////////////////////////////////////////////////////////////////

namespace graphics { namespace window {

template<typename T>
using WindowEvent = core::Signal<T>;

/*******************************************************************************
** CreatedEventArgs
*******************************************************************************/

struct CreatedEventArgs {
  WindowPosition position;
  WindowSize size;
};
using CreatedEvent = WindowEvent<void(CreatedEventArgs const &)>;
using CreatedDelegate = typename CreatedEvent::DelegateType;

/*******************************************************************************
** ResizedEventArgs
*******************************************************************************/

struct ResizedEventArgs {
  WindowSize size;
};
using ResizedEvent = WindowEvent<void(ResizedEventArgs const &)>;
using ResizedDelegate = typename ResizedEvent::DelegateType;

/*******************************************************************************
** DestroyEventArgs
*******************************************************************************/

struct DestroyEventArgs {};
using DestroyEvent = WindowEvent<void(DestroyEventArgs const &)>;
using DestroyDelegate = typename DestroyEvent::DelegateType;

/*******************************************************************************
** KeyEventArgs
*******************************************************************************/

struct KeyEventArgs {
  Key key;
};

/*******************************************************************************
** KeyPressedEvent
*******************************************************************************/

struct KeyPressedEventArgs : public KeyEventArgs {};
using KeyPressedEvent = WindowEvent<void(KeyPressedEventArgs const &)>;
using KeyPressedDelegate = typename KeyPressedEvent::DelegateType;

/*******************************************************************************
** KeyReleaseEventArgs
*******************************************************************************/

struct KeyReleasedEventArgs : public KeyEventArgs {};
using KeyReleasedEvent = WindowEvent<void(KeyReleasedEventArgs const &)>;
using KeyReleasedDelegate = typename KeyReleasedEvent::DelegateType;

/*******************************************************************************
** MouseButtonEventArgs
*******************************************************************************/

struct MouseButtonEventArgs {
  Button button;
};

/*******************************************************************************
** MousePressedEventArgs
*******************************************************************************/

struct MousePressedEventArgs : public MouseButtonEventArgs {};
using MousePressedEvent = WindowEvent<void(MousePressedEventArgs const &)>;
using MousePressedDelegate = typename MousePressedEvent::DelegateType;

/*******************************************************************************
** MouseReleaseEventArgs
*******************************************************************************/

struct MouseReleasedEventArgs : public MouseButtonEventArgs {};
using MouseReleasedEvent = WindowEvent<void(MouseReleasedEventArgs const &)>;
using MouseReleasedDelegate = typename MouseReleasedEvent::DelegateType;

/*******************************************************************************
** MouseMovedEventArgs
*******************************************************************************/

struct MouseMovedEventArgs {
  int32 delta_x;
  int32 delta_y;
};
using MouseMovedEvent = WindowEvent<void(MouseMovedEventArgs const &)>;
using MouseMovedDelegate = typename MouseMovedEvent::DelegateType;

}}

#endif
