#ifndef _GRAPHICS_IMPLEMENTATION_WINDOW_WINDOW_BASE_HPP_
#define _GRAPHICS_IMPLEMENTATION_WINDOW_WINDOW_BASE_HPP_

#include "graphics/implementation/window/window_events.hpp"

////////////////////////////////////////////////////////////////////////////////
// graphics window
////////////////////////////////////////////////////////////////////////////////

namespace graphics { namespace window {

/*******************************************************************************
** WindowBase
*******************************************************************************/

class WindowBase {
public:
  CreatedEvent OnCreated;
  ResizedEvent OnResized;
  DestroyEvent OnDestroy;
  KeyPressedEvent OnKeyPressed;
  KeyReleasedEvent OnKeyReleased;
  MousePressedEvent OnMousePressed;
  MouseReleasedEvent OnMouseReleased;
  MouseMovedEvent OnMouseMoved;

  WindowBase();
  WindowBase(WindowBase &&) = delete;
  WindowBase(WindowBase const &) = delete;
  WindowBase & operator=(WindowBase &&) = delete;
  WindowBase & operator=(WindowBase const &) = delete;
  ~WindowBase() = default;

  WindowSize Size() const { return size; }
  int32 Width() const { return size[0]; }
  int32 Height() const { return size[1]; }

  KeyState GetKeyState(Key _key) const {
    return keys[static_cast<size_t>(_key)];
  }
  KeyState GetButtonState(Button _button) const {
    return buttons[static_cast<size_t>(_button)];
  }

  core::Optional<WindowPosition> MousePosition() const {
    return mouse_position;
  }
  core::Optional<WindowPosition> MouseDelta() const { return mouse_delta; }
  int32 MouseWheel() const { return mouse_wheel; }

  bool IsGrabbingMouse() const { return grab_mouse; }
  void SetMouseGrab(bool _value) { grab_mouse = _value; }

protected:
  void StartFrame();
  void SetMousePosition(WindowPosition _position) {
    mouse_position = _position;
  }
  void ClearMousePosition() { mouse_position = core::nullopt; }

  void OnCreatedEvent(WindowSize _size);
  void OnResizedEvent(WindowSize _size);
  void OnDestroyEvent();
  void OnKeyPressedEvent(Key _key);
  void OnKeyReleasedEvent(Key _key);
  void OnMousePressedEvent(Button _button);
  void OnMouseReleasedEvent(Button _button);
  void OnMouseMovedEvent(int32 _delta_x, int32 _delta_y);
  void OnMouseWheelUpEvent();
  void OnMouseWheelDownEvent();

private:
  WindowSize size;
  KeyState keys[KeyCount()];
  KeyState buttons[ButtonCount()];
  core::Optional<WindowPosition> mouse_position = core::nullopt;
  core::Optional<WindowPosition> mouse_delta = core::nullopt;
  int32 mouse_wheel = 0;
  bool grab_mouse = false;
};

}}

////////////////////////////////////////////////////////////////////////////////
// graphics window
////////////////////////////////////////////////////////////////////////////////

namespace graphics { namespace window {

/*******************************************************************************
** WindowBase
*******************************************************************************/

inline WindowBase::WindowBase() {
  for (auto key = 0u; key < KeyCount(); ++key) {
    keys[key] = KeyState::Released;
  }
  for (auto button = 0u; button < ButtonCount(); ++button) {
    buttons[button] = KeyState::Released;
  }
}

inline void WindowBase::StartFrame() {
  mouse_delta = core::nullopt;
  mouse_wheel = 0;
}

inline void WindowBase::OnCreatedEvent(WindowSize _size) {
  size = _size;

  CreatedEventArgs args;
  args.position =
      WindowPosition(0, 0); // TODO: Fill additional CreatedEvent parameters!
  args.size = _size;
  OnCreated(args);
}

inline void WindowBase::OnResizedEvent(WindowSize _size) {
  size = _size;

  ResizedEventArgs args;
  args.size = _size;
  OnResized(args);
}

inline void WindowBase::OnDestroyEvent() {
  DestroyEventArgs args;
  OnDestroy(args);
}

inline void WindowBase::OnKeyPressedEvent(Key _key) {
  CORE_LIB_ASSERT(_key != Key::Unknown);
  auto const key_index = static_cast<size_t>(_key);
  if (keys[key_index] != KeyState::Pressed) {
    keys[key_index] = KeyState::Pressed;

    KeyPressedEventArgs args;
    args.key = _key;
    OnKeyPressed(args);
  }
}

inline void WindowBase::OnKeyReleasedEvent(Key _key) {
  CORE_LIB_ASSERT(_key != Key::Unknown);
  auto const key_index = static_cast<size_t>(_key);
  if (keys[key_index] != KeyState::Released) {
    keys[key_index] = KeyState::Released;

    KeyReleasedEventArgs args;
    args.key = _key;
    OnKeyReleased(args);
  }
}

inline void WindowBase::OnMousePressedEvent(Button _button) {
  auto const button_index = static_cast<size_t>(_button);
  if (buttons[button_index] != KeyState::Pressed) {
    buttons[button_index] = KeyState::Pressed;

    MousePressedEventArgs args;
    args.button = _button;
    OnMousePressed(args);
  }
}

inline void WindowBase::OnMouseReleasedEvent(Button _button) {
  auto const button_index = static_cast<size_t>(_button);
  if (buttons[button_index] != KeyState::Released) {
    buttons[button_index] = KeyState::Released;

    MouseReleasedEventArgs args;
    args.button = _button;
    OnMouseReleased(args);
  }
}

inline void WindowBase::OnMouseMovedEvent(int32 _delta_x, int32 _delta_y) {
  mouse_delta = WindowPosition(_delta_x, _delta_y);

  MouseMovedEventArgs args;
  args.delta_x = _delta_x;
  args.delta_y = _delta_y;
  OnMouseMoved(args);
}

inline void WindowBase::OnMouseWheelUpEvent() {
  CORE_LIB_ASSERT(0 <= mouse_wheel);
  ++mouse_wheel;
}

inline void WindowBase::OnMouseWheelDownEvent() {
  CORE_LIB_ASSERT(mouse_wheel <= 0);
  --mouse_wheel;
}

}}

#endif
