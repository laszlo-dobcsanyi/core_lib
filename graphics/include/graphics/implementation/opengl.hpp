#ifndef _GRAPHICS_IMPLEMENTATION_OPENGL_HPP_
#define _GRAPHICS_IMPLEMENTATION_OPENGL_HPP_

#include "graphics/base.hpp"

#ifndef CORE_GRAPHICS_OPENGL
#  error
#endif

#include "graphics/api/opengl/opengl.h"

////////////////////////////////////////////////////////////////////////////////
// graphics opengl
////////////////////////////////////////////////////////////////////////////////

namespace graphics { namespace opengl {

namespace detail {

using ObjectValueType = GLuint;
static const ObjectValueType InvalidObject = 0u;

using LocationValueType = GLint;
static const LocationValueType InvalidLocation = -1;

}

/*******************************************************************************
** ObjectTypes
*******************************************************************************/

using ObjectType = core::Value<detail::ObjectValueType, detail::InvalidObject>;
using BufferObjectType = ObjectType;
using ContainerObjectType = ObjectType;
using TextureObjectType = ObjectType;
using SamplerObjectType = ObjectType;
using ShaderObjectType = ObjectType;
using FramebufferObjectType = ObjectType;
using RenderbufferObjectType = ObjectType;

/*******************************************************************************
** LocationTypes
*******************************************************************************/

using LocationType =
    core::Value<detail::LocationValueType, detail::InvalidLocation>;
using UniformLocationType = LocationType;
using TextureUnit = LocationType;

/*******************************************************************************
** GlEnum
*******************************************************************************/

template<typename>
struct GlEnum;
template<>
struct GlEnum<int8> : public mpl::IntegralConstant<GLenum, GL_BYTE> {};
template<>
struct GlEnum<uint8>
    : public mpl::IntegralConstant<GLenum, GL_UNSIGNED_BYTE> {};
template<>
struct GlEnum<int16> : public mpl::IntegralConstant<GLenum, GL_SHORT> {};
template<>
struct GlEnum<uint16>
    : public mpl::IntegralConstant<GLenum, GL_UNSIGNED_SHORT> {};
template<>
struct GlEnum<int32> : public mpl::IntegralConstant<GLenum, GL_INT> {};
template<>
struct GlEnum<uint32>
    : public mpl::IntegralConstant<GLenum, GL_UNSIGNED_INT> {};
template<>
struct GlEnum<float> : public mpl::IntegralConstant<GLenum, GL_FLOAT> {};
template<>
struct GlEnum<double> : public mpl::IntegralConstant<GLenum, GL_DOUBLE> {};

/*******************************************************************************
** GlDrawMode
*******************************************************************************/

constexpr GLenum GlDrawMode(DrawMode _mode) {
  switch (_mode) {
    case DrawMode::Points:
      return GL_POINTS;
    case DrawMode::Lines:
      return GL_LINES;
    case DrawMode::LineStrip:
      return GL_LINE_STRIP;
    case DrawMode::LineLoop:
      return GL_LINE_LOOP;
    case DrawMode::Triangles:
      return GL_TRIANGLES;
    case DrawMode::TriangleStrip:
      return GL_TRIANGLE_STRIP;
    case DrawMode::TriangleFan:
      return GL_TRIANGLE_FAN;
    default:
      return 0;
  }
}

/*******************************************************************************
** GlTypeName
*******************************************************************************/

constexpr char const * GlTypeName(GLenum _type) {
  switch (_type) {
    case GL_BYTE:
      return "GL_BYTE";
    case GL_UNSIGNED_BYTE:
      return "GL_UNSIGNED_BYTE";
    case GL_SHORT:
      return "GL_SHORT";
    case GL_UNSIGNED_SHORT:
      return "GL_UNSIGNED_SHORT";
    case GL_INT:
      return "GL_INT";
    case GL_UNSIGNED_INT:
      return "GL_UNSIGNED_INT";
    case GL_FLOAT:
      return "GL_FLOAT";
    case GL_DOUBLE:
      return "GL_DOUBLE";
    default:
      return "???";
  }
}

/*******************************************************************************
** GlTextureWrap
*******************************************************************************/

constexpr GLint GlTextureWrap(TextureWrap _wrap) {
  switch (_wrap) {
    case TextureWrap::U: {
      return GL_TEXTURE_WRAP_S;
    } break;
    case TextureWrap::V: {
      return GL_TEXTURE_WRAP_T;
    } break;
    case TextureWrap::W: {
      return GL_TEXTURE_WRAP_R;
    } break;
    default: {
      CORE_LIB_ASSERT_MESSAGE("Unhandled TextureWrap!\n");
      return 0;
    } break;
  }
  CORE_LIB_ASSERT_MESSAGE("Unknown TextureWrap!");
  return 0;
}

/*******************************************************************************
** GlTextureWrapMode
*******************************************************************************/

constexpr GLint GlTextureWrapMode(TextureWrapMode _mode) {
  switch (_mode) {
    case TextureWrapMode::Clamp: {
      return GL_CLAMP_TO_EDGE;
    } break;
    case TextureWrapMode::Mirror: {
      return GL_MIRRORED_REPEAT;
    } break;
    case TextureWrapMode::Repeat: {
      return GL_REPEAT;
    } break;
    default: {
      CORE_LIB_ASSERT_MESSAGE("Unhandled TextureWrapMode!\n");
      return 0;
    } break;
  }
  CORE_LIB_ASSERT_MESSAGE("Unknown TextureWrapMode!");
  return 0;
}

/*******************************************************************************
** GlTextureFilter
*******************************************************************************/

constexpr GLint GlTextureFilter(TextureFilter _filter) {
  switch (_filter) {
    case TextureFilter::Minify: {
      return GL_TEXTURE_MIN_FILTER;
    } break;
    case TextureFilter::Magnify: {
      return GL_TEXTURE_MAG_FILTER;
    } break;
    default: {
      CORE_LIB_ASSERT_MESSAGE("Unhandled TextureFilter!\n");
      return 0;
    } break;
  }
  CORE_LIB_ASSERT_MESSAGE("Unknown TextureFilter!");
  return 0;
}

/*******************************************************************************
** GlTextureFilterMode
*******************************************************************************/

constexpr GLint GlTextureFilterMode(TextureFilterMode _mode) {
  switch (_mode) {
    case TextureFilterMode::Nearest: {
      return GL_NEAREST;
    } break;
    case TextureFilterMode::Linear: {
      return GL_LINEAR;
    } break;
    case TextureFilterMode::NearestMipmapNearest: {
      return GL_NEAREST_MIPMAP_NEAREST;
    } break;
    case TextureFilterMode::NearestMipmapLinear: {
      return GL_NEAREST_MIPMAP_LINEAR;
    } break;
    case TextureFilterMode::LinearMipmapNearest: {
      return GL_LINEAR_MIPMAP_NEAREST;
    } break;
    case TextureFilterMode::LinearMipmapLinear: {
      return GL_LINEAR_MIPMAP_LINEAR;
    } break;
    default: {
      CORE_LIB_ASSERT_MESSAGE("Unhandled TextureFilterMode!\n");
      return 0;
    } break;
  }
  CORE_LIB_ASSERT_MESSAGE("Unknown TextureFilterMode!");
  return 0;
}

}}

#endif
