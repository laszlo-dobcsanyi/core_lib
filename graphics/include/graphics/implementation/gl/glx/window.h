#ifndef _GRAPHICS_IMPLEMENTATION_GL_GLX_WINDOW_H_
#define _GRAPHICS_IMPLEMENTATION_GL_GLX_WINDOW_H_

#ifndef CORE_GRAPHICS_GLX
#  error
#endif

#include "graphics/implementation/opengl.hpp"
#include "graphics/implementation/x11.hpp"
#include "graphics/implementation/window/window_base.hpp"

////////////////////////////////////////////////////////////////////////////////
// graphics glx
////////////////////////////////////////////////////////////////////////////////

namespace graphics { namespace glx {

/*******************************************************************************
** Window
*******************************************************************************/

class Window final
    : public Resource<Window>
    , public window::WindowBase {
public:
  Window() = default;
  Window(Window &&) = delete;
  Window(Window const &) = delete;
  Window & operator=(Window &&) = delete;
  Window & operator=(Window const &) = delete;
  ~Window();

  bool Create(const_string _title);
  bool Create(WindowParameters const & _parameters);
  void Run();
  void Destroy();
  bool IsDestroyed() const;

  void Clear();
  void Present();

private:
  api::opengl::glx::window::Context context;
  bool destroyed = false;

  void UpdateMousePosition();
  void Resized(WindowSize _size);
};

}}

#endif