#ifndef _GRAPHICS_IMPLEMENTATION_GL_CREATE_MESH_HPP_
#define _GRAPHICS_IMPLEMENTATION_GL_CREATE_MESH_HPP_

#include "graphics/base.hpp"

#ifndef CORE_GRAPHICS_OPENGL
#  error
#endif

#include "graphics/implementation/gl/mesh.hpp"

////////////////////////////////////////////////////////////////////////////////
// graphics opengl
////////////////////////////////////////////////////////////////////////////////

namespace graphics { namespace opengl {

namespace detail {

template<typename>
struct PrimitiveOf;

#define DECLARE_PRIMITIVE_OF(_TYPE, _PRIMITIVE_TYPE)                           \
  template<>                                                                   \
  struct PrimitiveOf<_TYPE> {                                                  \
    using type = _PRIMITIVE_TYPE;                                              \
  }

#define DECLARE_PRIMITIVE(_TYPE) DECLARE_PRIMITIVE_OF(_TYPE, _TYPE)

DECLARE_PRIMITIVE(int8);
DECLARE_PRIMITIVE(uint8);
DECLARE_PRIMITIVE(int16);
DECLARE_PRIMITIVE(uint16);
DECLARE_PRIMITIVE(int32);
DECLARE_PRIMITIVE(uint32);
DECLARE_PRIMITIVE(int64);
DECLARE_PRIMITIVE(uint64);
DECLARE_PRIMITIVE(float);
DECLARE_PRIMITIVE(double);

DECLARE_PRIMITIVE_OF(numerics::Vector2f, float);
DECLARE_PRIMITIVE_OF(numerics::Vector3f, float);
DECLARE_PRIMITIVE_OF(numerics::Vector4f, float);
DECLARE_PRIMITIVE_OF(numerics::RowMajorMatrix4x4f, float);
DECLARE_PRIMITIVE_OF(numerics::ColumnMajorMatrix4x4f, float);

#undef DECLARE_PRIMITIVE
#undef DECLARE_PRIMITIVE_OF

template<typename T>
struct primitive_of {
  using type = typename PrimitiveOf<T>::type;
};

template<typename T>
struct primitive_of<T *> {
  using type = typename primitive_of<mpl::remove_const_t<T>>::type;
};

template<typename T, size_t Size>
struct primitive_of<container::InlineArray<T, Size>> {
  using type = typename primitive_of<mpl::remove_const_t<T>>::type;
};

template<typename T>
struct primitive_of<core::span<T const>> {
  using type = typename primitive_of<mpl::remove_const_t<T>>::type;
};

template<typename T>
using primitive_of_t = typename detail::primitive_of<T>::type;

}

/*******************************************************************************
** bind_vertex_attribute
*******************************************************************************/

template<typename ValueType, typename VertexType>
inline mpl::if_integral_t<detail::primitive_of_t<ValueType>>
bind_vertex_attribute(size_t _index, ValueType VertexType::*_member) {
  using PrimitiveType = detail::primitive_of_t<ValueType>;
  CORE_LIB_STATIC_ASSERT(sizeof(ValueType) % sizeof(PrimitiveType) == 0u);
  CORE_LIB_STATIC_ASSERT(sizeof(ValueType) / sizeof(PrimitiveType) <= 4u);
  glEnableVertexAttribArray(_index);
  auto const primitive_count = sizeof(ValueType) / sizeof(PrimitiveType);
  auto const vertex_size = sizeof(VertexType);
  auto const offset =
      reinterpret_cast<void const *>(core::memory::offset_of(_member));
  glVertexAttribIPointer(_index, primitive_count, GlEnum<PrimitiveType>::value,
                         vertex_size, offset);
}

template<typename ValueType, typename VertexType>
inline mpl::if_same_t<detail::primitive_of_t<ValueType>, float>
bind_vertex_attribute(size_t _index, ValueType VertexType::*_member) {
  using PrimitiveType = float;
  CORE_LIB_STATIC_ASSERT(sizeof(ValueType) % sizeof(PrimitiveType) == 0u);
  CORE_LIB_STATIC_ASSERT(sizeof(ValueType) / sizeof(PrimitiveType) <= 4u);
  glEnableVertexAttribArray(_index);
  auto const primitive_count = sizeof(ValueType) / sizeof(PrimitiveType);
  auto const vertex_size = sizeof(VertexType);
  auto const offset =
      reinterpret_cast<void const *>(core::memory::offset_of(_member));
  glVertexAttribPointer(_index, primitive_count, GL_FLOAT, GL_FALSE,
                        vertex_size, offset);
}

/*******************************************************************************
** VertexAttributeBinder
*******************************************************************************/

template<typename VertexType>
struct VertexAttributeBinder {
  size_t index = 0u;

  template<typename ValueType>
  VertexAttributeBinder<VertexType> Bind(ValueType VertexType::*_member) {
    bind_vertex_attribute(index++, _member);
    return *this;
  }
};

/*******************************************************************************
** set_vertex_data
*******************************************************************************/

template<typename VertexType>
inline void set_vertex_data(ArrayVertexBuffer & _vertex_buffer,
                            ArrayVertexDataView<VertexType> const & _view) {
  _vertex_buffer.Bind();
  bind_attributes(mpl::type<VertexType>(), VertexAttributeBinder<VertexType>());
  glBufferData(GL_ARRAY_BUFFER, _view.vertices.size() * sizeof(VertexType),
               _view.vertices.data(), GL_STATIC_DRAW);
  _vertex_buffer.element_count = static_cast<uint32>(_view.vertices.size());
  _vertex_buffer.Unbind();
}

template<typename VertexType>
inline void set_vertex_data(IndexedVertexBuffer & _vertex_buffer,
                            IndexedVertexDataView<VertexType> const & _view) {
  _vertex_buffer.Bind();
  bind_attributes(mpl::type<VertexType>(), VertexAttributeBinder<VertexType>());
  glBufferData(GL_ARRAY_BUFFER, _view.vertices.size() * sizeof(VertexType),
               _view.vertices.data(), GL_STATIC_DRAW);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, _view.indices.size() * sizeof(uint32),
               _view.indices.data(), GL_STATIC_DRAW);
#if defined(CORE_LIB_DEBUG)
  for (auto index = 0u; index < _view.indices.size(); ++index) {
    CORE_LIB_ASSERT(_view.indices[index] <= _view.vertices.size());
  }
#endif
  _vertex_buffer.element_count = static_cast<uint32>(_view.indices.size());
  _vertex_buffer.Unbind();
}

template<typename VertexType>
inline void set_vertex_data(ArrayVertexBuffer & _vertex_buffer,
                            ArrayVertexData<VertexType> const & _data) {
  set_vertex_data(_vertex_buffer, ArrayVertexDataView<VertexType>(_data));
}

template<typename VertexType>
inline void set_vertex_data(IndexedVertexBuffer & _vertex_buffer,
                            IndexedVertexData<VertexType> const & _data) {
  set_vertex_data(_vertex_buffer, IndexedVertexDataView<VertexType>(_data));
}

/*******************************************************************************
** create_mesh
*******************************************************************************/

template<typename VertexType>
inline ArrayMesh<VertexType>
create_mesh(ArrayVertexDataView<VertexType> const & _view) {
  ArrayMesh<VertexType> mesh;
  mesh.vertex_buffer.Allocate();
  set_vertex_data(mesh.vertex_buffer, _view);
  return mesh;
}

template<typename VertexType>
inline IndexedMesh<VertexType>
create_mesh(IndexedVertexDataView<VertexType> const & _view) {
  IndexedMesh<VertexType> mesh;
  mesh.vertex_buffer.Allocate();
  set_vertex_data(mesh.vertex_buffer, _view);
  return mesh;
}

template<typename VertexType>
inline ArrayMesh<VertexType>
create_mesh(ArrayVertexData<VertexType> const & _data) {
  return create_mesh(ArrayVertexDataView<VertexType>(_data));
}

template<typename VertexType>
inline IndexedMesh<VertexType>
create_mesh(IndexedVertexData<VertexType> const & _data) {
  return create_mesh(IndexedVertexDataView<VertexType>(_data));
}

}}

#endif
