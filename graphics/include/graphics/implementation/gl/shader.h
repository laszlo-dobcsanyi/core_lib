#ifndef _GRAPHICS_IMPLEMENTATION_SHADER_H_
#define _GRAPHICS_IMPLEMENTATION_SHADER_H_

#include "graphics/base.hpp"

#if !defined(CORE_GRAPHICS_OPENGL)
#  error
#endif

#include "graphics/implementation/opengl.hpp"

////////////////////////////////////////////////////////////////////////////////
// graphics opengl
////////////////////////////////////////////////////////////////////////////////

namespace graphics { namespace opengl {

/*******************************************************************************
** ShaderHandle
*******************************************************************************/

class ShaderHandle : public ShaderObjectType {
public:
  ShaderHandle() = default;
  ShaderHandle(typename ShaderObjectType::ValueType _value);
  ShaderHandle(ShaderHandle &&) = default;
  ShaderHandle(ShaderHandle const &) = delete;
  ShaderHandle & operator=(ShaderHandle &&) = default;
  ShaderHandle & operator=(ShaderHandle const &) = delete;
  ~ShaderHandle();

  void Destroy();
};

/*******************************************************************************
** ShaderProgram
*******************************************************************************/

class ShaderProgram : public ShaderObjectType {
public:
  ShaderProgram() = default;
  ShaderProgram(typename ShaderObjectType::ValueType _value);
  ShaderProgram(ShaderProgram &&) = default;
  ShaderProgram(ShaderProgram const &) = delete;
  ShaderProgram & operator=(ShaderProgram &&) = default;
  ShaderProgram & operator=(ShaderProgram const &) = delete;
  ~ShaderProgram();

  void Use() const;
  void Destroy();
};

/*******************************************************************************
** Shader
*******************************************************************************/

class Shader
    : public Resource<Shader>
    , public ShaderProgram {
public:
  Shader() = default;
  Shader(ShaderProgram && _base);
  Shader(Shader &&) = default;
  Shader(Shader const &) = delete;
  Shader & operator=(Shader &&) = default;
  Shader & operator=(Shader const &) = delete;
  ~Shader() = default;

  bool Create(const_string const & _vertex_shader,
              const_string const & _fragment_shader);

  UniformLocationType GetUniformLocation(const char * _name) const;
};

}}

#endif
