#ifndef _GRAPHICS_IMPLEMENTATION_GL_SAMPLER_H_
#define _GRAPHICS_IMPLEMENTATION_GL_SAMPLER_H_

#include "graphics/base.hpp"

#if !defined(CORE_GRAPHICS_OPENGL)
#  error
#endif

#include "graphics/implementation/opengl.hpp"

////////////////////////////////////////////////////////////////////////////////
// graphics opengl
////////////////////////////////////////////////////////////////////////////////

namespace graphics { namespace opengl {

/*******************************************************************************
** Sampler
*******************************************************************************/

class Sampler : public Resource<Sampler> {
public:
  SamplerObjectType object;

  Sampler() = default;
  Sampler(Sampler &&) = default;
  Sampler(Sampler const &) = delete;
  Sampler & operator=(Sampler &&) = default;
  Sampler & operator=(Sampler const &) = delete;
  ~Sampler();

  bool IsCreated() const;
  explicit operator bool() const;

  void Set(TextureWrap _wrap, TextureWrapMode _mode);
  void Set(TextureFilter _filter, TextureFilterMode _mode);

  void Destroy();
};

/*******************************************************************************
** create
*******************************************************************************/

bool create(Sampler & _sampler);

/*******************************************************************************
** create_sampler
*******************************************************************************/

core::Optional<Sampler> create_sampler();

}}

#endif
