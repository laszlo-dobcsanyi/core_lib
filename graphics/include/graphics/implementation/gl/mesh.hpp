#ifndef _GRAPHICS_IMPLEMENTATION_GL_MESH_HPP_
#define _GRAPHICS_IMPLEMENTATION_GL_MESH_HPP_

#include "graphics/base.hpp"

#ifndef CORE_GRAPHICS_OPENGL
#  error
#endif

#include "graphics/implementation/gl/vertex_buffers.hpp"

////////////////////////////////////////////////////////////////////////////////
// graphics opengl
////////////////////////////////////////////////////////////////////////////////

namespace graphics { namespace opengl {

/*******************************************************************************
** Mesh
*******************************************************************************/

template<typename Vertex, VertexDataType DataType>
class Mesh : public Resource<Mesh<Vertex, DataType>> {
public:
  VertexBuffer<DataType> vertex_buffer;

  Mesh() = default;
  Mesh(Mesh &&) = default;
  Mesh(Mesh const &) = delete;
  Mesh & operator=(Mesh &&) = default;
  Mesh & operator=(Mesh const &) = delete;
  ~Mesh() = default;

  bool IsAllocated() const { return vertex_buffer.IsAllocated(); }

  void Allocate() { vertex_buffer.Allocate(); }
  void Deallocate() { vertex_buffer.Deallocate(); }
};

}}

#endif
