#ifndef _CORE_GRAPHICS_IMPLEMENTATION_GL_DRAW_HPP_
#define _CORE_GRAPHICS_IMPLEMENTATION_GL_DRAW_HPP_

#include "graphics/base.hpp"

#ifndef CORE_GRAPHICS_OPENGL
#  error
#endif

#include "graphics/implementation/gl/vertex_buffers.hpp"

////////////////////////////////////////////////////////////////////////////////
// graphics opengl
////////////////////////////////////////////////////////////////////////////////

namespace graphics { namespace opengl {

/*******************************************************************************
** draw
*******************************************************************************/

inline void draw(ArrayVertexBuffer const & _vertex_buffer, DrawMode _mode) {
  _vertex_buffer.Bind();
  glDrawArrays(GlDrawMode(_mode), 0, _vertex_buffer.element_count);
  _vertex_buffer.Unbind();
}

inline void draw(IndexedVertexBuffer const & _vertex_buffer, DrawMode _mode) {
  _vertex_buffer.Bind();
  glDrawElements(GlDrawMode(_mode), _vertex_buffer.element_count,
                 GlEnum<uint32>::value, nullptr);
  _vertex_buffer.Unbind();
}

template<typename MeshType>
inline void draw(MeshType const & _mesh, DrawMode _mode) {
  draw(_mesh.vertex_buffer, _mode);
}

}}

#endif
