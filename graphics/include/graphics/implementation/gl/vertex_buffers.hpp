#ifndef _GRAPHICS_IMPLEMENTATION_GL_VERTEX_BUFFERS_HPP_
#define _GRAPHICS_IMPLEMENTATION_GL_VERTEX_BUFFERS_HPP_

#include "graphics/base.hpp"

#ifndef CORE_GRAPHICS_OPENGL
#  error
#endif

#include "graphics/implementation/opengl.hpp"

// TODO: Cache bound objects!

////////////////////////////////////////////////////////////////////////////////
// graphics opengl
////////////////////////////////////////////////////////////////////////////////

namespace graphics { namespace opengl {

/*******************************************************************************
** VertexBuffer
*******************************************************************************/

template<VertexDataType>
class VertexBuffer;

/*******************************************************************************
** ArrayVertexBuffer
*******************************************************************************/

template<>
class VertexBuffer<VertexDataType::Array>
    : public Resource<VertexBuffer<VertexDataType::Array>> {
public:
  ContainerObjectType array_object;
  BufferObjectType buffer_object;
  uint32 element_count;

  VertexBuffer() = default;
  VertexBuffer(VertexBuffer &&) = default;
  VertexBuffer(VertexBuffer const &) = delete;
  VertexBuffer & operator=(VertexBuffer &&);
  VertexBuffer & operator=(VertexBuffer const &) = delete;
  ~VertexBuffer();

  bool IsAllocated() const;

  void Allocate();
  void Deallocate();

  void Bind() const;
  void Unbind() const;
};

using ArrayVertexBuffer = VertexBuffer<VertexDataType::Array>;

/*******************************************************************************
** IndexedVertexBuffer
*******************************************************************************/

template<>
class VertexBuffer<VertexDataType::Indexed>
    : public Resource<VertexBuffer<VertexDataType::Indexed>>
    , public ArrayVertexBuffer {
public:
  BufferObjectType element_buffer_object;

  VertexBuffer() = default;
  VertexBuffer(VertexBuffer &&) = default;
  VertexBuffer(VertexBuffer const &) = delete;
  VertexBuffer & operator=(VertexBuffer &&);
  VertexBuffer & operator=(VertexBuffer const &) = delete;
  ~VertexBuffer();

  bool IsAllocated() const;

  void Allocate();
  void Deallocate();

  void Bind() const;
  void Unbind() const;

private:
  ArrayVertexBuffer & AsBase();
  ArrayVertexBuffer const & AsBase() const;
};

using IndexedVertexBuffer = VertexBuffer<VertexDataType::Indexed>;

}}

////////////////////////////////////////////////////////////////////////////////
// graphics opengl
////////////////////////////////////////////////////////////////////////////////

namespace graphics { namespace opengl {

/*******************************************************************************
** ArrayVertexBuffer
*******************************************************************************/

inline ArrayVertexBuffer &
ArrayVertexBuffer::operator=(ArrayVertexBuffer && _other) {
  if (this != &_other) {
    if (IsAllocated()) {
      Deallocate();
    }
    array_object = move(_other.array_object);
    buffer_object = move(_other.buffer_object);
    element_count = _other.element_count;
  }
  return *this;
}

inline ArrayVertexBuffer::~VertexBuffer() { Deallocate(); }

inline bool ArrayVertexBuffer::IsAllocated() const {
  return array_object.HasValue();
}

inline void ArrayVertexBuffer::Allocate() {
  CORE_LIB_ASSERT(!IsAllocated());
  glGenVertexArrays(1, &array_object);
  glGenBuffers(1, &buffer_object);
  CORE_LIB_ASSERT(IsAllocated());
}

inline void ArrayVertexBuffer::Deallocate() {
  if (IsAllocated()) {
    glDeleteVertexArrays(1, &array_object);
    glDeleteBuffers(1, &buffer_object);
    array_object.Reset();
  }
  CORE_LIB_ASSERT(!IsAllocated());
}

inline void ArrayVertexBuffer::Bind() const {
  CORE_LIB_ASSERT(IsAllocated());
  glBindVertexArray(array_object);
  glBindBuffer(GL_ARRAY_BUFFER, buffer_object);
}

inline void ArrayVertexBuffer::Unbind() const {
  glBindVertexArray(detail::InvalidObject);
  glBindBuffer(GL_ARRAY_BUFFER, detail::InvalidObject);
}

/*******************************************************************************
** IndexedVertexBuffer
*******************************************************************************/

inline IndexedVertexBuffer &
IndexedVertexBuffer::operator=(IndexedVertexBuffer && _other) {
  if (this != &_other) {
    if (IsAllocated()) {
      Deallocate();
    }
    array_object = move(_other.array_object);
    buffer_object = move(_other.buffer_object);
    element_buffer_object = move(_other.element_buffer_object);
    element_count = _other.element_count;
  }
  return *this;
}

inline IndexedVertexBuffer::~VertexBuffer() { Deallocate(); }

inline bool IndexedVertexBuffer::IsAllocated() const {
  return AsBase().IsAllocated();
}

inline void IndexedVertexBuffer::Allocate() {
  AsBase().Allocate();
  glGenBuffers(1, &element_buffer_object);
}

inline void IndexedVertexBuffer::Deallocate() {
  if (IsAllocated()) {
    glDeleteBuffers(1, &element_buffer_object);
  }
  AsBase().Deallocate();
}

inline void IndexedVertexBuffer::Bind() const {
  AsBase().Bind();
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, element_buffer_object);
}

inline void IndexedVertexBuffer::Unbind() const {
  AsBase().Unbind();
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, detail::InvalidObject);
}

inline ArrayVertexBuffer & IndexedVertexBuffer::AsBase() {
  return static_cast<ArrayVertexBuffer &>(*this);
}

inline ArrayVertexBuffer const & IndexedVertexBuffer::AsBase() const {
  return static_cast<ArrayVertexBuffer const &>(*this);
}

}}

#endif
