#ifndef _GRAPHICS_IMPLEMENTATION_GL_FRAMEBUFFER_H_
#define _GRAPHICS_IMPLEMENTATION_GL_FRAMEBUFFER_H_

#include "graphics/base.hpp"

#if !defined(CORE_GRAPHICS_OPENGL)
#  error
#endif

#include "graphics/implementation/opengl.hpp"

////////////////////////////////////////////////////////////////////////////////
// graphics opengl
////////////////////////////////////////////////////////////////////////////////

namespace graphics { namespace opengl {

class Texture;
class RenderBuffer;

/*******************************************************************************
** FrameBuffer
*******************************************************************************/

class FrameBuffer : public Resource<FrameBuffer> {
public:
  FramebufferObjectType object;

  FrameBuffer() = default;
  FrameBuffer(FrameBuffer &&) = default;
  FrameBuffer(FrameBuffer const &) = delete;
  FrameBuffer & operator=(FrameBuffer &&) = default;
  FrameBuffer & operator=(FrameBuffer const &) = delete;
  ~FrameBuffer();

  bool Create();
  void Destroy();

  explicit operator bool() const;
  bool IsCreated() const;

  void Bind();
  void Unbind();

  void Attach(uint32 _attachment_index, Texture const & _texture);
  void Attach(RenderBuffer const & _render_buffer);
};

}}

#endif
