#ifndef _GRAPHICS_IMPLEMENTATION_GL_TEXTURE_SAMPLER_H_
#define _GRAPHICS_IMPLEMENTATION_GL_TEXTURE_SAMPLER_H_

#include "graphics/base.hpp"

#if !defined(CORE_GRAPHICS_OPENGL)
#  error
#endif

#include "graphics/implementation/opengl.hpp"

////////////////////////////////////////////////////////////////////////////////
// graphics opengl
////////////////////////////////////////////////////////////////////////////////

namespace graphics { namespace opengl {

class Sampler;
class Texture;
class Cubemap;

/*******************************************************************************
** TextureSampler
*******************************************************************************/

class TextureSampler : public Resource<TextureSampler> {
public:
  TextureUnit texture_unit;
  core::Optional<core::Ref<Sampler>> sampler;

  TextureSampler() = default;
  TextureSampler(TextureSampler &&) = default;
  TextureSampler(TextureSampler const &) = delete;
  TextureSampler & operator=(TextureSampler &&) = default;
  TextureSampler & operator=(TextureSampler const &) = delete;
  ~TextureSampler() = default;

  bool IsBound() const;
  explicit operator bool() const;

  void Bind(TextureUnit _texture_unit);
  void Bind(core::Ref<Sampler> _sampler_ref);

  void Activate(Texture const & _texture) const;
  void Activate(Cubemap const & _cubemap) const;
};

/*******************************************************************************
** create
*******************************************************************************/

bool create(TextureSampler & _texture_sampler);

/*******************************************************************************
** create_texture_sampler
*******************************************************************************/

core::Optional<TextureSampler> create_texture_sampler();

}}

#endif
