#ifndef _GRAPHICS_IMPLEMENTATION_GL_TEXTURE_H_
#define _GRAPHICS_IMPLEMENTATION_GL_TEXTURE_H_

#include "graphics/base.hpp"

#if !defined(CORE_GRAPHICS_OPENGL)
#  error
#endif

#include "graphics/resource/image/image_data.h"
#include "graphics/implementation/opengl.hpp"

////////////////////////////////////////////////////////////////////////////////
// graphics opengl
////////////////////////////////////////////////////////////////////////////////

namespace graphics { namespace opengl {

/*******************************************************************************
** Texture
*******************************************************************************/

class Texture : public Resource<Texture> {
public:
  TextureObjectType object;

  Texture() = default;
  Texture(Texture &&) = default;
  Texture(Texture const &) = delete;
  Texture & operator=(Texture &&) = default;
  Texture & operator=(Texture const &) = delete;
  ~Texture();

  explicit operator bool() const;
  bool IsCreated() const;

  void Destroy();
};

/*******************************************************************************
** create_texture
*******************************************************************************/

Texture create_texture(uint32 _width, uint32 _height, ColorFormat _format,
                       core::span<byte const> _data,
                       TextureSettings const & _settings);

Texture create_texture(resource::ImageData const & _image_data,
                       TextureSettings const & _settings);

Texture create_texture(resource::ImageData const & _image_data);

}}

#endif
