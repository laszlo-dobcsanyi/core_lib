#ifndef _GRAPHICS_IMPLEMENTATION_GL_CUBEMAP_H_
#define _GRAPHICS_IMPLEMENTATION_GL_CUBEMAP_H_

#include "graphics/base.hpp"

#if !defined(CORE_GRAPHICS_OPENGL)
#  error
#endif

#include "graphics/resource/image/image_data.h"
#include "graphics/implementation/opengl.hpp"

////////////////////////////////////////////////////////////////////////////////
// graphics opengl
////////////////////////////////////////////////////////////////////////////////

namespace graphics { namespace opengl {

/*******************************************************************************
** Cubemap
*******************************************************************************/

class Cubemap : public Resource<Cubemap> {
public:
  TextureObjectType object;

  Cubemap() = default;
  Cubemap(Cubemap &&) = default;
  Cubemap(Cubemap const &) = delete;
  Cubemap & operator=(Cubemap &&) = default;
  Cubemap & operator=(Cubemap const &) = delete;
  ~Cubemap();

  bool IsCreated() const;
  explicit operator bool() const;

  void Destroy();
};

/*******************************************************************************
** create_cubemap
*******************************************************************************/

core::Optional<Cubemap>
create_cubemap(core::span<resource::ImageData> _image_data);

}}

#endif