#ifndef _GRAPHICS_IMPLEMENTATION_GL_RENDERBUFFER_H_
#define _GRAPHICS_IMPLEMENTATION_GL_RENDERBUFFER_H_

#include "graphics/base.hpp"

#if !defined(CORE_GRAPHICS_OPENGL)
#  error
#endif

#include "graphics/implementation/opengl.hpp"

////////////////////////////////////////////////////////////////////////////////
// graphics opengl
////////////////////////////////////////////////////////////////////////////////

namespace graphics { namespace opengl {

/*******************************************************************************
** RenderBuffer
*******************************************************************************/

class RenderBuffer : public Resource<RenderBuffer> {
public:
  RenderbufferObjectType object;

  RenderBuffer() = default;
  RenderBuffer(RenderBuffer &&) = default;
  RenderBuffer(RenderBuffer const &) = delete;
  RenderBuffer & operator=(RenderBuffer &&) = default;
  RenderBuffer & operator=(RenderBuffer const &) = delete;
  ~RenderBuffer();

  bool Create(RenderbufferFormat _format, uint32 _width, uint32 _height);
  void Destroy();

  explicit operator bool() const;
  bool IsCreated() const;

  void Bind();
  void Unbind();
};

}}

#endif
