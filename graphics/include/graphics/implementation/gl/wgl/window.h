#ifndef _GRAPHICS_IMPLEMENTATION_GL_WGL_WINDOW_H_
#define _GRAPHICS_IMPLEMENTATION_GL_WGL_WINDOW_H_

#if !defined(CORE_GRAPHICS_WGL)
#  error
#endif

#include "graphics/implementation/opengl.hpp"
#include "graphics/implementation/winapi.hpp"
#include "graphics/implementation/window/window_base.hpp"

////////////////////////////////////////////////////////////////////////////////
// graphics wgl
////////////////////////////////////////////////////////////////////////////////

namespace graphics { namespace wgl {

/*******************************************************************************
** Window
*******************************************************************************/

class Window final
    : public Resource<Window>
    , public window::WindowBase {
public:
  Window();
  Window(Window &&) = delete;
  Window(Window const &) = delete;
  Window & operator=(Window &&) = delete;
  Window & operator=(Window const &) = delete;
  ~Window();

  bool Create(const_string _title);
  bool Create(WindowParameters const & _parameters);
  void Run();
  void Destroy();
  bool IsDestroyed() const;

  void Clear();
  void Present();

private:
  HWND window = nullptr;
  HDC device_context = nullptr;
  using WindowDelegate = api::opengl::wgl::window::WindowDelegate;
  WindowDelegate window_delegate;
  int32 mouse_wheel_delta = 0;
  bool destroyed = false;

  LRESULT Process(HWND _hwnd, UINT _umsg, WPARAM _wparam, LPARAM _lparam);
  void UpdateMousePosition();

  void Resized(WindowSize _size);
};

}}

#endif
