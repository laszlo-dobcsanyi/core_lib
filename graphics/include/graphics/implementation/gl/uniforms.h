#ifndef _GRAPHICS_IMPLEMENTATION_GL_UNIFORMS_H_
#define _GRAPHICS_IMPLEMENTATION_GL_UNIFORMS_H_

#include "graphics/base.hpp"

#if !defined(CORE_GRAPHICS_OPENGL)
#  error
#endif

#include "graphics/implementation/opengl.hpp"

#include "graphics/implementation/gl/texture_sampler.h"

////////////////////////////////////////////////////////////////////////////////
// graphics
////////////////////////////////////////////////////////////////////////////////

namespace graphics {

template<typename Uniforms>
struct RendererBase;

}

////////////////////////////////////////////////////////////////////////////////
// graphics opengl
////////////////////////////////////////////////////////////////////////////////

namespace graphics { namespace opengl {

/*******************************************************************************
** UniformLocationsTypeBuilder
*******************************************************************************/

template<typename Uniforms, size_t Size = 0u>
struct UniformLocationsTypeBuilder {
  using type = container::InlineArray<UniformLocationType, Size>;

  template<typename T>
  UniformLocationsTypeBuilder<Uniforms, Size + 1u>
  Member(UniformId, T Uniforms::*_pointer) {
    return UniformLocationsTypeBuilder<Uniforms, Size + 1u>();
  }
};

/*******************************************************************************
** generate_uniform_locations_type
*******************************************************************************/

struct generate_uniform_locations_type_t {
  template<typename Uniforms>
  auto operator()(mpl::type<Uniforms>) const {
    UniformLocationsTypeBuilder<Uniforms> builder;
    return reflect_uniforms(mpl::type<Uniforms>(), builder);
  }
};
constexpr generate_uniform_locations_type_t generate_uniform_locations_type;

/*******************************************************************************
** UniformLocations
*******************************************************************************/

template<typename Uniforms>
using UniformLocations = typename decltype(
    generate_uniform_locations_type(mpl::type<Uniforms>()))::type;

/*******************************************************************************
** UniformLocationsBuilder
*******************************************************************************/

template<typename Uniforms, size_t Index = 0u>
struct UniformLocationsBuilder {
  UniformLocationsBuilder(RendererBase<Uniforms> & _renderer)
      : renderer(_renderer) {}

  template<typename T>
  UniformLocationsBuilder<Uniforms, Index + 1u> Member(UniformId _id,
                                                       T Uniforms::*_pointer) {
    UniformLocationType uniform_location =
        renderer.shader.GetUniformLocation(UniformIdName(_id));
    CORE_LIB_ASSERT(uniform_location.HasValue());
    renderer.uniform_locations[Index] = uniform_location;
    return UniformLocationsBuilder<Uniforms, Index + 1u>(renderer);
  }

  RendererBase<Uniforms> & renderer;
};

/*******************************************************************************
** generate_uniform_locations
*******************************************************************************/

struct generate_uniform_locations_t {
  template<typename Uniforms>
  auto operator()(RendererBase<Uniforms> & _renderer) const {
    UniformLocationsBuilder<Uniforms> builder(_renderer);
    return reflect_uniforms(mpl::type<Uniforms>(), builder);
  }
};
constexpr generate_uniform_locations_t generate_uniform_locations;

/*******************************************************************************
** set_uniform
*******************************************************************************/

inline void set_uniform(UniformLocationType _location, float _value) {
  glUniform1f(_location, _value);
}

inline void set_uniform(UniformLocationType _location,
                        numerics::Vector2f const & _vector) {
  glUniform2f(_location, _vector[0u], _vector[1u]);
}

inline void set_uniform(UniformLocationType _location,
                        numerics::Vector3f const & _vector) {
  glUniform3f(_location, _vector[0u], _vector[1u], _vector[2u]);
}

inline void set_uniform(UniformLocationType _location,
                        numerics::Vector4f const & _vector) {
  glUniform4f(_location, _vector[0u], _vector[1u], _vector[2u], _vector[3u]);
}

inline void set_uniform(UniformLocationType _location, int _value) {
  glUniform1i(_location, _value);
}

inline void set_uniform(UniformLocationType _location,
                        numerics::Vector2i32 const & _vector) {
  glUniform2i(_location, _vector[0u], _vector[1u]);
}

inline void set_uniform(UniformLocationType _location,
                        numerics::Vector3i32 const & _vector) {
  glUniform3i(_location, _vector[0u], _vector[1u], _vector[2u]);
}

inline void set_uniform(UniformLocationType _location,
                        numerics::Vector4i32 const & _vector) {
  glUniform4i(_location, _vector[0u], _vector[1u], _vector[2u], _vector[3u]);
}

inline void set_uniform(UniformLocationType _location,
                        core::span<float> const & _values) {
  glUniform1fv(_location, static_cast<GLsizei>(_values.size()), _values.data());
}

inline void set_uniform(UniformLocationType _location,
                        core::span<numerics::Vector2f> const & _values) {
  CORE_LIB_STATIC_ASSERT(alignof(numerics::Vector2f) <= sizeof(float[2u]));
  auto array = reinterpret_cast<float const *>(_values.data());
  glUniform2fv(_location, static_cast<GLsizei>(_values.size()), array);
}

inline void set_uniform(UniformLocationType _location,
                        core::span<numerics::Vector3f> const & _values) {
  CORE_LIB_STATIC_ASSERT(alignof(numerics::Vector3f) <= sizeof(float[3u]));
  auto array = reinterpret_cast<float const *>(_values.data());
  glUniform3fv(_location, static_cast<GLsizei>(_values.size()), array);
}

inline void set_uniform(UniformLocationType _location,
                        core::span<numerics::Vector4f> const & _values) {
  CORE_LIB_STATIC_ASSERT(alignof(numerics::Vector4f) <= sizeof(float[4u]));
  auto array = reinterpret_cast<float const *>(_values.data());
  glUniform4fv(_location, static_cast<GLsizei>(_values.size()), array);
}

inline void set_uniform(UniformLocationType _location,
                        core::span<int32> const & _values) {
  glUniform1iv(_location, static_cast<GLsizei>(_values.size()), _values.data());
}

inline void set_uniform(UniformLocationType _location,
                        core::span<numerics::Vector2i32> const & _values) {
  CORE_LIB_STATIC_ASSERT(alignof(numerics::Vector2i32) <= sizeof(int[2u]));
  auto array = reinterpret_cast<int const *>(_values.data());
  glUniform2iv(_location, static_cast<GLsizei>(_values.size()), array);
}

inline void set_uniform(UniformLocationType _location,
                        core::span<numerics::Vector3i32> const & _values) {
  CORE_LIB_STATIC_ASSERT(alignof(numerics::Vector3i32) <= sizeof(int[3u]));
  auto array = reinterpret_cast<int const *>(_values.data());
  glUniform3iv(_location, static_cast<GLsizei>(_values.size()), array);
}

inline void set_uniform(UniformLocationType _location,
                        core::span<numerics::Vector4i32> const & _values) {
  CORE_LIB_STATIC_ASSERT(alignof(numerics::Vector4i32) <= sizeof(int[4u]));
  auto array = reinterpret_cast<int const *>(_values.data());
  glUniform4iv(_location, static_cast<GLsizei>(_values.size()), array);
}

inline void set_uniform(UniformLocationType _location,
                        numerics::ColumnMajorMatrix4x4<float> const & _matrix) {
  CORE_LIB_STATIC_ASSERT(sizeof(_matrix.storage.components) ==
                         sizeof(float[16u]));
  auto array = reinterpret_cast<float const *>(_matrix.storage.components);
  glUniformMatrix4fv(_location, 1u, GL_FALSE, array);
}

inline void set_uniform(UniformLocationType _location,
                        numerics::RowMajorMatrix4x4<float> const & _matrix) {
  CORE_LIB_STATIC_ASSERT(sizeof(_matrix.storage.components) ==
                         sizeof(float[16u]));
  auto array = reinterpret_cast<float const *>(_matrix.storage.components);
  glUniformMatrix4fv(_location, 1u, GL_TRUE, array);
}

inline void set_uniform(
    UniformLocationType _location,
    core::span<numerics::ColumnMajorMatrix4x4<float> const> const & _matrices) {
  CORE_LIB_STATIC_ASSERT(alignof(numerics::ColumnMajorMatrix4x4<float> const) <=
                         sizeof(float[16u]));
  auto array = reinterpret_cast<float const *>(_matrices.data());
  glUniformMatrix4fv(_location, static_cast<GLsizei>(_matrices.size()),
                     GL_FALSE, array);
}

inline void set_uniform(
    UniformLocationType _location,
    core::span<numerics::RowMajorMatrix4x4<float> const> const & _matrices) {
  CORE_LIB_STATIC_ASSERT(alignof(numerics::RowMajorMatrix4x4<float> const) <=
                         sizeof(float[16u]));
  auto array = reinterpret_cast<float const *>(_matrices.data());
  glUniformMatrix4fv(_location, static_cast<GLsizei>(_matrices.size()), GL_TRUE,
                     array);
}

inline void set_uniform(UniformLocationType _location,
                        TextureSampler const & _texture_sampler) {
  glUniform1i(_location, _texture_sampler.texture_unit.Get());
}

inline void set_uniform(UniformLocationType _location,
                        TextureUnit const & _texture_unit) {
  glUniform1i(_location, _texture_unit.Get());
}
template<typename T>
inline void set_uniform(UniformLocationType _location, T * _ptr) {
  CORE_LIB_ASSERT(_ptr);
  set_uniform(_location, *_ptr);
}

/*******************************************************************************
** UniformSetter
*******************************************************************************/

template<typename Uniforms, size_t Index = 0u>
struct UniformSetter {
  UniformSetter(RendererBase<Uniforms> const & _renderer,
                Uniforms const & _uniforms)
      : renderer(_renderer)
      , uniforms(_uniforms) {}

  template<typename T>
  UniformSetter<Uniforms, Index + 1u> Member(UniformId _id,
                                             T Uniforms::*_pointer) {
    set_uniform(renderer.uniform_locations[Index], uniforms.*_pointer);
    return UniformSetter<Uniforms, Index + 1u>(renderer, uniforms);
  }

  RendererBase<Uniforms> const & renderer;
  Uniforms const & uniforms;
};

/*******************************************************************************
** set_uniforms
*******************************************************************************/

struct set_uniforms_t {
  template<typename Uniforms>
  auto operator()(RendererBase<Uniforms> const & _renderer,
                  Uniforms const & _uniforms) const {
    UniformSetter<Uniforms> setter(_renderer, _uniforms);
    return reflect_uniforms(mpl::type<Uniforms>(), setter);
  }
};
constexpr set_uniforms_t set_uniforms;

}}

#endif