#ifndef _GRAPHICS_IMPLEMENTATION_DX_WINDOW_HPP_
#define _GRAPHICS_IMPLEMENTATION_DX_WINDOW_HPP_

#ifndef GRAPHICS_WINDOW
#  error
#endif

#ifndef CORE_GRAPHICS_WINDX
#  error
#endif

#include "graphics/implementation/directx.hpp"

#define LOG_WINDX_WINDOW(__severity__, ...)                                    \
  LOG_GRAPHICS(__severity__, __VA_ARGS__)

////////////////////////////////////////////////////////////////////////////////
// graphics windx
////////////////////////////////////////////////////////////////////////////////

namespace graphics {
namespace windx {

/*******************************************************************************
** Window
*******************************************************************************/

class Window final : public WindowBase {
public:
  Window() = default;
  Window(Window &&) = delete;
  Window(Window const &) = delete;
  Window & operator=(Window &&) = delete;
  Window & operator=(Window const &) = delete;
  ~Window();

  bool Create(const_string _title);
  void Run();
  void Destroy();
  bool IsDestroyed() const;

  void Clear();
  void Present();

private:
};

/*******************************************************************************
** Window
*******************************************************************************/

inline Window::~Window() { Destroy(); }

inline bool Window::Create(const_string _title) {
  // TODO
  CORE_LIB_UNIMPLEMENTED;
  return false;
}

inline void Window::Run() {
  // TODO
  CORE_LIB_UNIMPLEMENTED;
}

inline void Window::Destroy() {
  // TODO
  CORE_LIB_UNIMPLEMENTED;
}

inline bool Window::IsDestroyed() const {
  // TODO
  CORE_LIB_UNIMPLEMENTED;
  return true;
}

inline void Window::Clear() {
  // TODO
  CORE_LIB_UNIMPLEMENTED;
}

inline void Window::Present() {
  // TODO
  CORE_LIB_UNIMPLEMENTED;
}

#endif