#ifndef _GRAPHICS_IMPLEMENTATION_DX_MESH_HPP_
#define _GRAPHICS_IMPLEMENTATION_DX_MESH_HPP_

#include "graphics/base.hpp"

#ifndef CORE_GRAPHICS_DIRECTX
#  error
#endif

////////////////////////////////////////////////////////////////////////////////
// graphics directx
////////////////////////////////////////////////////////////////////////////////

namespace graphics { namespace directx {

/*******************************************************************************
** Mesh
*******************************************************************************/

template<typename Vertex, VertexDataType DataType>
class Mesh {
public:
  using VertexType = Vertex;
  using VertexDataType = VertexData<Vertex, DataType>;

  Mesh() = default;
  Mesh(Mesh &&) = default;
  Mesh(Mesh const &) = delete;
  Mesh & operator=(Mesh &&) = default;
  Mesh & operator=(Mesh const &) = delete;
  ~Mesh() = default;

  bool IsAllocated() const;

  void Allocate();
  void Deallocate();
};

}}

#endif
