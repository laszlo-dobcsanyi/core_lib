#ifndef _GRAPHICS_IMPLEMENTATION_DX_IMAGE_HPP_
#define _GRAPHICS_IMPLEMENTATION_DX_IMAGE_HPP_

#include "graphics/base.hpp"

#if defined(CORE_GRAPHICS_DIRECTX)

#  include "graphics/resource/image_base.hpp"
#  include "graphics/provider/directx.h"

////////////////////////////////////////////////////////////////////////////////
// graphics directx
////////////////////////////////////////////////////////////////////////////////

namespace graphics { namespace directx {

/*******************************************************************************
** Image
*******************************************************************************/

class Image final : public ImageBase {
public:
  Image() = default;
  Image(Image &&) = default;
  Image(Image const &) = delete;
  Image & operator=(Image &&) = default;
  Image & operator=(Image const &) = delete;
  ~Image() = default;
};

}}

#endif

#endif