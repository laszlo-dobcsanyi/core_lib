#ifndef _GRAPHICS_IMPLEMENTATION_DX_SHADER_HPP_
#define _GRAPHICS_IMPLEMENTATION_DX_SHADER_HPP_

#include "graphics/base.hpp"

#if defined(CORE_GRAPHICS_DIRECTX)

////////////////////////////////////////////////////////////////////////////////
// graphics directx
////////////////////////////////////////////////////////////////////////////////

namespace graphics { namespace directx {

class Shader {
public:
  Shader() = default;
  Shader(Shader &&) = default;
  Shader(Shader const &) = default;
  Shader & operator=(Shader &&) = default;
  Shader & operator=(Shader const &) = default;
  ~Shader() = default;
};

}}

#endif

#endif