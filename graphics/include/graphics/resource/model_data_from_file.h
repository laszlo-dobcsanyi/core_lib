#ifndef _GRAPHICS_RESOURCE_MODEL_DATA_FROM_FILE_H_
#define _GRAPHICS_RESOURCE_MODEL_DATA_FROM_FILE_H_

#include "core/fs/filepath.h"
#include "core/optional.hpp"
#include "graphics/resource/model_data.hpp"

////////////////////////////////////////////////////////////////////////////////
// graphics resource
////////////////////////////////////////////////////////////////////////////////

namespace graphics { namespace resource {

/*******************************************************************************
** model_data_from_file
*******************************************************************************/

core::Optional<ModelData>
model_data_from_file(core::fs::FilePath const & _file_path);

}}

#endif
