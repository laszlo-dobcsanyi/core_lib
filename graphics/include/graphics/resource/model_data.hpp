#ifndef _GRAPHICS_RESOURCE_MODEL_DATA_HPP_
#define _GRAPHICS_RESOURCE_MODEL_DATA_HPP_

#include "core/memory/unique_array.hpp"
#include "core/numerics.hpp"
#include "core/string.hpp"
#include "graphics/base.hpp"

////////////////////////////////////////////////////////////////////////////////
// graphics resource
////////////////////////////////////////////////////////////////////////////////

namespace graphics { namespace resource {

/*******************************************************************************
** StaticMeshData
*******************************************************************************/

struct StaticMeshData {
  core::UniqueArray<numerics::Vector3f> positions;
  core::UniqueArray<numerics::Vector3f> normals;
  core::UniqueArray<numerics::Vector2f> texture_coordinates;
  core::UniqueArray<uint32> indices;
};

/*******************************************************************************
** SkinnedMeshData
*******************************************************************************/

struct SkinnedMeshData {
  core::UniqueArray<numerics::Vector3f> positions;
  core::UniqueArray<numerics::Vector3f> normals;
  core::UniqueArray<numerics::Vector2f> texture_coordinates;
  core::UniqueArray<uint32> bone_indices;
  core::UniqueArray<numerics::Vector4f> bone_weights;
  core::UniqueArray<uint32> indices;
};

/*******************************************************************************
** Bone
*******************************************************************************/

struct Bone {
  const_string name;
  numerics::ColumnMajorMatrix4x4<float> transformation;
  numerics::ColumnMajorMatrix4x4<float> offset;
  numerics::Vector3f translation;
  numerics::Vector3f rotation;
  numerics::Vector3f scale;
  uint32 next = 0u;
  uint32 child = 0u;
};

/*******************************************************************************
** Skeleton
*******************************************************************************/

struct SkeletonData : public Resource<SkeletonData> {
  core::UniqueArray<Bone> bones;
};

/*******************************************************************************
** AnimationValue
*******************************************************************************/

struct AnimationValue {
  float time;
  numerics::Vector3f value;
};

/*******************************************************************************
** AnimationChannel
*******************************************************************************/

struct AnimationChannel {
  core::UniqueArray<AnimationValue> translation;
  core::UniqueArray<AnimationValue> rotation;
  core::UniqueArray<AnimationValue> scale;
};

/*******************************************************************************
** Animation
*******************************************************************************/

struct AnimationData : public Resource<AnimationData> {
  const_string name;
  core::UniqueArray<AnimationChannel> channels;
};

/*******************************************************************************
** StaticModelData
*******************************************************************************/

struct StaticModelData : public Resource<StaticModelData> {
  StaticMeshData mesh_data;
};

/*******************************************************************************
** AnimatedModelData
*******************************************************************************/

struct AnimatedModelData : public Resource<AnimatedModelData> {
  SkinnedMeshData mesh_data;
  SkeletonData skeleton_data;
  core::UniqueArray<AnimationData> animation_data;
};

/*******************************************************************************
** ModelData
*******************************************************************************/

using ModelData = core::Variant<StaticModelData, AnimatedModelData>;

}}

#endif
