#ifndef _GRAPHICS_RESOURCE_OBJ_HPP_
#define _GRAPHICS_RESOURCE_OBJ_HPP_

#include "graphics/base.hpp"
#include "core/queries.hpp"
#include "core/fs/mapped_file.hpp"
#include "graphics/resource/model_data.hpp"

#define LOG_OBJPARSER(__severity__, ...) LOG_GRAPHICS(__severity__, __VA_ARGS__)

////////////////////////////////////////////////////////////////////////////////
// graphics
////////////////////////////////////////////////////////////////////////////////

namespace graphics {

/*******************************************************************************
** VertexAttributeData
*******************************************************************************/

template<typename>
struct VertexAttributeData;

/*******************************************************************************
** PositionVertexAttributeData
*******************************************************************************/

template<>
struct VertexAttributeData<PositionVertex> {
  core::UniqueArray<numerics::Vector3f> positions;
};

/*******************************************************************************
** TexturedVertexAttributeData
*******************************************************************************/

template<>
struct VertexAttributeData<TexturedVertex> {
  core::UniqueArray<numerics::Vector3f> positions;
  core::UniqueArray<numerics::Vector3f> normals;
  core::UniqueArray<numerics::Vector2f> texture_coordinates;
};

/*******************************************************************************
** SkinnedVertexAttributeData
*******************************************************************************/

template<>
struct VertexAttributeData<SkinnedVertex> {
  core::UniqueArray<numerics::Vector3f> positions;
  core::UniqueArray<numerics::Vector3f> normals;
  core::UniqueArray<numerics::Vector2f> texture_coordinates;
  core::UniqueArray<uint32> bone_indices;
  core::UniqueArray<float[4u]> bone_weights;
};

/*******************************************************************************
** IndexedVertexAttributeData
*******************************************************************************/

template<typename VertexType>
struct IndexedVertexAttributeData {
  VertexAttributeData<VertexType> attributes;
  core::UniqueArray<uint32> indices;
};

}

////////////////////////////////////////////////////////////////////////////////
// graphics obj
////////////////////////////////////////////////////////////////////////////////

namespace graphics { namespace obj {

/*******************************************************************************
** Stream
*******************************************************************************/

using Stream = core::ByteStreamReader;

/*******************************************************************************
** Position
*******************************************************************************/

using Position = numerics::Vector3f;

/*******************************************************************************
** Normal
*******************************************************************************/

using Normal = numerics::Vector3f;

/*******************************************************************************
** TextureCoordinate
*******************************************************************************/

using TextureCoordinate = numerics::Vector2f;

/*******************************************************************************
** FaceIndex
*******************************************************************************/

struct FaceIndex {
  uint32 position_index;
  uint32 normal_index;
  uint32 texture_coordinate_index;
};

/*******************************************************************************
** Face
*******************************************************************************/

struct Face {
  FaceIndex indices[3];
};

/*******************************************************************************
** Mesh
*******************************************************************************/

struct Mesh {
  container::UnboundedStack<Position> positions;
  container::UnboundedStack<Normal> normals;
  container::UnboundedStack<TextureCoordinate> texture_coordinates;
  container::UnboundedStack<Face> faces;
};

/*******************************************************************************
** ParserState
*******************************************************************************/

class ParserState {
public:
  char const * token_start;
  char const * line_start;
  size_t position;
  size_t line;
  size_t parsed;
  bool finished;

  inline ParserState(char const * _memory_start)
      : token_start(_memory_start)
      , line_start(token_start)
      , position(1)
      , line(1)
      , parsed(0)
      , finished(false) {}
};

/*******************************************************************************
** OBJParserState
*******************************************************************************/

class OBJParserState : public ParserState {
public:
  Mesh mesh;

  inline OBJParserState(char const * _memory_start)
      : ParserState(_memory_start) {}
};

inline size_t parse_position(char const * _token_start,
                             OBJParserState & _state) {
  LOG_OBJPARSER(MESSAGE, "Parsing vertex position data..\n");
  Position position;
  size_t parsed = 0;
  size_t sub_parsed;
  if ((sub_parsed = parser::parse_float(_token_start, position.x))) {
    parsed += sub_parsed;
    _token_start += sub_parsed;
    if ((sub_parsed = parser::parse_float(_token_start, position.y))) {
      parsed += sub_parsed;
      _token_start += sub_parsed;
      if ((sub_parsed = parser::parse_float(_token_start, position.z))) {
        LOG_OBJPARSER(MESSAGE, "Vertex position: %f, %f, %f\n", position.x,
                      position.y, position.z);
        _state.mesh.positions.Push(position);
        return parsed + sub_parsed;
      }
    }
  }
  _state.mesh.positions.Push();
  return 0;
}

inline size_t parse_normal(char const * _token_start, OBJParserState & _state) {
  LOG_OBJPARSER(MESSAGE, "Parsing vertex normal data..\n");
  Normal normal;
  size_t parsed = 0;
  size_t sub_parsed;
  if ((sub_parsed = parser::parse_float(_token_start, normal.x))) {
    parsed += sub_parsed;
    _token_start += sub_parsed;
    if ((sub_parsed = parser::parse_float(_token_start, normal.y))) {
      parsed += sub_parsed;
      _token_start += sub_parsed;
      if ((sub_parsed = parser::parse_float(_token_start, normal.z))) {
        LOG_OBJPARSER(MESSAGE, "Vertex normal: %f, %f, %f\n", normal.x,
                      normal.y, normal.z);
        _state.mesh.normals.Push(normal);
        return parsed + sub_parsed;
      }
    }
  }
  _state.mesh.normals.Push();
  return 0;
}

inline size_t parse_texture_coordinate(char const * _token_start,
                                       OBJParserState & _state) {
  LOG_OBJPARSER(MESSAGE, "Parsing vertex texture coordinate data..\n");
  TextureCoordinate coordinate;
  size_t parsed = 0;
  size_t sub_parsed;
  if ((sub_parsed = parser::parse_float(_token_start, coordinate.x))) {
    parsed += sub_parsed;
    _token_start += sub_parsed;
    if ((sub_parsed = parser::parse_float(_token_start, coordinate.y))) {
      parsed += sub_parsed;
      _token_start += sub_parsed;
      float w;
      if ((sub_parsed = parser::parse_float(_token_start, w))) {
        LOG_OBJPARSER(WARNING,
                      "Ignoring third texture coordinate component!\n");
      }
      LOG_OBJPARSER(MESSAGE, "Vertex texture coordinate: %f, %f\n",
                    coordinate.x, coordinate.y);
      _state.mesh.texture_coordinates.Push(coordinate);
      return parsed + sub_parsed;
    }
  }
  _state.mesh.texture_coordinates.Push();
  return 0;
}

inline size_t parse_face_index(char const * _token_start,
                               OBJParserState & _state,
                               FaceIndex & _face_index) {
  LOG_OBJPARSER(MESSAGE, "Parsing face index..\n");
  size_t parsed = 0;
  size_t sub_parsed = 0;
  int32 index;

  // Position index
  if ((sub_parsed = parser::parse_decimal(_token_start, index)) == 0) {
    return 0;
  }

  if (index < 0) {
    _face_index.position_index =
        static_cast<uint32>(_state.mesh.positions.Size()) + index;
  } else {
    _face_index.position_index = index;
  }
  parsed += sub_parsed;
  _token_start += sub_parsed;

  if (*_token_start != '/') {
    return parsed;
  }

  parsed += 1;
  _token_start += 1;

  // Normal index
  if (*_token_start != '/') {
    if ((sub_parsed = parser::parse_decimal(_token_start, index)) == 0) {
      return 0;
    }

    if (index < 0) {
      _face_index.normal_index =
          static_cast<uint32>(_state.mesh.normals.Size()) + index;
    } else {
      _face_index.normal_index = index;
    }

    parsed += sub_parsed;
    _token_start += sub_parsed;

    if (*_token_start != '/') {
      if ((sub_parsed = parser::parse_whitespaces(_token_start))) {
        return parsed + sub_parsed;
      } else {
        return 0;
      }
    } else {
      parsed += 1;
      _token_start += 1;
    }
  } else {
    parsed += 1;
    _token_start += 1;
  }

  // Texture coordinate index
  if ((sub_parsed = parser::parse_decimal(_token_start, index)) == 0) {
    return 0;
  }

  if (index < 0) {
    _face_index.texture_coordinate_index =
        static_cast<uint32>(_state.mesh.texture_coordinates.Size()) + index;
  } else {
    _face_index.texture_coordinate_index = index;
  }
  LOG_OBJPARSER(MESSAGE, "Face index:  %" PRIu32 ", %" PRIu32 ", %" PRIu32 "\n",
                _face_index.position_index, _face_index.normal_index,
                _face_index.texture_coordinate_index);
  return parsed + sub_parsed;
}

inline size_t parse_face(char const * _token_start, OBJParserState & _state) {
  size_t parsed = 0;
  size_t sub_parsed = 0;
  Face face;
  for (auto index = 0u; index < 3u; ++index) {
    if ((sub_parsed =
             parse_face_index(_token_start, _state, face.indices[index]))) {
      parsed += sub_parsed;
      _token_start += sub_parsed;
    } else {
      _state.mesh.faces.Push();
      return 0u;
    }
  }
  _state.mesh.faces.Push(face);
  return parsed;
}

inline bool parse_mesh(OBJParserState & _state) {
  static char const * position_token = "v";
  static char const * normal_token = "vn";
  static char const * texture_coordinate_token = "vt";
  static char const * face_token = "f";
  while (!_state.finished) {
    for (;;) {
      if ((_state.parsed = parser::parse_end_of_file(_state.token_start))) {
        LOG_OBJPARSER(MESSAGE, "End of file @ %ld,%ld\n", _state.line,
                      _state.position);
        _state.finished = true;
        break;
      } else if ((_state.parsed = parser::parse_line_end(_state.token_start))) {
        LOG_OBJPARSER(MESSAGE, "End of line %ld @ %ld\n", _state.line,
                      _state.position);
        ++_state.line;
        _state.position = 1;
        _state.token_start += _state.parsed;
        _state.line_start = _state.token_start;
        continue;
      } else if ((_state.parsed = parser::parse_comment(_state.token_start))) {
        const_string comment(_state.token_start, _state.parsed);
        LOG_OBJPARSER(MESSAGE, "Comment: %s\n", cstr(comment));
        break;
      } else if ((_state.parsed =
                      parser::parse_whitespaces(_state.token_start))) {
        LOG_OBJPARSER(MESSAGE, "Whitespaces\n");
        break;
      } else if ((_state.parsed = parser::parse_token(_state.token_start))) {
        const_string token(_state.token_start, _state.parsed);
        LOG_OBJPARSER(MESSAGE, "Token: %s\n", cstr(token));
        if (token == position_token) {
          _state.position += _state.parsed;
          _state.token_start += _state.parsed;
          if ((_state.parsed = parse_position(_state.token_start, _state))) {
            break;
          } else {
            LOG_OBJPARSER(ERROR, "Invalid vertex position data!\n");
            break;
          }
        } else if (token == normal_token) {
          _state.position += _state.parsed;
          _state.token_start += _state.parsed;
          if ((_state.parsed = parse_normal(_state.token_start, _state))) {
            break;
          } else {
            LOG_OBJPARSER(ERROR, "Invalid vertex normal data!\n");
            break;
          }
        } else if (token == texture_coordinate_token) {
          _state.position += _state.parsed;
          _state.token_start += _state.parsed;
          if ((_state.parsed =
                   parse_texture_coordinate(_state.token_start, _state))) {
            break;
          } else {
            LOG_OBJPARSER(ERROR, "Invalid vertex texture coordinate data!\n");
            break;
          }
        } else if (token == face_token) {
          _state.position += _state.parsed;
          _state.token_start += _state.parsed;
          if ((_state.parsed = parse_face(_state.token_start, _state))) {
            break;
          } else {
            LOG_OBJPARSER(ERROR, "Invalid face data!\n");
            break;
          }
        } else {
          LOG_OBJPARSER(ERROR, "Unknown token @ %ld,%ld: %s!\n", _state.line,
                        _state.position, cstr(token));
          _state.parsed = 0;
        }
        break;
      } else {
        LOG_OBJPARSER(ERROR, "Parser error @ %ld,%ld!\n", _state.line,
                      _state.position);
        _state.parsed = 0;
      }
    }
    if (_state.parsed == 0) {
      if ((_state.parsed = parser::parse_line(_state.line_start))) {
        const_string line_data(_state.line_start, _state.parsed);
        LOG_OBJPARSER(WARNING, "Skipping line %zu: %s\n", _state.line,
                      cstr(line_data));
        _state.position = 1;
        _state.token_start = _state.line_start;
      } else {
        return false;
      }
    }
    _state.position += _state.parsed;
    _state.token_start += _state.parsed;
  }
  return true;
}

/*******************************************************************************
** Data
*******************************************************************************/

using Data = IndexedVertexAttributeData<TexturedVertex>;

/*******************************************************************************
** load
*******************************************************************************/

inline core::Optional<Data> load(Stream & _stream) {
  OBJParserState parser_state(
      reinterpret_cast<char const *>(_stream.Bytes().data()));
  if (!parse_mesh(parser_state)) {
    LOG_OBJPARSER(ERROR, "Failed to parse file!\n");
    return core::nullopt;
  }

  Data result;
  // Copy vertex attributes
  CORE_LIB_ASSERT(parser_state.mesh.positions.Size() ==
                      parser_state.mesh.normals.Size() &&
                  parser_state.mesh.positions.Size() ==
                      parser_state.mesh.texture_coordinates.Size());
  result.attributes.positions = container::to_unique_array<numerics::Vector3f>(
      parser_state.mesh.positions);
  result.attributes.normals =
      container::to_unique_array<numerics::Vector3f>(parser_state.mesh.normals);
  result.attributes.texture_coordinates =
      container::to_unique_array<numerics::Vector2f>(
          parser_state.mesh.texture_coordinates);

  // Copy indices
  // const auto index_count = parser_state.mesh.faces.Size() * 3;
  auto & indices = result.indices;
  {
    auto face_query = query::make_query(parser_state.mesh.faces);
    auto current = 0u;
    while (face_query.IsValid()) {
      auto & triangle = face_query.Forward();
      for (auto edge = 0u; edge < 3u; ++edge) {
        auto const index = triangle.indices[edge].position_index;
        CORE_LIB_ASSERT(index == triangle.indices[edge].position_index &&
                        index == triangle.indices[edge].normal_index &&
                        index ==
                            triangle.indices[edge].texture_coordinate_index);
        indices[current++] = index;
      }
    }
  }
  return core::Optional<Data>(move(result));
#if 0
  const auto vertices_count = parser_state.mesh.positions.Size();
  CORE_LIB_ASSERT(  vertices_count == parser_state.mesh.positions.Size()
                 && vertices_count == parser_state.mesh.normals.Size()
                 && vertices_count == parser_state.mesh.texture_coordinates.Size() );
  _mesh_resource->vertices = make_unique< Vertex[] >( vertices_count );
  _mesh_resource->vertex_count = vertices_count;

  auto & vertices = _mesh_resource->vertices;
  {
    auto current = 0u;
    auto position_query = query::make_query( parser_state.mesh.positions );
    auto normal_query = query::make_query( parser_state.mesh.normals );
    auto texture_coordinate_query = query::make_query( parser_state.mesh.texture_coordinates );
    while ( position_query.IsValid() ) {
      CORE_LIB_ASSERT( normal_query.IsValid() && texture_coordinate_query.IsValid() );
      auto & vertex = vertices[ current++ ];
      vertex.position = position_query.Forward();
      vertex.normal = normal_query.Forward();
      auto & texture_coordinate = texture_coordinate_query.Forward();
      vertex.texture_coordinate = numerics::Vector2( texture_coordinate.u, texture_coordinate.v );
    }
    CORE_LIB_ASSERT( !normal_query.IsValid() && !texture_coordinate_query.IsValid() );
  }
  const auto index_count = parser_state.mesh.faces.Size() * 3;
  _mesh_resource->indices = make_unique< uint32[] >( index_count );
  _mesh_resource->index_count = index_count;

  auto & indices = _mesh_resource->indices;
  {
    auto face_query = query::make_query( parser_state.mesh.faces );
    auto current = 0u;
    while ( face_query.IsValid() ) {
      auto & triangle = face_query.Forward();
      for ( auto edge = 0u; edge < 3u; ++edge ) {
        auto const index = triangle.indices[ edge ].position_index;
        CORE_LIB_ASSERT(  index == triangle.indices[ edge ].position_index 
                       && index == triangle.indices[ edge ].normal_index
                       && index == triangle.indices[ edge ].texture_coordinate_index );
        indices[ current++ ] = index;
      }
    }
  }
  return core::Optional< Data >( move( data ) );
#endif
}

/*******************************************************************************
** load
*******************************************************************************/

inline core::Optional<Data> load(core::fs::FilePath const & _file_path) {
  LOG_OBJPARSER(STAGE, "Loading OBJ data from '%s'..\n", cstr(_file_path));
  auto mapping_reader = core::fs::mapped_file_reader(_file_path);
  if (!mapping_reader) {
    LOG_OBJPARSER(ERROR, "Failed to map '%s' into memory!\n", cstr(_file_path));
    return core::nullopt;
  }
  Stream stream(mapping_reader->Bytes());
  if (auto data = load(stream)) {
    return core::Optional<Data>(move(data));
  }
  LOG_OBJPARSER(ERROR, "Failed to parse file!\n");
  return core::nullopt;
}

/*******************************************************************************
** convert
*******************************************************************************/

inline core::Optional<resource::ModelData>
convert(core::fs::FilePath const & _file_path) {
  if (auto obj_data = load(_file_path)) {
    CORE_LIB_UNIMPLEMENTED;
#if 0
    resource::ModelDataBuilder model_builder( ... );
#endif
    return core::nullopt;
  }
  return core::nullopt;
}

/*******************************************************************************
** save
*******************************************************************************/

inline bool save(Data const & /*_data*/,
                 core::fs::FilePath const & /*_file_path*/) {
  CORE_LIB_UNIMPLEMENTED;
  return false;
}

}}

#endif
