#ifndef _GRAPHICS_RESOURCE_TEXTURE_FROM_FILE_HPP_
#define _GRAPHICS_RESOURCE_TEXTURE_FROM_FILE_HPP_

#include "graphics/texture.h"
#include "graphics/resource/image/image_data_from_file.h"

////////////////////////////////////////////////////////////////////////////////
// graphics resource
////////////////////////////////////////////////////////////////////////////////

namespace graphics { namespace resource {

/*******************************************************************************
** texture_from_file
*******************************************************************************/

inline Texture texture_from_file(core::fs::FilePath const & _file_path,
                                 TextureSettings const & _settings) {
  if (core::Optional<ImageData> image_data_result =
          image_data_from_file(_file_path)) {
    return create_texture(image_data_result.Value(), _settings);
  }
  LOG_GRAPHICS(WARNING, "Failed to load ImageData from '%s'!\n",
               cstr(_file_path));
  return Texture();
}

inline Texture texture_from_file(core::fs::FilePath const & _file_path) {
  return texture_from_file(_file_path, TextureSettings());
}

}}

#endif