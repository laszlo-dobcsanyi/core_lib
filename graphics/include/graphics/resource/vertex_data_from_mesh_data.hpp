#ifndef _GRAPHICS_RESOURCE_VERTEX_DATA_FROM_MESH_DATA_HPP_
#define _GRAPHICS_RESOURCE_VERTEX_DATA_FROM_MESH_DATA_HPP_

////////////////////////////////////////////////////////////////////////////////
// graphics resource
////////////////////////////////////////////////////////////////////////////////

namespace graphics { namespace resource {

/*******************************************************************************
** vertex_data_from_mesh_data
*******************************************************************************/

inline IndexedVertexData<TexturedVertex>
vertex_data_from_mesh_data(StaticMeshData && _data) {
  IndexedVertexData<TexturedVertex> vertex_data;
  auto const vertex_count = _data.positions.Size();
  vertex_data.vertices = core::make_unique_array<TexturedVertex>(vertex_count);
  for (auto current = 0u; current < vertex_count; ++current) {
    vertex_data.vertices[current].position = _data.positions[current];
    vertex_data.vertices[current].normal = _data.normals[current];
    vertex_data.vertices[current].texture_coordinate =
        _data.texture_coordinates[current];
  }
  vertex_data.indices = move(_data.indices);
  return vertex_data;
}

inline IndexedVertexData<SkinnedVertex>
vertex_data_from_mesh_data(SkinnedMeshData && _data) {
  IndexedVertexData<SkinnedVertex> vertex_data;
  auto const vertex_count = _data.positions.Size();
  vertex_data.vertices = core::make_unique_array<SkinnedVertex>(vertex_count);
  for (auto current = 0u; current < vertex_count; ++current) {
    vertex_data.vertices[current].position = _data.positions[current];
    vertex_data.vertices[current].normal = _data.normals[current];
    vertex_data.vertices[current].texture_coordinate =
        _data.texture_coordinates[current];
    vertex_data.vertices[current].bone_indices = _data.bone_indices[current];
    // TODO:
    for (size_t weight = 0u; weight < 4u; ++weight) {
      vertex_data.vertices[current].bone_weights[weight] =
          _data.bone_weights[current][weight];
    }
  }
  vertex_data.indices = move(_data.indices);
  return vertex_data;
}

}}

#endif