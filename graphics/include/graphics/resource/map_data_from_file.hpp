#ifndef _GRAPHICS_RESOURCE_MAP_DATA_FROM_FILE_HPP_
#define _GRAPHICS_RESOURCE_MAP_DATA_FROM_FILE_HPP_

#include "core/fs/mapped_file.hpp"
#include "core/fs/posix_file.hpp"
#include "graphics/resource/map_data.hpp"

////////////////////////////////////////////////////////////////////////////////
// graphics resource
////////////////////////////////////////////////////////////////////////////////

namespace graphics { namespace resource {

constexpr uint32 map_file_version() { return U32(0x00000003); }

enum class MapSaveMode {
  Binary,
  Text,
};

/*******************************************************************************
** map_data_from_file
*******************************************************************************/

inline core::Optional<MapData>
map_data_from_file(core::fs::FilePath const & _file_path) {
  LOG_GRAPHICS(STAGE, "Loading map from '%s'..\n", cstr(_file_path));

  CORE_LIB_ASSERT(_file_path.Exists());
  auto mapping_reader = core::fs::mapped_file_reader(_file_path);
  if (!mapping_reader) {
    LOG_GRAPHICS(ERROR, "Failed to map '%s' into memory!\n", cstr(_file_path));
    return core::nullopt;
  }

  MapData map_data;
  core::BigEndianByteStreamReader stream(mapping_reader->Bytes());
  uint32 header;
  if (!stream.Read(header)) {
    LOG_GRAPHICS(ERROR, "Error while reading header!\n");
    return core::nullopt;
  }
  if (header != U32(0x8A4D0FF6)) {
    LOG_GRAPHICS(ERROR, "Invalid header!\n");
    return core::nullopt;
  }
  uint8 encoding;
  if (!stream.Read(encoding)) {
    LOG_GRAPHICS(ERROR, "Error while reading encoding!\n");
    return core::nullopt;
  }
  if (encoding == U8(0x00)) {
    uint32 version;
    if (!stream.Read(version)) {
      LOG_GRAPHICS(ERROR, "Error while reading version!\n");
      return core::nullopt;
    }
    LOG_GRAPHICS(MESSAGE, "Version: %" PRIu32 "\n", version);
    if (version != map_file_version()) {
      LOG_GRAPHICS(ERROR, "Invalid version!\n");
      return core::nullopt;
    }
    if (!stream.Read(map_data.grid.size)) {
      return core::nullopt;
    }
    if (!stream.Read(map_data.grid.scale)) {
      return core::nullopt;
    }
    if (!stream.Read(map_data.grid.height)) {
      return core::nullopt;
    }
    LOG_GRAPHICS(MESSAGE, "Size: %" PRIu32 "\n", map_data.grid.size);
    map_data.grid.height_map =
        core::make_unique_array<float>(map_data.grid.size * map_data.grid.size);
    for (auto height = 0u; height < map_data.grid.size; ++height) {
      for (auto width = 0u; width < map_data.grid.size; ++width) {
        if (!stream.Read(map_data.grid.height_map[height * map_data.grid.size +
                                                  width])) {
          LOG_GRAPHICS(ERROR, "Read error @ %" PRIu32 ", %" PRIu32 "!\n", width,
                       height);
          return core::nullopt;
        }
        // CORE_LIB_LOG( LOG, "%.6f\n", map_data.grid.height_map[ height * map_data.grid.size + width ] );
      }
    }
  } else if (encoding == U8(0x01)) {
    LOG_GRAPHICS(ERROR, "Unimplemented encoding!\n");
    CORE_LIB_UNIMPLEMENTED;
    return core::nullopt;
  } else {
    LOG_GRAPHICS(ERROR, "Invalid encoding!\n");
    return core::nullopt;
  }
  return map_data;
}

/*******************************************************************************
** save_map
*******************************************************************************/

inline bool save_map(MapData const & _map_data,
                     core::fs::FilePath const & _file_path, MapSaveMode _mode) {
  LOG_GRAPHICS(STAGE, "Saving '%s'..\n", cstr(_file_path));
  if (_file_path.Exists()) {
    LOG_GRAPHICS(WARNING, "Overwriting '%s'!\n", cstr(_file_path));
  }

  core::fs::posix::BigEndianBinaryFileWriter file;
  if (!file.Open(_file_path)) {
    LOG_GRAPHICS(ERROR, "Error while opening '%s'!\n", cstr(_file_path));
    return false;
  }
  if (!file.Write(U32(0x8A4D0FF6))) {
    LOG_GRAPHICS(ERROR, "Error while writing header!\n");
    return false;
  }
  if (_mode == MapSaveMode::Binary) {
    if (!file.Write(U8(0x00))) {
      LOG_GRAPHICS(ERROR, "Error while writing encoding!\n");
      return false;
    }
    if (!file.Write(map_file_version())) {
      LOG_GRAPHICS(ERROR, "Error while writing version!\n");
      return false;
    }
    if (!file.Write(_map_data.grid.size)) {
      return false;
    }
    if (!file.Write(_map_data.grid.scale)) {
      return false;
    }
    if (!file.Write(_map_data.grid.height)) {
      return false;
    }
    auto height_map = core::make_span(_map_data.grid.height_map);
    for (auto current = 0u; current < height_map.size(); ++current) {
      if (!file.Write(height_map[current])) {
        return false;
      }
    }
    return true;
  } else if (_mode == MapSaveMode::Text) {
    if (!file.Write(U8(0x01))) {
      LOG_GRAPHICS(ERROR, "Error while writing encoding!\n");
      return false;
    }
    LOG_GRAPHICS(ERROR, "Unimplemented encoding!\n");
    CORE_LIB_UNIMPLEMENTED;
    return false;
  } else {
    LOG_GRAPHICS(ERROR, "Invalid encoding!\n");
    CORE_LIB_UNIMPLEMENTED;
    return false;
  }
}

}}

#endif