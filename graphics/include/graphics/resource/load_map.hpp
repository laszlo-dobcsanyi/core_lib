#ifndef _GRAPHICS_RESOURCE_LOAD_MAP_HPP_
#define _GRAPHICS_RESOURCE_LOAD_MAP_HPP_

#include "graphics/terrain.h"
#include "graphics/create_mesh.hpp"
#include "graphics/resource/map_data_from_file.hpp"
#include "graphics/resource/vertex_data_from_map_data.hpp"
#include "graphics/resource/texture_from_file.hpp"

////////////////////////////////////////////////////////////////////////////////
// graphics resource
////////////////////////////////////////////////////////////////////////////////

namespace graphics { namespace resource {

/*******************************************************************************
** load_map
*******************************************************************************/

inline core::Optional<Map> load_map(core::fs::Directory _map_directory) {
  LOG_GRAPHICS(STAGE, "Loading Map from '%s'!\n", cstr(_map_directory));

  Map map;

  MapData & map_data = map.map_data;
  {
    auto map_data_path = _map_directory / "map.data";
    if (auto map_data_result = map_data_from_file(map_data_path)) {
      map_data = move(*map_data_result);
    } else {
      LOG_GRAPHICS(ERROR, "Failed to load map data '%s'!\n",
                   cstr(map_data_path));
      return core::nullopt;
    }
  }

  Terrain & terrain = map.terrain;
  {
    terrain.mesh = create_mesh(vertex_data_from_map_data(map_data));
    terrain.size = map.map_data.grid.size;

    // BlendMap
    {
      size_t resolution = static_cast<size_t>(map.map_data.grid.scale / 64.f);
      size_t size = terrain.size * resolution;
      float offset = (map.map_data.grid.size - 1u) / 2.f;
      ImageData blend_map_data(size, size, ColorFormat::ABGR8UI);
      float blend_height[4u] = {0.f, 0.333f, 0.666f, 1.f};
      for (auto row = 0u; row < blend_map_data.Height(); ++row) {
        uint32 * row_memory =
            reinterpret_cast<uint32 *>(blend_map_data.GetRowBytes(row).data());
        for (auto column = 0u; column < blend_map_data.Width(); ++column) {
          numerics::Vector3f position;
          position[0u] = ((float)column / (float)resolution - offset) *
                         map.map_data.grid.scale;
          position[1u] = 0.f;
          position[2u] = (((float)row / (float)resolution) - offset) *
                         map.map_data.grid.scale;
          float height = map.map_data.grid.height_at(position);
          uint32 pixel = 0;
          for (auto current = 0u; current < 4u; ++current) {
            uint32 value = 0;
            float blend =
                abs(blend_height[current] * map.map_data.grid.height - height) /
                map.map_data.grid.height * 3.f * 256.f;
            if (blend < 255.f) {
              value = static_cast<uint8>(255.f - blend);
            }
            pixel |= (value & 0x000000FF) << (current * 8u);
          }
          *row_memory = pixel;
          ++row_memory;
        }
      }

      terrain.blend_map = create_texture(blend_map_data, TextureSettings());
      if (!terrain.blend_map) {
        LOG_GRAPHICS(ERROR, "Failed to create blend map texture!\n");
        return core::nullopt;
      }
    }

    // Layers
    terrain.layer_textures = core::make_unique_array<Texture>(4u);
    for (size_t current = 0u; current < terrain.layer_textures.Size();
         ++current) {
      string::Stream file_name_stream;
      file_name_stream << "layer_";
      file_name_stream << current;
      file_name_stream << ".tga";

      TextureSettings texture_settings;
      texture_settings.generate_mipmap = true;
      terrain.layer_textures[current] = resource::texture_from_file(
          _map_directory / file_name_stream.Get(), texture_settings);
      if (!terrain.layer_textures[current]) {
        LOG_GRAPHICS(ERROR, "Failed to load layer %zu ('%s')!\n", current);
        return core::nullopt;
      }
    }
  }

  return map;
}

/*******************************************************************************
** load_terrain
*******************************************************************************/

inline core::Optional<Terrain>
load_terrain(core::fs::Directory _map_directory) {
  if (core::Optional<Map> map_result = load_map(move(_map_directory))) {
    return move(map_result->terrain);
  }
  return core::nullopt;
}

}}

#endif
