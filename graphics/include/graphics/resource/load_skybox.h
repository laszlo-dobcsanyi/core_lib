#ifndef _GRAPHICS_RESOURCE_LOAD_SKYBOX_H_
#define _GRAPHICS_RESOURCE_LOAD_SKYBOX_H_

#include "core/fs/directory.h"
#include "graphics/skybox.h"

////////////////////////////////////////////////////////////////////////////////
// graphics resource
////////////////////////////////////////////////////////////////////////////////

namespace graphics { namespace resource {

/*******************************************************************************
** load_skybox
*******************************************************************************/

core::Optional<Skybox> load_skybox(core::fs::Directory _skybox_directory);

}}

#endif