#ifndef _GRAPHICS_RESOURCE_LOAD_MODEL_H_
#define _GRAPHICS_RESOURCE_LOAD_MODEL_H_

#include "core/fs/directory.h"
#include "graphics/static_model.h"
#include "graphics/animated_model.h"

////////////////////////////////////////////////////////////////////////////////
// graphics resource
////////////////////////////////////////////////////////////////////////////////

namespace graphics { namespace resource {

/*******************************************************************************
** load_static_model
*******************************************************************************/

core::Optional<StaticModel>
load_static_model(core::fs::Directory _model_directory);

/*******************************************************************************
** load_animated_model
*******************************************************************************/

core::Optional<AnimatedModel>
load_animated_model(core::fs::Directory _model_directory);

}}

#endif