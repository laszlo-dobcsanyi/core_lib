#ifndef _GRAPHICS_RESOURCE_CONVERT_IMAGE_DATA_HPP_
#define _GRAPHICS_RESOURCE_CONVERT_IMAGE_DATA_HPP_

////////////////////////////////////////////////////////////////////////////////
// graphics detail
////////////////////////////////////////////////////////////////////////////////

namespace graphics { namespace detail {

/*******************************************************************************
** swap_pixel
*******************************************************************************/

inline void swap_24bit_pixel(byte * _ptr) { core::swap(_ptr[0u], _ptr[2u]); }

inline void swap_32bit_pixel(byte * _ptr) {
  auto & value = *reinterpret_cast<uint32 *>(_ptr);
  value = byte_swap(value);
}

/*******************************************************************************
** convert_pixels
*******************************************************************************/

template<typename F>
inline void convert_pixels(ImageData & _data, F _f) {
  auto const byte_per_pixel = format_bpp(_data.Format()) / 8u;
  for (auto row = 0u; row < _data.Height(); ++row) {
    auto row_bytes = _data.GetRowBytes(row);
    auto row_memory = reinterpret_cast<uintptr_t>(row_buffer.Data());
    for (auto column = 0u; column < _data.Width(); column += byte_per_pixel) {
      _f(&row_bytes[column]);
    }
  }
}

/*******************************************************************************
** swap_components
*******************************************************************************/

inline void swap_pixel_components(ImageData & _data) {
  auto const bpp = format_bpp(_data->Format());
  switch (bpp) {
    case 24u: {
      convert_pixels(_data, swap_24bit_pixel);
    } break;
    case 32u: {
      convert_pixels(_data, swap_32bit_pixel);
    } break;
    default: {
      CORE_LIB_UNIMPLEMENTED;
    } break;
  }
}

}}

////////////////////////////////////////////////////////////////////////////////
// graphics
////////////////////////////////////////////////////////////////////////////////

namespace graphics {

/*******************************************************************************
** convert_format
*******************************************************************************/

inline void convert_format(ImageData & _data, ColorFormat _new_format) {
  if (_data.Format() == _new_format)
    return;

  switch (_data.Format()) {
    case ColorFormat::BGR8UI: {
      switch (_new_format) {
        case ColorFormat::RGB8UI: {
          detail::swap_pixel_components(_data);
          _data.SetFormat(_new_format);
        } break;
        default: {
          CORE_LIB_UNIMPLEMENTED;
        }
      }
    } break;
    default: {
      CORE_LIB_UNIMPLEMENTED;
    }
  }
}

}

#endif
