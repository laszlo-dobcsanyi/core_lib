#ifndef _GRAPHICS_RESOURCE_MODEL_DATA_BUILDER_HPP_
#define _GRAPHICS_RESOURCE_MODEL_DATA_BUILDER_HPP_

#include "core/containers/stacks.hpp"
#include "core/queries.hpp"
#include "graphics/resource/model_data.hpp"

////////////////////////////////////////////////////////////////////////////////
// graphics resource
////////////////////////////////////////////////////////////////////////////////

namespace graphics { namespace resource {

/*******************************************************************************
** DynamicBuilder
*******************************************************************************/

template<typename T>
using DynamicBuilder = container::UnboundedStack<T>;

/*******************************************************************************
** PrimitiveBuilder
*******************************************************************************/

template<typename T>
struct PrimitiveBuilder {
  core::UniqueArray<T> data;
  DynamicBuilder<T> builder;

  size_t Size() const {
    if (data) {
      return data.Size();
    }
    return builder.Size();
  }

  bool IsEmpty() const {
    if (data) {
      return data.IsEmpty();
    }
    return builder.IsEmpty();
  }

  core::UniqueArray<T> Build() {
    if (data) {
      return move(data);
    }
    return container::to_unique_array<T>(move(builder));
  }
};

/*******************************************************************************
** StaticMeshDataBuilder
*******************************************************************************/

struct StaticMeshDataBuilder {
  PrimitiveBuilder<numerics::Vector3f> positions;
  PrimitiveBuilder<numerics::Vector3f> normals;
  PrimitiveBuilder<numerics::Vector2f> texture_coordinates;
  PrimitiveBuilder<uint32> indices;
};

/*******************************************************************************
** BoneDataBuilder
*******************************************************************************/

struct BoneDataBuilder {
  uint32 bone_indices = 0u;
  container::InlineArray<uint32, 4u> indices;
  container::InlineArray<float, 4u> weights;
  uint32 count = 0u;

  void AddBone(uint32 _index, float _weight) {
    auto index = count;
    if (count < 4u) {
      ++count;
    } else {
      auto min_index = 0u;
      for (auto current = 1u; current < count; ++current)
        if (weights[current] < weights[min_index]) {
          min_index = current;
        }
      if (_weight < weights[min_index])
        return;
      index = min_index;
    }
    indices[index] = _index;
    weights[index] = _weight;
  }

  void Build() {
    bone_indices = 0u;
    for (auto current = 0u; current < count; ++current) {
      bone_indices = bone_indices << 7u;
      CORE_LIB_ASSERT((indices[count - current - 1u] & (~0x0000007F)) == 0u);
      bone_indices |= (indices[count - current - 1u] & 0x0000007F);
    }
    bone_indices = bone_indices << 4u;
    bone_indices |= (count & 0x0000000F);
#if defined(CORE_LIB_CONFIGURATION_DEBUG)
    uint32 bone_count = bone_indices & U32(0x0000000F);
    CORE_LIB_ASSERT(bone_count == count);
    if (0u < bone_count) {
      uint32 bone_index = (bone_indices & U32(0x000007F0)) >> 4u;
      CORE_LIB_ASSERT(indices[0u] == bone_index);
    }
    if (1u < bone_count) {
      uint32 bone_index = (bone_indices & U32(0x0003F800)) >> 11u;
      CORE_LIB_ASSERT(indices[1u] == bone_index);
    }
    if (2u < bone_count) {
      uint32 bone_index = (bone_indices & U32(0x01FC0000)) >> 18u;
      CORE_LIB_ASSERT(indices[2u] == bone_index);
    }
    if (3u < bone_count) {
      uint32 bone_index = (bone_indices & U32(0xFE000000)) >> 25u;
      CORE_LIB_ASSERT(indices[3u] == bone_index);
    }
#endif
  }

  bool Validate(uint32 _bones_count) {
    if (count == 0u)
      return false;

    auto bone_weights = 0.f;
    for (auto current = 0u; current < count; ++current)
      bone_weights += weights[current];
    if (!numerics::is_nearly_zero(bone_weights - 1.f)) {
      CORE_LIB_LOG(LOG, "%.7f\n", bone_weights);
    }

    for (auto current = 0u; current < count; ++current)
      if (_bones_count <= indices[current])
        return false;
    return true;
  }
};

/*******************************************************************************
** SkinnedMeshDataBuilder
*******************************************************************************/

struct SkinnedMeshDataBuilder {
  PrimitiveBuilder<numerics::Vector3f> positions;
  PrimitiveBuilder<numerics::Vector3f> normals;
  PrimitiveBuilder<numerics::Vector2f> texture_coordinates;
  PrimitiveBuilder<BoneDataBuilder> bone_builders;
  PrimitiveBuilder<uint32> indices;
};

/*******************************************************************************
** SkeletonDataBuilder
*******************************************************************************/

struct SkeletonDataBuilder {
  PrimitiveBuilder<Bone> bones;
};

/*******************************************************************************
** AnimationChannelBuilder
*******************************************************************************/

struct AnimationChannelBuilder {
  PrimitiveBuilder<AnimationValue> translation;
  PrimitiveBuilder<AnimationValue> rotation;
  PrimitiveBuilder<AnimationValue> scale;
};

/*******************************************************************************
** AnimationDataBuilder
*******************************************************************************/

struct AnimationDataBuilder {
  DynamicBuilder<AnimationChannelBuilder> channels;
};

/*******************************************************************************
** StaticModelDataBuilder
*******************************************************************************/

struct StaticModelDataBuilder {
  StaticMeshDataBuilder mesh_data_builder;
};

/*******************************************************************************
** AnimatedModelDataBuilder
*******************************************************************************/

struct AnimatedModelDataBuilder {
  SkinnedMeshDataBuilder mesh_data_builder;
  SkeletonDataBuilder skeleton_data_builder;
  PrimitiveBuilder<AnimationDataBuilder> animation_data_builders;
};

/*******************************************************************************
** ModelDataBuilder
*******************************************************************************/

using ModelDataBuilder =
    core::Variant<StaticModelDataBuilder, AnimatedModelDataBuilder>;

/*******************************************************************************
** validate
*******************************************************************************/

inline bool validate(StaticModelDataBuilder & _builder) {
  LOG_GRAPHICS(VERBOSE, "Validating...\n");

  auto & mesh_builder = _builder.mesh_data_builder;
  size_t vertex_count = mesh_builder.positions.Size();
  if (mesh_builder.normals.Size() != vertex_count) {
    LOG_GRAPHICS(ERROR,
                 "Validation failed: Normal count %zu != Vertex count %zu!\n",
                 mesh_builder.normals.Size(), vertex_count);
    return false;
  }
  if (mesh_builder.texture_coordinates.Size() != vertex_count) {
    LOG_GRAPHICS(ERROR,
                 "Validation failed: UV count %zu != Vertex count %zu!\n",
                 mesh_builder.texture_coordinates.Size(), vertex_count);
    return false;
  }

  return true;
}

inline bool validate(AnimatedModelDataBuilder & _builder) {
  LOG_GRAPHICS(VERBOSE, "Validating...\n");

  auto & mesh_builder = _builder.mesh_data_builder;
  size_t vertex_count = mesh_builder.positions.Size();
  if (mesh_builder.normals.Size() != vertex_count) {
    LOG_GRAPHICS(ERROR,
                 "Validation failed: Normal count %zu != Vertex count %zu!\n",
                 mesh_builder.normals.Size(), vertex_count);
    return false;
  }
  if (mesh_builder.texture_coordinates.Size() != vertex_count) {
    LOG_GRAPHICS(ERROR,
                 "Validation failed: UV count %zu != Vertex count %zu!\n",
                 mesh_builder.texture_coordinates.Size(), vertex_count);
    return false;
  }
  if (mesh_builder.bone_builders.Size() != vertex_count) {
    LOG_GRAPHICS(
        ERROR,
        "Validation failed: Bone builder count %zu != Vertex count %zu!\n",
        mesh_builder.bone_builders.Size(), vertex_count);
    return false;
  }

  auto & skeleton_builder = _builder.skeleton_data_builder;
  size_t bones_count = skeleton_builder.bones.Size();
  for (auto current = 0u; current < vertex_count; ++current)
    if (!mesh_builder.bone_builders.data[current].Validate(bones_count)) {
      LOG_GRAPHICS(ERROR, "Validation failed: Invalid bone builder!\n");
      return false;
    }

  for (AnimationDataBuilder & animation_builder :
       _builder.animation_data_builders.data) {
    size_t channels_count = animation_builder.channels.Size();
    if (bones_count != channels_count) {
      LOG_GRAPHICS(ERROR,
                   "Validation failed: Skeleton bone count %zu != Animation "
                   "channels count %zu!\n",
                   bones_count, channels_count);
      return false;
    }
  }
  return true;
}

/*******************************************************************************
** build
*******************************************************************************/

inline StaticMeshData build(StaticMeshDataBuilder && _builder) {
  StaticMeshData static_mesh_data;
  static_mesh_data.positions = _builder.positions.Build();
  static_mesh_data.normals = _builder.normals.Build();
  static_mesh_data.texture_coordinates = _builder.texture_coordinates.Build();
  static_mesh_data.indices = _builder.indices.Build();
  return static_mesh_data;
}

inline StaticModelData build(StaticModelDataBuilder && _builder) {
  CORE_LIB_ASSERT(validate(_builder));
  StaticModelData static_model_data;
  static_model_data.mesh_data = build(move(_builder.mesh_data_builder));
  return static_model_data;
}

inline SkinnedMeshData build(SkinnedMeshDataBuilder && _builder) {
  SkinnedMeshData skinned_mesh_data;
  skinned_mesh_data.positions = _builder.positions.Build();
  skinned_mesh_data.normals = _builder.normals.Build();
  skinned_mesh_data.texture_coordinates = _builder.texture_coordinates.Build();
  // TODO:
  {
    core::UniqueArray<BoneDataBuilder> bone_data_builders =
        _builder.bone_builders.Build();
    skinned_mesh_data.bone_indices =
        core::make_unique_array<uint32>(bone_data_builders.Size());
    skinned_mesh_data.bone_weights =
        core::make_unique_array<numerics::Vector4f>(bone_data_builders.Size());
    for (auto current = 0u; current < bone_data_builders.Size(); ++current) {
      bone_data_builders[current].Build();
      skinned_mesh_data.bone_indices[current] =
          bone_data_builders[current].bone_indices;
      skinned_mesh_data.bone_weights[current][0u] =
          bone_data_builders[current].weights[0u];
      skinned_mesh_data.bone_weights[current][1u] =
          bone_data_builders[current].weights[1u];
      skinned_mesh_data.bone_weights[current][2u] =
          bone_data_builders[current].weights[2u];
      skinned_mesh_data.bone_weights[current][3u] =
          bone_data_builders[current].weights[3u];
    }
  }
  skinned_mesh_data.indices = _builder.indices.Build();
  return skinned_mesh_data;
}

inline SkeletonData build(SkeletonDataBuilder && _builder) {
  SkeletonData skeleton_data;
  skeleton_data.bones = _builder.bones.Build();
  return skeleton_data;
}

inline AnimationData build(AnimationDataBuilder && _builder) {
  AnimationData animation_data;
  animation_data.channels =
      core::make_unique_array<AnimationChannel>(_builder.channels.Size());
  auto channel_index = 0u;
  auto channel_builder_query = container::make_query(_builder.channels);
  while (channel_builder_query) {
    auto & channel_builder = channel_builder_query.Forward();
    auto & channel = animation_data.channels[channel_index++];
    channel.translation = channel_builder.translation.Build();
    channel.rotation = channel_builder.rotation.Build();
    channel.scale = channel_builder.scale.Build();
  }
  return animation_data;
}

inline core::UniqueArray<AnimationData>
build(PrimitiveBuilder<AnimationDataBuilder> _builders) {
  core::UniqueArray<AnimationDataBuilder> builders = _builders.Build();
  core::UniqueArray<AnimationData> animation_data =
      core::make_unique_array<AnimationData>(builders.Size());
  for (auto current = 0u; current < animation_data.Size(); ++current)
    animation_data[current] = build(move(builders[current]));
  return animation_data;
}

inline AnimatedModelData build(AnimatedModelDataBuilder && _builder) {
  CORE_LIB_ASSERT(validate(_builder));
  AnimatedModelData model_data;
  model_data.mesh_data = build(move(_builder.mesh_data_builder));
  model_data.skeleton_data = build(move(_builder.skeleton_data_builder));
  model_data.animation_data = build(move(_builder.animation_data_builders));
  return model_data;
}

inline core::Optional<ModelData> build(ModelDataBuilder && _builder) {
  if (_builder.Is<StaticModelDataBuilder>()) {
    auto & static_model_data_builder = _builder.As<StaticModelDataBuilder>();
    if (!validate(static_model_data_builder))
      return core::nullopt;
    return ModelData(in_place_t<StaticModelData>(),
                     build(move(static_model_data_builder)));
  } else if (_builder.Is<AnimatedModelDataBuilder>()) {
    auto & animated_model_data_builder =
        _builder.As<AnimatedModelDataBuilder>();
    if (!validate(animated_model_data_builder))
      return core::nullopt;
    return ModelData(in_place_t<AnimatedModelData>(),
                     build(move(animated_model_data_builder)));
  }
  return core::nullopt;
}

}}

#endif
