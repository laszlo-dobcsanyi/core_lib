#ifndef _GRAPHICS_RESOURCE_VERTEX_DATA_FROM_MAP_DATA_HPP_
#define _GRAPHICS_RESOURCE_VERTEX_DATA_FROM_MAP_DATA_HPP_

////////////////////////////////////////////////////////////////////////////////
// graphics resource
////////////////////////////////////////////////////////////////////////////////

namespace graphics { namespace resource {

/*******************************************************************************
** clamp
*******************************************************************************/

// TODO: Remove this. Note signed/unsigned stuff in here!
inline uint32 clamp(uint32 _min, int32 _value, uint32 _max) {
  if (_value <= I32(0)) {
    return _min;
  }
  auto const value = static_cast<uint32>(_value);
  if (value < _min)
    return _min;
  if (_max < value)
    return _max;
  return value;
}

/*******************************************************************************
** map_height_at
*******************************************************************************/

inline float height_at(MapData::Grid const & _grid, int32 _x, int32 _z) {
  auto const x = clamp(U32(0u), _x, _grid.size - 1u);
  auto const z = clamp(U32(0u), _z, _grid.size - 1u);
  auto const index = z * _grid.size + x;
  return _grid.height_map[index];
}

/*******************************************************************************
** terrain_from_map_data
*******************************************************************************/

inline IndexedVertexData<TexturedVertex>
vertex_data_from_map_data(MapData const & _map_data) {
  IndexedVertexData<TexturedVertex> vertex_data;

  auto & grid = _map_data.grid;

  // Vertices
  float offset = (float)(grid.size - 1u) / 2.f;
  vertex_data.vertices =
      core::make_unique_array<TexturedVertex>(grid.size * grid.size);
  for (uint32 z = 0u; z < grid.size; ++z) {
    for (uint32 x = 0u; x < grid.size; ++x) {
      // Position
      auto & position = vertex_data.vertices[z * grid.size + x].position;
      position = numerics::Vector3f(
          (static_cast<float>(x) - offset) * _map_data.grid.scale,
          height_at(_map_data.grid, x, z),
          (static_cast<float>(z) - offset) * _map_data.grid.scale);
      // Normal
      auto & normal = vertex_data.vertices[z * grid.size + x].normal;
      auto const left = height_at(_map_data.grid, static_cast<int32>(x) - 1, z);
      auto const right =
          height_at(_map_data.grid, static_cast<int32>(x) + 1, z);
      auto const top = height_at(_map_data.grid, x, static_cast<int32>(z) - 1);
      auto const bottom =
          height_at(_map_data.grid, x, static_cast<int32>(z) + 1);
      normal = numerics::Vector3f(left - right, 2.f, top - bottom);
      numerics::normalize(normal);
      // Texture_coordinate
      auto & texture_coordinate =
          vertex_data.vertices[z * grid.size + x].texture_coordinate;
      texture_coordinate =
          numerics::Vector2f(static_cast<float>(x), static_cast<float>(z));
    }
  }

  // Indices
  auto const tile_count = (grid.size - 1u) * (grid.size - 1u);
  vertex_data.indices = core::make_unique_array<uint32>(tile_count * 2u * 3u);
  for (auto z = 0u; z < grid.size - 1u; ++z) {
    for (auto x = 0u; x < grid.size - 1u; ++x) {
      auto const index = z * (grid.size - 1u) + x;
      auto const current = z * grid.size + x;
      vertex_data.indices[index * 6u + 0u] = current;
      vertex_data.indices[index * 6u + 1u] = current + grid.size + 1u;
      vertex_data.indices[index * 6u + 2u] = current + grid.size;
      vertex_data.indices[index * 6u + 3u] = current;
      vertex_data.indices[index * 6u + 4u] = current + 1u;
      vertex_data.indices[index * 6u + 5u] = current + grid.size + 1u;
    }
  }

  return vertex_data;
}

}}

#endif