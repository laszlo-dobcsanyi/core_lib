#ifndef _GRAPHICS_RESOURCE_CGM_HPP_
#define _GRAPHICS_RESOURCE_CGM_HPP_

#include "core/base.h"
#include "core/fs/filepath.h"
#include "graphics/resource/model_data.hpp"

#define LOG_CGM(__severity__, ...) CORE_LIB_LOG(LOG, __VA_ARGS__)

////////////////////////////////////////////////////////////////////////////////
// graphics cgm
////////////////////////////////////////////////////////////////////////////////

namespace graphics { namespace cgm {

/*******************************************************************************
** version
*******************************************************************************/

constexpr uint32 version() { return 0; }

/*******************************************************************************
** load
*******************************************************************************/

inline core::Optional<resource::ModelData>
load(core::fs::FilePath const & /*_file*/) {
  CORE_LIB_UNIMPLEMENTED;
  return core::nullopt;
}

/*******************************************************************************
** save
*******************************************************************************/

inline bool save(resource::ModelData const & /*_data*/,
                 core::fs::FilePath const & /*_file*/) {
  CORE_LIB_UNIMPLEMENTED;
  return false;
}

}}

#endif
