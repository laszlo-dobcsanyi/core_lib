#ifndef _GRAPHICS_RESOURCE_MAP_DATA_HPP_
#define _GRAPHICS_RESOURCE_MAP_DATA_HPP_

#include "core/memory/unique_array.hpp"
#include "graphics/base.hpp"

////////////////////////////////////////////////////////////////////////////////
// graphics resource
////////////////////////////////////////////////////////////////////////////////

namespace graphics { namespace resource {

/*******************************************************************************
** MapData
*******************************************************************************/

struct MapData {
  struct Grid {
    uint32 size;
    float scale;
    float height;
    core::UniqueArray<float> height_map;

    float height_at(numerics::Vector3f _position) const {
      // CORE_LIB_LOG( LOG, "\nPosition: (%.4f, %.4f, %.4f)\n", _position[ 0u ], _position[ 1u ], _position[ 2u ] );
      _position += numerics::Vector3f((float)(size - 1u) * scale / 2.f, 0.f,
                                      (float)(size - 1u) * scale / 2.f);
      // CORE_LIB_LOG( LOG, "Adjusted position: (%.4f, %.4f, %.4f)\n", _position[ 0u ], _position[ 1u ], _position[ 2u ] );
      _position /= scale;
      // CORE_LIB_LOG( LOG, "Scaled position: (%.4f, %.4f, %.4f)\n", _position[ 0u ], _position[ 1u ], _position[ 2u ] );
      if (_position.x <= 0.f || (float)(size - 1u) <= _position.x)
        return 0.f;
      if (_position.z <= 0.f || (float)(size - 1u) <= _position.z)
        return 0.f;
      uint32 column = (uint32)_position.x;
      uint32 row = (uint32)_position.z;
      CORE_LIB_ASSERT(column < size);
      CORE_LIB_ASSERT(row < size);
      float x = _position.x - (float)column;
      float z = _position.z - (float)row;
      if (x < z) {
        float a = height_map[row * size + column];
        float b = height_map[(row + 1u) * size + column];
        float c = height_map[(row + 1u) * size + (column + 1u)];
        return a + (b - a) * z + (c - b) * x;
      } else {
        float a = height_map[row * size + column];
        float b = height_map[row * size + (column + 1u)];
        float c = height_map[(row + 1u) * size + (column + 1u)];
        return a + (b - a) * x + (c - b) * z;
      }
    }
  } grid;
};

}}

#endif