#ifndef _GRAPHICS_RESOURCE_FBX_SCENE_HPP_
#define _GRAPHICS_RESOURCE_FBX_SCENE_HPP_

#include "core/sort.hpp"

#define LOG_FBX_SCENE(__severity__, ...) LOG_FBX(__severity__, __VA_ARGS__)

////////////////////////////////////////////////////////////////////////////////
// x scene
////////////////////////////////////////////////////////////////////////////////

namespace graphics { namespace fbx { namespace scene {

/*******************************************************************************
** Matrix
*******************************************************************************/

using Matrix = numerics::ColumnMajorMatrix4x4<float>;

/*******************************************************************************
** Color
*******************************************************************************/

using Color = numerics::Vector3f;

/*******************************************************************************
** Time
*******************************************************************************/

struct Time {
  double seconds;
};

/*******************************************************************************
** convert
*******************************************************************************/

inline float convert_component(Property const & _property) {
  return static_cast<float>(_property.As<Primitive>().As<double>());
}

inline void convert(float & _value, Property const & _property) {
  CORE_LIB_ASSERT(_property.Is<Primitive>());
  if (_property.As<Primitive>().Is<float>()) {
    _value = _property.As<Primitive>().As<float>();
  } else if (_property.As<Primitive>().Is<double>()) {
    _value = static_cast<float>(_property.As<Primitive>().As<double>());
  } else {
    CORE_LIB_ASSERT(false);
  }
}

inline void convert(numerics::Vector3f & _vector, Property const & _x,
                    Property const & _y, Property const & _z) {
  convert(_vector.x, _x);
  convert(_vector.y, _y);
  convert(_vector.z, _z);
}

inline void convert(float & _value, Node const & _node, char const * _name) {
  CORE_LIB_ASSERT(_node.name == "P");
  CORE_LIB_ASSERT(_node.properties.Size() == 5u);
  CORE_LIB_ASSERT(_node.properties[0u].As<String>() == _name);
  CORE_LIB_ASSERT(_node.properties[1u].As<String>() == "Number");
  // TODO: Flag?
  convert(_value, _node.properties[4u]);
}

inline void convert(Time & _time, int64 _fbx_time) {
  _time.seconds = static_cast<double>(_fbx_time) / 46186158000L;
}

/*******************************************************************************
** parse
*******************************************************************************/

inline void parse_matrix(Matrix & _matrix, DoubleArray const & _elements) {
  CORE_LIB_ASSERT(_elements.Size() == 16u);
  for (auto current = 0u; current < 16u; ++current) {
    _matrix.storage[current] = static_cast<float>(_elements[current]);
  }
}

inline void parse(Color & _color, Node const & _node, char const * _name) {
  CORE_LIB_ASSERT(_node.name == "P");
  CORE_LIB_ASSERT(_node.properties.Size() == 7u);
  CORE_LIB_ASSERT(_node.properties[0u].As<String>() == _name);
  CORE_LIB_ASSERT(_node.properties[1u].As<String>() == "ColorRGB" ||
                  _node.properties[1u].As<String>() == "Color");
  // TODO: Flag?
  convert(_color, _node.properties[4u], _node.properties[5u],
          _node.properties[6u]);
}

inline void parse(Time & _time, Node const & _node, char const * _name) {
  CORE_LIB_ASSERT(_node.name == "P");
  CORE_LIB_ASSERT(_node.properties.Size() == 5u);
  CORE_LIB_ASSERT(_node.properties[0u].As<String>() == _name);
  CORE_LIB_ASSERT(_node.properties[1u].As<String>() == "KTime");
  CORE_LIB_ASSERT(_node.properties[2u].As<String>() == "Time");
  // TODO: Flag?
  convert(_time, _node.properties[4u].As<Primitive>().As<int64>());
}

inline void parse(String & _value, Node const & _node, char const * _name) {
  CORE_LIB_ASSERT(_node.name == "P");
  CORE_LIB_ASSERT(_node.properties.Size() == 5u);
  CORE_LIB_ASSERT(_node.properties[0u].As<String>() == _name);
  CORE_LIB_ASSERT(_node.properties[1u].As<String>() == "KString");
  CORE_LIB_ASSERT(_node.properties[2u].As<String>() == "");
  // TODO: Flag?
  _value = move(_node.properties[4u].As<String>());
}

inline void parse(double & _value, Node const & _node, char const * _name) {
  CORE_LIB_ASSERT(_node.name == "P");
  CORE_LIB_ASSERT(_node.properties.Size() == 5u);
  CORE_LIB_ASSERT(_node.properties[0u].As<String>() == _name);
  CORE_LIB_ASSERT(_node.properties[1u].As<String>() == "double");
  // TODO: Flag?
  _value = _node.properties[4u].As<Primitive>().As<double>();
}

inline void parse(float & _value, Node const & _node, char const * _name) {
  CORE_LIB_ASSERT(_node.name == "P");
  CORE_LIB_ASSERT(_node.properties.Size() == 5u);
  CORE_LIB_ASSERT(_node.properties[0u].As<String>() == _name);
  CORE_LIB_ASSERT(_node.properties[1u].As<String>() == "float");
  // TODO: Flag?
  _value = _node.properties[4u].As<Primitive>().As<float>();
}

/*******************************************************************************
** ObjectBase
*******************************************************************************/

struct ObjectBase {
  int64 id = 0;
  const_string name;
};

/*******************************************************************************
** PropertyTemplate
*******************************************************************************/

template<class Properties>
class PropertyTemplate : public ObjectBase {
public:
  Properties properties;

  PropertyTemplate() = default;
  PropertyTemplate(Properties const & _template)
      : ObjectBase()
      , properties(_template) {}
};

/*******************************************************************************
** Container
*******************************************************************************/

template<typename ValueType, typename TemplateType>
class Container {
public:
  using value_type = ValueType;

  core::UniqueArray<ValueType> values;
  core::Optional<TemplateType> property_template = core::nullopt;

  size_t Size() const { return values.Size(); }

  ValueType & operator[](size_t _index) { return values[_index]; }
  ValueType const & operator[](size_t _index) const { return values[_index]; }
};

/*******************************************************************************
** Node Attribute
*******************************************************************************/

struct NodeAttributeProperties {
  Color color;
  double size;
  double limb_length;
};

struct Null {};
struct LimbNode {};
struct Light {};

using NodeAttributeObject = core::Variant<Null, LimbNode, Light>;

struct NodeAttribute : public PropertyTemplate<NodeAttributeProperties> {
  NodeAttributeObject object;

  NodeAttribute()
      : object(in_place_t<Null>()){};
  NodeAttribute(NodeAttributeProperties const & _properties)
      : PropertyTemplate<NodeAttributeProperties>(_properties)
      , object(in_place_t<Null>()) {}
};

#if 0
using NodeAttribute = PropertyTemplate< NodeAttributeProperties >;
#endif
using NodeAttributes = Container<NodeAttribute, NodeAttributeProperties>;

/*******************************************************************************
** Geometry
*******************************************************************************/

struct BoundingBox {
  numerics::Vector3f min;
  numerics::Vector3f max;
};

struct GeometryProperties {
  Color color;
  BoundingBox bounding_box;
  // bool primary_visibility;
  // bool casts_shadow;
  // bool receive_shadows;
};

enum class MappingInformationType {
  ByVertice,
  ByPolygonVertex,
};

enum class ReferenceInformationType {
  Direct,
  IndexToDirect,
};

struct LayerElementNormal {
  int32 version;
  // String name;
  MappingInformationType mapping_information_type;
  ReferenceInformationType reference_information_type;
  core::UniqueArray<double> normals;
  core::UniqueArray<double> normals_w;
};

struct LayerElementUV {
  int32 version;
  // String name;
  MappingInformationType mapping_information_type;
  ReferenceInformationType reference_information_type;
  core::UniqueArray<double> uv;
  core::UniqueArray<int32> uv_index;
};

struct Geometry : public PropertyTemplate<GeometryProperties> {
  core::UniqueArray<double> vertices;
  core::UniqueArray<int32> indices;
  // core::UniqueArray< int32 > edges;
  LayerElementNormal layer_element_normal;
  core::UniqueArray<LayerElementUV> layer_element_uvs;

  Geometry() = default;
  Geometry(GeometryProperties const & _properties)
      : PropertyTemplate<GeometryProperties>(_properties) {}
};

using Geometries = Container<Geometry, GeometryProperties>;

/*******************************************************************************
** Model
*******************************************************************************/

using LclTranslation = numerics::Vector3f;
using LclRotation = numerics::Vector3f;
using LclScaling = numerics::Vector3f;

struct ModelProperties {
  LclTranslation translation;
  LclRotation rotation;
  LclScaling scaling;
};

struct Model : public PropertyTemplate<ModelProperties> {
  String type;

  Model() = default;
  Model(ModelProperties const & _properties)
      : PropertyTemplate<ModelProperties>(_properties) {}
};

#if 0
using Model = PropertyTemplate< ModelProperties >;
#endif
using Models = Container<Model, ModelProperties>;

/*******************************************************************************
** Pose
*******************************************************************************/

struct PoseNode {
  int64 id;
  Matrix matrix;
};

struct Pose : public ObjectBase {
  core::UniqueArray<PoseNode> nodes;
};

using Poses = core::UniqueArray<Pose>;

/*******************************************************************************
** Skin
*******************************************************************************/

struct Skin : public ObjectBase {};

/*******************************************************************************
** Cluster
*******************************************************************************/

struct Cluster : public ObjectBase {
  core::UniqueArray<int32> indices;
  core::UniqueArray<float> weights;
  Matrix transform;
  Matrix transform_link;
};

/*******************************************************************************
** Deformer
*******************************************************************************/

using Deformer = core::Variant<Valueless, Skin, Cluster>;
using Deformers = core::UniqueArray<Deformer>;

/*******************************************************************************
** Material
*******************************************************************************/

struct MaterialProperties {
  const_string shading_model;
  Color emissive_color;
  float emissive_factor;
  Color ambient_color;
  float ambient_factor;
  Color diffuse_color;
  float diffuse_factor;
};

struct Material : public PropertyTemplate<MaterialProperties> {
  Material() = default;
  Material(MaterialProperties const & _properties)
      : PropertyTemplate<MaterialProperties>(_properties) {}
};

using Materials = Container<Material, MaterialProperties>;

/*******************************************************************************
** AnimationStack
*******************************************************************************/

struct AnimationStackProperties {
  Time local_start;
  Time local_stop;
  Time reference_start;
  Time reference_stop;
};

struct AnimationStack : public PropertyTemplate<AnimationStackProperties> {
  AnimationStack() = default;
  AnimationStack(AnimationStackProperties const & _properties)
      : PropertyTemplate<AnimationStackProperties>(_properties) {}
};

#if 0
using AnimationStack = PropertyTemplate< AnimationStackProperties >;
#endif
using AnimationStacks = Container<AnimationStack, AnimationStackProperties>;

/*******************************************************************************
** AnimationLayer
*******************************************************************************/

struct AnimationLayerProperties {
  float weight;
  // bool mute;
  // bool solo;
  // bool lock;
  // Color color;
  // ??? blend_mode;
  // ??? rotation_accumulation_mode;
  // ??? blend_mode_bypass;
};

struct AnimationLayer : public PropertyTemplate<AnimationLayerProperties> {
  AnimationLayer() = default;
  AnimationLayer(AnimationLayerProperties const & _properties)
      : PropertyTemplate<AnimationLayerProperties>(_properties) {}
};

#if 0
using AnimationLayer = PropertyTemplate< AnimationLayerProperties >;
#endif
using AnimationLayers = Container<AnimationLayer, AnimationLayerProperties>;

/*******************************************************************************
** AnimationCurveNode
*******************************************************************************/

struct AnimationCurveNodeProperties {
  numerics::Vector3f d;
};

struct AnimationCurveNode
    : public PropertyTemplate<AnimationCurveNodeProperties> {
  AnimationCurveNode() = default;
  AnimationCurveNode(AnimationCurveNodeProperties const & _properties)
      : PropertyTemplate<AnimationCurveNodeProperties>(_properties) {}
};

#if 0
using AnimationCurveNode = PropertyTemplate< AnimationCurveNodeProperties >;
#endif
using AnimationCurveNodes =
    Container<AnimationCurveNode, AnimationCurveNodeProperties>;

/*******************************************************************************
** AnimationCurve
*******************************************************************************/

struct AnimationCurve : public ObjectBase {
  float default_value;
  core::UniqueArray<Time> key_times;
  core::UniqueArray<float> key_values;
};

using AnimationCurves = core::UniqueArray<AnimationCurve>;

/*******************************************************************************
** Connection
*******************************************************************************/

struct Connection {
  int64 parent_id;
  int64 child_id;
  String property_name;
};

using Connections = core::UniqueArray<Connection>;

/*******************************************************************************
** Scene
*******************************************************************************/

struct Scene {
  NodeAttributes node_attributes;
  Geometries geometries;
  Models models;
  Poses poses;
  Deformers deformers;
  Materials materials;
  AnimationStacks animation_stacks;
  AnimationLayers animation_layers;
  AnimationCurveNodes animation_curve_nodes;
  AnimationCurves animation_curves;

  Connections connections;
};

/*******************************************************************************
** parse_node_attribute
*******************************************************************************/

inline void parse_node_attribute_definition(Scene & _scene,
                                            Node const & _definition) {
  CORE_LIB_ASSERT(_definition.properties.Size() == 1u);
  CORE_LIB_ASSERT(_definition.properties[0].As<String>() == "NodeAttribute");
  // Count
  {
    CORE_LIB_ASSERT(0u < _definition.children.Size());
    auto & count = _definition.children[0u];
    CORE_LIB_ASSERT(count.name == "Count");
    CORE_LIB_ASSERT(count.properties.Size() == 1u);
    auto const node_attribute_count =
        count.properties[0u].As<Primitive>().As<int32>();
    _scene.node_attributes.values =
        core::make_unique_array<NodeAttribute>(node_attribute_count);
  }
  // PropertyTemplate
  NodeAttributeProperties node_attribute_template;
  if (1u < _definition.children.Size()) {
    auto & property_template = _definition.children[1u];
    CORE_LIB_ASSERT(property_template.name == "PropertyTemplate");
    {
      CORE_LIB_ASSERT(property_template.children.Size() == 1u);
      auto & properties_70 = property_template.children[0u];
      {
        CORE_LIB_ASSERT(properties_70.name == "Properties70");
        CORE_LIB_ASSERT(properties_70.children.Size() == 3u);
        parse(node_attribute_template.color, properties_70.children[0u],
              "Color");
        parse(node_attribute_template.size, properties_70.children[1u], "Size");
        parse(node_attribute_template.limb_length, properties_70.children[2u],
              "LimbLength");
      }
    }
  } else {
    node_attribute_template.color = numerics::Vector3f(0.8f, 0.8f, 0.8f);
    node_attribute_template.size = 33.3333f;
    node_attribute_template.limb_length = 100.f;
  }
  CORE_LIB_ASSERT(!_scene.node_attributes.property_template.HasValue());
  _scene.node_attributes.property_template = node_attribute_template;
}

inline void parse_node_attribute_object(Scene & /*_scene*/,
                                        NodeAttribute & _node_attribute,
                                        Node & _object) {
  CORE_LIB_ASSERT(_object.name == "NodeAttribute");
  CORE_LIB_ASSERT(_object.properties.Size() == 3u);
  _node_attribute.id = _object.properties[0u].As<Primitive>().As<int64>();
  _node_attribute.name = move(_object.properties[1u].As<String>());
  auto & type_name = _object.properties[2u].As<String>();
  if (type_name == "LimbNode") {
    for (auto current = 0u; current < _object.children.Size(); ++current) {
      auto & child = _object.children[current];
      if (child.name == "Properties70") {
        for (auto current_property = 0u;
             current_property < child.children.Size(); ++current_property) {
          auto & property = child.children[current_property];
          CORE_LIB_ASSERT(property.name == "P");
          CORE_LIB_ASSERT(2u < property.properties.Size());
          auto & name = property.properties[0u].As<String>();
          if (name == "Color") {
            parse(_node_attribute.properties.color, property, "Color");
          } else if (name == "Size") {
            parse(_node_attribute.properties.size, property, "Size");
          } else if (name == "LimbLength") {
            parse(_node_attribute.properties.limb_length, property,
                  "LimbLength");
          } else {
            LOG_FBX_SCENE(VERBOSE,
                          "Ignored NodeAttribute object property '%s'!\n",
                          cstr(name));
          }
        }
      } else if (child.name == "TypeFlags") {
      } else {
        LOG_FBX_SCENE(VERBOSE, "Ignored NodeAttribute object child '%s'!\n",
                      cstr(child.name));
      }
    }
  } else {
    LOG_FBX_SCENE(VERBOSE, "Ignored NodeAttribute object of type '%s'!\n",
                  cstr(type_name));
  }
}

/*******************************************************************************
** parse_geometry
*******************************************************************************/

inline void parse_geometry_color(GeometryProperties & _properties,
                                 Node const & _node) {
  CORE_LIB_ASSERT(_node.name == "P");
  CORE_LIB_ASSERT(_node.properties.Size() == 7u);
  CORE_LIB_ASSERT(_node.properties[0u].As<String>() == "Color");
  CORE_LIB_ASSERT(_node.properties[1u].As<String>() == "ColorRGB");
  CORE_LIB_ASSERT(_node.properties[2u].As<String>() == "Color");
  // TODO: Flag?
  convert(_properties.color, _node.properties[4u], _node.properties[5u],
          _node.properties[6u]);
}

inline void parse_geometry_bbox_min(GeometryProperties & _properties,
                                    Node const & _node) {
  CORE_LIB_ASSERT(_node.name == "P");
  CORE_LIB_ASSERT(_node.properties.Size() == 7u);
  CORE_LIB_ASSERT(_node.properties[0u].As<String>() == "BBoxMin");
  CORE_LIB_ASSERT(_node.properties[1u].As<String>() == "Vector3D");
  CORE_LIB_ASSERT(_node.properties[2u].As<String>() == "Vector");
  // TODO: Flag?
  convert(_properties.bounding_box.min, _node.properties[4u],
          _node.properties[5u], _node.properties[6u]);
}

inline void parse_geometry_bbox_max(GeometryProperties & _properties,
                                    Node const & _node) {
  CORE_LIB_ASSERT(_node.name == "P");
  CORE_LIB_ASSERT(_node.properties.Size() == 7u);
  CORE_LIB_ASSERT(_node.properties[0u].As<String>() == "BBoxMax");
  CORE_LIB_ASSERT(_node.properties[1u].As<String>() == "Vector3D");
  CORE_LIB_ASSERT(_node.properties[2u].As<String>() == "Vector");
  // TODO: Flag?
  convert(_properties.bounding_box.max, _node.properties[4u],
          _node.properties[5u], _node.properties[6u]);
}

inline void parse_geometry_definition(Scene & _scene,
                                      Node const & _definition) {
  CORE_LIB_ASSERT(_definition.properties.Size() == 1u);
  CORE_LIB_ASSERT(_definition.properties[0u].As<String>() == "Geometry");
  CORE_LIB_ASSERT(_definition.children.Size() == 2u);
  // Count
  {
    auto & count = _definition.children[0u];
    CORE_LIB_ASSERT(count.name == "Count");
    CORE_LIB_ASSERT(count.properties.Size() == 1u);
    auto const geometry_count =
        count.properties[0u].As<Primitive>().As<int32>();
    _scene.geometries.values =
        core::make_unique_array<Geometry>(geometry_count);
  }
  // PropertyTemplate
  {
    auto & property_template = _definition.children[1u];
    CORE_LIB_ASSERT(property_template.name == "PropertyTemplate");
    {
      CORE_LIB_ASSERT(property_template.children.Size() == 1u);
      auto & properties_70 = property_template.children[0u];
      {
        CORE_LIB_ASSERT(properties_70.name == "Properties70");
        CORE_LIB_ASSERT(properties_70.children.Size() == 6u);
        GeometryProperties geometry_template;
        parse_geometry_color(geometry_template, properties_70.children[0u]);
        parse_geometry_bbox_min(geometry_template, properties_70.children[1u]);
        parse_geometry_bbox_max(geometry_template, properties_70.children[2u]);
        // 'Primary Visibility'
        // 'Casts Shadows'
        // 'Receive Shadows'
        CORE_LIB_ASSERT(!_scene.geometries.property_template.HasValue());
        _scene.geometries.property_template = geometry_template;
      }
    }
  }
}

inline void parse_geometry_vertices(Geometry & _geometry, Node & _node) {
  CORE_LIB_ASSERT(_geometry.vertices.IsEmpty());
  CORE_LIB_ASSERT(_node.name == "Vertices");
  CORE_LIB_ASSERT(_node.properties.Size() == 1u);
  _geometry.vertices = move(_node.properties[0u].As<Array>().As<DoubleArray>());
}

inline void parse_geometry_indices(Geometry & _geometry, Node & _node) {
  CORE_LIB_ASSERT(_geometry.indices.IsEmpty());
  CORE_LIB_ASSERT(_node.name == "PolygonVertexIndex");
  CORE_LIB_ASSERT(_node.properties.Size() == 1u);
  _geometry.indices = move(_node.properties[0u].As<Array>().As<Int32Array>());
}

inline void
parse_layer_element_normal(LayerElementNormal & _layer_element_normal,
                           Node & _node) {
  LOG_FBX_SCENE(VERBOSE, "Parsing LayerElementNormal..\n");

  CORE_LIB_ASSERT(_node.name == "LayerElementNormal");
  CORE_LIB_ASSERT(_node.properties.Size() == 1u);

  // TODO: Verify that all members are parsed or initialize _layer_element_normal to default values!
  for (auto current = 0u; current < _node.children.Size(); ++current) {
    auto & child = _node.children[current];
    if (child.name == "Version") {
      LOG_FBX_SCENE(VERBOSE, "Parsing Version..\n");
      CORE_LIB_ASSERT(child.properties.Size() == 1u);
      _layer_element_normal.version =
          child.properties[0u].As<Primitive>().As<int32>();
    } else if (child.name == "Name") {
      LOG_FBX_SCENE(VERBOSE, "Parsing Name..\n");
      CORE_LIB_ASSERT(child.properties.Size() == 1u);
      // _layer_element_normal.name = child.properties[ 0u ].As< Primitive >().As< String >();
    } else if (child.name == "MappingInformationType") {
      LOG_FBX_SCENE(VERBOSE, "Parsing MappingInformationType..\n");
      CORE_LIB_ASSERT(child.properties.Size() == 1u);
      String & name = child.properties[0u].As<String>();
      if (name == "ByVertice") {
        _layer_element_normal.mapping_information_type =
            MappingInformationType::ByVertice;
      } else if (name == "ByPolygonVertex") {
        _layer_element_normal.mapping_information_type =
            MappingInformationType::ByPolygonVertex;
      } else {
        LOG_FBX_SCENE(WARNING, "Unknown MappingInformationType '%s'!\n",
                      cstr(name));
      }
    } else if (child.name == "ReferenceInformationType") {
      LOG_FBX_SCENE(VERBOSE, "Parsing ReferenceInformationType..\n");
      CORE_LIB_ASSERT(child.properties.Size() == 1u);
      String & name = child.properties[0u].As<String>();
      if (name == "Direct") {
        _layer_element_normal.reference_information_type =
            ReferenceInformationType::Direct;
      } else if (name == "IndexToDirect") {
        _layer_element_normal.reference_information_type =
            ReferenceInformationType::IndexToDirect;
      } else {
        LOG_FBX_SCENE(WARNING, "Unknown ReferenceInformationType '%s'!\n",
                      cstr(name));
      }
    } else if (child.name == "Normals") {
      LOG_FBX_SCENE(VERBOSE, "Parsing Normals..\n");
      CORE_LIB_ASSERT(child.properties.Size() == 1u);
      auto & normals = child.properties[0u].As<Array>().As<DoubleArray>();
      _layer_element_normal.normals = move(normals);
    } else if (child.name == "NormalsW") {
      LOG_FBX_SCENE(VERBOSE, "Parsing NormalsW..\n");
      CORE_LIB_ASSERT(child.properties.Size() == 1u);
      auto & normals_w = child.properties[0u].As<Array>().As<DoubleArray>();
      _layer_element_normal.normals_w = move(normals_w);
    } else {
      LOG_FBX_SCENE(VERBOSE, "Ignored LayerElementNormal object child '%s'!\n",
                    cstr(child.name));
    }
  }
}

inline void parse_layer_element_uv(LayerElementUV & _layer_element_uv,
                                   Node & _node) {
  LOG_FBX_SCENE(VERBOSE, "Parsing LayerElementUV..\n");

  CORE_LIB_ASSERT(_node.name == "LayerElementUV");
  CORE_LIB_ASSERT(_node.properties.Size() == 1u);

  // TODO: Verify that all members are parsed or initialize _layer_element_uv to default values!
  for (auto current = 0u; current < _node.children.Size(); ++current) {
    auto & child = _node.children[current];
    if (child.name == "Version") {
      LOG_FBX_SCENE(VERBOSE, "Parsing Version..\n");
      CORE_LIB_ASSERT(child.properties.Size() == 1u);
      _layer_element_uv.version =
          child.properties[0u].As<Primitive>().As<int32>();
    } else if (child.name == "Name") {
      LOG_FBX_SCENE(VERBOSE, "Parsing Name..\n");
      CORE_LIB_ASSERT(child.properties.Size() == 1u);
      // _layer_element_uv.name = child.properties[ 0u ].As< Primitive >().As< String >();
    } else if (child.name == "MappingInformationType") {
      LOG_FBX_SCENE(VERBOSE, "Parsing MappingInformationType..\n");
      CORE_LIB_ASSERT(child.properties.Size() == 1u);
      String & name = child.properties[0u].As<String>();
      if (name == "ByVertice") {
        _layer_element_uv.mapping_information_type =
            MappingInformationType::ByVertice;
      } else if (name == "ByPolygonVertex") {
        _layer_element_uv.mapping_information_type =
            MappingInformationType::ByPolygonVertex;
      } else {
        LOG_FBX_SCENE(WARNING, "Unknown MappingInformationType '%s'!\n",
                      cstr(name));
      }
    } else if (child.name == "ReferenceInformationType") {
      LOG_FBX_SCENE(VERBOSE, "Parsing ReferenceInformationType..\n");
      CORE_LIB_ASSERT(child.properties.Size() == 1u);
      String & name = child.properties[0u].As<String>();
      if (name == "Direct") {
        _layer_element_uv.reference_information_type =
            ReferenceInformationType::Direct;
      } else if (name == "IndexToDirect") {
        _layer_element_uv.reference_information_type =
            ReferenceInformationType::IndexToDirect;
      } else {
        LOG_FBX_SCENE(WARNING, "Unknown ReferenceInformationType '%s'!\n",
                      cstr(name));
      }
    } else if (child.name == "UV") {
      LOG_FBX_SCENE(VERBOSE, "Parsing UV..\n");
      CORE_LIB_ASSERT(child.properties.Size() == 1u);
      auto & uv_array = child.properties[0u].As<Array>().As<DoubleArray>();
      _layer_element_uv.uv = move(uv_array);
    } else if (child.name == "UVIndex") {
      LOG_FBX_SCENE(VERBOSE, "Parsing UVIndex..\n");
      CORE_LIB_ASSERT(child.properties.Size() == 1u);
      auto & uv_index_array = child.properties[0u].As<Array>().As<Int32Array>();
      _layer_element_uv.uv_index = move(uv_index_array);
    } else {
      LOG_FBX_SCENE(VERBOSE, "Ignored LayerElementUV object child '%s'!\n",
                    cstr(child.name));
    }
  }
}

inline void parse_geometry_object(Scene & /*_scene*/, Geometry & _geometry,
                                  Node & _object) {
  CORE_LIB_ASSERT(_object.name == "Geometry");
  CORE_LIB_ASSERT(_object.properties.Size() == 3u);
  _geometry.id = _object.properties[0u].As<Primitive>().As<int64>();
  _geometry.name = move(_object.properties[1u].As<String>());
  CORE_LIB_ASSERT(_object.properties[2u].As<String>() == "Mesh");
  // TODO: Optimize this double pass to count layer_element_uvs?
  auto layer_element_uv_index = 0u;
  for (auto current = 0u; current < _object.children.Size(); ++current) {
    auto & child = _object.children[current];
    if (child.name == "LayerElementUV") {
      ++layer_element_uv_index;
    }
  }

  _geometry.layer_element_uvs =
      core::make_unique_array<LayerElementUV>(layer_element_uv_index);

  layer_element_uv_index = 0u;
  for (auto current = 0u; current < _object.children.Size(); ++current) {
    auto & child = _object.children[current];
    if (child.name == "GeometryVersion") {
      CORE_LIB_ASSERT(child.properties.Size() == 1u);
      CORE_LIB_ASSERT(child.properties[0u].As<Primitive>().As<int32>() ==
                      I32(124));
    } else if (child.name == "Vertices") {
      parse_geometry_vertices(_geometry, child);
    } else if (child.name == "PolygonVertexIndex") {
      parse_geometry_indices(_geometry, child);
    } else if (child.name == "Edges") {
    } else if (child.name == "LayerElementNormal") {
      parse_layer_element_normal(_geometry.layer_element_normal, child);
    } else if (child.name == "LayerElementUV") {
      parse_layer_element_uv(
          _geometry.layer_element_uvs[layer_element_uv_index++], child);
    } else if (child.name == "LayerElementMaterial") {
    } else if (child.name == "Layer") {
    } else {
      LOG_FBX_SCENE(VERBOSE, "Ignored Geometry object child '%s'!\n",
                    cstr(child.name));
    }
  }

  CORE_LIB_ASSERT(layer_element_uv_index == _geometry.layer_element_uvs.Size());
}

/*******************************************************************************
** parse_model
*******************************************************************************/

inline void parse_model_translation(LclTranslation & _translation,
                                    Node const & _node) {
  CORE_LIB_ASSERT(_node.name == "P");
  CORE_LIB_ASSERT(_node.properties.Size() == 7u);
  CORE_LIB_ASSERT(_node.properties[0u].As<String>() == "Lcl Translation");
  CORE_LIB_ASSERT(_node.properties[1u].As<String>() == "Lcl Translation");
  // TODO: Flag?
  convert(_translation, _node.properties[4u], _node.properties[5u],
          _node.properties[6u]);
}

inline void parse_model_rotation(LclRotation & _rotation, Node const & _node) {
  CORE_LIB_ASSERT(_node.name == "P");
  CORE_LIB_ASSERT(_node.properties.Size() == 7u);
  CORE_LIB_ASSERT(_node.properties[0u].As<String>() == "Lcl Rotation");
  CORE_LIB_ASSERT(_node.properties[1u].As<String>() == "Lcl Rotation");
  // TODO: Flag?
  convert(_rotation, _node.properties[4u], _node.properties[5u],
          _node.properties[6u]);
}

inline void parse_model_scaling(LclScaling & _scaling, Node const & _node) {
  CORE_LIB_ASSERT(_node.name == "P");
  CORE_LIB_ASSERT(_node.properties.Size() == 7u);
  CORE_LIB_ASSERT(_node.properties[0u].As<String>() == "Lcl Scaling");
  CORE_LIB_ASSERT(_node.properties[1u].As<String>() == "Lcl Scaling");
  // TODO: Flag?
  convert(_scaling, _node.properties[4u], _node.properties[5u],
          _node.properties[6u]);
}

inline void parse_model_definition(Scene & _scene, Node & _definition) {
  CORE_LIB_ASSERT(_definition.properties.Size() == 1u);
  CORE_LIB_ASSERT(_definition.properties[0u].As<String>() == "Model");
  CORE_LIB_ASSERT(_definition.children.Size() == 2u);
  // Count
  {
    auto & count = _definition.children[0u];
    CORE_LIB_ASSERT(count.name == "Count");
    CORE_LIB_ASSERT(count.properties.Size() == 1u);
    auto const model_count = count.properties[0u].As<Primitive>().As<int32>();
    _scene.models.values = core::make_unique_array<Model>(model_count);
  }
  // PropertyTemplate
  {
    auto & property_template = _definition.children[1u];
    CORE_LIB_ASSERT(property_template.name == "PropertyTemplate");
    {
      CORE_LIB_ASSERT(property_template.children.Size() == 1u);
      auto & properties_70 = property_template.children[0u];
      {
        CORE_LIB_ASSERT(properties_70.name == "Properties70");
        ModelProperties model_template;
        for (auto child = 0u; child < properties_70.children.Size(); ++child) {
          auto & property = properties_70.children[child];
          CORE_LIB_ASSERT(property.name == "P");
          auto & property_name = property.properties[0u].As<String>();
          if (property_name == "Lcl Translation") {
            parse_model_translation(model_template.translation, property);
          } else if (property_name == "Lcl Rotation") {
            parse_model_rotation(model_template.rotation, property);
          } else if (property_name == "Lcl Scaling") {
            parse_model_scaling(model_template.scaling, property);
          } else {
            LOG_FBX_SCENE(VERBOSE, "Ignored Model definition property '%s'!\n",
                          cstr(property_name));
          }
        }
        // TODO: Assert all property default values are parsed!
        CORE_LIB_ASSERT(!_scene.models.property_template.HasValue());
        _scene.models.property_template = model_template;
      }
    }
  }
}

inline void parse_model_object(Scene & /*_scene*/, Model & _model,
                               Node & _object) {
  CORE_LIB_ASSERT(_object.name == "Model");
  CORE_LIB_ASSERT(_object.properties.Size() == 3u);
  _model.id = _object.properties[0u].As<Primitive>().As<int64>();
  _model.name = move(_object.properties[1u].As<String>());
  _model.type = move(_object.properties[2u].As<String>());
  // CORE_LIB_ASSERT( _model.type == "LimbNode" || _model.type == "Mesh" );
  // TODO: Differentiate based on type!
  for (auto current = 0u; current < _object.children.Size(); ++current) {
    auto & child = _object.children[current];
    if (child.name == "Version") {
      CORE_LIB_ASSERT(child.properties.Size() == 1u);
      CORE_LIB_ASSERT(child.properties[0u].As<Primitive>().As<int32>() == 232);
    } else if (child.name == "Properties70") {
      for (auto current_property = 0u; current_property < child.children.Size();
           ++current_property) {
        auto & property = child.children[current_property];
        CORE_LIB_ASSERT(property.name == "P");
        CORE_LIB_ASSERT(2u < property.properties.Size());
        auto const & name = property.properties[0u].As<String>();
        if (name == "Lcl Translation") {
          parse_model_translation(_model.properties.translation, property);
        } else if (name == "Lcl Rotation") {
          parse_model_rotation(_model.properties.rotation, property);
        } else if (name == "Lcl Scaling") {
          parse_model_scaling(_model.properties.scaling, property);
        } else {
          // LOG_FBX_SCENE( VERBOSE, "Ignored Model object property '%s'!\n", cstr( name ) );
        }
      }
    } else if (child.name == "Shading") {
    } else if (child.name == "Culling") {
    } else {
      LOG_FBX_SCENE(VERBOSE, "Ignored Model object child '%s'!\n",
                    cstr(child.name));
    }
  }
}

/*******************************************************************************
** parse_pose
*******************************************************************************/

inline void parse_pose_definition(Scene & _scene, Node & _definition) {
  CORE_LIB_ASSERT(_definition.properties.Size() == 1u);
  CORE_LIB_ASSERT(_definition.properties[0u].As<String>() == "Pose");
  CORE_LIB_ASSERT(_definition.children.Size() == 1u);
  // Count
  {
    auto & count = _definition.children[0u];
    CORE_LIB_ASSERT(count.name == "Count");
    CORE_LIB_ASSERT(count.properties.Size() == 1u);
    auto const pose_count = count.properties[0u].As<Primitive>().As<int32>();
    _scene.poses = core::make_unique_array<Pose>(pose_count);
  }
}

inline void parse_pose_object(Scene & /*_scene*/, Pose & _pose,
                              Node & _object) {
  CORE_LIB_ASSERT(_object.name == "Pose");
  CORE_LIB_ASSERT(_object.properties.Size() == 3u);
  _pose.id = _object.properties[0u].As<Primitive>().As<int64>();
  _pose.name = move(_object.properties[1u].As<String>());
  CORE_LIB_ASSERT(_object.properties[2u].As<String>() == "BindPose");
  auto pose_node_index = 0u;
  for (auto current = 0u; current < _object.children.Size(); ++current) {
    auto & child = _object.children[current];
    if (child.name == "Type") {
      CORE_LIB_ASSERT(child.properties.Size() == 1u);
      CORE_LIB_ASSERT(child.properties[0u].As<String>() == "BindPose");
    } else if (child.name == "Version") {
      CORE_LIB_ASSERT(child.properties.Size() == 1u);
      CORE_LIB_ASSERT(child.properties[0u].As<Primitive>().As<int32>() == 100);
    } else if (child.name == "NbPoseNodes") {
      CORE_LIB_ASSERT(child.properties.Size() == 1u);
      auto node_count = child.properties[0u].As<Primitive>().As<int32>();
      _pose.nodes = core::make_unique_array<PoseNode>(node_count);
      CORE_LIB_ASSERT(node_count != 0);
    } else if (child.name == "PoseNode") {
      CORE_LIB_ASSERT(_pose.nodes);
      CORE_LIB_ASSERT(pose_node_index < _pose.nodes.Size());
      auto & pose_node = _pose.nodes[pose_node_index++];
      CORE_LIB_ASSERT(child.children.Size() == 2u);
      // NodeId
      {
        auto & node_id = child.children[0u];
        CORE_LIB_ASSERT(node_id.name == "Node");
        CORE_LIB_ASSERT(node_id.properties.Size() == 1u);
        pose_node.id = node_id.properties[0u].As<Primitive>().As<int64>();
      }
      // Matrix
      {
        auto & matrix = child.children[1u];
        CORE_LIB_ASSERT(matrix.name == "Matrix");
        CORE_LIB_ASSERT(matrix.properties.Size() == 1u);
        auto & elements = matrix.properties[0u].As<Array>().As<DoubleArray>();
        parse_matrix(pose_node.matrix, elements);
      }
    } else {
      LOG_FBX_SCENE(VERBOSE, "Ignored Pose object child '%s'!\n",
                    cstr(child.name));
    }
  }
}

/*******************************************************************************
** parse_deformer
*******************************************************************************/

inline void parse_deformer_definition(Scene & _scene, Node & _definition) {
  CORE_LIB_ASSERT(_definition.properties.Size() == 1u);
  CORE_LIB_ASSERT(_definition.properties[0u].As<String>() == "Deformer");
  CORE_LIB_ASSERT(_definition.children.Size() == 1u);
  // Count
  {
    auto & count = _definition.children[0u];
    CORE_LIB_ASSERT(count.name == "Count");
    CORE_LIB_ASSERT(count.properties.Size() == 1u);
    auto const deformer_count =
        count.properties[0u].As<Primitive>().As<int32>();
    _scene.deformers = core::make_unique_array<Deformer>(
        deformer_count, in_place_t<Valueless>());
  }
}

inline void parse_deformer_object(Scene & /*_scene*/, Deformer & _deformer,
                                  Node & _object) {
  CORE_LIB_ASSERT(_object.name == "Deformer");
  CORE_LIB_ASSERT(_object.properties.Size() == 3u);
  auto const & id = _object.properties[0u].As<Primitive>().As<int64>();
  auto const & name = _object.properties[1u].As<String>();
  auto const & type = _object.properties[2u].As<String>();
  if (type == "Skin") {
    _deformer = Deformer(in_place_t<Skin>());
    Skin & skin = _deformer.As<Skin>();
    skin.id = id;
    skin.name = move(name);
    for (auto current = 0u; current < _object.children.Size(); ++current) {
      auto & child = _object.children[current];
      if (child.name == "Version") {
        CORE_LIB_ASSERT(child.properties.Size() == 1u);
        CORE_LIB_ASSERT(child.properties[0u].As<Primitive>().As<int32>() ==
                        101);
      } else if (child.name == "Link_DeformAcuracy") {
      } else {
        LOG_FBX_SCENE(VERBOSE, "Ignored Skin object child '%s'!\n",
                      cstr(child.name));
      }
    }
  } else if (type == "Cluster") {
    _deformer = Deformer(in_place_t<Cluster>());
    Cluster & cluster = _deformer.As<Cluster>();
    cluster.id = id;
    cluster.name = move(name);
    for (auto current = 0u; current < _object.children.Size(); ++current) {
      auto & child = _object.children[current];
      if (child.name == "Version") {
        CORE_LIB_ASSERT(child.properties.Size() == 1u);
        CORE_LIB_ASSERT(child.properties[0u].As<Primitive>().As<int32>() ==
                        100);
      } else if (child.name == "UserData") {
      } else if (child.name == "Indexes") {
        CORE_LIB_ASSERT(child.properties.Size() == 1u);
        auto & indices = child.properties[0u].As<Array>().As<Int32Array>();
        // Move the array, do not copy it!
        cluster.indices = move(indices);
      } else if (child.name == "Weights") {
        CORE_LIB_ASSERT(child.properties.Size() == 1u);
        auto & weights = child.properties[0u].As<Array>().As<DoubleArray>();
        // Can't move the array, as it's double and we need floats!
        cluster.weights = core::make_unique_array<float>(weights.Size());
        for (auto weight = 0u; weight < weights.Size(); ++weight) {
          cluster.weights[weight] = static_cast<float>(weights[weight]);
        }
      } else if (child.name == "Transform") {
        CORE_LIB_ASSERT(child.properties.Size() == 1u);
        auto & elements = child.properties[0u].As<Array>().As<DoubleArray>();
        parse_matrix(cluster.transform, elements);
      } else if (child.name == "TransformLink") {
        CORE_LIB_ASSERT(child.properties.Size() == 1u);
        auto & elements = child.properties[0u].As<Array>().As<DoubleArray>();
        parse_matrix(cluster.transform_link, elements);
      } else {
        LOG_FBX_SCENE(VERBOSE, "Ignored Cluster object child '%s'!\n",
                      cstr(child.name));
      }
    }
  } else {
    LOG_FBX_SCENE(VERBOSE, "Ignored Deformer object type '%s'!\n", cstr(type));
  }
}

/*******************************************************************************
** parse_material
*******************************************************************************/

inline void parse_material_definition(Scene & _scene, Node & _definition) {
  CORE_LIB_ASSERT(_definition.properties.Size() == 1u);
  CORE_LIB_ASSERT(_definition.properties[0u].As<String>() == "Material");
  CORE_LIB_ASSERT(_definition.children.Size() == 2u);
  // Count
  {
    auto & count = _definition.children[0u];
    CORE_LIB_ASSERT(count.name == "Count");
    CORE_LIB_ASSERT(count.properties.Size() == 1u);
    auto const material_count =
        count.properties[0u].As<Primitive>().As<int32>();
    _scene.materials.values = core::make_unique_array<Material>(material_count);
  }
  // PropertyTemplate
  {
    auto & property_template = _definition.children[1u];
    CORE_LIB_ASSERT(property_template.name == "PropertyTemplate");
    {
      CORE_LIB_ASSERT(property_template.children.Size() == 1u);
      auto & properties_70 = property_template.children[0u];
      {
        CORE_LIB_ASSERT(properties_70.name == "Properties70");
        MaterialProperties material_template;
        auto & properties = properties_70.children;
        for (auto current = 0u; current < properties_70.children.Size();
             ++current) {
          auto & property = properties_70.children[current];
          auto & name = property.properties[0u].As<String>();
          // TODO: Passing the property name is not necessary here..
          // TODO: Optimize this else-if string comparison!
          if (name == "ShadingModel") {
            parse(material_template.shading_model, property, "ShadingModel");
          } else if (name == "EmissiveColor") {
            parse(material_template.emissive_color, property, "EmissiveColor");
          } else if (name == "EmissiveFactor") {
            convert(material_template.emissive_factor, property,
                    "EmissiveFactor");
          } else if (name == "AmbientColor") {
            parse(material_template.ambient_color, property, "AmbientColor");
          } else if (name == "AmbientFactor") {
            convert(material_template.ambient_factor, property,
                    "AmbientFactor");
          } else if (name == "DiffuseColor") {
            parse(material_template.diffuse_color, property, "DiffuseColor");
          } else if (name == "DiffuseFactor") {
            convert(material_template.diffuse_factor, property,
                    "DiffuseFactor");
          } else if (name == "MultiLayer") {
          } else if (name == "Bump") {
          } else if (name == "BumpFactor") {
          } else if (name == "NormalMap") {
          } else if (name == "TransparentColor") {
          } else if (name == "TransparencyFactor") {
          } else if (name == "DisplacementColor") {
          } else if (name == "DisplacementFactor") {
          } else if (name == "VectorDisplacementColor") {
          } else if (name == "VectorDisplacementFactor") {
          } else if (name == "SpecularColor") {
          } else if (name == "SpecularFactor") {
          } else if (name == "Opacity") {
          } else if (name == "Shininess") {
          } else if (name == "ShininessExponent") {
          } else if (name == "ReflectionColor") {
          } else if (name == "ReflectionFactor") {
          } else {
            LOG_FBX_SCENE(WARNING, "Unknown Material property '%s'!\n",
                          cstr(name));
          }
        }
        CORE_LIB_ASSERT(!_scene.materials.property_template.HasValue());
        _scene.materials.property_template =
            core::Optional<MaterialProperties>(move(material_template));
      }
    }
  }
}

inline void parse_material_object(Scene & /*_scene*/, Material & _material,
                                  Node & _object) {
  CORE_LIB_ASSERT(_object.name == "Material");
  CORE_LIB_ASSERT(_object.properties.Size() == 3u);
  _material.id = _object.properties[0u].As<Primitive>().As<int64>();
  _material.name = move(_object.properties[1u].As<String>());
  CORE_LIB_ASSERT(_object.properties[2u].As<String>() == "");
  for (auto current = 0u; current < _object.children.Size(); ++current) {
    auto & child = _object.children[current];
    if (child.name == "Version") {
      CORE_LIB_ASSERT(child.properties.Size() == 1u);
      CORE_LIB_ASSERT(child.properties[0u].As<Primitive>().As<int32>() == 102);
    } else if (child.name == "Properties70") {
      for (auto current_property = 0u; current_property < child.children.Size();
           ++current_property) {
        auto & property = child.children[current_property];
        CORE_LIB_ASSERT(property.name == "P");
        CORE_LIB_ASSERT(2u < property.properties.Size());
        auto const & name = property.properties[0u].As<String>();
        if (name == "ShadingModel") {
          _material.properties.shading_model =
              move(property.properties[1u].As<String>());
        } else {
          LOG_FBX_SCENE(VERBOSE, "Ignored Material object property '%s'!\n",
                        cstr(name));
        }
      }
    } else {
      LOG_FBX_SCENE(VERBOSE, "Ignored Material object child '%s'!\n",
                    cstr(child.name));
    }
  }
}

/*******************************************************************************
** parse_animation_stack
*******************************************************************************/

inline void parse_animation_stack_definition(Scene & _scene,
                                             Node const & _definition) {
  CORE_LIB_ASSERT(_definition.properties.Size() == 1u);
  CORE_LIB_ASSERT(_definition.properties[0u].As<String>() == "AnimationStack");
  CORE_LIB_ASSERT(_definition.children.Size() == 2u);
  // Count
  {
    auto & count = _definition.children[0u];
    CORE_LIB_ASSERT(count.name == "Count");
    CORE_LIB_ASSERT(count.properties.Size() == 1u);
    auto const animation_stack_count =
        count.properties[0u].As<Primitive>().As<int32>();
    _scene.animation_stacks.values =
        core::make_unique_array<AnimationStack>(animation_stack_count);
  }
  // PropertyTemplate
  {
    auto & property_template = _definition.children[1u];
    CORE_LIB_ASSERT(property_template.name == "PropertyTemplate");
    {
      CORE_LIB_ASSERT(property_template.children.Size() == 1u);
      auto & properties_70 = property_template.children[0u];
      {
        CORE_LIB_ASSERT(properties_70.name == "Properties70");
        CORE_LIB_ASSERT(properties_70.children.Size() == 5u);
        AnimationStackProperties animation_stack_template;
        // 'Description'
        parse(animation_stack_template.local_start, properties_70.children[1u],
              "LocalStart");
        parse(animation_stack_template.local_stop, properties_70.children[2u],
              "LocalStop");
        parse(animation_stack_template.reference_start,
              properties_70.children[3u], "ReferenceStart");
        parse(animation_stack_template.reference_stop,
              properties_70.children[4u], "ReferenceStop");
        CORE_LIB_ASSERT(!_scene.animation_stacks.property_template.HasValue());
        _scene.animation_stacks.property_template = animation_stack_template;
      }
    }
  }
}

inline void parse_animation_stack_object(Scene & /*_scene*/,
                                         AnimationStack & _animation_stack,
                                         Node & _object) {
  CORE_LIB_ASSERT(_object.name == "AnimationStack");
  CORE_LIB_ASSERT(_object.properties.Size() == 3u);
  _animation_stack.id = _object.properties[0u].As<Primitive>().As<int64>();
  _animation_stack.name = move(_object.properties[1u].As<String>());
  CORE_LIB_ASSERT(_object.properties[2u].As<String>() == "");
  for (auto current = 0u; current < _object.children.Size(); ++current) {
    auto & child = _object.children[current];
    if (child.name == "Properties70") {
      for (auto current_property = 0u; current_property < child.children.Size();
           ++current_property) {
        auto & property = child.children[current_property];
        CORE_LIB_ASSERT(property.name == "P");
        CORE_LIB_ASSERT(2u < property.properties.Size());
        auto & name = property.properties[0u].As<String>();
        if (name == "LocalStart") {
          parse(_animation_stack.properties.local_start, property,
                "LocalStart");
        } else if (name == "LocalStop") {
          parse(_animation_stack.properties.local_stop, property, "LocalStop");
        } else if (name == "ReferenceStart") {
          parse(_animation_stack.properties.reference_start, property,
                "ReferenceStart");
        } else if (name == "ReferenceStop") {
          parse(_animation_stack.properties.reference_stop, property,
                "ReferenceStop");
        } else if (name == "Description") {
        } else {
          LOG_FBX_SCENE(VERBOSE,
                        "Ignored AnimationStack object property '%s'!\n",
                        cstr(name));
        }
      }
    } else {
      LOG_FBX_SCENE(VERBOSE, "Ignored AnimationStack object child '%s'!\n",
                    cstr(child.name));
    }
  }
}

/*******************************************************************************
** parse_animation_layer
*******************************************************************************/

inline void parse_animation_layer_definition(Scene & _scene,
                                             Node const & _definition) {
  CORE_LIB_ASSERT(_definition.properties.Size() == 1u);
  CORE_LIB_ASSERT(_definition.properties[0u].As<String>() == "AnimationLayer");
  CORE_LIB_ASSERT(_definition.children.Size() == 2u);
  // Count
  {
    auto & count = _definition.children[0u];
    CORE_LIB_ASSERT(count.name == "Count");
    CORE_LIB_ASSERT(count.properties.Size() == 1u);
    auto const animation_layer_count =
        count.properties[0u].As<Primitive>().As<int32>();
    _scene.animation_layers.values =
        core::make_unique_array<AnimationLayer>(animation_layer_count);
  }
  // PropertyTemplate
  {
    auto & property_template = _definition.children[1u];
    CORE_LIB_ASSERT(property_template.name == "PropertyTemplate");
    {
      CORE_LIB_ASSERT(property_template.children.Size() == 1u);
      auto & properties_70 = property_template.children[0u];
      {
        CORE_LIB_ASSERT(properties_70.name == "Properties70");
        CORE_LIB_ASSERT(properties_70.children.Size() == 9u);
        AnimationLayerProperties animation_layer_template;
        convert(animation_layer_template.weight, properties_70.children[0u],
                "Weight");
        // 'Mute'
        // 'Solo'
        // 'Lock'
        // 'Color'
        // 'BlendMode'
        // 'RotationAccumulationMode'
        // 'ScaleAccumulationMode'
        // 'BlendModeBypass'
        CORE_LIB_ASSERT(!_scene.animation_layers.property_template.HasValue());
        _scene.animation_layers.property_template = animation_layer_template;
      }
    }
  }
}

inline void parse_animation_layer_object(Scene & /*_scene*/,
                                         AnimationLayer & _animation_layer,
                                         Node & _object) {
  CORE_LIB_ASSERT(_object.name == "AnimationLayer");
  CORE_LIB_ASSERT(_object.properties.Size() == 3u);
  _animation_layer.id = _object.properties[0u].As<Primitive>().As<int64>();
  _animation_layer.name = move(_object.properties[1u].As<String>());
  CORE_LIB_ASSERT(_object.properties[2u].As<String>() == "");
  for (auto current = 0u; current < _object.children.Size(); ++current) {
    auto & child = _object.children[current];
    if (child.name == "Properties70") {
      for (auto current_property = 0u; current_property < child.children.Size();
           ++current_property) {
        auto & property = child.children[current_property];
        CORE_LIB_ASSERT(property.name == "P");
        CORE_LIB_ASSERT(2u < property.properties.Size());
        auto & name = property.properties[0u].As<String>();
        if (name == "Weight") {
          convert(_animation_layer.properties.weight, property, "Weight");
        } else {
          LOG_FBX_SCENE(VERBOSE,
                        "Ignored AnimationLayer object property '%s'!\n",
                        cstr(name));
        }
      }
    } else {
      LOG_FBX_SCENE(VERBOSE, "Ignored AnimationLayer object child '%s'!\n",
                    cstr(child.name));
    }
  }
}

/*******************************************************************************
** parse_animation_curve_node
*******************************************************************************/

inline void parse_animation_curve_node_definition(Scene & _scene,
                                                  Node const & _definition) {
  CORE_LIB_ASSERT(_definition.properties.Size() == 1u);
  CORE_LIB_ASSERT(_definition.properties[0u].As<String>() ==
                  "AnimationCurveNode");
  CORE_LIB_ASSERT(_definition.children.Size() == 2u);
  // Count
  {
    auto & count = _definition.children[0u];
    CORE_LIB_ASSERT(count.name == "Count");
    CORE_LIB_ASSERT(count.properties.Size() == 1u);
    auto const animation_curve_node_count =
        count.properties[0u].As<Primitive>().As<int32>();
    _scene.animation_curve_nodes.values =
        core::make_unique_array<AnimationCurveNode>(animation_curve_node_count);
  }
  // PropertyTemplate
  {
    CORE_LIB_ASSERT(_definition.children[1u].name == "PropertyTemplate");
    CORE_LIB_ASSERT(_definition.children[1u].children.Size() == 1u);
    CORE_LIB_ASSERT(_definition.children[1u].children[0u].name ==
                    "Properties70");
    CORE_LIB_ASSERT(_definition.children[1u].children[0u].children.Size() ==
                    1u);
    AnimationCurveNodeProperties animation_curve_node_template;
    // TODO: ???
    CORE_LIB_ASSERT(!_scene.animation_curve_nodes.property_template.HasValue());
    _scene.animation_curve_nodes.property_template =
        animation_curve_node_template;
  }
}

inline void
parse_animation_curve_node_object(Scene & /*_scene*/,
                                  AnimationCurveNode & _animation_curve_node,
                                  Node & _object) {
  CORE_LIB_ASSERT(_object.name == "AnimationCurveNode");
  CORE_LIB_ASSERT(_object.properties.Size() == 3u);
  _animation_curve_node.id = _object.properties[0u].As<Primitive>().As<int64>();
  _animation_curve_node.name = move(_object.properties[1u].As<String>());
  CORE_LIB_ASSERT(_object.properties[2u].As<String>() == "");
  for (auto current = 0u; current < _object.children.Size(); ++current) {
    auto & child = _object.children[current];
    if (child.name == "Properties70") {
      for (auto current_property = 0u; current_property < child.children.Size();
           ++current_property) {
        auto & property = child.children[current_property];
        CORE_LIB_ASSERT(property.name == "P");
        CORE_LIB_ASSERT(2u < property.properties.Size());
        auto & name = property.properties[0u].As<String>();
        if (name == "d|X") {
          convert(_animation_curve_node.properties.d.x, property, "d|X");
        } else if (name == "d|Y") {
          convert(_animation_curve_node.properties.d.y, property, "d|Y");
        } else if (name == "d|Z") {
          convert(_animation_curve_node.properties.d.z, property, "d|Z");
        } else {
          LOG_FBX_SCENE(VERBOSE,
                        "Ignored AnimationCurveNode object property '%s'!\n",
                        cstr(name));
        }
      }
    } else {
      LOG_FBX_SCENE(VERBOSE, "Ignored AnimationCurveNode object child '%s'!\n",
                    cstr(child.name));
    }
  }
}

/*******************************************************************************
** parse_animation_curve
*******************************************************************************/

inline void parse_animation_curve_definition(Scene & _scene,
                                             Node & _definition) {
  CORE_LIB_ASSERT(_definition.properties.Size() == 1u);
  CORE_LIB_ASSERT(_definition.properties[0u].As<String>() == "AnimationCurve");
  CORE_LIB_ASSERT(_definition.children.Size() == 1u);
  // Count
  {
    auto & count = _definition.children[0u];
    CORE_LIB_ASSERT(count.name == "Count");
    CORE_LIB_ASSERT(count.properties.Size() == 1u);
    auto const animation_curve_count =
        count.properties[0u].As<Primitive>().As<int32>();
    _scene.animation_curves =
        core::make_unique_array<AnimationCurve>(animation_curve_count);
  }
}

inline void parse_animation_curve_object(Scene & /*_scene*/,
                                         AnimationCurve & _animation_curve,
                                         Node & _object) {
  CORE_LIB_ASSERT(_object.name == "AnimationCurve");
  CORE_LIB_ASSERT(_object.properties.Size() == 3u);
  _animation_curve.id = _object.properties[0u].As<Primitive>().As<int64>();
  _animation_curve.name = move(_object.properties[1u].As<String>());
  CORE_LIB_ASSERT(_object.properties[2u].As<String>() == "");
  for (auto current = 0u; current < _object.children.Size(); ++current) {
    auto & child = _object.children[current];
    CORE_LIB_ASSERT(child.properties.Size() == 1u);
    auto & property = child.properties[0u];
    if (child.name == "Default") {
      convert(_animation_curve.default_value, property);
    } else if (child.name == "KeyVer") {
      // TODO: Version meaning?
      auto const version = child.properties[0u].As<Primitive>().As<int32>();
      CORE_LIB_ASSERT(version == 4008 || version == 4009);
    } else if (child.name == "KeyTime") {
      // TODO: This conversion could be done in place without an extra allocation!
      auto & fbx_key_times = property.As<Array>().As<Int64Array>();
      auto key_times = core::make_unique_array<Time>(fbx_key_times.Size());
      for (auto key = 0u; key < fbx_key_times.Size(); ++key) {
        convert(key_times[key], fbx_key_times[key]);
      }
      _animation_curve.key_times = move(key_times);
    } else if (child.name == "KeyValueFloat") {
      // Move instead of copy!
      _animation_curve.key_values = move(property.As<Array>().As<FloatArray>());
    } else if (child.name == "KeyAttrFlags") {
      // TODO: ???
    } else if (child.name == "KeyAttrDataFloat") {
      // TODO: ???
    } else if (child.name == "KeyAttrRefCount") {
      // TODO: ???
    } else {
      LOG_FBX_SCENE(VERBOSE, "Ignored AnimationCurve object child '%s'!\n",
                    cstr(child.name));
    }
  }
}

/*******************************************************************************
** parse_definitions
*******************************************************************************/

inline void parse_definitions(Scene & _scene, Node & _definitions) {
  CORE_LIB_ASSERT(_definitions.name == "Definitions");
  auto & definitions = _definitions.children;
  for (auto current = 0u; current < definitions.Size(); ++current) {
    auto & definition = definitions[current];
    if (definition.name == "ObjectType") {
      CORE_LIB_ASSERT(definition.properties.Size() == 1u);
      CORE_LIB_ASSERT(definition.properties[0u].Is<String>());
      auto const & type = definition.properties[0u].As<String>();
      if (type == "NodeAttribute") {
        parse_node_attribute_definition(_scene, definition);
      } else if (type == "Geometry") {
        parse_geometry_definition(_scene, definition);
      } else if (type == "Model") {
        parse_model_definition(_scene, definition);
      } else if (type == "Pose") {
        parse_pose_definition(_scene, definition);
      } else if (type == "Deformer") {
        parse_deformer_definition(_scene, definition);
      } else if (type == "Material") {
        parse_material_definition(_scene, definition);
      } else if (type == "AnimationStack") {
        parse_animation_stack_definition(_scene, definition);
      } else if (type == "AnimationLayer") {
        parse_animation_layer_definition(_scene, definition);
      } else if (type == "AnimationCurveNode") {
        parse_animation_curve_node_definition(_scene, definition);
      } else if (type == "AnimationCurve") {
        parse_animation_curve_definition(_scene, definition);
      } else {
        LOG_FBX_SCENE(VERBOSE, "Ignored ObjectType definition '%s'!\n",
                      cstr(type));
      }
    } else {
      LOG_FBX_SCENE(VERBOSE, "Ignored definition '%s'!\n",
                    cstr(definition.name));
    }
  }
}

/*******************************************************************************
** parse_objects
*******************************************************************************/

inline void parse_objects(Scene & _scene, Node & _objects) {
  auto node_attribute_index = 0u;
  auto geometry_index = 0u;
  auto model_index = 0u;
  auto pose_index = 0u;
  auto deformer_index = 0u;
  auto material_index = 0u;
  auto animation_stack_index = 0u;
  auto animation_layer_index = 0u;
  auto animation_curve_node_index = 0u;
  auto animation_curve_index = 0u;
  CORE_LIB_ASSERT(_objects.name == "Objects");
  auto & objects = _objects.children;
  for (auto current = 0u; current < objects.Size(); ++current) {
    auto & object = objects[current];
    if (object.name == "NodeAttribute") {
      CORE_LIB_ASSERT(node_attribute_index < _scene.node_attributes.Size());
      auto & node_attribute = _scene.node_attributes[node_attribute_index++];
      CORE_LIB_ASSERT(_scene.node_attributes.property_template.HasValue());
      node_attribute = _scene.node_attributes.property_template.Value();
      parse_node_attribute_object(_scene, node_attribute, object);
    } else if (object.name == "Geometry") {
      CORE_LIB_ASSERT(geometry_index < _scene.geometries.Size());
      auto & geometry = _scene.geometries[geometry_index++];
      CORE_LIB_ASSERT(_scene.geometries.property_template.HasValue());
      geometry = _scene.geometries.property_template.Value();
      parse_geometry_object(_scene, geometry, object);
    } else if (object.name == "Model") {
      CORE_LIB_ASSERT(model_index < _scene.models.Size());
      auto & model = _scene.models[model_index++];
      CORE_LIB_ASSERT(_scene.models.property_template.HasValue());
      model = _scene.models.property_template.Value();
      parse_model_object(_scene, model, object);
    } else if (object.name == "Pose") {
      CORE_LIB_ASSERT(pose_index < _scene.poses.Size());
      auto & pose = _scene.poses[pose_index++];
      parse_pose_object(_scene, pose, object);
    } else if (object.name == "Deformer") {
      CORE_LIB_ASSERT(deformer_index < _scene.deformers.Size());
      auto & deformer = _scene.deformers[deformer_index++];
      parse_deformer_object(_scene, deformer, object);
    } else if (object.name == "Material") {
      CORE_LIB_ASSERT(material_index < _scene.materials.Size());
      auto & material = _scene.materials[material_index++];
      CORE_LIB_ASSERT(_scene.materials.property_template.HasValue());
      material = _scene.materials.property_template.Value();
      parse_material_object(_scene, material, object);
    } else if (object.name == "AnimationStack") {
      CORE_LIB_ASSERT(animation_stack_index < _scene.animation_stacks.Size());
      auto & animation_stack = _scene.animation_stacks[animation_stack_index++];
      CORE_LIB_ASSERT(_scene.animation_stacks.property_template.HasValue());
      animation_stack = _scene.animation_stacks.property_template.Value();
      parse_animation_stack_object(_scene, animation_stack, object);
    } else if (object.name == "AnimationLayer") {
      CORE_LIB_ASSERT(animation_layer_index < _scene.animation_layers.Size());
      auto & animation_layer = _scene.animation_layers[animation_layer_index++];
      CORE_LIB_ASSERT(_scene.animation_layers.property_template.HasValue());
      animation_layer = _scene.animation_layers.property_template.Value();
      parse_animation_layer_object(_scene, animation_layer, object);
    } else if (object.name == "AnimationCurveNode") {
      CORE_LIB_ASSERT(animation_curve_node_index <
                      _scene.animation_curve_nodes.Size());
      auto & animation_curve_node =
          _scene.animation_curve_nodes[animation_curve_node_index++];
      CORE_LIB_ASSERT(
          _scene.animation_curve_nodes.property_template.HasValue());
      animation_curve_node =
          _scene.animation_curve_nodes.property_template.Value();
      parse_animation_curve_node_object(_scene, animation_curve_node, object);
    } else if (object.name == "AnimationCurve") {
      CORE_LIB_ASSERT(animation_curve_index < _scene.animation_curves.Size());
      auto & animation_curve = _scene.animation_curves[animation_curve_index++];
      parse_animation_curve_object(_scene, animation_curve, object);
    } else {
      LOG_FBX_SCENE(VERBOSE, "Ignored Object '%s'!\n", cstr(object.name));
    }
  }
  CORE_LIB_ASSERT(node_attribute_index == _scene.node_attributes.Size());
  CORE_LIB_ASSERT(geometry_index == _scene.geometries.Size());
  CORE_LIB_ASSERT(model_index == _scene.models.Size());
  CORE_LIB_ASSERT(pose_index == _scene.poses.Size());
  CORE_LIB_ASSERT(deformer_index == _scene.deformers.Size());
  CORE_LIB_ASSERT(material_index == _scene.materials.Size());
  CORE_LIB_ASSERT(animation_stack_index == _scene.animation_stacks.Size());
  CORE_LIB_ASSERT(animation_layer_index == _scene.animation_layers.Size());
  CORE_LIB_ASSERT(animation_curve_node_index ==
                  _scene.animation_curve_nodes.Size());
  CORE_LIB_ASSERT(animation_curve_index == _scene.animation_curves.Size());
}

/*******************************************************************************
** parse_connections
*******************************************************************************/

inline void parse_connections(Scene & _scene, Node & _connections) {
  CORE_LIB_ASSERT(_connections.name == "Connections");
  auto & connections = _connections.children;
  auto connection_index = 0u;
  _scene.connections = core::make_unique_array<Connection>(connections.Size());
  for (auto current = 0u; current < connections.Size(); ++current) {
    auto & connection = _scene.connections[connection_index++];
    if (connections[current].name == "C") {
      auto & properties = connections[current].properties;
      CORE_LIB_ASSERT(3u <= properties.Size());
      auto const & type = properties[0u].As<String>();
      if (type == "OO") {
        CORE_LIB_ASSERT(properties.Size() == 3u);
        connection.child_id = properties[1u].As<Primitive>().As<int64>();
        connection.parent_id = properties[2u].As<Primitive>().As<int64>();
      } else if (type == "OP") {
        CORE_LIB_ASSERT(properties.Size() == 4u);
        connection.child_id = properties[1u].As<Primitive>().As<int64>();
        connection.parent_id = properties[2u].As<Primitive>().As<int64>();
        connection.property_name = move(properties[3u].As<String>());
      } else {
        LOG_FBX_SCENE(VERBOSE, "Ignored Connection type '%s'!\n", cstr(type));
      }
    } else {
      LOG_FBX_SCENE(VERBOSE, "Ignored Connection '%s'!\n",
                    cstr(connections[current].name));
    }
  }
  CORE_LIB_ASSERT(connection_index == _scene.connections.Size());
}

/*******************************************************************************
** parse
*******************************************************************************/

inline Scene parse(Node & _root) {
  Scene scene;
  auto & root_nodes = _root.children;
  for (auto current = 0u; current < root_nodes.Size(); ++current) {
    auto & node = root_nodes[current];
    if (node.name == "Definitions") {
      parse_definitions(scene, node);
    } else if (node.name == "Objects") {
      parse_objects(scene, node);
    } else if (node.name == "Connections") {
      parse_connections(scene, node);
    } else {
      LOG_FBX_SCENE(VERBOSE, "Ignored Root node '%s'!\n", cstr(node.name));
    }
  }
  return scene;
}

inline core::Optional<Scene> load(core::fs::FilePath const & _file_path) {
  if (auto data = io::load(_file_path)) {
    return core::Optional<Scene>(parse(data->root));
  }
  return core::nullopt;
}

}}}

#undef LOG_FBX_SCENE

#endif
