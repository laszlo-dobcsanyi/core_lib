#ifndef _GRAPHICS_RESOURCE_FBX_CONVERT_HPP_
#define _GRAPHICS_RESOURCE_FBX_CONVERT_HPP_

#include "graphics/resource/model_data.hpp"
#include "graphics/resource/model_data_builder.hpp"

#include "graphics/resource/fbx/converter/base.hpp"
#include "graphics/resource/fbx/converter/to_string.hpp"
#include "graphics/resource/fbx/converter/object_map.hpp"
#include "graphics/resource/fbx/converter/skeleton_builder.hpp"
#include "graphics/resource/fbx/converter/converter.hpp"
#include "graphics/resource/fbx/converter/children.hpp"
#include "graphics/resource/fbx/converter/component_merger.hpp"

////////////////////////////////////////////////////////////////////////////////
// x converter
////////////////////////////////////////////////////////////////////////////////

namespace graphics { namespace fbx { namespace converter {

/*******************************************************************************
** convert
*******************************************************************************/

inline void
convert(Converter & _converter,
        resource::PrimitiveBuilder<resource::AnimationValue> & _builder,
        scene::AnimationCurveNode const & _curve_node) {
  scene::AnimationCurve const * x_curve = nullptr;
  scene::AnimationCurve const * y_curve = nullptr;
  scene::AnimationCurve const * z_curve = nullptr;
  Children children(_curve_node.id, _converter);
  for (Binding & binding : children) {
    LOG_FBX_CONVERTER(VERBOSE, "Child: '%s' ('%s')\n",
                      cstr(to_string(binding.object)), cstr(binding.property));
    if (binding.object.Is<scene::AnimationCurve *>()) {
      scene::AnimationCurve * animation_curve =
          binding.object.As<scene::AnimationCurve *>();
      if (binding.property == "d|X") {
        x_curve = animation_curve;
      } else if (binding.property == "d|Y") {
        y_curve = animation_curve;
      } else if (binding.property == "d|Z") {
        z_curve = animation_curve;
      } else {
        LOG_FBX_CONVERTER(WARNING, "Ignored Animation curve '%s'!\n",
                          cstr(binding.property));
      }
    } else {
      LOG_FBX_CONVERTER(WARNING, "Ignored Animation curve child!\n");
    }
  }
  ComponentMerger merger(x_curve, y_curve, z_curve);
  while (!merger.Finished()) {
    auto pose = merger.Next();
    auto & animation_value = _builder.builder.Push();
    animation_value.time = pose.time;
    animation_value.value = pose.value;
  }
}

inline void convert(Converter & _converter,
                    resource::AnimationChannelBuilder & _channel_builder,
                    scene::AnimationCurveNode const * _translation_curve,
                    scene::AnimationCurveNode const * _rotation_curve,
                    scene::AnimationCurveNode const * _scale_curve) {
  if (_translation_curve) {
    LOG_FBX_CONVERTER(VERBOSE, "Converting translation curve..\n");
    convert(_converter, _channel_builder.translation, *_translation_curve);
  }
  if (_rotation_curve) {
    LOG_FBX_CONVERTER(VERBOSE, "Converting rotation curve..\n");
    convert(_converter, _channel_builder.rotation, *_rotation_curve);
  }
  if (_scale_curve) {
    LOG_FBX_CONVERTER(VERBOSE, "Converting scale curve..\n");
    convert(_converter, _channel_builder.scale, *_scale_curve);
  }
}

/*******************************************************************************
** convert ( LimbNode )
*******************************************************************************/

inline void convert(scene::Model & _limb_node, Converter & _converter,
                    BoneBuilder * _parent) {
  CORE_LIB_ASSERT(_limb_node.type == "LimbNode");
  LOG_FBX_CONVERTER(VERBOSE, "Converting LimbNode '%s'..\n",
                    cstr(_limb_node.name));

  BoneBuilder & bone_builder =
      _converter.skeleton_builder.AddBoneBuilder(_limb_node, _parent);
  bone_builder.bone->name = _limb_node.name;
  bone_builder.bone->translation = _limb_node.properties.translation;
  bone_builder.bone->rotation = _limb_node.properties.rotation;
  bone_builder.bone->scale = _limb_node.properties.scaling;

  resource::AnimationChannelBuilder * channel_builder = nullptr;
  resource::AnimatedModelDataBuilder & animated_model_data_builder =
      _converter.model_data_builder.As<resource::AnimatedModelDataBuilder>();
  if (!animated_model_data_builder.animation_data_builders.IsEmpty())
    channel_builder =
        &animated_model_data_builder.animation_data_builders.data[0u]
             .channels.Push();
  scene::AnimationCurveNode const * translation_curve = nullptr;
  scene::AnimationCurveNode const * rotation_curve = nullptr;
  scene::AnimationCurveNode const * scale_curve = nullptr;
  Children children(_limb_node.id, _converter);
  for (Binding & binding : children) {
    LOG_FBX_CONVERTER(VERBOSE, "Child: '%s' ('%s')\n",
                      cstr(to_string(binding.object)), cstr(binding.property));
    if (binding.object.Is<scene::AnimationCurveNode *>()) {
      scene::AnimationCurveNode * curve_node =
          binding.object.As<scene::AnimationCurveNode *>();
      if (binding.property == "Lcl Translation") {
        CORE_LIB_ASSERT(!translation_curve);
        translation_curve = curve_node;
      } else if (binding.property == "Lcl Rotation") {
        CORE_LIB_ASSERT(!rotation_curve);
        rotation_curve = curve_node;
      } else if (binding.property == "Lcl Scale" ||
                 binding.property == "Lcl Scaling") {
        CORE_LIB_ASSERT(!scale_curve);
        scale_curve = curve_node;
      } else if (binding.property == "lockInfluenceWeights") {
      } else if (binding.property == "lockInfluenceWeights1") {
      } else if (binding.property == "filmboxTypeID") {
      } else if (binding.property == "liw") {
      } else {
        LOG_FBX_CONVERTER(
            WARNING, "Ignored animation curve node child to property '%s'!\n",
            cstr(binding.property));
      }
    } else if (binding.object.Is<scene::Model *>()) {
      scene::Model * model = binding.object.As<scene::Model *>();
      CORE_LIB_ASSERT(model->type == "LimbNode");
      LOG_FBX_CONVERTER(VERBOSE, "Attaching '%s' to '%s'..\n",
                        cstr(model->name), cstr(_limb_node.name));
      convert(*model, _converter, &bone_builder);
    } else if (binding.object.Is<scene::NodeAttribute *>()) {
    } else {
      LOG_FBX_CONVERTER(WARNING, "Ignored LimbNode child '%s'!\n",
                        cstr(to_string(binding.object)));
    }
  }
  if (!animated_model_data_builder.animation_data_builders.IsEmpty()) {
    if (translation_curve || rotation_curve || scale_curve) {
      convert(_converter, *channel_builder, translation_curve, rotation_curve,
              scale_curve);
    }
  } else {
    CORE_LIB_ASSERT(!translation_curve && !rotation_curve && !scale_curve);
  }
}

/*******************************************************************************
** convert ( Cluster )
*******************************************************************************/

inline void convert(scene::Cluster & _cluster, Converter & _converter) {
  LOG_FBX_CONVERTER(VERBOSE, "Converting Cluster '%s'..\n",
                    cstr(_cluster.name));
  Children children(_cluster.id, _converter);
  scene::Model * limb_node = nullptr;
  for (Binding & binding : children) {
    LOG_FBX_CONVERTER(VERBOSE, "Child: '%s' ('%s')\n",
                      cstr(to_string(binding.object)), cstr(binding.property));
    if (binding.object.Is<scene::Deformer *>()) {
      scene::Deformer * deformer = binding.object.As<scene::Deformer *>();
      CORE_LIB_ASSERT(deformer->Is<scene::Cluster>());
      convert(deformer->As<scene::Cluster>(), _converter);
    } else if (binding.object.Is<scene::Model *>()) {
      scene::Model * model = binding.object.As<scene::Model *>();
      CORE_LIB_ASSERT(model->type == "LimbNode");
      limb_node = model;
    } else {
      LOG_FBX_CONVERTER(WARNING, "Ignored Cluster child '%s'\n",
                        cstr(to_string(binding.object)));
    }
  }
  if (limb_node) {
    resource::AnimatedModelDataBuilder & animated_model_data_builder =
        _converter.model_data_builder.As<resource::AnimatedModelDataBuilder>();
    auto & mesh_data_builder = animated_model_data_builder.mesh_data_builder;
    auto * limb_bone_builder = _converter.skeleton_builder.Find(limb_node->id);
    CORE_LIB_ASSERT(limb_bone_builder);
    auto & bone_builder = *limb_bone_builder;
    CORE_LIB_ASSERT(_cluster.indices.Size() == _cluster.weights.Size());
    for (auto current = 0u; current < _cluster.indices.Size(); ++current) {
      auto index = _cluster.indices[current];
      auto & bone_data = mesh_data_builder.bone_builders.data[index];
      bone_data.AddBone(bone_builder.bone_index, _cluster.weights[current]);
    }
    bone_builder.bone->transformation = _cluster.transform_link;
    bone_builder.bone->offset = numerics::inverse(_cluster.transform_link);
  }
}

/*******************************************************************************
** convert ( Skin )
*******************************************************************************/

inline void convert(scene::Skin & _skin, Converter & _converter) {
  LOG_FBX_CONVERTER(VERBOSE, "Converting Skin '%s'..\n", cstr(_skin.name));
  Children children(_skin.id, _converter);
  for (Binding & binding : children) {
    LOG_FBX_CONVERTER(VERBOSE, "Child: '%s' ('%s')\n",
                      cstr(to_string(binding.object)), cstr(binding.property));
    if (binding.object.Is<scene::Deformer *>()) {
      scene::Deformer * deformer = binding.object.As<scene::Deformer *>();
      CORE_LIB_ASSERT(deformer->Is<scene::Cluster>());
      convert(deformer->As<scene::Cluster>(), _converter);
    } else {
      LOG_FBX_CONVERTER(WARNING, "Ignored Cluster child '%s'\n",
                        cstr(to_string(binding.object)));
    }
  }
}

/*******************************************************************************
** convert_geometry_vertices
*******************************************************************************/

inline void
convert_geometry_vertices(scene::Geometry const & _geometry,
                          size_t _vertex_count,
                          core::UniqueArray<numerics::Vector3f> & _positions) {
  _positions = core::make_unique_array<numerics::Vector3f>(_vertex_count);

  for (auto vertex = 0u; vertex < _vertex_count; ++vertex) {
    _positions[vertex] = numerics::Vector3f(
        static_cast<float>(_geometry.vertices[vertex * 3u + 0u]),
        static_cast<float>(_geometry.vertices[vertex * 3u + 1u]),
        static_cast<float>(_geometry.vertices[vertex * 3u + 2u]));
  }
}

/*******************************************************************************
** convert_geometry_indices
*******************************************************************************/

inline void convert_geometry_indices(scene::Geometry const & _geometry,
                                     core::UniqueArray<uint32> & _indices) {
  size_t triangle_count = 0u;
  {
    size_t index_start = 0u;
    while (index_start < _geometry.indices.Size()) {
      size_t index_end = index_start;
      // Polygon end is marked by a negated index
      while (0 <= _geometry.indices[index_end]) {
        ++index_end;
        CORE_LIB_ASSERT(index_end < _geometry.indices.Size());
      }
      triangle_count += index_end - index_start - 1u;
      index_start = index_end + 1u;
    }
  }

  _indices = core::make_unique_array<uint32>(triangle_count * 3u);

  {
    size_t triangle = 0u;
    size_t index_start = 0u;
    while (index_start < _geometry.indices.Size()) {
      size_t index_end = index_start;
      // Polygon end is marked by a negated index
      while (0 <= _geometry.indices[index_end]) {
        ++index_end;
        CORE_LIB_ASSERT(index_end < _geometry.indices.Size());
      }
      // Add triangulated indices
      size_t index = index_start + 2u;
      while (index < index_end) {
        _indices[triangle * 3u + 2u] =
            static_cast<uint32>(_geometry.indices[index_start]);
        _indices[triangle * 3u + 0u] =
            static_cast<uint32>(_geometry.indices[index - 1u]);
        _indices[triangle * 3u + 1u] =
            static_cast<uint32>(_geometry.indices[index]);
        ++triangle;
        ++index;
      }
      // Add last triangle
      _indices[triangle * 3u + 2u] =
          static_cast<uint32>(_geometry.indices[index_start]);
      _indices[triangle * 3u + 0u] =
          static_cast<uint32>(_geometry.indices[index - 1u]);
      _indices[triangle * 3u + 1u] =
          static_cast<uint32>(-_geometry.indices[index]) - 1u;
      ++triangle;
      index_start = index_end + 1u;
    }
    CORE_LIB_ASSERT(triangle == triangle_count);
  }
}

/*******************************************************************************
** convert_geometry_normals
*******************************************************************************/

inline void
convert_geometry_normals(scene::Geometry const & _geometry,
                         size_t _vertex_count,
                         core::UniqueArray<numerics::Vector3f> & _normals) {
  _normals = core::make_unique_array<numerics::Vector3f>(_vertex_count);

  auto & layer_element_normal = _geometry.layer_element_normal;
  if (layer_element_normal.version == 102) {
    if (layer_element_normal.mapping_information_type ==
        scene::MappingInformationType::ByPolygonVertex) {
      if (layer_element_normal.reference_information_type ==
          scene::ReferenceInformationType::Direct) {
        for (auto vertex_index = 0u; vertex_index < _vertex_count;
             ++vertex_index) {
          auto x = static_cast<float>(
              layer_element_normal.normals[vertex_index * 3u + 0u]);
          auto y = static_cast<float>(
              layer_element_normal.normals[vertex_index * 3u + 1u]);
          auto z = static_cast<float>(
              layer_element_normal.normals[vertex_index * 3u + 2u]);
          _normals[vertex_index] = numerics::Vector3f(x, y, z);
        }
      } else {
        LOG_FBX_CONVERTER(
            WARNING,
            "Unsupported layer element normal reference information type!\n");
      }
    } else {
      LOG_FBX_CONVERTER(WARNING,
                        "Unsupported layer element normal mapping type!\n");
    }
  } else {
    LOG_FBX_CONVERTER(WARNING,
                      "Unsupported layer element normal version %" PRIi32 "\n",
                      layer_element_normal.version);
  }
}

/*******************************************************************************
** convert_geometry_uvs
*******************************************************************************/

inline void convert_geometry_uvs(scene::Geometry const & _geometry,
                                 size_t _vertex_count,
                                 core::UniqueArray<numerics::Vector2f> & _uvs) {
  _uvs = core::make_unique_array<numerics::Vector2f>(_vertex_count);

  auto & layer_element_uv = _geometry.layer_element_uvs[0u];
  if (layer_element_uv.version == 101) {
    if (layer_element_uv.mapping_information_type ==
        scene::MappingInformationType::ByPolygonVertex) {
      if (layer_element_uv.reference_information_type ==
          scene::ReferenceInformationType::IndexToDirect) {
        CORE_LIB_ASSERT(_geometry.indices.Size() ==
                        layer_element_uv.uv_index.Size());
        for (auto current = 0u; current < _geometry.indices.Size(); ++current) {
          auto vertex_index = _geometry.indices[current];
          if (vertex_index < 0)
            vertex_index = -vertex_index - 1u;
          auto uv_index = layer_element_uv.uv_index[current] * 2u;
          auto u = static_cast<float>(layer_element_uv.uv[uv_index + 0u]);
          auto v = static_cast<float>(layer_element_uv.uv[uv_index + 1u]);
          _uvs[vertex_index] = numerics::Vector2f(u, v);
        }
      } else {
        LOG_FBX_CONVERTER(
            WARNING,
            "Unsupported layer element uv reference information type!\n");
      }
    } else {
      LOG_FBX_CONVERTER(WARNING,
                        "Unsupported layer element uv mapping type!\n");
    }
  } else {
    LOG_FBX_CONVERTER(WARNING,
                      "Unsupported layer element uv version %" PRIi32 "\n",
                      layer_element_uv.version);
  }
}

/*******************************************************************************
** convert_geometry_bone_data
*******************************************************************************/

inline void convert_geometry_bone_data(
    scene::Geometry const & _geometry, Converter & _converter,
    size_t _vertex_count,
    core::UniqueArray<resource::BoneDataBuilder> & _bone_data) {
  _bone_data =
      core::make_unique_array<resource::BoneDataBuilder>(_vertex_count);

  scene::Deformer * geometry_deformer = nullptr;
  {
    Children children(_geometry.id, _converter);
    for (Binding & binding : children) {
      LOG_FBX_CONVERTER(VERBOSE, "Child: '%s' ('%s')\n",
                        cstr(to_string(binding.object)),
                        cstr(binding.property));
      if (binding.object.Is<scene::Deformer *>()) {
        scene::Deformer * deformer = binding.object.As<scene::Deformer *>();
        CORE_LIB_ASSERT(!geometry_deformer);
        geometry_deformer = deformer;
      } else {
        LOG_FBX_CONVERTER(WARNING, "Ignored Geometry child '%s'\n",
                          cstr(to_string(binding.object)));
      }
    }
  }
  if (geometry_deformer) {
    CORE_LIB_ASSERT(geometry_deformer->Is<scene::Skin>());
    convert(geometry_deformer->As<scene::Skin>(), _converter);
  }
}

/*******************************************************************************
** convert ( Geometry )
*******************************************************************************/

inline void convert(scene::Geometry & _geometry, Converter & _converter) {
  LOG_FBX_CONVERTER(VERBOSE, "Converting Geometry '%s'..\n",
                    cstr(_geometry.name));
  CORE_LIB_ASSERT(_geometry.vertices.Size() % 3u == 0u);
  size_t vertex_count = _geometry.vertices.Size() / 3u;

  if (_converter.model_data_builder.Is<resource::StaticModelDataBuilder>()) {
    resource::StaticMeshDataBuilder & mesh_data_builder =
        _converter.model_data_builder.As<resource::StaticModelDataBuilder>()
            .mesh_data_builder;
    convert_geometry_vertices(_geometry, vertex_count,
                              mesh_data_builder.positions.data);
    convert_geometry_indices(_geometry, mesh_data_builder.indices.data);
    convert_geometry_normals(_geometry, vertex_count,
                             mesh_data_builder.normals.data);
    convert_geometry_uvs(_geometry, vertex_count,
                         mesh_data_builder.texture_coordinates.data);
  } else {
    resource::SkinnedMeshDataBuilder & mesh_data_builder =
        _converter.model_data_builder.As<resource::AnimatedModelDataBuilder>()
            .mesh_data_builder;
    convert_geometry_vertices(_geometry, vertex_count,
                              mesh_data_builder.positions.data);
    convert_geometry_indices(_geometry, mesh_data_builder.indices.data);
    convert_geometry_normals(_geometry, vertex_count,
                             mesh_data_builder.normals.data);
    convert_geometry_uvs(_geometry, vertex_count,
                         mesh_data_builder.texture_coordinates.data);
    convert_geometry_bone_data(_geometry, _converter, vertex_count,
                               mesh_data_builder.bone_builders.data);
  }
}

/*******************************************************************************
** convert ( Mesh )
*******************************************************************************/

inline void convert(scene::Model & _mesh, Converter & _converter) {
  CORE_LIB_ASSERT(_mesh.type == "Mesh");
  LOG_FBX_CONVERTER(VERBOSE, "Converting mesh '%s'..\n", cstr(_mesh.name));
  Children children(_mesh.id, _converter);
  for (Binding & binding : children) {
    LOG_FBX_CONVERTER(VERBOSE, "Child: '%s' ('%s')\n",
                      cstr(to_string(binding.object)), cstr(binding.property));
    if (binding.object.Is<scene::Geometry *>()) {
      scene::Geometry * geometry = binding.object.As<scene::Geometry *>();
      convert(*geometry, _converter);
    } else {
      LOG_FBX_CONVERTER(WARNING, "Ignored mesh child '%s'\n",
                        cstr(to_string(binding.object)));
    }
  }
}

/*******************************************************************************
** convert ( Root )
*******************************************************************************/

inline void convert(Root & _root, Converter & _converter) {
  LOG_FBX_CONVERTER(VERBOSE, "Converting root '%s'..\n", cstr(_root.name));

  scene::Model * skeleton = nullptr;
  scene::Model * mesh = nullptr;

  Children children(_root.id, _converter);
  for (Binding & binding : children) {
    LOG_FBX_CONVERTER(VERBOSE, "Child: '%s' ('%s')\n",
                      cstr(to_string(binding.object)), cstr(binding.property));
    if (binding.object.Is<scene::Model *>()) {
      scene::Model * model = binding.object.As<scene::Model *>();
      if (model->type == "LimbNode") {
        CORE_LIB_ASSERT(!skeleton);
        skeleton = model;
      } else if (model->type == "Mesh") {
        CORE_LIB_ASSERT(!mesh);
        mesh = model;
      } else {
        LOG_FBX_CONVERTER(WARNING, "Ignored root model child of type '%s'\n",
                          cstr(model->type));
      }
    } else {
      LOG_FBX_CONVERTER(WARNING, "Ignored root child '%s'\n",
                        cstr(to_string(binding.object)));
    }
  }

  if (skeleton) {
    LOG_FBX_CONVERTER(STAGE, "Converting skeleton..\n");
    convert(*skeleton, _converter, nullptr);
    _converter.skeleton_builder.SortByLimbId();
  }
  if (mesh) {
    LOG_FBX_CONVERTER(STAGE, "Converting mesh..\n");
    convert(*mesh, _converter);
  }
}

/*******************************************************************************
** print_connections
*******************************************************************************/

inline void print_connections(Converter & _converter, int64 _id,
                              size_t _depth) {
  core::span<scene::Connection> connections =
      _converter.ConnectionsByParentId(_id);
  for (scene::Connection & connection : connections) {
    for (size_t current = 0u; current < _depth; ++current)
      std::printf("  ");
    auto child = _converter.object_map.Find(connection.child_id);
    if (!child) {
      std::printf("<UNKNOWN - %" PRId64 ">\n", _id);
      continue;
    }
    std::printf("'%s' | '%s'\n", cstr(to_string(*child)),
                cstr(connection.property_name));
    print_connections(_converter, connection.child_id, _depth + 1u);
  }
}

inline void print_connections(Converter & _converter) {
  print_connections(_converter, 0, 0u);
}

resource::ModelDataBuilder
make_model_data_builder(scene::Scene const & _scene) {
  bool animated = false;
  for (scene::Model const & model : _scene.models.values) {
    if (model.type == "LimbNode") {
      animated = true;
      break;
    }
  }

  if (animated) {
    return in_place_t<resource::AnimatedModelDataBuilder>();
  }
  return in_place_t<resource::StaticModelDataBuilder>();
}

/*******************************************************************************
** convert
*******************************************************************************/

inline resource::ModelDataBuilder convert(scene::Scene && _scene) {
  LOG_FBX_CONVERTER(STAGE, "Converting..\n");

  resource::ModelDataBuilder model_data_builder =
      make_model_data_builder(_scene);
  Converter converter(move(_scene), model_data_builder);
  auto const & root_node = converter.object_map.GetRootNode();
  Root * root = root_node.object.As<Root *>();
  convert(*root, converter);
  return model_data_builder;
}

}}}

////////////////////////////////////////////////////////////////////////////////
// graphics fbx
////////////////////////////////////////////////////////////////////////////////

namespace graphics { namespace fbx {

inline core::Optional<resource::ModelData>
convert(core::fs::FilePath const & _file_path) {
  if (auto scene = scene::load(_file_path)) {
    return build(converter::convert(move(scene.Value())));
  }
  return core::nullopt;
}

}}

#endif
