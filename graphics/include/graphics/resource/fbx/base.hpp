#ifndef _GRAPHICS_RESOURCE_FBX_BASE_HPP_
#define _GRAPHICS_RESOURCE_FBX_BASE_HPP_

#ifndef GRAPHICS_FBX
#  error
#endif

#include "graphics/base.hpp"

////////////////////////////////////////////////////////////////////////////////
// graphics fbx
////////////////////////////////////////////////////////////////////////////////

namespace graphics { namespace fbx {

/*******************************************************************************
** Valueless
*******************************************************************************/

struct Valueless {};

/*******************************************************************************
** Primitive
*******************************************************************************/

using Primitives =
    mpl::TypePack<Valueless, bool, int16, int32, int64, float, double>;
using Primitive = mpl::inject_typepack_t<core::Variant, Primitives>;

using FloatArray = core::UniqueArray<float>;
using DoubleArray = core::UniqueArray<double>;
using Int64Array = core::UniqueArray<int64>;
using Int32Array = core::UniqueArray<int32>;
using BoolArray = core::UniqueArray<bool>;

using Arrays = mpl::TypePack<Valueless, FloatArray, DoubleArray, Int64Array,
                             Int32Array, BoolArray>;
using Array = mpl::inject_typepack_t<core::Variant, Arrays>;

/*******************************************************************************
** String
*******************************************************************************/

using String = const_string;

/*******************************************************************************
** RawBinary
*******************************************************************************/

using RawBinary = core::UniqueArray<byte>;

/*******************************************************************************
** Value
*******************************************************************************/

using Value = core::Variant<Valueless, Primitive, Array, String, RawBinary>;

/*******************************************************************************
** Property
*******************************************************************************/

using Property = Value;

/*******************************************************************************
** set
*******************************************************************************/

template<typename T>
using IfPrimitive =
    mpl::enable_if_t<mpl::typepack_contains<T, Primitives>::value>;

template<typename TPrimitive>
inline IfPrimitive<TPrimitive> set(Property & _property,
                                   TPrimitive _primitive) {
  new (&_property)
      Value(in_place_t<Primitive>(), in_place_t<TPrimitive>(), _primitive);
}

template<typename T>
using IfArray = mpl::enable_if_t<mpl::typepack_contains<T, Arrays>::value>;

template<typename TArray>
inline IfArray<TArray> set(Property & _property, TArray _array) {
  new (&_property)
      Value(in_place_t<Array>(), in_place_t<TArray>(), move(_array));
}

inline void set(Property & _property, String _string) {
  new (&_property) Value(in_place_t<String>(), move(_string));
}

inline void set(Property & _property, RawBinary _raw_binary) {
  new (&_property) Value(in_place_t<RawBinary>(), move(_raw_binary));
}

/*******************************************************************************
** Node
*******************************************************************************/

struct Node {
  String name;
  core::UniqueArray<Property> properties;
  core::AlignedUniqueArray<Node> children;
};

}}

#endif
