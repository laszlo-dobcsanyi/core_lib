#ifndef _GRAPHICS_RESOURCE_FBX_CONVERTER_CONVERTER_HPP_
#define _GRAPHICS_RESOURCE_FBX_CONVERTER_CONVERTER_HPP_

////////////////////////////////////////////////////////////////////////////////
// x converter
////////////////////////////////////////////////////////////////////////////////

namespace graphics { namespace fbx { namespace converter {

/*******************************************************************************
** Converter
*******************************************************************************/

class Converter {
public:
  scene::Scene scene;
  ObjectMap object_map;
  resource::ModelDataBuilder & model_data_builder;
  SkeletonBuilder skeleton_builder;

  Converter() = delete;
  Converter(scene::Scene _scene,
            resource::ModelDataBuilder & _model_data_builder)
      : scene(move(_scene))
      , model_data_builder(_model_data_builder) {
    if (auto * animated_model_data_builder =
            model_data_builder.Get<resource::AnimatedModelDataBuilder>()) {
      skeleton_builder.Set(animated_model_data_builder->skeleton_data_builder);
      // TODO: This is not correct!
      if (0u < scene.animation_layers.Size())
        animated_model_data_builder->animation_data_builders.data =
            core::make_unique_array<resource::AnimationDataBuilder>(1u);
    }
    CreateObjectMap();
    SortConnections();
  }
  Converter(Converter &&) = delete;
  Converter(Converter const &) = delete;
  Converter & operator=(Converter &&) = delete;
  Converter & operator=(Converter const &) = delete;
  ~Converter() = default;

  core::span<scene::Connection> ConnectionsByParentId(int64 _parent_id);

private:
  void CreateObjectMap();
  void SortConnections();
};

/*******************************************************************************
** Converter
*******************************************************************************/

inline void Converter::CreateObjectMap() {
  size_t object_count = 0u;
  object_count += scene.node_attributes.Size();
  object_count += scene.geometries.Size();
  object_count += scene.models.Size();
  object_count += scene.poses.Size();
  object_count += scene.deformers.Size();
  object_count += scene.materials.Size();
  object_count += scene.animation_stacks.Size();
  object_count += scene.animation_layers.Size();
  object_count += scene.animation_curve_nodes.Size();
  object_count += scene.animation_curves.Size();
  object_map.SetCapacity(object_count);
  object_map.Insert(scene.node_attributes);
  object_map.Insert(scene.geometries);
  object_map.Insert(scene.models);
  object_map.Insert(scene.poses);
  object_map.Insert(scene.deformers);
  object_map.Insert(scene.materials);
  object_map.Insert(scene.animation_stacks);
  object_map.Insert(scene.animation_layers);
  object_map.Insert(scene.animation_curve_nodes);
  object_map.Insert(scene.animation_curves);
  object_map.SortByParentId();
}

inline void Converter::SortConnections() {
  struct Predicate {
    bool operator()(scene::Connection const & _lhs,
                    scene::Connection const & _rhs) {
#if 0
      LOG_FBX_CONVERTER( LOG, "Checking %li ? %li ( %li ? %li) ('%s' ? '%s' )..\n"
                          , _lhs.parent_id, _rhs.parent_id
                          , _lhs.child_id, _rhs.child_id
                          , cstr( _lhs.property_name ), cstr( _rhs.property_name ) );
#endif
      if (_lhs.parent_id != _rhs.parent_id)
        return _lhs.parent_id < _rhs.parent_id;
      if (_lhs.child_id != _rhs.child_id)
        return _lhs.child_id < _rhs.child_id;
      return _lhs.property_name < _rhs.property_name;
    }
  };
  LOG_FBX_CONVERTER(VERBOSE, "Sorting connections..\n");
  core::sort(core::make_span(scene.connections), Predicate());
}

inline core::span<scene::Connection>
Converter::ConnectionsByParentId(int64 _parent_id) {
  auto & connections = scene.connections;
  size_t size = connections.Size();
  // logaritmic search for an index with parent_id
  size_t index = 0u;
  {
    LOG_FBX_CONVERTER(VERBOSE, "Finding index..\n");
    size_t start = 0u;
    size_t end = size - 1u;
    while (start <= end) {
      index = start + (end - start) / 2u;
      LOG_FBX_CONVERTER(VERBOSE, "\t%ld - %ld -> %ld\n", start, end, index);
      auto const parent_id = connections[index].parent_id;
      LOG_FBX_CONVERTER(VERBOSE, "\t\t%li (%li)\n", parent_id, _parent_id);
      if (parent_id == _parent_id) {
        break;
      } else if (parent_id < _parent_id) {
        start = index + 1u;
      } else {
        end = index - 1u;
      }
    }
  }

  // if the no index is found, return an empty buffer
  if (connections[index].parent_id != _parent_id) {
    return core::span<scene::Connection>();
  }

  // sequentially find the first by parent_id
  auto first = index;
  {
    LOG_FBX_CONVERTER(VERBOSE, "Finding first..\n");
    while (0u < first) {
      if (connections[first - 1u].parent_id != _parent_id) {
        break;
      }
      --first;
    }
  }

  // sequentially find the last by parent_id
  auto last = index;
  {
    LOG_FBX_CONVERTER(VERBOSE, "Finding last..\n");
    while (last < size - 1u) {
      if (connections[last + 1u].parent_id != _parent_id) {
        break;
      }
      ++last;
    }
  }

  CORE_LIB_ASSERT(connections[first].parent_id == _parent_id);
  CORE_LIB_ASSERT(connections[last].parent_id == _parent_id);

  auto * first_connection = &connections[first];
  LOG_FBX_CONVERTER(VERBOSE, "Range is from %ld to %ld!\n", first, last);
  return core::make_span(first_connection, last - first + 1u);
}

}}}

#endif
