#ifndef _GRAPHICS_RESOURCE_FBX_CONVERTER_CHILDREN_HPP_
#define _GRAPHICS_RESOURCE_FBX_CONVERTER_CHILDREN_HPP_

////////////////////////////////////////////////////////////////////////////////
// x converter
////////////////////////////////////////////////////////////////////////////////

namespace graphics { namespace fbx { namespace converter {

class Child;

/*******************************************************************************
** Children
*******************************************************************************/

class Children {
public:
  friend Child;

  Children() = delete;
  Children(int64 _id, Converter & _converter)
      : converter(_converter)
      , connections(converter.ConnectionsByParentId(_id)) {}

  Child begin();
  Child end();

private:
  Converter & converter;
  core::span<scene::Connection> connections;
};

/*******************************************************************************
** Binding
*******************************************************************************/

struct Binding {
  ObjectPointer object;
  const_string property;

  Binding()
      : object(in_place_t<Valueless>()) {}
  Binding(ObjectPointer _object, const_string _property)
      : object(move(_object))
      , property(move(_property)) {}
};

/*******************************************************************************
** Child
*******************************************************************************/

class Child {
public:
  friend Children;

  Child() = delete;
  Child(Child &&) = default;
  Child(Child const &) = delete;
  Child & operator=(Child &&) = default;
  Child & operator=(Child const &) = delete;
  ~Child() = default;

  Child & operator++() {
    ++index;
    SetBinding();
    return *this;
  }

  Binding & operator*() { return binding; }

  bool operator==(Child const & _other) { return index == _other.index; }
  bool operator!=(Child const & _other) { return !(*this == _other); }

private:
  Children * children;
  size_t index;
  Binding binding;

  Child(Children & _children, size_t _index)
      : children(&_children)
      , index(_index) {
    SetBinding();
  }

  void SetBinding();
};

}}}

////////////////////////////////////////////////////////////////////////////////
// x converter
////////////////////////////////////////////////////////////////////////////////

namespace graphics { namespace fbx { namespace converter {

/*******************************************************************************
** Children
*******************************************************************************/

inline Child Children::begin() { return Child(*this, 0u); }

inline Child Children::end() { return Child(*this, connections.size()); }

/*******************************************************************************
** Child
*******************************************************************************/

inline void Child::SetBinding() {
  CORE_LIB_ASSERT(children);
  while (index < children->connections.size()) {
    auto & connection = children->connections[index];
    auto * child_node =
        children->converter.object_map.Find(connection.child_id);
    if (child_node) {
      binding = Binding(child_node->object, connection.property_name);
      return;
    } else {
      LOG_FBX_CONVERTER(WARNING, "Skipping child '%li'..\n",
                        connection.child_id);
      ++index;
    }
  }
  index = children->connections.size();
  binding = Binding();
}

}}}

#endif
