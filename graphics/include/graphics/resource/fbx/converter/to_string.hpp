#ifndef _GRAPHICS_RESOURCE_FBX_CONVERTER_TO_STRING_HPP_
#define _GRAPHICS_RESOURCE_FBX_CONVERTER_TO_STRING_HPP_

////////////////////////////////////////////////////////////////////////////////
// x converter
////////////////////////////////////////////////////////////////////////////////

namespace graphics { namespace fbx { namespace converter {

/*******************************************************************************
** get_object_type_name
*******************************************************************************/

inline char const * get_object_type_name(Root const &) { return "Root"; }
inline char const * get_object_type_name(scene::NodeAttribute const &) {
  return "NodeAttribute";
}
inline char const * get_object_type_name(scene::Geometry const &) {
  return "Geometry";
}
inline char const * get_object_type_name(scene::Model const &) {
  return "Model";
}
inline char const * get_object_type_name(scene::Pose const &) { return "Pose"; }
inline char const * get_object_type_name(scene::Deformer const &) {
  return "Deformer";
}
inline char const * get_object_type_name(scene::Material const &) {
  return "Material";
}
inline char const * get_object_type_name(scene::AnimationStack const &) {
  return "AnimationStack";
}
inline char const * get_object_type_name(scene::AnimationLayer const &) {
  return "AnimationLayer";
}
inline char const * get_object_type_name(scene::AnimationCurveNode const &) {
  return "AnimationCurveNode";
}
inline char const * get_object_type_name(scene::AnimationCurve const &) {
  return "AnimationCurve";
}

/*******************************************************************************
** to_string
*******************************************************************************/

template<typename T>
inline const_string to_string(T const * _object) {
  string::Stream stream;
  stream << get_object_type_name(*_object);
  stream << " \"";
  stream << get_name(_object);
  stream << "\"";
  return stream.Get();
}

struct ToString {
  template<typename T>
  const_string operator()(T const * _object) {
    return to_string(_object);
  }
  const_string operator()(Valueless const &) { return "Valueless"; }
};

inline const_string to_string(ObjectPointer const & _object) {
  return _object.Apply(ToString());
}

inline const_string to_string(Node const & _node) {
  return to_string(_node.object);
}

}}}

#endif
