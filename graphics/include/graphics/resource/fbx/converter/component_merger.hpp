#ifndef _GRAPHICS_RESOURCE_FBX_CONVERTER_COMPONENT_MERGER_HPP_
#define _GRAPHICS_RESOURCE_FBX_CONVERTER_COMPONENT_MERGER_HPP_

////////////////////////////////////////////////////////////////////////////////
// x converter
////////////////////////////////////////////////////////////////////////////////

namespace graphics { namespace fbx { namespace converter {

/*******************************************************************************
** Pose
*******************************************************************************/

struct Pose {
  float time;
  numerics::Vector3f value;
};

/*******************************************************************************
** ComponentChannel
*******************************************************************************/

class ComponentChannel {
public:
  ComponentChannel(scene::AnimationCurve const * _curve)
      : index(0u)
      , curve(_curve) {
    CORE_LIB_ASSERT(!_curve || 0u < curve->key_times.Size());
    CORE_LIB_ASSERT(!_curve ||
                    curve->key_times.Size() == curve->key_values.Size());
    CORE_LIB_ASSERT(!_curve || curve->key_times[0u].seconds == 0);
  }

  float Value(float _time);
  bool Finished() const;

  float NextTime() const;
  float LastTime() const;

private:
  size_t index;
  scene::AnimationCurve const * curve = nullptr;

  float ValueAt(size_t _index) const;
  float TimeAt(size_t _index) const;
};

/*******************************************************************************
** ComponentMerger
*******************************************************************************/

class ComponentMerger {
public:
  ComponentChannel x;
  ComponentChannel y;
  ComponentChannel z;

  ComponentMerger(scene::AnimationCurve const * _x_curve,
                  scene::AnimationCurve const * _y_curve,
                  scene::AnimationCurve const * _z_curve)
      : x(_x_curve)
      , y(_y_curve)
      , z(_z_curve)
      , time(0)
      , end(core::max(x.LastTime(), core::max(y.LastTime(), z.LastTime())))
      , finished(false) {}

  bool Finished() const { return finished; }

  Pose Next();

private:
  float time;
  float end;
  bool finished;
};

}}}

////////////////////////////////////////////////////////////////////////////////
// x converter
////////////////////////////////////////////////////////////////////////////////

namespace graphics { namespace fbx { namespace converter {

/*******************************************************************************
** ComponentChannel
*******************************************************************************/

inline float ComponentChannel::Value(float _time) {
  // LOG_FBX_CONVERTER( VERBOSE, "Getting value @ %f [%ld] (%f / %f)..\n"
  //                         , _time, index, TimeAt( index ), TimeAt( index + 1u ) );
  auto next_time = NextTime();
  if (_time == next_time) {
    // LOG_FBX_CONVERTER( VERBOSE, "Advancing local time!\n" );
    ++index;
    return ValueAt(index);
  }
  auto current_time = TimeAt(index);
  CORE_LIB_ASSERT(current_time <= _time && _time < next_time);
  auto alpha = (_time - current_time) / (next_time - current_time);
  return numerics::lerp(ValueAt(index), ValueAt(index + 1u), alpha);
}

inline bool ComponentChannel::Finished() const {
  if (!curve)
    return true;
  return index == curve->key_values.Size();
}

inline float ComponentChannel::NextTime() const { return TimeAt(index + 1u); }

inline float ComponentChannel::LastTime() const {
  if (!curve)
    return 0.f;
  return static_cast<float>(
      curve->key_times[curve->key_times.Size() - 1u].seconds);
}

inline float ComponentChannel::ValueAt(size_t _index) const {
  if (!curve)
    return 0.f;
  if (_index < curve->key_values.Size())
    return curve->key_values[_index];
  return curve->key_values[curve->key_values.Size() - 1u];
}

inline float ComponentChannel::TimeAt(size_t _index) const {
  if (!curve)
    return 0.f;
  if (_index < curve->key_times.Size())
    return static_cast<float>(curve->key_times[_index].seconds);
  return static_cast<float>(
      curve->key_times[curve->key_times.Size() - 1u].seconds);
}

/*******************************************************************************
** ComponentMerger
*******************************************************************************/

inline Pose ComponentMerger::Next() {
  finished = x.Finished() && y.Finished() && z.Finished();
  Pose result;
  result.time = time;
  result.value[0u] = x.Value(time);
  result.value[1u] = y.Value(time);
  result.value[2u] = z.Value(time);
  LOG_FBX_CONVERTER(VERBOSE, "Local time is %f -> %f, %f, %f..\n", time,
                    x.NextTime(), y.NextTime(), z.NextTime());
  if (!finished) {
    time = core::min(x.NextTime(), core::min(y.NextTime(), z.NextTime()));
  }
  return result;
}

}}}

#endif
