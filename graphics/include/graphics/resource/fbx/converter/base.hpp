#ifndef _GRAPHICS_RESOURCE_FBX_CONVERTER_BASE_HPP_
#define _GRAPHICS_RESOURCE_FBX_CONVERTER_BASE_HPP_

#define LOG_FBX_CONVERTER(__severity__, ...) LOG_FBX(__severity__, __VA_ARGS__)

////////////////////////////////////////////////////////////////////////////////
// x converter
////////////////////////////////////////////////////////////////////////////////

namespace graphics { namespace fbx { namespace converter {

/*******************************************************************************
** Root
*******************************************************************************/

struct Root {
  int64 id;
  const_string name;

  Root()
      : id(0)
      , name("Root") {}
};

/*******************************************************************************
** ObjectTypes
*******************************************************************************/

using ObjectTypes =
    mpl::TypePack<Root, scene::NodeAttribute, scene::Geometry, scene::Model,
                  scene::Pose, scene::Deformer, scene::Material,
                  scene::AnimationStack, scene::AnimationLayer,
                  scene::AnimationCurveNode, scene::AnimationCurve>;

/*******************************************************************************
** ObjectPointer
*******************************************************************************/

using ObjectPointers = mpl::for_each_t<mpl::add_pointer, ObjectTypes>;
using ObjectPointerTypes = mpl::insert_head_t<Valueless, ObjectPointers>;
using ObjectPointer = mpl::inject_typepack_t<core::Variant, ObjectPointerTypes>;

/*******************************************************************************
** Node
*******************************************************************************/

class Node {
public:
  ObjectPointer object;

  Node()
      : object(in_place_t<Valueless>()) {}
  Node(ObjectPointer _object)
      : object(_object) {}
  Node(Node &&) = default;
  Node(Node const &) = default;
  Node & operator=(Node &&) = default;
  Node & operator=(Node const &) = default;
  ~Node() = default;
};

/*******************************************************************************
** get_id
*******************************************************************************/

struct GetId {
  template<typename T>
  mpl::if_pointer_t<T, int64> operator()(T _object) {
    return _object->id;
  }

  template<typename T>
  mpl::if_not_pointer_t<T, int64> operator()(T const & _object) {
    return _object.id;
  }

  int64 operator()(scene::Deformer * _deformer) {
    return _deformer->Apply(GetId());
  }

  int64 operator()(Valueless const &) {
    CORE_LIB_ASSERT(false);
    return 0;
  }
};

inline int64 get_id(ObjectPointer const & _object) {
  return _object.Apply(GetId());
}

inline int64 get_id(Node const & _node) { return get_id(_node.object); }

/*******************************************************************************
** get_name
*******************************************************************************/

struct GetName {
  template<typename T>
  mpl::if_pointer_t<T, char const *> operator()(T _object) {
    return cstr(_object->name);
  }

  template<typename T>
  mpl::if_not_pointer_t<T, char const *> operator()(T const & _object) {
    return cstr(_object.name);
  }

  char const * operator()(scene::Deformer * _deformer) {
    return _deformer->Apply(GetName());
  }

  char const * operator()(scene::Deformer const * _deformer) {
    return _deformer->Apply(GetName());
  }

  char const * operator()(Valueless const &) {
    CORE_LIB_ASSERT(false);
    return "???";
  }
};

template<typename T>
inline char const * get_name(T const * _object) {
  GetName get_name;
  return get_name(_object);
}

inline char const * get_name(ObjectPointer const & _object) {
  return _object.Apply(GetName());
}

inline char const * get_name(Node const & _node) {
  return get_name(_node.object);
}

}}}

#endif
