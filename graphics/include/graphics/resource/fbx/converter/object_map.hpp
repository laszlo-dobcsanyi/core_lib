#ifndef _GRAPHICS_RESOURCE_FBX_CONVERTER_OBJECT_MAP_HPP_
#define _GRAPHICS_RESOURCE_FBX_CONVERTER_OBJECT_MAP_HPP_

////////////////////////////////////////////////////////////////////////////////
// x converter
////////////////////////////////////////////////////////////////////////////////

namespace graphics { namespace fbx { namespace converter {

/*******************************************************************************
** ObjectMap
*******************************************************************************/

class ObjectMap {
public:
  ObjectMap() = default;
  ObjectMap(ObjectMap &&) = default;
  ObjectMap(ObjectMap const &) = delete;
  ObjectMap & operator=(ObjectMap &&) = default;
  ObjectMap & operator=(ObjectMap const &) = delete;
  ~ObjectMap() = default;

  void SetCapacity(size_t _size);

  template<typename T>
  using if_object_t =
      mpl::enable_if_t<mpl::typepack_contains<T, ObjectPointers>::value>;

  template<typename T>
  if_object_t<T> Insert(T _object);
  template<typename Container>
  void Insert(Container & _container);

  void SortByParentId();

  Node const * Find(int64 _id) const;

  Node & GetRootNode() { return nodes[0u]; }
  Node const & GetRootNode() const { return nodes[0u]; }

private:
  core::UniqueArray<Node> nodes;
  Root root;
  size_t count = 0u;
};

inline bool operator<(Node const & _lhs, Node const & _rhs) {
  return get_id(_lhs) < get_id(_rhs);
}

/*******************************************************************************
** ObjectMap
*******************************************************************************/

inline void ObjectMap::SetCapacity(size_t _size) {
  CORE_LIB_ASSERT(!nodes);
  nodes = core::make_unique_array<Node>(_size + 1u);
  Insert(&root);
}

template<typename T>
inline ObjectMap::if_object_t<T> ObjectMap::Insert(T _object) {
  nodes[count++].object = ObjectPointer(in_place_t<T>(), _object);
}

template<typename Container>
inline void ObjectMap::Insert(Container & _container) {
  for (auto current = 0u; current < _container.Size(); ++current) {
    Insert(&_container[current]);
  }
}

inline void ObjectMap::SortByParentId() { core::sort(core::make_span(nodes)); }

inline Node const * ObjectMap::Find(int64 _id) const {
  return find_in_sorted(core::make_span(nodes), _id,
                        [](Node const & _node) { return get_id(_node); });
}

}}}

#endif
