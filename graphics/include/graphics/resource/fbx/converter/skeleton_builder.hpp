#ifndef _GRAPHICS_RESOURCE_FBX_CONVERTER_SKELETON_BUILDER_HPP_
#define _GRAPHICS_RESOURCE_FBX_CONVERTER_SKELETON_BUILDER_HPP_

////////////////////////////////////////////////////////////////////////////////
// x converter
////////////////////////////////////////////////////////////////////////////////

namespace graphics { namespace fbx { namespace converter {

/*******************************************************************************
** BoneBuilder
*******************************************************************************/

struct BoneBuilder {
  int64 limb_node_id;
  uint32 bone_index;
  resource::Bone * bone;

  BoneBuilder(int64 _limb_node_id, uint32 _bone_index, resource::Bone & _bone)
      : limb_node_id(_limb_node_id)
      , bone_index(_bone_index)
      , bone(&_bone) {}
  BoneBuilder(BoneBuilder &&) = default;
  BoneBuilder(BoneBuilder const &) = default;
  BoneBuilder & operator=(BoneBuilder &&) = default;
  BoneBuilder & operator=(BoneBuilder const &) = default;
  ~BoneBuilder() = default;
};

inline bool operator<(BoneBuilder const & _lhs, BoneBuilder const & _rhs) {
  return _lhs.limb_node_id < _rhs.limb_node_id;
}

/*******************************************************************************
** SkeletonBuilder
*******************************************************************************/

class SkeletonBuilder {
public:
  SkeletonBuilder() = default;
  SkeletonBuilder(SkeletonBuilder &&) = delete;
  SkeletonBuilder(SkeletonBuilder const &) = delete;
  SkeletonBuilder & operator=(SkeletonBuilder &&) = delete;
  SkeletonBuilder & operator=(SkeletonBuilder const &) = delete;
  ~SkeletonBuilder() = default;

  void Set(resource::SkeletonDataBuilder & _skeleton_data_builder) {
    skeleton_data_builder = &_skeleton_data_builder;
  }

  BoneBuilder & AddBoneBuilder(scene::Model & _limb_node,
                               BoneBuilder * _parent);

  void SortByLimbId();

  BoneBuilder * Find(int64 _limb_id);

private:
  container::BoundedStack<BoneBuilder, 256u> bone_builders;
  resource::SkeletonDataBuilder * skeleton_data_builder = nullptr;
};

}}}

////////////////////////////////////////////////////////////////////////////////
// x converter
////////////////////////////////////////////////////////////////////////////////

namespace graphics { namespace fbx { namespace converter {

/*******************************************************************************
** SkeletonBuilder
*******************************************************************************/

inline BoneBuilder & SkeletonBuilder::AddBoneBuilder(scene::Model & _limb_node,
                                                     BoneBuilder * _parent) {
  CORE_LIB_ASSERT(skeleton_data_builder);
  CORE_LIB_ASSERT(_limb_node.type == "LimbNode");
  auto bone_index =
      static_cast<uint32>(skeleton_data_builder->bones.builder.Size());
  auto & bone = skeleton_data_builder->bones.builder.Push();
  auto & bone_builder = bone_builders.Push(_limb_node.id, bone_index, bone);
  if (_parent) {
    LOG_FBX_CONVERTER(VERBOSE, "Bone child with index %ld (%ld)\n", bone_index,
                      _parent->bone_index);
    bone.next = _parent->bone->child;
    _parent->bone->child = bone_index;
  } else {
    LOG_FBX_CONVERTER(VERBOSE, "Bone root with index %ld\n", bone_index);
  }
  return bone_builder;
}

inline void SkeletonBuilder::SortByLimbId() {
  core::sort(core::make_span(bone_builders));
}

inline BoneBuilder * SkeletonBuilder::Find(int64 _limb_id) {
  return core::find_in_sorted(
      core::make_span(bone_builders), _limb_id,
      [](BoneBuilder & _builder) { return _builder.limb_node_id; });
}

}}}

#endif
