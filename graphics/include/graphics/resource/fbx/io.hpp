#ifndef _GRAPHICS_RESOURCE_FBX_IO_HPP_
#define _GRAPHICS_RESOURCE_FBX_IO_HPP_

#ifndef GRAPHICS_FBX
#  error
#endif

#include "core/zlib.hpp"
#include "core/fs/mapped_file.hpp"
#include "core/byte_stream.hpp"

#define PRINT_FBX(...)                                                         \
  std::printf(__VA_ARGS__) // CORE_LIB_LOG( LOG, __VA_ARGS__ )

// TODO: Parsing functions' first argument should be the ParserState.

////////////////////////////////////////////////////////////////////////////////
// x io
////////////////////////////////////////////////////////////////////////////////

namespace graphics { namespace fbx { namespace io {

/*******************************************************************************
** Stream
*******************************************************************************/

using Stream = core::LittleEndianByteStreamReader;

/*******************************************************************************
** NodeHeader
*******************************************************************************/

struct NodeHeader {
  uint64 end_offset;
  uint64 num_properties;
  uint64 property_list_len;
  uint8 name_len;
};

inline size_t to_size_t(uint64 _value) {
#if defined(CORE_LIB_ARCH_32)
  CORE_LIB_ASSERT((_value & U64(0xFFFFFFFF00000000)) == U64(0));
  return static_cast<size_t>(_value);
#else
  return _value;
#endif
}

/*******************************************************************************
** Data
*******************************************************************************/

struct Data {
  Node root;
  uint32 version;
};

/*******************************************************************************
** print
*******************************************************************************/

inline void print(Valueless) { PRINT_FBX("???"); }

inline void print(bool _value) { PRINT_FBX("%s", _value ? "true" : "false"); }

inline void print(int16 _value) { PRINT_FBX("%" PRIi16, _value); }

inline void print(int32 _value) { PRINT_FBX("%" PRIi32, _value); }

inline void print(int64 _value) { PRINT_FBX("%" PRIi64, _value); }

inline void print(float _value) { PRINT_FBX("%f", _value); }

inline void print(double _value) { PRINT_FBX("%f", _value); }

struct PrintPrimitive {
  template<typename T>
  void operator()(T const & _primitive) {
    print(_primitive);
  }
};

inline void print(Primitive const & _primitive) {
  _primitive.Apply(PrintPrimitive());
}

template<typename ElementType>
inline void print_array(core::UniqueArray<ElementType> const & _array) {
  PRINT_FBX("[");
  auto size = core::min(_array.Size(), static_cast<size_t>(32u));
  for (auto current = 0u; current < size; ++current) {
    print(_array[current]);
    if (current != _array.Size() - 1u) {
      PRINT_FBX(", ");
    }
  }
  if (32u < _array.Size()) {
    PRINT_FBX(" --omitted--  Size: %zu", _array.Size());
  }
  PRINT_FBX("]");
}

struct PrintArray {
  void operator()(Valueless) { PRINT_FBX("???"); }

  template<typename ArrayType>
  void operator()(ArrayType const & _array) {
    // TODO:
    // Note: Can't deduce 'ElementType'.
    print_array<typename ArrayType::value_type>(_array);
  }
};

inline void print(Array const & _array) { _array.Apply(PrintArray()); }

inline void print(String const & _string) { PRINT_FBX("'%s'", cstr(_string)); }

inline void print(RawBinary const & _raw_binary) {
  // TODO:
  // Note: Can't deduce 'ElementType'.
  print_array<typename RawBinary::value_type>(_raw_binary);
}

struct PrintProperty {
  template<typename T>
  void operator()(T & _property) {
    print(_property);
  }
};

/*******************************************************************************
** print
*******************************************************************************/

inline void print(uint32 _level, uint32 _index, Node const & _node) {
  for (auto level = 0u; level < _level; ++level) {
    PRINT_FBX("\t");
  }
  PRINT_FBX("%" PRIu32 ":\t%s", _index, cstr(_node.name));
  if (0u < _node.properties.Size()) {
    PRINT_FBX(" <");
    for (auto property = 0u; property < _node.properties.Size(); ++property) {
      _node.properties[property].Apply(PrintProperty());
      if (property != _node.properties.Size() - 1u) {
        PRINT_FBX(", ");
      }
    }
    PRINT_FBX(">");
  }
  PRINT_FBX("\n");
  for (auto child = 0u; child < _node.children.Size(); ++child) {
    print(_level + 1u, child, _node.children[child]);
  }
}

inline void print(Node const & _node) { print(0u, 0u, _node); }

////////////////////////////////////////////////////////////////////////////////
//                                                                                                //
////////////////////////////////////////////////////////////////////////////////

/*******************************************************************************
** ParserState
*******************************************************************************/

struct ParserState {
  Stream & stream;
  Data data;

  inline ParserState(Stream & _stream)
      : stream(_stream) {}
};

/*******************************************************************************
** parse_string
*******************************************************************************/

inline String parse_string(char const * _source, size_t _size) {
  using StringStorage = typename String::storage_type;
  StringStorage storage(_size);
  ::memcpy(storage.Get(), _source, _size);
  // FBX Strings are not null terminated, need to do it manually!
  *(storage.Get() + _size) = '\0';
  return String(move(storage));
}

/*******************************************************************************
** parse_header
*******************************************************************************/

inline bool parse_header(ParserState & _state) {
  char const * signature = "Kaydara FBX Binary  ";
  if (_state.stream.Available() <= 21u + 2u + 4u)
    return false;
  auto header_bytes = _state.stream.RemainingBytes(20u);
  const_string header(reinterpret_cast<char const *>(header_bytes.data()),
                      header_bytes.size());
  if (header != signature) {
    LOG_FBX(ERROR, "Signature mismatch! %s\n", cstr(header));
    return false;
  }
  _state.stream.Advance(21u);
  uint16 unknown;
  _state.stream.Read(unknown);
  _state.stream.Read(_state.data.version);
  if (unknown != U16(0x001A)) {
    LOG_FBX(ERROR, "Byte order mismatch!\n");
    return false;
  }
  if (7500 < _state.data.version) {
    LOG_FBX(ERROR, "Version error! Expected 0x00001D4C ( 7500 ), got 0x%08X\n",
            _state.data.version);
    return false;
  }
  return true;
}

/*******************************************************************************
** is_null_node_header
*******************************************************************************/

inline bool is_null_node_header(NodeHeader & _header) {
  return _header.end_offset == 0u && _header.num_properties == 0u &&
         _header.property_list_len == 0u && _header.name_len == 0u;
}

/*******************************************************************************
** parse_node_header
*******************************************************************************/

inline bool parse_node_header(ParserState & _state, NodeHeader & _node_header) {
  if (7500 <= _state.data.version) {
    if (25u <= _state.stream.Available()) {
      _node_header.end_offset = _state.stream.ReadUint64();
      _node_header.num_properties = _state.stream.ReadUint64();
      _node_header.property_list_len = _state.stream.ReadUint64();
      _node_header.name_len = _state.stream.ReadUint8();
      return !is_null_node_header(_node_header);
    }
  } else {
    if (13u <= _state.stream.Available()) {
      _node_header.end_offset = _state.stream.ReadUint32();
      _node_header.num_properties = _state.stream.ReadUint32();
      _node_header.property_list_len = _state.stream.ReadUint32();
      _node_header.name_len = _state.stream.ReadUint8();
      return !is_null_node_header(_node_header);
    }
  }
  return false;
}

/*******************************************************************************
** parse_child_count
*******************************************************************************/

inline size_t parse_child_count(ParserState & _state) {
  auto const start = _state.stream.Location();
  size_t child_count = 0u;
  NodeHeader header;
  while (parse_node_header(_state, header)) {
    LOG_FBX(VERBOSE,
            "Child %zu: End: %" PRIu64 "; Properties: %" PRIu64
            "; Length: %" PRIu64 ", Name: %" PRIu8 "\n",
            child_count, header.end_offset, header.num_properties,
            header.property_list_len, header.name_len);
    _state.stream.Seek(to_size_t(header.end_offset));
    ++child_count;
  }
  _state.stream.Seek(start);
  return child_count;
}

/*******************************************************************************
** parse_primitive_array
*******************************************************************************/

template<typename T>
inline void parse_primitive_array(ParserState & _state, Property & _property) {
  uint32 size = _state.stream.ReadUint32();
  uint32 encoding = _state.stream.ReadUint32();
  uint32 compressed_length = _state.stream.ReadUint32();
  if (encoding == 0u) {
    core::UniqueArray<T> array = core::make_unique_array<T>(size);
    for (uint32 current = 0u; current < size; ++current) {
      _state.stream.Read(array[current]);
    }
    set(_property, move(array));
  } else {
    zlib::detail::Decoder<1024u * 64u> decoder;
    if (!decoder.Decompress(_state.stream.RemainingBytes(compressed_length))) {
      LOG_FBX(ERROR, "Error while decompressing property content!\n");
      return;
    }
    core::UniqueArray<byte> decompressed = decoder.Acquire();
    CORE_LIB_ASSERT(decompressed.Size() % sizeof(T) == 0u);
    auto data_size = decompressed.Size() / sizeof(T);
    auto data = reinterpret_cast<T *>(decompressed.Release());
    core::UniqueArray<T> array(data, data_size);
    set(_property, move(array));
    _state.stream.Advance(compressed_length);
  }
}

/*******************************************************************************
** parse_property
*******************************************************************************/

inline void parse_property(ParserState & _state, Property & _property) {
  char type_code;
  if (!_state.stream.Read(type_code)) {
    LOG_FBX(VERBOSE, "Failed to read type_code!\n");
    return;
  }
  LOG_FBX(VERBOSE, "Parsing '%c' property..\n", type_code);
  switch (type_code) {
    // Primitives
    case 'Y': {
      set(_property, _state.stream.ReadInt16());
    }
      return;
    case 'C': {
      set(_property, (_state.stream.ReadUint8() != 0));
    }
      return;
    case 'I': {
      set(_property, _state.stream.ReadInt32());
    }
      return;
    case 'F': {
      set(_property, _state.stream.ReadFloat());
    }
      return;
    case 'D': {
      set(_property, _state.stream.ReadDouble());
    }
      return;
    case 'L': {
      set(_property, _state.stream.ReadInt64());
    }
      return;
    // Arrays
    case 'f': {
      parse_primitive_array<float>(_state, _property);
    }
      return;
    case 'd': {
      parse_primitive_array<double>(_state, _property);
    }
      return;
    case 'l': {
      parse_primitive_array<int64>(_state, _property);
    }
      return;
    case 'i': {
      parse_primitive_array<int32>(_state, _property);
    }
      return;
    case 'b': {
      parse_primitive_array<bool>(_state, _property);
    }
      return;
    // String
    case 'S': {
      uint32 size = _state.stream.ReadUint32();
      char const * source =
          reinterpret_cast<char const *>(_state.stream.RemainingBytes().data());
      set(_property, parse_string(source, size));
      _state.stream.Advance(size);
    }
      return;
    // Raw Binary
    case 'R': {
      uint32 size = _state.stream.ReadUint32();
      auto buffer = _state.stream.RemainingBytes(size);
      CORE_LIB_ASSERT(buffer.size() == size);
      auto raw_binary = core::make_unique_array<byte>(size);
      core::memory::copy_bytes(core::make_span(raw_binary), buffer);
      _state.stream.Advance(size);
      set(_property, move(raw_binary));
    }
      return;
    default: {
      CORE_LIB_ASSERT_TEXT(false, "Unknown FBX Property type: %c!\n",
                           type_code);
      CORE_LIB_UNREACHABLE;
    }
      return;
  }
}

size_t parse_children(Node & _parent, ParserState & _state);

/*******************************************************************************
** parse_node
*******************************************************************************/

inline size_t parse_node(Node & _node, ParserState & _state) {
  size_t parsed = 0u;
  size_t header_size = _state.data.version < 7500 ? 13u : 25u;
  uint64 const start = _state.stream.Location();

  NodeHeader header;
  if (parse_node_header(_state, header)) {
    parsed += header_size;
    uint64 const end = header.end_offset;
    CORE_LIB_ASSERT(header.name_len <= _state.stream.Available());
    char const * source =
        reinterpret_cast<char const *>(_state.stream.RemainingBytes().data());
    _node.name = parse_string(source, header.name_len);
    _state.stream.Advance(header.name_len);
    parsed += header.name_len;
    LOG_FBX(VERBOSE,
            "Node: end_offset: %" PRId64 "\tnum_properties: %" PRId64
            "\tproperty_list_len: %" PRId64 "\t"
            "name_len: %" PRId8 "\tname: %s\n",
            header.end_offset, header.num_properties, header.property_list_len,
            header.name_len, cstr(_node.name));

    // parse properties
    // _state.stream.Seek( start + parsed + header.property_list_len ); // Skip properties
    CORE_LIB_ASSERT(header.property_list_len <= _state.stream.Available());
    _node.properties = core::make_unique_array<Property>(
        to_size_t(header.num_properties), in_place_t<Valueless>());
    for (size_t property = 0u; property < header.num_properties; ++property) {
      parse_property(_state, _node.properties[property]);
    }
    CORE_LIB_ASSERT(_state.stream.Location() ==
                    start + parsed + header.property_list_len);
    parsed += to_size_t(header.property_list_len);

    LOG_FBX(VERBOSE, "'%s': from %" PRIu64 " to %" PRIu64 "\n",
            cstr(_node.name), start, end);
    if (start + parsed < end) {
      parsed += parse_children(_node, _state);
      _state.stream.Advance(header_size);
      parsed += header_size;
    }
    CORE_LIB_ASSERT(_state.stream.Location() == end);
    CORE_LIB_ASSERT(start + parsed == end);

    return parsed;
  }
  LOG_FBX(ERROR, "Invalid Node header!\n");
  return 0u;
}

/*******************************************************************************
** parse_children
*******************************************************************************/

inline size_t parse_children(Node & _parent, ParserState & _state) {
  size_t parsed = 0u;
  size_t const child_count = parse_child_count(_state);
  LOG_FBX(VERBOSE, "%s: Parsing %zu children..\n", cstr(_parent.name),
          child_count);
  if (child_count != 0u) {
    _parent.children = core::make_aligned_unique_array<Node>(child_count);
    for (size_t child = 0u; child < child_count; ++child) {
      parsed += parse_node(_parent.children[child], _state);
    }
  }
  return parsed;
}

/*******************************************************************************
** load
*******************************************************************************/

inline core::Optional<Data> load(Stream & _stream) {
  ParserState state(_stream);
  if (!parse_header(state)) {
    LOG_FBX(ERROR, "Error while parsing header!\n");
    return core::nullopt;
  }
  LOG_FBX(VERBOSE, "Version: %" PRIu32 "\n", state.data.version);
  state.data.root.name = "FBXRoot";
  if (0u < parse_children(state.data.root, state)) {
    return core::Optional<Data>(move(state.data));
  }
  return core::nullopt;
}

/*******************************************************************************
** load
*******************************************************************************/

inline core::Optional<Data> load(core::fs::FilePath const & _file_path) {
  core::fs::MappedFile file;
  if (!file.Open(_file_path, core::fs::OpenMode::ReadOnly)) {
    LOG_FBX(VERBOSE, "Failed to open %s!\n", cstr(_file_path));
    return core::nullopt;
  }
  auto file_size = file.GetSize();
  if (file_size == core::fs::InvalidFileSize) {
    LOG_FBX(ERROR, "Invalid file size!\n");
    return core::nullopt;
  }
  auto mapping_reader = file.GetMappingReader(0, file_size);
  if (mapping_reader.Empty()) {
    LOG_FBX(ERROR, "Failed to map file into memory!\n");
    return core::nullopt;
  }
  Stream stream(mapping_reader.Bytes());
  if (auto data = load(stream)) {
    return core::Optional<Data>(move(data));
  }
  LOG_FBX(ERROR, "Failed to load mesh!\n");
  return core::nullopt;
}

/*******************************************************************************
** save
*******************************************************************************/

inline bool save(Data const & /*_data*/,
                 core::fs::FilePath const & /*_file_path*/) {
  CORE_LIB_UNIMPLEMENTED;
  return false;
}

}}}

#undef PRINT_FBX

#endif
