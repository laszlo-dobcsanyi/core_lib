#ifndef _GRAPHICS_RESOURCE_FBX_H_
#define _GRAPHICS_RESOURCE_FBX_H_

#include "core/fs/filepath.h"
#include "core/optional.hpp"
#include "graphics/resource/model_data.hpp"

////////////////////////////////////////////////////////////////////////////////
// graphics fbx
////////////////////////////////////////////////////////////////////////////////

namespace graphics { namespace fbx {

/*******************************************************************************
** load
*******************************************************************************/

core::Optional<resource::ModelData> load(core::fs::FilePath const & _file_path);

}}

#endif