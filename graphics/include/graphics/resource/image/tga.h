#ifndef _GRAPHICS_RESOURCE_IMAGE_TGA_H_
#define _GRAPHICS_RESOURCE_IMAGE_TGA_H_

#include "core/fs/filepath.h"
#include "core/optional.hpp"
#include "graphics/resource/image/image_data.h"

////////////////////////////////////////////////////////////////////////////////
// e tga
////////////////////////////////////////////////////////////////////////////////

namespace graphics { namespace resource { namespace tga {

/*******************************************************************************
** load
*******************************************************************************/

core::Optional<ImageData> load(core::fs::FilePath const & _file_path);

}}}

#endif