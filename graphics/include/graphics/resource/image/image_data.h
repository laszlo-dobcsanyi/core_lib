#ifndef _GRAPHICS_RESOURCE_IMAGE_IMAGE_DATA_H_
#define _GRAPHICS_RESOURCE_IMAGE_IMAGE_DATA_H_

#include "core/memory/aligned_memory.hpp"
#include "graphics/base.hpp"

////////////////////////////////////////////////////////////////////////////////
// graphics resource
////////////////////////////////////////////////////////////////////////////////

namespace graphics { namespace resource {

/*******************************************************************************
** ImageData
*******************************************************************************/

class ImageData {
public:
  ImageData() = default;
  ImageData(uint32 _width, uint32 _height, ColorFormat _format);
  ImageData(uint32 _width, uint32 _height, ColorFormat _format,
            uint32 _row_alignment);
  ImageData(ImageData &&) = default;
  ImageData(ImageData const &) = delete;
  ImageData & operator=(ImageData &&) = default;
  ImageData & operator=(ImageData const &) = delete;
  ~ImageData() = default;

  bool IsNull() const { return data.IsNull(); }
  explicit operator bool() const { return !IsNull(); }

  uint32 Width() const { return width; }
  uint32 Height() const { return height; }
  uint32 RowAlignment() const { return row_alignment; }

  ColorFormat Format() const { return format; }
  void SetFormat(ColorFormat _format);

  uint32 Size() const;
  core::span<byte> GetBytes() { return data.Bytes(); }
  core::span<byte const> GetBytes() const { return data.Bytes(); }

  uint32 RowSize() const;
  core::span<byte> GetRowBytes(uint32 _row);
  core::span<byte const> GetRowBytes(uint32 _row) const;

private:
  uint32 width = 0u;
  uint32 height = 0u;
  uint32 row_alignment = 0u;
  ColorFormat format = ColorFormat::BGR8UI;
  core::AlignedMemory<4u> data;
};

}}

#endif
