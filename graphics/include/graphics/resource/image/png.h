#ifndef _GRAPHICS_RESOURCE_IMAGE_PNG_H_
#define _GRAPHICS_RESOURCE_IMAGE_PNG_H_

#include "core/optional.hpp"
#include "core/fs/filepath.h"
#include "graphics/resource/image/image_data.h"

////////////////////////////////////////////////////////////////////////////////
// e png
////////////////////////////////////////////////////////////////////////////////

namespace graphics { namespace resource { namespace png {

/*******************************************************************************
** load
*******************************************************************************/

core::Optional<ImageData> load(core::fs::FilePath const & _file_path);

}}}

#endif
