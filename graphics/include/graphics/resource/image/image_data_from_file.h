#ifndef _GRAPHICS_RESOURCE_IMAGE_IMAGE_DATA_FROM_FILE_H_
#define _GRAPHICS_RESOURCE_IMAGE_IMAGE_DATA_FROM_FILE_H_

#include "core/fs/filepath.h"
#include "core/optional.hpp"
#include "graphics/resource/image/image_data.h"

////////////////////////////////////////////////////////////////////////////////
// graphics resource
////////////////////////////////////////////////////////////////////////////////

namespace graphics { namespace resource {

/*******************************************************************************
** image_data_from_file
*******************************************************************************/

core::Optional<ImageData>
image_data_from_file(core::fs::FilePath const & _file_path);

}}

#endif