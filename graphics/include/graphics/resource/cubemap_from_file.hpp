#ifndef _GRAPHICS_RESOURCE_CUBEMAP_FROM_FILE_HPP_
#define _GRAPHICS_RESOURCE_CUBEMAP_FROM_FILE_HPP_

#include "graphics/cubemap.h"
#include "graphics/resource/image/image_data_from_file.h"

////////////////////////////////////////////////////////////////////////////////
// graphics resource
////////////////////////////////////////////////////////////////////////////////

namespace graphics { namespace resource {

/*******************************************************************************
** cubemap_from_file
*******************************************************************************/

inline core::Optional<Cubemap>
cubemap_from_file(core::span<core::fs::FilePath> _file_paths) {
  CORE_LIB_ASSERT(_file_paths.size() == 6u);
  container::InlineArray<resource::ImageData, 6u> image_data;
  for (auto current = 0u; current < _file_paths.size(); ++current) {
    if (core::Optional<resource::ImageData> image_data_result =
            image_data_from_file(_file_paths[current])) {
      image_data[current] = move(image_data_result.Value());
    } else {
      LOG_GRAPHICS(WARNING, "Failed to load ImageData from '%s'!\n",
                   cstr(_file_paths[current]));
      return core::nullopt;
    }
  }
  return create_cubemap(core::make_span(image_data));
}

}}

#endif