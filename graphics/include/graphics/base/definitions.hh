#ifndef _GRAPHICS_BASE_DEFINITIONS_HH_
#define _GRAPHICS_BASE_DEFINITIONS_HH_

#ifndef CORE_GRAPHICS_BASE
#  error
#endif

#if !defined(CORE_GRAPHICS_GLX) && !defined(CORE_GRAPHICS_WGL) &&              \
    !defined(CORE_GRAPHICS_DIRECTX)
#  error No graphics provider is specified!
#endif

#if defined(CORE_GRAPHICS_OPENGL)
#  if !defined(CORE_GRAPHICS_GLX) && !defined(CORE_GRAPHICS_WGL)
#    error No OpenGL provider is specified!
#  endif
#endif

#if !defined(CORE_GRAPHICS_X11) && !defined(CORE_GRAPHICS_WINAPI)
#  error No window provider is specified!
#endif

#endif