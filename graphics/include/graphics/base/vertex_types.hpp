#ifndef _GRAPHICS_BASE_VERTEX_TYPES_HPP_
#define _GRAPHICS_BASE_VERTEX_TYPES_HPP_

#if !defined(CORE_GRAPHICS_BASE)
#  error
#endif

////////////////////////////////////////////////////////////////////////////////
// graphics
////////////////////////////////////////////////////////////////////////////////

namespace graphics {

/*******************************************************************************
** bind_attributes
*******************************************************************************/

struct bind_attributes_t {
  template<typename VertexType, typename VertexAttributeBinder>
  void operator()(mpl::type<VertexType>, VertexAttributeBinder _binder) const {
    tag_invoke(*this, mpl::type<VertexType>(), _binder);
  }
};
constexpr bind_attributes_t bind_attributes;

/*******************************************************************************
** PositionVertex
*******************************************************************************/

struct PositionVertex {
  numerics::Vector3f position;

  PositionVertex() = default;
  explicit PositionVertex(numerics::Vector3f _position)
      : position(_position) {}
  PositionVertex(PositionVertex &&) = default;
  PositionVertex(PositionVertex const &) = default;
  PositionVertex & operator=(PositionVertex &&) = default;
  PositionVertex & operator=(PositionVertex const &) = default;
  ~PositionVertex() = default;

private:
  template<typename VertexAttributeBinder>
  friend void tag_invoke(bind_attributes_t, mpl::type<PositionVertex>,
                         VertexAttributeBinder & _binder) {
    _binder.Bind(&PositionVertex::position);
  }
};

/*******************************************************************************
** ColoredVertex
*******************************************************************************/

struct ColoredVertex {
  numerics::Vector3f position;
  numerics::Vector4f color;

  ColoredVertex() = default;
  ColoredVertex(numerics::Vector3f _position, numerics::Vector4f _color)
      : position(_position)
      , color(_color) {}
  ColoredVertex(ColoredVertex &&) = default;
  ColoredVertex(ColoredVertex const &) = default;
  ColoredVertex & operator=(ColoredVertex &&) = default;
  ColoredVertex & operator=(ColoredVertex const &) = default;
  ~ColoredVertex() = default;

private:
  template<typename VertexAttributeBinder>
  friend void tag_invoke(bind_attributes_t, mpl::type<ColoredVertex>,
                         VertexAttributeBinder & _binder) {
    _binder.Bind(&ColoredVertex::position).Bind(&ColoredVertex::color);
  }
};

/*******************************************************************************
** TexturedVertex
*******************************************************************************/

struct TexturedVertex {
  numerics::Vector3f position;
  numerics::Vector3f normal;
  numerics::Vector2f texture_coordinate;

  TexturedVertex() = default;
  TexturedVertex(numerics::Vector3f _position, numerics::Vector3f _normal,
                 numerics::Vector2f _texture_coordinate)
      : position(_position)
      , normal(_normal)
      , texture_coordinate(_texture_coordinate) {}
  TexturedVertex(TexturedVertex &&) = default;
  TexturedVertex(TexturedVertex const &) = default;
  TexturedVertex & operator=(TexturedVertex &&) = default;
  TexturedVertex & operator=(TexturedVertex const &) = default;
  ~TexturedVertex() = default;

private:
  template<typename VertexAttributeBinder>
  friend void tag_invoke(bind_attributes_t, mpl::type<TexturedVertex>,
                         VertexAttributeBinder & _binder) {
    _binder.Bind(&TexturedVertex::position)
        .Bind(&TexturedVertex::normal)
        .Bind(&TexturedVertex::texture_coordinate);
  }
};

/*******************************************************************************
** SkinnedVertex
*******************************************************************************/

struct SkinnedVertex {
  numerics::Vector3f position;
  numerics::Vector3f normal;
  numerics::Vector2f texture_coordinate;
  uint32 bone_indices;
  container::InlineArray<float, 4u> bone_weights;

  SkinnedVertex() = default;
  SkinnedVertex(numerics::Vector3f _position, numerics::Vector3f _normal,
                numerics::Vector2f _texture_coordinate, uint32 _bone_indices,
                container::InlineArray<float, 4u> const & _bone_weights)
      : position(_position)
      , normal(_normal)
      , texture_coordinate(_texture_coordinate)
      , bone_indices(_bone_indices)
      , bone_weights(_bone_weights) {}
  SkinnedVertex(SkinnedVertex &&) = default;
  SkinnedVertex(SkinnedVertex const &) = default;
  SkinnedVertex & operator=(SkinnedVertex &&) = default;
  SkinnedVertex & operator=(SkinnedVertex const &) = default;
  ~SkinnedVertex() = default;

private:
  template<typename VertexAttributeBinder>
  friend void tag_invoke(bind_attributes_t, mpl::type<SkinnedVertex>,
                         VertexAttributeBinder & _binder) {
    _binder.Bind(&SkinnedVertex::position)
        .Bind(&SkinnedVertex::normal)
        .Bind(&SkinnedVertex::texture_coordinate)
        .Bind(&SkinnedVertex::bone_indices)
        .Bind(&SkinnedVertex::bone_weights);
  }
};

}

#endif
