#ifndef _GRAPHICS_BASE_VERTEX_DATA_HPP_
#define _GRAPHICS_BASE_VERTEX_DATA_HPP_

#if !defined(CORE_GRAPHICS_BASE)
#  error
#endif

////////////////////////////////////////////////////////////////////////////////
// graphics
////////////////////////////////////////////////////////////////////////////////

namespace graphics {

/*******************************************************************************
** VertexDataType
*******************************************************************************/

enum class VertexDataType {
  Array,
  Indexed,
};

/*******************************************************************************
** VertexData
*******************************************************************************/

template<typename Vertex, VertexDataType>
struct VertexData;

/*******************************************************************************
** ArrayVertexData
*******************************************************************************/

template<typename Vertex>
struct VertexData<Vertex, VertexDataType::Array> {
  core::UniqueArray<Vertex> vertices;
};

template<typename Vertex>
using ArrayVertexData = VertexData<Vertex, VertexDataType::Array>;

/*******************************************************************************
** IndexedVertexData
*******************************************************************************/

template<typename Vertex>
struct VertexData<Vertex, VertexDataType::Indexed> {
  core::UniqueArray<Vertex> vertices;
  core::UniqueArray<uint32> indices;
};

template<typename Vertex>
using IndexedVertexData = VertexData<Vertex, VertexDataType::Indexed>;

/*******************************************************************************
** VertexDataView
*******************************************************************************/

template<typename Vertex, VertexDataType>
struct VertexDataView;

/*******************************************************************************
** ArrayVertexDataView
*******************************************************************************/

template<typename Vertex>
struct VertexDataView<Vertex, VertexDataType::Array> {
  core::span<const Vertex> vertices;

  VertexDataView() = default;
  VertexDataView(ArrayVertexData<Vertex> const & _vertex_data)
      : vertices(core::make_span(_vertex_data.vertices)) {}
  VertexDataView(core::span<const Vertex> _vertices)
      : vertices(_vertices) {}
  VertexDataView(VertexDataView &&) = default;
  VertexDataView(VertexDataView const &) = default;
  VertexDataView & operator=(VertexDataView &&) = default;
  VertexDataView & operator=(VertexDataView const &) = default;
  ~VertexDataView() = default;
};

template<typename Vertex>
using ArrayVertexDataView = VertexDataView<Vertex, VertexDataType::Array>;

/*******************************************************************************
** IndexedVertexDataView
*******************************************************************************/

template<typename Vertex>
struct VertexDataView<Vertex, VertexDataType::Indexed> {
  core::span<const Vertex> vertices;
  core::span<const uint32> indices;

  VertexDataView() = default;
  VertexDataView(IndexedVertexData<Vertex> const & _vertex_data)
      : vertices(core::make_span(_vertex_data.vertices))
      , indices(core::make_span(_vertex_data.indices)) {}
  VertexDataView(core::span<const Vertex> _vertices,
                 core::span<const uint32> _indices)
      : vertices(_vertices)
      , indices(_indices) {}
  VertexDataView(VertexDataView &&) = default;
  VertexDataView(VertexDataView const &) = default;
  VertexDataView & operator=(VertexDataView &&) = default;
  VertexDataView & operator=(VertexDataView const &) = default;
  ~VertexDataView() = default;
};

template<typename Vertex>
using IndexedVertexDataView = VertexDataView<Vertex, VertexDataType::Indexed>;

}

#endif
