#ifndef _GRAPHICS_CUBEMAP_H_
#define _GRAPHICS_CUBEMAP_H_

#include "graphics/base.hpp"

#if defined(CORE_GRAPHICS_OPENGL)
#  include "graphics/implementation/gl/cubemap.h"
#elif defined(CORE_GRAPHICS_DIRECTX)
#  error
#  include "graphics/implementation/dx/cubemap.h"
#else
#  error
#endif

////////////////////////////////////////////////////////////////////////////////
// graphics
////////////////////////////////////////////////////////////////////////////////

namespace graphics {

/*******************************************************************************
** Cubemap
*******************************************************************************/

#if defined(CORE_GRAPHICS_OPENGL)
using Cubemap = opengl::Cubemap;
using opengl::create_cubemap;
#elif defined(CORE_GRAPHICS_DIRECTX)
#  error
using Cubemap = directx::Cubemap;
using directx::create_cubemap;
#else
#  error
#endif

}

#endif
