#ifndef _GRAPHICS_RENDERER_HPP_
#define _GRAPHICS_RENDERER_HPP_

#include "graphics/base.hpp"

#include "graphics/shader.h"
#include "graphics/reflect_uniforms.hpp"
#include "graphics/mesh.hpp"

////////////////////////////////////////////////////////////////////////////////
// graphics
////////////////////////////////////////////////////////////////////////////////

namespace graphics {

/*******************************************************************************
** bind
*******************************************************************************/

struct bind_t {
  template<class T, class... Args>
  void operator()(T && _value, Args &&... _args) const {
    tag_invoke(bind_t(), forward<T>(_value), forward<Args>(_args)...);
  }
};
constexpr bind_t bind;

/*******************************************************************************
** render
*******************************************************************************/

struct render_t {
  template<class T, class... Args>
  void operator()(T && _value, Args &&... _args) const {
    tag_invoke(render_t(), forward<T>(_value), forward<Args>(_args)...);
  }
};
constexpr render_t render;

/*******************************************************************************
** RendererBase
*******************************************************************************/

template<typename Uniforms>
struct RendererBase {
  Shader shader;
  implementation::UniformLocations<Uniforms> uniform_locations;
};

/*******************************************************************************
** render_mesh
*******************************************************************************/

template<class Uniforms, typename Vertex, VertexDataType DataType>
inline void render_mesh(RendererBase<Uniforms> const & _renderer,
                        Uniforms const & _uniforms,
                        Mesh<Vertex, DataType> const & _mesh, DrawMode _mode) {
  implementation::set_uniforms(_renderer, _uniforms);
  implementation::draw(_mesh, _mode);
}

}

#endif
