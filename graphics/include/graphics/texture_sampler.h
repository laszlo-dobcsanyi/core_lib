#ifndef _GRAPHICS_TEXTURE_SAMPLER_H_
#define _GRAPHICS_TEXTURE_SAMPLER_H_

#include "graphics/base.hpp"

#if defined(CORE_GRAPHICS_OPENGL)
#  include "graphics/implementation/gl/texture_sampler.h"
#elif defined(CORE_GRAPHICS_DIRECTX)
#  error
#  include "graphics/implementation/dx/texture_sampler.h"
#else
#  error
#endif

////////////////////////////////////////////////////////////////////////////////
// graphics
////////////////////////////////////////////////////////////////////////////////

namespace graphics {

/*******************************************************************************
** TextureSampler
*******************************************************************************/

#if defined(CORE_GRAPHICS_OPENGL)
using TextureSampler = opengl::TextureSampler;
using opengl::create_texture_sampler;
#elif defined(CORE_GRAPHICS_DIRECTX)
#  error
using TextureSampler = directx::TextureSampler;
using directx::create_texture_sampler;
#else
#endif

}

#endif
