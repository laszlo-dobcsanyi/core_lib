#ifndef _GRAPHICS_TEXTURE_H_
#define _GRAPHICS_TEXTURE_H_

#include "graphics/base.hpp"

#if defined(CORE_GRAPHICS_OPENGL)
#  include "graphics/implementation/gl/texture.h"
#elif defined(CORE_GRAPHICS_DIRECTX)
#  error
#  include "graphics/implementation/dx/texture.h"
#else
#  error
#endif

////////////////////////////////////////////////////////////////////////////////
// graphics
////////////////////////////////////////////////////////////////////////////////

namespace graphics {

/*******************************************************************************
** Texture
*******************************************************************************/

#if defined(CORE_GRAPHICS_OPENGL)
using TextureUnit = opengl::TextureUnit;
using Texture = opengl::Texture;
using opengl::create_texture;
#elif defined(CORE_GRAPHICS_DIRECTX)
using TextureUnit = directx::TextureUnit;
using Texture = directx::Texture;
using directx::create_texture;
#else
#  error
#endif

}

#endif
