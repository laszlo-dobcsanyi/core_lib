#ifndef _GRAPHICS_SHADER_H_
#define _GRAPHICS_SHADER_H_

#include "graphics/base.hpp"

#if defined(CORE_GRAPHICS_OPENGL)
#  include "graphics/implementation/gl/shader.h"
#elif defined(CORE_GRAPHICS_DIRECTX)
#  include "graphics/implementation/dx/shader.h"
#else
#  error
#endif

////////////////////////////////////////////////////////////////////////////////
// graphics
////////////////////////////////////////////////////////////////////////////////

namespace graphics {

/*******************************************************************************
** Shader
*******************************************************************************/

#if defined(CORE_GRAPHICS_OPENGL)
using Shader = opengl::Shader;
#elif defined(CORE_GRAPHICS_DIRECTX)
using Shader = directx::Shader;
#else
#  error
#endif

}

#endif
