#ifndef _GRAPHICS_API_DIRECTX_DIRECTX_H_
#define _GRAPHICS_API_DIRECTX_DIRECTX_H_

#ifndef CORE_GRAPHICS_DIRECTX
#  error
#endif

////////////////////////////////////////////////////////////////////////////////
// graphics directx
////////////////////////////////////////////////////////////////////////////////

namespace graphics { namespace directx {

bool Initialize();
bool IsInitialized();
void Finalize();

}}

#endif
