#ifndef _GRAPHICS_API_CONTEXT_H_
#define _GRAPHICS_API_CONTEXT_H_

#include "core/base.h"

////////////////////////////////////////////////////////////////////////////////
// graphics api
////////////////////////////////////////////////////////////////////////////////

namespace graphics { namespace api {

/*******************************************************************************
** Context
*******************************************************************************/

class Context final {
public:
  Context() = default;
  Context(Context &&) = delete;
  Context(Context const &) = delete;
  Context & operator=(Context &&) = delete;
  Context & operator=(Context const &) = delete;
  ~Context();

  bool Initialize();
  bool IsInitialized();
  explicit operator bool();
  void Finalize();

private:
  static Mutex state_mutex;
  static uint32 state_counter;
  bool has_state = false;
};

}}

#endif
