#ifndef _GRAPHICS_API_X11_CONTEXT_HH_
#define _GRAPHICS_API_X11_CONTEXT_HH_

#include "core/optional.hpp"

////////////////////////////////////////////////////////////////////////////////
// 1 context
////////////////////////////////////////////////////////////////////////////////

namespace graphics { namespace api { namespace x11 {

struct Context {
  ::Display * display = nullptr;
  ::Window root_window;
  ::Screen * screen = nullptr;
  int screen_id;
  Atom wm_protocols_atom;
  Atom wm_delete_window_atom;
  Key scancode_map[256];

  Key key_from_scancode(unsigned int _scancode);
  Key scancode_to_key(unsigned int _scancode);
};

core::Optional<Context> create_context();
void destroy(Context & _context);

}}}

#endif
