#ifndef _GRAPHICS_API_X11_H_
#define _GRAPHICS_API_X11_H_

#include "graphics/base.hpp"
#include "core/library.hpp"

#ifndef CORE_GRAPHICS_GLX
#  error
#endif

#define XLIB_ILLEGAL_ACCESS //  :)
#include <X11/X.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>

#undef None

#define LOG_X11(__severity__, ...) CORE_LIB_LOG(__severity__, __VA_ARGS__);

#include "graphics/api/x11/context.hh"

////////////////////////////////////////////////////////////////////////////////
// graphics
////////////////////////////////////////////////////////////////////////////////

namespace graphics {

#define X11_FUNCTION(NAME) extern core::LibraryFunction<decltype(&::NAME)> NAME
#include "graphics/api/x11/symbols.inl"
#undef X11_FUNCTION

}

////////////////////////////////////////////////////////////////////////////////
// i x11
////////////////////////////////////////////////////////////////////////////////

namespace graphics { namespace api { namespace x11 {

bool Initialize();
bool IsInitialized();
void Finalize();

}}}

#endif
