#if !defined(X11_FUNCTION)
#  define X11_FUNCTION(NAME)
#endif // X11_FUNCTION

/*******************************************************************************
** Imported
*******************************************************************************/

X11_FUNCTION(XOpenDisplay);
X11_FUNCTION(XDefaultRootWindow);
X11_FUNCTION(XStoreName);
X11_FUNCTION(XMapWindow);
X11_FUNCTION(XCreateWindow);
X11_FUNCTION(XCreateColormap);
X11_FUNCTION(XGetKeyboardMapping);

X11_FUNCTION(XNextEvent);
X11_FUNCTION(XEventsQueued);
X11_FUNCTION(XDestroyWindow);
X11_FUNCTION(XGetWindowAttributes);
X11_FUNCTION(XCloseDisplay);
X11_FUNCTION(XConnectionNumber);

X11_FUNCTION(XFree);
X11_FUNCTION(XPending);
X11_FUNCTION(XFlush);
X11_FUNCTION(XSetWMProtocols);
X11_FUNCTION(XInternAtom);

X11_FUNCTION(XGetInputFocus);
#if 0
X11_FUNCTION( XGrabPointer );
X11_FUNCTION( XUngrabPointer );
#endif
X11_FUNCTION(XQueryPointer);
X11_FUNCTION(XWarpPointer);

#if defined(CORE_LIB_CONFIGURATION_DEBUG)
X11_FUNCTION(XSetErrorHandler);
X11_FUNCTION(XGetErrorText);
X11_FUNCTION(XSetIOErrorHandler);
#endif

#undef X11_FUNCTION
