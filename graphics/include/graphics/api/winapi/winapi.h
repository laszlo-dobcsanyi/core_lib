#ifndef _GRAPHICS_API_WINAPI_WINAPI_H_
#define _GRAPHICS_API_WINAPI_WINAPI_H_

#include "graphics/base.hpp"

#ifndef CORE_GRAPHICS_WINAPI
#  error
#endif

#define LOG_WINAPI(__severity__, ...) CORE_LIB_LOG(__severity__, __VA_ARGS__);

#include "graphics/api/winapi/keyboard.hh"

////////////////////////////////////////////////////////////////////////////////
// i winapi
////////////////////////////////////////////////////////////////////////////////

namespace graphics { namespace api { namespace winapi {

bool Initialize();
bool IsInitialized();
void Finalize();

}}}

#endif
