#ifndef _GRAPHICS_API_WINAPI_KEYBOARD_HH_
#define _GRAPHICS_API_WINAPI_KEYBOARD_HH_

////////////////////////////////////////////////////////////////////////////////
// i keyboard
////////////////////////////////////////////////////////////////////////////////

namespace graphics { namespace api { namespace winapi { namespace keyboard {

Key key_from_scancode(int _scancode);

bool Initialize();
bool IsInitialized();
void Finalize();

}}}}

#endif
