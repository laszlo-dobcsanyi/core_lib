#ifndef _GRAPHICS_API_API_H_
#define _GRAPHICS_API_API_H_

#include "graphics/base.hpp"

#if defined(CORE_GRAPHICS_X11)
#  include "graphics/api/x11/x11.h"
#elif defined(CORE_GRAPHICS_WINAPI)
#  include "graphics/api/winapi/winapi.h"
#endif

#if defined(CORE_GRAPHICS_OPENGL)
#  include "graphics/api/opengl/opengl.h"
#elif defined(CORE_GRAPHICS_DIRECTX)
#  include "graphics/api/directx/directx.h"
#endif

////////////////////////////////////////////////////////////////////////////////
// graphics api
////////////////////////////////////////////////////////////////////////////////

namespace graphics { namespace api {

bool Initialize();
bool IsInitialized();
void Finalize();

}}

#endif
