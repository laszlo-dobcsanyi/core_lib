#ifndef _GRAPHICS_API_OPENGL_FUNCTION_HPP_
#define _GRAPHICS_API_OPENGL_FUNCTION_HPP_

#include "core/library.hpp"

#if defined(CORE_LIB_CONFIGURATION_DEBUG)

////////////////////////////////////////////////////////////////////////////////
// y debug
////////////////////////////////////////////////////////////////////////////////

namespace graphics { namespace api { namespace opengl { namespace library {
namespace debug {

/*******************************************************************************
** Function
*******************************************************************************/

template<class F, typename R, typename Args>
class Function;

template<class F, typename R, typename... Args>
class Function<F, R, mpl::TypePack<Args...>> final
    : public core::DynamicFunction<F, R, mpl::TypePack<Args...>> {
public:
  using Base = core::DynamicFunction<F, R, mpl::TypePack<Args...>>;
  Function() = default;
  Function(Function &&) = default;
  Function(Function const &) = delete;
  Function & operator=(Function &&) = default;
  Function & operator=(Function const &) = delete;
  ~Function() = default;

  Function & operator=(F _fptr) {
    Base::operator=(_fptr);
    return *this;
  }

  template<typename ResultType = R>
  mpl::if_void_t<ResultType, ResultType> operator()(Args... _args) {
    CheckErrors();
    Base::operator()(forward<Args>(_args)...);
    CheckErrors();
  }

  template<typename ResultType = R>
  mpl::if_not_void_t<ResultType, ResultType> operator()(Args... _args) {
    CheckErrors();
    auto result = Base::operator()(forward<Args>(_args)...);
    CheckErrors();
    return result;
  }

protected:
  void CheckErrors() {
#  if defined(CORE_GRAPHICS_GLX)
    glx::CheckErrors();
#  elif defined(CORE_GRAPHICS_WGL)
    wgl::CheckErrors();
#  endif
  }
};

}}}}}

#endif

////////////////////////////////////////////////////////////////////////////////
// l library
////////////////////////////////////////////////////////////////////////////////

namespace graphics { namespace api { namespace opengl { namespace library {

#if defined(CORE_LIB_CONFIGURATION_DEBUG)

template<class F>
using Function =
    debug::Function<F, mpl::result_of_t<F>, mpl::parameters_of_t<F>>;

#else

template<class F>
using Function = core::LibraryFunction<F>;

#endif

}}}}

#endif