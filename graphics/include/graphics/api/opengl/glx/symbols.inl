#if !defined(GLX_EXPORTED_FUNCTION)
#  define GLX_EXPORTED_FUNCTION(NAME)
#endif // GLX_EXPORTED_FUNCTION

#if !defined(GLX_ARB_LOADED_FUNCTION)
#  define GLX_ARB_LOADED_FUNCTION(NAME, PTR)
#endif // GLX_ARB_LOADED_FUNCTION

/*******************************************************************************
** Imported
*******************************************************************************/

GLX_EXPORTED_FUNCTION(glXMakeCurrent);
GLX_EXPORTED_FUNCTION(glXGetCurrentContext);
GLX_EXPORTED_FUNCTION(glXGetVisualFromFBConfig);
GLX_EXPORTED_FUNCTION(glXGetFBConfigAttrib);
GLX_EXPORTED_FUNCTION(glXDestroyContext);
GLX_EXPORTED_FUNCTION(glXSwapBuffers);
GLX_EXPORTED_FUNCTION(glXQueryVersion);
GLX_EXPORTED_FUNCTION(glXChooseFBConfig);

GLX_ARB_LOADED_FUNCTION(glXCreateContextAttribsARB, GLXCREATECONTEXTATTRIBSARB);

#undef GLX_ARB_LOADED_FUNCTION
#undef GLX_EXPORTED_FUNCTION