#ifndef _GRAPHICS_API_OPENGL_GLX_GLX_H_
#define _GRAPHICS_API_OPENGL_GLX_GLX_H_

#include <GL/gl.h>
#include <GL/glext.h>
#include <GL/glx.h>

#include "core/library.hpp"
#include "core/optional.hpp"

#include "graphics/api/x11/x11.h"

////////////////////////////////////////////////////////////////////////////////
// graphics
////////////////////////////////////////////////////////////////////////////////

namespace graphics {

#define GLX_EXPORTED_FUNCTION(NAME)                                            \
  extern core::LibraryFunction<decltype(&::NAME)> NAME
#define GLX_ARB_LOADED_FUNCTION(NAME, PTR)                                     \
  extern core::LibraryFunction<PFN##PTR##PROC> NAME
#include "graphics/api/opengl/glx/symbols.inl"

}

////////////////////////////////////////////////////////////////////////////////
// x window
////////////////////////////////////////////////////////////////////////////////

namespace graphics { namespace api { namespace opengl { namespace glx {
namespace window {

struct Context {
  ::Window window;
  ::GLXContext glx_context;
  x11::Context x11_context;
};

core::Optional<Context>
create_context(WindowParameters const & _window_parameters,
               RenderContextParameters const & _context_parameters);
void destroy(Context & _context);

}}}}}

////////////////////////////////////////////////////////////////////////////////
// l glx
////////////////////////////////////////////////////////////////////////////////

namespace graphics { namespace api { namespace opengl { namespace glx {

void CheckErrors();

bool Initialize();
bool IsInitialized();
void Finalize();

}}}}

#endif