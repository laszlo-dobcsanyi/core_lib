#if !defined(WGL_EXPORTED_FUNCTION)
#  define WGL_EXPORTED_FUNCTION(NAME)
#endif // WGL_EXPORTED_FUNCTION

#if !defined(WGL_ARB_LOADED_FUNCTION)
#  define WGL_ARB_LOADED_FUNCTION(NAME, PTR)
#endif // WGL_ARB_LOADED_FUNCTION

/*******************************************************************************
** Imported
*******************************************************************************/

WGL_EXPORTED_FUNCTION(wglCreateContext);
WGL_EXPORTED_FUNCTION(wglDeleteContext);
WGL_EXPORTED_FUNCTION(wglGetCurrentDC);
WGL_EXPORTED_FUNCTION(wglGetCurrentContext);
WGL_EXPORTED_FUNCTION(wglMakeCurrent);

WGL_ARB_LOADED_FUNCTION(wglCreateContextAttribsARB, WGLCREATECONTEXTATTRIBSARB);

#undef WGL_ARB_LOADED_FUNCTION
#undef WGL_EXPORTED_FUNCTION