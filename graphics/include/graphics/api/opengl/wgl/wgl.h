#ifndef _GRAPHICS_API_OPENGL_WGL_WGL_H_
#define _GRAPHICS_API_OPENGL_WGL_WGL_H_

#include <GL/gl.h>
#include "graphics/api/opengl/glext.h"
#include "graphics/api/opengl/wglext.h"

#include "core/callable/delegate.hpp"
#include "core/library.hpp"

////////////////////////////////////////////////////////////////////////////////
// graphics
////////////////////////////////////////////////////////////////////////////////

namespace graphics {

#define WGL_EXPORTED_FUNCTION(NAME)                                            \
  extern core::LibraryFunction<decltype(&::NAME)> NAME
#define WGL_ARB_LOADED_FUNCTION(NAME, PTR)                                     \
  extern core::LibraryFunction<PFN##PTR##PROC> NAME
#include "graphics/api/opengl/wgl/symbols.inl"

}

////////////////////////////////////////////////////////////////////////////////
// l wgl
////////////////////////////////////////////////////////////////////////////////

namespace graphics { namespace api { namespace opengl { namespace wgl {
namespace window {

using WindowDelegate = core::Delegate<LRESULT(HWND, UINT, WPARAM, LPARAM)>;

HWND create(WindowParameters const & _window_parameters,
            RenderContextParameters const & _context_parameters,
            WindowDelegate & _delegate);
void destroy(HWND _window_handle);

}}}}}

////////////////////////////////////////////////////////////////////////////////
// l wgl
////////////////////////////////////////////////////////////////////////////////

namespace graphics { namespace api { namespace opengl { namespace wgl {

void CheckErrors();

bool Initialize();
bool IsInitialized();
void Finalize();

}}}}

#endif