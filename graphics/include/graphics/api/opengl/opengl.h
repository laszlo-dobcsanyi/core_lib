#ifndef _GRAPHICS_API_OPENGL_OPENGL_H_
#define _GRAPHICS_API_OPENGL_OPENGL_H_

#include "graphics/base.hpp"

#ifndef CORE_GRAPHICS_OPENGL
#  error
#endif

#define LOG_GRAPHICS_OPENGL(__severity__, ...)                                 \
  CORE_LIB_LOG(__severity__, __VA_ARGS__)

#if defined(CORE_GRAPHICS_GLX)
#  include "graphics/api/opengl/glx/glx.h"
#elif defined(CORE_GRAPHICS_WGL)
#  include "graphics/api/opengl/wgl/wgl.h"
#endif

#include "graphics/api/opengl/function.hpp"
#include "graphics/api/opengl/symbols.hh"

////////////////////////////////////////////////////////////////////////////////
// i opengl
////////////////////////////////////////////////////////////////////////////////

namespace graphics { namespace api { namespace opengl {

bool Initialize();
bool IsInitialized();
void Finalize();

}}}

#endif
