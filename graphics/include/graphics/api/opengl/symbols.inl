#undef PTRDEF
#define PTRDEF(PTR) PFN##PTR##PROC

/*******************************************************************************
** GL_EXPORTED_FUNCTION
*******************************************************************************/

#if defined(DECLARE_SYMBOLS)
#  define GL_EXPORTED_FUNCTION(NAME)                                           \
    extern graphics::api::opengl::library::Function<decltype(&::NAME)> NAME
#endif // DECLARE_SYMBOLS

#if defined(DEFINE_SYMBOLS)
#  define GL_EXPORTED_FUNCTION(NAME)                                           \
    graphics::api::opengl::library::Function<decltype(&::NAME)> NAME
#endif // DEFINE_SYMBOLS

#if defined(LOAD_SYMBOLS)
#  define GL_EXPORTED_FUNCTION(NAME)                                           \
    LOG_GRAPHICS_OPENGL(VERBOSE, "Loading exported function '%s'..\n",         \
                        CORE_LIB_TO_STRING(NAME));                             \
    if (!NAME.Load(opengl_library, const_string(CORE_LIB_TO_STRING(NAME))))    \
    return false
#endif // LOAD_SYMBOLS

#if defined(UNLOAD_SYMBOLS)
#  define GL_EXPORTED_FUNCTION(NAME) NAME.Unload();
#endif // UNLOAD_SYMBOLS

#if !defined(GL_EXPORTED_FUNCTION)
#  error
#endif // GL_EXPORTED_FUNCTION

/*******************************************************************************
** GL_LOADED_FUNCTION
*******************************************************************************/

#if defined(DECLARE_SYMBOLS)
#  define GL_LOADED_FUNCTION(NAME, PTR)                                        \
    using __##NAME##_Type = PTRDEF(PTR);                                       \
    extern graphics::api::opengl::library::Function<PTRDEF(PTR)> NAME
#endif // DECLARE_SYMBOLS

#if defined(DEFINE_SYMBOLS)
#  define GL_LOADED_FUNCTION(NAME, PTR)                                        \
    graphics::api::opengl::library::Function<PTRDEF(PTR)> NAME
#endif // DEFINE_SYMBOLS

#if defined(LOAD_SYMBOLS)
#  define GL_LOADED_FUNCTION(NAME, PTR)                                        \
    LOG_GRAPHICS_OPENGL(VERBOSE, "Loading symbol '%s'..\n",                    \
                        CORE_LIB_TO_STRING(NAME));                             \
    NAME = (__##NAME##_Type)get_process(CORE_LIB_TO_STRING(NAME));             \
    if (!NAME) {                                                               \
      LOG_GRAPHICS_OPENGL(ERROR, "Failed to load symbol '%s'!\n");             \
      return false;                                                            \
    }
#endif // LOAD_SYMBOLS

#if defined(UNLOAD_SYMBOLS)
#  define GL_LOADED_FUNCTION(NAME, PTR) NAME.Unload();
#endif // UNLOAD_SYMBOLS

#if !defined(GL_LOADED_FUNCTION)
#  error
#endif // GL_LOADED_FUNCTION

/*******************************************************************************
** GL_ARB_LOADED_FUNCTION
*******************************************************************************/

#if defined(DECLARE_SYMBOLS)
#  define GL_ARB_LOADED_FUNCTION(NAME, PTR)                                    \
    using __##NAME##_Type = PTRDEF(PTR);                                       \
    extern graphics::api::opengl::library::Function<PTRDEF(PTR)> NAME
#endif // DECLARE_SYMBOLS

#if defined(DEFINE_SYMBOLS)
#  define GL_ARB_LOADED_FUNCTION(NAME, PTR)                                    \
    graphics::api::opengl::library::Function<PTRDEF(PTR)> NAME
#endif // DEFINE_SYMBOLS

#if defined(LOAD_SYMBOLS)
#  define GL_ARB_LOADED_FUNCTION(NAME, PTR)                                    \
    LOG_GRAPHICS_OPENGL(VERBOSE, "Loading symbol '%s'..\n",                    \
                        CORE_LIB_TO_STRING(NAME));                             \
    NAME = (__##NAME##_Type)get_process_arb(CORE_LIB_TO_STRING(NAME));         \
    if (!NAME) {                                                               \
      LOG_GRAPHICS_OPENGL(ERROR, "Failed to load symbol '%s'!\n",              \
                          CORE_LIB_TO_STRING(NAME));                           \
      return false;                                                            \
    }
#endif // LOAD_SYMBOLS

#if defined(UNLOAD_SYMBOLS)
#  define GL_ARB_LOADED_FUNCTION(NAME, PTR) NAME.Unload();
#endif // UNLOAD_SYMBOLS

#if !defined(GL_ARB_LOADED_FUNCTION)
#  error
#endif // GL_ARB_LOADED_FUNCTION

/*******************************************************************************
** Imported
*******************************************************************************/

// Misc
GL_EXPORTED_FUNCTION(glGetIntegerv);
GL_EXPORTED_FUNCTION(glViewport);
GL_EXPORTED_FUNCTION(glDepthRange);
GL_EXPORTED_FUNCTION(glClearColor);
GL_EXPORTED_FUNCTION(glClear);
// GL_EXPORTED_FUNCTION( glGetString );
GL_EXPORTED_FUNCTION(glEnable);
GL_EXPORTED_FUNCTION(glDisable);
GL_EXPORTED_FUNCTION(glDepthFunc);
GL_EXPORTED_FUNCTION(glBlendFunc);
GL_EXPORTED_FUNCTION(glCullFace);
GL_EXPORTED_FUNCTION(glPolygonMode);

// Texture
GL_EXPORTED_FUNCTION(glGenTextures);
GL_EXPORTED_FUNCTION(glDeleteTextures);
GL_EXPORTED_FUNCTION(glBindTexture);
// GL_EXPORTED_FUNCTION( glPixelStorei );
GL_EXPORTED_FUNCTION(glTexParameteri);
GL_EXPORTED_FUNCTION(glTexImage2D);
GL_EXPORTED_FUNCTION(glTexSubImage2D);
GL_LOADED_FUNCTION(glGenerateMipmap, GLGENERATEMIPMAP);

// Draw
GL_EXPORTED_FUNCTION(glDrawElements);
GL_EXPORTED_FUNCTION(glDrawArrays);

// Shader
GL_LOADED_FUNCTION(glCreateShader, GLCREATESHADER);
GL_LOADED_FUNCTION(glShaderSource, GLSHADERSOURCE);
GL_LOADED_FUNCTION(glCompileShader, GLCOMPILESHADER);
GL_LOADED_FUNCTION(glGetShaderiv, GLGETSHADERIV);
GL_LOADED_FUNCTION(glDeleteShader, GLDELETESHADER);
GL_LOADED_FUNCTION(glGetShaderInfoLog, GLGETSHADERINFOLOG);

// Shader program
GL_LOADED_FUNCTION(glCreateProgram, GLCREATEPROGRAM);
GL_LOADED_FUNCTION(glAttachShader, GLATTACHSHADER);
GL_LOADED_FUNCTION(glDetachShader, GLDETACHSHADER);
GL_LOADED_FUNCTION(glLinkProgram, GLLINKPROGRAM);
GL_LOADED_FUNCTION(glGetProgramiv, GLGETPROGRAMIV);
GL_LOADED_FUNCTION(glUseProgram, GLUSEPROGRAM);
GL_LOADED_FUNCTION(glDeleteProgram, GLDELETEPROGRAM);
GL_LOADED_FUNCTION(glGetProgramInfoLog, GLGETPROGRAMINFOLOG);

// Shader uniform
GL_LOADED_FUNCTION(glGetUniformLocation, GLGETUNIFORMLOCATION);
GL_LOADED_FUNCTION(glUniform1f, GLUNIFORM1F);
GL_LOADED_FUNCTION(glUniform2f, GLUNIFORM2F);
GL_LOADED_FUNCTION(glUniform3f, GLUNIFORM3F);
GL_LOADED_FUNCTION(glUniform4f, GLUNIFORM4F);
GL_LOADED_FUNCTION(glUniform1i, GLUNIFORM1I);
GL_LOADED_FUNCTION(glUniform2i, GLUNIFORM2I);
GL_LOADED_FUNCTION(glUniform3i, GLUNIFORM3I);
GL_LOADED_FUNCTION(glUniform4i, GLUNIFORM4I);
GL_LOADED_FUNCTION(glUniform1fv, GLUNIFORM1FV);
GL_LOADED_FUNCTION(glUniform2fv, GLUNIFORM2FV);
GL_LOADED_FUNCTION(glUniform3fv, GLUNIFORM3FV);
GL_LOADED_FUNCTION(glUniform4fv, GLUNIFORM4FV);
GL_LOADED_FUNCTION(glUniform1iv, GLUNIFORM1IV);
GL_LOADED_FUNCTION(glUniform2iv, GLUNIFORM2IV);
GL_LOADED_FUNCTION(glUniform3iv, GLUNIFORM3IV);
GL_LOADED_FUNCTION(glUniform4iv, GLUNIFORM4IV);
GL_LOADED_FUNCTION(glUniformMatrix2fv, GLUNIFORMMATRIX2FV);
GL_LOADED_FUNCTION(glUniformMatrix3fv, GLUNIFORMMATRIX3FV);
GL_LOADED_FUNCTION(glUniformMatrix4fv, GLUNIFORMMATRIX4FV);

GL_LOADED_FUNCTION(glGenVertexArrays, GLGENVERTEXARRAYS);
GL_LOADED_FUNCTION(glGenBuffers, GLGENBUFFERS);
GL_LOADED_FUNCTION(glBindVertexArray, GLBINDVERTEXARRAY);
GL_LOADED_FUNCTION(glBindBuffer, GLBINDBUFFER);
GL_LOADED_FUNCTION(glBufferData, GLBUFFERDATA);
GL_LOADED_FUNCTION(glEnableVertexAttribArray, GLENABLEVERTEXATTRIBARRAY);
GL_LOADED_FUNCTION(glVertexAttribPointer, GLVERTEXATTRIBPOINTER);
GL_LOADED_FUNCTION(glVertexAttribIPointer, GLVERTEXATTRIBIPOINTER);
GL_LOADED_FUNCTION(glDeleteVertexArrays, GLDELETEVERTEXARRAYS);
GL_LOADED_FUNCTION(glDeleteBuffers, GLDELETEBUFFERS);

// FrameBuffer
GL_LOADED_FUNCTION(glGenFramebuffers, GLGENFRAMEBUFFERS);
GL_LOADED_FUNCTION(glBindFramebuffer, GLBINDFRAMEBUFFER);
GL_LOADED_FUNCTION(glDrawBuffers, GLDRAWBUFFERS);
GL_LOADED_FUNCTION(glFramebufferTexture, GLFRAMEBUFFERTEXTURE);
GL_LOADED_FUNCTION(glFramebufferTexture2D, GLFRAMEBUFFERTEXTURE2D);
GL_LOADED_FUNCTION(glCheckFramebufferStatus, GLCHECKFRAMEBUFFERSTATUS);
GL_LOADED_FUNCTION(glDeleteFramebuffers, GLDELETEFRAMEBUFFERS);

// RenderBuffer
GL_LOADED_FUNCTION(glGenRenderbuffers, GLGENRENDERBUFFERS);
GL_LOADED_FUNCTION(glBindRenderbuffer, GLBINDRENDERBUFFER);
GL_LOADED_FUNCTION(glRenderbufferStorage, GLRENDERBUFFERSTORAGE);
GL_LOADED_FUNCTION(glFramebufferRenderbuffer, GLFRAMEBUFFERRENDERBUFFER);
GL_LOADED_FUNCTION(glDeleteRenderbuffers, GLDELETERENDERBUFFERS);

// TextureUnit
GL_LOADED_FUNCTION(glActiveTexture, GLACTIVETEXTURE);

// Sampler
GL_LOADED_FUNCTION(glGenSamplers, GLGENSAMPLERS);
GL_LOADED_FUNCTION(glDeleteSamplers, GLDELETESAMPLERS);
GL_LOADED_FUNCTION(glBindSampler, GLBINDSAMPLER);
GL_LOADED_FUNCTION(glSamplerParameteri, GLSAMPLERPARAMETERI);

#undef GL_ARB_LOADED_FUNCTION

#undef GL_LOADED_FUNCTION

#undef GL_EXPORTED_FUNCTION

#undef PTRDEF