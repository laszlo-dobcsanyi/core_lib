#ifndef _GRAPHICS_STATIC_MODEL_H_
#define _GRAPHICS_STATIC_MODEL_H_

#include "graphics/texture.h"
#include "graphics/mesh.hpp"

////////////////////////////////////////////////////////////////////////////////
// graphics
////////////////////////////////////////////////////////////////////////////////

namespace graphics {

/*******************************************************************************
** StaticMesh
*******************************************************************************/

using StaticMesh = IndexedMesh<TexturedVertex>;

/*******************************************************************************
** StaticModel
*******************************************************************************/

struct StaticModel {
  const_string name;
  StaticMesh mesh;
  Texture diffuse_texture;
};

}

#endif
