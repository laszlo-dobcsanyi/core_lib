#ifndef _GRAPHICS_EXPERIMENTAL_CANVAS_HPP_
#define _GRAPHICS_EXPERIMENTAL_CANVAS_HPP_

#include "graphics/base.hpp"
#include "graphics/create_mesh.hpp"

////////////////////////////////////////////////////////////////////////////////
// graphics experimental
////////////////////////////////////////////////////////////////////////////////

namespace graphics { namespace experimental {

/*******************************************************************************
** Canvas
*******************************************************************************/

template<typename Vertex>
class Canvas final {
public:
  using MeshType = graphics::Mesh<Vertex, graphics::VertexDataType::Indexed>;
  using VertexDataType =
      graphics::VertexData<Vertex, graphics::VertexDataType::Indexed>;

  MeshType mesh;

  Canvas() = default;
  Canvas(Canvas &&) = default;
  Canvas(Canvas const &) = delete;
  Canvas & operator=(Canvas &&) = default;
  Canvas & operator=(Canvas const &) = delete;
  ~Canvas() = default;

  void Set() {
    auto const width = 1.f;
    auto const height = 1.f;

    container::InlineArray<Vertex, 4u> vertices;
    vertices[0u].position = numerics::Vector3f(-width, +height, 0.f);
    vertices[1u].position = numerics::Vector3f(-width, -height, 0.f);
    vertices[2u].position = numerics::Vector3f(+width, -height, 0.f);
    vertices[3u].position = numerics::Vector3f(+width, +height, 0.f);

    Set(core::make_span(vertices));
  }

  void Set(core::span<Vertex> _vertices) {
    CORE_LIB_ASSERT(_vertices.size() == 4u);

    container::InlineArray<uint32, 6u> indices;
    indices[0u] = 0u;
    indices[1u] = 1u;
    indices[2u] = 2u;
    indices[3u] = 0u;
    indices[4u] = 2u;
    indices[5u] = 3u;

    if (mesh.IsAllocated()) {
      mesh.Deallocate();
    }

    mesh = create_mesh(graphics::IndexedVertexDataView<Vertex>(
        _vertices, core::make_span(indices)));
    LOG_GRAPHICS(ERROR, "Failed to create canvas mesh!\n");
  }
};

}}

#endif
