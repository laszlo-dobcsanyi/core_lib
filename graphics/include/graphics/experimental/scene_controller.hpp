#ifndef _GRAPHICS_EXPERIMENTAL_SCENE_CONTROLLER_HPP_
#define _GRAPHICS_EXPERIMENTAL_SCENE_CONTROLLER_HPP_

#include "graphics/window.h"
#include "graphics/camera.h"

////////////////////////////////////////////////////////////////////////////////
// graphics
////////////////////////////////////////////////////////////////////////////////

namespace graphics {

/*******************************************************************************
** SceneController
*******************************************************************************/

class SceneController {
public:
  SceneController(core::Ref<Window> _window, core::Ref<Camera> _camera);
  SceneController(SceneController &&) = delete;
  SceneController(SceneController const &) = delete;
  SceneController & operator=(SceneController &&) = delete;
  SceneController & operator=(SceneController const &) = delete;
  ~SceneController() = default;

  void Update();

protected:
  core::Ref<Window> window;
  core::Ref<Camera> camera;

  numerics::Vector3f camera_position;
  numerics::Vector3f camera_rotation;
  numerics::Vector3f camera_forward;
  core::Optional<WindowPosition> drag_position = core::nullopt;

  void UpdateCameraRotation();
  void UpdateCameraPosition();
};

}

////////////////////////////////////////////////////////////////////////////////
// graphics
////////////////////////////////////////////////////////////////////////////////

namespace graphics {

/*******************************************************************************
** SceneController
*******************************************************************************/

inline SceneController::SceneController(core::Ref<Window> _window,
                                        core::Ref<Camera> _camera)
    : window(move(_window))
    , camera(move(_camera))
    , camera_position(0.f, 0.f, 0.f)
    , camera_rotation(0.f, 0.f, 0.f) {}

inline void SceneController::UpdateCameraRotation() {
  // camera_rotation = { yaw, roll, pitch };
#if 1
  if (window->GetButtonState(Button::Left) == graphics::KeyState::Pressed) {
#else
  auto const navigation_key = graphics::Key::kLControl;
  if (window->GetKeyState(navigation_key) == graphics::KeyState::Pressed) {
#endif
    if (drag_position.HasValue()) {
      auto const mouse_position = window->MousePosition();
      if (mouse_position.HasValue()) {
        auto const position_change =
            mouse_position.Value() - drag_position.Value();
        auto const roll = position_change.x * 0.001f;
        camera_rotation.y += roll;
        auto const yaw = position_change.y * 0.001f;
        camera_rotation.x -= yaw;
      }
    }
  }
  auto const camera_view = numerics::from_euler(camera_rotation);
  auto const forward = numerics::Vector3f(0.f, 0.f, 1.f);
  camera_forward = numerics::rotate(forward, camera_view);
}

inline void SceneController::UpdateCameraPosition() {
  auto const navigation_key = graphics::Key::kLAlt;
  if (window->GetKeyState(navigation_key) == graphics::KeyState::Pressed) {
    if (drag_position.HasValue()) {
      auto const mouse_position = window->MousePosition();
      if (mouse_position.HasValue()) {
        auto const position_change =
            mouse_position.Value() - drag_position.Value();
        auto const world_up = numerics::Vector3f(0.f, 1.f, 0.f);
        auto const left = numerics::cross(camera_forward, world_up);
        auto const up = numerics::cross(camera_forward, left);
        auto const speed = 5.f;
        camera_position -= (position_change.x * speed) * left;
        camera_position -= (position_change.y * speed) * up;
      }
    }
  }
  if (window->MouseWheel() != 0) {
    auto const distance = static_cast<float>(window->MouseWheel()) * 100.f;
    camera_position += distance * camera_forward;
  }
}

inline void SceneController::Update() {
  UpdateCameraRotation();
  UpdateCameraPosition();
  auto const camera_target = camera_position + camera_forward;
  camera->Set(camera_position, camera_target);

  if (window->GetButtonState(Button::Left) == graphics::KeyState::Pressed) {
    drag_position = window->MousePosition();
  } else if (drag_position.HasValue()) {
    drag_position = core::nullopt;
  }
}

}

#endif
