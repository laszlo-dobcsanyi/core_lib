macro(graphics_source file_path)
  list(APPEND GRAPHICS_SOURCES ${file_path})
endmacro(graphics_source)

macro(conditional_graphics_source condition file_path)
  if (${condition})
    graphics_source(${file_path})
  endif()
endmacro(conditional_graphics_source)

macro(x11_source file_path)
  conditional_graphics_source(${GRAPHICS_X11} ${file_path})
endmacro(x11_source)

macro(winapi_source file_path)
  conditional_graphics_source(${GRAPHICS_WINAPI} ${file_path})
endmacro(winapi_source)

macro(glx_source file_path)
  conditional_graphics_source(${GRAPHICS_GLX} ${file_path})
endmacro(glx_source)

macro(wgl_source file_path)
  conditional_graphics_source(${GRAPHICS_WGL} ${file_path})
endmacro(wgl_source)

macro(opengl_source file_path)
  conditional_graphics_source(${GRAPHICS_OPENGL} ${file_path})
endmacro(opengl_source)

macro(directx_source file_path)
  conditional_graphics_source(${GRAPHICS_DIRECTX} ${file_path})
endmacro(directx_source )

macro(freetype_source file_path)
  graphics_source(${file_path})
endmacro(freetype_source)

graphics_source("include/graphics/animated_model.h")
graphics_source("include/graphics/animation/animation_channel.h")
graphics_source("include/graphics/animation/animation_state.h")
graphics_source("include/graphics/animation/animation_timeline.h")
graphics_source("include/graphics/animation/bone_transformations.h")
graphics_source("include/graphics/animation/skeletal_animation.h")
graphics_source("include/graphics/api/api.h")
graphics_source("include/graphics/api/context.h")
directx_source("include/graphics/api/directx/directx.h")
graphics_source("include/graphics/api.h")
opengl_source("include/graphics/api/opengl/function.hpp")
opengl_source("include/graphics/api/opengl/glext.h")
glx_source("include/graphics/api/opengl/glx/glx.h")
glx_source("include/graphics/api/opengl/glx/symbols.inl")
opengl_source("include/graphics/api/opengl/opengl.h")
opengl_source("include/graphics/api/opengl/symbols.hh")
opengl_source("include/graphics/api/opengl/symbols.inl")
wgl_source("include/graphics/api/opengl/wglext.h")
wgl_source("include/graphics/api/opengl/wgl/symbols.inl")
wgl_source("include/graphics/api/opengl/wgl/wgl.h")
winapi_source("include/graphics/api/winapi/keyboard.hh")
winapi_source("include/graphics/api/winapi/winapi.h")
x11_source("include/graphics/api/x11/context.hh")
x11_source("include/graphics/api/x11/symbols.inl")
x11_source("include/graphics/api/x11/x11.h")
graphics_source("include/graphics/base/definitions.hh")
graphics_source("include/graphics/base.hpp")
graphics_source("include/graphics/base/vertex_data.hpp")
graphics_source("include/graphics/base/vertex_types.hpp")
graphics_source("include/graphics/camera.h")
graphics_source("include/graphics/create_mesh.hpp")
graphics_source("include/graphics/cubemap.h")
graphics_source("include/graphics/experimental/canvas.hpp")
graphics_source("include/graphics/experimental/scene_controller.hpp")
graphics_source("include/graphics/framebuffer.h")
directx_source("include/graphics/implementation/dx/image.hpp")
directx_source("include/graphics/implementation/dx/mesh.hpp")
directx_source("include/graphics/implementation/dx/shader.hpp")
directx_source("include/graphics/implementation/dx/window.hpp")
opengl_source("include/graphics/implementation/gl/create_mesh.hpp")
opengl_source("include/graphics/implementation/gl/cubemap.h")
opengl_source("include/graphics/implementation/gl/draw.hpp")
opengl_source("include/graphics/implementation/gl/framebuffer.h")
opengl_source("include/graphics/implementation/gl/glx/window.h")
opengl_source("include/graphics/implementation/gl/material.h")
opengl_source("include/graphics/implementation/gl/mesh.hpp")
opengl_source("include/graphics/implementation/gl/renderbuffer.h")
opengl_source("include/graphics/implementation/gl/sampler.h")
opengl_source("include/graphics/implementation/gl/shader.h")
opengl_source("include/graphics/implementation/gl/texture.h")
opengl_source("include/graphics/implementation/gl/texture_sampler.h")
opengl_source("include/graphics/implementation/gl/uniforms.h")
opengl_source("include/graphics/implementation/gl/vertex_buffers.hpp")
wgl_source("include/graphics/implementation/gl/wgl/window.h")
opengl_source("include/graphics/implementation/opengl.hpp")
winapi_source("include/graphics/implementation/winapi.hpp")
graphics_source("include/graphics/implementation/window/window_base.hpp")
graphics_source("include/graphics/implementation/window/window_events.hpp")
x11_source("include/graphics/implementation/x11.hpp")
graphics_source("include/graphics/mesh.hpp")
graphics_source("include/graphics/pipeline.hpp")
graphics_source("include/graphics/reflect_uniforms.hpp")
graphics_source("include/graphics/renderbuffer.h")
graphics_source("include/graphics/renderer.hpp")
graphics_source("include/graphics/renderers/forward_animated_model_renderer.h")
graphics_source("include/graphics/renderers/forward_position_mesh_renderer.h")
graphics_source("include/graphics/renderers/forward_skybox_renderer.h")
graphics_source("include/graphics/renderers/forward_static_model_renderer.h")
graphics_source("include/graphics/renderers/forward_terrain_renderer.h")
graphics_source("include/graphics/resource/cgm.hpp")
graphics_source("include/graphics/resource/convert_image_data.hpp")
graphics_source("include/graphics/resource/cubemap_from_file.hpp")
graphics_source("include/graphics/resource/fbx/base.hpp")
graphics_source("include/graphics/resource/fbx/converter/base.hpp")
graphics_source("include/graphics/resource/fbx/converter/children.hpp")
graphics_source("include/graphics/resource/fbx/converter/component_merger.hpp")
graphics_source("include/graphics/resource/fbx/converter/converter.hpp")
graphics_source("include/graphics/resource/fbx/converter/object_map.hpp")
graphics_source("include/graphics/resource/fbx/converter/skeleton_builder.hpp")
graphics_source("include/graphics/resource/fbx/converter/to_string.hpp")
graphics_source("include/graphics/resource/fbx/convert.hpp")
graphics_source("include/graphics/resource/fbx.h")
graphics_source("include/graphics/resource/fbx/io.hpp")
graphics_source("include/graphics/resource/fbx/scene.hpp")
graphics_source("include/graphics/resource/image/bmp.h")
graphics_source("include/graphics/resource/image/image_data_from_file.h")
graphics_source("include/graphics/resource/image/image_data.h")
graphics_source("include/graphics/resource/image/png.h")
graphics_source("include/graphics/resource/image/tga.h")
graphics_source("include/graphics/resource/load_map.hpp")
graphics_source("include/graphics/resource/load_model.h")
graphics_source("include/graphics/resource/load_skybox.h")
graphics_source("include/graphics/resource/map_data_from_file.hpp")
graphics_source("include/graphics/resource/map_data.hpp")
graphics_source("include/graphics/resource/model_data_builder.hpp")
graphics_source("include/graphics/resource/model_data_from_file.h")
graphics_source("include/graphics/resource/model_data.hpp")
graphics_source("include/graphics/resource/obj.hpp")
graphics_source("include/graphics/resource/texture_from_file.hpp")
graphics_source("include/graphics/resource/vertex_data_from_map_data.hpp")
graphics_source("include/graphics/resource/vertex_data_from_mesh_data.hpp")
graphics_source("include/graphics/sampler.h")
graphics_source("include/graphics/shader.h")
graphics_source("include/graphics/skybox.h")
graphics_source("include/graphics/static_model.h")
graphics_source("include/graphics/terrain.h")
graphics_source("include/graphics/texture.h")
graphics_source("include/graphics/texture_sampler.h")
graphics_source("include/graphics/window.h")
graphics_source("src/animation/animation_timeline.cpp")
graphics_source("src/animation/bone_transformations.cpp")
graphics_source("src/animation/skeletal_animation.cpp")
graphics_source("src/api/api.cpp")
graphics_source("src/api/context.cpp")
directx_source("src/api/directx/directx.cpp")
glx_source("src/api/opengl/glx/glx.cpp")
opengl_source("src/api/opengl/opengl.cpp")
opengl_source("src/api/opengl/symbols.cpp")
wgl_source("src/api/opengl/wgl/wgl.cpp")
winapi_source("src/api/winapi/keyboard.cpp")
winapi_source("src/api/winapi/winapi.cpp")
x11_source("src/api/x11/context.cpp")
x11_source("src/api/x11/x11.cpp")
graphics_source("src/camera.cpp")
opengl_source("src/implementation/gl/cubemap.cpp")
opengl_source("src/implementation/gl/framebuffer.cpp")
opengl_source("src/implementation/gl/glx/window.cpp")
opengl_source("src/implementation/gl/renderbuffer.cpp")
opengl_source("src/implementation/gl/sampler.cpp")
opengl_source("src/implementation/gl/shader.cpp")
opengl_source("src/implementation/gl/texture.cpp")
opengl_source("src/implementation/gl/texture_sampler.cpp")
wgl_source("src/implementation/gl/wgl/window.cpp")
graphics_source("src/renderers/forward_animated_model_renderer.cpp")
graphics_source("src/renderers/forward_position_mesh_renderer.cpp")
graphics_source("src/renderers/forward_skybox_renderer.cpp")
graphics_source("src/renderers/forward_static_model_renderer.cpp")
graphics_source("src/renderers/forward_terrain_renderer.cpp")
graphics_source("src/resource/fbx.cpp")
graphics_source("src/resource/image/bmp.cpp")
graphics_source("src/resource/image/image_data.cpp")
graphics_source("src/resource/image/image_data_from_file.cpp")
graphics_source("src/resource/image/png.cpp")
graphics_source("src/resource/image/tga.cpp")
graphics_source("src/resource/load_model.cpp")
graphics_source("src/resource/load_skybox.cpp")
graphics_source("src/resource/model_data_from_file.cpp")

