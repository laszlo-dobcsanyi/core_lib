#include "gui/text_renderer.h"

////////////////////////////////////////////////////////////////////////////////
// gui
////////////////////////////////////////////////////////////////////////////////

namespace gui {

/*******************************************************************************
** create_text_renderer
*******************************************************************************/

core::Optional<TextRenderer> create_text_renderer() {
  using namespace graphics;

  TextRenderer renderer;

// Shader
#if defined(CORE_GRAPHICS_OPENGL)
  // clang-format off
  const char * vertex_shader =
    "#version 330 core\n"
    "layout (location = 0) in vec4 position_and_uv;\n"
    "out vec2 texture_coordinate;\n"
    "uniform mat4 transformation;\n"
    "void main() {\n"
    "  gl_Position = transformation * vec4(position_and_uv.xy, 0.f, 1.f);\n"
    "  texture_coordinate = position_and_uv.zw;\n"
    "}\n";
  const char * fragment_shader =
    "#version 330 core\n"
    "in vec2 texture_coordinate;\n"
    "out vec4 color;\n"
    "uniform sampler2D texture1; //font\n"
    "void main() {\n"
    "  color = texture(texture1, texture_coordinate);\n"
    "}\n";
  // clang-format on

  if (!renderer.shader.Create(vertex_shader, fragment_shader)) {
    LOG_GRAPHICS(ERROR, "Failed to create renderer shader!\n");
    return core::nullopt;
  }
  opengl::generate_uniform_locations(renderer);
#else
#  error
#endif

  // Sampler
  if (create(renderer.sampler)) {
    renderer.sampler.Set(TextureWrap::U, TextureWrapMode::Repeat);
    renderer.sampler.Set(TextureWrap::V, TextureWrapMode::Repeat);
    renderer.sampler.Set(TextureFilter::Minify,
                         TextureFilterMode::LinearMipmapLinear);
    renderer.sampler.Set(TextureFilter::Magnify, TextureFilterMode::Linear);
  } else {
    LOG_GRAPHICS(ERROR, "Failed to create sampler!\n");
    return core::nullopt;
  }

  // FontSampler
  if (!create(renderer.font_sampler)) {
    LOG_GRAPHICS(ERROR, "Failed to create font texture sampler!\n");
    return core::nullopt;
  }

  return renderer;
}

/*******************************************************************************
** bind
*******************************************************************************/

void tag_invoke(graphics::bind_t, TextRenderer & _this) {
  _this.font_sampler.Bind(graphics::TextureUnit(0));
  _this.font_sampler.Bind(_this.sampler);
  _this.shader.Use();
}

/*******************************************************************************
** render
*******************************************************************************/

void tag_invoke(graphics::render_t, TextRenderer const & _this,
                Text const & _text,
                graphics::TransformationMatrix const & _projection,
                numerics::Vector2f _position) {
  _this.font_sampler.Activate(_text.font_atlas->texture);

  graphics::TransformationMatrix translation;
  numerics::set_translate(translation, numerics::Vector3f(_position, 0.f));

  TextRendererUniforms uniforms;
  uniforms.transformation = _projection * translation;
  uniforms.font_sampler = &_this.font_sampler;
  graphics::render_mesh(_this, uniforms, _text.mesh,
                        graphics::DrawMode::Triangles);
}

}
