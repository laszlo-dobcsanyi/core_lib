#include "gui/freetype/freetype.h"

#define LOG_FREETYPE(__severity__, ...) CORE_LIB_LOG(__severity__, __VA_ARGS__)

////////////////////////////////////////////////////////////////////////////////
// gui freetype
////////////////////////////////////////////////////////////////////////////////

namespace gui { namespace freetype {

FT_Library library;

/*******************************************************************************
** library_handle
*******************************************************************************/

FT_Library const & library_handle() { return library; }

/*******************************************************************************
** cstr
*******************************************************************************/

char const * cstr(FT_Error err) {
#undef __FTERRORS_H__
#define FT_ERROR_START_LIST switch (err) {
#define FT_ERRORDEF(e, v, s)                                                   \
  case e:                                                                      \
    return s;
#define FT_ERROR_END_LIST }
#include FT_ERRORS_H
  return "(Unknown error)";
}

/*******************************************************************************
** Initialize
*******************************************************************************/

bool Initialize() {
  LOG_FREETYPE(STAGE, "Initialize..\n");
  if (auto error = FT_Init_FreeType(&library)) {
    LOG_FREETYPE(ERROR, "Failed to initialize library: '%s'!\n", cstr(error));
    return false;
  }
  LOG_FREETYPE(VERBOSE, "Initialized!\n");
  return true;
}

/*******************************************************************************
** IsInitialized
*******************************************************************************/

bool IsInitialized() { return library != nullptr; }

/*******************************************************************************
** Finalize
*******************************************************************************/

void Finalize() {
  if (IsInitialized()) {
    LOG_FREETYPE(STAGE, "Finalize..\n");
    if (auto error = FT_Done_FreeType(library)) {
      LOG_FREETYPE(ERROR, "Failed to finalize library: '%s'!\n", cstr(error));
    }
    LOG_FREETYPE(VERBOSE, "Finalized!\n");
  }
}

}}
