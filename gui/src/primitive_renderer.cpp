#include "gui/primitive_renderer.h"

////////////////////////////////////////////////////////////////////////////////
// gui
////////////////////////////////////////////////////////////////////////////////

namespace gui {

/*******************************************************************************
** PrimitiveContext
*******************************************************************************/

void PrimitiveContext::AddText(numerics::Vector3f _position,
                               const_string const & _string) {}

void PrimitiveContext::AddLine(numerics::Vector3f _start,
                               numerics::Vector3f _end,
                               numerics::Vector4f _color) {
  line_vertices_buffer.Push(_start, _color);
  line_vertices_buffer.Push(_end, _color);
}

void PrimitiveContext::Render() const {}

void PrimitiveContext::Clear() {
  text_vertices_stack.Clear();
  line_vertices_buffer.Clear();
}

}
