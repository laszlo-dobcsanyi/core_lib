#include "gui/font_atlas.h"

#include "graphics/resource/image/image_data.h"
#include "gui/freetype/freetype.h"

#define LOG_FONT_ATLAS(__severity__, ...)                                      \
  CORE_LIB_LOG(__severity__, __VA_ARGS__)

////////////////////////////////////////////////////////////////////////////////
// gui
////////////////////////////////////////////////////////////////////////////////

namespace gui {

/*******************************************************************************
** font_atlas_from_file
*******************************************************************************/

core::Optional<FontAtlas>
font_atlas_from_file(core::fs::FilePath const & _file_path, uint32 _size,
                     int32 _dpi, uint32 _glyph_count) {
  // Based on https://gist.github.com/baines/b0f9e4be04ba4e6f56cab82eef5008ff
  using namespace freetype;

  FontAtlas atlas;
  atlas.glyphs = core::make_unique_array<FontAtlasGlyph>(_glyph_count);

  FT_Face face;
  if (auto error =
          FT_New_Face(freetype::library_handle(), cstr(_file_path), 0, &face)) {
    LOG_FONT_ATLAS(ERROR, "Failed to load face '%s': '%s'!\n", cstr(_file_path),
                   freetype::cstr(error));
    return core::nullopt;
  }
  auto char_size = _size << 6;
  if (auto error = FT_Set_Char_Size(face, 0, char_size, _dpi, _dpi)) {
    LOG_FONT_ATLAS(ERROR, "Failed to set character size: '%s'!\n",
                   freetype::cstr(error));
    return core::nullopt;
  }
  // TODO: This is a quick estimation
  uint32 estimated_dimension = static_cast<uint32>(
      numerics::ceil(numerics::sqrt(static_cast<float>(_glyph_count))));
  uint32 max_dimension =
      (1 + static_cast<uint32>(face->size->metrics.height >> 6)) *
      estimated_dimension;
  uint32 texture_size = 1;
  while (texture_size < max_dimension) {
    texture_size <<= 1;
  }

  graphics::resource::ImageData image_data(texture_size, texture_size,
                                           graphics::ColorFormat::ABGR8UI);
  auto image_bytes = image_data.GetBytes();
  ::memset(image_bytes.data(), U8(0x00), image_bytes.size());
  uint32 pen_x = 0u;
  uint32 pen_y = 0u;
  for (auto current = 0u; current < _glyph_count; ++current) {
    if (auto error = FT_Load_Char(face, current,
                                  FT_LOAD_RENDER | FT_LOAD_FORCE_AUTOHINT |
                                      FT_LOAD_TARGET_LIGHT)) {
      LOG_FONT_ATLAS(ERROR, "Failed to load character: '%s'!\n",
                     freetype::cstr(error));
      return core::nullopt;
    }
    FT_Bitmap & bitmap = face->glyph->bitmap;

    if (texture_size <= pen_x + bitmap.width) {
      pen_x = 0;
      pen_y += (static_cast<uint32>(face->size->metrics.height >> 6) + 1);
    }

    // Copy from bitmap to pixels
    for (auto bitmap_row = 0u; bitmap_row < bitmap.rows; ++bitmap_row) {
      auto const image_row = pen_y + bitmap_row;
      auto row_bytes = image_data.GetRowBytes(image_row);
      for (auto bitmap_column = 0u; bitmap_column < bitmap.width;
           ++bitmap_column) {
        auto const image_pixel = (pen_x + bitmap_column) * 4u;
        auto const bitmap_pixel = bitmap_row * bitmap.pitch + bitmap_column;
        row_bytes[image_pixel + 0u] = U8(0xFF);
        row_bytes[image_pixel + 1u] = bitmap.buffer[bitmap_pixel];
        row_bytes[image_pixel + 2u] = bitmap.buffer[bitmap_pixel];
        row_bytes[image_pixel + 3u] = bitmap.buffer[bitmap_pixel];
      }
    }

    atlas.glyphs[current].top_left_uv = numerics::Vector2f(
        static_cast<float>(pen_x) / static_cast<float>(texture_size),
        static_cast<float>(pen_y) / static_cast<float>(texture_size));

    atlas.glyphs[current].bottom_right_uv =
        numerics::Vector2f(static_cast<float>(pen_x + bitmap.width) /
                               static_cast<float>(texture_size),
                           static_cast<float>(pen_y + bitmap.rows) /
                               static_cast<float>(texture_size));

    atlas.glyphs[current].x_off = face->glyph->bitmap_left;
    atlas.glyphs[current].y_off = face->glyph->bitmap_top;
    atlas.glyphs[current].width = bitmap.width;
    atlas.glyphs[current].height = bitmap.rows;

    atlas.glyphs[current].advance =
        static_cast<uint32>(face->glyph->advance.x >> 6);

    pen_x += bitmap.width + 1;
  }
  atlas.texture =
      graphics::create_texture(image_data, graphics::TextureSettings());
  if (!atlas.texture) {
    LOG_FONT_ATLAS(ERROR, "Failed to create texture!");
    return core::nullopt;
  }
  return core::Optional<FontAtlas>(move(atlas));
}

core::Optional<FontAtlas>
font_atlas_from_file(core::fs::FilePath const & _file_path, uint32 _size) {
  return font_atlas_from_file(_file_path, _size, 96u, 128u);
}

}
