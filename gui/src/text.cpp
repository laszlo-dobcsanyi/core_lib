#include "gui/text.h"

#include "graphics/create_mesh.hpp"

////////////////////////////////////////////////////////////////////////////////
// gui
////////////////////////////////////////////////////////////////////////////////

namespace gui {

/*******************************************************************************
** create_text
*******************************************************************************/

Text create_text(core::Ref<FontAtlas> _font_atlas,
                 const_string const & _string) {
  Text text(move(_font_atlas));
  text.mesh.Allocate();
  set_text(text, _string);
  return text;
}

/*******************************************************************************
** measure_text
*******************************************************************************/

TextMeasures measure_text(FontAtlas const & _font_atlas,
                          const_string const & _string) {
  int32 top = 0;
  int32 bottom = 0;
  int32 width = 0;
  for (size_t index = 0u; index < strlen(_string); ++index) {
    const auto & glyph = _font_atlas.glyphs[cstr(_string)[index]];
    top = core::max(top, glyph.y_off);
    bottom = core::max(bottom, glyph.y_off - glyph.height);
    width += glyph.advance;
  }
  TextMeasures result;
  result.top = top;
  result.bottom = bottom;
  result.width = width;
  result.height = top + bottom;
  return result;
}

/*******************************************************************************
** set_text
*******************************************************************************/

void set_text(Text & _text, const_string const & _string) {
  graphics::ArrayVertexData<TextVertex> vertex_data;
  vertex_data.vertices =
      core::make_unique_array<TextVertex>(strlen(_string) * 6u);
  float x = 0.f;
  float y = 0.f;
  for (size_t index = 0u; index < strlen(_string); ++index) {
    const auto & glyph = _text.font_atlas->glyphs[cstr(_string)[index]];

    float top_x = x + glyph.x_off;
    float top_y = y - glyph.y_off;
    float top_u = glyph.top_left_uv.x;
    float top_v = glyph.top_left_uv.y;

    float bottom_x = top_x + glyph.width;
    float bottom_y = top_y + glyph.height;
    float bottom_u = glyph.bottom_right_uv.x;
    float bottom_v = glyph.bottom_right_uv.y;

    vertex_data.vertices[index * 6u + 0u].position_and_uv =
        numerics::Vector4f(top_x, top_y, top_u, top_v);
    vertex_data.vertices[index * 6u + 1u].position_and_uv =
        numerics::Vector4f(top_x, bottom_y, top_u, bottom_v);
    vertex_data.vertices[index * 6u + 2u].position_and_uv =
        numerics::Vector4f(bottom_x, bottom_y, bottom_u, bottom_v);

    vertex_data.vertices[index * 6u + 3u].position_and_uv =
        numerics::Vector4f(top_x, top_y, top_u, top_v);
    vertex_data.vertices[index * 6u + 4u].position_and_uv =
        numerics::Vector4f(bottom_x, bottom_y, bottom_u, bottom_v);
    vertex_data.vertices[index * 6u + 5u].position_and_uv =
        numerics::Vector4f(bottom_x, top_y, bottom_u, top_v);

    x += glyph.advance;
  }

  graphics::set_vertex_data(_text.mesh.vertex_buffer, vertex_data);
  _text.measures = measure_text(*_text.font_atlas, _string);
}

}
