#ifndef _GRAPHICS_GUI_PRIMITIVES_HPP_
#define _GRAPHICS_GUI_PRIMITIVES_HPP_

#ifndef CORE_GUI
#  error
#endif

////////////////////////////////////////////////////////////////////////////////
// graphics gui
////////////////////////////////////////////////////////////////////////////////

namespace graphics { namespace gui {

using Size = numerics::Vector2<int32>;
using Point = numerics::Vector2<int32>;

struct Rectangle {
  Point location;
  Size size;
};

using Triangle = numerics::Vector3<int32>;
using Box = Rectangle;

using RGBColor = uint32;

}}

#endif
