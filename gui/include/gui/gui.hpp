#ifndef _GRAPHICS_GUI_HPP_
#define _GRAPHICS_GUI_HPP_

#include "core/base.h"

#include "graphics/gui.hh"

#define LOG_GUI(__severity__, ...) CORE_LIB_LOG(__severity__, __VA_ARGS__)

#define CORE_GUI
#include "graphics/gui/primitives.hpp"
#include "graphics/gui/context.hpp"
#include "graphics/gui/element_container.hpp"
#include "graphics/gui/element.hpp"
#include "graphics/gui/controller.hpp"
#include "graphics/gui/scene.hpp"
#undef CORE_GUI

#endif
