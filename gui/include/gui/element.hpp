#ifndef _GRAPHICS_GUI_ELEMENT_HPP_
#define _GRAPHICS_GUI_ELEMENT_HPP_

#ifndef CORE_GUI
#  error
#endif

////////////////////////////////////////////////////////////////////////////////
// graphics gui
////////////////////////////////////////////////////////////////////////////////

namespace graphics { namespace gui {

/*******************************************************************************
** Element
*******************************************************************************/

class Element
    : public ElementBase
    , public LinkedElement<Element>
    , public ElementContainer {
public:
  virtual ~Element() = default;

  void Update();
  void Draw();

protected:
  Element() = default;
  Element(Element &&) = delete;
  Element(Element const &) = delete;
  Element & operator=(Element &&) = delete;
  Element & operator=(Element const &) = delete;

  virtual void OnUpdate() {}
  virtual void OnDraw() {}
};

}}

////////////////////////////////////////////////////////////////////////////////
// graphics gui
////////////////////////////////////////////////////////////////////////////////

namespace graphics { namespace gui {

/*******************************************************************************
** Element
*******************************************************************************/

inline void Element::Update() {
  OnUpdate();

  auto query = ElementContainer::LinkedQuery();
  while (auto element = query.Forward()) {
    element->Update();
  }
}

inline void Element::Draw() {
  OnDraw();

  auto query = ElementContainer::LinkedQuery();
  while (auto element = query.Forward()) {
    element->Draw();
  }
}

}}

#endif
