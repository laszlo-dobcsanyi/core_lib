#ifndef _GRAPHICS_GUI_CONTROLLER_HPP_
#define _GRAPHICS_GUI_CONTROLLER_HPP_

#ifndef CORE_GUI
#  error
#endif

////////////////////////////////////////////////////////////////////////////////
// graphics gui
////////////////////////////////////////////////////////////////////////////////

namespace graphics { namespace gui {

/*******************************************************************************
** Controller
*******************************************************************************/

class Controller : public ElementContainer {
public:
  Controller(core::Ref<Window> _window);
  Controller(Controller &&) = delete;
  Controller(Controller const &) = delete;
  Controller & operator=(Controller &&) = delete;
  Controller & operator=(Controller const &) = delete;
  ~Controller() = default;

  bool IsBound() const { return window.IsBound(); }
  void Bind(core::Ref<Window> _window);

  void Update();
  void Draw();

private:
  core::Ref<Window> window;
};

}}

////////////////////////////////////////////////////////////////////////////////
// graphics gui
////////////////////////////////////////////////////////////////////////////////

namespace graphics { namespace gui {

/*******************************************************************************
** Controller
*******************************************************************************/

inline Controller::Controller(core::Ref<Window> _window)
    : window(move(_window)) {}

inline void Controller::Bind(core::Ref<Window> _window) {
  window = move(_window);
}

inline void Controller::Update() {
  auto query = ElementContainer::LinkedQuery();
  while (auto element = query.Forward()) {
    element->Update();
  }
}

inline void Controller::Draw() {
  auto query = ElementContainer::LinkedQuery();
  while (auto element = query.Forward()) {
    element->Draw();
  }
}

}}

#endif
