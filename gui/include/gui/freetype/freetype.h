#ifndef _GUI_FREETYPE_FREETYPE_H_
#define _GUI_FREETYPE_FREETYPE_H_

#include "core/base.h"

#include "ft2build.h"
#include FT_FREETYPE_H

////////////////////////////////////////////////////////////////////////////////
// gui freetype
////////////////////////////////////////////////////////////////////////////////

namespace gui { namespace freetype {

FT_Library const & library_handle();
char const * cstr(FT_Error _error);

bool Initialize();
bool IsInitialized();
void Finalize();

}}

#endif
