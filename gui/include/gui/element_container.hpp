#ifndef _GRAPHICS_GUI_ELEMENT_CONTAINER_HPP_
#define _GRAPHICS_GUI_ELEMENT_CONTAINER_HPP_

#include "core/queries.hpp"

#ifndef CORE_GUI
#  error
#endif

////////////////////////////////////////////////////////////////////////////////
// graphics gui
////////////////////////////////////////////////////////////////////////////////

namespace graphics { namespace gui {

/*******************************************************************************
** ElementBase
*******************************************************************************/

class ElementBase : public container::ChainElement<ElementBase> {
public:
  virtual ~ElementBase() = default;

protected:
  ElementBase() = default;
  ElementBase(ElementBase &&) = delete;
  ElementBase(ElementBase const &) = delete;
  ElementBase & operator=(ElementBase &&) = delete;
  ElementBase & operator=(ElementBase const &) = delete;
};

/*******************************************************************************
** LinkedElement
*******************************************************************************/

template<class T>
struct LinkedElement : public container::ChainElement<T> {};

/*******************************************************************************
** ElementContainer
*******************************************************************************/

class ElementContainer {
public:
  ElementContainer() = default;
  ElementContainer(ElementContainer &&) = default;
  ElementContainer(ElementContainer const &) = delete;
  ElementContainer & operator=(ElementContainer &&) = default;
  ElementContainer & operator=(ElementContainer const &) = delete;
  ~ElementContainer();

  template<typename T, typename... Args>
  T & Create(Args &&... _args);

  void Link(Element & _element);
  void Unlink(Element & _element);

protected:
  using AllocatorType = allocator::DefaultMemoryAllocator;
  AllocatorType allocator;

  using AllocatedElementChain = container::Chain<ElementBase>;
  AllocatedElementChain allocated;

  using LinkedElementChain = container::Chain<Element, LinkedElement>;
  LinkedElementChain linked;

  using AllocatedElementQuery = typename AllocatedElementChain::QueryType;
  AllocatedElementQuery AllocatedQuery() {
    return AllocatedElementQuery::FromFirst(allocated);
  }

  using LinkedElementQuery = typename LinkedElementChain::QueryType;
  LinkedElementQuery LinkedQuery() {
    return LinkedElementQuery::FromFirst(linked);
  }
};

}}

////////////////////////////////////////////////////////////////////////////////
// graphics gui
////////////////////////////////////////////////////////////////////////////////

namespace graphics { namespace gui {

/*******************************************************************************
** ElementContainer
*******************************************************************************/

inline ElementContainer::~ElementContainer() {
  auto query = AllocatedQuery();
  while (auto element_base = query.UnlinkForward()) {
    Unlink(*reinterpret_cast<Element *>(element_base));
    element_base->~ElementBase();
    allocator.Deallocate(element_base);
  }
}

template<typename T, typename... Args>
inline T & ElementContainer::Create(Args &&... _args) {
#if 0
  CORE_LIB_STATIC_ASSERT( mpl::is_base_of< container::ChainElement< Element >, T >::value );
  auto allocation = allocator.Allocate( sizeof( T ) );
  new ( allocation.template As< ElementBase * >() ) T( forward< Args >( _args ) ... );
  allocated.Link( *allocation.template As< Element * >() );
  Link( *allocation.template As< Element * >() );
  return *allocation.template As< T * >();
#endif
}

inline void ElementContainer::Link(Element & _element) {
  linked.template Link<container::Tail>(_element);
}

inline void ElementContainer::Unlink(Element & _element) {
  linked.template Unlink<container::Tail>(_element);
}

}}

#endif
