#ifndef _GUI_TEXT_RENDERER_H_
#define _GUI_TEXT_RENDERER_H_

#include "graphics/sampler.h"
#include "graphics/texture_sampler.h"
#include "graphics/renderer.hpp"

#include "gui/text.h"

////////////////////////////////////////////////////////////////////////////////
// gui
////////////////////////////////////////////////////////////////////////////////

namespace gui {

/*******************************************************************************
** TextRendererUniforms
*******************************************************************************/

struct TextRendererUniforms {
  graphics::TransformationMatrix transformation;
  graphics::TextureSampler const * font_sampler = nullptr;

private:
  template<typename UniformContext>
  friend auto tag_invoke(graphics::reflect_uniforms_t,
                         mpl::type<TextRendererUniforms>,
                         UniformContext & _context) {
    return _context
        .Member(graphics::UniformId::Transformation,
                &TextRendererUniforms::transformation)
        .Member(graphics::UniformId::Texture1,
                &TextRendererUniforms::font_sampler);
  }
};

/*******************************************************************************
** TextRenderer
*******************************************************************************/

struct TextRenderer
    : public graphics::Resource<TextRenderer>
    , public graphics::RendererBase<TextRendererUniforms> {
  graphics::Sampler sampler;
  graphics::TextureSampler font_sampler;

private:
  friend void tag_invoke(graphics::bind_t, TextRenderer & _this);
  friend void tag_invoke(graphics::render_t, TextRenderer const & _this,
                         Text const & _text,
                         graphics::TransformationMatrix const & _projection,
                         numerics::Vector2f _position);
};

/*******************************************************************************
** create_text_renderer
*******************************************************************************/

core::Optional<TextRenderer> create_text_renderer();

}

#endif
