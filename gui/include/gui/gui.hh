#ifndef _GRAPHICS_GUI_HH_
#define _GRAPHICS_GUI_HH_

////////////////////////////////////////////////////////////////////////////////
// graphics gui
////////////////////////////////////////////////////////////////////////////////

namespace graphics { namespace gui {

class Context;
class Element;
class ElementContainer;
class Controller;
class Scene;

}}

#endif
