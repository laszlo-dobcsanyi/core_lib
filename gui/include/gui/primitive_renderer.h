#ifndef _GUI_PRIMITIVE_CONTEXT_H_
#define _GUI_PRIMITIVE_CONTEXT_H_

#include "core/numerics.hpp"

#include "graphics/base.hpp"

#include "gui/text.h"

////////////////////////////////////////////////////////////////////////////////
// gui
////////////////////////////////////////////////////////////////////////////////

namespace gui {

/*******************************************************************************
** PrimitiveContext
*******************************************************************************/

class PrimitiveContext {
public:
  void AddText(numerics::Vector3f _position, const_string const & _string);
  void AddLine(numerics::Vector3f _start, numerics::Vector3f _end,
               numerics::Vector4f _color);
  void Render() const;
  void Clear();

private:
  container::UnboundedStack<TextVertex> text_vertices_stack;
  container::UnboundedStack<graphics::ColoredVertex> line_vertices_buffer;
};

}

#endif
