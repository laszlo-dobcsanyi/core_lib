#ifndef _GUI_TEXT_H_
#define _GUI_TEXT_H_

#include "graphics/mesh.hpp"

#include "gui/font_atlas.h"

////////////////////////////////////////////////////////////////////////////////
// gui
////////////////////////////////////////////////////////////////////////////////

namespace gui {

/*******************************************************************************
** TextVertex
*******************************************************************************/

struct TextVertex {
  numerics::Vector4f position_and_uv;

private:
  template<typename VertexAttributeBinder>
  friend void tag_invoke(graphics::bind_attributes_t, mpl::type<TextVertex>,
                         VertexAttributeBinder & _binder) {
    _binder.Bind(&TextVertex::position_and_uv);
  }
};

/*******************************************************************************
** TextMeasures
*******************************************************************************/

struct TextMeasures {
  int32 top = 0;
  int32 bottom = 0;
  int32 width = 0;
  int32 height = 0;
};

/*******************************************************************************
** Text
*******************************************************************************/

struct Text {
  core::Ref<FontAtlas> font_atlas;
  graphics::ArrayMesh<TextVertex> mesh;
  TextMeasures measures;

  Text(core::Ref<FontAtlas> _font_atlas)
      : font_atlas(move(_font_atlas)) {}
  Text(core::Ref<FontAtlas> _font_atlas, graphics::ArrayMesh<TextVertex> _mesh,
       TextMeasures const & _measures)
      : font_atlas(move(_font_atlas))
      , mesh(move(_mesh))
      , measures(_measures) {}
  Text(Text &&) = default;
  Text(Text const &) = delete;
  Text & operator=(Text &&) = default;
  Text & operator=(Text const &) = delete;
  ~Text() = default;
};

/*******************************************************************************
** measure_text
*******************************************************************************/

TextMeasures measure_text(FontAtlas const & _font_atlas,
                          const_string const & _string);

/*******************************************************************************
** create_text
*******************************************************************************/

Text create_text(core::Ref<FontAtlas> _font_atlas,
                 const_string const & _string);

/*******************************************************************************
** set_text
*******************************************************************************/

void set_text(Text & _text, const_string const & _string);

}

#endif
