#ifndef _GUI_FONT_ATLAS_H_
#define _GUI_FONT_ATLAS_H_

#include "core/fs/filepath.h"
#include "graphics/texture.h"

////////////////////////////////////////////////////////////////////////////////
// gui
////////////////////////////////////////////////////////////////////////////////

namespace gui {

/*******************************************************************************
** FontAtlasGlyph
*******************************************************************************/

struct FontAtlasGlyph {
  numerics::Vector2f top_left_uv;
  numerics::Vector2f bottom_right_uv;

  int32 x_off;
  int32 y_off;
  int32 width;
  int32 height;

  int32 advance;
};

/*******************************************************************************
** FontAtlas
*******************************************************************************/

struct FontAtlas : public graphics::Resource<FontAtlas> {
  graphics::Texture texture;
  core::UniqueArray<FontAtlasGlyph> glyphs;
};

/*******************************************************************************
** font_atlas_from_file
*******************************************************************************/

core::Optional<FontAtlas>
font_atlas_from_file(core::fs::FilePath const & _file_path, uint32 _size,
                     int32 _dpi, uint32 _glyph_count);

core::Optional<FontAtlas>
font_atlas_from_file(core::fs::FilePath const & _file_path, uint32 _size);

}

#endif
