#ifndef _GRAPHICS_GUI_CONTEXT_HPP_
#define _GRAPHICS_GUI_CONTEXT_HPP_

#ifndef CORE_GUI
#  error
#endif

////////////////////////////////////////////////////////////////////////////////
// graphics gui
////////////////////////////////////////////////////////////////////////////////

namespace graphics { namespace gui {

/*******************************************************************************
** Context
*******************************************************************************/

class Context {
public:
  Context() = default;
  Context(Context &&) = default;
  Context(Context const &) = delete;
  Context & operator=(Context &&) = default;
  Context & operator=(Context const &) = delete;
  ~Context() = default;
};

}}

#endif
