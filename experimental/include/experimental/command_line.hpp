#ifndef _CORE_LIB_EXPERIMENTAL_COMMAND_LINE_HPP_
#define _CORE_LIB_EXPERIMENTAL_COMMAND_LINE_HPP_

////////////////////////////////////////////////////////////////////////////////
// e detail
////////////////////////////////////////////////////////////////////////////////

namespace experimental { namespace command_line { namespace detail {

/*******************************************************************************
** Variable
*******************************************************************************/

struct Variable {
  char const * key;
  void * value;
};

/*******************************************************************************
** VariableList
*******************************************************************************/

template<size_t Size>
struct VariableList {
  Variable variables[Size];
};

/*******************************************************************************
** Args
*******************************************************************************/

struct Args {};

/*******************************************************************************
** add_option
*******************************************************************************/

template<size_t Size, size_t Index>
inline void add_option(VariableList<Size> & _list, Variable && _variable) {
  _list.variables[Index] = move(_variable);
}

}}}

////////////////////////////////////////////////////////////////////////////////
// experimental command_line
////////////////////////////////////////////////////////////////////////////////

namespace experimental { namespace command_line {

/*******************************************************************************
** option
*******************************************************************************/

template<typename ValueType>
inline detail::Option option(char const * _key, ValueType & value_ptr) {
  return detail::Option(_key, value_ptr);
}

/*******************************************************************************
** parse
*******************************************************************************/

template<typename... Options>
inline void parse(detail::Args _args, Options &&... _options) {
  detail::VariableList<sizeof...(Options)> variable_list;
}

/*******************************************************************************
** Arguments
*******************************************************************************/

class Arguments {
public:
  Arguments() = default;
  Arguments(Arguments &&) = default;
  Arguments(Arguments const &) = delete;
  Arguments & operator=(Arguments &&) = default;
  Arguments & operator=(Arguments const &) = delete;
  ~Arguments() = default;
};

// Example:
//
// struct Parameters {
//   char const * file_name;
//   bool option_1;
//   bool option_2;
// };
//
// Parameters parameters;
// parse( arguments( argv, argc )
//      , options( { { "file_name", parameters.file_name }
//                 , { "option_1",  parameters.option_1 }
//                 , { "option_2",  parameters.option_2 } } );

}}

#endif