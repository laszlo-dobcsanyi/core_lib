#ifndef _CORE_LIB_REFLECTION_HPP_
#define _CORE_LIB_REFLECTION_HPP_

#include "experimental/reflection.hh"

#include "core/containers.hpp"
#include "core/string.hpp"

////////////////////////////////////////////////////////////////////////////////
//                                                                                                //
////////////////////////////////////////////////////////////////////////////////

template<typename T>
inline void construct(void * _ptr) {
  new (reinterpret_cast<T *>(_ptr)) T();
}

template<typename T>
inline void serialize(void * _ptr, reflection::Serializer & _serializer) {
  _serializer >> *(reinterpret_cast<T *>(_ptr));
}

template<typename T>
inline void destruct(void * _ptr) {
  delete reinterpret_cast<T *>(_ptr);
}

////////////////////////////////////////////////////////////////////////////////
// reflection
////////////////////////////////////////////////////////////////////////////////

namespace reflection {

/*******************************************************************************
** Serializer
*******************************************************************************/

class Serializer {
public:
  Serializer & operator<<(uint32 _value) { return *this; }
  Serializer & operator<<(float _value) { return *this; }
  Serializer & operator<<(const_string const & _value) { return *this; }

  Serializer & operator>>(uint32 & _value) {
    _value = 42;
    return *this;
  }
  Serializer & operator>>(float & _value) {
    _value = 42.0f;
    return *this;
  }
  Serializer & operator>>(const_string & _value) {
    _value = "42";
    return *this;
  }

  Serializer & operator|(uint32 & _value) { return *this; }
  Serializer & operator|(float & _value) { return *this; }
  Serializer & operator|(const_string & _value) { return *this; }
};

/*******************************************************************************
** Object
*******************************************************************************/

class Object {
public:
  const_string const & Name() const { return name; }
  size_t Size() const { return size; }
  size_t Alignment() const { return alignment; }

protected:
  TypeSystem * type_system = nullptr;
  const_string name;
  size_t size;
  size_t alignment;
  Constructor constructor;
  Creator creator;
  Destructor destructor;

  Object(TypeSystem * _type_system, const_string _name, size_t _size,
         size_t _alignment, Constructor _constructor, Creator _creator,
         Destructor _destructor)
      : type_system(_type_system)
      , name(move(_name))
      , size(_size)
      , alignment(_alignment)
      , constructor(_constructor)
      , creator(_creator)
      , destructor(_destructor) {}
  Object(Object &&) = default;
  Object(Object const &) = default;
  Object & operator=(Object &&) = default;
  Object & operator=(Object const &) = default;
  ~Object() = default;

  void Construct(void * _ptr) {
    if (constructor)
      constructor(_ptr);
  }

  void Create(void * _ptr, Serializer & _serializer) {
    if (creator)
      creator(_ptr, _serializer);
  }

  void Destruct(void * _ptr) {
    if (destructor)
      destructor(_ptr);
  }
};

/*******************************************************************************
** MemberInfo
*******************************************************************************/

class MemberInfo : public Object {
public:
  friend class TypeInfo;
  template<typename C>
  friend class Type;

  MemberInfo() = delete;
  template<class C, typename T>
  MemberInfo(TypeSystem * _type_system, const_string _name,
             const_string _type_name, T C::*_member);
  template<class C, typename T>
  MemberInfo(TypeSystem * _type_system, const_string _name,
             const_string _type_name, core::DefaultUniqueArray<T> C::*_array);
  MemberInfo(MemberInfo &&) = default;
  MemberInfo(MemberInfo const &) = delete;
  MemberInfo & operator=(MemberInfo &&) = default;
  MemberInfo & operator=(MemberInfo const &) = delete;
  ~MemberInfo() = default;

  const_string const & TypeName() const { return type_name; }
  uintptr_t Offset() const { return offset; }

private:
  const_string type_name;
  TypeInfo * type_info;
  uintptr_t offset;

  MemberInfo(TypeSystem * _type_system, const_string _name, size_t _size,
             size_t _alignment, Constructor _constructor, Creator _creator,
             Destructor _destructor, const_string _type_name,
             uintptr_t _offset);

  TypeInfo & GetTypeInfo();

  void CreateProperty(void * _ptr, Serializer & _serializer);

  template<typename U>
  void CreateArray(void * _ptr, Serializer & _serializer);
};

/*******************************************************************************
** TypeInfo
*******************************************************************************/

class TypeInfo : public Object {
public:
  friend class MemberInfo;

  TypeInfo(TypeSystem * _type_system, const_string _name, size_t _size,
           size_t _alignment, Constructor _constructor, Creator _creator,
           Destructor _destructor, uint32 _id)
      : Object(_type_system, move(_name), _size, _alignment, _constructor,
               _creator, _destructor)
      , id(_id) {}

protected:
  container::Array<MemberInfo> member_infos;
  uint32 id;
};

/*******************************************************************************
** Type
*******************************************************************************/

template<class C>
class Type : private TypeInfo {
public:
  template<typename T>
  Type & Property(const_string _name, const_string _type_name, T C::*_member) {
    member_infos.Create(type_system, move(_name), move(_type_name), _member);
    return *this;
  }

  template<typename T>
  Type & Array(const_string _name, const_string _type_name,
               core::DefaultUniqueArray<T> C::*_array) {
    member_infos.Create(type_system, move(_name), move(_type_name), _array);
    return *this;
  }
};

/*******************************************************************************
** TypeSystem
*******************************************************************************/

class TypeSystem {
public:
  TypeSystem() {
    Add<uint32>("uint32");
    Add<float>("float");
    Add<const_string>("const_string");
  }

  TypeInfo * GetTypeInfo(const_string _type_name) {
    for (TypeInfo & _type_info : type_infos) {
      if (_type_info.Name() == _type_name) {
        return &_type_info;
      }
    }
    return nullptr;
  }

  template<class C>
  Type<C> & Create(const_string _name) {
    return Create<C>(move(_name), Creator());
  }

  template<class C>
  Type<C> & Create(const_string _name, Creator _creator) {
    CORE_LIB_STATIC_ASSERT(sizeof(Type<C>) == sizeof(TypeInfo));
    TypeInfo & type_info = Add<C>(_name, _creator);
    return reinterpret_cast<Type<C> &>(type_info);
  }

private:
  container::Array<TypeInfo> type_infos;

  template<typename T>
  TypeInfo & Add(const_string _name) {
    return Add<T>(move(_name), Creator::Bind(&serialize<T>));
  }

  template<typename T>
  TypeInfo & Add(const_string _name, Creator _creator) {
    return type_infos.Create(this, move(_name), sizeof(T), alignof(T),
                             Constructor::Bind(&construct<T>), _creator,
                             Destructor::Bind(&destruct<T>), type_infos.Size());
  }
};

}

////////////////////////////////////////////////////////////////////////////////
// reflection
////////////////////////////////////////////////////////////////////////////////

namespace reflection {

/*******************************************************************************
** MemberInfo
*******************************************************************************/

template<class C, typename T>
inline MemberInfo::MemberInfo(TypeSystem * _type_system, const_string _name,
                              const_string _type_name, T C::*_member)
    : MemberInfo(_type_system, move(_name), sizeof(T), alignof(T),
                 Constructor::Bind(&construct<T>),
                 Creator::Bind(this).Method<&MemberInfo::CreateProperty>(),
                 Destructor::Bind(&destruct<T>), move(_type_name),
                 core::memory::offset_of(_member)) {}

template<class C, typename T>
inline MemberInfo::MemberInfo(TypeSystem * _type_system, const_string _name,
                              const_string _type_name,
                              core::DefaultUniqueArray<T> C::*_array)
    : MemberInfo(_type_system, move(_name), sizeof(T), alignof(T),
                 Constructor::Bind(&construct<T>),
                 Creator::Bind(this).Method<&MemberInfo::CreateArray<T>>(),
                 Destructor::Bind(&destruct<T>), move(_type_name),
                 core::memory::offset_of(_array)) {}

inline MemberInfo::MemberInfo(TypeSystem * _type_system, const_string _name,
                              size_t _size, size_t _alignment,
                              Constructor _constructor, Creator _creator,
                              Destructor _destructor, const_string _type_name,
                              uintptr_t _offset)
    : Object(_type_system, move(_name), _size, _alignment, _constructor,
             _creator, _destructor)
    , type_name(move(_type_name))
    , type_info(nullptr)
    , offset(_offset) {}

inline TypeInfo & MemberInfo::GetTypeInfo() {
  if (!type_info) {
    type_info = type_system->GetTypeInfo(type_name);
    CORE_LIB_ASSERT(type_info);
  }
  return *type_info;
}

inline void MemberInfo::CreateProperty(void * _ptr, Serializer & _serializer) {
  void * property_ptr =
      reinterpret_cast<void *>(reinterpret_cast<uintptr_t>(_ptr) + offset);
  GetTypeInfo().Create(property_ptr, _serializer);
}

template<typename T>
inline void MemberInfo::CreateArray(void * _ptr, Serializer & _serializer) {
  uint32 array_size;
  _serializer >> array_size;
  core::UniqueArray<T> * array_ptr = reinterpret_cast<core::UniqueArray<T> *>(
      reinterpret_cast<uintptr_t>(_ptr) + offset);
  *array_ptr = core::make_unique_array<T>(array_size);
  for (T & _element : *array_ptr) {
    GetTypeInfo().Create(&_element, _serializer);
  }
}

}

#endif
