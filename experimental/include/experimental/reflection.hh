#ifndef _CORE_LIB_REFLECTION_HH_
#define _CORE_LIB_REFLECTION_HH_

#include "core/callable/delegate.hpp"

////////////////////////////////////////////////////////////////////////////////
// reflection
////////////////////////////////////////////////////////////////////////////////

namespace reflection {

class Serializer;
class Object;
class MemberInfo;
class TypeInfo;
template<class C>
class Type;
class TypeSystem;

using Constructor = core::Delegate<void(void *)>;
using Creator = core::Delegate<void(void *, Serializer &)>;
using Destructor = core::Delegate<void(void *)>;

}

#endif
