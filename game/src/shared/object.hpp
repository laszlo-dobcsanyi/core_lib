#ifndef _GAME_SHARED_OBJECT_HPP_
#define _GAME_SHARED_OBJECT_HPP_

#include "core/memory/object.hpp"

namespace game {

template<typename T>
using Object = core::WeakObjectWrapper<T>;

template<typename T>
using ObjectBase = core::WeakCounter<T>;

template<typename T>
using Ref = core::Ref<T>;

template<typename T>
using WeakRef = core::Ptr<T>;

}

#endif
