#ifndef _GAME_SHARED_UNIQUE_ID_H_
#define _GAME_SHARED_UNIQUE_ID_H_

#include "core/base.h"

////////////////////////////////////////////////////////////////////////////////
// game
////////////////////////////////////////////////////////////////////////////////

namespace game {

/*******************************************************************************
** UniqueId
*******************************************************************************/

using UniqueId = core::Value<uint32, U32(0xFFFFFFFF)>;

/*******************************************************************************
** UniqueIdGenerator
*******************************************************************************/

class UniqueIdGenerator {
public:
  UniqueIdGenerator() = default;
  UniqueIdGenerator(UniqueIdGenerator &&) = default;
  UniqueIdGenerator(UniqueIdGenerator const &) = delete;
  UniqueIdGenerator & operator=(UniqueIdGenerator &&) = default;
  UniqueIdGenerator & operator=(UniqueIdGenerator const &) = default;
  ~UniqueIdGenerator() = default;

  UniqueId Next() {
    ++unique_id;
    if (!unique_id.HasValue()) {
      ++unique_id;
    }
    return unique_id;
  }

private:
  UniqueId unique_id;
};

}

#endif