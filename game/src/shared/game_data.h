#ifndef _GAME_SHARED_GAME_DATA_H_
#define _GAME_SHARED_GAME_DATA_H_

#include "core/base.h"
#include "core/containers.hpp"
#include "core/numerics.hpp"
#include "core/string.hpp"
#include "core/fs/directory.h"
#include "core/optional.hpp"
#include "core/variant.hpp"

#include "shared/object.hpp"
#include "shared/unique_id.h"

////////////////////////////////////////////////////////////////////////////////
// game game_data
////////////////////////////////////////////////////////////////////////////////

namespace game { namespace game_data {

/*******************************************************************************
** Map
*******************************************************************************/

struct Map : public ObjectBase<Map> {
  UniqueId unique_id;
  const_string name;
};

/*******************************************************************************
** StaticModel
*******************************************************************************/

struct StaticModel : public ObjectBase<StaticModel> {
  const_string name;
};

/*******************************************************************************
** Animation
*******************************************************************************/

struct AnimationNode : public ObjectBase<AnimationNode> {
  const_string name;
  uint32 priority;
  float blend_in;
  float blend_out;
};

struct StateAnimationNode : public ObjectBase<StateAnimationNode> {
  const_string idle;
  const_string run_forward;
  const_string run_backward;
  const_string run_left;
  const_string run_right;
  const_string attack;

  float run_speed;
};

/*******************************************************************************
** AnimatedModel
*******************************************************************************/

struct AnimatedModel : public ObjectBase<AnimatedModel> {
  const_string name;
  StateAnimationNode state_animation_node;
  container::Array<AnimationNode> animation_nodes;
};

/*******************************************************************************
** Bullet
*******************************************************************************/

struct Bullet : public ObjectBase<Bullet> {
  UniqueId unique_id;
  const_string name;

  StaticModel static_model;

  float range;
  float velocity;
};

/*******************************************************************************
** Weapon
*******************************************************************************/

struct Weapon : public ObjectBase<Weapon> {
  UniqueId unique_id;
  const_string name;

  StaticModel static_model;
};

/*******************************************************************************
** Entity
*******************************************************************************/

struct Entity : public ObjectBase<Entity> {
  UniqueId unique_id;
  const_string name;
  core::Optional<StaticModel> static_model;
  core::Optional<AnimatedModel> animated_model;

  numerics::Vector3f shot_pos;
  const_string attachment_bone;
  numerics::Vector3f attachment_translation;
  numerics::Vector3f attachment_direction;

  float friction;
  float input_acceleration;
  float max_speed;
};

}}

////////////////////////////////////////////////////////////////////////////////
// game
////////////////////////////////////////////////////////////////////////////////

namespace game {

/*******************************************************************************
** GameData
*******************************************************************************/

struct GameData {
  container::Array<game_data::Map> maps;
  container::Array<game_data::Entity> entities;
  container::Array<game_data::Weapon> weapons;
  container::Array<game_data::Bullet> bullets;

  core::Optional<Ref<game_data::Map>> GetMapData(UniqueId _map_unique_id);
  core::Optional<Ref<game_data::Entity>>
  GetEntityData(UniqueId _entity_unique_id);
  core::Optional<Ref<game_data::Weapon>>
  GetWeaponData(UniqueId _weapon_unique_id);
  core::Optional<Ref<game_data::Bullet>>
  GetBulletData(UniqueId _bullet_unique_id);
};

/*******************************************************************************
** load_game_data
*******************************************************************************/

core::Optional<GameData> load_game_data();

/*******************************************************************************
** default_data_directory
*******************************************************************************/

core::fs::Directory default_data_directory();

}

#endif
