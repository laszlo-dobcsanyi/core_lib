#ifndef _GAME_SHARED_MOVEMENT_H_
#define _GAME_SHARED_MOVEMENT_H_

#include "core/base.h"
#include "core/numerics.hpp"

#define LOG_SHARED_MOVEMENT(__severity__, ...)                                 \
  CORE_LIB_LOG(__severity__, __VA_ARGS__)

////////////////////////////////////////////////////////////////////////////////
// game
////////////////////////////////////////////////////////////////////////////////

namespace game {

/*******************************************************************************
** Movement
*******************************************************************************/

class MovementState {
public:
  float delta_seconds;
  numerics::Vector3f acceleration;
  numerics::Vector3f velocity;
  numerics::Vector3f position;

  float friction;
  float max_speed;

  MovementState();
  MovementState(MovementState &&) = default;
  MovementState(MovementState const &) = default;
  MovementState & operator=(MovementState &&) = default;
  MovementState & operator=(MovementState const &) = default;
  ~MovementState() = default;

  void Reset();
  void Tick(float _delta_seconds);
};

}

#endif