#include "shared/movement.h"

////////////////////////////////////////////////////////////////////////////////
// game
////////////////////////////////////////////////////////////////////////////////

namespace game {

/*******************************************************************************
** Movement
*******************************************************************************/

MovementState::MovementState()
    : delta_seconds(0.f)
    , acceleration(0.f, 0.f, 0.f)
    , velocity(0.f, 0.f, 0.f)
    , position(0.f, 0.f, 0.f)
    , friction(0.f)
    , max_speed(0.f) {}

void MovementState::Reset() {
  delta_seconds = 0.f;
  acceleration = numerics::Vector3f(0.f, 0.f, 0.f);
  velocity = numerics::Vector3f(0.f, 0.f, 0.f);
  position = numerics::Vector3f(0.f, 0.f, 0.f);
  friction = 0.f;
  max_speed = 0.f;
}

void MovementState::Tick(float _delta_seconds) {
  delta_seconds = _delta_seconds;
  if (!numerics::is_nearly_zero(acceleration)) {
    velocity += acceleration * _delta_seconds;
    if (max_speed < numerics::length(velocity)) {
      velocity = numerics::normalized(velocity) * max_speed;
    }
  } else if (!numerics::is_nearly_zero(velocity)) {
    numerics::Vector3f old_velocity = velocity;
    if (0.f < friction) {
      numerics::Vector3f deceleration = velocity * -friction;
      velocity += deceleration * _delta_seconds;
      if (numerics::dot(old_velocity, velocity) <= 0.f) {
        velocity = numerics::Vector3f(0.f, 0.f, 0.f);
      }
    }
  }
  position += velocity * _delta_seconds;
  LOG_SHARED_MOVEMENT(VERBOSE, "Acceleration: %.4f, %.4f, %.4f\n",
                      acceleration[0u], acceleration[1u], acceleration[2u]);
  LOG_SHARED_MOVEMENT(VERBOSE, "Velocity: %.4f, %.4f, %.4f\n", velocity[0u],
                      velocity[1u], velocity[2u]);
  LOG_SHARED_MOVEMENT(VERBOSE, "Position: %.4f, %.4f, %.4f\n", position[0u],
                      position[1u], position[2u]);
}

}
