#include "shared/game_data.h"

#define LOG_GAME_DATA(__severity__, ...) CORE_LIB_LOG(__severity__, __VA_ARGS__)

////////////////////////////////////////////////////////////////////////////////
// game
////////////////////////////////////////////////////////////////////////////////

namespace game {

/*******************************************************************************
** GameData
*******************************************************************************/

core::Optional<Ref<game_data::Map>>
GameData::GetMapData(UniqueId _map_unique_id) {
  if (_map_unique_id < maps.Size()) {
    Ref<game_data::Map> result(maps[_map_unique_id]);
    CORE_LIB_ASSERT(result->unique_id == _map_unique_id);
    return result;
  }
  LOG_GAME_DATA(ERROR, "Map with unique_id %" PRIu32 " not found!\n",
                _map_unique_id.Get());
  return core::nullopt;
}

core::Optional<Ref<game_data::Entity>>
GameData::GetEntityData(UniqueId _entity_unique_id) {
  if (_entity_unique_id < entities.Size()) {
    Ref<game_data::Entity> result(entities[_entity_unique_id]);
    CORE_LIB_ASSERT(result->unique_id == _entity_unique_id);
    return result;
  }
  LOG_GAME_DATA(ERROR, "Entity with unique_id %" PRIu32 " not found!\n",
                _entity_unique_id.Get());
  return core::nullopt;
}

core::Optional<Ref<game_data::Weapon>>
GameData::GetWeaponData(UniqueId _weapon_unique_id) {
  if (_weapon_unique_id < weapons.Size()) {
    Ref<game_data::Weapon> result(weapons[_weapon_unique_id]);
    CORE_LIB_ASSERT(result->unique_id == _weapon_unique_id);
    return result;
  }
  LOG_GAME_DATA(ERROR, "Weapon with unique_id %" PRIu32 " not found!\n",
                _weapon_unique_id.Get());
  return core::nullopt;
}

core::Optional<Ref<game_data::Bullet>>
GameData::GetBulletData(UniqueId _bullet_unique_id) {
  if (_bullet_unique_id < bullets.Size()) {
    Ref<game_data::Bullet> result(bullets[_bullet_unique_id]);
    CORE_LIB_ASSERT(result->unique_id == _bullet_unique_id);
    return result;
  }
  LOG_GAME_DATA(ERROR, "Bullet with unique_id %" PRIu32 " not found!\n",
                _bullet_unique_id.Get());
  return core::nullopt;
}

/*******************************************************************************
** load_game_data
*******************************************************************************/

core::Optional<GameData> load_game_data() {
  GameData game_data;

  // Maps
  UniqueIdGenerator map_unique_id_generator;
  {
    game_data::Map map;
    map.unique_id = map_unique_id_generator.Next();
    map.name = "test";
    game_data.maps.Create(move(map));
  }

  // Entities
  UniqueIdGenerator entity_unique_id_generator;

  // Kachujin
  {
    game_data::Entity entity;
    entity.unique_id = entity_unique_id_generator.Next();
    entity.name = "kachujin";
    // AnimatedModel
    {
      entity.animated_model = in_place_t<void>();
      game_data::AnimatedModel & animated_model = entity.animated_model.Value();
      animated_model.name = "kachujin";
      // StateAnimationNode
      {
        game_data::StateAnimationNode & state_animation_node =
            animated_model.state_animation_node;
        state_animation_node.idle = "idle";
        state_animation_node.run_forward = "run_forward";
        state_animation_node.run_backward = "run_backward";
        state_animation_node.run_right = "run_right";
        state_animation_node.run_left = "run_left";
        state_animation_node.attack = "shoot";

        state_animation_node.run_speed = 300.f;
      }
    }
    entity.shot_pos = numerics::Vector3f(50.f, 125.f, 25.f);
    entity.attachment_bone = "RightHand";
    entity.attachment_translation = numerics::Vector3f(-10, 0.f, 0.f);
    entity.attachment_direction = numerics::Vector3f(120.f, 0.f, -90.f);
    entity.friction = 9.f;
    entity.input_acceleration = 3000.f;
    entity.max_speed = 600.f;
    game_data.entities.Create(move(entity));
  }

  // Ely
  {
    game_data::Entity entity;
    entity.unique_id = entity_unique_id_generator.Next();
    entity.name = "ely";
    // AnimatedModel
    {
      entity.animated_model = in_place_t<void>();
      game_data::AnimatedModel & animated_model = entity.animated_model.Value();
      animated_model.name = "ely";
      // StateAnimationNode
      {
        game_data::StateAnimationNode & state_animation_node =
            animated_model.state_animation_node;
        state_animation_node.idle = "idle";
        state_animation_node.run_forward = "run_forward";
        state_animation_node.run_backward = "run_backward";
        state_animation_node.run_right = "run_right";
        state_animation_node.run_left = "run_left";
        state_animation_node.attack = "shoot";

        state_animation_node.run_speed = 300.f;
      }
    }
    entity.shot_pos = numerics::Vector3f(50.f, 125.f, 25.f);
    entity.attachment_bone = "mixamorig:RightHand";
    entity.attachment_translation = numerics::Vector3f(-10, 0.f, 0.f);
    entity.attachment_direction = numerics::Vector3f(120.f, 0.f, -90.f);
    entity.friction = 9.f;
    entity.input_acceleration = 3000.f;
    entity.max_speed = 600.f;
    game_data.entities.Create(move(entity));
  }

  // Parasite
  {
    game_data::Entity entity;
    entity.unique_id = entity_unique_id_generator.Next();
    entity.name = "parasite";
    // AnimatedModel
    {
      entity.animated_model = in_place_t<void>();
      game_data::AnimatedModel & animated_model = entity.animated_model.Value();
      animated_model.name = "parasite";
      // StateAnimationNode
      {
        game_data::StateAnimationNode & state_animation_node =
            animated_model.state_animation_node;
        state_animation_node.idle = "idle";
        state_animation_node.run_forward = "run_forward";
        state_animation_node.run_backward = "run_backward";
        state_animation_node.run_right = "run_right";
        state_animation_node.run_left = "run_left";
        state_animation_node.attack = "attack";

        state_animation_node.run_speed = 250.f;
      }
    }
    entity.shot_pos = numerics::Vector3f(0.f, 0.f, 0.f);
    entity.attachment_bone = "RightHand";
    entity.attachment_translation = numerics::Vector3f(100.f, 0.f, 0.f);
    entity.attachment_direction = numerics::Vector3f(105.f, 0.f, -90.f);
    entity.friction = 9.f;
    entity.input_acceleration = 3000.f;
    entity.max_speed = 450.f;
    game_data.entities.Create(move(entity));
  }

  // Pumpkin
  {
    game_data::Entity entity;
    entity.unique_id = entity_unique_id_generator.Next();
    entity.name = "pumpkin";
    // AnimatedModel
    {
      entity.animated_model = in_place_t<void>();
      game_data::AnimatedModel & animated_model = entity.animated_model.Value();
      animated_model.name = "pumpkin";
      // StateAnimationNode
      {
        game_data::StateAnimationNode & state_animation_node =
            animated_model.state_animation_node;
        state_animation_node.idle = "idle";
        state_animation_node.run_forward = "run_forward";
        state_animation_node.run_backward = "run_backward";
        state_animation_node.run_right = "run_right";
        state_animation_node.run_left = "run_left";
        state_animation_node.attack = "attack";

        state_animation_node.run_speed = 250.f;
      }
    }
    entity.shot_pos = numerics::Vector3f(0.f, 0.f, 0.f);
    entity.attachment_bone = "RightHand";
    entity.attachment_translation = numerics::Vector3f(100.f, 0.f, 0.f);
    entity.attachment_direction = numerics::Vector3f(105.f, 0.f, -90.f);
    entity.friction = 9.f;
    entity.input_acceleration = 3000.f;
    entity.max_speed = 450.f;
    game_data.entities.Create(move(entity));
  }

  // Warrok
  {
    game_data::Entity entity;
    entity.unique_id = entity_unique_id_generator.Next();
    entity.name = "warrok";
    // AnimatedModel
    {
      entity.animated_model = in_place_t<void>();
      game_data::AnimatedModel & animated_model = entity.animated_model.Value();
      animated_model.name = "warrok";
      // StateAnimationNode
      {
        game_data::StateAnimationNode & state_animation_node =
            animated_model.state_animation_node;
        state_animation_node.idle = "idle";
        state_animation_node.run_forward = "run_forward";
        state_animation_node.run_backward = "run_backward";
        state_animation_node.run_right = "run_right";
        state_animation_node.run_left = "run_left";
        state_animation_node.attack = "attack";

        state_animation_node.run_speed = 250.f;
      }
    }
    entity.shot_pos = numerics::Vector3f(0.f, 0.f, 0.f);
    entity.attachment_bone = "RightHand";
    entity.attachment_translation = numerics::Vector3f(100.f, 0.f, 0.f);
    entity.attachment_direction = numerics::Vector3f(105.f, 0.f, -90.f);
    entity.friction = 9.f;
    entity.input_acceleration = 3000.f;
    entity.max_speed = 450.f;
    game_data.entities.Create(move(entity));
  }

  // Zombie
  {
    game_data::Entity entity;
    entity.unique_id = entity_unique_id_generator.Next();
    entity.name = "zombie";
    // AnimatedModel
    {
      entity.animated_model = in_place_t<void>();
      game_data::AnimatedModel & animated_model = entity.animated_model.Value();
      animated_model.name = "zombie";
      // StateAnimationNode
      {
        game_data::StateAnimationNode & state_animation_node =
            animated_model.state_animation_node;
        state_animation_node.idle = "idle";
        state_animation_node.run_forward = "run_forward";
        state_animation_node.run_backward = "run_backward";
        state_animation_node.run_right = "run_right";
        state_animation_node.run_left = "run_left";
        state_animation_node.attack = "attack";

        state_animation_node.run_speed = 250.f;
      }
    }
    entity.shot_pos = numerics::Vector3f(0.f, 0.f, 0.f);
    entity.attachment_bone = "RightHand";
    entity.attachment_translation = numerics::Vector3f(100.f, 0.f, 0.f);
    entity.attachment_direction = numerics::Vector3f(105.f, 0.f, -90.f);
    entity.friction = 9.f;
    entity.input_acceleration = 3000.f;
    entity.max_speed = 450.f;
    game_data.entities.Create(move(entity));
  }

  // Weapons
  UniqueIdGenerator weapon_unique_id_generator;
  {
    game_data::Weapon m4a1;
    m4a1.unique_id = weapon_unique_id_generator.Next();
    m4a1.name = "m4a1";
    // StaticModel
    {
      game_data::StaticModel & static_model = m4a1.static_model;
      static_model.name = "m4a1";
    }
    game_data.weapons.Create(move(m4a1));
  }

  // Bullets
  UniqueIdGenerator bullet_unique_id_generator;
  {
    game_data::Bullet bullet_5_56;
    bullet_5_56.unique_id = bullet_unique_id_generator.Next();
    bullet_5_56.name = "bullet_5_56";
    // StaticModel
    {
      game_data::StaticModel & static_model = bullet_5_56.static_model;
      static_model.name = "bullet_5_56";
    }
    bullet_5_56.range = 25000.f;
    bullet_5_56.velocity = 5000.f;
    game_data.bullets.Create(move(bullet_5_56));
  }

  return game_data;
}

/*******************************************************************************
** default_data_directory
*******************************************************************************/

core::fs::Directory default_data_directory() {
  core::fs::Directory executable_directory =
      core::fs::Directory(core::fs::path_parent(core::fs::executable_path()));
  if (auto executable_parent_directory =
          executable_directory.GetParentDirectory()) {
    return core::fs::Directory(*executable_parent_directory / "data");
  }
  return executable_directory;
}

}
