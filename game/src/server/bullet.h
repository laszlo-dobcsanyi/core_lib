#ifndef _GAME_SERVER_BULLET_H_
#define _GAME_SERVER_BULLET_H_

#include "server/server.hh"

////////////////////////////////////////////////////////////////////////////////
// game server
////////////////////////////////////////////////////////////////////////////////

namespace game { namespace server {

/*******************************************************************************
** Bullet
*******************************************************************************/

class Bullet : public ObjectBase<Bullet> {
public:
  Ref<game_data::Bullet> bullet_data;
  UniqueId unique_id;
  WeakRef<Player> player;

  Bullet() = delete;
  Bullet(World & _world, Ref<game_data::Bullet> _bullet_data,
         UniqueId _unique_id);
  Bullet(Bullet &&) = delete;
  Bullet(Bullet const &) = delete;
  Bullet & operator=(Bullet &&) = delete;
  Bullet & operator=(Bullet const &) = delete;
  ~Bullet() = default;

  void SetPlayer(WeakRef<Player> _player);

  numerics::Vector3f GetPosition() const;
  void SetPosition(numerics::Vector3f _position);

  numerics::Vector3f GetDirection() const;
  void SetDirection(numerics::Vector3f _position);

  numerics::Vector3f GetVelocity() const;
  void SetVelocity(numerics::Vector3f _velocity);

  bool ShouldBeDestroyed() const;

  void Tick(float _delta_seconds);

private:
  Ref<World> world;
  numerics::Vector3f position;
  numerics::Vector3f direction;
  MovementState movement_state;
  float time;
  uint8 should_be_destroyed : 1;
};

}}

#endif
