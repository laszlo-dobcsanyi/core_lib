#ifndef _GAME_SERVER_SERVER_HH_
#define _GAME_SERVER_SERVER_HH_

#include "game.hh"
// TODO:
#include "core/containers.hpp"
#include "core/numerics.hpp"

#include "shared/object.hpp"
#include "shared/game_data.h"
#include "shared/unique_id.h"
#include "shared/movement.h"

// TODO: graphics dependecy
#include "graphics/resource/map_data.hpp"

#define LOG_SERVER(__severity__, ...) LOG_GAME(__severity__, __VA_ARGS__)

////////////////////////////////////////////////////////////////////////////////
// game server
////////////////////////////////////////////////////////////////////////////////

namespace game { namespace server {

// TODO: graphics dependecy
using MapData = graphics::resource::MapData;

class Server;

class Session;
class World;

class Player;
class Bullet;
class Entity;

}}

#endif
