#include "server/entity.h"
#include "server/world.h"

////////////////////////////////////////////////////////////////////////////////
// game server
////////////////////////////////////////////////////////////////////////////////

namespace game { namespace server {

/*******************************************************************************
** Entity
*******************************************************************************/

Entity::Entity(World & _world, Ref<game_data::Entity> _entity_data,
               UniqueId _unique_id)
    : entity_data(move(_entity_data))
    , unique_id(_unique_id)
    , world(_world)
    , position(0.f, 0.f, 0.f)
    , direction(0.f, 0.f, 0.f) {}

bool Entity::IsAttacking() const { return attacking; }

void Entity::SetAttacking(bool _attacking) { attacking = _attacking; }

numerics::Vector3f Entity::GetPosition() const { return position; }

void Entity::SetPosition(numerics::Vector3f _position) { position = _position; }

numerics::Vector3f Entity::GetDirection() const { return direction; }

void Entity::SetDirection(numerics::Vector3f _direction) {
  direction = _direction;
}

numerics::Vector3f Entity::GetForward() const {
  numerics::Vector3f forward = numerics::Vector3f(0.f, 0.f, 1.f);
  numerics::Quaternionf rotation = numerics::to_rotation(direction);
  return numerics::rotate(forward, rotation);
}

numerics::Vector3f Entity::GetWorldShotpos() const {
  numerics::Vector3f const & shot_pos = entity_data->shot_pos;
  numerics::Vector3f up(0.f, 1.f, 0.f);
  numerics::Vector3f right =
      numerics::normalized(numerics::cross(GetForward(), up));
  numerics::Vector3f forward = numerics::normalized(numerics::cross(up, right));
  return GetPosition() + forward * shot_pos[0u] + up * shot_pos[1u] +
         right * shot_pos[2u];
}

void Entity::Tick(float _delta_seconds) {}

}}
