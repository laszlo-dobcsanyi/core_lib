#include "server/session.h"
#include "server/server.h"
#include "net/tcp/connection.h"

////////////////////////////////////////////////////////////////////////////////
// game server
////////////////////////////////////////////////////////////////////////////////

namespace game { namespace server {

/*******************************************************************************
** Session
*******************************************************************************/

Session::Session(Server & _server)
    : server(_server) {}

bool Session::Create(net::ip::Endpoint _local_endpoint, size_t _max_players) {
  if (core::Optional<net::tcp::Listener> listener_result =
          net::tcp::create_listener(_local_endpoint)) {
    LOG_SERVER(LOG, "Created listener at %s!\n",
               cstr(net::ip::to_string(_local_endpoint)));
    listener = move(listener_result.Value());
    return true;
  }
  LOG_SERVER(ERROR, "Failed to create listener at %s!\n",
             cstr(net::ip::to_string(_local_endpoint)));
  return false;
}

void Session::Accept() {
  if (!listener.IsOpen())
    return;

  if (core::Optional<net::tcp::Connection> connection = listener.Accept()) {
    LOG_SERVER(LOG, "Accepted connection from %s!\n",
               cstr(net::ip::to_string(connection->RemoteEndpoint())));
    server->world.CreatePlayer(move(*connection));
  }
}

void Session::Shutdown() { listener.Close(); }

}}
