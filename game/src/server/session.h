#ifndef _GAME_SERVER_SESSION_H_
#define _GAME_SERVER_SESSION_H_

#include "server/server.hh"
#include "server/player.h"

#include "core/containers.hpp"
#include "net/tcp/listener.h"

////////////////////////////////////////////////////////////////////////////////
// game server
////////////////////////////////////////////////////////////////////////////////

namespace game { namespace server {

/*******************************************************************************
** Session
*******************************************************************************/

class Session : public ObjectBase<Session> {
public:
  Ref<Server> server;

  Session() = delete;
  Session(Server & _server);
  Session(Session &&) = default;
  Session(Session const &) = delete;
  Session & operator=(Session &&) = default;
  Session & operator=(Session const &) = delete;
  ~Session() = default;

  bool Create(net::ip::Endpoint _local_endpoint, size_t _max_players);
  void Accept();
  void Shutdown();

private:
  net::tcp::Listener listener;
};

}}

#endif
