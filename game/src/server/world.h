#ifndef _GAME_SERVER_WORLD_H_
#define _GAME_SERVER_WORLD_H_

#include "server/server.hh"
#include "server/player.h"
#include "server/monster.h"
#include "server/bullet.h"
#include "server/entity.h"

////////////////////////////////////////////////////////////////////////////////
// game server
////////////////////////////////////////////////////////////////////////////////

namespace game { namespace server {

/*******************************************************************************
** World
*******************************************************************************/

class World : public ObjectBase<World> {
public:
  Ref<Server> server;

  World() = delete;
  World(Server & _server);
  World(World &&) = default;
  World(World const &) = delete;
  World & operator=(World &&) = default;
  World & operator=(World const &) = delete;
  ~World() = default;

  bool Initialize(MapData _map_data);

  void Tick(float _delta_seconds);

  core::Optional<Ref<Player>> CreatePlayer(net::tcp::Connection _connection);

  Ref<Entity> CreateEntity(Ref<game_data::Entity> _entity_data_ref,
                           numerics::Vector3f _position,
                           numerics::Vector3f _direction);
  void DestroyEntity(Entity & _entity);

  Ref<Bullet> CreateBullet(Ref<game_data::Bullet> _bullet_data_ref,
                           numerics::Vector3f _position,
                           numerics::Vector3f _direction);
  void DestroyBullet(Bullet & _bullet);

  MapData const & GetMapData() const { return map_data; }
  container::PoolArray<Player> & GetPlayers() { return players; }
  container::PoolArray<Monster> & GetMonsters() { return monsters; }
  container::PoolArray<Bullet> & GetBullets() { return bullets; }
  container::PoolArray<Entity> & GetEntities() { return entities; };

private:
  MapData map_data;
  container::PoolArray<Player> players;
  container::PoolArray<Monster> monsters;
  container::PoolArray<Bullet> bullets;
  container::PoolArray<Entity> entities;

  bool survival = true;
  float survival_restart_timer = 0.f;
  float survival_next_wave_timer = 0.f;
  size_t survival_spawn_index = 0u;
  float survival_spawn_timer = 0.f;

  numerics::random::XorShift32 rng;
  UniqueIdGenerator bullet_unique_id_generator;
  UniqueIdGenerator entity_unique_id_generator;

  void SpawnMonsterEntity(Monster & _monster, numerics::Vector3f _position);
  void SpawnPlayerEntity(Player & _player, numerics::Vector3f _position);

  void TickSurvival(float _delta_seconds);
  void TickDeathmatch(float _delta_seconds);
};

}}

#endif
