#include "server/monster.h"
#include "server/world.h"

////////////////////////////////////////////////////////////////////////////////
// game server
////////////////////////////////////////////////////////////////////////////////

namespace game { namespace server {

/*******************************************************************************
** Monster
*******************************************************************************/

Monster::Monster(World & _world)
    : world(_world)
    , approach_angle(0.f)
    , idle_timer(0.f)
    , targeting_timer(0.f)
    , attack_timer(0.f)
    , attack_hit_timer(0.f)
    , attack_delay_timer(0.f) {}

void Monster::SetEntity(WeakRef<Entity> _entity) {
  entity = move(_entity);

  idle_timer = 2.f;
  targeting_timer = 0.f;

  if (entity) {
    movement_state.Reset();
    movement_state.position = entity->GetPosition();
    movement_state.friction = entity->entity_data->friction;
    movement_state.max_speed = entity->entity_data->max_speed;
  }
}

WeakRef<Entity> Monster::GetEntity() const { return entity; }

void Monster::SetApproachAngle(float _approach_angle) {
  approach_angle = _approach_angle;
}

void Monster::Tick(float _delta_seconds) {
  if (!entity.IsBound()) {
    return;
  }

  targeting_timer -= _delta_seconds;
  if (targeting_timer < 0.f) {
    targeting_timer = 0.5f;

    float best_distance = 0.f;
    WeakRef<Entity> best_target;
    for (Player const & player : world->GetPlayers()) {
      if (WeakRef<Entity> controlled_entity = player.GetEntity()) {
        if (best_target) {
          float distance = numerics::distance(controlled_entity->GetPosition(),
                                              entity->GetPosition());
          if (distance < best_distance) {
            best_target = move(controlled_entity);
            best_distance = distance;
          }
        } else {
          best_target = move(controlled_entity);
          best_distance = numerics::distance(best_target->GetPosition(),
                                             entity->GetPosition());
        }
      }
    }

    target = best_target;
  }

  numerics::Vector3f acceleration(0.f, 0.f, 0.f);
  numerics::Vector3f direction = entity->GetDirection();
  if (target) {
    numerics::Vector3f approach_target_vector = target->GetPosition();
    approach_target_vector +=
        numerics::Vector3f(numerics::cos(approach_angle), 0.f,
                           numerics::sin(approach_angle)) *
        500.f;
    approach_target_vector -= entity->GetPosition();
    numerics::Vector3f target_vector =
        target->GetPosition() - entity->GetPosition();
    if (600.f < numerics::length(target_vector)) {
      acceleration = numerics::normalized(approach_target_vector) *
                     entity->entity_data->input_acceleration;
    } else if (250.f < numerics::length(target_vector)) {
      acceleration = numerics::normalized(target_vector) *
                     entity->entity_data->input_acceleration;
    }
    float rotation_distance = 25.f;
    if (rotation_distance < numerics::length(target_vector)) {
      numerics::Vector3f target_direction = numerics::normalized(target_vector);
      direction[1u] = numerics::to_degrees(
          numerics::atan2(target_direction[0u], target_direction[2u]));
    }
  }

  if (0.f < idle_timer) {
    idle_timer -= _delta_seconds;
    entity->SetDirection(direction);
    return;
  }

  if (target && attack_delay_timer <= 0.f) {
    numerics::Vector3f target_vector =
        target->GetPosition() - entity->GetPosition();
    if (numerics::length(target_vector) <= 250.f) {
      attack_timer = 1.f;
      attack_hit_timer = 0.5f;
      attack_delay_timer = 2.f;
      entity->SetAttacking(true);
    }
  }

  if (0.f < attack_delay_timer) {
    attack_delay_timer -= _delta_seconds;
  }

  if (0.f < attack_timer) {
    attack_timer -= _delta_seconds;

    if (0.f < attack_hit_timer) {
      attack_hit_timer -= _delta_seconds;

      if (attack_hit_timer <= 0.f) {
        for (Player & player : world->GetPlayers()) {
          if (WeakRef<Entity> player_entity = player.GetEntity()) {
            numerics::Vector3f target_vector =
                player_entity->GetPosition() - entity->GetPosition();
            if (300.f < numerics::length(target_vector)) {
              continue;
            }
            numerics::Vector3f target_direction =
                numerics::normalized(target_vector);
            if (numerics::dot(entity->GetForward(), target_direction) < 0.5f) {
              continue;
            }

            world->DestroyEntity(*player_entity.GetObject());
          }
        }
      }
    }

    if (attack_timer <= 0.f) {
      entity->SetAttacking(false);
    }
    return;
  }

  MovementState new_movement = movement_state;
  new_movement.acceleration = acceleration;
  new_movement.Tick(_delta_seconds);
  // TODO: Collision
  new_movement.position.y =
      world->GetMapData().grid.height_at(new_movement.position);
  movement_state = new_movement;

  entity->SetPosition(movement_state.position);
  entity->SetDirection(direction);

  LOG_SERVER(VERBOSE, "Position: (%.4f, %.4f, %.4f)\n", new_movement.position.x,
             new_movement.position.y, new_movement.position.z);
}

}}
