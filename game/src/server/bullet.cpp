#include "server/bullet.h"
#include "server/world.h"

////////////////////////////////////////////////////////////////////////////////
// game server
////////////////////////////////////////////////////////////////////////////////

namespace game { namespace server {

/*******************************************************************************
** Bullet
*******************************************************************************/

Bullet::Bullet(World & _world, Ref<game_data::Bullet> _bullet_data,
               UniqueId _unique_id)
    : bullet_data(move(_bullet_data))
    , unique_id(_unique_id)
    , world(_world)
    , direction(0.f, 0.f, 0.f)
    , time(0.f)
    , should_be_destroyed(false) {}

void Bullet::SetPlayer(WeakRef<Player> _player) { player = _player; }

numerics::Vector3f Bullet::GetPosition() const {
  return movement_state.position;
}

void Bullet::SetPosition(numerics::Vector3f _position) {
  movement_state.position = _position;
}

numerics::Vector3f Bullet::GetDirection() const { return direction; }

void Bullet::SetDirection(numerics::Vector3f _direction) {
  direction = _direction;

  numerics::Vector3f forward = numerics::Vector3f(0.f, 0.f, 1.f);
  numerics::Quaternionf rotation = numerics::to_rotation(direction);
  forward = numerics::rotate(forward, rotation);
  SetVelocity(forward * bullet_data->velocity);
}

numerics::Vector3f Bullet::GetVelocity() const {
  return movement_state.velocity;
}

void Bullet::SetVelocity(numerics::Vector3f _velocity) {
  movement_state.velocity = _velocity;
}

bool Bullet::ShouldBeDestroyed() const { return should_be_destroyed; }

void Bullet::Tick(float _delta_seconds) {
  MovementState new_movement = movement_state;
  new_movement.Tick(_delta_seconds);
  {
    for (Entity & entity : world->GetEntities()) {
      if (player && player->GetEntity() &&
          player->GetEntity()->unique_id == entity.unique_id) {
        continue;
      }

      float distance = numerics::point_line_distance(
          entity.GetPosition() + numerics::Vector3f(0.f, 100.f, 0.f),
          movement_state.position, new_movement.position);
      if (distance < 125.f) {
        world->DestroyEntity(entity);
        should_be_destroyed = true;
        break;
      }
    }
  }
  movement_state = new_movement;

  // TODO: Collision
  if (movement_state.position.y <=
      world->GetMapData().grid.height_at(movement_state.position)) {
    should_be_destroyed = true;
  }

  time += _delta_seconds;
  float distance = time * bullet_data->velocity;
  if (bullet_data->range < distance) {
    should_be_destroyed = true;
  }
}

}}
