#include "server/player.h"
#include "server/server.h"
#include "server/world.h"

#define LOG_SERVER_RPC CORE_LIB_LOG(VERBOSE, "\n")

////////////////////////////////////////////////////////////////////////////////
// game server
////////////////////////////////////////////////////////////////////////////////

namespace game { namespace server {

/*******************************************************************************
** Player
*******************************************************************************/

Player::Player(World & _world, net::tcp::Connection _connection)
    : world(_world)
    , shoot_timer(0.f)
    , respawn_timer(0.f)
    , connection(move(_connection))
    , shutdown(false) {
  receive_buffer = core::make_unique_array<byte>(8192u);
  send_buffer = core::make_unique_array<byte>(1024u);
  send_writer = core::BigEndianByteStreamWriter(core::make_span(send_buffer));
}

void Player::Tick(float _delta_seconds) {}

void Player::Receive() {
  if (!connection.IsOpen()) {
    Shutdown();
    return;
  }

  core::span<byte const> received =
      connection.Receive(core::make_span(receive_buffer));
  if (!received) {
    return;
  }

  core::BigEndianByteStreamReader reader(received);
  while (reader.Available() != 0u) {
    uint8 header;
    if (!reader.Read(header)) {
      LOG_SERVER(ERROR, "Failed to read header!\n");
      Shutdown();
      return;
    }

    CORE_LIB_LOG(VERBOSE, "Header: %" PRIu8 " (Available: %zu)..\n", header,
                 reader.Available());

    switch (header) {
      case U8(0): {
      } break;

      case U8(1): {
        ReceiveServerUpdate(reader);
      } break;

      default: {
        LOG_SERVER(ERROR, "Unknown header!\n");
        Shutdown();
        return;
      } break;
    }
  }
}

void Player::Send() {
  if (!connection.IsOpen()) {
    Shutdown();
    return;
  }

  if (!send_writer.Write(U8(0))) {
    LOG_SERVER(VERBOSE, "Failed to close payload!\n");
    return;
  }

  if (!connection.Send(send_writer.WrittenBytes())) {
    LOG_SERVER(ERROR, "Failed to send payload!\n");
    Shutdown();
    return;
  }
  send_writer.Reset();
}

bool Player::IsShutdown() const { return shutdown; }

void Player::Shutdown() {
  if (!shutdown) {
    shutdown = true;
    LOG_SERVER(LOG, "Shutting down..\n");

    if (connection.IsOpen()) {
      connection.Close();
    }
  }
}

void Player::SetEntityData(
    core::Optional<Ref<game_data::Entity>> _entity_data_ref) {
  entity_data = move(_entity_data_ref);
}

core::Optional<Ref<game_data::Entity>> Player::GetEntityData() const {
  return entity_data;
}

void Player::SetWeaponData(
    core::Optional<Ref<game_data::Weapon>> _weapon_data_ref) {
  weapon_data = _weapon_data_ref;
  if (entity) {
    entity->weapon_data = weapon_data;
  }
}

core::Optional<Ref<game_data::Weapon>> Player::GetWeaponData() const {
  return weapon_data;
}

void Player::SetEntity(WeakRef<Entity> _entity) {
  entity = move(_entity);
  if (entity) {
    entity->weapon_data = weapon_data;
    SendChangeEntity(entity->unique_id);

    movement_state.Reset();
    movement_state.position = entity->GetPosition();
    movement_state.friction = entity->entity_data->friction;
    movement_state.max_speed = entity->entity_data->max_speed;
  }
}

WeakRef<Entity> Player::GetEntity() const { return entity; }

void Player::SetRespawnTimer(float _value) { respawn_timer = _value; }

float Player::GetRespawnTimer() const { return respawn_timer; }

void Player::SendCreateEntity(Entity & _entity) {
  LOG_SERVER_RPC;

  if (!send_writer.Write(U8(1))) {
    Shutdown();
    return;
  }

  if (!send_writer.Write(_entity.unique_id.Get())) {
    Shutdown();
    return;
  }

  if (!send_writer.Write(_entity.entity_data->unique_id.Get())) {
    Shutdown();
    return;
  }

  numerics::Vector3f position = _entity.GetPosition();
  if (!send_writer.Write(position[0u])) {
    Shutdown();
    return;
  }
  if (!send_writer.Write(position[1u])) {
    Shutdown();
    return;
  }
  if (!send_writer.Write(position[2u])) {
    Shutdown();
    return;
  }

  numerics::Vector3f direction = _entity.GetDirection();
  // if ( !send_writer.Write( position[ 0u ] ) ) { Shutdown(); return; }
  if (!send_writer.Write(position[1u])) {
    Shutdown();
    return;
  }
  // if ( !send_writer.Write( position[ 2u ] ) ) { Shutdown(); return; }
}

void Player::SendUpdateEntity(Entity & _entity) {
  LOG_SERVER_RPC;

  if (!send_writer.Write(U8(2))) {
    Shutdown();
    return;
  }

  if (!send_writer.Write(_entity.unique_id.Get())) {
    Shutdown();
    return;
  }

  UniqueId weapon_data_unique_id =
      _entity.weapon_data ? (*_entity.weapon_data)->unique_id : UniqueId();
  if (!send_writer.Write(weapon_data_unique_id.Get())) {
    Shutdown();
    return;
  }

  uint8 flags = 0;
  if (_entity.IsAttacking()) {
    flags |= (1 << 0);
  }
  if (!send_writer.Write(flags)) {
    Shutdown();
    return;
  }

  numerics::Vector3f position = _entity.GetPosition();
  if (!send_writer.Write(position[0u])) {
    Shutdown();
    return;
  }
  if (!send_writer.Write(position[1u])) {
    Shutdown();
    return;
  }
  if (!send_writer.Write(position[2u])) {
    Shutdown();
    return;
  }

  numerics::Vector3f direction = _entity.GetDirection();
  // if ( !send_writer.Write( direction[ 0u ] ) ) { Shutdown(); return; }
  if (!send_writer.Write(direction[1u])) {
    Shutdown();
    return;
  }
  // if ( !send_writer.Write( direction[ 2u ] ) ) { Shutdown(); return; }
}

void Player::SendDestroyEntity(Entity & _entity) {
  LOG_SERVER_RPC;

  if (!send_writer.Write(U8(3))) {
    Shutdown();
    return;
  }

  if (!send_writer.Write(_entity.unique_id.Get())) {
    Shutdown();
    return;
  }
}

void Player::SendCreateBullet(Bullet & _bullet) {
  LOG_SERVER_RPC;

  if (!send_writer.Write(U8(4))) {
    Shutdown();
    return;
  }

  if (!send_writer.Write(_bullet.unique_id.Get())) {
    Shutdown();
    return;
  }

  if (!send_writer.Write(_bullet.bullet_data->unique_id.Get())) {
    Shutdown();
    return;
  }

  numerics::Vector3f position = _bullet.GetPosition();
  if (!send_writer.Write(position[0u])) {
    Shutdown();
    return;
  }
  if (!send_writer.Write(position[1u])) {
    Shutdown();
    return;
  }
  if (!send_writer.Write(position[2u])) {
    Shutdown();
    return;
  }

  numerics::Vector3f direction = _bullet.GetDirection();
  if (!send_writer.Write(direction[0u])) {
    Shutdown();
    return;
  }
  if (!send_writer.Write(direction[1u])) {
    Shutdown();
    return;
  }
  if (!send_writer.Write(direction[2u])) {
    Shutdown();
    return;
  }
}

void Player::SendDestroyBullet(Bullet & _bullet) {
  LOG_SERVER_RPC;

  if (!send_writer.Write(U8(5))) {
    Shutdown();
    return;
  }

  if (!send_writer.Write(_bullet.unique_id.Get())) {
    Shutdown();
    return;
  }
}

void Player::ReceiveServerUpdate(core::BigEndianByteStreamReader & _reader) {
  LOG_SERVER_RPC;

  MovementState client_state;
  if (!_reader.Read(client_state.delta_seconds)) {
    Shutdown();
    return;
  }

  if (!_reader.Read(client_state.acceleration[0u])) {
    Shutdown();
    return;
  }
  if (!_reader.Read(client_state.acceleration[1u])) {
    Shutdown();
    return;
  }
  if (!_reader.Read(client_state.acceleration[2u])) {
    Shutdown();
    return;
  }

  if (!_reader.Read(client_state.velocity[0u])) {
    Shutdown();
    return;
  }
  if (!_reader.Read(client_state.velocity[1u])) {
    Shutdown();
    return;
  }
  if (!_reader.Read(client_state.velocity[2u])) {
    Shutdown();
    return;
  }

  if (!_reader.Read(client_state.position[0u])) {
    Shutdown();
    return;
  }
  if (!_reader.Read(client_state.position[1u])) {
    Shutdown();
    return;
  }
  if (!_reader.Read(client_state.position[2u])) {
    Shutdown();
    return;
  }

  numerics::Vector3f direction(0.f, 0.f, 0.f);
  if (!_reader.Read(direction[0u])) {
    Shutdown();
    return;
  }
  if (!_reader.Read(direction[1u])) {
    Shutdown();
    return;
  }
  // if ( !_reader.Read( direction[ 2u ] ) ) { Shutdown(); return; }

  uint8 flags;
  if (!_reader.Read(flags)) {
    Shutdown();
    return;
  }

  ServerUpdate(client_state, direction, flags);
}

void Player::ServerUpdate(MovementState const & _client_state,
                          numerics::Vector3f _direction, uint8 _flags) {
  if (!entity.IsBound()) {
    return;
  }
  MovementState new_movement = movement_state;
  new_movement.acceleration = _client_state.acceleration;
  new_movement.Tick(_client_state.delta_seconds);
  // TODO: Collision
  new_movement.position.y =
      world->GetMapData().grid.height_at(new_movement.position);
  movement_state = new_movement;

  entity->SetPosition(movement_state.position);
  entity->SetDirection(_direction);

  LOG_SERVER(VERBOSE, "Position: (%.4f, %.4f, %.4f)\n",
             new_movement.position[0u], new_movement.position[1u],
             new_movement.position[2u]);

  shoot_timer += _client_state.delta_seconds;
  if ((_flags & (1 << 0)) != 0) {
    if (0.2f < shoot_timer) {
      shoot_timer = 0.f;

      // TODO: EntityType!
      Ref<game_data::Bullet> bullet_data_ref =
          world->server->application->game_data.bullets[0u];
      Ref<Bullet> bullet =
          world->CreateBullet(move(bullet_data_ref), entity->GetWorldShotpos(),
                              entity->GetDirection());
      bullet->SetPlayer(this);
    }
  }

  entity->Tick(_client_state.delta_seconds);

  {
    float distance =
        numerics::length(_client_state.position - movement_state.position);
    if (distance > 3.f) {
      SendClientAdjust(movement_state);
      return;
    }

    float speed_difference = abs(numerics::length(_client_state.velocity) -
                                 numerics::length(movement_state.velocity));
    if (speed_difference > 3.f) {
      SendClientAdjust(movement_state);
      return;
    }
  }
}

void Player::SendClientAdjust(MovementState _movement_state) {
  LOG_SERVER_RPC;

  if (!send_writer.Write(U8(20))) {
    Shutdown();
    return;
  }

  if (!send_writer.Write(_movement_state.acceleration[0u])) {
    Shutdown();
    return;
  }
  if (!send_writer.Write(_movement_state.acceleration[1u])) {
    Shutdown();
    return;
  }
  if (!send_writer.Write(_movement_state.acceleration[2u])) {
    Shutdown();
    return;
  }

  if (!send_writer.Write(_movement_state.velocity[0u])) {
    Shutdown();
    return;
  }
  if (!send_writer.Write(_movement_state.velocity[1u])) {
    Shutdown();
    return;
  }
  if (!send_writer.Write(_movement_state.velocity[2u])) {
    Shutdown();
    return;
  }

  if (!send_writer.Write(_movement_state.position[0u])) {
    Shutdown();
    return;
  }
  if (!send_writer.Write(_movement_state.position[1u])) {
    Shutdown();
    return;
  }
  if (!send_writer.Write(_movement_state.position[2u])) {
    Shutdown();
    return;
  }
}

void Player::SendChangeEntity(UniqueId _entity_unique_id) {
  LOG_SERVER_RPC;

  if (!send_writer.Write(U8(10))) {
    Shutdown();
    return;
  }

  if (!send_writer.Write(_entity_unique_id.Get())) {
    Shutdown();
    return;
  }
}

}}
