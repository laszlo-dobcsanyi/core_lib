#include "server/world.h"
#include "server/server.h"
#include "server/player.h"

////////////////////////////////////////////////////////////////////////////////
// game server
////////////////////////////////////////////////////////////////////////////////

namespace game { namespace server {

/*******************************************************************************
** World
*******************************************************************************/

World::World(Server & _server)
    : server(_server) {
  if (core::Optional<const_string> game_mode_result =
          _server.application->command_line.Get("-gamemode")) {
    if (*game_mode_result == "deathmatch") {
      survival = false;
    }
  }
}

bool World::Initialize(MapData _map_data) {
  map_data = move(_map_data);
  return true;
}

void World::Tick(float _delta_seconds) {
  for (Player & player : players) {
    player.Receive();
  }

  {
    for (Player & player : players) {
      player.Tick(_delta_seconds);
    }

    for (Monster & monster : monsters) {
      monster.Tick(_delta_seconds);
    }

    for (Bullet & bullet : bullets) {
      bullet.Tick(_delta_seconds);
    }

    for (Entity & entity : entities) {
      entity.Tick(_delta_seconds);
    }
  }

  for (Entity & entity : entities) {
    for (Player & player : players) {
      player.SendUpdateEntity(entity);
    }
  }

  for (Player & player : players) {
    player.Send();
  }

  for (Player & player : players) {
    if (player.IsShutdown()) {
      if (WeakRef<Entity> entity = player.GetEntity()) {
        DestroyEntity(*entity.GetObject());
      }
      players.Destroy(player);
    }
  }

  for (Bullet & bullet : bullets) {
    if (bullet.ShouldBeDestroyed()) {
      DestroyBullet(bullet);
    }
  }

  if (survival)
    TickSurvival(_delta_seconds);
  else
    TickDeathmatch(_delta_seconds);
}

core::Optional<Ref<Player>>
World::CreatePlayer(net::tcp::Connection _connection) {
  uint32 entity_data_index = players.Size() % 2;
  core::Optional<Ref<game_data::Entity>> entity_data_ref =
      server->application->game_data.GetEntityData(entity_data_index);
  if (!entity_data_ref) {
    LOG_SERVER(ERROR, "Entity data not found!\n");
    return core::nullopt;
  }
  core::Optional<Ref<game_data::Weapon>> weapon_data_ref =
      server->application->game_data.GetWeaponData(0u);
  if (!weapon_data_ref) {
    LOG_SERVER(ERROR, "Weapon data not found!\n");
    return core::nullopt;
  }

  LOG_SERVER(LOG, "Creating player..\n");
  Player & player = players.Create(*this, move(_connection));
  player.SetEntityData(*entity_data_ref);
  player.SetWeaponData(*weapon_data_ref);

  // Send world data
  {
    for (Entity & entity : entities) {
      player.SendCreateEntity(entity);
    }
    for (Bullet & bullet : bullets) {
      player.SendCreateBullet(bullet);
    }
  }

  // Create Entity
  {
    numerics::Vector3f position(0.f, 0.f, 0.f);
    numerics::Vector3f direction(0.f, 0.f, 0.f);

    Ref<Entity> spawned_entity =
        CreateEntity(*player.GetEntityData(), position, direction);
    spawned_entity->weapon_data = weapon_data_ref;
    player.SetEntity(spawned_entity);
  }

  return player;
}

Ref<Entity> World::CreateEntity(Ref<game_data::Entity> _entity_data_ref,
                                numerics::Vector3f _position,
                                numerics::Vector3f _direction) {
  UniqueId unique_id = entity_unique_id_generator.Next();

  LOG_SERVER(LOG, "Creating entity #%" PRIu32 " '%s'..\n", unique_id.Get(),
             cstr(_entity_data_ref->name));
  Entity & entity = entities.Create(*this, move(_entity_data_ref), unique_id);
  entity.SetPosition(_position);
  entity.SetDirection(_direction);

  // Send
  {
    for (Player & player : players) {
      player.SendCreateEntity(entity);
    }
  }
  return entity;
}

void World::DestroyEntity(Entity & _entity) {
  for (Player & player : players) {
    player.SendDestroyEntity(_entity);
  }
  entities.Destroy(_entity);
}

Ref<Bullet> World::CreateBullet(Ref<game_data::Bullet> _bullet_data_ref,
                                numerics::Vector3f _position,
                                numerics::Vector3f _direction) {
  UniqueId unique_id = bullet_unique_id_generator.Next();

  LOG_SERVER(LOG, "Creating bullet #%" PRIu32 " '%s'..\n", unique_id.Get(),
             cstr(_bullet_data_ref->name));
  Bullet & bullet = bullets.Create(*this, move(_bullet_data_ref), unique_id);
  bullet.SetPosition(_position);
  bullet.SetDirection(_direction);

  // Send
  {
    for (Player & player : players) {
      player.SendCreateBullet(bullet);
    }
  }

  return bullet;
}

void World::DestroyBullet(Bullet & _bullet) {
  for (Player & player : players) {
    player.SendDestroyBullet(_bullet);
  }
  bullets.Destroy(_bullet);
}

void World::SpawnMonsterEntity(Monster & _monster,
                               numerics::Vector3f _position) {
  // TODO: Collision
  _position.y = map_data.grid.height_at(_position);

  numerics::Vector3f direction(0.f, 0.f, 0.f);

  uint32 entity_data_index = (rng.Next() % 4) + 2;
  Ref<game_data::Entity> entity_data_ref =
      server->application->game_data.entities[entity_data_index];

  Ref<Entity> spawned_entity =
      CreateEntity(entity_data_ref, _position, direction);
  _monster.SetEntity(spawned_entity);
}

void World::SpawnPlayerEntity(Player & _player, numerics::Vector3f _position) {
  numerics::Vector3f direction(0.f, 0.f, 0.f);

  Ref<Entity> spawned_entity =
      CreateEntity(*_player.GetEntityData(), _position, direction);
  _player.SetEntity(spawned_entity);
}

void World::TickSurvival(float _delta_seconds) {
  if (players.Size() == 0u) {
    return;
  }

  if (0.f < survival_restart_timer) {
    survival_restart_timer -= _delta_seconds;

    if (survival_restart_timer <= 0.f) {
      survival_next_wave_timer = 0.f;
      survival_spawn_index = 0u;
      survival_spawn_timer = 0.f;

      monsters.Clear();
      for (Entity & entity : entities) {
        DestroyEntity(entity);
      }
      for (Bullet & bullet : bullets) {
        DestroyBullet(bullet);
      }

      for (Player & player : players) {
        if (!player.GetEntity()) {
          numerics::Vector3f position = player.GetPosition();
          SpawnPlayerEntity(player, position);
        }
      }
    }
    return;
  }

  size_t player_character_count = 0u;
  for (Player const & player : players) {
    if (player.GetEntity()) {
      ++player_character_count;
    }
  }

  if (player_character_count == 0u) {
    survival_restart_timer = 5.f;
    return;
  }

  if (survival_spawn_index != monsters.Size()) {
    survival_spawn_timer += _delta_seconds;
    if (0.5f < survival_spawn_timer) {
      survival_spawn_timer = 0.f;

      numerics::Vector3f position(0.f, 0.f, 0.f);

      size_t index = 0u;
      for (Player const & player : players) {
        if (WeakRef<Entity> controlled_entity = player.GetEntity()) {
          if (index == survival_spawn_index % player_character_count) {
            position = controlled_entity->GetPosition();
            break;
          }
          ++index;
        }
      }

      float angle = numerics::to_radians(static_cast<float>(rng.Next()));
      position +=
          numerics::Vector3f(numerics::cos(angle), 0.f, numerics::sin(angle)) *
          500.f;

      SpawnMonsterEntity(monsters[survival_spawn_index], position);

      ++survival_spawn_index;
    }
  }

  size_t monsters_alive = 0u;
  for (Monster & monster : monsters) {
    if (monster.GetEntity()) {
      ++monsters_alive;
    }
  }

  if (monsters_alive == 0) {
    survival_next_wave_timer += _delta_seconds;
    if (3.f < survival_next_wave_timer) {
      survival_next_wave_timer = 0.f;
      survival_spawn_index = 0u;

      for (Player & player : players) {
        if (!player.GetEntity()) {
          numerics::Vector3f position = player.GetPosition();
          SpawnPlayerEntity(player, position);
        }
      }

      for (auto current = 0u; current < players.Size(); ++current) {
        Monster & monster = monsters.Create(*this);
        monster.SetApproachAngle(
            numerics::to_radians(static_cast<float>(rng.Next())));
      }
    }
  }
}

void World::TickDeathmatch(float _delta_seconds) {
  // Respawn Players
  for (Player & player : players) {
    if (!player.GetEntity()) {
      player.SetRespawnTimer(player.GetRespawnTimer() + _delta_seconds);
      if (2.5f < player.GetRespawnTimer()) {
        player.SetRespawnTimer(0.f);

        float angle = numerics::to_radians(static_cast<float>(rng.Next()));
        numerics::Vector3f position =
            player.GetPosition() + numerics::Vector3f(numerics::cos(angle), 0.f,
                                                      numerics::sin(angle)) *
                                       2500.f;
        SpawnPlayerEntity(player, position);
      }
    }
  }
}

}}
