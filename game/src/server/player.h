#ifndef _GAME_SERVER_PLAYER_H_
#define _GAME_SERVER_PLAYER_H_

#include "server/server.hh"
#include "server/entity.h"
#include "core/byte_stream.hpp"
#include "net/tcp/connection.h"

////////////////////////////////////////////////////////////////////////////////
// game server
////////////////////////////////////////////////////////////////////////////////

namespace game { namespace server {

/*******************************************************************************
** Player
*******************************************************************************/

class Player : public ObjectBase<Player> {
public:
  Player() = delete;
  Player(World & _world, net::tcp::Connection _connection);
  Player(Player &&) = delete;
  Player(Player const &) = delete;
  Player & operator=(Player &&) = delete;
  Player & operator=(Player const &) = delete;
  ~Player() = default;

  void Tick(float _delta_seconds);

  void Receive();
  void Send();

  bool IsShutdown() const;
  void Shutdown();

  void SetEntityData(core::Optional<Ref<game_data::Entity>> _entity_data_ref);
  core::Optional<Ref<game_data::Entity>> GetEntityData() const;

  void SetWeaponData(core::Optional<Ref<game_data::Weapon>> _weapon_data_ref);
  core::Optional<Ref<game_data::Weapon>> GetWeaponData() const;

  void SetEntity(WeakRef<Entity> _entity);
  WeakRef<Entity> GetEntity() const;

  void SetRespawnTimer(float _value);
  float GetRespawnTimer() const;

  void SendCreateEntity(Entity & _entity);
  void SendUpdateEntity(Entity & _entity);
  void SendDestroyEntity(Entity & _entity);

  void SendCreateBullet(Bullet & _bullet);
  void SendDestroyBullet(Bullet & _bullet);

  numerics::Vector3f GetPosition() { return movement_state.position; }

private:
  Ref<World> world;
  core::Optional<Ref<game_data::Entity>> entity_data;
  core::Optional<Ref<game_data::Weapon>> weapon_data;
  WeakRef<Entity> entity;
  MovementState movement_state;
  float shoot_timer;
  float respawn_timer;

  net::tcp::Connection connection;
  core::UniqueArray<byte> receive_buffer;
  core::UniqueArray<byte> send_buffer;
  core::BigEndianByteStreamWriter send_writer;
  bool shutdown;

  void ReceiveServerUpdate(core::BigEndianByteStreamReader & _reader);
  void ServerUpdate(MovementState const & _client_state,
                    numerics::Vector3f _direction, uint8 _flags);

  void SendClientAdjust(MovementState _movement_state);
  void SendChangeEntity(UniqueId _entity_unique_id);
};

}}

#endif
