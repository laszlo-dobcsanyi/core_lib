#include "server/server.h"

#include "net/ip/api.h"
#include "net/ip/endpoint.h"

// TODO: graphics dependecy
#include "graphics/resource/map_data_from_file.hpp"

////////////////////////////////////////////////////////////////////////////////
// game server
////////////////////////////////////////////////////////////////////////////////

namespace game { namespace server {

/*******************************************************************************
** Server
*******************************************************************************/

Server::Server(GameApplication & _application)
    : application(_application)
    , world(*this)
    , session(*this)
    , shutdown_requested(false)
    , shutdown(false) {}

void Server::Launch(ServerLaunchParameters const & _launch_parameters) {
  LOG_SERVER(STAGE, "Launch..\n");

  core::fs::Directory data_directory = game::default_data_directory();
  if (!data_directory.Exists()) {
    LOG_SERVER(ERROR, "Data directory '%s' does not exists!\n",
               cstr(data_directory));
    return;
  }

  // TODO: MapType!
  Ref<game_data::Map> map = application->game_data.maps[0u];
  // TODO: graphics dependecy
  graphics::resource::MapData map_data;
  {
    auto map_directory = data_directory / "maps" / map->name;
    auto map_data_path = map_directory / "map.data";
    if (auto map_data_result =
            graphics::resource::map_data_from_file(map_data_path)) {
      map_data = move(*map_data_result);
    } else {
      LOG_SERVER(ERROR, "Failed to load map data '%s'!\n", cstr(map_data_path));
      RequestShutdown();
      return;
    }
  }

  if (!world.Initialize(move(map_data))) {
    LOG_SERVER(ERROR, "Failed to initialize world!\n");
    RequestShutdown();
    return;
  }

  if (!net::ip::Initialize()) {
    // TODO: Terminate
    LOG_SERVER(ERROR, "Failed to initialize net!\n");
    RequestShutdown();
    return;
  }

  if (!session.Create(_launch_parameters.endpoint, 4u)) {
    // TODO: Terminate
    LOG_SERVER(ERROR, "Failed to create session!\n");
    RequestShutdown();
    return;
  }
}

void Server::Tick(float _delta_seconds) {
  LOG_SERVER(VERBOSE, "Tick %.6f..\n", _delta_seconds);

  session.Accept();

  world.Tick(_delta_seconds);
}

void Server::Shutdown() {
  LOG_SERVER(STAGE, "Shutting down..\n");

  CORE_LIB_ASSERT(!shutdown);
  shutdown = true;

  session.Shutdown();

  net::ip::Finalize();
}

void Server::RequestShutdown() {
  LOG_SERVER(LOG, "Shutdown requested!\n");
  shutdown_requested = true;
}

bool Server::IsShutdownRequested() const { return shutdown_requested; }

bool Server::IsShutdown() const { return shutdown; }

}}
