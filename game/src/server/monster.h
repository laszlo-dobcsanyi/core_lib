#ifndef _GAME_SERVER_MONSTER_H_
#define _GAME_SERVER_MONSTER_H_

#include "server/server.hh"
#include "server/entity.h"

////////////////////////////////////////////////////////////////////////////////
// game server
////////////////////////////////////////////////////////////////////////////////

namespace game { namespace server {

/*******************************************************************************
** Monster
*******************************************************************************/

class Monster : public ObjectBase<Monster> {
public:
  Ref<World> world;

  Monster() = delete;
  Monster(World & _world);
  Monster(Monster &&) = delete;
  Monster(Monster const &) = delete;
  Monster & operator=(Monster &&) = delete;
  Monster & operator=(Monster const &) = delete;
  ~Monster() = default;

  void SetEntity(WeakRef<Entity> _entity);
  WeakRef<Entity> GetEntity() const;

  void SetApproachAngle(float _approach_angle);

  void Tick(float _delta_seconds);

private:
  WeakRef<Entity> entity;
  WeakRef<Entity> target;
  MovementState movement_state;

  float approach_angle;
  float idle_timer;
  float targeting_timer;
  float attack_timer;
  float attack_hit_timer;
  float attack_delay_timer;
};

}}

#endif
