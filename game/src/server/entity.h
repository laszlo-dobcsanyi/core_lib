#ifndef _GAME_SERVER_ENTITY_H_
#define _GAME_SERVER_ENTITY_H_

#include "server/server.hh"

////////////////////////////////////////////////////////////////////////////////
// game server
////////////////////////////////////////////////////////////////////////////////

namespace game { namespace server {

/*******************************************************************************
** Entity
*******************************************************************************/

class Entity : public ObjectBase<Entity> {
public:
  Ref<game_data::Entity> entity_data;
  core::Optional<Ref<game_data::Weapon>> weapon_data;
  UniqueId unique_id;

  Entity() = delete;
  Entity(World & _world, Ref<game_data::Entity> _entity_data,
         UniqueId _unique_id);
  Entity(Entity &&) = default;
  Entity(Entity const &) = delete;
  Entity & operator=(Entity &&) = default;
  Entity & operator=(Entity const &) = delete;
  ~Entity() = default;

  bool IsAttacking() const;
  void SetAttacking(bool _attacking);

  numerics::Vector3f GetPosition() const;
  void SetPosition(numerics::Vector3f _position);

  numerics::Vector3f GetDirection() const;
  void SetDirection(numerics::Vector3f _direction);

  numerics::Vector3f GetForward() const;
  numerics::Vector3f GetWorldShotpos() const;

  void Tick(float _delta_seconds);

private:
  Ref<World> world;
  bool attacking = false;
  numerics::Vector3f position;
  numerics::Vector3f direction;
};

}}

#endif
