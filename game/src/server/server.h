#ifndef _GAME_SERVER_SERVER_H_
#define _GAME_SERVER_SERVER_H_

#include "game_application.h"
#include "server/server.hh"
#include "server/session.h"
#include "server/world.h"

////////////////////////////////////////////////////////////////////////////////
// game server
////////////////////////////////////////////////////////////////////////////////

namespace game { namespace server {

/*******************************************************************************
** ServerLaunchParameters
*******************************************************************************/

struct ServerLaunchParameters {
  net::ip::Endpoint endpoint;
};

/*******************************************************************************
** Server
*******************************************************************************/

class Server : public ObjectBase<Server> {
public:
  using LaunchParameters = ServerLaunchParameters;

  Ref<GameApplication> application;
  World world;
  Session session;

  Server() = delete;
  Server(GameApplication & _application);
  Server(Server &&) = default;
  Server(Server const &) = delete;
  Server & operator=(Server &&) = default;
  Server & operator=(Server const &) = delete;
  ~Server() = default;

  void Launch(ServerLaunchParameters const & _launch_parameters);
  void Tick(float _delta_seconds);
  void Shutdown();

  void RequestShutdown();
  bool IsShutdownRequested() const;
  bool IsShutdown() const;

private:
  uint8 shutdown_requested : 1;
  uint8 shutdown : 1;
};

}}

#endif
