#ifndef _GAME_GAME_APPLICATION_H_
#define _GAME_GAME_APPLICATION_H_

#include "core/base.h"
#include "core/application.hpp"
#include "game.hh"
#include "shared/game_data.h"

////////////////////////////////////////////////////////////////////////////////
// game
////////////////////////////////////////////////////////////////////////////////

namespace game {

/*******************************************************************************
** GameApplication
*******************************************************************************/

class GameApplication : public ObjectBase<GameApplication> {
public:
  application::CommandLine command_line;
  GameData game_data;

  GameApplication() = delete;
  GameApplication(application::CommandLine _command_line);
  GameApplication(GameApplication &&) = default;
  GameApplication(GameApplication const &) = delete;
  GameApplication & operator=(GameApplication &&) = default;
  GameApplication & operator=(GameApplication const &) = delete;
  ~GameApplication() = default;

  void Run();
};

}

#endif
