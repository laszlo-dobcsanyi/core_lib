#include "game_application.h"

#include "core/containers.hpp"

#include "process.hpp"
#include "server/server.h"
#include "client/client.h"

////////////////////////////////////////////////////////////////////////////////
// game
////////////////////////////////////////////////////////////////////////////////

namespace game {

/*******************************************************************************
** GameApplication
*******************************************************************************/

GameApplication::GameApplication(application::CommandLine _command_line)
    : command_line(move(_command_line)) {}

void GameApplication::Run() {
  LOG_GAME(STAGE, "Loading game data..\n");
  if (auto game_data_result = load_game_data()) {
    game_data = move(*game_data_result);
  } else {
    LOG_GAME(ERROR, "Failed to load game data!\n");
    return;
  }

  net::ip::Endpoint endpoint = net::ip::local_endpoint(1425);
  if (core::Optional<const_string> endpoint_parameter =
          command_line.Get("--endpoint")) {
    if (core::Optional<net::ip::Endpoint> endpoint_result =
            net::ip::from_string(*endpoint_parameter)) {
      endpoint = *endpoint_result;
    } else {
      LOG_GAME(ERROR, "Invalid endpoint parameter '%s'!\n",
               cstr(*endpoint_parameter));
    }
  }

  size_t processor_index = 0u;

  Process<server::Server> server_process(*this);
  if (command_line.Has("--server")) {
    LOG_GAME(STAGE, "Launching server..\n");
    server::ServerLaunchParameters server_parameters;
    server_parameters.endpoint = endpoint;
    server_process.Launch(server_parameters,
                          parallel::ThreadAttributes(processor_index++, 0u));
  }

  if (command_line.Has("--client")) {
    LOG_GAME(STAGE, "Launching client..\n");
    Process<client::Client> client_process(*this);
    {
      client::ClientLaunchParameters client_parameters;
      client_parameters.endpoint = endpoint;
      client_process.Launch(client_parameters,
                            parallel::ThreadAttributes(processor_index++, 0u));
    }

    LOG_GAME(STAGE, "Joining client..\n");
    client_process.Join();

    LOG_GAME(STAGE, "Terminating server..\n");
    server_process.Terminate();
  }

  LOG_GAME(STAGE, "Joining server..\n");
  server_process.Join();
}

}

////////////////////////////////////////////////////////////////////////////////
//                                                                                                //
////////////////////////////////////////////////////////////////////////////////

#if defined(CORE_LIB_OS_WINDOWS)
void game_main(char * _command_line) {
  game::GameApplication application((application::CommandLine(_command_line)));
  application.Run();
}

int32 WINAPI WinMain(HINSTANCE _instance, HINSTANCE _previous_instance,
                     char * _command_line, int32 _show_cmd) {
#else
void game_main(char ** _command_line) {
  game::GameApplication application((application::CommandLine(_command_line)));
  application.Run();
}

int main(int, char ** _command_line) {
#endif
  application::program_main(core::wrap_callable(game_main), _command_line);
  return 0;
}
