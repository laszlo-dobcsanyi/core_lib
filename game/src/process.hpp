#ifndef _GAME_PROCESS_HPP_
#define _GAME_PROCESS_HPP_

#include "core/parallel/thread.hpp"
#include "core/time.hpp"
#include "core/memory/unique_ptr.hpp"

#include "game.hh"

////////////////////////////////////////////////////////////////////////////////
// game
////////////////////////////////////////////////////////////////////////////////

namespace game {

/*******************************************************************************
** Process
*******************************************************************************/

template<class Type>
class Process final {
public:
  using LaunchParameters = typename Type::LaunchParameters;

  Process() = delete;
  Process(GameApplication & _application);
  Process(Process &&) = delete;
  Process(Process const &) = delete;
  Process & operator=(Process &&) = delete;
  Process & operator=(Process const &) = delete;
  ~Process() = default;

  void Launch(LaunchParameters const & _launch_parameters);
  void Launch(LaunchParameters const & _launch_parameters,
              parallel::ThreadAttributes _thread_attributes);
  void Terminate();
  void Join();

private:
  GameApplication & application;
  Atomic<bool> terminated;
  core::UniquePtr<Type> object;
  parallel::Thread thread;

  void Start(LaunchParameters _launch_parameters);
  void Run();
};

}

////////////////////////////////////////////////////////////////////////////////
// game
////////////////////////////////////////////////////////////////////////////////

namespace game {

/*******************************************************************************
** Process
*******************************************************************************/

template<class Type>
inline Process<Type>::Process(GameApplication & _application)
    : application(_application)
    , terminated(false) {}

template<class Type>
inline void Process<Type>::Launch(LaunchParameters const & _launch_parameters) {
  Launch(_launch_parameters, parallel::ThreadAttributes());
}

template<class Type>
inline void
Process<Type>::Launch(LaunchParameters const & _launch_parameters,
                      parallel::ThreadAttributes _thread_attributes) {
  CORE_LIB_ASSERT(!thread.IsJoinable());
  thread.SetAttributes(_thread_attributes);
  thread.Run(core::wrap_callable(&Process<Type>::Start, *this),
             _launch_parameters);
}

template<class Type>
inline void Process<Type>::Terminate() {
  terminated = true;
}

template<class Type>
inline void Process<Type>::Join() {
  thread.Join();
}

template<class Type>
inline void Process<Type>::Start(LaunchParameters _launch_parameters) {
  CORE_LIB_ASSERT(!object);
  object = core::make_unique<Type>(application);
  object->Launch(_launch_parameters);
  Run();
}

template<class Type>
inline void Process<Type>::Run() {
  core::HighResolutionClock clock;
  for (;;) {
    if (terminated || object->IsShutdownRequested()) {
      object->Shutdown();
      object.Reset();
      return;
    }

    core::TimeSpan elapsed = clock.Reset();
    object->Tick(static_cast<float>(core::to_seconds(elapsed)));

    // TODO:
    const std::chrono::milliseconds relax_duration(5);
    std::this_thread::sleep_for(relax_duration);
  }
}

}

#endif
