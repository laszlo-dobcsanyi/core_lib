#ifndef _GAME_CLIENT_MODEL_H_
#define _GAME_CLIENT_MODEL_H_

#include "client/client.hh"

#include "graphics/animation/skeletal_animation.h"

////////////////////////////////////////////////////////////////////////////////
// game client
////////////////////////////////////////////////////////////////////////////////

namespace game { namespace client {

/*******************************************************************************
** StaticModel
*******************************************************************************/

class StaticModel : public ObjectBase<StaticModel> {
public:
  Ref<game_data::StaticModel> static_model_data_ref;
  Ref<StaticModelObject> static_model_ref;

  StaticModel(Ref<game_data::StaticModel> _static_model_data_ref,
              Ref<StaticModelObject> _static_model_ref);
  StaticModel(StaticModel &&) = default;
  StaticModel(StaticModel const &) = delete;
  StaticModel & operator=(StaticModel &&) = default;
  StaticModel & operator=(StaticModel const &) = delete;
  ~StaticModel() = default;

  numerics::Transformation GetTransformation() const { return transformation; }

  void Tick(float _delta_seconds, numerics::Transformation _transformation);

private:
  float delta_seconds;
  numerics::Transformation transformation;
};

/*******************************************************************************
** Attachment
*******************************************************************************/

class Attachment {
public:
  Ref<StaticModel> static_model;
  size_t bone_index;
  numerics::Transformation transformation;

  Attachment(Ref<StaticModel> _static_model, size_t _bone_index,
             numerics::Transformation _transformation)
      : static_model(move(_static_model))
      , bone_index(_bone_index)
      , transformation(_transformation) {}
  Attachment(Attachment &&) = default;
  Attachment(Attachment const &) = default;
  Attachment & operator=(Attachment &&) = default;
  Attachment & operator=(Attachment const &) = default;
  ~Attachment() = default;
};

/*******************************************************************************
** AnimatedModel
*******************************************************************************/

class AnimatedModel : public ObjectBase<AnimatedModel> {
public:
  Ref<game_data::AnimatedModel> animated_model_data_ref;
  Ref<AnimatedModelObject> animated_model_ref;
  graphics::animation::SkeletalAnimation skeletal_animation;
  core::Optional<Attachment> attachment;

  AnimatedModel(Ref<game_data::AnimatedModel> _animated_model_data_ref,
                Ref<AnimatedModelObject> _animated_model_ref);
  AnimatedModel(AnimatedModel &&) = default;
  AnimatedModel(AnimatedModel const &) = delete;
  AnimatedModel & operator=(AnimatedModel &&) = default;
  AnimatedModel & operator=(AnimatedModel const &) = delete;
  ~AnimatedModel() = default;

  void CreateAttachment(Ref<StaticModel> _static_model_ref,
                        const_string _bone_name,
                        numerics::Vector3f _translation,
                        numerics::Vector3f _direction);

  numerics::Transformation GetTransformation() const { return transformation; }

  void Tick(float _delta_seconds, numerics::Transformation _transformation,
            bool _attacking, numerics::Vector3f _movement,
            numerics::Vector3f _forward);

  void Animate();

protected:
  graphics::animation::AnimationState * idle = nullptr;
  graphics::animation::AnimationState * run_forward = nullptr;
  graphics::animation::AnimationState * run_backward = nullptr;
  graphics::animation::AnimationState * run_right = nullptr;
  graphics::animation::AnimationState * run_left = nullptr;
  graphics::animation::AnimationState * attack = nullptr;

  float run_speed = 0.f;

  float delta_seconds;
  bool attacking;
  numerics::Vector3f movement;
  numerics::Vector3f forward;
  numerics::Transformation transformation;

  graphics::animation::AnimationState * FindAnimationState(const_string _name);
  float Blend(float _current_weight, float _target_weight,
              float _delta_seconds);
  void Blend(float & _weight,
             graphics::animation::AnimationState & _animation_state,
             float _target_weight, float _delta_seconds);
};

}}

#endif
