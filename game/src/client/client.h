#ifndef _GAME_CLIENT_CLIENT_H_
#define _GAME_CLIENT_CLIENT_H_

#include "game_application.h"
#include "client/client.hh"
#include "client/graphics.h"
#include "client/resources.h"
#include "client/scene.h"
#include "client/session.h"
#include "client/world.h"
#include "client/player.h"

////////////////////////////////////////////////////////////////////////////////
// game client
////////////////////////////////////////////////////////////////////////////////

namespace game { namespace client {

/*******************************************************************************
** ClientLaunchParameters
*******************************************************************************/

struct ClientLaunchParameters {
  net::ip::Endpoint endpoint;
};

/*******************************************************************************
** Client
*******************************************************************************/

class Client : public ObjectBase<Client> {
public:
  using LaunchParameters = ClientLaunchParameters;

  Ref<GameApplication> application;
  Graphics graphics;
  Resources resources;
  World world;
  Scene scene;
  Session session;
  Player player;

  Client() = delete;
  Client(GameApplication & _application);
  Client(Client &&) = delete;
  Client(Client const &) = delete;
  Client & operator=(Client &&) = delete;
  Client & operator=(Client const &) = delete;
  ~Client() = default;

  void Launch(ClientLaunchParameters const & _launch_parameters);
  void Tick(float _delta_seconds);
  void Shutdown();

  void RequestShutdown();
  bool IsShutdownRequested() const;
  bool IsShutdown() const;

private:
  uint8 shutdown_requested : 1;
  uint8 shutdown : 1;

  void OnWindowDestroy(graphics::window::DestroyEventArgs const &);
};

}}

#endif
