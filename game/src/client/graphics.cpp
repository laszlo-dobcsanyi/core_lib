#include "client/graphics.h"
#include "client/client.h"

////////////////////////////////////////////////////////////////////////////////
// game client
////////////////////////////////////////////////////////////////////////////////

namespace game { namespace client {

/*******************************************************************************
** Graphics
*******************************************************************************/

Graphics::Graphics(Client & _client)
    : client(_client) {}

bool Graphics::Create() {
  if (!graphics_context.Initialize()) {
    LOG_CLIENT(ERROR, "Failed to initialize graphics context!\n");
    return false;
  }

  if (!window.Create(const_string("Client"))) {
    LOG_CLIENT(ERROR, "Failed to create window!\n");
    return false;
  }
  window.SetMouseGrab(true);

  // TODO: OpenGL
  graphics::glEnable(GL_DEPTH_TEST);
  graphics::glDepthFunc(GL_LEQUAL);

  return true;
}

void Graphics::Tick(float _delta_seconds) { window.Run(); }

void Graphics::Destroy() {
  window.Destroy();
  graphics_context.Finalize();
}

bool Graphics::ShouldTerminate() const { return window.IsDestroyed(); }

}}
