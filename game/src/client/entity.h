#ifndef _GAME_CLIENT_ENTITY_H_
#define _GAME_CLIENT_ENTITY_H_

#include "client/client.hh"
#include "client/model.h"

////////////////////////////////////////////////////////////////////////////////
// game client
////////////////////////////////////////////////////////////////////////////////

namespace game { namespace client {

/*******************************************************************************
** Entity
*******************************************************************************/

class Entity : public ObjectBase<Entity> {
public:
  Ref<game_data::Entity> entity_data;
  core::Optional<Ref<game_data::Weapon>> weapon_data;
  UniqueId unique_id;
  WeakRef<StaticModel> static_model;
  WeakRef<AnimatedModel> animated_model;

  Entity() = delete;
  Entity(World & _world, Ref<game_data::Entity> _entity_data,
         UniqueId _unique_id);
  Entity(Entity &&) = delete;
  Entity(Entity const &) = delete;
  Entity & operator=(Entity &&) = delete;
  Entity & operator=(Entity const &) = delete;
  ~Entity() = default;

  void SetWeaponData(core::Optional<Ref<game_data::Weapon>> _weapon_data);

  bool IsAttacking() const;
  void SetAttacking(bool _attacking);

  numerics::Transformation GetTransformation() const;

  numerics::Vector3f GetPosition() const;
  void SetPosition(numerics::Vector3f _position);

  numerics::Vector3f GetDirection() const;
  void SetDirection(numerics::Vector3f _direction);

  numerics::Vector3f GetForward() const;

  numerics::Vector3f GetMovement();
  void Move(numerics::Vector3f _position);

  numerics::Vector3f GetWorldShotpos() const;

  void Tick(float _delta_seconds);

private:
  Ref<World> world;
  bool attacking = false;
  numerics::Transformation transformation;
  numerics::Vector3f position;
  numerics::Vector3f direction;
  numerics::Vector3f movement;
};

}}

#endif
