#include "client/world.h"
#include "client/client.h"

////////////////////////////////////////////////////////////////////////////////
// game client
////////////////////////////////////////////////////////////////////////////////

namespace game { namespace client {

/*******************************************************************************
** World
*******************************************************************************/

World::World(Client & _client)
    : client(_client) {}

bool World::Initialize(Ref<game_data::Map> _map_data) {
  map = client->resources.GetMap(_map_data);
  return true;
}

void World::Tick(float _delta_seconds) {
  for (Entity & entity : entities) {
    entity.Tick(_delta_seconds);
  }

  for (Bullet & bullet : bullets) {
    bullet.Tick(_delta_seconds);
  }
}

Entity & World::CreateEntity(Ref<game_data::Entity> _entity_data,
                             UniqueId _unique_id, numerics::Vector3f _position,
                             numerics::Vector3f _direction) {
  LOG_CLIENT(LOG, "Creating entity #%" PRIu32 " '%s'..\n", _unique_id.Get(),
             cstr(_entity_data->name));
  Entity & entity = entities.Create(*this, _entity_data, _unique_id);
  if (_entity_data->static_model) {
    core::Optional<Ref<StaticModelObject>> static_model_ref =
        client->resources.GetStaticModel(*_entity_data->static_model);
    entity.static_model = client->scene.CreateStaticModel(
        *_entity_data->static_model, *static_model_ref);
  } else if (_entity_data->animated_model) {
    core::Optional<Ref<AnimatedModelObject>> animated_model_ref =
        client->resources.GetAnimatedModel(*_entity_data->animated_model);
    entity.animated_model = client->scene.CreateAnimatedModel(
        *_entity_data->animated_model, *animated_model_ref);
  }
  entity.SetPosition(_position);
  entity.SetDirection(_direction);
  return entity;
}

Entity * World::GetEntity(UniqueId _entity_unique_id) {
  for (Entity & entity : entities) {
    if (entity.unique_id == _entity_unique_id) {
      return &entity;
    }
  }
  return nullptr;
}

void World::DestroyEntity(Entity & _entity) {
  if (_entity.static_model) {
    client->scene.DestroyStaticModel(*_entity.static_model.Unbind());
  } else if (_entity.animated_model) {
    client->scene.DestroyAnimatedModel(*_entity.animated_model.Unbind());
  }
  entities.Destroy(_entity);
}

Bullet & World::CreateBullet(Ref<game_data::Bullet> _bullet_data,
                             UniqueId _unique_id, numerics::Vector3f _position,
                             numerics::Vector3f _direction) {
  LOG_CLIENT(LOG, "Creating bullet #%" PRIu32 " '%s'..\n", _unique_id.Get(),
             cstr(_bullet_data->name));
  Bullet & bullet = bullets.Create(*this, _bullet_data, _unique_id);
  core::Optional<Ref<StaticModelObject>> static_model_ref =
      client->resources.GetStaticModel(_bullet_data->static_model);
  bullet.static_model = client->scene.CreateStaticModel(
      _bullet_data->static_model, *static_model_ref);
  bullet.SetPosition(_position);
  bullet.SetDirection(_direction);
  return bullet;
}

Bullet * World::GetBullet(UniqueId _bullet_unique_id) {
  for (Bullet & bullet : bullets) {
    if (bullet.unique_id == _bullet_unique_id) {
      return &bullet;
    }
  }
  return nullptr;
}

void World::DestroyBullet(Bullet & _bullet) {
  client->scene.DestroyStaticModel(*_bullet.static_model.GetObject());
  bullets.Destroy(_bullet);
}

}}
