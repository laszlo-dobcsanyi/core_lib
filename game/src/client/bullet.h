#ifndef _GAME_CLIENT_BULLET_H_
#define _GAME_CLIENT_BULLET_H_

#include "client/client.hh"
#include "client/model.h"

////////////////////////////////////////////////////////////////////////////////
// game client
////////////////////////////////////////////////////////////////////////////////

namespace game { namespace client {

/*******************************************************************************
** Bullet
*******************************************************************************/

class Bullet {
public:
  Ref<game_data::Bullet> bullet_data;
  UniqueId unique_id;
  WeakRef<StaticModel> static_model;

  Bullet() = delete;
  Bullet(World & _world, Ref<game_data::Bullet> _bullet_data,
         UniqueId _unique_id);
  Bullet(Bullet &&) = delete;
  Bullet(Bullet const &) = delete;
  Bullet & operator=(Bullet &&) = delete;
  Bullet & operator=(Bullet const &) = delete;
  ~Bullet() = default;

  numerics::Transformation GetTransformation() const;

  numerics::Vector3f GetPosition() const;
  void SetPosition(numerics::Vector3f _position);

  numerics::Vector3f GetDirection() const;
  void SetDirection(numerics::Vector3f _direction);

  numerics::Vector3f GetVelocity() const;
  void SetVelocity(numerics::Vector3f _velocity);

  void Tick(float _delta_seconds);

private:
  Ref<World> world;
  numerics::Transformation transformation;
  numerics::Vector3f direction;
  MovementState movement_state;
};

}}

#endif
