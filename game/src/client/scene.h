#ifndef _GAME_CLIENT_SCENE_H_
#define _GAME_CLIENT_SCENE_H_

#include "client/client.hh"
#include "client/model.h"
#include "graphics/camera.h"

////////////////////////////////////////////////////////////////////////////////
// game client
////////////////////////////////////////////////////////////////////////////////

namespace game { namespace client {

/*******************************************************************************
** Scene
*******************************************************************************/

class Scene {
public:
  Scene() = delete;
  Scene(Client & _client);
  Scene(Scene &&) = delete;
  Scene(Scene const &) = delete;
  Scene & operator=(Scene &&) = delete;
  Scene & operator=(Scene const &) = delete;
  ~Scene() = default;

  void Tick(float _delta_seconds);

  StaticModel &
  CreateStaticModel(Ref<game_data::StaticModel> _static_model_data_ref,
                    Ref<StaticModelObject> _static_model_ref);
  void DestroyStaticModel(StaticModel & _static_model);

  AnimatedModel &
  CreateAnimatedModel(Ref<game_data::AnimatedModel> _animated_model_data_ref,
                      Ref<AnimatedModelObject> _animated_model_ref);
  void DestroyAnimatedModel(AnimatedModel & _animated_model);

private:
  Ref<Client> client;

  graphics::Camera camera;

  container::PoolArray<StaticModel> static_models;
  container::PoolArray<AnimatedModel> animated_models;

  void TickCamera(float _delta_seconds);
};

}}

#endif
