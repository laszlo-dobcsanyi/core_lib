#include "client/entity.h"
#include "client/client.h"

////////////////////////////////////////////////////////////////////////////////
// game client
////////////////////////////////////////////////////////////////////////////////

namespace game { namespace client {

/*******************************************************************************
** Entity
*******************************************************************************/

Entity::Entity(World & _world, Ref<game_data::Entity> _entity_data,
               UniqueId _unique_id)
    : entity_data(move(_entity_data))
    , unique_id(_unique_id)
    , world(_world)
    , position(0.f, 0.f, 0.f)
    , direction(0.f, 0.f, 0.f)
    , movement(0.f, 0.f, 0.f) {
  numerics::set_identity(transformation);
}

void Entity::SetWeaponData(
    core::Optional<Ref<game_data::Weapon>> _weapon_data) {
  if (!weapon_data && !_weapon_data) {
    return;
  }

  if (weapon_data && _weapon_data &&
      (*weapon_data)->unique_id == (*_weapon_data)->unique_id) {
    return;
  }

  weapon_data = _weapon_data;

  if (!weapon_data) {
    return;
  } else if (animated_model) {
    core::Optional<Ref<StaticModelObject>> static_model_ref =
        world->client->resources.GetStaticModel((*weapon_data)->static_model);
    if (!static_model_ref)
      return;
    StaticModel & weapon_model = world->client->scene.CreateStaticModel(
        (*weapon_data)->static_model, *static_model_ref);
    animated_model->CreateAttachment(weapon_model, entity_data->attachment_bone,
                                     entity_data->attachment_translation,
                                     entity_data->attachment_direction);
  }
}

bool Entity::IsAttacking() const { return attacking; }

void Entity::SetAttacking(bool _attacking) { attacking = _attacking; }

numerics::Transformation Entity::GetTransformation() const {
  return transformation;
}

numerics::Vector3f Entity::GetPosition() const { return position; }

void Entity::SetPosition(numerics::Vector3f _position) {
  position = _position;
  numerics::set_translation(transformation, _position);
}

numerics::Vector3f Entity::GetDirection() const { return direction; }

void Entity::SetDirection(numerics::Vector3f _direction) {
  direction = _direction;
  numerics::set_rotation(transformation, numerics::to_rotation(direction));
}

numerics::Vector3f Entity::GetForward() const {
  numerics::Vector3f forward = numerics::Vector3f(0.f, 0.f, 1.f);
  numerics::Quaternionf rotation = numerics::to_rotation(direction);
  return numerics::rotate(forward, rotation);
}

numerics::Vector3f Entity::GetMovement() { return movement; }

void Entity::Move(numerics::Vector3f _position) {
  movement = _position - position;
  LOG_CLIENT(VERBOSE,
             "Moving to (%.4f, %.4f, %.4f) from (%.4f, %.4f, %.4f) = (%.4f, "
             "%.4f, %.4f) %.4f\n",
             _position[0u], _position[1u], _position[2u], position[0u],
             position[1u], position[2u], movement[0u], movement[1u],
             movement[2u], numerics::length(movement));
  SetPosition(_position);
}

numerics::Vector3f Entity::GetWorldShotpos() const {
  numerics::Vector3f const & shot_pos = entity_data->shot_pos;
  numerics::Vector3f up(0.f, 1.f, 0.f);
  numerics::Vector3f right =
      numerics::normalized(numerics::cross(GetForward(), up));
  numerics::Vector3f forward = numerics::normalized(numerics::cross(up, right));
  return GetPosition() + forward * shot_pos[0u] + up * shot_pos[1u] +
         right * shot_pos[2u];
}

void Entity::Tick(float _delta_seconds) {
  if (static_model) {
    static_model->Tick(_delta_seconds, GetTransformation());
  } else if (animated_model) {
    animated_model->Tick(_delta_seconds, GetTransformation(), attacking,
                         GetMovement(), GetForward());
  }
}

}}
