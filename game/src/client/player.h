#ifndef _GAME_CLIENT_PLAYER_H_
#define _GAME_CLIENT_PLAYER_H_

#include "core/byte_stream.hpp"
#include "client/client.hh"
#include "client/entity.h"

#include "net/tcp/connection.h"

////////////////////////////////////////////////////////////////////////////////
// game client
////////////////////////////////////////////////////////////////////////////////

namespace game { namespace client {

/*******************************************************************************
** Player
*******************************************************************************/

class Player {
public:
  Ref<World> world;
  UniqueId controlled_entity_id;

  Player() = delete;
  Player(World & _world);
  Player(Player &&) = delete;
  Player(Player const &) = delete;
  Player & operator=(Player &&) = delete;
  Player & operator=(Player const &) = delete;
  ~Player() = default;

  void SetConnection(net::tcp::Connection _connection);
  void Receive();
  void Send();
  void Shutdown();

  void Tick(float _delta_seconds);

  void SetControlledEntity(WeakRef<Entity> _controlled_entity_ref);
  WeakRef<Entity> GetControlledEntity() const;

  void SetForwardInput(float _value);
  void SetSideInput(float _value);
  void SetHorizontalInput(float _value);
  void SetVerticalInput(float _value);
  void SetShootInput(bool _value);

  numerics::Vector3f GetPosition() const;
  numerics::Vector3f GetDirection() const;
  numerics::Vector3f GetForward() const;

private:
  WeakRef<Entity> controlled_entity;

  float forward_input;
  float side_input;
  float horizontal_input;
  float vertical_input;
  bool shoot;

  numerics::Vector3f position;
  numerics::Vector3f direction;
  MovementState movement_state;

  net::tcp::Connection connection;
  core::UniqueArray<byte> receive_buffer;
  core::UniqueArray<byte> send_buffer;
  core::BigEndianByteStreamWriter send_writer;

  void SendServerUpdate(MovementState const & _client_movement,
                        numerics::Vector3f _direction, uint8 _flags);

  void ReceiveClientAdjust(core::BigEndianByteStreamReader & _reader);
  void ClientAdjust(MovementState _server_state);

  void ReceiveCreateEntity(core::BigEndianByteStreamReader & _reader);
  void CreateEntity(UniqueId _entity_id, UniqueId _entity_data_id,
                    numerics::Vector3f _position,
                    numerics::Vector3f _direction);

  void ReceiveUpdateEntity(core::BigEndianByteStreamReader & _reader);
  void UpdateEntity(UniqueId _entity_id, UniqueId _weapon_data_id,
                    bool _attacking, numerics::Vector3f _position,
                    numerics::Vector3f _direction);

  void ReceiveDestroyEntity(core::BigEndianByteStreamReader & _reader);
  void DestroyEntity(UniqueId _entity_id);

  void ReceiveCreateBullet(core::BigEndianByteStreamReader & _reader);
  void CreateBullet(UniqueId _bullet_id, UniqueId _bullet_data_id,
                    numerics::Vector3f _position,
                    numerics::Vector3f _direction);

  void ReceiveDestroyBullet(core::BigEndianByteStreamReader & _reader);
  void DestroyBullet(UniqueId _bullet_id);

  void ReceiveChangeEntity(core::BigEndianByteStreamReader & _reader);
  void ChangeEntity(UniqueId _entity_id);
};

}}

#endif
