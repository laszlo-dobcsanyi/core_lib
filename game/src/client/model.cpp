#include "client/model.h"

////////////////////////////////////////////////////////////////////////////////
// game client
////////////////////////////////////////////////////////////////////////////////

namespace game { namespace client {

/*******************************************************************************
** StaticModel
*******************************************************************************/

StaticModel::StaticModel(Ref<game_data::StaticModel> _static_model_data_ref,
                         Ref<StaticModelObject> _static_model_ref)
    : static_model_data_ref(move(_static_model_data_ref))
    , static_model_ref(move(_static_model_ref))
    , delta_seconds(0.f) {
  numerics::set_identity(transformation);
}

void StaticModel::Tick(float _delta_seconds,
                       numerics::Transformation _transformation) {
  delta_seconds = _delta_seconds;
  transformation = _transformation;
}

/*******************************************************************************
** AnimatedModel
*******************************************************************************/

AnimatedModel::AnimatedModel(
    Ref<game_data::AnimatedModel> _animated_model_data_ref,
    Ref<AnimatedModelObject> _animated_model_ref)
    : animated_model_data_ref(move(_animated_model_data_ref))
    , animated_model_ref(move(_animated_model_ref))
    , skeletal_animation(animated_model_ref->skeleton_data,
                         core::make_span(animated_model_ref->animation_data))
    , idle(nullptr)
    , run_forward(nullptr)
    , run_backward(nullptr)
    , run_right(nullptr)
    , run_left(nullptr)
    , run_speed(0.f)
    , delta_seconds(0.f)
    , attacking(false)
    , movement(0.f, 0.f, 0.f)
    , forward(0.f, 0.f, 0.f) {
  numerics::set_identity(transformation);

  idle = FindAnimationState(animated_model_data_ref->state_animation_node.idle);
  run_forward = FindAnimationState(
      animated_model_data_ref->state_animation_node.run_forward);
  run_backward = FindAnimationState(
      animated_model_data_ref->state_animation_node.run_backward);
  run_right = FindAnimationState(
      animated_model_data_ref->state_animation_node.run_right);
  run_left = FindAnimationState(
      animated_model_data_ref->state_animation_node.run_left);
  attack =
      FindAnimationState(animated_model_data_ref->state_animation_node.attack);

  run_speed = animated_model_data_ref->state_animation_node.run_speed;

  skeletal_animation.Start(*idle, true, 1.f);
  skeletal_animation.Start(*run_forward, true, 1.f);
  skeletal_animation.Start(*run_backward, true, 1.f);
  skeletal_animation.Start(*run_right, true, 1.f);
  skeletal_animation.Start(*run_left, true, 1.f);
}

void AnimatedModel::CreateAttachment(Ref<StaticModel> _static_model_ref,
                                     const_string _bone_name,
                                     numerics::Vector3f _translation,
                                     numerics::Vector3f _direction) {
  CORE_LIB_ASSERT(!attachment.HasValue());

  size_t bone_index = 0u;
  {
    core::UniqueArray<graphics::resource::Bone> & bones =
        skeletal_animation.GetSkeletonData().bones;
    for (size_t index = 0u; index < bones.Size(); ++index) {
      if (bones[index].name == _bone_name) {
        bone_index = index;
        break;
      }
    }
  }

  numerics::Transformation attachment_transformation(
      _translation, numerics::to_rotation(_direction));
  attachment = core::Optional<Attachment>(move(_static_model_ref), bone_index,
                                          attachment_transformation);
}

void AnimatedModel::Tick(float _delta_seconds,
                         numerics::Transformation _transformation,
                         bool _attacking, numerics::Vector3f _movement,
                         numerics::Vector3f _forward) {
  delta_seconds = _delta_seconds;
  attacking = _attacking;
  movement = _movement;
  forward = _forward;
  transformation = _transformation;
}

void AnimatedModel::Animate() {
  float weight = 1.f;

  if (attacking) {
    if (!attack->IsActive()) {
      skeletal_animation.Start(*attack, false, 1.f);
    }
    Blend(weight, *attack, 1.f, delta_seconds);
  } else {
    if (attack->IsActive()) {
      Blend(weight, *attack, 0.f, delta_seconds);
      if (numerics::is_nearly_zero(attack->GetWeight())) {
        skeletal_animation.Stop(*attack);
      }
    }
  }

  // CORE_LIB_LOG( VERBOSE, "Movement = (%.4f, %.4f, %.4f)\n", movement[ 0u ], movement[ 1u ], movement[ 2u ] );
  movement[1u] = 0.f;
  float run_rate = (numerics::length(movement) / run_speed) / delta_seconds;
  if (run_rate < 0.15f) {
    Blend(weight, *run_forward, 0.f, delta_seconds);
    Blend(weight, *run_backward, 0.f, delta_seconds);
    Blend(weight, *run_right, 0.f, delta_seconds);
    Blend(weight, *run_left, 0.f, delta_seconds);
  } else {
    numerics::normalize(movement);
    CORE_LIB_ASSERT(numerics::is_nearly_zero(forward[1u]));
    float forward_weight = numerics::dot(forward, movement);

    if (0.f < forward_weight) {
      float run_weight = numerics::sqr(forward_weight);
      Blend(weight, *run_forward, run_weight, delta_seconds);
      Blend(weight, *run_backward, 0.f, delta_seconds);
    } else {
      float run_weight = numerics::sqr(forward_weight);
      Blend(weight, *run_forward, 0.f, delta_seconds);
      Blend(weight, *run_backward, run_weight, delta_seconds);
    }
    numerics::Vector3f side;
    side[0u] = -forward[2u];
    side[1u] = 0.f;
    side[2u] = forward[0u];
    float side_weight = numerics::dot(side, movement);
    if (0.f < side_weight) {
      float run_weight = numerics::sqr(side_weight);
      Blend(weight, *run_right, run_weight, delta_seconds);
      Blend(weight, *run_left, 0.f, delta_seconds);
    } else {
      float run_weight = numerics::sqr(side_weight);
      Blend(weight, *run_right, 0.f, delta_seconds);
      Blend(weight, *run_left, run_weight, delta_seconds);
    }
    // CORE_LIB_LOG( LOG, "forward_weight = %.4f, side_weight = %.4f\n", numerics::sqr( forward_weight ), numerics::sqr(side_weight ) );
    CORE_LIB_ASSERT(numerics::are_nearly_equal(
        1.f, numerics::sqr(forward_weight) + numerics::sqr(side_weight)));
  }
  run_forward->SetRate(run_rate * run_forward->GetDuration());
  run_backward->SetRate(run_rate * run_backward->GetDuration());
  run_right->SetRate(run_rate * run_right->GetDuration());
  run_left->SetRate(run_rate * run_left->GetDuration());

  CORE_LIB_ASSERT(0.f <= weight);
  idle->SetWeight(weight);

  skeletal_animation.Update(delta_seconds);
  skeletal_animation.Animate();

  if (attachment) {
    auto & bones = skeletal_animation.GetSkeletonData().bones;
    graphics::TransformationMatrix bone_transformation =
        skeletal_animation.GetTransformations()[attachment->bone_index] *
        bones[attachment->bone_index].transformation;
    numerics::Transformation attachment_transformation;
    numerics::set_transformation(attachment_transformation,
                                 transformation * bone_transformation *
                                     attachment->transformation);
    attachment->static_model->Tick(delta_seconds, attachment_transformation);
  }
}

graphics::animation::AnimationState *
AnimatedModel::FindAnimationState(const_string _name) {
  for (graphics::animation::AnimationState & animation_state :
       skeletal_animation.GetAnimationStates()) {
    if (animation_state.GetAnimationData().name == _name) {
      return &animation_state;
    }
  }
  CORE_LIB_ASSERT_MESSAGE("Failed to find animation '%s'!\n", cstr(_name));
  return nullptr;
}

float AnimatedModel::Blend(float _current_weight, float _target_weight,
                           float _delta_seconds) {
  const float blend = 5.f;
  if (_current_weight < _target_weight) {
    return core::min(_current_weight + _delta_seconds * blend, _target_weight);
  }
  return core::max(_target_weight, _current_weight - _delta_seconds * blend);
}

void AnimatedModel::Blend(
    float & _weight, graphics::animation::AnimationState & _animation_state,
    float _target_weight, float _delta_seconds) {
  CORE_LIB_ASSERT(0.f <= _weight);
  float target_weight = core::min(_weight, _target_weight);
  float animation_weight =
      Blend(_animation_state.GetWeight(), target_weight, _delta_seconds);
  CORE_LIB_ASSERT(0.f <= animation_weight);
  CORE_LIB_ASSERT(animation_weight <= 1.f);
  _animation_state.SetWeight(animation_weight);
  _weight = core::max(0.f, _weight - animation_weight);
}

}}
