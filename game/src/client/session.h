#ifndef _GAME_CLIENT_SESSION_H_
#define _GAME_CLIENT_SESSION_H_

#include "client/client.hh"
#include "net/ip/endpoint.h"
#include "net/tcp/connector.h"

////////////////////////////////////////////////////////////////////////////////
// game client
////////////////////////////////////////////////////////////////////////////////

namespace game { namespace client {

/*******************************************************************************
** Session
*******************************************************************************/

class Session {
public:
  Session() = delete;
  Session(Client & _client);
  Session(Session &&) = delete;
  Session(Session const &) = delete;
  Session & operator=(Session &&) = delete;
  Session & operator=(Session const &) = delete;
  ~Session() = default;

  bool Create(net::ip::Endpoint _remote_endpoint);

  bool IsConnecting() const;

  void Tick(float _delta_seconds);

private:
  Ref<Client> client;
  net::tcp::Connector connector;
  float timeout;

  void Connect();
};

}}

#endif
