#include "client/resources.h"
#include "graphics/resource/load_map.hpp"
#include "graphics/resource/load_model.h"
#include "graphics/resource/load_skybox.h"

#include "client/client.h"

////////////////////////////////////////////////////////////////////////////////
// game client
////////////////////////////////////////////////////////////////////////////////

namespace game { namespace client {

/*******************************************************************************
** Resources
*******************************************************************************/

Resources::Resources(Client & _client)
    : client(_client) {}

graphics::ForwardTerrainRenderer & Resources::GetTerrainRenderer() {
  return terrain_renderer;
}

graphics::ForwardSkyboxRenderer & Resources::GetSkyboxRenderer() {
  return skybox_renderer;
}

graphics::ForwardStaticModelRenderer & Resources::GetStaticModelRenderer() {
  return static_model_renderer;
}

graphics::ForwardAnimatedModelRenderer & Resources::GetAnimatedModelRenderer() {
  return animated_model_renderer;
}

core::Optional<Ref<MapObject>>
Resources::GetMap(Ref<game_data::Map> _map_data) {
  for (MapObject & map : maps) {
    if (map.name == _map_data->name) {
      return Ref<MapObject>(map);
    }
  }
  LOG_CLIENT(ERROR, "Map '%s' not found!\n", cstr(_map_data->name));
  return core::nullopt;
}

Ref<SkyboxObject> Resources::GetSkybox() { return Ref<SkyboxObject>(skybox); }

core::Optional<Ref<StaticModelObject>>
Resources::GetStaticModel(Ref<game_data::StaticModel> _static_model_data) {
  for (StaticModelObject & static_model : static_models) {
    if (static_model.name == _static_model_data->name) {
      return Ref<StaticModelObject>(static_model);
    }
  }
  LOG_CLIENT(ERROR, "StaticModel '%s' not found!\n",
             cstr(_static_model_data->name));
  return core::nullopt;
}

core::Optional<Ref<AnimatedModelObject>> Resources::GetAnimatedModel(
    Ref<game_data::AnimatedModel> _animated_model_data) {
  for (AnimatedModelObject & animated_model : animated_models) {
    if (animated_model.name == _animated_model_data->name) {
      return Ref<AnimatedModelObject>(animated_model);
    }
  }
  LOG_CLIENT(ERROR, "AnimatedModel '%s' not found!\n",
             cstr(_animated_model_data->name));
  return core::nullopt;
}

bool Resources::Initialize(GameData const & _game_data,
                           core::fs::Directory const & _data_root) {
  // Renderers
  if (auto terrain_renderer_result =
          graphics::create_forward_terrain_renderer()) {
    terrain_renderer = move(*terrain_renderer_result);
  } else {
    LOG_CLIENT(ERROR, "Failed to create terrain renderer!\n");
    return false;
  }

  if (auto skybox_renderer_result =
          graphics::create_forward_skybox_renderer()) {
    skybox_renderer = move(*skybox_renderer_result);
  } else {
    LOG_CLIENT(ERROR, "Failed to create skybox renderer!\n");
    return false;
  }

  if (auto static_model_renderer_result =
          graphics::create_forward_static_model_renderer()) {
    static_model_renderer = move(*static_model_renderer_result);
  } else {
    LOG_CLIENT(ERROR, "Failed to create static model renderer!\n");
    return false;
  }

  if (auto animated_model_renderer_result =
          graphics::create_forward_animated_model_renderer()) {
    animated_model_renderer = move(*animated_model_renderer_result);
  } else {
    LOG_CLIENT(ERROR, "Failed to create animated model renderer!\n");
    return false;
  }

  // Maps
  maps = core::make_unique_array<Object<graphics::Map>>(_game_data.maps.Size());
  size_t map_index = 0u;
  for (game_data::Map const & map : _game_data.maps) {
    core::fs::Directory map_directory(_data_root / "maps" / map.name);
    if (auto map_data_result =
            graphics::resource::load_map(move(map_directory))) {
      maps[map_index] = Object<graphics::Map>(move(*map_data_result));
      // TODO: Move this to load_map!
      maps[map_index].name = map.name;
      ++map_index;
    } else {
      LOG_CLIENT(ERROR, "Failed to load map from '%s'!\n", cstr(map_directory));
      return false;
    }
  }

  // Skybox
  core::fs::Directory skybox_directory(_data_root / "skybox");
  if (auto skybox_result =
          graphics::resource::load_skybox(move(skybox_directory))) {
    skybox = Object<graphics::Skybox>(move(*skybox_result));
  } else {
    LOG_CLIENT(ERROR, "Failed to load skybox from '%s'!\n",
               cstr(skybox_directory));
    return false;
  }

  // Entities
  for (game_data::Entity const & entity : _game_data.entities) {
    if (entity.static_model) {
      core::fs::Directory model_directory(_data_root / "models" /
                                          entity.static_model->name);
      if (auto static_model_result =
              graphics::resource::load_static_model(model_directory)) {
        Object<graphics::StaticModel> & static_model =
            static_models.Create(move(*static_model_result));
        // TODO: Move this to load_static_model!
        static_model.name = entity.static_model->name;
      } else {
        LOG_CLIENT(ERROR, "Failed to load static model for '%s'!\n",
                   cstr(entity.name));
        return false;
      }
    } else if (entity.animated_model) {
      core::fs::Directory model_directory(_data_root / "models" /
                                          entity.animated_model->name);
      if (auto animated_model_result =
              graphics::resource::load_animated_model(model_directory)) {
        Object<graphics::AnimatedModel> & animated_model =
            animated_models.Create(move(*animated_model_result));
        // TODO: Move this to load_animated_model!
        animated_model.name = entity.animated_model->name;
      } else {
        LOG_CLIENT(ERROR, "Failed to load animated model for '%s'!\n",
                   cstr(entity.name));
        return false;
      }
    }
  }

  // Bullets
  for (game_data::Bullet const & bullet : _game_data.bullets) {
    core::fs::Directory model_directory(_data_root / "models" /
                                        bullet.static_model.name);
    if (auto static_model_result =
            graphics::resource::load_static_model(model_directory)) {
      Object<graphics::StaticModel> & static_model =
          static_models.Create(move(*static_model_result));
      // TODO: Move this to load_static_model!
      static_model.name = bullet.static_model.name;
    } else {
      LOG_CLIENT(ERROR, "Failed to load static model for '%s'!\n",
                 cstr(bullet.name));
      return false;
    }
  }

  // Weapons
  for (game_data::Weapon const & weapon : _game_data.weapons) {
    core::fs::Directory model_directory(_data_root / "models" /
                                        weapon.static_model.name);
    if (auto static_model_result =
            graphics::resource::load_static_model(model_directory)) {
      Object<graphics::StaticModel> & static_model =
          static_models.Create(move(*static_model_result));
      // TODO: Move this to load_static_model!
      static_model.name = weapon.static_model.name;
    } else {
      LOG_CLIENT(ERROR, "Failed to load static model for '%s'!\n",
                 cstr(weapon.name));
      return false;
    }
  }
  return true;
}

}}
