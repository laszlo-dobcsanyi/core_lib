#ifndef _GAME_CLIENT_GRAPHICS_H_
#define _GAME_CLIENT_GRAPHICS_H_

#include "client/client.hh"
#include "client/resources.h"

#include "graphics/api.h"
#include "graphics/window.h"

////////////////////////////////////////////////////////////////////////////////
// game client
////////////////////////////////////////////////////////////////////////////////

namespace game { namespace client {

/*******************************************************************************
** Graphics
*******************************************************************************/

class Graphics {
public:
  graphics::api::Context graphics_context;
  graphics::Window window;

  Graphics() = delete;
  Graphics(Client & _client);
  Graphics(Graphics &&) = delete;
  Graphics(Graphics const &) = delete;
  Graphics & operator=(Graphics &&) = delete;
  Graphics & operator=(Graphics const &) = delete;
  ~Graphics() = default;

  bool Create();
  void Tick(float _delta_seconds);
  void Destroy();

  bool ShouldTerminate() const;

private:
  Ref<Client> client;
};

}}

#endif
