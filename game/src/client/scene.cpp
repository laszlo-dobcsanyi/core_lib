#include "client/scene.h"
#include "client/client.h"

////////////////////////////////////////////////////////////////////////////////
// game client
////////////////////////////////////////////////////////////////////////////////

namespace game { namespace client {

/*******************************************************************************
** Scene
*******************************************************************************/

Scene::Scene(Client & _client)
    : client(_client) {
  camera.SetPerspective(45.f, 16.f / 9.f, 0.1f, 100000.f);
}

void Scene::Tick(float _delta_seconds) {
  TickCamera(_delta_seconds);

  for (AnimatedModel & animated_model : animated_models) {
    animated_model.Animate();
  }

  // Terrain
  {
    graphics::ForwardTerrainRenderer & terrain_renderer =
        client->resources.GetTerrainRenderer();
    graphics::bind(terrain_renderer);
    graphics::render(terrain_renderer, client->world.GetMap().terrain, camera);
  }

  // Skybox
  {
    graphics::ForwardSkyboxRenderer & skybox_renderer =
        client->resources.GetSkyboxRenderer();
    graphics::bind(skybox_renderer);
    graphics::render(skybox_renderer, *client->resources.GetSkybox(), camera);
  }

  // StaticModels
  {
    graphics::ForwardStaticModelRenderer & static_model_renderer =
        client->resources.GetStaticModelRenderer();
    graphics::bind(static_model_renderer);
    for (StaticModel const & static_model : static_models) {
      graphics::TransformationMatrix transformation;
      numerics::set_matrix(transformation, static_model.GetTransformation());
      graphics::render(static_model_renderer, *static_model.static_model_ref,
                       transformation, camera);
    }
  }

  // AnimatedModels
  {
    graphics::ForwardAnimatedModelRenderer & animated_model_renderer =
        client->resources.GetAnimatedModelRenderer();
    graphics::bind(animated_model_renderer);
    for (AnimatedModel const & animated_model : animated_models) {
      graphics::TransformationMatrix transformation;
      numerics::set_matrix(transformation, animated_model.GetTransformation());
      graphics::render(
          animated_model_renderer, *animated_model.animated_model_ref,
          transformation,
          core::make_span(
              animated_model.skeletal_animation.GetTransformations()),
          camera);
    }
  }
}

StaticModel &
Scene::CreateStaticModel(Ref<game_data::StaticModel> _static_model_data_ref,
                         Ref<StaticModelObject> _static_model_ref) {
  return static_models.Create(move(_static_model_data_ref),
                              move(_static_model_ref));
}

void Scene::DestroyStaticModel(StaticModel & _static_model) {
  static_models.Destroy(_static_model);
}

client::AnimatedModel & Scene::CreateAnimatedModel(
    Ref<game_data::AnimatedModel> _animated_model_data_ref,
    Ref<AnimatedModelObject> _animated_model_ref) {
  return animated_models.Create(move(_animated_model_data_ref),
                                move(_animated_model_ref));
}

void Scene::DestroyAnimatedModel(AnimatedModel & _animated_model) {
  if (_animated_model.attachment.HasValue()) {
    DestroyStaticModel(_animated_model.attachment->static_model.Unbind());
  }
  animated_models.Destroy(_animated_model);
}

void Scene::TickCamera(float _delta_seconds) {
  numerics::Vector3f camera_position;
  numerics::Vector3f camera_target;
  if (!client->player.GetControlledEntity()) {
    float distance = 1500.f;
    camera_target = client->player.GetPosition();
    numerics::Vector3f forward = client->player.GetForward();
    camera_position = camera_target - forward * distance;
    float height =
        client->world.GetMap().map_data.grid.height_at(camera_position) + 1.f;
    camera_position.y = core::max(height, camera_position.y);
  } else {
    WeakRef<Entity> entity = client->player.GetControlledEntity();
    numerics::Vector3f const & shot_pos = entity->entity_data->shot_pos;
    numerics::Vector3f up(0.f, 1.f, 0.f);
    numerics::Vector3f right =
        numerics::normalized(numerics::cross(entity->GetForward(), up));
    numerics::Vector3f forward =
        numerics::normalized(numerics::cross(up, right));
    camera_target = entity->GetPosition() + forward * shot_pos[0u] +
                    up * (shot_pos[1u] + 25.f) + right * (shot_pos[2u] + 10.f);
    // camera_target = client.player.GetControlledEntity()->GetWorldShotpos();
    float distance = 400.f;
    camera_position =
        camera_target - client->player.GetForward() * distance + right * 10.f;
    float height =
        client->world.GetMap().map_data.grid.height_at(camera_position) + 1.f;
    camera_position.y = core::max(height, camera_position.y);
  }

  camera.Set(camera_position, camera_target);
}

}}
