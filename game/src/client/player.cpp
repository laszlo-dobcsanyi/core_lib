#include "client/player.h"
#include "client/client.h"

#define LOG_CLIENT_RPC CORE_LIB_LOG(VERBOSE, "\n")

////////////////////////////////////////////////////////////////////////////////
// game client
////////////////////////////////////////////////////////////////////////////////

namespace game { namespace client {

/*******************************************************************************
** Player
*******************************************************************************/

Player::Player(World & _world)
    : world(_world)
    , forward_input(0.f)
    , side_input(0.f)
    , horizontal_input(0.f)
    , vertical_input(0.f)
    , shoot(false)
    , position(0.f, 0.f, 0.f)
    , direction(0.f, 0.f, 0.f) {}

void Player::SetConnection(net::tcp::Connection _connection) {
  connection = move(_connection);
  receive_buffer = core::make_unique_array<byte>(8192u);
  send_buffer = core::make_unique_array<byte>(1024u);
  send_writer = core::BigEndianByteStreamWriter(core::make_span(send_buffer));
}

void Player::Receive() {
  // TODO:
  if (!connection.IsOpen()) {
    // LOG_CLIENT( WARNING, "Connection is closed!\n" );
    return;
  }

  core::span<byte const> received =
      connection.Receive(core::make_span(receive_buffer));
  if (!received) {
    return;
  }

  core::BigEndianByteStreamReader reader(received);
  while (reader.Available() != 0u) {
    uint8 header;
    if (!reader.Read(header)) {
      LOG_CLIENT(ERROR, "Failed to read header!\n");
      Shutdown();
      return;
    }

    CORE_LIB_LOG(VERBOSE, "Header: %" PRIu8 " (Available: %zu)..\n", header,
                 reader.Available());

    switch (header) {
      case U8(0): {
      } break;

      case U8(1): {
        ReceiveCreateEntity(reader);
      } break;
      case U8(2): {
        ReceiveUpdateEntity(reader);
      } break;
      case U8(3): {
        ReceiveDestroyEntity(reader);
      } break;
      case U8(4): {
        ReceiveCreateBullet(reader);
      } break;
      case U8(5): {
        ReceiveDestroyBullet(reader);
      } break;

      case U8(10): {
        ReceiveChangeEntity(reader);
      } break;

      case U8(20): {
        ReceiveClientAdjust(reader);
      } break;

      default: {
        LOG_CLIENT(ERROR, "Unknown header!\n");
        Shutdown();
        return;
      } break;
    }
  }
}

void Player::Send() {
  // TODO:
  if (!connection.IsOpen()) {
    // LOG_CLIENT( WARNING, "Connection is closed!\n" );
    send_writer.Reset();
    return;
  }

  if (!send_writer.Write(U8(0))) {
    LOG_CLIENT(VERBOSE, "Failed to close payload!\n");
    return;
  }

  if (!connection.Send(send_writer.WrittenBytes())) {
    LOG_CLIENT(ERROR, "Failed to send payload!\n");
    // TODO: Handle error
    return;
  }
  send_writer.Reset();
}

void Player::Shutdown() { LOG_CLIENT(LOG, "Shutting down..\n"); }

void Player::Tick(float _delta_seconds) {
  direction.x = numerics::clamp(direction.x + vertical_input, -85.f, 85.f);
  direction.y = direction.y - horizontal_input;

  numerics::Vector3f input = numerics::Vector3f(side_input, 0.f, forward_input);
  numerics::Quaternionf rotation = numerics::to_rotation(direction);
  numerics::Vector3f forward = numerics::rotate(input, rotation);

  if (controlled_entity) {
    MovementState new_movement = movement_state;
    new_movement.acceleration =
        forward * controlled_entity->entity_data->input_acceleration;
    new_movement.Tick(_delta_seconds);
    // TODO: Collision
    new_movement.position.y =
        world->GetMap().map_data.grid.height_at(new_movement.position);
    movement_state = new_movement;

    controlled_entity->Move(new_movement.position);
    controlled_entity->SetDirection(numerics::Vector3f(0.f, direction.y, 0.f));

    position = new_movement.position;

    LOG_CLIENT(VERBOSE, "Position: (%.4f, %.4f, %.4f)\n", position[0u],
               position[1u], position[2u]);

    uint8 flags = 0;
    if (shoot) {
      flags |= (1 << 0);
    }
    SendServerUpdate(movement_state, direction, flags);
  } else {
    position += forward * 3000.f * _delta_seconds;
    // TODO: Collision
    position.y = core::max(position.y,
                           world->GetMap().map_data.grid.height_at(position));
  }
}

void Player::SetControlledEntity(WeakRef<Entity> _controlled_entity_ref) {
  controlled_entity = move(_controlled_entity_ref);
  if (controlled_entity) {
    movement_state.Reset();
    movement_state.position = controlled_entity->GetPosition();
    movement_state.friction = controlled_entity->entity_data->friction;
    movement_state.max_speed = controlled_entity->entity_data->max_speed;
  }
}

WeakRef<Entity> Player::GetControlledEntity() const {
  return controlled_entity;
}

void Player::SetForwardInput(float _value) { forward_input = _value; }

void Player::SetSideInput(float _value) { side_input = _value; }

void Player::SetHorizontalInput(float _value) { horizontal_input = _value; }

void Player::SetVerticalInput(float _value) { vertical_input = _value; }

void Player::SetShootInput(bool _value) { shoot = _value; }

numerics::Vector3f Player::GetPosition() const { return position; }

numerics::Vector3f Player::GetDirection() const { return direction; }

numerics::Vector3f Player::GetForward() const {
  numerics::Vector3f forward = numerics::Vector3f(0.f, 0.f, 1.f);
  numerics::Quaternionf rotation = numerics::to_rotation(direction);
  return numerics::rotate(forward, rotation);
}

void Player::SendServerUpdate(MovementState const & _client_movement,
                              numerics::Vector3f _direction, uint8 _flags) {
  LOG_CLIENT_RPC;

  if (!send_writer.Write(U8(1))) {
    Shutdown();
    return;
  }

  if (!send_writer.Write(_client_movement.delta_seconds)) {
    Shutdown();
    return;
  }

  if (!send_writer.Write(_client_movement.acceleration[0u])) {
    Shutdown();
    return;
  }
  if (!send_writer.Write(_client_movement.acceleration[1u])) {
    Shutdown();
    return;
  }
  if (!send_writer.Write(_client_movement.acceleration[2u])) {
    Shutdown();
    return;
  }

  if (!send_writer.Write(_client_movement.velocity[0u])) {
    Shutdown();
    return;
  }
  if (!send_writer.Write(_client_movement.velocity[1u])) {
    Shutdown();
    return;
  }
  if (!send_writer.Write(_client_movement.velocity[2u])) {
    Shutdown();
    return;
  }

  if (!send_writer.Write(_client_movement.position[0u])) {
    Shutdown();
    return;
  }
  if (!send_writer.Write(_client_movement.position[1u])) {
    Shutdown();
    return;
  }
  if (!send_writer.Write(_client_movement.position[2u])) {
    Shutdown();
    return;
  }

  if (!send_writer.Write(_direction[0u])) {
    Shutdown();
    return;
  }
  if (!send_writer.Write(_direction[1u])) {
    Shutdown();
    return;
  }
  // if ( !send_writer.Write( _direction[ 1u ] ) ) { Shutdown(); return; }

  if (!send_writer.Write(_flags)) {
    Shutdown();
    return;
  }
}

void Player::ReceiveClientAdjust(core::BigEndianByteStreamReader & _reader) {
  LOG_CLIENT_RPC;

  MovementState server_state;
  if (!_reader.Read(server_state.acceleration[0u])) {
    Shutdown();
    return;
  }
  if (!_reader.Read(server_state.acceleration[1u])) {
    Shutdown();
    return;
  }
  if (!_reader.Read(server_state.acceleration[2u])) {
    Shutdown();
    return;
  }

  if (!_reader.Read(server_state.velocity[0u])) {
    Shutdown();
    return;
  }
  if (!_reader.Read(server_state.velocity[1u])) {
    Shutdown();
    return;
  }
  if (!_reader.Read(server_state.velocity[2u])) {
    Shutdown();
    return;
  }

  if (!_reader.Read(server_state.position[0u])) {
    Shutdown();
    return;
  }
  if (!_reader.Read(server_state.position[1u])) {
    Shutdown();
    return;
  }
  if (!_reader.Read(server_state.position[2u])) {
    Shutdown();
    return;
  }

  // TODO: Temporary
  // ClientAdjust( server_state );
}

void Player::ClientAdjust(MovementState _server_state) {
  movement_state.acceleration = _server_state.acceleration;
  movement_state.velocity = _server_state.velocity;
  movement_state.position = _server_state.position;
  if (controlled_entity) {
    controlled_entity->SetPosition(movement_state.position);
  }
}

void Player::ReceiveCreateEntity(core::BigEndianByteStreamReader & _reader) {
  LOG_CLIENT_RPC;

  UniqueId entity_id;
  if (!_reader.Read(entity_id.Get())) {
    Shutdown();
    return;
  }

  UniqueId entity_data_id;
  if (!_reader.Read(entity_data_id.Get())) {
    Shutdown();
    return;
  }

  numerics::Vector3f entity_position;
  if (!_reader.Read(entity_position[0u])) {
    Shutdown();
    return;
  }
  if (!_reader.Read(entity_position[1u])) {
    Shutdown();
    return;
  }
  if (!_reader.Read(entity_position[2u])) {
    Shutdown();
    return;
  }

  numerics::Vector3f entity_direction;
  // if ( !_reader.Read( entity_direction[ 0u ] ) ) { Shutdown(); return; }
  if (!_reader.Read(entity_direction[1u])) {
    Shutdown();
    return;
  }
  // if ( !_reader.Read( entity_direction[ 2u ] ) ) { Shutdown(); return; }

  CreateEntity(entity_id, entity_data_id, entity_position, entity_direction);
}

void Player::CreateEntity(UniqueId _entity_id, UniqueId _entity_data_id,
                          numerics::Vector3f _position,
                          numerics::Vector3f _direction) {
  core::Optional<Ref<game_data::Entity>> entity_data =
      world->client->application->game_data.GetEntityData(_entity_data_id);
  if (!entity_data) {
    LOG_CLIENT(ERROR, "Failed to Create Entity!\n");
    return;
  }
  Entity & entity =
      world->CreateEntity(*entity_data, _entity_id, _position, _direction);
  if (controlled_entity_id == _entity_id) {
    SetControlledEntity(entity);
  }
}

void Player::ReceiveUpdateEntity(core::BigEndianByteStreamReader & _reader) {
  LOG_CLIENT_RPC;

  UniqueId entity_id;
  if (!_reader.Read(entity_id.Get())) {
    Shutdown();
    return;
  }

  UniqueId weapon_data_id;
  if (!_reader.Read(weapon_data_id.Get())) {
    Shutdown();
    return;
  }

  uint8 flags;
  if (!_reader.Read(flags)) {
    Shutdown();
    return;
  }
  bool attacking = (flags & (1 << 0)) != 0;

  numerics::Vector3f entity_position;
  if (!_reader.Read(entity_position[0u])) {
    Shutdown();
    return;
  }
  if (!_reader.Read(entity_position[1u])) {
    Shutdown();
    return;
  }
  if (!_reader.Read(entity_position[2u])) {
    Shutdown();
    return;
  }

  numerics::Vector3f entity_direction(0.f, 0.f, 0.f);
  // if ( !_reader.Read( entity_direction[ 0u ] ) ) { Shutdown(); return; }
  if (!_reader.Read(entity_direction[1u])) {
    Shutdown();
    return;
  }
  // if ( !_reader.Read( entity_direction[ 2u ] ) ) { Shutdown(); return; }

  UpdateEntity(entity_id, weapon_data_id, attacking, entity_position,
               entity_direction);
}

void Player::UpdateEntity(UniqueId _entity_id, UniqueId _weapon_data_id,
                          bool _attacking, numerics::Vector3f _position,
                          numerics::Vector3f _direction) {
  Entity * entity = world->GetEntity(_entity_id);
  if (!entity) {
    LOG_CLIENT(ERROR, "Failed to Update Entity!\n");
    return;
  }

  if (_weapon_data_id.HasValue()) {
    entity->SetWeaponData(
        world->client->application->game_data.GetWeaponData(_weapon_data_id));
  } else {
    entity->SetWeaponData(core::nullopt);
  }

  if (_entity_id != controlled_entity_id) {
    entity->SetAttacking(_attacking);
    entity->Move(_position);
    entity->SetDirection(_direction);
  }
}

void Player::ReceiveDestroyEntity(core::BigEndianByteStreamReader & _reader) {
  LOG_CLIENT_RPC;

  UniqueId entity_id;
  if (!_reader.Read(entity_id.Get())) {
    Shutdown();
    return;
  }

  DestroyEntity(entity_id);
}

void Player::DestroyEntity(UniqueId _entity_id) {
  Entity * entity = world->GetEntity(_entity_id);
  CORE_LIB_ASSERT(entity);
  if (!entity) {
    LOG_CLIENT(ERROR, "Failed to Destroy Entity!\n");
    return;
  }
  world->DestroyEntity(*entity);
}

void Player::ReceiveCreateBullet(core::BigEndianByteStreamReader & _reader) {
  LOG_CLIENT_RPC;

  UniqueId bullet_id;
  if (!_reader.Read(bullet_id.Get())) {
    Shutdown();
    return;
  }

  UniqueId bullet_data_id;
  if (!_reader.Read(bullet_data_id.Get())) {
    Shutdown();
    return;
  }

  numerics::Vector3f bullet_position;
  if (!_reader.Read(bullet_position[0u])) {
    Shutdown();
    return;
  }
  if (!_reader.Read(bullet_position[1u])) {
    Shutdown();
    return;
  }
  if (!_reader.Read(bullet_position[2u])) {
    Shutdown();
    return;
  }

  numerics::Vector3f bullet_direction;
  if (!_reader.Read(bullet_direction[0u])) {
    Shutdown();
    return;
  }
  if (!_reader.Read(bullet_direction[1u])) {
    Shutdown();
    return;
  }
  if (!_reader.Read(bullet_direction[2u])) {
    Shutdown();
    return;
  }

  CreateBullet(bullet_id, bullet_data_id, bullet_position, bullet_direction);
}

void Player::CreateBullet(UniqueId _bullet_id, UniqueId _bullet_data_id,
                          numerics::Vector3f _position,
                          numerics::Vector3f _direction) {
  core::Optional<Ref<game_data::Bullet>> bullet_data =
      world->client->application->game_data.GetBulletData(_bullet_data_id);
  if (!bullet_data) {
    LOG_CLIENT(ERROR, "Failed to Create Bullet!\n");
    return;
  }
  Bullet & bullet =
      world->CreateBullet(*bullet_data, _bullet_id, _position, _direction);
}

void Player::ReceiveDestroyBullet(core::BigEndianByteStreamReader & _reader) {
  LOG_CLIENT_RPC;

  UniqueId bullet_id;
  if (!_reader.Read(bullet_id.Get())) {
    Shutdown();
    return;
  }

  DestroyBullet(bullet_id);
}

void Player::DestroyBullet(UniqueId _bullet_id) {
  Bullet * bullet = world->GetBullet(_bullet_id);
  CORE_LIB_ASSERT(bullet);
  if (!bullet) {
    LOG_CLIENT(ERROR, "Failed to Destroy Bullet!\n");
    return;
  }
  world->DestroyBullet(*bullet);
}

void Player::ReceiveChangeEntity(core::BigEndianByteStreamReader & _reader) {
  LOG_CLIENT_RPC;

  UniqueId entity_id;
  if (!_reader.Read(entity_id.Get())) {
    Shutdown();
    return;
  }

  ChangeEntity(entity_id);
}

void Player::ChangeEntity(UniqueId _entity_id) {
  controlled_entity_id = _entity_id;
  if (controlled_entity_id.HasValue()) {
    SetControlledEntity(world->GetEntity(controlled_entity_id));
  } else {
    SetControlledEntity(nullptr);
  }
}

}}
