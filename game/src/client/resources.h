#ifndef _GAME_CLIENT_RESOURCES_H_
#define _GAME_CLIENT_RESOURCES_H_

#include "client/client.hh"

#include "graphics/renderers/forward_animated_model_renderer.h"
#include "graphics/renderers/forward_static_model_renderer.h"
#include "graphics/renderers/forward_skybox_renderer.h"
#include "graphics/renderers/forward_terrain_renderer.h"

////////////////////////////////////////////////////////////////////////////////
// game client
////////////////////////////////////////////////////////////////////////////////

namespace game { namespace client {

/*******************************************************************************
** Resources
*******************************************************************************/

class Resources {
public:
  Resources() = delete;
  Resources(Client & _client);
  Resources(Resources &&) = default;
  Resources(Resources const &) = delete;
  Resources & operator=(Resources &&) = default;
  Resources & operator=(Resources const &) = delete;
  ~Resources() = default;

  graphics::ForwardTerrainRenderer & GetTerrainRenderer();
  graphics::ForwardSkyboxRenderer & GetSkyboxRenderer();
  graphics::ForwardStaticModelRenderer & GetStaticModelRenderer();
  graphics::ForwardAnimatedModelRenderer & GetAnimatedModelRenderer();

  core::Optional<Ref<MapObject>> GetMap(Ref<game_data::Map> _map_data);
  Ref<SkyboxObject> GetSkybox();
  core::Optional<Ref<StaticModelObject>>
  GetStaticModel(Ref<game_data::StaticModel> _static_model_data);
  core::Optional<Ref<AnimatedModelObject>>
  GetAnimatedModel(Ref<game_data::AnimatedModel> _animated_model_data);

  bool Initialize(GameData const & _game_data,
                  core::fs::Directory const & _data_root);

private:
  Ref<Client> client;

  // Renderers
  graphics::ForwardTerrainRenderer terrain_renderer;
  graphics::ForwardSkyboxRenderer skybox_renderer;
  graphics::ForwardStaticModelRenderer static_model_renderer;
  graphics::ForwardAnimatedModelRenderer animated_model_renderer;

  // Resources
  core::UniqueArray<MapObject> maps;
  SkyboxObject skybox;
  container::Array<StaticModelObject> static_models;
  container::Array<AnimatedModelObject> animated_models;
};

}}

#endif
