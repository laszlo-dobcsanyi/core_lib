#ifndef _GAME_CLIENT_CLIENT_HH_
#define _GAME_CLIENT_CLIENT_HH_

#include "game.hh"

#include "graphics/terrain.h"
#include "graphics/skybox.h"
#include "graphics/static_model.h"
#include "graphics/animated_model.h"

#include "shared/object.hpp"
#include "shared/game_data.h"
#include "shared/unique_id.h"
#include "shared/movement.h"

#define LOG_CLIENT(__severity__, ...) LOG_GAME(__severity__, __VA_ARGS__)

////////////////////////////////////////////////////////////////////////////////
// game client
////////////////////////////////////////////////////////////////////////////////

namespace game { namespace client {

using MapObject = Object<graphics::Map>;
using SkyboxObject = Object<graphics::Skybox>;
using StaticModelObject = Object<graphics::StaticModel>;
using AnimatedModelObject = Object<graphics::AnimatedModel>;

class Client;

class Graphics;
class Resources;
class Scene;
class Session;
class World;

class StaticModel;
class AnimatedModel;

class Player;
class Entity;
class Bullet;

}}

#endif
