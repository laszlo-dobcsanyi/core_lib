#ifndef _GAME_CLIENT_WORLD_H_
#define _GAME_CLIENT_WORLD_H_

#include "client/client.hh"
#include "client/entity.h"
#include "client/bullet.h"

////////////////////////////////////////////////////////////////////////////////
// game client
////////////////////////////////////////////////////////////////////////////////

namespace game { namespace client {

/*******************************************************************************
** World
*******************************************************************************/

class World : public ObjectBase<World> {
public:
  Ref<Client> client;

  World() = delete;
  World(Client & _client);
  World(World &&) = default;
  World(World const &) = delete;
  World & operator=(World &&) = default;
  World & operator=(World const &) = delete;
  ~World() = default;

  bool Initialize(Ref<game_data::Map> _map_data);

  void Tick(float _delta_seconds);

  Entity & CreateEntity(Ref<game_data::Entity> _entity_data,
                        UniqueId _unique_id, numerics::Vector3f _position,
                        numerics::Vector3f _direction);
  Entity * GetEntity(UniqueId _entity_unique_id);
  void DestroyEntity(Entity & _entity);

  Bullet & CreateBullet(Ref<game_data::Bullet> _bullet_data,
                        UniqueId _unique_id, numerics::Vector3f _position,
                        numerics::Vector3f _direction);
  Bullet * GetBullet(UniqueId _bullet_unique_id);
  void DestroyBullet(Bullet & _bullet);

  MapObject const & GetMap() const { return **map; }
  container::PoolArray<Entity> const & GetEntities() const { return entities; };

private:
  core::Optional<Ref<MapObject>> map;
  container::PoolArray<Entity> entities;
  container::PoolArray<Bullet> bullets;
};

}}

#endif
