#include "client/client.h"

#include "net/ip/api.h"
#include "net/tcp/connection.h"

////////////////////////////////////////////////////////////////////////////////
// game client
////////////////////////////////////////////////////////////////////////////////

namespace game { namespace client {

/*******************************************************************************
** Client
*******************************************************************************/

Client::Client(GameApplication & _application)
    : application(_application)
    , graphics(*this)
    , resources(*this)
    , world(*this)
    , scene(*this)
    , session(*this)
    , player(world)
    , shutdown_requested(false)
    , shutdown(false) {}

void Client::Launch(ClientLaunchParameters const & _launch_parameters) {
  LOG_CLIENT(STAGE, "Launch..\n");

  if (!graphics.Create()) {
    LOG_CLIENT(ERROR, "Failed create graphics!\n");
    RequestShutdown();
    return;
  }

  graphics.window.OnDestroy.connect(
      graphics::window::DestroyDelegate::Bind(this)
          .Method<&Client::OnWindowDestroy>());

  core::fs::Directory data_directory = game::default_data_directory();
  if (!data_directory.Exists()) {
    LOG_CLIENT(ERROR, "Data directory '%s' does not exists!\n",
               cstr(data_directory));
    return;
  }

  if (!resources.Initialize(application->game_data, move(data_directory))) {
    LOG_CLIENT(ERROR, "Failed load resources!\n");
    RequestShutdown();
    return;
  }

  // TODO: Map
  if (!world.Initialize(application->game_data.maps[0u])) {
    LOG_CLIENT(ERROR, "Failed initialize world!\n");
    RequestShutdown();
    return;
  }

  if (!net::ip::Initialize()) {
    LOG_CLIENT(ERROR, "Failed to initialize net!\n");
    RequestShutdown();
    return;
  }

  if (!session.Create(_launch_parameters.endpoint)) {
    LOG_CLIENT(ERROR, "Failed to create session!\n");
    RequestShutdown();
    return;
  }
}

void Client::Tick(float _delta_seconds) {
  LOG_CLIENT(VERBOSE, "Tick %.6f..\n", _delta_seconds);

  // _delta_seconds /= 4.f;

  {
    session.Tick(_delta_seconds);
    player.Receive();
  }

  {
    graphics.Tick(_delta_seconds);
    // TODO: This should be input.Tick( _delta_seconds );
    {
      // TODO:
      const graphics::Key kForwardKey = graphics::Key::kW;
      const graphics::Key kBackwardKey = graphics::Key::kS;

      float forward_input = 0.f;
      if (graphics.window.GetKeyState(kForwardKey) ==
          graphics::KeyState::Pressed) {
        forward_input = 1.f;
      } else if (graphics.window.GetKeyState(kBackwardKey) ==
                 graphics::KeyState::Pressed) {
        forward_input = -1.f;
      }

      // TODO:
      const graphics::Key kLeftKey = graphics::Key::kA;
      const graphics::Key kRightKey = graphics::Key::kD;

      float side_input = 0.f;
      if (graphics.window.GetKeyState(kLeftKey) ==
          graphics::KeyState::Pressed) {
        side_input = 1.f;
      } else if (graphics.window.GetKeyState(kRightKey) ==
                 graphics::KeyState::Pressed) {
        side_input = -1.f;
      }

      float horizontal_input = 0.f;
      float vertical_input = 0.f;
      if (core::Optional<graphics::WindowPosition> mouse_delta =
              graphics.window.MouseDelta()) {
        horizontal_input = static_cast<float>(mouse_delta->x) / 3.f;
        vertical_input = static_cast<float>(mouse_delta->y) / 3.f;
      }

      bool shoot = false;
      if (graphics.window.GetButtonState(graphics::Button::Left) ==
          graphics::KeyState::Pressed) {
        shoot = true;
      }

      player.SetForwardInput(forward_input);
      player.SetSideInput(side_input);
      player.SetHorizontalInput(horizontal_input);
      player.SetVerticalInput(vertical_input);
      player.SetShootInput(shoot);

#if defined(CORE_LIB_CONFIGURATION_DEBUG)
      if (graphics.window.GetKeyState(graphics::Key::kF1) ==
          graphics::KeyState::Pressed) {
        if (Entity * controlled_entity =
                world.GetEntity(player.controlled_entity_id)) {
          player.SetControlledEntity(*controlled_entity);
        }
      }
      if (graphics.window.GetKeyState(graphics::Key::kF2) ==
          graphics::KeyState::Pressed) {
        player.SetControlledEntity(nullptr);
      }
#endif
    }
    player.Tick(_delta_seconds);
    world.Tick(_delta_seconds);
  }

  {
    if (!graphics.window.IsDestroyed()) {
      graphics.window.Clear();
      scene.Tick(_delta_seconds);
      graphics.window.Present();
    }
  }

  { player.Send(); }
}

void Client::Shutdown() {
  LOG_CLIENT(STAGE, "Shutting down..\n");

  CORE_LIB_ASSERT(!shutdown);
  shutdown = true;

  net::ip::Finalize();
}

void Client::RequestShutdown() {
  LOG_CLIENT(LOG, "Shutdown requested!\n");
  shutdown_requested = true;
}

bool Client::IsShutdownRequested() const { return shutdown_requested; }

bool Client::IsShutdown() const { return shutdown; }

void Client::OnWindowDestroy(graphics::window::DestroyEventArgs const &) {
  LOG_CLIENT(LOG, "Window destroy..\n");
  RequestShutdown();
}

}}
