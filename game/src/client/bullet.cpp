#include "client/bullet.h"
#include "client/world.h"

////////////////////////////////////////////////////////////////////////////////
// game client
////////////////////////////////////////////////////////////////////////////////

namespace game { namespace client {

/*******************************************************************************
** Bullet
*******************************************************************************/

Bullet::Bullet(World & _world, Ref<game_data::Bullet> _bullet_data,
               UniqueId _unique_id)
    : bullet_data(move(_bullet_data))
    , unique_id(_unique_id)
    , world(_world)
    , direction(0.f, 0.f, 0.f) {
  numerics::set_identity(transformation);
}

numerics::Transformation Bullet::GetTransformation() const {
  return transformation;
}

numerics::Vector3f Bullet::GetPosition() const {
  return movement_state.position;
}

void Bullet::SetPosition(numerics::Vector3f _position) {
  movement_state.position = _position;
  numerics::set_translation(transformation, movement_state.position);
}

numerics::Vector3f Bullet::GetDirection() const { return direction; }

void Bullet::SetDirection(numerics::Vector3f _direction) {
  direction = _direction;
  // TODO: Rotation
  numerics::Vector3f mesh_direction = direction;
  mesh_direction[0u] += 90.f;
  numerics::set_rotation(transformation, numerics::to_rotation(mesh_direction));

  numerics::Vector3f forward = numerics::Vector3f(0.f, 0.f, 1.f);
  numerics::Quaternionf rotation = numerics::to_rotation(direction);
  forward = numerics::rotate(forward, rotation);
  SetVelocity(forward * bullet_data->velocity);
}

numerics::Vector3f Bullet::GetVelocity() const {
  return movement_state.velocity;
}

void Bullet::SetVelocity(numerics::Vector3f _velocity) {
  movement_state.velocity = _velocity;
}

void Bullet::Tick(float _delta_seconds) {
  MovementState new_movement = movement_state;
  new_movement.Tick(_delta_seconds);
  movement_state = new_movement;

  SetPosition(movement_state.position);

  static_model->Tick(_delta_seconds, GetTransformation());
}

}}
