#include "client/session.h"
#include "client/client.h"
#include "net/tcp/connection.h"

////////////////////////////////////////////////////////////////////////////////
// game client
////////////////////////////////////////////////////////////////////////////////

namespace game { namespace client {

/*******************************************************************************
** Session
*******************************************************************************/

Session::Session(Client & _client)
    : client(_client)
    , timeout(10.f) {}

bool Session::Create(net::ip::Endpoint _remote_endpoint) {
  if (core::Optional<net::tcp::Connector> connector_result =
          net::tcp::create_connector(net::ip::local_endpoint(),
                                     _remote_endpoint)) {
    LOG_CLIENT(LOG, "Created connector to %s!\n",
               cstr(net::ip::to_string(_remote_endpoint)));
    connector = move(connector_result.Value());
    Connect();
    return true;
  }

  LOG_CLIENT(ERROR, "Failed to create connector to %s!\n",
             cstr(net::ip::to_string(_remote_endpoint)));
  return false;
}

bool Session::IsConnecting() const {
  return connector.IsOpen() && connector.IsConnecting();
}

void Session::Tick(float _delta_seconds) {
  if (IsConnecting()) {
    int32 previous_timeout_seconds = static_cast<int32>(timeout);
    timeout -= _delta_seconds;
    int32 new_timeout_seconds = static_cast<int32>(timeout);
    if (new_timeout_seconds != previous_timeout_seconds) {
      LOG_CLIENT(LOG, "Connection timeout in %" PRIi32 " seconds..\n",
                 previous_timeout_seconds);
    }
    if (timeout <= 0.f) {
      LOG_CLIENT(ERROR, "Connection timed out!\n");
      client->RequestShutdown();
      return;
    }
    Connect();
  }
}

void Session::Connect() {
  if (!connector.IsOpen())
    return;

  if (core::Optional<net::tcp::Connection> connection_result =
          connector.Connect()) {
    LOG_CLIENT(LOG, "Connected to %s!\n",
               cstr(net::ip::to_string(connection_result->RemoteEndpoint())));
    client->player.SetConnection(move(*connection_result));
  }
}

}}
