#ifndef _GAME_GAME_HH_
#define _GAME_GAME_HH_

#include "core/base.h"

#define LOG_GAME(__severity__, ...) CORE_LIB_LOG(__severity__, __VA_ARGS__)

namespace game {

class GameApplication;

}

#endif