#!/bin/bash

for dir in ./core ./demo/ ./experimental/ ./game/ ./graphics/ ./gui/ ./net/ ./test/; do
    pushd "${dir}" &>/dev/null
    echo "Processing ${dir}"
    find . \
         \( -name '*.c' \
         -o -name '*.cpp' \
         -o -name '*.h' \
         -o -name '*.hh' \
         -o -name '*.hpp' \
         -o -name '*.inl' \) \
         -exec clang-format -style=file -i '{}' \;
    popd &>/dev/null
done