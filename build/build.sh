#!/bin/bash
clear

# Variables

arch="x64"
configuration="debug"
compiler="clang"
generator="Ninja"
clean=false
clang_tidy=

# Functions
print_option() {
  printf "%-20s %-20s %-20s\n" "$1" "$2" "$3"
}

print_options() {
  echo "Options:"
  print_option "<option>" "<values>" "<description>"
  print_option "-a, --arch" "[x86, x64]" "set target architecture"
  print_option "-c, --configuration" "[debug, release]" "set build configuration"
  print_option "-C, --compiler" "[gcc, clang]" "set compiler"
  print_option "-g, --generator" "" "set build generator"
  print_option "-t, --tidy" "" "run clang-tidy"
  print_option "--clean" "" "clean build directory"
  print_option "-h, --help" "" "display this help and exit"
}

print_stage() {
  echo "$1"
}

print_stage "~ Parsing.."

while [ "$1" != "" ]; do
  case $1 in
    -a | --arch)
      arch=$2; shift;
      echo "Architecture set to $arch.";
      ;;
    -c | --configuration)
      configuration=$2; shift;
      echo "Configuration set to $configuration.";
      ;;
    -C | --compiler)
      compiler=$2; shift;
      echo "Compiler set to $compiler.";
      ;;
    -g | --generator)
      generator=$2; shift;
      echo "Generator set to $generator.";
      ;;
    -t | --tidy)
      clang_tidy=-DCMAKE_CXX_CLANG_TIDY="$(which clang-tidy);--warnings-as-errors;--checks=-*,bugprone-*,clang-analyzer-*";
      echo "clang-tidy set.";
      ;;
    --clean)
      clean=true;
      echo "Clean set.";
      ;;
    -h | --help)
      print_options
      exit 1
      ;;

    *)
      echo "Invalid argument: $1.";
      print_options
      exit 1;
      ;;
  esac
  shift;
done

print_stage "~ Verifying.."

print_stage "~ Setting up.."

build_directory=$(pwd)"/build/""$configuration"_"$arch"

if [ "$clean" = true ]; then
  if [ -d "$build_directory" ]; then
    rm -r "$build_directory"
  fi
fi

if [ ! -d "$build_directory" ]; then
  mkdir "$build_directory"
fi

toolchain_file=$(pwd)"/cmake/toolchains/""$compiler"_"$arch"".cmake"
third_party_install=$build_directory/install

print_stage "~ Configuring freetype.."
cmake ./third_party/freetype -B "$build_directory"/third_party/freetype -G "$generator" -DCMAKE_TOOLCHAIN_FILE=$toolchain_file -DCMAKE_BUILD_TYPE=$configuration -DCMAKE_INSTALL_PREFIX="$third_party_install" -DWITH_ZLIB=OFF -DWITH_BZip2=OFF -DWITH_PNG=OFF -DWITH_HarfBuzz=OFF
print_stage "~ Building freetype.."
cmake --build "$build_directory"/third_party/freetype --target install --config %configuration%

print_stage "~ Configuring zlib.."
cmake ./third_party/zlib -B "$build_directory"/third_party/zlib -G "$generator" -DCMAKE_TOOLCHAIN_FILE=$toolchain_file -DCMAKE_BUILD_TYPE=$configuration -DCMAKE_INSTALL_PREFIX="$third_party_install"
print_stage "~ Building zlib.."
cmake --build "$build_directory"/third_party/zlib --target install --config %configuration%

print_stage "~ Configuring core_lib.."
cmake ./ -B "$build_directory"/build -G "$generator" -DCMAKE_TOOLCHAIN_FILE=$toolchain_file -DCMAKE_BUILD_TYPE=$configuration -DCMAKE_INSTALL_PREFIX="$build_directory" -DTHIRD_PARTY_INSTALL_DIR="$third_party_install" -DCMAKE_EXPORT_COMPILE_COMMANDS=ON "$clang_tidy" -DGRAPHICS_OPENGL=ON
print_stage "~ Building core_lib.."
cmake --build "$build_directory"/build --target install --config %configuration%
