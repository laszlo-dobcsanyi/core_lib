#version 330 core
precision highp float;
precision highp sampler2D;

in vec2 texture_coordinate;

uniform float time;
// uniform float time_delta;
uniform vec2 resolution;
uniform sampler2D texture1;

out vec4 color;

float mag2(vec2 p) {
  return dot(p,p);
}

// TODO: Copy
vec2 point1(float t) {
    t *= 0.62;
    return vec2(0.12,0.5 + sin(t)*0.2);
}

// TODO: Copy
vec2 point2(float t) {
    t *= 0.62;
    return vec2(0.88,0.5 + cos(t + 1.5708)*0.2);
}

void main() {
  if (time < 0.5f) {
      color = vec4(0.5f, 0.f, 0.f, 0.f);
  } else {
    const float K = 0.2;
    const float v = 0.55;
    const float kVorticityAmount = 0.03f;
    // TODO: Copy
    const float kdt = 0.15f;

    vec2 uv = gl_FragCoord.xy / resolution;
    vec2 w = 1.0f / resolution;

    vec4 data = textureLod(texture1, uv, 0.0);
    vec4 tr = textureLod(texture1, uv + vec2(w.x , 0), 0.0);
    vec4 tl = textureLod(texture1, uv - vec2(w.x , 0), 0.0);
    vec4 tu = textureLod(texture1, uv + vec2(0 , w.y), 0.0);
    vec4 td = textureLod(texture1, uv - vec2(0 , w.y), 0.0);

    vec3 dx = (tr.xyz - tl.xyz)*0.5;
    vec3 dy = (tu.xyz - td.xyz)*0.5;
    vec2 densDif = vec2(dx.z ,dy.z);

    // Density
    data.z -= kdt*dot(vec3(densDif, dx.x + dy.y) ,data.xyz);

    vec2 laplacian = tu.xy + td.xy + tr.xy + tl.xy - 4.0*data.xy;
    vec2 viscForce = vec2(v)*laplacian;

    // Advection
    data.xyw = textureLod(texture1, uv - kdt*data.xy*w, 0.).xyw;

    // Force
    vec2 newForce = vec2(0);
    newForce.xy += 0.75*vec2(.0003, 0.00015)/(mag2(uv-point1(time))+0.0001);
    newForce.xy -= 0.75*vec2(.0003, 0.00015)/(mag2(uv-point2(time))+0.0001);

    // Update velocity
    data.xy += kdt*(viscForce.xy - K/kdt*densDif + newForce);

    // Linear velocity decay
    data.xy = max(vec2(0), abs(data.xy)-1e-4)*sign(data.xy);

    // Vorticity confinement
   	data.w = (tr.y - tl.y - tu.x + td.x);
    vec2 vort = vec2(abs(tu.w) - abs(td.w), abs(tl.w) - abs(tr.w));
    vort *= kVorticityAmount / length(vort + 1e-9) * data.w;
    data.xy += vort;

    // Boundaries
    data.y *= smoothstep(.5,.48,abs(uv.y-0.5));

    data = clamp(data, vec4(vec2(-10), 0.5 , -10.), vec4(vec2(10), 3.0 , 10.));

    color = data;
  }
}