@ECHO OFF
CLS

CALL "build.bat" -a x86 -c Debug %* || (
  ECHO ! Failed to build Debug_x86..
  EXIT /B 1
)

CALL "build.bat" -a x64 -c Debug %* || (
  ECHO ! Failed to build Debug_x64..
  EXIT /B 1
)

CALL "build.bat" -a x86 -c Release %* || (
  ECHO ! Failed to build Release_x86..
  EXIT /B 1
)

CALL "build.bat" -a x64 -c Release %* || (
  ECHO ! Failed to build Release_x64..
  EXIT /B 1
)