@ECHO OFF
CLS
SETLOCAL EnableDelayedExpansion

SET "VsDevCmd="C:\Program Files (x86)\Microsoft Visual Studio\2017\BuildTools\Common7\Tools\VsDevCmd.bat""

SET "arch="
SET "configuration="
SET "compiler=msvc"
SET "generator="Ninja""
SET "clean="

:parse
  ECHO ~ Parsing..

  :loop
  IF NOT "%1"=="" (
    IF "%1"=="-a" ( SET "arch=%2" & SHIFT & GOTO :next )
    IF "%1"=="--arch" ( SET "arch=%2" & SHIFT & GOTO :next )

    IF "%1"=="-c" ( SET "configuration=%2" & SHIFT & GOTO :next )
    IF "%1"=="--configuration" ( SET "configuration=%2" & SHIFT & GOTO :next )

    IF "%1"=="-C" ( SET "compiler=%2" & SHIFT & GOTO :next )
    IF "%1"=="--compiler" ( SET "compiler=%2" & SHIFT & GOTO :next )

    IF "%1"=="-g" ( SET "generator=%2" & SHIFT & GOTO :next )
    IF "%1"=="--generator" ( SET "generator=%2" & SHIFT & GOTO :next )

    IF "%1"=="--clean" ( SET "clean=y" & GOTO :next )

    ECHO Unknown argument: "%1%"
    EXIT /B 1

  :next
    SHIFT
    GOTO :loop
  )
:end_parse

:verify
  ECHO ~ Verifying..

  IF NOT DEFINED arch (
    ECHO arch is not defined!
    EXIT /B 1
  )

  IF NOT DEFINED configuration (
    ECHO configuration is not defined!
    EXIT /B 1
  )
:end_verify

:setup
  ECHO ~ Setting up..

  SET "build_directory=%configuration%_%arch%"

  IF DEFINED clean (
    RMDIR /S /Q "%build_directory%"
  )
  IF NOT EXIST "%build_directory%" (
    MKDIR "%build_directory%"
  )

  IF NOT DEFINED generator (
    IF "%arch%"=="x86" (
      SET "generator="Visual Studio 15 2017""
    )
    IF "%arch%"=="x64" (
      SET "generator="Visual Studio 15 2017 Win64""
    )
  )

  SET "cmake_options="

  IF %generator%=="Ninja" (
    ECHO ~ Setting up environment for Ninja..
    CALL %VsDevCmd% -arch=%arch% -no_logo
  )

  SET "third_party_install=%~dp0%build_directory%/build/install"
:end_setup

ECHO ~ Configuring freetype..
cmake ./../third_party/freetype -B./"%build_directory%"/build/third_party/freetype -G %generator% -DCMAKE_BUILD_TYPE=%configuration% -DCMAKE_INSTALL_PREFIX="%third_party_install%" -DWITH_ZLIB=OFF -DWITH_BZip2=OFF -DWITH_PNG=OFF -DWITH_HarfBuzz=OFF || (
  ECHO ! Failed to configure freetype..
  EXIT /B 1
)
ECHO ~ Building freetype..
cmake --build ./"%build_directory%"/build/third_party/freetype --target install --config %configuration% || (
  ECHO ! Failed to build freetype..
  EXIT /B 1
)

ECHO ~ Configuring zlib..
cmake ./../third_party/zlib -B./"%build_directory%"/build/third_party/zlib -G %generator% -DCMAKE_BUILD_TYPE=%configuration% -DCMAKE_INSTALL_PREFIX="%third_party_install%" || (
  ECHO ! Failed to configure zlib..
  EXIT /B 1
)
ECHO ~ Building zlib..
cmake --build ./"%build_directory%"/build/third_party/zlib --target install --config %configuration% || (
  ECHO ! Failed to build zlib..
  EXIT /B 1
)

ECHO ~ Configuring core_lib..
cmake ./../ -B./"%build_directory%"/build/ -G %generator% -DCMAKE_BUILD_TYPE=%configuration% -DCMAKE_INSTALL_PREFIX="%build_directory%" -DTHIRD_PARTY_INSTALL_DIR="%third_party_install%" -DGRAPHICS_OPENGL=ON || (
  ECHO ! Failed to configure core_lib..
  EXIT /B 1
)
ECHO ~ Building core_lib..
cmake --build ./"%build_directory%"/build/ --target install --config %configuration% || (
  ECHO ! Failed to build core_lib..
  EXIT /B 1
)
