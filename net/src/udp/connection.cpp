#include "net/udp/connection.h"

////////////////////////////////////////////////////////////////////////////////
// net udp
////////////////////////////////////////////////////////////////////////////////

namespace net { namespace udp {

/*******************************************************************************
** Connection
*******************************************************************************/

Connection::Connection(ip::Socket _socket, ip::Endpoint _remote_endpoint)
    : ip::Socket(move(_socket))
    , remote_endpoint(_remote_endpoint) {}

ip::Endpoint Connection::GetRemoteEndpoint() const { return remote_endpoint; }

}}