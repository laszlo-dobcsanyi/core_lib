#include "net/tcp/connection.h"

////////////////////////////////////////////////////////////////////////////////
// net tcp
////////////////////////////////////////////////////////////////////////////////

namespace net { namespace tcp {

/*******************************************************************************
** Connection
*******************************************************************************/

Connection::Connection(ip::Socket _socket, ip::Endpoint _remote_endpoint)
    : socket(move(_socket))
    , remote_endpoint(_remote_endpoint) {}

Connection::~Connection() { Close(); }

bool Connection::Send(core::span<byte const> _payload) {
  if (core::Optional<ip::SocketError> send_error = socket.Send(_payload)) {
    LOG_NET(ERROR, "Socket error!\n");
    Close();
    return false;
  }
  return true;
}

core::span<byte const> Connection::Receive(core::span<byte> _buffer) {
  core::Variant<core::span<byte const>, ip::SocketError> receive_result =
      socket.Receive(_buffer);
  if (receive_result.Is<ip::SocketError>()) {
    Close();
    return core::make_span<byte const>(nullptr, 0);
  }
  return receive_result.As<core::span<byte const>>();
}

void Connection::Close() {
  if (IsOpen()) {
    LOG_NET(MESSAGE, "Closing..\n");
    ip::shutdown(socket.GetHandle());
    socket.Close();
  }
}

}}