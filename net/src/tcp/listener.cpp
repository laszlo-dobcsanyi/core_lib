#include "net/tcp/listener.h"

#include "net/ip/socket_error.h"
#include "net/ip/socket_handle.h"
#include "net/tcp/connection.h"

////////////////////////////////////////////////////////////////////////////////
// net tcp
////////////////////////////////////////////////////////////////////////////////

namespace net { namespace tcp {

/*******************************************************************************
** Listener
*******************************************************************************/

Listener::Listener(ip::Socket _socket)
    : socket(move(_socket)) {
  timeout.tv_sec = 0;
  timeout.tv_usec = 0;
}

bool Listener::IsOpen() const { return socket.IsOpen(); }
core::Optional<Connection> Listener::Accept() {
  CORE_LIB_ASSERT(IsOpen());

  for (;;) {
    FD_ZERO(&read_set);
    FD_SET(socket.GetHandle(), &read_set);
    // TODO: What is this sockets?
    int sockets = static_cast<int>(socket.GetHandle() + 1);
    int result = ::select(sockets, &read_set, nullptr, nullptr, &timeout);
    if (result == SOCKET_ERROR) {
      ip::SocketError error = ip::get_last_socket_error();
      LOG_NET(ERROR, "::select error: '%s'!\n", cstr(error.Message()));
      Close();
      return core::nullopt;
    }

    if (result == 0) {
      LOG_NET(VERBOSE, "No connection!\n");
      return core::nullopt;
    }

    ip::Endpoint remote_endpoint;
    socklen_t remote_endpoint_size = sizeof(remote_endpoint);
    ip::SocketHandle remote_socket_handle =
        ::accept(socket.GetHandle(), (sockaddr *)&remote_endpoint,
                 &remote_endpoint_size);
    if (!remote_socket_handle.HasValue()) {
      LOG_NET(VERBOSE, "No socket handle!\n");
      continue;
    }

    return core::Optional<Connection>(ip::Socket(remote_socket_handle),
                                      remote_endpoint);
  }
}

void Listener::Close() { socket.Close(); }

/*******************************************************************************
** create_listener
*******************************************************************************/

core::Optional<Listener> create_listener(ip::Endpoint _local_endpoint) {
  core::Optional<ip::SocketHandle> socket_handle = ip::open_tcp();
  if (!socket_handle.HasValue())
    return core::nullopt;

  ip::Socket socket(*socket_handle);
  if (core::Optional<ip::SocketError> error =
          ip::set_non_blocking(*socket_handle))
    return core::nullopt;
  if (core::Optional<ip::SocketError> error =
          ip::set_reusable_address(*socket_handle))
    return core::nullopt;
  if (core::Optional<ip::SocketError> error = ip::set_no_delay(*socket_handle))
    return core::nullopt;
  if (core::Optional<ip::SocketError> error =
          ip::bind(*socket_handle, _local_endpoint))
    return core::nullopt;
  if (core::Optional<ip::SocketError> error = ip::listen(*socket_handle))
    return core::nullopt;

  return core::Optional<Listener>(move(socket));
}

}}