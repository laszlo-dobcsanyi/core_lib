#include "net/tcp/connector.h"
#include "net/tcp/connection.h"

////////////////////////////////////////////////////////////////////////////////
// net tcp
////////////////////////////////////////////////////////////////////////////////

namespace net { namespace tcp {

/*******************************************************************************
** Connector
*******************************************************************************/

Connector::Connector(ip::Socket _socket, ip::Endpoint _remote_endpoint)
    : socket(move(_socket))
    , remote_endpoint(_remote_endpoint) {}

bool Connector::IsOpen() const { return socket.IsOpen(); }

bool Connector::IsConnecting() const { return connecting; }

core::Optional<Connection> Connector::Connect() {
  CORE_LIB_ASSERT(IsOpen());

  if (!IsConnecting()) {
    int connect_result =
        ::connect(socket.GetHandle(), (sockaddr *)&remote_endpoint,
                  sizeof(remote_endpoint));
    if (connect_result != SOCKET_ERROR) {
      return core::Optional<Connection>(move(socket), remote_endpoint);
    }

    ip::SocketError connect_error = ip::get_last_socket_error();
    if (connect_error.ErrorCode() != SOCKET_ERROR_WOULD_BLOCK &&
        connect_error.ErrorCode() != SOCKET_ERROR_IN_PROGRESS) {
      LOG_NET(ERROR, "::connect error: '%s'!\n", cstr(connect_error.Message()));
      Close();
      return core::nullopt;
    }

    connecting = true;

    timeout.tv_sec = 0;
    timeout.tv_usec = 0;
  }

  FD_ZERO(&write_set);
  FD_SET(socket.GetHandle(), &write_set);
  // TODO: What is this sockets?
  int sockets = static_cast<int>(socket.GetHandle() + 1);
  int select_result = ::select(sockets, NULL, &write_set, NULL, &timeout);
  if (select_result == SOCKET_ERROR) {
    ip::SocketError select_error = ip::get_last_socket_error();
#if defined(CORE_LIB_OS_WINDOWS)
    if (select_error.ErrorCode() == WSAEALREADY ||
        select_error.ErrorCode() == WSAEINVAL) {
      return core::nullopt;
    }
#endif
    LOG_NET(ERROR, "::select error: '%s'!\n", cstr(select_error.Message()));
    connecting = false;
    Close();
    return core::nullopt;
  }

  if (select_result == 0) {
    LOG_NET(VERBOSE, "Connection still pending..\n");
    return core::nullopt;
  }

  int socket_error_code;
  socklen_t socket_error_code_size = sizeof(socket_error_code);
  int getsockopt_result =
      ::getsockopt(socket.GetHandle(), SOL_SOCKET, SO_ERROR,
                   (char *)&socket_error_code, &socket_error_code_size);
  if (getsockopt_result == SOCKET_ERROR) {
    ip::SocketError getsockopt_error = ip::get_last_socket_error();
    LOG_NET(ERROR, "::getsockopt error: '%s'!\n",
            cstr(getsockopt_error.Message()));
    connecting = false;
    Close();
    return core::nullopt;
  }

  if (socket_error_code) {
    ip::SocketError socket_error = ip::SocketError(socket_error_code);
    LOG_NET(ERROR, "Socket error: '%s'!\n", cstr(socket_error.Message()));
    connecting = false;
    Close();
    return core::nullopt;
  }

  connecting = false;
  return core::Optional<Connection>(move(socket), remote_endpoint);
}

void Connector::Close() { socket.Close(); }

/*******************************************************************************
** create_connector
*******************************************************************************/

core::Optional<Connector> create_connector(ip::Endpoint _local_endpoint,
                                           ip::Endpoint _remote_endpoint) {
  core::Optional<ip::SocketHandle> socket_handle = ip::open_tcp();
  if (!socket_handle.HasValue())
    return core::nullopt;

  ip::Socket socket(*socket_handle);
  if (core::Optional<ip::SocketError> error =
          ip::set_non_blocking(*socket_handle))
    return core::nullopt;
  if (core::Optional<ip::SocketError> error =
          ip::set_reusable_address(*socket_handle))
    return core::nullopt;
  if (core::Optional<ip::SocketError> error = ip::set_no_delay(*socket_handle))
    return core::nullopt;
  return core::Optional<Connector>(move(socket), _remote_endpoint);
}

}}