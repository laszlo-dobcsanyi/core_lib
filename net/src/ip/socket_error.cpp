#include "net/ip/socket_error.h"

////////////////////////////////////////////////////////////////////////////////
// net ip
////////////////////////////////////////////////////////////////////////////////

namespace net { namespace ip {

/*******************************************************************************
** SocketError
*******************************************************************************/

SocketError::SocketError(SocketErrorCode _error_code)
    : error_code(_error_code) {}

SocketErrorCode SocketError::ErrorCode() const { return error_code; }

const_string const & SocketError::Message() const {
  if (!message) {
#if defined(CORE_LIB_OS_WINDOWS)
    using Storage = typename const_string::storage_type;
    const size_t kMessageSize = 256u;
    Storage storage(kMessageSize);
    ::memset(storage.Get(), 0, kMessageSize);
    uint32 stored = ::FormatMessage(
        FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS, nullptr,
        error_code, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), storage.Get(),
        kMessageSize, nullptr);
    if (1 < stored) {
      char * cstring = storage.Get() + stored - 2;
      if (*cstring == '\r') {
        *cstring = '\0';
      }
    }
    message = const_string(move(storage));
#elif defined(CORE_LIB_OS_LINUX)
    message = const_string(::strerror(error_code));
#endif
  }
  return message;
}

/*******************************************************************************
** get_last_socket_error
*******************************************************************************/

SocketError get_last_socket_error() {
#if defined(CORE_LIB_OS_WINDOWS)
  return SocketError(::WSAGetLastError());
#elif defined(CORE_LIB_OS_LINUX)
  return SocketError(errno);
#endif
}

}}