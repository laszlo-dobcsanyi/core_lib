#include "net/ip/socket.h"

////////////////////////////////////////////////////////////////////////////////
// net ip
////////////////////////////////////////////////////////////////////////////////

namespace net { namespace ip {

/*******************************************************************************
** Socket
*******************************************************************************/

Socket::Socket(SocketHandle _socket_handle)
    : socket_handle(_socket_handle) {}

Socket::~Socket() { Close(); }

SocketHandle Socket::GetHandle() const { return socket_handle; }

bool Socket::IsOpen() const { return socket_handle.HasValue(); }

core::Optional<SocketError> Socket::Send(core::span<const byte> _payload) {
  CORE_LIB_ASSERT(!_payload.empty());
  CORE_LIB_ASSERT(IsOpen());

  int sent_bytes = ::send(socket_handle, (char const *)_payload.data(),
                          (int)_payload.size(), 0);
  if (sent_bytes == SOCKET_ERROR) {
    SocketError send_error = get_last_socket_error();
    LOG_NET(ERROR, "::send error: '%s'\n", cstr(send_error.Message()));
    return move(send_error);
  }

  CORE_LIB_ASSERT((size_t)sent_bytes == _payload.size());
  return core::nullopt;
}

core::Variant<core::span<byte const>, SocketError>
Socket::Receive(core::span<byte> _buffer) {
  using ResultType = core::Variant<core::span<byte const>, SocketError>;
  CORE_LIB_ASSERT(!_buffer.empty());
  CORE_LIB_ASSERT(IsOpen());

  int received_bytes =
      ::recv(socket_handle, (char *)_buffer.data(), (int)_buffer.size(), 0);
  if (received_bytes == SOCKET_ERROR) {
    SocketError recv_error = get_last_socket_error();
    if (recv_error.ErrorCode() == SOCKET_ERROR_WOULD_BLOCK) {
      LOG_NET(VERBOSE, "Receiving would block..\n");
      return ResultType(in_place_t<core::span<byte const>>(),
                        core::make_span<byte const>(nullptr, 0));
    }

    LOG_NET(ERROR, "::recv error: '%s'\n", cstr(recv_error.Message()));
    return ResultType(in_place_t<SocketError>(), move(recv_error));
  }

  return ResultType(
      in_place_t<core::span<byte const>>(),
      core::make_span<byte const>(_buffer.data(), received_bytes));
}

void Socket::Close() {
  if (IsOpen()) {
    LOG_NET(VERBOSE, "Closing..\n");
    close(socket_handle);
    socket_handle.Reset();
  }
}

}}