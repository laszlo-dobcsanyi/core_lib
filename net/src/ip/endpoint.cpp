#include "net/ip/endpoint.h"

////////////////////////////////////////////////////////////////////////////////
// net ip
////////////////////////////////////////////////////////////////////////////////

namespace net { namespace ip {

/*******************************************************************************
** Endpoint
*******************************************************************************/

Endpoint make_endpoint(uint8 _a, uint8 _b, uint8 _c, uint8 _d) {
  return make_endpoint(_a, _b, _c, _d, 0);
}

Endpoint make_endpoint(uint8 _a, uint8 _b, uint8 _c, uint8 _d, uint16 _port) {
  Endpoint endpoint;
  // ::memset( &endpoint, 0, sizeof( endpoint ) );
  endpoint.sin_family = AF_INET;
  endpoint.sin_addr.s_addr =
      htobe(U32((_a << 24) | (_b << 16) | (_c << 8) | (_d << 0)));
  endpoint.sin_port = htobe(_port);
  return endpoint;
}

Endpoint local_endpoint() { return local_endpoint(0); }

Endpoint local_endpoint(uint16 _port) {
  return make_endpoint(127, 0, 0, 1, _port);
}

const_string to_string(Endpoint _endpoint) {
  using Storage = typename const_string::storage_type;
  static const size_t kMaxSize = 22;
  Storage storage(kMaxSize);
  std::snprintf(
      storage.Get(), kMaxSize,
      "%" PRIu8 ".%" PRIu8 ".%" PRIu8 ".%" PRIu8 ":%" PRIu16
#if defined(CORE_LIB_OS_WINDOWS)
      ,
      _endpoint.sin_addr.S_un.S_un_b.s_b1, _endpoint.sin_addr.S_un.S_un_b.s_b2,
      _endpoint.sin_addr.S_un.S_un_b.s_b3, _endpoint.sin_addr.S_un.S_un_b.s_b4
#elif defined(CORE_LIB_OS_LINUX)
      ,
      (_endpoint.sin_addr.s_addr & U32(0x000000FF)) >> 0u,
      (_endpoint.sin_addr.s_addr & U32(0x0000FF00)) >> 8u,
      (_endpoint.sin_addr.s_addr & U32(0x00FF0000)) >> 16u,
      (_endpoint.sin_addr.s_addr & U32(0xFF000000)) >> 24u
#else
#  error
#endif
      ,
      betoh(_endpoint.sin_port));
  return const_string(move(storage));
}

core::Optional<Endpoint> from_string(const_string const & _const_string) {
  char const * current = cstr(_const_string);
  uint8 a = 0;
  if (size_t parsed = cstring::parse_decimal(current, a)) {
    current += parsed;
  } else {
    return core::nullopt;
  }
  if (*current == '.') {
    ++current;
  } else {
    return core::nullopt;
  }
  uint8 b = 0;
  if (size_t parsed = cstring::parse_decimal(current, b)) {
    current += parsed;
  } else {
    return core::nullopt;
  }
  if (*current == '.') {
    ++current;
  } else {
    return core::nullopt;
  }
  uint8 c = 0;
  if (size_t parsed = cstring::parse_decimal(current, c)) {
    current += parsed;
  } else {
    return core::nullopt;
  }
  if (*current == '.') {
    ++current;
  } else {
    return core::nullopt;
  }
  uint8 d = 0;
  if (size_t parsed = cstring::parse_decimal(current, d)) {
    current += parsed;
  } else {
    return core::nullopt;
  }
  if (*current == ':') {
    ++current;
  } else {
    return core::nullopt;
  }
  uint16 port = 0;
  if (size_t parsed = cstring::parse_decimal(current, port)) {
    // current += parsed;
  } else {
    return core::nullopt;
  }
  return make_endpoint(a, b, c, d, port);
}

}}
