#include "net/ip/socket_handle.h"

////////////////////////////////////////////////////////////////////////////////
// net ip
////////////////////////////////////////////////////////////////////////////////

namespace net { namespace ip {

/*******************************************************************************
** open_tcp
*******************************************************************************/

core::Optional<SocketHandle> open_tcp() {
  SocketHandle result = ::socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
  if (!result.HasValue()) {
    SocketError socket_error = get_last_socket_error();
    LOG_NET(ERROR, "::socket error: '%s'!\n", cstr(socket_error.Message()));
    return core::nullopt;
  }
  return result;
}

/*******************************************************************************
** open_udp
*******************************************************************************/

core::Optional<SocketHandle> open_udp() {
  SocketHandle result = ::socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
  if (!result.HasValue()) {
    SocketError socket_error = get_last_socket_error();
    LOG_NET(ERROR, "::socket error: '%s'!\n", cstr(socket_error.Message()));
    return core::nullopt;
  }
  return result;
}

/*******************************************************************************
** set_non_blocking
*******************************************************************************/

core::Optional<SocketError> set_non_blocking(SocketHandle _socket_handle) {
  int result;
#if defined(CORE_LIB_OS_LINUX)
  result = ::fcntl(_socket_handle, F_SETFL, O_NONBLOCK, (socklen_t)1);
#elif defined(CORE_LIB_OS_WINDOWS)
  DWORD non_blocking = 1;
  result = ::ioctlsocket(_socket_handle, FIONBIO, &non_blocking);
#endif
  if (result == SOCKET_ERROR) {
    SocketError error = get_last_socket_error();
    LOG_NET(ERROR, "::fcntl/::ioctlsocket error: '%s'!\n",
            cstr(error.Message()));
    return error;
  }
  return core::nullopt;
}

/*******************************************************************************
** set_reusable_address
*******************************************************************************/

core::Optional<SocketError> set_reusable_address(SocketHandle _socket_handle) {
  socklen_t reuse_address = 1;
  int setsockopt_result =
      ::setsockopt(_socket_handle, SOL_SOCKET, SO_REUSEADDR,
                   (char *)&reuse_address, sizeof(reuse_address));
  if (setsockopt_result == SOCKET_ERROR) {
    SocketError setsockopt_error = get_last_socket_error();
    LOG_NET(ERROR, "::setsockopt error: '%s'!\n",
            cstr(setsockopt_error.Message()));
    return setsockopt_error;
  }
  return core::nullopt;
}

/*******************************************************************************
** set_no_delay
*******************************************************************************/

core::Optional<SocketError> set_no_delay(SocketHandle _socket_handle) {
  socklen_t no_delay = 1;
  int setsockopt_result = ::setsockopt(_socket_handle, IPPROTO_TCP, TCP_NODELAY,
                                       (char *)&no_delay, sizeof(no_delay));
  if (setsockopt_result == SOCKET_ERROR) {
    SocketError setsockopt_error = get_last_socket_error();
    LOG_NET(ERROR, "::setsockopt error: '%s'!\n",
            cstr(setsockopt_error.Message()));
    return setsockopt_error;
  }
  return core::nullopt;
}

/*******************************************************************************
** bind
*******************************************************************************/

core::Optional<SocketError> bind(SocketHandle _socket_handle,
                                 Endpoint _local_endpoint) {
  int bind_result = ::bind(_socket_handle, (sockaddr const *)&_local_endpoint,
                           sizeof(_local_endpoint));
  if (bind_result == SOCKET_ERROR) {
    SocketError bind_error = get_last_socket_error();
    LOG_NET(ERROR, "::bind error: '%s'\n", cstr(bind_error.Message()));
    return bind_error;
  }
  return core::nullopt;
}

/*******************************************************************************
** listen
*******************************************************************************/

core::Optional<SocketError> listen(SocketHandle _socket_handle) {
  int listen_result = ::listen(_socket_handle, SOMAXCONN);
  if (listen_result == SOCKET_ERROR) {
    SocketError listen_error = get_last_socket_error();
    LOG_NET(ERROR, "::listen error: '%s'\n", cstr(listen_error.Message()));
    return listen_error;
  }
  return core::nullopt;
}

/*******************************************************************************
** connect
*******************************************************************************/

core::Optional<SocketError> connect(SocketHandle _socket_handle,
                                    Endpoint _remote_endpoint) {
  int connect_result =
      ::connect(_socket_handle, (sockaddr const *)&_remote_endpoint,
                sizeof(_remote_endpoint));
  if (connect_result == SOCKET_ERROR) {
    SocketError connect_error = get_last_socket_error();
    LOG_NET(ERROR, "::connect error: '%s'\n", cstr(connect_error.Message()));
    return connect_error;
  }
  return core::nullopt;
}

/*******************************************************************************
** shutdown
*******************************************************************************/

core::Optional<SocketError> shutdown(SocketHandle _socket_handle) {
  int shutdown_result = ::shutdown(_socket_handle, SD_BOTH);
  if (shutdown_result == SOCKET_ERROR) {
    SocketError shutdown_error = get_last_socket_error();
    LOG_NET(ERROR, "::shutdown error: '%s'!\n", cstr(shutdown_error.Message()));
    return shutdown_error;
  }
  return core::nullopt;
}

/*******************************************************************************
** close
*******************************************************************************/

core::Optional<SocketError> close(SocketHandle _socket_handle) {
#if defined(CORE_LIB_OS_WINDOWS)
  ::closesocket(_socket_handle);
#elif defined(CORE_LIB_OS_LINUX)
  ::close(_socket_handle);
#endif
  return core::nullopt;
}

}}