#include "net/ip/api.h"
#include "net/ip/socket_error.h"

#if defined(CORE_LIB_OS_LINUX)
#  include <signal.h>
#endif

#if defined(CORE_LIB_OS_LINUX)
void sigpipe_handler(int _signal_num) {
  LOG_NET(MESSAGE, "Broken pipe received!\n");
}
#endif

////////////////////////////////////////////////////////////////////////////////
// net ip
////////////////////////////////////////////////////////////////////////////////

namespace net { namespace ip {

bool Initialize() {
  LOG_NET(STAGE, "Initializing..\n");
#if defined(CORE_LIB_OS_WINDOWS)
  WSADATA WsaData;
  int result = ::WSAStartup(MAKEWORD(2, 2), &WsaData);
  if (result != NO_ERROR) {
    SocketError wsa_startup_error(result);
    LOG_NET(ERROR, "WSAStartup failed: %d!\n",
            cstr(wsa_startup_error.Message()));
    return false;
  }
#elif defined(CORE_LIB_OS_LINUX)
  // ::sigaction( SIGPIPE, &(struct sigaction){SIG_IGN}, nullptr );
  ::signal(SIGPIPE, sigpipe_handler);
#endif
  LOG_NET(VERBOSE, "Initialized!\n");
  return true;
}

void Finalize() {
  LOG_NET(STAGE, "Finalizing..\n");
#if defined(CORE_LIB_OS_WINDOWS)
  ::WSACleanup();
#endif
  LOG_NET(VERBOSE, "Finalized!\n");
}

}}
