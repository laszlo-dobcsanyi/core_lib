#ifndef _CORE_NET_UDP_CONNECTION_H_
#define _CORE_NET_UDP_CONNECTION_H_

#include "net/net.hh"
#include "net/ip/socket.h"

////////////////////////////////////////////////////////////////////////////////
// net udp
////////////////////////////////////////////////////////////////////////////////

namespace net { namespace udp {

/*******************************************************************************
** Connection
*******************************************************************************/

class Connection : public ip::Socket {
public:
  Connection() = default;
  Connection(ip::Socket _socket, ip::Endpoint _remote_endpoint);
  Connection(Connection &&) = default;
  Connection(Connection const &) = delete;
  Connection & operator=(Connection &&) = default;
  Connection & operator=(Connection const &) = delete;
  ~Connection() = default;

  ip::Endpoint GetRemoteEndpoint() const;

private:
  ip::Endpoint remote_endpoint;
};

}}

#endif