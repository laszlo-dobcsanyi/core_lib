#ifndef _CORE_NET_TCP_CONNECTOR_H_
#define _CORE_NET_TCP_CONNECTOR_H_

#include "net/net.hh"
#include "net/ip/socket.h"

////////////////////////////////////////////////////////////////////////////////
// net tcp
////////////////////////////////////////////////////////////////////////////////

namespace net { namespace tcp {

/*******************************************************************************
** Connector
*******************************************************************************/

class Connector {
public:
  Connector() = default;
  Connector(ip::Socket _socket, ip::Endpoint _remote_endpoint);
  Connector(Connector &&) = default;
  Connector(Connector const &) = delete;
  Connector & operator=(Connector &&) = default;
  Connector & operator=(Connector const &) = delete;
  ~Connector() = default;

  bool IsOpen() const;

  bool IsConnecting() const;

  core::Optional<Connection> Connect();

  void Close();

private:
  ip::Socket socket;
  ip::Endpoint remote_endpoint;
  fd_set write_set;
  timeval timeout;
  bool connecting = false;
};

/*******************************************************************************
** create_connector
*******************************************************************************/

core::Optional<Connector> create_connector(ip::Endpoint _local_endpoint,
                                           ip::Endpoint _remote_endpoint);

}}

#endif