#ifndef _CORE_NET_TCP_CONNECTION_H_
#define _CORE_NET_TCP_CONNECTION_H_

#include "core/ranges/span.hpp"

#include "net/net.hh"
#include "net/ip/socket.h"

////////////////////////////////////////////////////////////////////////////////
// net tcp
////////////////////////////////////////////////////////////////////////////////

namespace net { namespace tcp {

/*******************************************************************************
** Connection
*******************************************************************************/

class Connection {
public:
  Connection() = default;
  Connection(ip::Socket _socket, ip::Endpoint _remote_endpoint);
  Connection(Connection &&) = default;
  Connection(Connection const &) = delete;
  Connection & operator=(Connection &&) = default;
  Connection & operator=(Connection const &) = delete;
  ~Connection();

  ip::Endpoint RemoteEndpoint() const { return remote_endpoint; }

  ip::SocketHandle GetHandle() const { return socket.GetHandle(); }

  bool IsOpen() const { return socket.IsOpen(); }

  bool Send(core::span<byte const> _payload);

  core::span<byte const> Receive(core::span<byte> _buffer);

  void Close();

private:
  ip::Socket socket;
  ip::Endpoint remote_endpoint;
};

}}

#endif