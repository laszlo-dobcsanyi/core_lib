#ifndef _CORE_NET_TCP_LISTENER_H_
#define _CORE_NET_TCP_LISTENER_H_

#include "net/net.hh"
#include "net/ip/socket.h"

////////////////////////////////////////////////////////////////////////////////
// net tcp
////////////////////////////////////////////////////////////////////////////////

namespace net { namespace tcp {

/*******************************************************************************
** Listener
*******************************************************************************/

class Listener {
public:
  Listener() = default;
  Listener(ip::Socket _socket);
  Listener(Listener &&) = default;
  Listener(Listener const &) = delete;
  Listener & operator=(Listener &&) = default;
  Listener & operator=(Listener const &) = delete;
  ~Listener() = default;

  bool IsOpen() const;

  core::Optional<Connection> Accept();

  void Close();

private:
  ip::Socket socket;
  fd_set read_set;
  timeval timeout;
};

/*******************************************************************************
** create_listener
*******************************************************************************/

core::Optional<Listener> create_listener(ip::Endpoint _local_endpoint);

}}

#endif