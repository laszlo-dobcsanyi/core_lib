#ifndef _CORE_NET_IP_SOCKET_HANDLE_H_
#define _CORE_NET_IP_SOCKET_HANDLE_H_

#include "core/optional.hpp"

#include "net/net.hh"
#include "net/ip/endpoint.h"
#include "net/ip/socket_error.h"

////////////////////////////////////////////////////////////////////////////////
// net ip
////////////////////////////////////////////////////////////////////////////////

namespace net { namespace ip {

/*******************************************************************************
** SocketHandle
*******************************************************************************/

using SocketHandle = core::Value<SOCKET, INVALID_SOCKET>;

core::Optional<SocketHandle> open_tcp();
core::Optional<SocketHandle> open_udp();

core::Optional<SocketError> set_non_blocking(SocketHandle _socket_handle);
core::Optional<SocketError> set_reusable_address(SocketHandle _socket_handle);
core::Optional<SocketError> set_no_delay(SocketHandle _socket_handle);

core::Optional<SocketError> bind(SocketHandle _socket_handle,
                                 Endpoint _local_endpoint);
core::Optional<SocketError> listen(SocketHandle _socket_handle);

core::Optional<SocketError> connect(SocketHandle _socket_handle,
                                    Endpoint _remote_endpoint);

core::Optional<SocketError> shutdown(SocketHandle _socket_handle);
core::Optional<SocketError> close(SocketHandle _socket_handle);

}}

#endif