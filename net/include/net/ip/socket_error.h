#ifndef _CORE_NET_IP_SOCKET_ERROR_H_
#define _CORE_NET_IP_SOCKET_ERROR_H_

#include "core/string.hpp"

#include "net/net.hh"

////////////////////////////////////////////////////////////////////////////////
// net ip
////////////////////////////////////////////////////////////////////////////////

namespace net { namespace ip {

using SocketErrorCode = int;

/*******************************************************************************
** SocketError
*******************************************************************************/

class SocketError {
public:
  SocketError(SocketErrorCode _error_code);
  SocketError(SocketError &&) = default;
  SocketError(SocketError const &) = default;
  SocketError & operator=(SocketError &&) = default;
  SocketError & operator=(SocketError const &) = default;
  ~SocketError() = default;

  SocketErrorCode ErrorCode() const;
  const_string const & Message() const;

private:
  SocketErrorCode error_code;
  mutable const_string message;
};

/*******************************************************************************
** get_last_socket_error
*******************************************************************************/

SocketError get_last_socket_error();

}}

#endif