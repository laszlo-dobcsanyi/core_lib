#ifndef _CORE_NET_IP_ENDPOINT_H_
#define _CORE_NET_IP_ENDPOINT_H_

#include "core/string.hpp"
#include "core/optional.hpp"

#include "net/net.hh"

////////////////////////////////////////////////////////////////////////////////
// net ip
////////////////////////////////////////////////////////////////////////////////

namespace net { namespace ip {

/*******************************************************************************
** Endpoint
*******************************************************************************/

using Endpoint = sockaddr_in;

Endpoint make_endpoint(uint8 _a, uint8 _b, uint8 _c, uint8 _d);
Endpoint make_endpoint(uint8 _a, uint8 _b, uint8 _c, uint8 _d, uint16 _port);
Endpoint local_endpoint();
Endpoint local_endpoint(uint16 _port);

const_string to_string(Endpoint _endpoint);
core::Optional<Endpoint> from_string(const_string const & _const_string);

}}

#endif