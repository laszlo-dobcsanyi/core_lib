#ifndef _CORE_NET_IP_SOCKET_H_
#define _CORE_NET_IP_SOCKET_H_

#include "core/ranges/span.hpp"
#include "core/optional.hpp"
#include "core/variant.hpp"

#include "net/net.hh"
#include "net/ip/socket_handle.h"

////////////////////////////////////////////////////////////////////////////////
// net ip
////////////////////////////////////////////////////////////////////////////////

namespace net { namespace ip {

/*******************************************************************************
** Socket
*******************************************************************************/

class Socket {
public:
  Socket() = default;
  explicit Socket(SocketHandle _socket_handle);
  Socket(Socket &&) = default;
  Socket(Socket const &) = delete;
  Socket & operator=(Socket &&) = default;
  Socket & operator=(Socket const &) = delete;
  ~Socket();

  SocketHandle GetHandle() const;

  bool IsOpen() const;

  core::Optional<SocketError> Send(core::span<const byte> _payload);

  core::Variant<core::span<byte const>, SocketError>
  Receive(core::span<byte> _buffer);

  void Close();

private:
  SocketHandle socket_handle;
};

}}

#endif