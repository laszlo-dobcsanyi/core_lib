#ifndef _CORE_NET_NET_HH_
#define _CORE_NET_NET_HH_

#include "core/base.h"

#if defined(CORE_LIB_OS_WINDOWS)

#  include <winsock2.h>
#  pragma comment(lib, "wsock32.lib")

using socklen_t = int;

#  define SOCKET_ERROR_WOULD_BLOCK WSAEWOULDBLOCK
#  define SOCKET_ERROR_IN_PROGRESS WSAEINPROGRESS

#elif defined(CORE_LIB_OS_LINUX)

#  include <sys/socket.h>
#  include <netinet/in.h>
#  include <netinet/tcp.h>
#  include <fcntl.h>
#  include <arpa/inet.h>

#  define SOCKET int
#  define INVALID_SOCKET (SOCKET)(~0)
#  define SOCKET_ERROR -1
#  define SOCKET_ERROR_WOULD_BLOCK EWOULDBLOCK
#  define SOCKET_ERROR_IN_PROGRESS EINPROGRESS
#  define SD_BOTH SHUT_RDWR

#endif

#define LOG_NET(__severity__, ...) CORE_LIB_LOG(__severity__, __VA_ARGS__)

namespace net { namespace ip {

// Endpoint;
class SocketError;
// SocketHandle

}}

namespace net { namespace tcp {

class Connection;
class Connector;
class Listener;
class Socket;

}}

namespace net { namespace udp {

class Socket;
class Connection;

}}

#endif