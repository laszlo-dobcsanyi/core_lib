list(APPEND NET_SOURCES "include/net/ip/api.h")
list(APPEND NET_SOURCES "include/net/ip/endpoint.h")
list(APPEND NET_SOURCES "include/net/ip/socket_error.h")
list(APPEND NET_SOURCES "include/net/ip/socket.h")
list(APPEND NET_SOURCES "include/net/ip/socket_handle.h")
list(APPEND NET_SOURCES "include/net/net.hh")
list(APPEND NET_SOURCES "include/net/tcp/connection.h")
list(APPEND NET_SOURCES "include/net/tcp/connector.h")
list(APPEND NET_SOURCES "include/net/tcp/listener.h")
list(APPEND NET_SOURCES "include/net/udp/connection.h")
list(APPEND NET_SOURCES "src/ip/api.cpp")
list(APPEND NET_SOURCES "src/ip/endpoint.cpp")
list(APPEND NET_SOURCES "src/ip/socket.cpp")
list(APPEND NET_SOURCES "src/ip/socket_error.cpp")
list(APPEND NET_SOURCES "src/ip/socket_handle.cpp")
list(APPEND NET_SOURCES "src/tcp/connection.cpp")
list(APPEND NET_SOURCES "src/tcp/connector.cpp")
list(APPEND NET_SOURCES "src/tcp/listener.cpp")
list(APPEND NET_SOURCES "src/udp/connection.cpp")
