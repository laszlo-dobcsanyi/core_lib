include("${CMAKE_CURRENT_LIST_DIR}/clang.cmake")

set(CMAKE_CXX_FLAGS -m64)

#
# setup_core_library
#
function(setup_core_library target)
  setup_core_clang_library(${target})
endfunction()

