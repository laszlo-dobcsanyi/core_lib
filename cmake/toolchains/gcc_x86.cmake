include(cmake/toolchains/gcc.cmake)

string(APPEND CMAKE_CXX_FLAGS -m32)

#
# setup_core_target
#
function(setup_core_target ${target})
  setup_core_gcc_target(${target})
endfunction()

