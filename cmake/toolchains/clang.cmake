# Compiler
set(CMAKE_CXX_COMPILER clang++)
# C++ standard
set(CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_FIND_ROOT_PATH /usr/bin)

#
# setup_core_clang_library
#
function(setup_core_clang_library target)
  #
  # Definitions
  #

  target_compile_definitions(${target} PUBLIC $<$<CONFIG:debug>:CORE_LIB_CONFIGURATION_DEBUG>)

  #
  # Build options
  #

  target_compile_options(${target} PRIVATE -fvisibility=hidden)

  #
  # Warning options
  #

  target_compile_options(${target} PRIVATE -Wall)

  target_compile_options(${target} PRIVATE -Wextra)

  target_compile_options(${target} PRIVATE -Werror)

  # warn if non-standard C++ is used
  target_compile_options(${target} PRIVATE -Wpedantic)

  # warn the user if a variable declaration shadows one from a parent context
  target_compile_options(${target} PRIVATE -Wshadow)

  # warn the user if a class with virtual functions has a non-virtual destructor
  target_compile_options(${target} PRIVATE -Wnon-virtual-dtor)

  # warn for c-style casts
  # target_compile_options(${target} PRIVATE -Wold-style-cast)

  # warn for potential performance problem casts
  target_compile_options(${target} PRIVATE -Wcast-align)

  # warn if you overload (not override) a virtual function
  target_compile_options(${target} PRIVATE -Woverloaded-virtual)

  # warn on type conversions that may lose data
  # target_compile_options(${target} PRIVATE -Wconversion)

  # warn on sign conversions
  # target_compile_options(${target} PRIVATE -Wsign-conversion)

  # warn if indentation implies blocks where blocks do not exist
  target_compile_options(${target} PRIVATE -Wmisleading-indentation)

  # warn if a null dereference is detected
  target_compile_options(${target} PRIVATE -Wnull-dereference)
  
  target_compile_options(${target} PRIVATE -Wno-unused-variable)

  target_compile_options(${target} PRIVATE -Wno-unused-parameter)
endfunction()

