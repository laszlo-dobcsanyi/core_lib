include(cmake/toolchains/clang.cmake)

string(APPEND CMAKE_CXX_FLAGS -m32)

#
# setup_core_target
#
function(setup_core_target ${target})
  setup_core_clang_target(${target})
endfunction()

