# Compiler
set(CMAKE_CXX_COMPILER gcc)
# C++ standard
set(CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_FIND_ROOT_PATH /usr/bin)

#
# setup_core_gcc_target
#
function(setup_core_gcc_target ${target})
  #
  # Definitions
  #

  target_compile_definitions(${target} PUBLIC $<$<CONFIG:debug>:CORE_LIB_CONFIGURATION_DEBUG>)

  #
  # Build options
  #

  target_compile_options(${target} PRIVATE -fvisibility=hidden)

  #
  # Warning options
  #

  target_compile_options(${target} PRIVATE -Wall)
  target_compile_options(${target} PRIVATE -Wextra)
  target_compile_options(${target} PRIVATE -Werror)

  # warn if non-standard C++ is used
  target_compile_options(${target} PRIVATE -Wpedantic)

  # warn the user if a variable declaration shadows one from a parent context
  target_compile_options(${target} PRIVATE -Wshadow)

  # warn the user if a class with virtual functions has a non-virtual destructor.
  target_compile_options(${target} PRIVATE -Wnon-virtual-dtor)

  # warn for c-style casts
  # target_compile_options(${target} PRIVATE -Wold-style-cast)

  # warn for potential performance problem casts
  target_compile_options(${target} PRIVATE -Wcast-align)

  # warn on anything being unused
  # target_compile_options(${target} PRIVATE -Wunused)

  # warn if you overload (not override) a virtual function
  target_compile_options(${target} PRIVATE -Woverloaded-virtual)

  # warn on type conversions that may lose data
  # target_compile_options(${target} PRIVATE -Wconversion)

  # warn on sign conversions
  # target_compile_options(${target} PRIVATE -Wsign-conversion)

  # warn if indentation implies blocks where blocks do not exist
  target_compile_options(${target} PRIVATE -Wmisleading-indentation)

  # warn if if / else chain has duplicated conditions
  target_compile_options(${target} PRIVATE -Wduplicated-cond)

  # warn if if / else branches have duplicated code
  target_compile_options(${target} PRIVATE -Wduplicated-branches)

  # warn about logical operations being used where bitwise were probably wanted
  target_compile_options(${target} PRIVATE -Wlogical-op)

  # warn if a null dereference is detected
  # target_compile_options(${target} PRIVATE -Wnull-dereference)

  # warn if you perform a cast to the same type
  # target_compile_options(${target} PRIVATE -Wuseless-cast)

  # warn if float is implicit promoted to double
  # target_compile_options(${target} PRIVATE -Wdouble-promotion)

  target_compile_options(${target} PRIVATE -Wno-unused-variable)

  target_compile_options(${target} PRIVATE -Wno-unused-parameter)

  target_compile_options(${target} PRIVATE -Wno-maybe-uninitialized)
endfunction()

