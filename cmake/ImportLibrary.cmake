# Wrote by someone who understands CMake..
# https://github.com/google/gapid/blob/e3de9dcbfd93e5b49610c8d9a0cf996ef0f01976/cmake/Utils.cmake

function( import_library tgt )
    if( TARGET ${tgt} )
        return()
    endif()
    list( LENGTH ARGN count )
    if( count EQUAL 0 )
        add_library( ${tgt} INTERFACE IMPORTED )
    elseif( count EQUAL 1 )
        add_library( ${tgt} UNKNOWN IMPORTED )
        set_target_properties( ${tgt} PROPERTIES IMPORTED_LOCATION "${ARGN}" )
    else()
        message( "Only 0 or 1 libraries allowed right now" )
    endif()
endfunction( import_library )
