# Locate ZLib
#
# This module defines
#   ZLIB_LIBRARY
#   ZLIB_FOUND
#   ZLIB_INCLUDE_DIR

find_path( ZLIB_INCLUDE_DIR zlib.h ${THIRD_PARTY_INSTALL_DIR}/include NO_DEFAULT_PATH )

find_library( ZLIB_LIBRARY NAMES zlibstatic.lib zlibstaticd.lib libz.a PATHS ${THIRD_PARTY_INSTALL_DIR}/lib NO_DEFAULT_PATH )

set( ZLIB_FOUND "NO" )
if( ZLIB_INCLUDE_DIR )
  if( ZLIB_LIBRARY )
    set( ZLIB_FOUND "YES" )
    message( STATUS "Zlib include: " ${ZLIB_INCLUDE_DIR} )
    message( STATUS "Zlib library: " ${ZLIB_LIBRARY} )
  else()
    message( FATAL_ERROR "ZLib library not found in " ${THIRD_PARTY_INSTALL_DIR}/lib )
  endif()
else()
  message( FATAL_ERROR "ZLib includes not found in " ${THIRD_PARTY_INSTALL_DIR}/include )
endif()
