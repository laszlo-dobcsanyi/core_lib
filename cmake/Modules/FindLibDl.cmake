# Wrote by someone who understands CMake..
# https://github.com/google/gapid/blob/e3de9dcbfd93e5b49610c8d9a0cf996ef0f01976/cmake/Utils.cmake

find_library( DL_LIBRARY NAMES dl )
include( FindPackageHandleStandardArgs )
find_package_handle_standard_args( LibDl REQUIRED_VARS DL_LIBRARY )
import_library( LibDl::Lib "${DL_LIBRARY}")
