# Locate FreeType
#
# This module defines
#   FREETYPE_LIBRARY
#   FREETYPE_FOUND
#   FREETYPE_INCLUDE_DIR

find_path( FREETYPE_INCLUDE_DIR ft2build.h ${THIRD_PARTY_INSTALL_DIR}/include/freetype2 NO_DEFAULT_PATH )

find_library( FREETYPE_LIBRARY NAMES freetype.lib freetyped.lib libfreetype.a libfreetyped.a PATHS ${THIRD_PARTY_INSTALL_DIR}/lib NO_DEFAULT_PATH )

set( FREETYPE_FOUND "NO" )
if( FREETYPE_INCLUDE_DIR )
  if( FREETYPE_LIBRARY )
    set( FREETYPE_FOUND "YES" )
    message( STATUS "FreeType include: " ${FREETYPE_INCLUDE_DIR} )
    message( STATUS "FreeType library: " ${FREETYPE_LIBRARY} )
  else()
    message( FATAL_ERROR "Could not find library files in " ${THIRD_PARTY_INSTALL_DIR}/lib )
  endif()
else()
  message( FATAL_ERROR "Could not find include dirs in " ${THIRD_PARTY_INSTALL_DIR}/include/freetype2 )
endif()
