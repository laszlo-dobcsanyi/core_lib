# Wrote by someone who understands CMake..
# https://github.com/google/gapid/blob/e3de9dcbfd93e5b49610c8d9a0cf996ef0f01976/cmake/Utils.cmake

find_library( RT_LIBRARY NAMES rt )
include( FindPackageHandleStandardArgs )
find_package_handle_standard_args( LibRt REQUIRED_VARS RT_LIBRARY)
import_library( LibRt::Lib "${RT_LIBRARY}")
