#ifndef _DEMO_DEMO_APPLICATION_H_
#define _DEMO_DEMO_APPLICATION_H_

#include "core/base.h"
#include "core/application.hpp"

////////////////////////////////////////////////////////////////////////////////
// demo
////////////////////////////////////////////////////////////////////////////////

namespace demo {

/*******************************************************************************
** DemoApplication
*******************************************************************************/

class DemoApplication {
public:
  application::CommandLine command_line;

  DemoApplication() = delete;
  DemoApplication(application::CommandLine _command_line);
  DemoApplication(DemoApplication &&) = delete;
  DemoApplication(DemoApplication const &) = delete;
  DemoApplication & operator=(DemoApplication &&) = delete;
  DemoApplication & operator=(DemoApplication const &) = delete;
  ~DemoApplication() = default;

  void Run();
};

}

#endif