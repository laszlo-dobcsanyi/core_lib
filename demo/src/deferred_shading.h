#ifndef _DEMO_DEFERRED_SHADING_H_
#define _DEMO_DEFERRED_SHADING_H_

#include "core/containers.hpp"
#include "core/optional.hpp"
#include "core/fs/directory.h"
#include "graphics/api.h"
#include "graphics/window.h"
#include "graphics/renderbuffer.h"
#include "graphics/framebuffer.h"
#include "graphics/animation/skeletal_animation.h"
#include "graphics/renderers/forward_animated_model_renderer.h"
#include "graphics/renderers/forward_static_model_renderer.h"
#include "graphics/renderers/forward_skybox_renderer.h"
#include "graphics/renderers/forward_terrain_renderer.h"

////////////////////////////////////////////////////////////////////////////////
// demo
////////////////////////////////////////////////////////////////////////////////

namespace demo {

template<class T>
using Resource = core::WeakObjectWrapper<T>;

using TerrainResource = Resource<graphics::Terrain>;
using SkyboxResource = Resource<graphics::Skybox>;
using StaticModelResource = Resource<graphics::StaticModel>;
using AnimatedModelResource = Resource<graphics::AnimatedModel>;

/*******************************************************************************
** Assets
*******************************************************************************/

class Assets {
public:
  Assets() = default;
  Assets(Assets &&) = default;
  Assets(Assets const &) = delete;
  Assets & operator=(Assets &&) = default;
  Assets & operator=(Assets const &) = delete;
  ~Assets() = default;

  core::Optional<core::Ref<TerrainResource>>
  LoadTerrain(core::fs::Directory const & _terrain_directory);
  core::Optional<core::Ref<SkyboxResource>>
  LoadSkybox(core::fs::Directory const & _skybox_directory);
  core::Optional<core::Ref<StaticModelResource>>
  LoadStaticModel(core::fs::Directory const & _static_model_directory);
  core::Optional<core::Ref<AnimatedModelResource>>
  LoadAnimatedModel(core::fs::Directory const & _animated_model_directory);

  TerrainResource & Terrain() { return terrain; }
  SkyboxResource & Skybox() { return skybox; }
  container::PoolArray<StaticModelResource> & StaticModels() {
    return static_models;
  }
  container::PoolArray<AnimatedModelResource> & AnimatedModels() {
    return animated_models;
  }

private:
  TerrainResource terrain;
  SkyboxResource skybox;
  container::PoolArray<StaticModelResource> static_models;
  container::PoolArray<AnimatedModelResource> animated_models;
};

/*******************************************************************************
** StaticModelObject
*******************************************************************************/

class StaticModelObject : public core::WeakCounter<StaticModelObject> {
public:
  StaticModelObject(core::Ref<StaticModelResource> _static_model,
                    numerics::Transformation _transformation);
  StaticModelObject(StaticModelObject &&) = default;
  StaticModelObject(StaticModelObject const &) = delete;
  StaticModelObject & operator=(StaticModelObject &&) = default;
  StaticModelObject & operator=(StaticModelObject const &) = delete;
  ~StaticModelObject() = default;

  StaticModelResource const & StaticModel() const { return *static_model; }
  numerics::Transformation const & Transformation() const {
    return transformation;
  }

private:
  core::Ref<StaticModelResource> static_model;
  numerics::Transformation transformation;
};

/*******************************************************************************
** AnimatedModelObject
*******************************************************************************/

class AnimatedModelObject : public core::WeakCounter<AnimatedModelObject> {
public:
  AnimatedModelObject(core::Ref<AnimatedModelResource> _animated_model,
                      numerics::Transformation _transformation);
  AnimatedModelObject(AnimatedModelObject &&) = default;
  AnimatedModelObject(AnimatedModelObject const &) = delete;
  AnimatedModelObject & operator=(AnimatedModelObject &&) = default;
  AnimatedModelObject & operator=(AnimatedModelObject const &) = delete;
  ~AnimatedModelObject() = default;

  AnimatedModelResource const & AnimatedModel() const {
    return *animated_model;
  }
  graphics::animation::SkeletalAnimation const & Animation() const {
    return animation;
  }
  numerics::Transformation const & Transformation() const {
    return transformation;
  }

private:
  core::Ref<AnimatedModelResource> animated_model;
  graphics::animation::SkeletalAnimation animation;
  numerics::Transformation transformation;
};

/*******************************************************************************
** Scene
*******************************************************************************/

class Scene {
public:
  void SetTerrain(core::Ref<TerrainResource> _terrain);
  void SetSkybox(core::Ref<SkyboxResource> _skybox);
  core::Ref<StaticModelObject>
  CreateStaticModelObject(core::Ref<StaticModelResource> _static_model,
                          numerics::Transformation _transformation);
  core::Ref<AnimatedModelObject>
  CreateAnimatedModelObject(core::Ref<AnimatedModelResource> _animated_model,
                            numerics::Transformation _transformation);

  core::Optional<core::Ref<TerrainResource>> const & Terrain() const {
    return terrain;
  }
  core::Optional<core::Ref<SkyboxResource>> const & Skybox() const {
    return skybox;
  }
  container::PoolArray<StaticModelObject> const & StaticModels() const {
    return static_models;
  }
  container::PoolArray<AnimatedModelObject> const & AnimatedModels() const {
    return animated_models;
  }

  void Clear();

private:
  core::Optional<core::Ref<TerrainResource>> terrain;
  core::Optional<core::Ref<SkyboxResource>> skybox;
  container::PoolArray<StaticModelObject> static_models;
  container::PoolArray<AnimatedModelObject> animated_models;
};

/*******************************************************************************
** DeferredPipeline
*******************************************************************************/

class DeferredPipeline {
public:
  DeferredPipeline() = delete;
  DeferredPipeline(
      graphics::Texture _color_buffer, graphics::Texture _normal_buffer,
      graphics::Texture _specular_buffer, graphics::RenderBuffer _depth_buffer,
      graphics::FrameBuffer _frame_buffer,
      graphics::ForwardTerrainRenderer _terrain_renderer,
      graphics::ForwardSkyboxRenderer _skybox_renderer,
      graphics::ForwardStaticModelRenderer _static_model_renderer,
      graphics::ForwardAnimatedModelRenderer _animated_model_renderer);
  DeferredPipeline(DeferredPipeline &&) = default;
  DeferredPipeline(DeferredPipeline const &) = delete;
  DeferredPipeline & operator=(DeferredPipeline &&) = default;
  DeferredPipeline & operator=(DeferredPipeline const &) = delete;
  ~DeferredPipeline() = default;

  void Render(Scene const & _scene, graphics::Camera const & _camera);

private:
  graphics::Texture color_buffer;
  graphics::Texture normal_buffer;
  graphics::Texture specular_buffer;
  graphics::RenderBuffer depth_buffer;
  graphics::FrameBuffer frame_buffer;

  graphics::ForwardTerrainRenderer terrain_renderer;
  graphics::ForwardSkyboxRenderer skybox_renderer;
  graphics::ForwardStaticModelRenderer static_model_renderer;
  graphics::ForwardAnimatedModelRenderer animated_model_renderer;
};

/*******************************************************************************
** create_deferred_pipeline
*******************************************************************************/

core::Optional<DeferredPipeline> create_deferred_pipeline(uint32 _width,
                                                          uint32 _height);

/*******************************************************************************
** DeferredShading
*******************************************************************************/

class DeferredShading {
public:
  bool Initialize();
  void Load(core::fs::Directory const & _root);
  void Run();

private:
  graphics::api::Context context;
  graphics::Window window;
  graphics::Camera camera;
  Assets assets;
  Scene scene;
  core::Optional<DeferredPipeline> deferred_pipeline;
};

}

#endif
