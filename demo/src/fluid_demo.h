#ifndef _DEMO_FLUID_DEMO_H_
#define _DEMO_FLUID_DEMO_H_

#include "core/fs/directory.h"
#include "graphics/api.h"
#include "graphics/window.h"
#include "graphics/framebuffer.h"
#include "graphics/texture.h"
#include "graphics/renderer.hpp"
#include "graphics/experimental/canvas.hpp"

////////////////////////////////////////////////////////////////////////////////
// demo
////////////////////////////////////////////////////////////////////////////////

namespace demo {

/*******************************************************************************
** FluidSimulatorUniforms
*******************************************************************************/

struct FluidSimulatorUniforms {
  float time;
  // float time_delta;
  numerics::Vector2f resolution;
  graphics::TextureUnit texture_unit_1;

private:
  template<typename UniformContext>
  friend auto tag_invoke(graphics::reflect_uniforms_t,
                         mpl::type<FluidSimulatorUniforms>,
                         UniformContext & _context) {
    return _context
        .Member(graphics::UniformId::Time, &FluidSimulatorUniforms::time)
        // .Member(graphics::UniformId::TimeDelta, &FluidSimulatorUniforms::time_delta)
        .Member(graphics::UniformId::Resolution,
                &FluidSimulatorUniforms::resolution)
        .Member(graphics::UniformId::Texture1,
                &FluidSimulatorUniforms::texture_unit_1);
  }
};

/*******************************************************************************
** FluidSimulator
*******************************************************************************/

struct FluidSimulator : public graphics::RendererBase<FluidSimulatorUniforms> {
  core::fs::FilePath vertex_shader_path;
  core::fs::FilePath fragment_shader_path;
  graphics::FrameBuffer frame_buffer;

  size_t source_index = 0u;
  container::InlineArray<graphics::Texture, 2u> textures;
  float time = 0.f;

  size_t TargetIndex() const { return (source_index + 1u) % 2u; }

  graphics::Texture const & SourceTexture() const {
    return textures[source_index];
  }

  graphics::Texture const & TargetTexture() const {
    return textures[TargetIndex()];
  }

  void SwapTextures() { source_index = TargetIndex(); }
};

/*******************************************************************************
** FluidVisualizerUniforms
*******************************************************************************/

struct FluidVisualizerUniforms {
  float time;
  // float time_delta;
  numerics::Vector2f resolution;
  graphics::TextureUnit texture_unit_1;
  graphics::TextureUnit texture_unit_2;

private:
  template<typename UniformContext>
  friend auto tag_invoke(graphics::reflect_uniforms_t,
                         mpl::type<FluidVisualizerUniforms>,
                         UniformContext & _context) {
    return _context
        .Member(graphics::UniformId::Time, &FluidVisualizerUniforms::time)
        // .Member(graphics::UniformId::TimeDelta, &FluidVisualizerUniforms::time_delta)
        .Member(graphics::UniformId::Resolution,
                &FluidVisualizerUniforms::resolution)
        .Member(graphics::UniformId::Texture1,
                &FluidVisualizerUniforms::texture_unit_1)
        .Member(graphics::UniformId::Texture2,
                &FluidVisualizerUniforms::texture_unit_2);
  }
};

/*******************************************************************************
** FluidVisualizer
*******************************************************************************/

struct FluidVisualizer
    : public graphics::RendererBase<FluidVisualizerUniforms> {
  core::fs::FilePath vertex_shader_path;
  core::fs::FilePath fragment_shader_path;
  graphics::FrameBuffer frame_buffer;

  size_t source_index = 0u;
  container::InlineArray<graphics::Texture, 2u> textures;
  float time = 0.f;

  size_t TargetIndex() const { return (source_index + 1u) % 2u; }

  graphics::Texture const & SourceTexture() const {
    return textures[source_index];
  }

  graphics::Texture const & TargetTexture() const {
    return textures[TargetIndex()];
  }

  void SwapTextures() { source_index = TargetIndex(); }
};

/*******************************************************************************
** FluidRendererUniforms
*******************************************************************************/

struct FluidRendererUniforms {
  graphics::TextureUnit texture_unit_1;

private:
  template<typename UniformContext>
  friend auto tag_invoke(graphics::reflect_uniforms_t,
                         mpl::type<FluidRendererUniforms>,
                         UniformContext & _context) {
    return _context.Member(graphics::UniformId::Texture1,
                           &FluidRendererUniforms::texture_unit_1);
  }
};

/*******************************************************************************
** FluidRenderer
*******************************************************************************/

using FluidRenderer = graphics::RendererBase<FluidRendererUniforms>;

/*******************************************************************************
** FluidDemo
*******************************************************************************/

class FluidDemo {
public:
  bool Initialize();
  void Load(core::fs::Directory const & _root);
  void Run();

private:
  graphics::api::Context context;
  graphics::Window window;
  graphics::experimental::Canvas<graphics::TexturedVertex> canvas;

  FluidSimulator fluid_simulator;
  FluidVisualizer fluid_visualizer;
  FluidRenderer fluid_renderer;

  void OnKeyPressed(graphics::window::KeyPressedEventArgs const & _args);
};

}

#endif