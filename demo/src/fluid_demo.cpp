#include "fluid_demo.h"

#include "core/time.hpp"
#include "core/fs/mapped_file.hpp"

#define LOG_DEMO(__severity__, ...) CORE_LIB_LOG(__severity__, __VA_ARGS__)

////////////////////////////////////////////////////////////////////////////////
// demo
////////////////////////////////////////////////////////////////////////////////

namespace demo {

/*******************************************************************************
** file_to_string
*******************************************************************************/

inline core::Optional<const_string>
file_to_string(core::fs::FilePath const & _file_path) {
  auto mapping_reader = core::fs::mapped_file_reader(_file_path);
  if (!mapping_reader) {
    CORE_LIB_LOG(ERROR, "Failed to map '%s' into memory!\n", cstr(_file_path));
    return core::nullopt;
  }
  string::storage::UniqueStorage storage(mapping_reader->Available());
  auto destination = reinterpret_cast<uintptr_t>(storage.Get());
  auto source = reinterpret_cast<uintptr_t>(mapping_reader->Bytes().data());
  ::memcpy(reinterpret_cast<void *>(destination),
           reinterpret_cast<void const *>(source), mapping_reader->Available());
  const_string data(move(storage));
  return data;
}

/*******************************************************************************
** shader_from_file
*******************************************************************************/

inline core::Optional<graphics::Shader>
shader_from_file(core::fs::FilePath const & _vertex_shader_path,
                 core::fs::FilePath const & _fragment_shader_path) {
  core::Optional<const_string> vertex_shader_data =
      file_to_string(_vertex_shader_path);
  if (!vertex_shader_data.HasValue()) {
    CORE_LIB_LOG(ERROR, "Failed to load vertex shader data from '%s'!\n",
                 cstr(_vertex_shader_path));
    return core::nullopt;
  }
  core::Optional<const_string> fragment_shader_data =
      file_to_string(_fragment_shader_path);
  if (!fragment_shader_data.HasValue()) {
    CORE_LIB_LOG(ERROR, "Failed to load fragment shader data from '%s'!\n",
                 cstr(_fragment_shader_path));
    return core::nullopt;
  }

  graphics::Shader shader;
  if (!shader.Create(*vertex_shader_data, *fragment_shader_data)) {
    CORE_LIB_LOG(ERROR, "Failed to create shader!\n");
    return core::nullopt;
  }
  return shader;
}

/*******************************************************************************
** create_fluid_simulator
*******************************************************************************/

core::Optional<FluidSimulator> create_fluid_simulator() {
  FluidSimulator simulator;
  core::fs::FilePath vertex_shader_path =
      core::fs::FilePath("./debug_x64/demo/simulator.vert.glsl");
  core::fs::FilePath fragment_shader_path =
      core::fs::FilePath("./debug_x64/demo/simulator.frag.glsl");
  {
    core::Optional<graphics::Shader> shader =
        shader_from_file(vertex_shader_path, fragment_shader_path);
    if (!shader.HasValue()) {
      return core::nullopt;
    }

    simulator.shader = move(*shader);
    graphics::opengl::generate_uniform_locations(simulator);
  }

  if (!simulator.frame_buffer.Create()) {
    return core::nullopt;
  }

  uint32 width = 1920;
  uint32 height = 1080;
  graphics::ColorFormat color_format = graphics::ColorFormat::RGBA;
  graphics::resource::ImageData image_data(width, height, color_format);

  {
    for (auto row = 0u; row < image_data.Height(); ++row) {
      core::span<byte> row_bytes = image_data.GetRowBytes(row);
      byte * row_memory = row_bytes.data();
      byte * row_memory_end = row_memory + row_bytes.size();
      while (row_memory < row_memory_end) {
        *row_memory = 0;
        ++row_memory;
      }
    }
  }

  graphics::TextureSettings settings;
  settings.generate_mipmap = false;
  settings.wrap_u = graphics::TextureWrapMode::Repeat;
  settings.wrap_v = graphics::TextureWrapMode::Repeat;
  settings.filter_minify = graphics::TextureFilterMode::Nearest;
  settings.filter_magnify = graphics::TextureFilterMode::Nearest;

  // Textures
  for (size_t texture = 0u; texture < simulator.textures.Size(); ++texture) {
    simulator.textures[texture] =
        graphics::create_texture(image_data, settings);
    if (!simulator.textures[texture]) {
      LOG_DEMO(ERROR, "Failed to create texture %zu!\n", texture);
      return core::nullopt;
    }
  }

  return simulator;
}

/*******************************************************************************
** bind
*******************************************************************************/

void bind(FluidSimulator & _simulator) {
  graphics::glActiveTexture(GL_TEXTURE0 + 0);
  graphics::glBindTexture(GL_TEXTURE_2D, _simulator.SourceTexture().object);

  _simulator.shader.Use();
}

/*******************************************************************************
** render
*******************************************************************************/

template<typename MeshType>
void render(FluidSimulator const & _simulator, MeshType const & _mesh,
            float _time_delta) {
  FluidSimulatorUniforms uniforms;
  uniforms.time = _simulator.time;
  // uniforms.time_delta = _time_delta;
  uniforms.resolution = numerics::Vector2f(1920.0f, 1080.f);
  uniforms.texture_unit_1 = graphics::TextureUnit(0);
  graphics::render_mesh(_simulator, uniforms, _mesh,
                        graphics::DrawMode::Triangles);
}

/*******************************************************************************
** create_fluid_visualizer
*******************************************************************************/

core::Optional<FluidVisualizer> create_fluid_visualizer() {
  FluidVisualizer visualizer;
  core::fs::FilePath vertex_shader_path =
      core::fs::FilePath("./debug_x64/demo/visualizer.vert.glsl");
  core::fs::FilePath fragment_shader_path =
      core::fs::FilePath("./debug_x64/demo/visualizer.frag.glsl");
  {
    core::Optional<graphics::Shader> shader =
        shader_from_file(vertex_shader_path, fragment_shader_path);
    if (!shader.HasValue()) {
      return core::nullopt;
    }

    visualizer.shader = move(*shader);
    graphics::opengl::generate_uniform_locations(visualizer);
  }

  if (!visualizer.frame_buffer.Create()) {
    return core::nullopt;
  }

  uint32 width = 1920;
  uint32 height = 1080;
  graphics::ColorFormat color_format = graphics::ColorFormat::RGBA;
  graphics::resource::ImageData image_data(width, height, color_format);

  {
    for (auto row = 0u; row < image_data.Height(); ++row) {
      core::span<byte> row_bytes = image_data.GetRowBytes(row);
      byte * row_memory = row_bytes.data();
      byte * row_memory_end = row_memory + row_bytes.size();
      while (row_memory < row_memory_end) {
        *row_memory = 0;
        ++row_memory;
      }
    }
  }

  graphics::TextureSettings settings;
  settings.generate_mipmap = false;
  settings.wrap_u = graphics::TextureWrapMode::Repeat;
  settings.wrap_v = graphics::TextureWrapMode::Repeat;
  settings.filter_minify = graphics::TextureFilterMode::Nearest;
  settings.filter_magnify = graphics::TextureFilterMode::Nearest;

  // Textures
  for (size_t texture = 0u; texture < visualizer.textures.Size(); ++texture) {
    visualizer.textures[texture] =
        graphics::create_texture(image_data, settings);
    if (!visualizer.textures[texture]) {
      LOG_DEMO(ERROR, "Failed to create texture %zu!\n", texture);
      return core::nullopt;
    }
  }
  return visualizer;
}

/*******************************************************************************
** bind
*******************************************************************************/

void bind(FluidVisualizer & _visualizer, FluidSimulator & _simulator) {
  graphics::glActiveTexture(GL_TEXTURE0 + 0);
  graphics::glBindTexture(GL_TEXTURE_2D, _simulator.SourceTexture().object);

  graphics::glActiveTexture(GL_TEXTURE0 + 1);
  graphics::glBindTexture(GL_TEXTURE_2D, _visualizer.SourceTexture().object);

  _visualizer.shader.Use();
}

/*******************************************************************************
** render
*******************************************************************************/

template<typename MeshType>
void render(FluidVisualizer const & _visualizer, MeshType const & _mesh,
            float _time_delta) {
  FluidVisualizerUniforms uniforms;
  uniforms.time = _visualizer.time;
  // uniforms.time_delta = _time_delta;
  uniforms.resolution = numerics::Vector2f(1920.0f, 1080.f);
  uniforms.texture_unit_1 = graphics::TextureUnit(0);
  uniforms.texture_unit_2 = graphics::TextureUnit(1);
  graphics::render_mesh(_visualizer, uniforms, _mesh,
                        graphics::DrawMode::Triangles);
}

/*******************************************************************************
** create_fluid_renderer
*******************************************************************************/

core::Optional<FluidRenderer> create_fluid_renderer() {
  FluidRenderer renderer;

  core::fs::FilePath vertex_shader_path =
      core::fs::FilePath("./debug_x64/demo/quad.vert.glsl");
  core::fs::FilePath fragment_shader_path =
      core::fs::FilePath("./debug_x64/demo/quad.frag.glsl");
  if (core::Optional<graphics::Shader> shader =
          shader_from_file(vertex_shader_path, fragment_shader_path)) {
    renderer.shader = move(*shader);
    graphics::opengl::generate_uniform_locations(renderer);
    return renderer;
  }
  return core::nullopt;
}

/*******************************************************************************
** bind
*******************************************************************************/

void bind(FluidRenderer & _renderer, FluidVisualizer const & _fluid_visualizer,
          FluidSimulator const & _fluid_simulator) {
  graphics::glActiveTexture(GL_TEXTURE0 + 0);
  graphics::glBindTexture(GL_TEXTURE_2D,
                          _fluid_simulator.SourceTexture().object);
  _renderer.shader.Use();
}

/*******************************************************************************
** render
*******************************************************************************/

template<typename MeshType>
void render(FluidRenderer const & _renderer, MeshType const & _mesh) {
  FluidRendererUniforms uniforms;
  uniforms.texture_unit_1 = graphics::TextureUnit(0);
  graphics::render_mesh(_renderer, uniforms, _mesh,
                        graphics::DrawMode::Triangles);
}

/*******************************************************************************
** FluidDemo
*******************************************************************************/

bool FluidDemo::Initialize() {
  LOG_DEMO(STAGE, "Initializing..\n");

  // Context
  LOG_DEMO(STAGE, "Initializing Context..\n");
  if (!context.Initialize()) {
    LOG_DEMO(ERROR, "Failed to initialize context!\n");
    return false;
  }

  // Window
  LOG_DEMO(STAGE, "Initializing Window..\n");
  if (!window.Create(const_string("fluid_demo"))) {
    LOG_DEMO(ERROR, "Failed to create window!\n");
    return false;
  }

  window.OnKeyPressed.connect(graphics::window::KeyPressedDelegate::Bind(this)
                                  .Method<&FluidDemo::OnKeyPressed>());

  // Canvas
  {
    const float kScale = 1.f;

    container::InlineArray<graphics::TexturedVertex, 4u> vertices;
    vertices[0u].position = numerics::Vector3f(-kScale, +kScale, 0.f);
    vertices[0u].normal = numerics::Vector3f(0.f, 0.f, 1.f);
    vertices[0u].texture_coordinate = numerics::Vector2f(0.f, 1.f);
    vertices[1u].position = numerics::Vector3f(-kScale, -kScale, 0.f);
    vertices[1u].normal = numerics::Vector3f(0.f, 0.f, 1.f);
    vertices[1u].texture_coordinate = numerics::Vector2f(0.f, 0.f);
    vertices[2u].position = numerics::Vector3f(+kScale, -kScale, 0.f);
    vertices[2u].normal = numerics::Vector3f(0.f, 0.f, 1.f);
    vertices[2u].texture_coordinate = numerics::Vector2f(1.f, 0.f);
    vertices[3u].position = numerics::Vector3f(+kScale, +kScale, 0.f);
    vertices[3u].normal = numerics::Vector3f(0.f, 0.f, 1.f);
    vertices[3u].texture_coordinate = numerics::Vector2f(1.f, 1.f);
    canvas.Set(core::make_span(vertices));
  }

  // FluidSimulator
  if (auto simulator_result = create_fluid_simulator()) {
    fluid_simulator = move(*simulator_result);
  } else {
    LOG_DEMO(ERROR, "Failed to create fluid simulator!\n");
    return false;
  }

  // FluidVisualizer
  if (auto visualizer_result = create_fluid_visualizer()) {
    fluid_visualizer = move(*visualizer_result);
  } else {
    LOG_DEMO(ERROR, "Failed to create fluid visualizer!\n");
    return false;
  }

  // FluidRenderer
  if (auto renderer_result = create_fluid_renderer()) {
    fluid_renderer = move(*renderer_result);
  } else {
    LOG_DEMO(ERROR, "Failed to create fluid renderer!\n");
    return false;
  }

  return true;
}

void FluidDemo::Run() {
  LOG_DEMO(STAGE, "Running..\n");

  // Clock
  core::HighResolutionClock clock;

  // TODO( Silent ): Hack
  graphics::glDisable(GL_DEPTH_TEST);
  graphics::glDisable(GL_BLEND);

  // Main loop
  LOG_DEMO(STAGE, "Entering Main Loop..\n");
  for (;;) {
    window.Run();
    if (window.IsDestroyed()) {
      break;
    }

    float delta = static_cast<float>(core::to_seconds(clock.Reset()));
    fluid_simulator.time += delta;
    fluid_visualizer.time += delta;

    // Pass 1: Fluid simulation
    {
      graphics::glViewport(0, 0, 1920, 1080);
      fluid_simulator.frame_buffer.Bind();

      for (auto pass = 0; pass < 1; ++pass) {
        fluid_simulator.frame_buffer.Attach(0, fluid_simulator.TargetTexture());

        {
          graphics::glClearColor(0.2f, 0.3f, 0.3f, 1.f);
          graphics::glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

          bind(fluid_simulator);
          render(fluid_simulator, canvas.mesh, delta);

          fluid_simulator.SwapTextures();
        }
      }

      fluid_simulator.frame_buffer.Unbind();
      // graphics::glViewport(0, 0, window.GetWidth(), window.GetHeight());
    }

    // Pass 2: Fluid visualization
    {
      // graphics::glViewport(0, 0, 1920, 1080);
      fluid_visualizer.frame_buffer.Bind();
      fluid_visualizer.frame_buffer.Attach(0, fluid_visualizer.TargetTexture());

      {
        graphics::glClearColor(0.2f, 0.3f, 0.3f, 1.f);
        graphics::glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        bind(fluid_visualizer, fluid_simulator);
        render(fluid_visualizer, canvas.mesh, delta);

        fluid_visualizer.SwapTextures();
      }

      fluid_visualizer.frame_buffer.Unbind();
      graphics::glViewport(0, 0, window.Width(), window.Height());
    }

    // Pass 3: Fluid render
    {
      window.Clear();

      bind(fluid_renderer, fluid_visualizer, fluid_simulator);
      render(fluid_renderer, canvas.mesh);

      window.Present();
    }
  }
  LOG_DEMO(STAGE, "Leaving Main Loop..\n");

  LOG_DEMO(STAGE, "Finished!\n");
}

void FluidDemo::OnKeyPressed(
    graphics::window::KeyPressedEventArgs const & _args) {
  if (_args.key == graphics::Key::kF5) {
    // Reload shaders

    // FluidSimulator
    if (auto simulator_result = create_fluid_simulator()) {
      fluid_simulator = move(*simulator_result);
    } else {
      LOG_DEMO(ERROR, "Failed to create fluid simulator!\n");
      return;
    }

    // FluidVisualizer
    if (auto visualizer_result = create_fluid_visualizer()) {
      fluid_visualizer = move(*visualizer_result);
    } else {
      LOG_DEMO(ERROR, "Failed to create fluid visualizer!\n");
      return;
    }

    // FluidRenderer
    if (auto renderer_result = create_fluid_renderer()) {
      fluid_renderer = move(*renderer_result);
    } else {
      LOG_DEMO(ERROR, "Failed to create fluid renderer!\n");
      return;
    }
  }
}

}
