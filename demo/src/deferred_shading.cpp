#include "deferred_shading.h"

#include "core/time.hpp"
#include "graphics/resource/load_map.hpp"
#include "graphics/resource/load_model.h"
#include "graphics/resource/load_skybox.h"

////////////////////////////////////////////////////////////////////////////////
// demo
////////////////////////////////////////////////////////////////////////////////

namespace demo {

/*******************************************************************************
** Assets
*******************************************************************************/

core::Optional<core::Ref<TerrainResource>>
Assets::LoadTerrain(core::fs::Directory const & _terrain_directory) {
  core::Optional<graphics::Map> loaded_map =
      graphics::resource::load_map(_terrain_directory);
  if (!loaded_map) {
    CORE_LIB_LOG(ERROR, "Failed to load terrain from '%s'!\n",
                 cstr(_terrain_directory));
    return core::nullopt;
  }
  terrain = move(loaded_map->terrain);
  return terrain;
}

core::Optional<core::Ref<SkyboxResource>>
Assets::LoadSkybox(core::fs::Directory const & _skybox_directory) {
  core::Optional<graphics::Skybox> loaded_skybox =
      graphics::resource::load_skybox(_skybox_directory);
  if (!loaded_skybox) {
    CORE_LIB_LOG(ERROR, "Failed to load skybox from '%s'!\n",
                 cstr(_skybox_directory));
    return core::nullopt;
  }
  skybox = move(*loaded_skybox);
  return skybox;
}

core::Optional<core::Ref<StaticModelResource>>
Assets::LoadStaticModel(core::fs::Directory const & _static_model_directory) {
  core::Optional<graphics::StaticModel> loaded_static_model =
      graphics::resource::load_static_model(_static_model_directory);
  if (!loaded_static_model) {
    CORE_LIB_LOG(ERROR, "Failed to load static model from '%s'!\n",
                 cstr(_static_model_directory));
    return core::nullopt;
  }
  return static_models.Create(move(*loaded_static_model));
}

core::Optional<core::Ref<AnimatedModelResource>> Assets::LoadAnimatedModel(
    core::fs::Directory const & _animated_model_directory) {
  core::Optional<graphics::AnimatedModel> loaded_animated_model =
      graphics::resource::load_animated_model(_animated_model_directory);
  if (!loaded_animated_model) {
    CORE_LIB_LOG(ERROR, "Failed to load static model from '%s'!\n",
                 cstr(_animated_model_directory));
    return core::nullopt;
  }
  return animated_models.Create(move(*loaded_animated_model));
}

/*******************************************************************************
** StaticModelObject
*******************************************************************************/

StaticModelObject::StaticModelObject(
    core::Ref<StaticModelResource> _static_model,
    numerics::Transformation _transformation)
    : static_model(move(_static_model))
    , transformation(_transformation) {}

/*******************************************************************************
** AnimatedModelObject
*******************************************************************************/

AnimatedModelObject::AnimatedModelObject(
    core::Ref<AnimatedModelResource> _animated_model,
    numerics::Transformation _transformation)
    : animated_model(move(_animated_model))
    , animation(animated_model->skeleton_data,
                core::make_span(animated_model->animation_data))
    , transformation(_transformation) {}

/*******************************************************************************
** Scene
*******************************************************************************/

void Scene::SetTerrain(core::Ref<TerrainResource> _terrain) {
  terrain = _terrain;
}

void Scene::SetSkybox(core::Ref<SkyboxResource> _skybox) { skybox = _skybox; }

core::Ref<StaticModelObject>
Scene::CreateStaticModelObject(core::Ref<StaticModelResource> _static_model,
                               numerics::Transformation _transformation) {
  return static_models.Create(move(_static_model), _transformation);
}

core::Ref<AnimatedModelObject> Scene::CreateAnimatedModelObject(
    core::Ref<AnimatedModelResource> _animated_model,
    numerics::Transformation _transformation) {
  return animated_models.Create(move(_animated_model), _transformation);
}

void Scene::Clear() {
  terrain = core::nullopt;
  skybox = core::nullopt;
  static_models.Clear();
  animated_models.Clear();
}

/*******************************************************************************
** DeferredPipeline
*******************************************************************************/

DeferredPipeline::DeferredPipeline(
    graphics::Texture _color_buffer, graphics::Texture _normal_buffer,
    graphics::Texture _specular_buffer, graphics::RenderBuffer _depth_buffer,
    graphics::FrameBuffer _frame_buffer,
    graphics::ForwardTerrainRenderer _terrain_renderer,
    graphics::ForwardSkyboxRenderer _skybox_renderer,
    graphics::ForwardStaticModelRenderer _static_model_renderer,
    graphics::ForwardAnimatedModelRenderer _animated_model_renderer)
    : color_buffer(move(_color_buffer))
    , normal_buffer(move(_normal_buffer))
    , specular_buffer(move(_specular_buffer))
    , depth_buffer(move(_depth_buffer))
    , frame_buffer(move(_frame_buffer))
    , terrain_renderer(move(_terrain_renderer))
    , skybox_renderer(move(skybox_renderer))
    , static_model_renderer(move(static_model_renderer))
    , animated_model_renderer(move(animated_model_renderer)) {}

void DeferredPipeline::Render(Scene const & _scene,
                              graphics::Camera const & _camera) {
  // StaticModels
  if (!_scene.StaticModels().IsEmpty()) {
    graphics::bind(static_model_renderer);
    for (StaticModelObject const & static_object : _scene.StaticModels()) {
      graphics::TransformationMatrix transformation;
      numerics::set_matrix(transformation, static_object.Transformation());
      graphics::render(static_model_renderer, static_object.StaticModel(),
                       transformation, _camera);
    }
  }

  // AnimatedModels
  if (!_scene.AnimatedModels().IsEmpty()) {
    graphics::bind(animated_model_renderer);
    for (AnimatedModelObject const & animated_object :
         _scene.AnimatedModels()) {
      graphics::TransformationMatrix transformation;
      numerics::set_matrix(transformation, animated_object.Transformation());
      graphics::render(
          animated_model_renderer, animated_object.AnimatedModel(),
          transformation,
          core::make_span(animated_object.Animation().GetTransformations()),
          _camera);
    }
  }

  // Terrain
  if (_scene.Terrain().HasValue()) {
    graphics::bind(terrain_renderer);
    graphics::render(terrain_renderer, *_scene.Terrain().Value(), _camera);
  }

  // Skybox
  if (_scene.Skybox().HasValue()) {
    graphics::bind(skybox_renderer);
    graphics::render(skybox_renderer, *_scene.Skybox().Value(), _camera);
  }
}

/*******************************************************************************
** create_buffer_texture
*******************************************************************************/

graphics::Texture create_buffer_texture(uint32 _width, uint32 _height) {
  graphics::ColorFormat color_format = graphics::ColorFormat::ABGR8UI;
  graphics::TextureSettings settings;
  settings.generate_mipmap = false;
  settings.wrap_u = graphics::TextureWrapMode::Repeat;
  settings.wrap_v = graphics::TextureWrapMode::Repeat;
  settings.filter_minify = graphics::TextureFilterMode::Nearest;
  settings.filter_magnify = graphics::TextureFilterMode::Nearest;
  return graphics::create_texture(_width, _height, color_format,
                                  core::span<byte const>(), settings);
}

/*******************************************************************************
** create_deferred_pipeline
*******************************************************************************/

core::Optional<DeferredPipeline> create_deferred_pipeline(uint32 _width,
                                                          uint32 _height) {
  CORE_LIB_LOG(MESSAGE, "Creating deferred pipeline..\n");

  // color_buffer
  CORE_LIB_LOG(MESSAGE, "Creating color_buffer..\n");
  graphics::Texture color_buffer = create_buffer_texture(_width, _height);
  if (!color_buffer) {
    CORE_LIB_LOG(ERROR, "Failed to create color_buffer!\n");
    return core::nullopt;
  }

  // normal_buffer
  CORE_LIB_LOG(MESSAGE, "Creating normal_buffer..\n");
  graphics::Texture normal_buffer = create_buffer_texture(_width, _height);
  if (!normal_buffer) {
    CORE_LIB_LOG(ERROR, "Failed to create normal_buffer!\n");
    return core::nullopt;
  }

  // specular_buffer
  CORE_LIB_LOG(MESSAGE, "Creating specular_buffer..\n");
  graphics::Texture specular_buffer = create_buffer_texture(_width, _height);
  if (!specular_buffer) {
    CORE_LIB_LOG(ERROR, "Failed to create specular_buffer!\n");
    return core::nullopt;
  }

  // depth_buffer
  CORE_LIB_LOG(MESSAGE, "Creating depth_buffer..\n");
  graphics::RenderBuffer depth_buffer;
  if (!depth_buffer.Create(graphics::RenderbufferFormat::Depth24Stencil8,
                           _width, _height)) {
    CORE_LIB_LOG(ERROR, "Failed to create depth_buffer!\n");
    return core::nullopt;
  }

  // frame_buffer
  CORE_LIB_LOG(MESSAGE, "Creating frame_buffer..\n");
  graphics::FrameBuffer frame_buffer;
  if (!frame_buffer.Create()) {
    CORE_LIB_LOG(ERROR, "Failed to create frame_buffer!\n");
    return core::nullopt;
  }

  CORE_LIB_LOG(MESSAGE, "Attaching buffers..\n");
  frame_buffer.Bind();
  frame_buffer.Attach(0, color_buffer);
  frame_buffer.Attach(1, normal_buffer);
  frame_buffer.Attach(2, specular_buffer);
  frame_buffer.Attach(depth_buffer);

  // HACK
  GLenum attachments[3] = {GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1,
                           GL_COLOR_ATTACHMENT2};
  graphics::glDrawBuffers(3, attachments);
  // ~HACK

  frame_buffer.Unbind();

  // Renderers
  core::Optional<graphics::ForwardTerrainRenderer> terrain_renderer =
      graphics::create_forward_terrain_renderer();
  if (!terrain_renderer) {
    CORE_LIB_LOG(ERROR, "Failed to create terrain renderer!\n");
    return core::nullopt;
  }

  core::Optional<graphics::ForwardSkyboxRenderer> skybox_renderer =
      graphics::create_forward_skybox_renderer();
  if (!skybox_renderer) {
    CORE_LIB_LOG(ERROR, "Failed to create skybox renderer!\n");
    return core::nullopt;
  }

  core::Optional<graphics::ForwardStaticModelRenderer> static_model_renderer =
      graphics::create_forward_static_model_renderer();
  if (!static_model_renderer) {
    CORE_LIB_LOG(ERROR, "Failed to create static model renderer!\n");
    return core::nullopt;
  }

  core::Optional<graphics::ForwardAnimatedModelRenderer>
      animated_model_renderer =
          graphics::create_forward_animated_model_renderer();
  if (!animated_model_renderer) {
    CORE_LIB_LOG(ERROR, "Failed to create animated model renderer!\n");
    return core::nullopt;
  }

  return DeferredPipeline(move(color_buffer), move(normal_buffer),
                          move(specular_buffer), move(depth_buffer),
                          move(frame_buffer), move(*terrain_renderer),
                          move(*skybox_renderer), move(*static_model_renderer),
                          move(*animated_model_renderer));
}

/*******************************************************************************
** DeferredShading
*******************************************************************************/

bool DeferredShading::Initialize() {
  CORE_LIB_LOG(STAGE, "Initializing..\n");

  // context
  CORE_LIB_LOG(STAGE, "Initializing context..\n");
  if (!context.Initialize()) {
    CORE_LIB_LOG(ERROR, "Failed to initialize context!\n");
    return false;
  }

  // window
  CORE_LIB_LOG(MESSAGE, "Creating window..\n");
  if (!window.Create(const_string("deferred_shading"))) {
    CORE_LIB_LOG(ERROR, "Failed to create window!\n");
    return false;
  }

  // camera
  camera.SetPerspective(45.f, 16.f / 9.f, 0.1f, 100000.f);

  // deferred_pipeline
  {
    uint32 kWidth = 1920;
    uint32 kHeight = 1080;
    deferred_pipeline = create_deferred_pipeline(kWidth, kHeight);
    if (!deferred_pipeline) {
      CORE_LIB_LOG(ERROR, "Failed to create deferred pipeline!\n");
      return false;
    }
  }

  return true;
}

void DeferredShading::Load(core::fs::Directory const & _root) {
  CORE_LIB_LOG(LOG, "Loading data from '%s'\n", cstr(_root));
  // Map
  {
    core::fs::Directory map_directory = core::fs::Directory(_root / "map");
    if (map_directory.Exists()) {
      assets.LoadTerrain(map_directory);
    }
  }

  // Skybox
  {
    core::fs::Directory skybox_directory =
        core::fs::Directory(_root / "skybox");
    if (skybox_directory.Exists()) {
      assets.LoadSkybox(skybox_directory);
    }
  }

  // StaticModels
  {
    core::fs::Directory static_models_directory =
        core::fs::Directory(_root / "static_models");
    if (static_models_directory.Exists()) {
      for (auto entry : static_models_directory) {
        if (entry.Is<core::fs::Directory>()) {
          assets.LoadStaticModel(entry.As<core::fs::Directory>());
        }
      }
    }
  }

  // AnimatedModels
  {
    core::fs::Directory animated_models_directory =
        core::fs::Directory(_root / "animated_models");
    if (animated_models_directory.Exists()) {
      for (auto entry : animated_models_directory) {
        if (entry.Is<core::fs::Directory>()) {
          assets.LoadAnimatedModel(entry.As<core::fs::Directory>());
        }
      }
    }
  }

  // HACK
  numerics::Transformation transformation;
  numerics::set_identity(transformation);
  scene.CreateAnimatedModelObject(assets.AnimatedModels()[0], transformation);
  // ~HACK
}

void DeferredShading::Run() {
  CORE_LIB_LOG(STAGE, "Running..\n");
  core::HighResolutionClock clock;
  for (;;) {
    window.Run();
    if (window.IsDestroyed()) {
      break;
    }

    window.Clear();

    camera.Set(numerics::Vector3f(0.f, -100.f, -100.f),
               numerics::Vector3f(0.f, 0.f, 0.f));

    if (deferred_pipeline) {
      deferred_pipeline->Render(scene, camera);
    }

    window.Present();
  }
}

}
