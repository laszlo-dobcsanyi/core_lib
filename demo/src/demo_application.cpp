#include "demo_application.h"

#include "core/callable/wrapped_callable.hpp"

#include "deferred_shading.h"

////////////////////////////////////////////////////////////////////////////////
// demo
////////////////////////////////////////////////////////////////////////////////

namespace demo {

/*******************************************************************************
** DemoApplication
*******************************************************************************/

DemoApplication::DemoApplication(application::CommandLine _command_line)
    : command_line(move(_command_line)) {}

void DemoApplication::Run() {
  DeferredShading program;
  if (!program.Initialize()) {
    return;
  }
  program.Load(core::fs::Directory(
      core::fs::Directory(core::fs::DirectoryType::Executable) / "data"));
  program.Run();
}

}

////////////////////////////////////////////////////////////////////////////////
//                                                                                                //
////////////////////////////////////////////////////////////////////////////////

#if defined(CORE_LIB_OS_WINDOWS)
void demo_main(char * _command_line) {
  demo::DemoApplication application((application::CommandLine(_command_line)));
  application.Run();
}

int32 WINAPI WinMain(HINSTANCE _instance, HINSTANCE _previous_instance,
                     char * _command_line, int32 _show_cmd) {
  application::program_main(core::wrap_callable(demo_main), _command_line);
  return 0;
}
#else
void demo_main(char ** _command_line) {
  demo::DemoApplication application((application::CommandLine(_command_line)));
  application.Run();
}

int main(int, char ** _command_line) {
  application::program_main(core::wrap_callable(demo_main), _command_line);
  return 0;
}
#endif
