#include "test.h"
#include "core/containers.hpp"
#include "core/queries.hpp"

////////////////////////////////////////////////////////////////////////////////
// test
////////////////////////////////////////////////////////////////////////////////

namespace test {

/*******************************************************************************
** check_arrays
*******************************************************************************/

void check_arrays() {
  LOG_TEST(STAGE, "Running..\n");

  // container::InlineArray
  {
    container::InlineArray<float, 4u> array1;
    array1[0u] = 1.f;
    array1[1u] = 2.f;
    array1[2u] = 3.f;
    array1[3u] = 4.f;

    // Copy
    {
      container::InlineArray<float, 4u> array2 = array1;
      ASSERT_TEST(array2[0u] == 1.f);
      ASSERT_TEST(array2[1u] == 2.f);
      ASSERT_TEST(array2[2u] == 3.f);
      ASSERT_TEST(array2[3u] == 4.f);
    }
  }

  // container::PoolArray
  {
    container::PoolArray<float> pool_array;

    pool_array.Create(0.f);
    pool_array.Create(12.f);

    for (float & value : pool_array) {
    }

    container::PoolArray<float> const & const_pool_array = pool_array;
    for (float const & value : const_pool_array) {
    }
  }

  // container::Array
  { container::Array<float> heap_array; }

  LOG_TEST(STAGE, "Finished..\n");
}

/*******************************************************************************
** check_stacks
*******************************************************************************/

void check_stacks() {
  LOG_TEST(STAGE, "Running..\n");

  LOG_TEST(STAGE, "BoundedStack..\n");
  {
    container::BoundedStack<int, 64u> bounded_stack;
    for (auto current = 0u; current != 32; ++current) {
      bounded_stack.Push(current);
    }

    {
      auto stack_query = query::make_query(bounded_stack);
      while (stack_query.IsValid()) {
        auto & value = stack_query.Forward();
        LOG_TEST(MESSAGE, "%i\n", value);
      }
    }

    {
      container::BoundedStack<int, 64u> const & const_bounded_stack =
          bounded_stack;
      auto stack_query = query::make_query(const_bounded_stack);
      while (stack_query.IsValid()) {
        auto & value = stack_query.Forward();
        LOG_TEST(MESSAGE, "%i\n", value);
      }
    }
  }

  LOG_TEST(STAGE, "UnboundedStack..\n");
  { container::UnboundedStack<int> unbounded_stack; }

  LOG_TEST(STAGE, "Finished!\n");
}

/*******************************************************************************
** check_containers
*******************************************************************************/

void check_containers() {
  LOG_TEST(STAGE, "Running..\n");
  check_arrays();
  check_stacks();
  LOG_TEST(STAGE, "Finished!\n");
}

}
