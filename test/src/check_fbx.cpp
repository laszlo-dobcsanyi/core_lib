#include "test.h"
#include "core/time.hpp"
#include "core/fs/directory.h"
#include "graphics/api.h"
#include "graphics/window.h"
#include "graphics/create_mesh.hpp"
#include "graphics/resource/load_model.h"
#include "graphics/animation/skeletal_animation.h"
#include "graphics/renderers/forward_animated_model_renderer.h"
#include "graphics/renderers/forward_position_mesh_renderer.h"
#include "graphics/experimental/scene_controller.hpp"

////////////////////////////////////////////////////////////////////////////////
// test
////////////////////////////////////////////////////////////////////////////////

namespace test {

/*******************************************************************************
** Skeleton
*******************************************************************************/

class Skeleton {
public:
  using VertexType = graphics::PositionVertex;
  using MeshType = graphics::Mesh<VertexType, graphics::VertexDataType::Array>;

  MeshType mesh;

  Skeleton(graphics::animation::SkeletalAnimation & _skeletal_animation)
      : skeletal_animation(_skeletal_animation) {
    Update();
  }

  void Update() {
    if (mesh.IsAllocated()) {
      mesh.Deallocate();
    }

    using VertexDataType =
        graphics::VertexData<VertexType, graphics::VertexDataType::Array>;
    VertexDataType vertex_data;
    {
      auto & bones = skeletal_animation.GetSkeletonData().bones;

      CORE_LIB_ASSERT(0u < bones.Size());
      auto const bone_vertex_count = (bones.Size() - 1u) * 2u;

      auto & vertices = vertex_data.vertices;
      vertices =
          core::make_unique_array<graphics::PositionVertex>(bone_vertex_count);
      size_t vertex_index = 0u;

      size_t child = bones[0u].child;
      while (child != 0u) {
        CreateBone(0u, child, vertices, vertex_index);
        child = bones[child].next;
      }
      CORE_LIB_ASSERT(vertex_index == vertices.Size());
    }

    mesh = create_mesh(vertex_data);
  }

private:
  graphics::animation::SkeletalAnimation & skeletal_animation;

  void CreateBone(size_t _parent_index, size_t _index,
                  core::UniqueArray<graphics::PositionVertex> & _vertices,
                  size_t & _vertex_index) {
    auto & bones = skeletal_animation.GetSkeletonData().bones;

    auto const & transformations = skeletal_animation.GetTransformations();
    _vertices[_vertex_index++].position =
        numerics::Vector3f(transformations[_parent_index] *
                           bones[_parent_index].transformation[3u]);
    _vertices[_vertex_index++].position = numerics::Vector3f(
        transformations[_index] * bones[_index].transformation[3u]);

    size_t child = bones[_index].child;
    while (child != 0u) {
      CreateBone(_index, child, _vertices, _vertex_index);
      child = bones[child].next;
    }
  }
};

/*******************************************************************************
** Grid
*******************************************************************************/

using GridMesh =
    graphics::Mesh<graphics::PositionVertex, graphics::VertexDataType::Indexed>;

core::Optional<GridMesh> create_grid(uint32 _size, float _distance) {
  GridMesh mesh;
  {
    using VertexDataType =
        graphics::VertexData<graphics::PositionVertex,
                             graphics::VertexDataType::Indexed>;
    VertexDataType vertex_data;
    // Generate vertices
    {
      auto vertex_count = (_size + 1u) * (_size + 1u);
      auto & vertices = vertex_data.vertices;
      vertices =
          core::make_unique_array<graphics::PositionVertex>(vertex_count);
      auto offset = -(static_cast<float>(_size) * _distance) / 2.f;
      auto vertex_offset = numerics::Vector3f(offset, 0.f, offset);
      for (auto row = 0u; row < _size + 1u; ++row) {
        for (auto column = 0u; column < _size + 1u; ++column) {
          auto index = row * (_size + 1u) + column;
          vertices[index].position =
              numerics::Vector3f(static_cast<float>(column) * _distance, 0.f,
                                 static_cast<float>(row) * _distance);
          vertices[index].position += vertex_offset;
        }
      }
    }
    // Generate indices
    {
      size_t index = 0u;
      auto grid_count = _size * _size;
      auto index_count = grid_count * 4u + 2u * _size * 2u;
      auto & indices = vertex_data.indices;
      indices = core::make_unique_array<uint32>(index_count);
      for (auto row = 0u; row < _size; ++row) {
        for (auto column = 0u; column < _size; ++column) {
          auto vertex = row * (_size + 1u) + column;
          indices[index + 0u] = vertex;
          indices[index + 1u] = vertex + 1u;
          indices[index + 2u] = vertex;
          indices[index + 3u] = vertex + _size + 1u;
          index += 4u;
        }
      }
      for (auto current = 0u; current < _size; ++current) {
        indices[index + 0u] = (current + 1u) * (_size + 1u) - 1u;
        indices[index + 1u] = (current + 2u) * (_size + 1u) - 1u;
        index += 2u;
      }
      auto bottom = _size * (_size + 1u);
      for (auto current = 0u; current < _size; ++current) {
        indices[index + 0u] = bottom + current;
        indices[index + 1u] = bottom + current + 1u;
        index += 2u;
      }
    }
    mesh = create_mesh(vertex_data);
  }
  return mesh;
}

/*******************************************************************************
** check_fbx
*******************************************************************************/

void check_fbx() {
  using namespace graphics;

  LOG_TEST(STAGE, "Running..\n");

  // Context
  LOG_TEST(STAGE, "Initializing Context..\n");
  api::Context context;
  if (!context.Initialize()) {
    LOG_TEST(ERROR, "Failed to initialize context!\n");
    return;
  }

  // Window
  LOG_TEST(STAGE, "Initializing Window..\n");
  graphics::Window window;
  if (!window.Create(const_string("FBX_Test"))) {
    LOG_TEST(ERROR, "Failed to create window!\n");
    return;
  }

  core::fs::Directory data_directory = core::fs::Directory(
      core::fs::Directory(core::fs::DirectoryType::Executable) / "data");
  if (!data_directory.Exists()) {
    LOG_TEST(ERROR, "Data directory '%s' does not exists!\n",
             cstr(data_directory));
    return;
  }

  // AnimatedModel
  graphics::AnimatedModel animated_model;
  if (auto animated_model_result = resource::load_animated_model(
          core::fs::Directory(data_directory / "models" / "kachujin"))) {
    animated_model = move(*animated_model_result);
  } else {
    LOG_TEST(ERROR, "Failed to load animated model!\n");
    return;
  }

  // SkeletalAnimation
  animation::SkeletalAnimation skeletal_animation(
      animated_model.skeleton_data,
      core::make_span(animated_model.animation_data));

  // Skeleton
  Skeleton skeleton(skeletal_animation);

  // Grid
  GridMesh grid;
  if (auto grid_result = create_grid(16u, 32.f)) {
    grid = move(*grid_result);
  } else {
    LOG_TEST(ERROR, "Failed to create grid!\n");
    return;
  }

  // Animated Model Renderer
  LOG_TEST(STAGE, "Initializing animated model renderer..\n");
  ForwardAnimatedModelRenderer animated_model_renderer;
  if (auto animated_model_renderer_result =
          create_forward_animated_model_renderer()) {
    animated_model_renderer = move(*animated_model_renderer_result);
  } else {
    LOG_TEST(ERROR, "Failed to initialize animted model renderer!\n");
    return;
  }

  // ForwardPositionMeshRenderer
  LOG_TEST(STAGE, "Initializing position mesh renderer..\n");
  ForwardPositionMeshRenderer position_mesh_renderer;
  if (auto position_mesh_renderer_result =
          create_forward_position_mesh_renderer()) {
    position_mesh_renderer = move(*position_mesh_renderer_result);
  } else {
    LOG_TEST(ERROR, "Failed to initialize position mesh renderer!\n");
    return;
  }

  // Camera
  Camera camera;
  camera.SetPerspective();

  // SceneController
  graphics::SceneController scene_controller(core::make_ref(window),
                                             core::make_ref(camera));

  // Hack
  // graphics::glCullFace( GL_FRONT );
  // graphics::glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );
  graphics::glEnable(GL_DEPTH_TEST);

  // Hack
  animation::AnimationState * idle_animation = nullptr;
  for (animation::AnimationState & animation_state :
       skeletal_animation.GetAnimationStates()) {
    if (animation_state.GetAnimationData().name == "idle") {
      idle_animation = &animation_state;
      break;
    }
  }
  if (!idle_animation) {
    LOG_TEST(ERROR, "No idle animation can be found!\n");
    return;
  }
  bool loop = true;
  float rate = 1.f;
  skeletal_animation.Start(*idle_animation, loop, rate);
  idle_animation->SetWeight(1.f);

  // Main loop
  LOG_TEST(STAGE, "Entering Main Loop..\n");
  core::HighResolutionClock clock;
  for (;;) {
    window.Run();
    if (window.IsDestroyed()) {
      break;
    }

    // Update
    {
      auto elapsed = static_cast<float>(core::to_seconds(clock.Reset()));

      scene_controller.Update();

      skeletal_animation.Update(elapsed);
      skeletal_animation.Animate();

      skeleton.Update();
    }

    // Draw
    {
      window.Clear();

      // Draw grid
      {
        TransformationMatrix transformation;
        numerics::set_identity(transformation);
        render(position_mesh_renderer, grid, transformation,
               numerics::Vector4f(0.2f, 0.2f, 0.2f, 1.f), camera);
      }

      // Draw animated model
      {
        graphics::bind(animated_model_renderer);
        TransformationMatrix transformation;
        numerics::set_identity(transformation);
        render(animated_model_renderer, animated_model, transformation,
               core::make_span(skeletal_animation.GetTransformations()),
               camera);
      }

      // Draw skeleton
      {
        TransformationMatrix transformation;
        numerics::set_identity(transformation);
        render(position_mesh_renderer, skeleton.mesh, transformation,
               numerics::Vector4f(1.f, 1.f, 1.f, 1.f), camera);
      }

      window.Present();
    }
  }
  LOG_TEST(STAGE, "Leaving Main Loop..\n");

  LOG_TEST(STAGE, "Finished!\n");
}

}
