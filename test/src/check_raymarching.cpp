namespace test {
void check_raymarching() {}
}

#if 0

#  include "test.h"
#  include "graphics/base.hpp"
#  include "graphics/experimental/canvas.hpp"

////////////////////////////////////////////////////////////////////////////////
// test
////////////////////////////////////////////////////////////////////////////////

namespace test {

inline void check_raymarching() {
  LOG_TEST( STAGE, "Running..\n" );

  // Context
  LOG_TEST( STAGE, "Initializing Context..\n" );
  graphics::Context context;
  if ( !context.Initialize() ) {
    LOG_TEST( ERROR, "Failed to initialize context!\n" );
    return;
  }

  // Window
  LOG_TEST( STAGE, "Initializing Window..\n" );
  graphics::Window window;
  if ( !window.Create( const_string( "RaymarchingTest" ) ) ) {
    LOG_TEST( ERROR, "Failed to create window!\n" );
    return;
  }

  // Canvas
  graphics::experimental::Canvas canvas;
  canvas.Set( window.GetWidth(), window.GetHeight() );

  // Camera
  graphics::Camera camera;
  camera.Set( glm::vec3( 0.f, 0.f, -1.f ), glm::vec3( 0.f, 0.f, 0.f ) );

  // Shader
  const char * vertex_shader =
    "#version 330 core\n"
    "layout (location = 0) in vec3 position;\n"
    "out vec3 vertex_color;\n"
    "uniform mat4 transform;\n"
    "uniform float time;\n"
    "void main() {\n"
    "  gl_Position = transform * vec4( position, 1.f );\n"
    "  vertex_color = vec3( time, 0.5f, 0.5f );\n"
    "}\n";
  const char * fragment_shader =
    "#version 330 core\n"
    "in vec3 vertex_color;\n"
    "out vec4 color;\n"
    "uniform float time;\n"
    "\n"
    "float sphere( vec3 p, vec3 c, float r ) {\n"
    "  return length( p - c ) - r;\n"
    "}\n"
    "\n"
    "float f( vec3 p ) {\n"
    "  float sphere1 = sphere( p, vec3( 0.f, 25.f, 25.f ), 15.f );\n"
    "  float sphere2 = sphere( p, vec3( 0.f, 0.f, 0.f ), 1.f );\n"
    "  float sphere3 = sphere( p, vec3( 10.f, 0.f, 0.f), 5.f );\n"
    "  return min( min( sphere1, sphere2 ), sphere3 );\n"
    "}\n"
    "\n"
    "void main() {\n"
    "  vec3 camera = vec3( 0.f, 0.f, -time );\n"
    "  vec3 v = vec3( ( ( gl_FragCoord.x / 1920.f ) - 0.5f ) * 2.f, ( ( gl_FragCoord.y / 1080.f ) - 0.5f ) * 2.f * ( 1080.f / 1920.f ), 1920.f / 1000.f / 2.f );\n"
    "  vec3 dir = normalize( v );\n"
    "  float d = 0.f;\n"
    "  color = vec4( 0.f, 0.f, 0.f, 0.f );\n"
    "  do {\n"
    "    vec3 p = camera + dir * d;\n"
    "    float fp = f( p );\n"
    "    d = d + fp;\n"
    "    if ( fp <= 0.05f ) {\n"
    "      color = vec4( 1.f - d / 1000.f, 1.f - d / 1000.f, 1.f - d / 100.f, 0.f );\n"
    "      return;\n"
    "    }\n"
    "  } while ( d < 1000.f );\n"
    "}\n";
  graphics::Shader shader;
  if ( !shader.Create( vertex_shader, fragment_shader ) ) {
    LOG_TEST( ERROR, "Failed to create shader!\n" );
    return;
  }
  shader.Use();
  // Projection
  graphics::opengl::UniformLocationType transform_location = shader.GetUniformLocation( "transform" );
  graphics::glUniformMatrix4fv( transform_location, 1, GL_FALSE, glm::value_ptr( camera.view_projection ) );
  graphics::opengl::UniformLocationType time_location = shader.GetUniformLocation( "time" );
 
  // TODO: Hack!
  // graphics::glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );

  // Main loop
  auto frame = 0u;
  LOG_TEST( STAGE, "Entering Main Loop..\n" );
  for ( ;; ) {
    window.Run();
    if ( window.IsDestroyed() ) {
      break;
    }
    window.Clear();
    {
      const float time = frame / 10.f;
      canvas.Set( window.GetWidth(), window.GetHeight() );
      graphics::glUniform1f( time_location, time );

      graphics::opengl::draw( canvas.mesh.vertex_buffer, graphics::DrawMode::Triangles );
    }
    window.Present();
    ++frame;
  }
  LOG_TEST( STAGE, "Leaving Main Loop..\n" );

  LOG_TEST( STAGE, "Finished!\n" );
}

}

#endif
