#include "test.h"
#include "core/base.h"
#include "core/fs/directory.h"
#include "graphics/api.h"
#include "graphics/window.h"
#include "graphics/renderers/forward_static_model_renderer.h"
#include "graphics/experimental/canvas.hpp"
#include "graphics/resource/texture_from_file.hpp"

////////////////////////////////////////////////////////////////////////////////
// test
////////////////////////////////////////////////////////////////////////////////

namespace test {

struct State {
  graphics::ForwardStaticModelRenderer renderer;
  graphics::StaticModel model;
  graphics::Camera camera;
};

/*******************************************************************************
** check_image
*******************************************************************************/

void check_image() {
  LOG_TEST(STAGE, "Running..\n");

  auto data_directory = core::fs::Directory(
      core::fs::Directory(core::fs::DirectoryType::Executable) /
      "./../../data");

  // Context
  LOG_TEST(STAGE, "Initializing Context..\n");
  graphics::api::Context api_context;
  if (!api_context.Initialize()) {
    LOG_TEST(ERROR, "Failed to initialize context!\n");
    return;
  }

  // Window
  LOG_TEST(STAGE, "Initializing Window..\n");
  graphics::Window window;
  if (!window.Create(const_string("Image Test"))) {
    LOG_TEST(ERROR, "Failed to create window!\n");
    return;
  }

  State state;

  // Static model renderer
  if (auto static_model_renderer_result =
          graphics::create_forward_static_model_renderer()) {
    state.renderer = move(*static_model_renderer_result);
  } else {
    LOG_TEST(ERROR, "Failed to create static model renderer!\n");
    return;
  }

  // Static model
  graphics::StaticModel static_model;
  {
    // Canvas
    {
      graphics::experimental::Canvas<graphics::TexturedVertex> canvas;
      auto const width = 1.f;
      auto const height = 1.f;

      container::InlineArray<graphics::TexturedVertex, 4u> vertices;
      vertices[0u].position = numerics::Vector3f(-width, +height, 0.f);
      vertices[0u].normal = numerics::Vector3f(0.f, 0.f, -1.f);
      vertices[0u].texture_coordinate = numerics::Vector2f(1.f, 0.f);
      vertices[1u].position = numerics::Vector3f(-width, -height, 0.f);
      vertices[1u].normal = numerics::Vector3f(0.f, 0.f, -1.f);
      vertices[1u].texture_coordinate = numerics::Vector2f(1.f, 1.f);
      vertices[2u].position = numerics::Vector3f(+width, -height, 0.f);
      vertices[2u].normal = numerics::Vector3f(0.f, 0.f, -1.f);
      vertices[2u].texture_coordinate = numerics::Vector2f(0.f, 1.f);
      vertices[3u].position = numerics::Vector3f(+width, +height, 0.f);
      vertices[3u].normal = numerics::Vector3f(0.f, 0.f, -1.f);
      vertices[3u].texture_coordinate = numerics::Vector2f(0.f, 0.f);

      canvas.Set(core::make_span(vertices));
      static_model.mesh = move(canvas.mesh);
    }

    // Texture
    core::fs::FilePath texture_path(data_directory /
                                    "textures/kachujin/diffuse.tga");
    graphics::Texture texture = graphics::resource::texture_from_file(
        texture_path, graphics::TextureSettings());
    if (!texture) {
      LOG_TEST(ERROR, "Failed to load image!\n");
      return;
    }

    state.model = move(static_model);
  }

  // Camera
  state.camera.SetPerspective();

  // TODO: Hack..
  // graphics::glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );
  graphics::glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  graphics::glEnable(GL_BLEND);

  // Main loop
  LOG_TEST(STAGE, "Entering Main Loop..\n");
  for (;;) {
    window.Run();
    if (window.IsDestroyed()) {
      break;
    }
    window.Clear();

    state.camera.Set(numerics::Vector3f(0.f, 0.f, -3.f),
                     numerics::Vector3f(0.f, 0.f, 0.f));

    {
      graphics::bind(state.renderer);

      graphics::TransformationMatrix transform;
      numerics::set_identity(transform);
      graphics::render(state.renderer, state.model, transform, state.camera);
    }

    window.Present();
  }
  LOG_TEST(STAGE, "Leaving Main Loop..\n");

  LOG_TEST(STAGE, "Finished!\n");
}

}
