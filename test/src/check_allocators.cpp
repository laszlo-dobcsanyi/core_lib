#include "test.h"
#include "core/base.h"
#include "core/allocators.hpp"
#include "core/numerics.hpp"

////////////////////////////////////////////////////////////////////////////////
// test
////////////////////////////////////////////////////////////////////////////////

namespace test {

class TestClass {
public:
  int p1;
  int p2;
  char p3;
  float p4;
};

using PageAllocator = allocator::PageAllocator<mpl::power_of_2<13u>::value>;
template<typename T>
using TestPageAllocator = allocator::TypeAllocator<T, PageAllocator>;

/*******************************************************************************
** check_allocators
*******************************************************************************/

void check_allocators() {
  LOG_TEST(STAGE, "Running!\n");

  {
    LOG_TEST(STAGE, "Checking heap allocator..\n");
    allocator::HeapAllocator heap_allocator;
    allocator::Allocation allocations[64u];
    for (auto current = 0u; current < 64u; ++current) {
      allocations[current] = heap_allocator.Allocate(2312u);
      LOG_TEST(VERBOSE, "Allocated: %zu\n", allocations[current].Size());
    }
    for (auto current = 0u; current < 64u; ++current) {
      LOG_TEST(VERBOSE, "Dellocating: %zu\n", allocations[current].Size());
      heap_allocator.Deallocate(move(allocations[current]));
    }
  }

  {
    LOG_TEST(STAGE, "Checking interval allocator..\n");
    numerics::XorShiftGenerator_32 rng(12);
    allocator::IntervalAllocator<
        256u, allocator::PageAllocator<::mpl::power_of_2<13u>::value>,
        allocator::HeapAllocator>
        interval_allocator;
    size_t page_allocated = 0u;
    allocator::Allocation allocations[64u];
    for (auto current = 0u; current < 64u; ++current) {
      uint32 allocation_size = rng.Next() % U32(512);
      if (allocation_size <= 256u) {
        page_allocated += allocation_size;
      }
      LOG_TEST(MESSAGE, "Allocate %" PRIu32 " (%zu)..\n", allocation_size,
               page_allocated);
      allocations[current] = interval_allocator.Allocate(allocation_size);
      LOG_TEST(MESSAGE, "Interval allocated: %zu\n",
               allocations[current].Size());
    }
    for (auto current = 0u; current < 64u; ++current) {
      interval_allocator.Deallocate(move(allocations[current]));
    }
  }

  LOG_TEST(MESSAGE, "Finished!\n");
}

}
