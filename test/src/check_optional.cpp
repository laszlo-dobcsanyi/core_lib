#include "test.h"
#include "core/optional.hpp"

////////////////////////////////////////////////////////////////////////////////
// test
////////////////////////////////////////////////////////////////////////////////

namespace test {

/*******************************************************************************
** check_optional
*******************************************************************************/

void check_optional() {
  LOG_TEST(STAGE, "Running..\n");
  {
    core::Optional<int *> optional;
    if (optional.HasValue()) {
      if (int * ptr = optional.Value()) {
        *ptr = 8;
      }
    }
  }
  {
    core::Optional<int *> optional(nullptr);
    if (optional.HasValue()) {
      if (int * ptr = optional.Value()) {
        *ptr = 8;
      }
    }
  }
  {
    int x = 0;
    core::Optional<int *> optional(&x);
    if (optional.HasValue()) {
      if (int * ptr = optional.Value()) {
        *ptr = 8;
      }
    }
  }

  {
    core::Optional<int> optional(42);
    for (int & value : core::make_span(optional)) {
      value = 4;
    }
    for (int & value : optional) {
      value = 8;
    }
  }
  LOG_TEST(STAGE, "Finished!\n");
}

}
