#include "test.h"
#include "graphics/api.h"
#include "graphics/window.h"
#include "graphics/camera.h"
#include "graphics/pipeline.hpp"
#include "graphics/renderers/forward_static_model_renderer.h"
#include "graphics/renderers/forward_animated_model_renderer.h"
#include "graphics/renderers/forward_terrain_renderer.h"
#include "graphics/renderers/forward_skybox_renderer.h"
#include "graphics/animation/skeletal_animation.h"

#define LOG_GRAPHICS_TEST(__severity__, ...)                                   \
  CORE_LIB_LOG(__severity__, __VA_ARGS__)

////////////////////////////////////////////////////////////////////////////////
// test
////////////////////////////////////////////////////////////////////////////////

namespace test {

struct ForwardPipelineState {
  graphics::Camera camera;
};

struct TestStaticObject {
  graphics::StaticModel model;
  graphics::TransformationMatrix transformation;
};

struct TestAnimatedObject {
  graphics::AnimatedModel model;
  graphics::animation::SkeletalAnimation animation;
  graphics::TransformationMatrix transformation;
};

struct TestScene {
  core::UniqueArray<TestStaticObject> static_objects;
  core::UniqueArray<TestAnimatedObject> animated_objects;
  core::Optional<graphics::Terrain> terrain;
  core::Optional<graphics::Skybox> skybox;
};

auto forward_static_object_renderer() {
  return graphics::object_renderer(
      [](TestScene const & _scene) {
        return core::make_span(_scene.static_objects);
      },
      graphics::adapt_renderer(
          graphics::ForwardStaticModelRenderer(),
          [](graphics::ForwardStaticModelRenderer & _renderer,
             ForwardPipelineState & _pipeline_state,
             TestStaticObject const & _static_object) {
            graphics::render(_renderer, _static_object.model,
                             _static_object.transformation,
                             _pipeline_state.camera);
          }));
}

auto forward_animated_object_renderer() {
  return graphics::object_renderer(
      [](TestScene const & _scene) {
        return core::make_span(_scene.animated_objects);
      },
      graphics::adapt_renderer(
          graphics::ForwardAnimatedModelRenderer(),
          [](graphics::ForwardAnimatedModelRenderer & _renderer,
             ForwardPipelineState & _pipeline_state,
             TestAnimatedObject const & _animated_object) {
            graphics::render(
                _renderer, _animated_object.model,
                _animated_object.transformation,
                core::make_span(
                    _animated_object.animation.GetTransformations()),
                _pipeline_state.camera);
          }));
}

auto forward_terrain_renderer() {
  return graphics::object_renderer(
      [](TestScene const & _scene) { return core::make_span(_scene.terrain); },
      graphics::adapt_renderer(graphics::ForwardTerrainRenderer(),
                               [](graphics::ForwardTerrainRenderer & _renderer,
                                  ForwardPipelineState & _pipeline_state,
                                  graphics::Terrain const & _terrain) {
                                 graphics::render(_renderer, _terrain,
                                                  _pipeline_state.camera);
                               }));
}

auto forward_skybox_renderer() {
  return graphics::object_renderer(
      [](TestScene const & _scene) { return core::make_span(_scene.skybox); },
      graphics::adapt_renderer(graphics::ForwardSkyboxRenderer(),
                               [](graphics::ForwardSkyboxRenderer & _renderer,
                                  ForwardPipelineState & _pipeline_state,
                                  graphics::Skybox const & _skybox) {
                                 graphics::render(_renderer, _skybox,
                                                  _pipeline_state.camera);
                               }));
}

auto forward_pipeline() {
  return graphics::pipeline(graphics::pass(
      forward_static_object_renderer(), forward_animated_object_renderer(),
      forward_terrain_renderer(), forward_skybox_renderer()));
}

using ForwardPipeline = decltype(forward_pipeline());

/*******************************************************************************
** check_graphics
*******************************************************************************/

void check_graphics() {
  LOG_GRAPHICS_TEST(STAGE, "Running..\n");

  // Context
  LOG_GRAPHICS_TEST(STAGE, "Initializing Context..\n");
  graphics::api::Context api_context;
  if (!api_context.Initialize()) {
    LOG_GRAPHICS_TEST(ERROR, "Failed to initialize context!\n");
    return;
  }

  // Window
  LOG_GRAPHICS_TEST(STAGE, "Initializing Window..\n");
  graphics::Window window;
  if (!window.Create(const_string("GraphicsTest"))) {
    LOG_GRAPHICS_TEST(ERROR, "Failed to create window!\n");
    return;
  }

  // Scene
  TestScene test_scene;

  // ForwardPipelineState
  ForwardPipelineState pipeline_state;
  pipeline_state.camera.SetPerspective();

  // ForwardPipeline
  ForwardPipeline pipeline = forward_pipeline();

  // Main loop
  LOG_GRAPHICS_TEST(STAGE, "Entering Main Loop..\n");
  for (;;) {
    window.Run();
    if (window.IsDestroyed()) {
      break;
    }
    window.Clear();

    graphics::render(pipeline, pipeline_state, test_scene);

    window.Present();
  }
  LOG_GRAPHICS_TEST(STAGE, "Leaving Main Loop..\n");

  LOG_GRAPHICS_TEST(STAGE, "Finished!\n");
}

}
