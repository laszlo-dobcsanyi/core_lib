#include "test.h"

#include "core/ranges/span.hpp"
#include "core/containers.hpp"

////////////////////////////////////////////////////////////////////////////////
// test
////////////////////////////////////////////////////////////////////////////////

namespace test {

/*******************************************************************************
** check_ranges
*******************************************************************************/

void check_ranges() {
  LOG_TEST(STAGE, "Running..\n");

  // span
  {
    // span from InlineArray
    {
      container::InlineArray<int, 8> array;
      core::span<int> elements = core::make_span(array);
      for (int & value : elements) {
      }

      container::InlineArray<int, 8> const & const_array = array;
      core::span<int const> const_elements = core::make_span(const_array);
      for (int const & value : const_elements) {
      }
    }

    // span from Array
    {
      container::Array<int> array;
      core::span<int> elements = core::make_span(array);
      for (int & value : elements) {
      }

      container::Array<int> const & const_array = array;
      core::span<int const> const_elements = core::make_span(const_array);
      for (int const & value : const_elements) {
      }
    }

    // span from array
    {
      int array[4] = {0, 1, 2, 3};
      core::span<int> elements = core::make_span(array);
    }

    // span from pointer and size
    {
      int array[4] = {0, 1, 2, 3};
      core::span<int> elements = core::make_span(array, 4);
    }
  }

  LOG_TEST(STAGE, "Finidhed!\n");
}

}
