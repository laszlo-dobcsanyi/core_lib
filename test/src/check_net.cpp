#include "test.h"
#include "net/tcp/listener.h"
#include "net/tcp/connector.h"
#include "net/tcp/connection.h"
#include "net/udp/connection.h"

#define NET_TEST_LOG(__severity__, ...) LOG_TEST(__severity__, __VA_ARGS__)

////////////////////////////////////////////////////////////////////////////////
// test
////////////////////////////////////////////////////////////////////////////////

namespace test {

/*******************************************************************************
** check_net
*******************************************************************************/

void check_net() {
  NET_TEST_LOG(STAGE, "Running..\n");
  NET_TEST_LOG(STAGE, "Finished!\n");
}

}
