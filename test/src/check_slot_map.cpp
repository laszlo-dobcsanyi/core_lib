#include "test.h"
#include "core/containers/slot_map.hpp"

////////////////////////////////////////////////////////////////////////////////
// test
////////////////////////////////////////////////////////////////////////////////

namespace test {

/*******************************************************************************
** check_slot_map
*******************************************************************************/

void check_slot_map() {
  LOG_TEST(STAGE, "Running..\n");
  {
    core::container::SlotMapId ids[16u];
    core::container::SlotMap<int> slot_map;
    {
      CORE_LIB_LOG(LOG, "Empty..\n");
      for (int & value : slot_map) {
        CORE_LIB_LOG(LOG, "\t\tValue %d\n", value);
      }
    }
    {
      CORE_LIB_LOG(LOG, "Creating..\n");
      for (auto current = 0u; current < 8u; ++current) {
        ids[current] = slot_map.Create(current);

        CORE_LIB_LOG(LOG, "\tAfter create..\n");
        for (int & value : slot_map) {
          CORE_LIB_LOG(LOG, "\t\tValue %d\n", value);
        }
      }
    }
    {
      CORE_LIB_LOG(LOG, "Destroying..\n");
      for (auto current = 0u; current < 8u; current += 2) {
        slot_map.Destroy(ids[current]);

        CORE_LIB_LOG(LOG, "\tAfter destroy %d..\n", current);
        for (int & value : slot_map) {
          CORE_LIB_LOG(LOG, "\t\tValue %d\n", value);
        }
      }
    }
    {
      CORE_LIB_LOG(LOG, "Destroying..\n");
      slot_map.Destroy(ids[1u]);

      CORE_LIB_LOG(LOG, "\tAfter destroy..\n");
      for (int & value : slot_map) {
        CORE_LIB_LOG(LOG, "\t\tValue %d\n", value);
      }
    }
    {
      CORE_LIB_LOG(LOG, "Creating..\n");
      for (auto current = 8u; current < 16u; ++current) {
        ids[current] = slot_map.Create(current);

        CORE_LIB_LOG(LOG, "\tAfter create..\n");
        for (int & value : slot_map) {
          CORE_LIB_LOG(LOG, "\t\tValue %d\n", value);
        }
      }
    }
    {
      CORE_LIB_LOG(LOG, "Destroying..\n");
      slot_map.Destroy(ids[5u]);

      CORE_LIB_LOG(LOG, "\tAfter destroy..\n");
      for (int & value : slot_map) {
        CORE_LIB_LOG(LOG, "\t\tValue %d\n", value);
      }
    }
  }
  LOG_TEST(STAGE, "Finished!\n");
}

}
