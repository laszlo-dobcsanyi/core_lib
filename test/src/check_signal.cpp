#include "test.h"

#include "core/signal/signal.hpp"

////////////////////////////////////////////////////////////////////////////////
// test
////////////////////////////////////////////////////////////////////////////////

namespace test {

class FooInterface {
public:
  virtual ~FooInterface() = default;

  virtual void Print() = 0;
  virtual void Print2(int _value) = 0;
  virtual int Calculate(int _value) = 0;
};

void check_signal() {
  LOG_TEST(STAGE, "Running..\n");

  {
    core::Signal<void()> signal;

    signal();

    signal.connect([]() { CORE_LIB_LOG(LOG, "Hello signal!\n"); });

    signal();

    int value = 0;
    signal.connect([&value]() {
      ++value;
      CORE_LIB_LOG(LOG, "Value is %d!\n", value);
    });

    signal();

    signal();

    class Foo {
    public:
      void Callback() { CORE_LIB_LOG(LOG, "Callback!\n"); }
    };
    Foo foo;

    {
      using SlotType = core::Slot<void()>;
      using DelegateType = core::Delegate<void()>;
      SlotType slot(DelegateType::Bind(&foo).Method<&Foo::Callback>());
      signal.connect(slot);

      signal();

      signal.connect([]() { CORE_LIB_LOG(LOG, "Top!\n"); });

      signal();
    }

    // signal.disconnect( slot );

    signal();
  }

  {
    core::Signal<FooInterface> signal;

    class Foo1 : public core::Slot<FooInterface> {
      virtual void Print() override { CORE_LIB_LOG(LOG, "Foo1::Print!\n"); }
      virtual void Print2(int _value) override {
        CORE_LIB_LOG(LOG, "Foo1::Print2 %d!\n", Calculate(_value));
      }
      virtual int Calculate(int _value) override { return _value + 1; };
    };

    class Foo2 : public core::Slot<FooInterface> {
      virtual void Print() override { CORE_LIB_LOG(LOG, "Foo2::Print!\n"); }
      virtual void Print2(int _value) override {
        CORE_LIB_LOG(LOG, "Foo2::Print2 %d!\n", Calculate(_value));
      }
      virtual int Calculate(int _value) override { return _value + 42; };
    };

    {
      Foo1 foo1;
      signal.connect(foo1);

      signal(&FooInterface::Print);

      {
        Foo2 foo2;
        signal.connect(foo2);
        signal(&FooInterface::Print);

        signal(&FooInterface::Print2, 42);

        int result = signal(&FooInterface::Calculate, 8);
        CORE_LIB_LOG(MESSAGE, "Calculate result is %d!\n", result);
      }

      signal(&FooInterface::Print2, 3);
    }
  }

  LOG_TEST(STAGE, "Finished!\n");
}

}
