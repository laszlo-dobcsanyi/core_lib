#include "test.h"
#include "core/collections/intrusive_list.hpp"

////////////////////////////////////////////////////////////////////////////////
// test
////////////////////////////////////////////////////////////////////////////////

namespace test {

/*******************************************************************************
** check_intrusive_list
*******************************************************************************/

void check_intrusive_list() {
  LOG_TEST(STAGE, "Running..\n");

  LOG_TEST(STAGE, "Inherited policy tests..\n");
  {
    struct Item : public core::intrusive_list_node {
      int value = 0;

      Item() = default;
      Item(int _value)
          : value(_value) {}
    };

    core::intrusive_list<Item> item_list;
    {
      Item items[] = {0, 1, 2};
      ASSERT_TEST(item_list.empty());
      item_list.push_front(items[1]);
      ASSERT_TEST(!item_list.empty());
      item_list.push_back(items[2]);
      item_list.push_front(items[0]);

      for (Item & item : item_list) {
        if (item.value == 1) {
          item_list.erase(item);
        }
      }

      auto const & const_item_list = item_list;
      ASSERT_TEST(!const_item_list.empty());
      ASSERT_TEST(const_item_list.contains(items[0]));
      ASSERT_TEST(!const_item_list.contains(items[1]));
      ASSERT_TEST(const_item_list.contains(items[2]));
      for (const Item & item : const_item_list) {
        ASSERT_TEST(item.value == 0 || item.value == 2);
      }

      item_list.push_back(items[1]);
      for (auto it = item_list.begin(); it != item_list.end();) {
        if ((it->value % 2) == 0) {
          it = item_list.erase(it);
          continue;
        }
        it = ++it;
      }
    }
    ASSERT_TEST(item_list.empty());
  }

  LOG_TEST(STAGE, "Member policy tests..\n");
  {
    struct Item {
      int value = 0;
      core::intrusive_list_node node;

      Item() = default;
      Item(int _value)
          : value(_value) {}
    };

    core::intrusive_list<Item, core::member_policy<Item, &Item::node>>
        item_list;
    {
      Item items[] = {0, 1, 2};
      ASSERT_TEST(item_list.empty());
      item_list.push_front(items[1]);
      ASSERT_TEST(!item_list.empty());
      item_list.push_back(items[2]);
      item_list.push_front(items[0]);

      for (Item & item : item_list) {
        if (item.value == 1) {
          item_list.erase(item);
        }
      }

      auto const & const_item_list = item_list;
      ASSERT_TEST(!const_item_list.empty());
      ASSERT_TEST(const_item_list.contains(items[0]));
      ASSERT_TEST(!const_item_list.contains(items[1]));
      ASSERT_TEST(const_item_list.contains(items[2]));
      for (const Item & item : const_item_list) {
        ASSERT_TEST(item.value == 0 || item.value == 2);
      }

      item_list.push_back(items[1]);
      for (auto it = item_list.begin(); it != item_list.end();) {
        if ((it->value % 2) == 0) {
          it = item_list.erase(it);
          continue;
        }
        it = ++it;
      }
    }
    ASSERT_TEST(item_list.empty());
  }

  LOG_TEST(STAGE, "Finished!\n");
}

/*******************************************************************************
** check_collections
*******************************************************************************/

void check_collections() {
  LOG_TEST(STAGE, "Running..\n");

  check_intrusive_list();

  LOG_TEST(STAGE, "Finished!\n");
}

}
