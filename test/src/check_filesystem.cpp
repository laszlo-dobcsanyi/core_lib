#include "test.h"
#include "core/fs/filepath.h"
#include "core/fs/directory.h"
#include "core/fs/mapped_file.hpp"

////////////////////////////////////////////////////////////////////////////////
// test
////////////////////////////////////////////////////////////////////////////////

namespace test {

/*******************************************************************************
** check_filesystem
*******************************************************************************/

void check_filesystem() {
  using namespace core::fs;
  LOG_TEST(STAGE, "Running..\n");

  Directory current_directory;
  LOG_TEST(MESSAGE, "Current working directory is %s\n",
           cstr(current_directory));

#if 0
  LOG_TEST( STAGE, "Text file..\n" );
  {
    const_string text_file( "text_file.txt" );

    if ( current_directory.Exists( text_file ) ) {
      LOG_TEST( MESSAGE, "'%s' already exists!\n", text_file.Get() );
    } else {
      LOG_TEST( MESSAGE, "'%s' does not exists!\n", text_file.Get() );
    }

    {
      LOG_TEST( MESSAGE, "empty ansi BinaryFileWriter..\n" );
      {
        ansi::LittleEndianBinaryFileWriter file;
        if ( !file.Open( text_file ) ) {
          LOG_TEST( ERROR, "Error while opening '%s' for write!\n", cstr( text_file ) );
          return;
        }
      }

      LOG_TEST( MESSAGE, "empty ansi BinaryFileReader..\n" );
      {
        ansi::LittleEndianBinaryFileReader file;
        if ( file.Open( text_file ) ) {
          const_string line;
          while ( file.Read( line ) ) {
            LOG_TEST( MESSAGE, "'%s'\n", cstr( line ) );
          }
        } else {
          LOG_TEST( ERROR, "Error while opening '%s' for read!\n", cstr( text_file ) );
          return;
        }
      }
    }

    {
      const_string strings[] = { "QQqqQ" };
      LOG_TEST( MESSAGE, "one line ansi BinaryFileWriter..\n" );
      {
        ansi::LittleEndianBinaryFileWriter file;
        if ( !file.Open( text_file ) ) {
          for ( auto current = 0u; current < sizeof( strings ) / sizeof( strings[ 0u ] ); ++current ) {
            file.Write( strings[ current ] );
          }
          LOG_TEST( ERROR, "Error while opening '%s' for write!\n", cstr( text_file ) );
          return;
        }
      }

      LOG_TEST( MESSAGE, "one line ansi BinaryFileReader..\n" );
      {
        ansi::LittleEndianBinaryFileReader file;
        if ( file.Open( text_file ) ) {
          const_string line;
          for ( auto current = 0u; current < sizeof( strings ) / sizeof( strings[ 0u ] ); ++current ) {
            CORE_LIB_ASSERT( file.Read( line ) );
            CORE_LIB_ASSERT( line == strings[ current ] );
          }
          while ( file.Read( line ) ) {
            LOG_TEST( MESSAGE, "'%s'\n", cstr( line ) );
          }
        } else {
          LOG_TEST( ERROR, "Error while opening '%s' for read!\n", cstr( text_file ) );
          return;
        }
      }
    }

    {
      const_string strings[] = { "X", "YY\n", "ZZZ", "WWWW", "1234567890" };
      LOG_TEST( MESSAGE, "multi line ansi BinaryFileWriter..\n" );
      {
        ansi::LittleEndianBinaryFileWriter file;
        if ( !file.Open( text_file ) ) {
          for ( auto current = 0u; current < sizeof( strings ) / sizeof( strings[ 0u ] ); ++current ) {
            file.Write( strings[ current ] );
          }
          LOG_TEST( ERROR, "Error while opening '%s' for write!\n", cstr( text_file ) );
          return;
        }
      }

      LOG_TEST( MESSAGE, "multi line ansi BinaryFileReader..\n" );
      {
        ansi::LittleEndianBinaryFileReader file;
        if ( file.Open( text_file ) ) {
          const_string line;
          for ( auto current = 0u; current < sizeof( strings ) / sizeof( strings[ 0u ] ); ++current ) {
            std::printf( "%u\n", current );
            CORE_LIB_ASSERT( file.Read( line ) );
            CORE_LIB_ASSERT( line == strings[ current ] );
          }
          while ( file.Read( line ) ) {
            LOG_TEST( MESSAGE, "'%s'\n", cstr( line ) );
          }
        } else {
          LOG_TEST( ERROR, "Error while opening '%s' for read!\n", cstr( text_file ) );
          return;
        }
      }
    }

    if ( file_exists( text_file ) ) {
      LOG_TEST( MESSAGE, "Removing '%s'..\n", cstr( text_file ) );
      if ( !remove( NativeString( cstr( text_file ) ) ) ) {
        LOG_TEST( ERROR, "Failed to remove '%s'!\n", cstr( text_file ) );
      }
    }
  }
#endif

  LOG_TEST(STAGE, "Binary file..\n");
  {}

  LOG_TEST(MESSAGE, "MappedFile..\n");
  {
    const_string test_file("test_file.txt");
    MappedFile mapped_file;
    if (mapped_file.Open(FilePath(test_file), OpenMode::ReadOnly)) {
      auto mapping = mapped_file.GetMappingReader(0, 1024);
      LOG_TEST(MESSAGE, "Mapped @ %p\n",
               (void *)mapping.RemainingBytes().data());
    } else {
      LOG_TEST(ERROR, "Failed to open mapped file!\n");
    }
  }

  LOG_TEST(MESSAGE, "DirectoryIterator..\n");
  {
    Directory home(DirectoryType::Home);
    for (auto entry : home) {
      if (auto file = entry.Get<FilePath>()) {
        LOG_TEST(MESSAGE, "File: %s\n", cstr(*file));
      } else if (auto directory = entry.Get<Directory>()) {
        LOG_TEST(MESSAGE, "Directory: %s\n", cstr(*directory));
      }
    }
  }

  LOG_TEST(STAGE, "Finished!\n");
}

}
