#include "test.h"
#include "core/fs/directory.h"
#include "graphics/resource/image/tga.h"
#include "graphics/resource/map_data_from_file.hpp"

////////////////////////////////////////////////////////////////////////////////
// test
////////////////////////////////////////////////////////////////////////////////

namespace test {

/*******************************************************************************
** generate_map
*******************************************************************************/

inline bool generate_map(float _scale, float _height,
                         core::fs::FilePath const & _height_map_path,
                         core::fs::FilePath const & _file_path) {
  using namespace graphics;
  using namespace graphics::resource;

  core::Optional<ImageData> height_map_data = tga::load(_height_map_path);
  if (!height_map_data) {
    LOG_TEST(ERROR, "Failed to load height map!\n");
    return false;
  }

  CORE_LIB_ASSERT(height_map_data->Format() == ColorFormat::ABGR8UI);
  CORE_LIB_ASSERT(height_map_data->Width() == height_map_data->Height());

  MapData map_data;
  map_data.grid.size = height_map_data->Width();
  map_data.grid.scale = _scale;
  map_data.grid.height = _height;
  map_data.grid.height_map =
      core::make_unique_array<float>(map_data.grid.size * map_data.grid.size);
  for (auto row = 0u; row < map_data.grid.size; ++row) {
    uint32 * row_data =
        reinterpret_cast<uint32 *>(height_map_data->GetRowBytes(row).data());
    for (auto column = 0u; column < map_data.grid.size; ++column) {
      float height =
          static_cast<float>(row_data[column] & U32(0x000000FF)) / 255.f;
      // CORE_LIB_LOG( LOG, "(%zu, %zu) %" PRIu32 " -> %.4f\n", column, row, row_data[ column ] & U32( 0x000000FF ), height );
      map_data.grid.height_map[row * map_data.grid.size + column] =
          height * _height;
    }
  }
  return save_map(map_data, _file_path, MapSaveMode::Binary);
}

/*******************************************************************************
** check_map
*******************************************************************************/

void check_map() {
  LOG_TEST(STAGE, "Running..\n");
  {
    core::fs::Directory root_directory(core::fs::DirectoryType::Current);
    generate_map(512.f, 2048.f,
                 root_directory / "data" / "maps" / "test" / "height_map.tga",
                 root_directory / "data" / "maps" / "test" / "map.data");
  }
  LOG_TEST(STAGE, "Finished!\n");
}

}
