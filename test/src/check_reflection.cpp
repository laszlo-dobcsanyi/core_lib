#include "test.h"
#include "core/base.h"
#include "experimental/reflection.hpp"

#define LOG_REFLECTION_TEST(__severity__, ...)                                 \
  LOG_TEST(__severity__, __VA_ARGS__)

namespace reflection_test {

namespace graphics {

struct Texture {
  uint32 handle;
};

struct Mesh {
  uint32 handle;
};

struct Model {
  Mesh mesh;
  Texture diffuse;
  Texture normal;
  Texture specular;
};

struct Animation {};

struct Animation1 : public Animation {
  uint32 data;
};

struct Animation2 : public Animation {};

struct AnimatedModel {
  const_string name;
  Model model;
  core::UniqueArray<Animation> animations;
};

}

namespace game {

struct Character {
  const_string name;
  graphics::AnimatedModel * animated_model = nullptr;
  graphics::Animation * shoot_animation = nullptr;
};

}

inline void serialize_mesh(void * _ptr, reflection::Serializer & _serializer) {
  const_string path;
  _serializer >> path;
  graphics::Mesh * mesh = reinterpret_cast<graphics::Mesh *>(_ptr);
  // mesh_from_file ...
}

inline void serialize_texture(void * _ptr,
                              reflection::Serializer & _serializer) {
  const_string path;
  _serializer >> path;
  graphics::Mesh * mesh = reinterpret_cast<graphics::Mesh *>(_ptr);
  // mesh_from_file ...
}

inline void serialize_animation(void * _ptr,
                                reflection::Serializer & _serializer) {
  const_string path;
  _serializer >> path;
  graphics::Mesh * mesh = reinterpret_cast<graphics::Mesh *>(_ptr);
  // mesh_from_file ...
}

}

////////////////////////////////////////////////////////////////////////////////
// test
////////////////////////////////////////////////////////////////////////////////

namespace test {

/*******************************************************************************
** check_reflection
*******************************************************************************/

void check_reflection() {
  LOG_REFLECTION_TEST(STAGE, "Running..\n");

  using namespace reflection;
  using namespace reflection_test;
  TypeSystem type_system;

  type_system.Create<graphics::Mesh>("Mesh", Creator::Bind(&serialize_mesh));
  type_system.Create<graphics::Texture>("Texture",
                                        Creator::Bind(&serialize_texture));
  type_system.Create<graphics::Animation>("Animation",
                                          Creator::Bind(&serialize_animation));

  type_system.Create<graphics::Model>("Model")
      .Property("mesh", "Mesh", &graphics::Model::mesh)
      .Property("diffuse", "Texture", &graphics::Model::diffuse)
      .Property("normal", "Texture", &graphics::Model::normal)
      .Property("specular", "Texture", &graphics::Model::specular);

  type_system.Create<graphics::AnimatedModel>("AnimatedModel")
      .Property("name", "const_string", &graphics::AnimatedModel::name)
      .Property("model", "Model", &graphics::AnimatedModel::model)
      .Array("animations", "Animation", &graphics::AnimatedModel::animations);

  type_system.Create<game::Character>("Character")
      .Property("name", "const_string", &game::Character::name)
      .Property("animated_model", "AnimatedModel",
                &game::Character::animated_model)
      .Property("shoot_animation", "Animation",
                &game::Character::shoot_animation);

  LOG_REFLECTION_TEST(STAGE, "Finished!\n");
}

}
