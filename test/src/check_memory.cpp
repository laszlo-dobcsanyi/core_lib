#include "test.h"
#include "core/base.h"
#include "core/string.hpp"
#include "core/optional.hpp"
#include "core/variant.hpp"

#include "core/memory/unique_array.hpp"

////////////////////////////////////////////////////////////////////////////////
// test
////////////////////////////////////////////////////////////////////////////////

namespace test {

/*******************************************************************************
** Leaky
*******************************************************************************/

class Leaky {
public:
  static size_t alive;
  Leaky() { ++alive; }
  Leaky(Leaky &&) {}
  Leaky(Leaky const &) { ++alive; }
  Leaky & operator=(Leaky &&) { return *this; }
  Leaky & operator=(Leaky const & _other) {
    if (this != &_other) {
      ++alive;
    }
    return *this;
  }
  ~Leaky() { --alive; }
};

size_t Leaky::alive = 0;

struct Asd {
  core::UniqueArray<Leaky> x;
};

/*******************************************************************************
** check_memory
*******************************************************************************/

void check_memory() {
  LOG_TEST(STAGE, "Running..\n");
  {
    struct Local {
      static core::Optional<Asd> make_asd() {
        Asd asd;
        asd.x = core::make_unique_array<Leaky>(4u);
        // return core::Optional< Asd >( move( asd ) );
        return core::nullopt;
      }
    };
    Asd me;
    if (auto result = Local::make_asd()) {
      // me = move( result.Value() );
    }
  }
  ASSERT_TEST(Leaky::alive == 0u);
  LOG_TEST(STAGE, "Finished!\n");
}

}
