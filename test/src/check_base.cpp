#include "test.h"
#include "core/base.h"

////////////////////////////////////////////////////////////////////////////////
// test
////////////////////////////////////////////////////////////////////////////////

namespace test {

struct Dummy {};

/*******************************************************************************
** check_mpl
*******************************************************************************/

void check_mpl() {
  CORE_LIB_STATIC_ASSERT(mpl::is_same<int, int>::value);
  CORE_LIB_STATIC_ASSERT(mpl::is_same<int *, int *>::value);
  CORE_LIB_STATIC_ASSERT(mpl::is_same<int const *, int const *>::value);
  CORE_LIB_STATIC_ASSERT(mpl::is_same<int * const, int * const>::value);
  CORE_LIB_STATIC_ASSERT(
      mpl::is_same<int const * const, int const * const>::value);
  CORE_LIB_STATIC_ASSERT(mpl::is_same<Dummy, Dummy>::value);
  CORE_LIB_STATIC_ASSERT(mpl::is_same<Dummy *, Dummy *>::value);
  CORE_LIB_STATIC_ASSERT(mpl::is_same<Dummy const *, Dummy const *>::value);
  CORE_LIB_STATIC_ASSERT(mpl::is_same<Dummy * const, Dummy * const>::value);
  CORE_LIB_STATIC_ASSERT(
      mpl::is_same<Dummy const * const, Dummy const * const>::value);

  CORE_LIB_STATIC_ASSERT(!mpl::is_same<int, bool>::value);
  CORE_LIB_STATIC_ASSERT(!mpl::is_same<int *, bool>::value);
  CORE_LIB_STATIC_ASSERT(!mpl::is_same<int const *, bool>::value);
  CORE_LIB_STATIC_ASSERT(!mpl::is_same<int * const, bool>::value);
  CORE_LIB_STATIC_ASSERT(!mpl::is_same<int const * const, bool>::value);
  CORE_LIB_STATIC_ASSERT(!mpl::is_same<Dummy, bool>::value);
  CORE_LIB_STATIC_ASSERT(!mpl::is_same<Dummy *, bool>::value);
  CORE_LIB_STATIC_ASSERT(!mpl::is_same<Dummy const *, bool>::value);
  CORE_LIB_STATIC_ASSERT(!mpl::is_same<Dummy * const, bool>::value);
  CORE_LIB_STATIC_ASSERT(!mpl::is_same<Dummy const * const, bool>::value);

  CORE_LIB_STATIC_ASSERT(mpl::is_same<int, int>::value);

  CORE_LIB_STATIC_ASSERT(
      mpl::typepack_contains<int, mpl::TypePack<int>>::value == true);
  CORE_LIB_STATIC_ASSERT(
      mpl::typepack_contains<float, mpl::TypePack<int>>::value == false);

  CORE_LIB_STATIC_ASSERT(
      mpl::index_of_type_in_pack<int, mpl::TypePack<int, bool>>::value == 0u);
  CORE_LIB_STATIC_ASSERT(
      mpl::index_of_type_in_pack<bool, mpl::TypePack<int, bool>>::value == 1u);

  CORE_LIB_STATIC_ASSERT(mpl::is_same<mpl::remove_head_t<mpl::TypePack<int>>,
                                      mpl::TypePack<>>::value);
  CORE_LIB_STATIC_ASSERT(
      mpl::is_same<mpl::remove_head_t<mpl::TypePack<int, float, double>>,
                   mpl::TypePack<float, double>>::value);
}

/*******************************************************************************
** check_base
*******************************************************************************/

void check_base() {
  LOG_TEST(STAGE, "Running..\n");
  check_mpl();
  LOG_TEST(STAGE, "Finished!\n");
}

}
