#include "test.h"
#include "core/variant.hpp"

////////////////////////////////////////////////////////////////////////////////
// test
////////////////////////////////////////////////////////////////////////////////

namespace test {

struct Implementation1 {
  int value = 0;

  Implementation1() {
    LOG_TEST(MESSAGE, "Implementation1 Default constructor\n");
  }
  Implementation1(int _value)
      : value(_value) {
    LOG_TEST(MESSAGE, "Implementation1 Int constructor\n");
  }
  Implementation1(Implementation1 &&) {
    LOG_TEST(MESSAGE, "Implementation1 move constructor\n");
  }
  Implementation1(Implementation1 const &) {
    LOG_TEST(MESSAGE, "Implementation1 copy constructor\n");
  }
  Implementation1 & operator=(Implementation1 &&) {
    LOG_TEST(MESSAGE, "Implementation1 move assign\n");
    return *this;
  }
  Implementation1 & operator=(Implementation1 const &) {
    LOG_TEST(MESSAGE, "Implementation1 copy assign\n");
    return *this;
  }
  ~Implementation1() { LOG_TEST(MESSAGE, "Implementation1 destructor\n"); }

  void Print() { LOG_TEST(MESSAGE, "Implementation1\n"); }
};

struct Implementation2 {
  int value = 0;
  int value2 = 32;

  Implementation2() {
    LOG_TEST(MESSAGE, "Implementation2 Default constructor\n");
  }
  Implementation2(int _value)
      : value(_value)
      , value2(42) {
    LOG_TEST(MESSAGE, "Implementation2 Int constructor\n");
  }
  Implementation2(Implementation2 &&) {
    LOG_TEST(MESSAGE, "Implementation2 move constructor\n");
  }
  Implementation2(Implementation2 const &) {
    LOG_TEST(MESSAGE, "Implementation2 copy constructor\n");
  }
  Implementation2 & operator=(Implementation2 &&) {
    LOG_TEST(MESSAGE, "Implementation2 move assign\n");
    return *this;
  }
  Implementation2 & operator=(Implementation2 const &) {
    LOG_TEST(MESSAGE, "Implementation2 copy assign\n");
    return *this;
  }
  ~Implementation2() { LOG_TEST(MESSAGE, "Implementation2 destructor\n"); }

  void Print() { LOG_TEST(MESSAGE, "Implementation2\n"); }
};

struct ImplementationPrinter {
  template<typename T>
  void operator()(T & _value) {
    _value.Print();
  }
};

/*******************************************************************************
** check_variant
*******************************************************************************/

void check_variant() {
  LOG_TEST(STAGE, "Running..\n");

  using IntBoolVariant = core::Variant<int, bool>;
  auto int_bool_variant = IntBoolVariant(in_place_t<int>(), 8);

  using ImplementationVariant = core::Variant<Implementation1, Implementation2>;
  auto implementation_variant_1 =
      ImplementationVariant(in_place_t<Implementation1>());
  implementation_variant_1.Apply(ImplementationPrinter());
  auto implementation_variant_2 =
      ImplementationVariant(in_place_t<Implementation2>());
  implementation_variant_2.Apply(ImplementationPrinter());

  implementation_variant_1 = implementation_variant_2;
  implementation_variant_1.Apply(ImplementationPrinter());

  implementation_variant_1 =
      ImplementationVariant(in_place_t<Implementation1>(), 8);

  new (&implementation_variant_2)
      ImplementationVariant(implementation_variant_1);
  new (&implementation_variant_1)
      ImplementationVariant(move(implementation_variant_2));

  LOG_TEST(STAGE, "Finished!\n");
}

}
