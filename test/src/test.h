#ifndef _TEST_TEST_H_
#define _TEST_TEST_H_

#include "core/base.h"

#if defined(CORE_LIB_CONFIGURATION_DEBUG)
#  define LOG_TEST(__severity__, ...) CORE_LIB_LOG(LOG, __VA_ARGS__)
#  define ASSERT_TEST(__condition__) CORE_LIB_ASSERT(__condition__)
#else
#  define LOG_TEST(__severity__, ...) std::printf(__VA_ARGS__)
#  define ASSERT_TEST(__condition__)                                           \
    do {                                                                       \
      if (!(__condition__)) {                                                  \
        LOG_TEST(ERROR,                                                        \
                 "Assertion failed! Expression: %s \nFile: %s Line: %s\n",     \
                 CORE_LIB_TO_STRING(__condition__),                            \
                 CORE_LIB_TO_STRING(__FILE__), CORE_LIB_TO_STRING(__LINE__));  \
      }                                                                        \
    } while (0);
#endif

#endif
