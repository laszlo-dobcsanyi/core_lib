#include "test.h"
#include "core/numerics.hpp"

#define LOG_NUMERICS_TEST(__severity__, ...)                                   \
  CORE_LIB_LOG(__severity__, __VA_ARGS__)

////////////////////////////////////////////////////////////////////////////////
// test
////////////////////////////////////////////////////////////////////////////////

namespace test {

/*******************************************************************************
** print
*******************************************************************************/

template<typename Matrix>
void print(Matrix const & _matrix) {
  using Storage = typename Matrix::StorageType;
  for (auto row = 0u; row < Storage::Rows; ++row) {
    for (auto column = 0u; column < Storage::Columns; ++column) {
      std::printf("%.8f ", _matrix(row, column));
    }
    std::printf("\n");
  }
}

template<typename T>
bool equal(T const & _lhs, T const & _rhs) {
  return numerics::is_nearly_zero(_rhs - _lhs);
}

/*******************************************************************************
** check_numerics
*******************************************************************************/

void check_numerics() {
  using namespace numerics;
  LOG_NUMERICS_TEST(STAGE, "Running..\n");
  {
    LOG_NUMERICS_TEST(STAGE, "Checking matrices..\n");
    {
      LOG_NUMERICS_TEST(MESSAGE, "Checking translation..\n");
      {
        ColumnMajorMatrix4x4f matrix;
        set_translate(matrix, Vector3f(1.f, 0.f, 0.f));
        ASSERT_TEST(get_translation(matrix) == Vector3f(1.f, 0.f, 0.f));
        set_translation(matrix, Vector3f(0.f, 1.f, 0.f));
        ASSERT_TEST(get_translation(matrix) == Vector3f(0.f, 1.f, 0.f));
        set_translation(matrix, Vector3f(0.f, 0.f, -1.f));
        ASSERT_TEST(get_translation(matrix) == Vector3f(0.f, 0.f, -1.f));
      }

      LOG_NUMERICS_TEST(MESSAGE, "Checking rotation..\n");
      {
        int kStep = 45;

        ColumnMajorMatrix4x4f matrix;
        for (int x_angle = -360; x_angle < 360; x_angle += kStep) {
          for (int y_angle = -360; y_angle < 360; y_angle += kStep) {
            for (int z_angle = -360; z_angle < 360; z_angle += kStep) {
              Quaternionf original = to_rotation(Vector3f(
                  static_cast<float>(x_angle), static_cast<float>(y_angle),
                  static_cast<float>(z_angle)));
              set_rotate(matrix, original);
              Quaternionf result = get_rotation(matrix);
              ASSERT_TEST(are_equivalent(result, original));
            }
          }
        }
      }

      LOG_NUMERICS_TEST(MESSAGE, "Checking determinant..\n");
      {
        ColumnMajorMatrix4x4f matrix;
        matrix[0u] = {2.f, -2.f, 1.f, -1.f};
        matrix[1u] = {5.f, -3.f, 3.f, -6.f};
        matrix[2u] = {-3.f, 2.f, -2.f, 4.f};
        matrix[3u] = {-2.f, -5.f, 0.f, 0.f};
        ASSERT_TEST(determinant(matrix) == 5.f);
      }

      LOG_NUMERICS_TEST(MESSAGE, "Checking inverse..\n");
      {
        ColumnMajorMatrix4x4f matrix;
        matrix[0u] = {2.f, -2.f, 1.f, -1.f};
        matrix[1u] = {5.f, -3.f, 3.f, -6.f};
        matrix[2u] = {-3.f, 2.f, -2.f, 4.f};
        matrix[3u] = {-2.f, -5.f, 0.f, 0.f};
        ColumnMajorMatrix4x4f expected_inverse;
        expected_inverse[0u] = {0.f, 10.f / 2.f, 15.f / 5.f, 0.f};
        expected_inverse[1u] = {0.f, -4.f / 5.f, -6.f / 5.f, -1.f / 5.f};
        expected_inverse[2u] = {10.f / 5.f, -37.f / 5.f, -53.f / 5.f,
                                -3.f / 5.f};
        expected_inverse[3u] = {5.f / 5.f, -9.f / 5.f, -11.f / 5.f, -1.f / 5.f};
        ASSERT_TEST(inverse(matrix) == expected_inverse);
      }

      LOG_NUMERICS_TEST(MESSAGE, "Checking multiplication..\n");
      {
        ColumnMajorMatrix4x4f matrix;
        set_rotate(matrix, to_rotation(Vector3f(90.f, 0.f, 0.f)));
        Vector4f x(1.f, 0.f, 0.f, 1.f);
        ASSERT_TEST(matrix * x == x);
        Vector4f y(0.f, 1.f, 0.f, 1.f);
        ASSERT_TEST(equal(matrix * y, Vector4f(0.f, 0.f, 1.f, 1.f)));
        Vector4f z(0.f, 0.f, 1.f, 1.f);
        ASSERT_TEST(equal(matrix * z, Vector4f(0.f, -1.f, 0.f, 1.f)));
        Vector4f a(1.f, 1.f, 1.f, 1.f);
        ASSERT_TEST(equal(matrix * a, Vector4f(1.f, -1.f, 1.f, 1.f)));

        ColumnMajorMatrix4x4f translate1;
        set_translate(translate1, Vector3f(0.f, 0.f, 10.f));
        ColumnMajorMatrix4x4f rotate;
        set_rotate(rotate, to_rotation(Vector3f(0.f, 90.f, 0.f)));
        ColumnMajorMatrix4x4f translate2;
        set_translate(translate2, Vector3f(0.f, 10.f, 0.f));
        ColumnMajorMatrix4x4f combined = translate2 * rotate * translate1;
        ASSERT_TEST(equal(combined * x, Vector4f(10.f, 10.f, -1.f, 1.f)));
      }
    }
    LOG_NUMERICS_TEST(MESSAGE, "Checking transformation..\n");
    { Transformation transform; }
  }
  LOG_NUMERICS_TEST(STAGE, "Finished!\n");
}

}
