#include "test.h"
#include "core/base.h"
#include "core/string.hpp"

////////////////////////////////////////////////////////////////////////////////
// test
////////////////////////////////////////////////////////////////////////////////

namespace test {

void check_string() {
  LOG_TEST(STAGE, "Running..\n");

  LOG_TEST(MESSAGE, "cstring decimal parse..\n");
  {
    uint32 value;
    size_t parsed;
#define DECIMAL_PARSE_TEST_(__decimal_value__, __cstring_name__)               \
  char const * __cstring_name__ = CORE_LIB_TO_STRING(__decimal_value__);       \
  parsed = cstring::parse_decimal(__cstring_name__, value);                    \
  LOG_TEST(MESSAGE,                                                            \
           "[StringTest]>\t%s parsed as 32bit signed: %" PRIu32 " (%zu)\n",    \
           __cstring_name__, value, parsed);
#define DECIMAL_PARSE_TEST(__decimal_value__)                                  \
  DECIMAL_PARSE_TEST_(__decimal_value__,                                       \
                      CORE_LIB_CONCAT(string_value, __COUNTER__))

    DECIMAL_PARSE_TEST(0);
    DECIMAL_PARSE_TEST(000009);
    DECIMAL_PARSE_TEST(1);
    DECIMAL_PARSE_TEST(-0);
    DECIMAL_PARSE_TEST(-1);
    DECIMAL_PARSE_TEST(255);
    DECIMAL_PARSE_TEST(256);
    DECIMAL_PARSE_TEST(-255);
    DECIMAL_PARSE_TEST(-256);
    DECIMAL_PARSE_TEST(65535);
    DECIMAL_PARSE_TEST(65536);
    DECIMAL_PARSE_TEST(-65534);
    DECIMAL_PARSE_TEST(-65535);
    DECIMAL_PARSE_TEST(4294967295);
    DECIMAL_PARSE_TEST(4294967296);

#undef DECIMAL_PARSE_TEST_
#undef DECIMAL_PARSE_TEST
  }

  LOG_TEST(MESSAGE, "cstring floating parse..\n");
  {
    float value;
    size_t parsed;

#define FLOATING_PARSE_TEST_(__floating_value__, __cstring_name__)             \
  char const * __cstring_name__ = CORE_LIB_TO_STRING(__floating_value__);      \
  parsed = cstring::parse_floating(__cstring_name__, value);                   \
  LOG_TEST(MESSAGE, "[StringTest]>\t%s parsed as 32bit floating: %f (%zu)\n",  \
           __cstring_name__, value, parsed);
#define FLOATING_PARSE_TEST(__floating_value__)                                \
  FLOATING_PARSE_TEST_(__floating_value__,                                     \
                       CORE_LIB_CONCAT(string_value, __COUNTER__))

    FLOATING_PARSE_TEST(0);
    FLOATING_PARSE_TEST(-0);
    FLOATING_PARSE_TEST(0.0);
    FLOATING_PARSE_TEST(-0.0);
    FLOATING_PARSE_TEST(0.14);
    FLOATING_PARSE_TEST(0.145);
    FLOATING_PARSE_TEST(3.14);
    FLOATING_PARSE_TEST(-3.14);
    FLOATING_PARSE_TEST(42.4242424);
    FLOATING_PARSE_TEST(12345.12345);
    FLOATING_PARSE_TEST(1234567890.12345);

#undef FLOATING_PARSE_TEST_
#undef FLOATING_PARSE_TEST
  }

  LOG_TEST(MESSAGE, "cstring decimal format..\n");
  {}

  LOG_TEST(MESSAGE, "const_string..\n");
  {
    const_string const_string_value;
    LOG_TEST(MESSAGE, "Default constructed: %s\n", cstr(const_string_value));
    const_string empty_const_string("");
    LOG_TEST(MESSAGE, "Empty: %s\n", cstr(empty_const_string));
    const_string not_empty_const_string("not_empty");
    LOG_TEST(MESSAGE, "Not empty : %s\n", cstr(not_empty_const_string));
  }

  LOG_TEST(MESSAGE, "Stream..\n");
  {
    int8 int8_value = -1;
    uint8 uint8_value = 2u;
    int16 int16_value = -3;
    uint16 uint16_value = 4u;
    int32 int32_value = -5;
    uint32 uint32_value = 6u;
    int64 int64_value = -7;
    uint64 uint64_value = 8u;

    string::Stream stream;
    stream.Write(int8_value);
    stream.Write(uint8_value);
    stream.Write(int16_value);
    stream.Write(uint16_value);
    stream.Write(int32_value);
    stream.Write(uint32_value);
    stream.Write(int64_value);
    stream.Write(uint64_value);
  }

  LOG_TEST(STAGE, "Finished!\n");
}

}
