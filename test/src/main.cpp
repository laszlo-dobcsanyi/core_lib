#include "core/application.hpp"

////////////////////////////////////////////////////////////////////////////////
// test
////////////////////////////////////////////////////////////////////////////////

namespace test {

void check_base();
void check_allocators();
// void check_application();
// void check_byte_stream();
void check_callable();
void check_collections();
void check_containers();
void check_slot_map();
void check_filesystem();
void check_memory();
void check_numerics();
void check_optional();
void check_parallel();
// void check_platform();
// void check_queries();
void check_ranges();
void check_reflection();
void check_signal();
// void check_sort();
void check_string();
// void check_time();
void check_variant();

void check_arrays();
void check_stacks();

void check_graphics();
void check_fbx();
void check_bmp();
void check_image();
void check_font();
void check_raymarching();

void check_net();

void check_map();

/*******************************************************************************
** check
*******************************************************************************/

inline void check(void (*_test)()) { (*_test)(); }

}

////////////////////////////////////////////////////////////////////////////////
//                                                                                                //
////////////////////////////////////////////////////////////////////////////////

int main() {
  application::program_main([]() {
    if (true) {
      test::check(test::check_collections);
    } else {
      test::check(test::check_base);
      test::check(test::check_allocators);
      // test::check( test::check_application );
      // test::check( test::check_byte_stream );
      test::check(test::check_callable);
      test::check(test::check_collections);
      test::check(test::check_containers);
      test::check(test::check_slot_map);
      test::check(test::check_filesystem);
      test::check(test::check_memory);
      test::check(test::check_numerics);
      test::check(test::check_optional);
      test::check(test::check_parallel);
      // test::check( test::check_platform );
      // test::check( test::check_queries );
      test::check(test::check_ranges);
      test::check(test::check_reflection);
      test::check(test::check_signal);
      // test::check( test::check_sort );
      test::check(test::check_string);
      // test::check( test::check_time );
      test::check(test::check_variant);

      test::check(test::check_graphics);
      test::check(test::check_fbx);
      test::check(test::check_bmp);
      test::check(test::check_image);
      test::check(test::check_font);
      test::check(test::check_raymarching);

      test::check(test::check_net);

      test::check(test::check_map);
    }
  });
  return 0;
}
