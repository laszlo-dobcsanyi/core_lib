#include "test.h"
#include "core/base.h"
#include "core/fs/directory.h"
#include "core/time.hpp"
#include "graphics/api.h"
#include "graphics/window.h"
#include "graphics/camera.h"
#include "gui/freetype/freetype.h"
#include "gui/font_atlas.h"
#include "gui/text.h"
#include "gui/text_renderer.h"

////////////////////////////////////////////////////////////////////////////////
// test
////////////////////////////////////////////////////////////////////////////////

namespace test {

/*******************************************************************************
** FrameStatistics
*******************************************************************************/

class FrameStatistics {
public:
  FrameStatistics() {
    for (float & frame_duration : frame_durations) {
      frame_duration = 0.f;
    }
  }
  FrameStatistics(FrameStatistics &&) = default;
  FrameStatistics(FrameStatistics const &) = default;
  FrameStatistics & operator=(FrameStatistics &&) = default;
  FrameStatistics & operator=(FrameStatistics const &) = default;
  ~FrameStatistics() = default;

  void AddFrame(float duration) {
    frame_durations[last_index] = duration;
    last_index =
        last_index == frame_durations.Size() - 1u ? 0u : last_index + 1u;
  }

  float GetAverageFrameDuration() const {
    float total_duration = 0.f;
    for (float frame_duration : frame_durations) {
      total_duration += frame_duration;
    }
    return total_duration / static_cast<float>(frame_durations.Size());
  }

private:
  size_t last_index = 0u;
  container::InlineArray<float, 128u> frame_durations;
};

/*******************************************************************************
** check_font
*******************************************************************************/

void check_font() {
  LOG_TEST(STAGE, "Running..\n");

  auto data_directory = core::fs::Directory(
      core::fs::Directory(core::fs::DirectoryType::Executable) /
      "./../../data");

  // Context
  LOG_TEST(STAGE, "Initializing Context..\n");
  graphics::api::Context api_context;
  if (!api_context.Initialize()) {
    LOG_TEST(ERROR, "Failed to initialize context!\n");
    return;
  }

  // Window
  LOG_TEST(STAGE, "Initializing Window..\n");
  graphics::Window window;
  if (!window.Create(const_string("Image Test"))) {
    LOG_TEST(ERROR, "Failed to create window!\n");
    return;
  }

  // FreeType
  if (!gui::freetype::Initialize()) {
    LOG_TEST(ERROR, "Failed to initialize freetype!\n");
    return;
  }

  // FontAtlas
  LOG_TEST(STAGE, "Creating FontAtlas..\n");
  gui::FontAtlas font_atlas;
  {
    core::fs::FilePath font_path(data_directory / "fonts/DejaVuSans.ttf");
    if (auto result = gui::font_atlas_from_file(font_path, 11)) {
      font_atlas = move(result.Value());
    } else {
      LOG_TEST(ERROR, "Failed to create font atlas!\n");
      return;
    }
  }

  // TextRenderer
  LOG_TEST(STAGE, "Creating TextRenderer..\n");
  gui::TextRenderer text_renderer;
  {
    if (auto text_renderer_result = gui::create_text_renderer()) {
      text_renderer = move(*text_renderer_result);
    } else {
      LOG_TEST(ERROR, "Failed to create text renderer!\n");
      return;
    }
  }

  // Text
  gui::Text text = gui::create_text(core::make_ref(font_atlas), "Hello world!");

  // Camera
  graphics::Camera camera;
  camera.SetOrtho(0.0f, 1920.0f, 1080.0f, 0.f, -1.0f, 1.0f);

  // TODO: Hack..
  graphics::glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  graphics::glEnable(GL_BLEND);

  // Main loop
  LOG_TEST(STAGE, "Entering Main Loop..\n");
  core::HighResolutionClock clock;
  FrameStatistics frame_statistics;
  for (;;) {
    window.Run();
    if (window.IsDestroyed()) {
      break;
    }
    window.Clear();

    {
      auto frame_duration = static_cast<float>(core::to_seconds(clock.Reset()));
      frame_statistics.AddFrame(frame_duration);
      float average_frame_duration = frame_statistics.GetAverageFrameDuration();
      float average_frames_per_second =
          numerics::is_nearly_zero(average_frame_duration)
              ? 0.f
              : 1.f / average_frame_duration;
      char buffer[128u];
      std::snprintf(buffer, 128u, "FPS: %10.5f FrameDuration: %10.7f",
                    average_frames_per_second, average_frame_duration);
      gui::set_text(text, buffer);
    }

    {
      graphics::bind(text_renderer);
      graphics::render(text_renderer, text, camera.GetProjection(),
                       numerics::Vector2f(300.f, 300.f));
    }

    window.Present();
  }
  LOG_TEST(STAGE, "Leaving Main Loop..\n");

  LOG_TEST(STAGE, "Finished!\n");
}
}
