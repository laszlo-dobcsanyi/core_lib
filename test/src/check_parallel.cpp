#include "test.h"
#include "core/base.h"
#include "core/parallel/process.hpp"

#define LOG_PARALLEL_TEST(__severity__, ...) LOG_TEST(__severity__, __VA_ARGS__)

////////////////////////////////////////////////////////////////////////////////
// test
////////////////////////////////////////////////////////////////////////////////

namespace test {

/*******************************************************************************
** ParallelTest
*******************************************************************************/

struct TestState {};

using ProcessState = parallel::ProcessState<TestState>;

struct VoidFunctor {
  void operator()(ProcessState & _state);
};
struct BoolFunctor {
  bool operator()(ProcessState & _state, int _value);
};

struct Main {
  void operator()(ProcessState & _state);
};
struct RelaxMain {
  void operator()(ProcessState & _state);
};
struct Update2Task {
  void operator()(ProcessState & _state,
                  parallel::Task<ProcessState, RelaxMain> * _task);
};
struct Update1Task {
  void operator()(ProcessState & _state,
                  parallel::Task<ProcessState, RelaxMain> * _task);
};

inline void VoidFunctor::operator()(ProcessState & /* _state */) {}
inline bool BoolFunctor::operator()(ProcessState & /* _state */,
                                    int /* _value */) {
  return true;
}

inline void Main::operator()(ProcessState & _state) {
  static size_t count = 0u;
  if (++count == 5u) {
    _state.Terminate();
    return;
  }
  LOG_PARALLEL_TEST(LOG, "Main %zu..\n", count);
  if (auto task = _state.CreateSharedTask<RelaxMain>(
          2u, core::wrap_callable(RelaxMain()))) {
    _state.PostShared<Update1Task>(core::wrap_callable(Update1Task()), task);
    _state.PostShared<Update2Task>(core::wrap_callable(Update2Task()), task);
    /*
    !!FAIL!!
    task->Post< Update1Task >( task );
    task->Post< Update2Task >( task );
*/
  } else {
    CORE_LIB_ASSERT_MESSAGE("Main: Failed to allocate Task!\n");
  }
}

inline void RelaxMain::operator()(ProcessState & _state) {
  LOG_PARALLEL_TEST(LOG, "RelaxMain..\n");
  _state.Sleep();

  const std::chrono::milliseconds relax_duration(5);
  std::this_thread::sleep_for(relax_duration);

  _state.PostLocal<Main>(core::wrap_callable(Main()));

  _state.Wake();
}

inline void
Update2Task::operator()(ProcessState & _state,
                        parallel::Task<ProcessState, RelaxMain> * _task) {
  LOG_PARALLEL_TEST(LOG, "Update2Task..\n");
  CORE_LIB_ASSERT(_task);
  using RelaxFinalizer = parallel::TaskFinalizer<ProcessState, RelaxMain>;
  if (auto task = _state.CreateSharedTask<RelaxFinalizer>(
          512u, core::wrap_callable(RelaxFinalizer()), _task)) {
    for (auto current = 0; current < 512; ++current) {
      task->Post(core::wrap_callable(BoolFunctor()), 1024u + current);
    }
  } else {
    CORE_LIB_ASSERT_MESSAGE("Update2Task: Failed to allocate Task!\n");
  }
}

inline void
Update1Task::operator()(ProcessState & _state,
                        parallel::Task<ProcessState, RelaxMain> * _task) {
  LOG_PARALLEL_TEST(LOG, "Update1Task..\n");
  CORE_LIB_ASSERT(_task);
  using RelaxFinalizer = parallel::TaskFinalizer<ProcessState, RelaxMain>;
  if (auto task = _state.CreateSharedTask<RelaxFinalizer>(
          512u, core::wrap_callable(RelaxFinalizer()), _task)) {
    for (auto current = 0; current < 512; ++current) {
      task->Post(core::wrap_callable(BoolFunctor()), current);
    }
  } else {
    CORE_LIB_ASSERT_MESSAGE("Update1Task: Failed to allocate Task!\n");
  }
}

void check_parallel() {
  LOG_PARALLEL_TEST(LOG, "Running..\n");

  LOG_PARALLEL_TEST(LOG, "Process test..\n");
  {
    LOG_PARALLEL_TEST(LOG, "\tAllocating thread pool..\n");
    container::Array<parallel::Thread> threads;
    for (auto current = 0u; current < 4; ++current) {
      threads.Create(parallel::ThreadAttributes(current, 0u));
    }

    {
      LOG_PARALLEL_TEST(LOG, "\tCreating process..\n");
      parallel::Process<TestState> main_process(move(threads));

      LOG_PARALLEL_TEST(LOG, "\tPosting main call..\n");
      if (!main_process.Post(core::wrap_callable(Main()))) {
        LOG_PARALLEL_TEST(LOG, "\tFailed to post main call!\n");
      }

      LOG_PARALLEL_TEST(LOG, "\tLaunching process..\n");
      main_process.Launch();

      LOG_PARALLEL_TEST(LOG, "\tJoining process..\n");
      if (main_process.IsJoinable()) {
        main_process.Join();
      }
    }
  }
  LOG_PARALLEL_TEST(LOG, "Finished!\n");

#if defined(FUNCTION_TEST_ENABLED) || defined(CALL_STORAGE_TEST_ENABLED)
  void foo1() { LOG_PARALLEL_TEST(LOG, "foo1\n"); }

  int foo2(int _value) {
    LOG_PARALLEL_TEST(LOG, "foo2: %i\n", _value);
    return 0;
  }

  struct Bar {
    void foo1() { LOG_PARALLEL_TEST(LOG, "Bar::foo1\n"); }

    int foo2(int _value) {
      LOG_PARALLEL_TEST(LOG, "Bar::foo2: %i\n", _value);
      return 0;
    }
  };
#endif

#if defined(CALL_STORAGE_TEST_ENABLED)
  LOG_PARALLEL_TEST(LOG, "CallStorage test..\n");
  {
    using CallAllocator = allocator::PageAllocator<mpl::PowerOf2<14u>::value>;
    using CallStorage = parallel::CallStorage<CallAllocator>;
    CallStorage call_storage;

    {
      LOG_PARALLEL_TEST(LOG, "\tFree function call test..\n");

      call_storage.Create<decltype(&foo1)>(&foo1);
      call_storage.Create<decltype(&foo2)>(42, &foo2);

      auto foo1_func = bind(&foo1);
      auto foo2_func = bind(&foo2);
      auto foo1_call = call_storage.Create(foo1_func);
      auto foo2_call = call_storage.Create(foo2_func, 42);

      call_storage.Reset();
    }

    {
      LOG_PARALLEL_TEST(LOG, "\tMember function call test..\n");

      Bar instance;
      call_storage.Create<decltype(&Bar::foo1)>(&Bar::foo1, instance);
      call_storage.Create<decltype(&Bar::foo2)>(42, &Bar::foo2, instance);

      auto bar_foo1_func = bind(&Bar::foo1, instance);
      auto bar_foo2_func = bind(&Bar::foo2, instance);
      auto bar_foo1_call = call_storage.Create(bar_foo1_func);
      auto bar_foo2_call = call_storage.Create(bar_foo2_func, 42);

      call_storage.Reset();
    }

    {
      LOG_PARALLEL_TEST(LOG, "\tFunctor call test..\n");

      call_storage.Create<VoidFunctor>();
      call_storage.Create<BoolFunctor>(42);

      auto void_functor_func = bind<VoidFunctor>();
      auto bool_functor_func = bind<BoolFunctor>();
      auto void_functor_call = call_storage.Create(void_functor_func);
      auto bool_functor_call = call_storage.Create(bool_functor_func, 4242);

      call_storage.Reset();
    }
  }

  LOG_PARALLEL_TEST(LOG, "CallContext test..\n");
  {
    // TODO:
    LOG_PARALLEL_TEST(LOG, ":(\n");
  }
#endif
  LOG_PARALLEL_TEST(LOG, "Finished!\n");
}

}
