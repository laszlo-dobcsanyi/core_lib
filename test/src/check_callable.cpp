#include "test.h"

#include "core/callable/closure.hpp"

#define LOG_CALLABLE_TEST(__severity__, ...) LOG_TEST(__severity__, __VA_ARGS__)

////////////////////////////////////////////////////////////////////////////////
// test
////////////////////////////////////////////////////////////////////////////////

namespace test {

void void_void() {}
void void_int(int) {}
void void_int_float(int, float) {}
int int_void() { return 0; }
int int_int(int) { return 0; }
int int_int_float(int, float) { return 0; }

struct Struct {
  void void_void() {}
  void void_int(int) {}
  void void_int_float(int, float) {}
  int int_void() { return 0; }
  int int_int(int) { return 0; }
  int int_int_float(int, float) { return 0; }
};

struct VoidVoid {
  void operator()() {}
};
struct VoidInt {
  void operator()(int) {}
};
struct VoidIntFloat {
  void operator()(int, float) {}
};
struct IntVoid {
  int operator()() { return 0; }
};
struct IntInt {
  int operator()(int) { return 0; }
};
struct IntIntFloat {
  int operator()(int, float) { return 0; }
};

/*******************************************************************************
** CallableTest
*******************************************************************************/

void check_callable() {
  LOG_CALLABLE_TEST(MESSAGE, "Running..\n");
  {
    LOG_CALLABLE_TEST(MESSAGE, "Function test..\n");
    {
      LOG_CALLABLE_TEST(MESSAGE, "Free function test..\n");

      auto void_void_func = core::wrap_callable(void_void);
      auto void_int_func = core::wrap_callable(void_int);
      auto int_void_func = core::wrap_callable(int_void);
      auto int_int_func = core::wrap_callable(int_int);

      void_void_func();
      void_int_func(1);
      /* auto result1 = */ int_void_func();
      /* auto result2 = */ int_int_func(7);
    }

    {
      LOG_CALLABLE_TEST(MESSAGE, "Member function test..\n");

      Struct instance;
      auto void_void_func = core::wrap_callable(&Struct::void_void, instance);
      auto void_int_func = core::wrap_callable(&Struct::void_int, instance);
      auto int_void_func = core::wrap_callable(&Struct::int_void, instance);
      auto int_int_func = core::wrap_callable(&Struct::int_int, instance);

      void_void_func();
      void_int_func(42);
      /* auto result1 = */ int_void_func();
      /* auto result2 = */ int_int_func(42);
    }

    {
      LOG_CALLABLE_TEST(MESSAGE, "Functor test..\n");

      auto void_void_func = core::wrap_callable(VoidVoid());
      auto void_int_func = core::wrap_callable(VoidInt());
      auto int_void_func = core::wrap_callable(IntVoid());
      auto int_int_func = core::wrap_callable(IntInt());

      void_void_func();
      void_int_func(42);
      /* auto result1 = */ int_void_func();
      /* auto result2 = */ int_int_func(42);
    }
  }

  {
    LOG_CALLABLE_TEST(MESSAGE, "Closure test..\n");
    {
      LOG_CALLABLE_TEST(MESSAGE, "Free function closure test..\n");

      auto void_void_func = core::bind_closure(core::wrap_callable(void_void));
      auto void_int_func = core::bind_closure(core::wrap_callable(void_int), 1);
      auto int_void_func = core::bind_closure(core::wrap_callable(int_void));
      auto int_int_func = core::bind_closure(core::wrap_callable(int_int), 42);

      void_void_func();
      void_int_func();
      /* auto result1 = */ int_void_func();
      /* auto result2 = */ int_int_func();
    }

    {
      LOG_CALLABLE_TEST(MESSAGE, "Member function closure test..\n");

      Struct instance;
      auto void_void_func =
          core::bind_closure(core::wrap_callable(&Struct::void_void, instance));
      auto void_int_func = core::bind_closure(
          core::wrap_callable(&Struct::void_int, instance), 42);
      auto int_void_func =
          core::bind_closure(core::wrap_callable(&Struct::int_void, instance));
      auto int_int_func = core::bind_closure(
          core::wrap_callable(&Struct::int_int, instance), 42);

      void_void_func();
      void_int_func();
      /* auto result1 = */ int_void_func();
      /* auto result2 = */ int_int_func();
    }

    {
      LOG_CALLABLE_TEST(MESSAGE, "Functor closure test..\n");

      auto void_void_func = core::bind_closure(core::wrap_callable(VoidVoid()));
      auto void_int_func =
          core::bind_closure(core::wrap_callable(VoidInt()), 42);
      auto int_void_func = core::bind_closure(core::wrap_callable(IntVoid()));
      auto int_int_func = core::bind_closure(core::wrap_callable(IntInt()), 42);

      void_void_func();
      void_int_func();
      /* auto result1 = */ int_void_func();
      /* auto result2 = */ int_int_func();
    }
  }

  {
    LOG_CALLABLE_TEST(MESSAGE, "Callable Closure test..\n");
    {
      LOG_CALLABLE_TEST(MESSAGE, "Free function callable closure test..\n");

      auto void_void_func =
          core::bind_callable_closure(core::wrap_callable(void_void));
      auto void_int_func =
          core::bind_callable_closure(core::wrap_callable(void_int), 1);
      auto int_void_func =
          core::bind_callable_closure(core::wrap_callable(int_void));
      auto int_int_func =
          core::bind_callable_closure(core::wrap_callable(int_int), 42);

      void_void_func();
      void_int_func();
      /* auto result1 = */ int_void_func();
      /* auto result2 = */ int_int_func();
    }

    {
      LOG_CALLABLE_TEST(MESSAGE, "Member function callable closure test..\n");

      Struct instance;
      auto void_void_func = core::bind_callable_closure(
          core::wrap_callable(&Struct::void_void, instance));
      auto void_int_func = core::bind_callable_closure(
          core::wrap_callable(&Struct::void_int, instance), 42);
      auto int_void_func = core::bind_callable_closure(
          core::wrap_callable(&Struct::int_void, instance));
      auto int_int_func = core::bind_callable_closure(
          core::wrap_callable(&Struct::int_int, instance), 42);

      void_void_func();
      void_int_func();
      /* auto result1 = */ int_void_func();
      /* auto result2 = */ int_int_func();
    }

    {
      LOG_CALLABLE_TEST(MESSAGE, "Functor callable closure test..\n");

      auto void_void_func =
          core::bind_callable_closure(core::wrap_callable(VoidVoid()));
      auto void_int_func =
          core::bind_callable_closure(core::wrap_callable(VoidInt()), 42);
      auto int_void_func =
          core::bind_callable_closure(core::wrap_callable(IntVoid()));
      auto int_int_func =
          core::bind_callable_closure(core::wrap_callable(IntInt()), 42);

      void_void_func();
      void_int_func();
      /* auto result1 = */ int_void_func();
      /* auto result2 = */ int_int_func();
    }
  }

  {
    LOG_CALLABLE_TEST(MESSAGE, "Partial Closure test..\n");
    {
      LOG_CALLABLE_TEST(MESSAGE, "Free function partial closure test..\n");

      auto void_int_float_func =
          core::bind_closure(core::wrap_callable(void_int_float), 42.f);
      auto int_int_float_func =
          core::bind_closure(core::wrap_callable(int_int_float), 42.f);

      void_int_float_func(42);
      /* auto result2 = */ int_int_float_func(42);
    }

    {
      LOG_CALLABLE_TEST(MESSAGE, "Member function partial closure test..\n");

#if 0
      Struct instance;
      auto void_void_func = bind_any_closure( &Struct::void_void, instance );
      auto void_int_func = bind_any_closure( &Struct::void_int, instance, 42 );
      auto int_void_func = bind_any_closure( &Struct::int_void, instance );
      auto int_int_func = bind_any_closure( &Struct::int_int, instance, 42 );

      void_void_func();
      void_int_func();
      auto result1 = int_void_func();
      auto result2 = int_int_func();
#endif
    }

    {
      LOG_CALLABLE_TEST(MESSAGE, "Functor partial closure test..\n");

#if 0
      auto void_void_func = core::bind_closure< VoidVoid >();
      auto void_int_func = core::bind_closure< VoidInt >( 42 );
      auto int_void_func = core::bind_closure< IntVoid >();
      auto int_int_func = core::bind_closure< IntInt >( 42 );

      void_void_func();
      void_int_func();
      auto result1 = int_void_func();
      auto result2 = int_int_func();
#endif
    }
  }

  LOG_CALLABLE_TEST(MESSAGE, "Finished!\n");
}

}
