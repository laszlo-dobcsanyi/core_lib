#!/bin/bash
clear

echo [1/5] Updating submodules..
git submodule update --init --recursive

echo [2/5] Generating zlib..
cmake -H./zlib -B./build/zlib-debug -DCMAKE_DEBUG_POSTFIX=d -DCMAKE_INSTALL_PREFIX="./install"
echo [3/5] Installing zlib..
cmake --build ./build/zlib-debug --target install

echo [4/5] Generating freetype..
cmake -H./freetype -B./build/freetype-debug -DCMAKE_BUILD_TYPE=Debug -DCMAKE_DEBUG_POSTFIX=d -DCMAKE_INSTALL_PREFIX="./install" -DBUILD_SHARED_LIBS:BOOL=true
echo [5/5] Installing freetype..
cmake --build ./build/freetype-debug --target install
