@ECHO OFF
CLS

SET "VsDevCmd="C:\Program Files (x86)\Microsoft Visual Studio\2017\BuildTools\Common7\Tools\VsDevCmd.bat""

ECHO [1\5] Updating submodules..
git submodule update --init --recursive

CALL %VsDevCmd% -arch=amd64 -host_arch=amd64 %platform% %winsdk% %vc% -no_logo

ECHO [2\5] Generating zlib..
cmake -H.\zlib -B.\build\zlib-debug -G Ninja -DCMAKE_BUILD_TYPE=Debug -DBUILD_TYPE=Debug -DCMAKE_DEBUG_POSTFIX=d -DCMAKE_INSTALL_PREFIX=".\install"
ECHO [3\5] Installing zlib..
cmake --build .\build\zlib-debug --target install --config Debug

ECHO [4\5] Generating freetype..
cmake -H.\freetype -B.\build\freetype-debug -G Ninja -DCMAKE_BUILD_TYPE=Debug -DBUILD_TYPE=Debug-DCMAKE_DEBUG_POSTFIX=d -DCMAKE_INSTALL_PREFIX=".\install"
ECHO [5\5] Installing freetype..
cmake --build .\build\freetype-debug --target install --config Debug
