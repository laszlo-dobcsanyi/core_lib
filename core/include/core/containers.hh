#ifndef _CORE_LIB_CONTAINERS_HH_
#define _CORE_LIB_CONTAINERS_HH_

#include "core/base.h"
#include "core/allocators.hh"

namespace container {

// Tags
class Head {};
class Tail {};

template<class Tag, typename Result = void>
using if_head_t = mpl::if_same_t<Tag, Head, Result>;
template<class Tag, typename Result = void>
using if_tail_t = mpl::if_same_t<Tag, Tail, Result>;

// BitSet
template<size_t Size, typename StorageType = size_t>
class BitSet;
// Arrays
template<class T,
         class AllocationPolicy = allocator::policy::Exponential<8u, 2u>,
         template<class> class TypeAllocator = allocator::DefaultTypeAllocator>
class Array;
template<typename T, size_t Size>
class InlineArray;
template<class T,
         template<class> class TypeAllocator = allocator::DefaultTypeAllocator,
         size_t PoolSize = mpl::bit_count<size_t>::value>
class PoolArray;
// Lists
template<class T,
         template<class> class TypeAllocator = allocator::DefaultTypeAllocator>
class SingleList;
template<class T,
         template<class> class TypeAllocator = allocator::DefaultTypeAllocator>
class DoubleList;
// Chain & Link
template<class DerivedType>
class ChainElement;
template<class DerivedType, template<class> class ChainElement = ChainElement>
class Chain;
template<class DerivedType,
         template<class> class TypeAllocator = allocator::DefaultTypeAllocator,
         template<class> class LinkElementType = ChainElement>
class Link;
// Stacks
template<typename T, size_t Bound>
class BoundedStack;
template<typename T, template<typename> class TypeAllocator =
                         allocator::DefaultTypeAllocator>
class UnboundedStack;
// Queues
template<class T, size_t Bound>
class BoundedQueue;
template<class T, template<typename> class TypeAllocator =
                      allocator::DefaultTypeAllocator>
class UnboundedQueue;
template<typename T, template<typename> class TypeAllocator =
                         allocator::DefaultTypeAllocator>
class LockedQueue;
// Buffers
template<typename T, size_t Bound>
class RingBuffer;
// Hash Tables
namespace detail {

template<typename Key, typename ValueType, class Allocator>
class HashTable;

}

template<typename Key, typename Value>
using HashTable =
    detail::HashTable<Key, Value, allocator::DefaultMemoryAllocator>;

}

#endif
