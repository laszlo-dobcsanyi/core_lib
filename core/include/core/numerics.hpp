#ifndef _CORE_LIB_NUMERICS_HPP_
#define _CORE_LIB_NUMERICS_HPP_

#include "core/base.h"
#include "core/ranges/span.hpp"
#include "core/variant.hpp"

#define CORE_LIB_NUMERICS
#include "core/numerics/math.hpp"
#include "core/numerics/simd.hpp"
#include "core/numerics/vector.hpp"
#include "core/numerics/vector2.hpp"
#include "core/numerics/vector3.hpp"
#include "core/numerics/vector4.hpp"
#include "core/numerics/vector_functions.hpp"
#include "core/numerics/quaternion.hpp"
#include "core/numerics/quaternion_functions.hpp"
#include "core/numerics/matrix.hpp"
#include "core/numerics/matrix.inl"
#include "core/numerics/matrix_storage.hpp"
#include "core/numerics/matrices.hpp"
#include "core/numerics/matrix_functions.hpp"
#include "core/numerics/transformation.hpp"
#include "core/numerics/transformation_functions.hpp"
#include "core/numerics/spline.hpp"
#include "core/numerics/random.hpp"
#include "core/numerics/hash.hpp"
#undef CORE_LIB_NUMERICS

#endif
