#ifndef _CORE_LIB_STRING_HPP_
#define _CORE_LIB_STRING_HPP_

#include "core/base.h"
#include "core/allocators.hpp"

#include "core/string.hh"

#define CORE_LIB_STRING
#include "core/string/cstring.h"
#include "core/string/const_string.hpp"
#include "core/string/stream.hpp"
#include "core/string/parser.hpp"
#undef CORE_LIB_STRING

#endif
