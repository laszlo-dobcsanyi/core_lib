#ifndef _CORE_LIB_ALLOCATORS_ALLOCATION_HPP_
#define _CORE_LIB_ALLOCATORS_ALLOCATION_HPP_

#ifndef CORE_LIB_ALLOCATORS
#  error
#endif

#include "core/memory/base.hpp"

////////////////////////////////////////////////////////////////////////////////
// allocator
////////////////////////////////////////////////////////////////////////////////

namespace allocator {

template<typename T>
using if_void_ptr_t = mpl::if_same_t<mpl::remove_const_t<T>, void *, T>;
template<typename T>
using if_not_void_ptr_t = mpl::if_not_same_t<mpl::remove_const_t<T>, void *, T>;

/*******************************************************************************
** Allocation
*******************************************************************************/

struct Allocation {
  Allocation();
  Allocation(nullptr_t);
  template<typename T>
  Allocation(T * _ptr);
  template<typename T>
  Allocation(T * _ptr, size_t _count);
  Allocation(uintptr_t _memory, size_t _size);
  Allocation(Allocation && _other);
  Allocation(Allocation const &) = delete;
  Allocation & operator=(Allocation && _other);
  Allocation & operator=(Allocation const &) = delete;
  ~Allocation() = default;

  size_t Size() const { return size; }
  bool IsEmpty() const { return memory == 0u || size == 0u; }
  explicit operator bool() const { return !IsEmpty(); }

  template<typename T>
  if_void_ptr_t<T> As();
  template<typename T>
  if_not_void_ptr_t<T> As();
  template<typename T>
  if_void_ptr_t<T> As() const;
  template<typename T>
  if_not_void_ptr_t<T> As() const;

  void Reset();
  uintptr_t Release();
  template<typename T>
  T ReleaseAs();

  friend bool operator==(Allocation const & _left, Allocation const & _right);
  friend bool operator==(Allocation const & _left, Allocation const & _right);

private:
  uintptr_t memory;
  size_t size;
};

bool operator==(Allocation const & _left, Allocation const & _right);
bool operator!=(Allocation const & _left, Allocation const & _right);

}

////////////////////////////////////////////////////////////////////////////////
// allocator
////////////////////////////////////////////////////////////////////////////////

namespace allocator {

/*******************************************************************************
** Allocation
*******************************************************************************/

inline Allocation::Allocation()
    : Allocation(nullptr) {}

inline Allocation::Allocation(nullptr_t)
    : Allocation(0u, 0u) {}

template<typename T>
inline Allocation::Allocation(T * _ptr)
    : Allocation(reinterpret_cast<uintptr_t>(_ptr), sizeof(T)) {}

template<typename T>
inline Allocation::Allocation(T * _ptr, size_t _count)
    : Allocation(reinterpret_cast<uintptr_t>(_ptr), _count * sizeof(T)) {}

inline Allocation::Allocation(uintptr_t _memory, size_t _size)
    : memory(_memory)
    , size(_size) {}

inline Allocation::Allocation(Allocation && _other) {
  CORE_LIB_ASSERT(this != &_other);

  memory = _other.memory;
  size = _other.size;

  _other.memory = 0u;
  _other.size = 0u;
}

inline Allocation & Allocation::operator=(Allocation && _other) {
  CORE_LIB_ASSERT(IsEmpty());
  CORE_LIB_ASSERT(this != &_other);

  memory = _other.memory;
  size = _other.size;

  _other.memory = 0u;
  _other.size = 0u;

  return *this;
}

inline void Allocation::Reset() {
  memory = 0u;
  size = 0u;
}

inline uintptr_t Allocation::Release() {
  uintptr_t result = memory;
  Reset();
  return result;
}

template<typename T>
inline T Allocation::ReleaseAs() {
  T result = As<T>();
  Reset();
  return result;
}

template<typename T>
inline if_void_ptr_t<T> Allocation::As() {
  return reinterpret_cast<T>(memory);
}

template<typename T>
inline if_not_void_ptr_t<T> Allocation::As() {
  CORE_LIB_ASSERT(core::memory::is_aligned(memory, alignof(T)));
  return reinterpret_cast<T>(memory);
}

template<typename T>
inline if_void_ptr_t<T> Allocation::As() const {
  return reinterpret_cast<T>(memory);
}

template<typename T>
inline if_not_void_ptr_t<T> Allocation::As() const {
  CORE_LIB_ASSERT(core::memory::is_aligned(memory, alignof(T)));
  return reinterpret_cast<T>(memory);
}

inline bool operator==(Allocation const & _left, Allocation const & _right) {
  return (_left.memory == _right.memory) && (_left.size == _right.size);
}

inline bool operator!=(Allocation const & _left, Allocation const & _right) {
  return !(_left == _right);
}

}

#endif
