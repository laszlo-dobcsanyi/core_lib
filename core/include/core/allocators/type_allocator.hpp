#ifndef _CORE_LIB_ALLOCATORS_TYPE_ALLOCATOR_HPP_
#define _CORE_LIB_ALLOCATORS_TYPE_ALLOCATOR_HPP_

////////////////////////////////////////////////////////////////////////////////
// allocator
////////////////////////////////////////////////////////////////////////////////

namespace allocator {

/*******************************************************************************
** TypeAllocator
*******************************************************************************/

template<class T, class MemoryAllocator>
class TypeAllocator final {
public:
  template<typename... Args>
  TypeAllocator(Args &&... _args);
  TypeAllocator(TypeAllocator &&) = default;
  TypeAllocator(TypeAllocator const &) = default;
  TypeAllocator & operator=(TypeAllocator &&) = default;
  TypeAllocator & operator=(TypeAllocator const &) = default;
  ~TypeAllocator() = default;

  T * Allocate();

  T * AllocateA(size_t _count);

  template<typename... Args>
  T & Create(Args &&... _args);

  void Deallocate(T * _ptr);

  void DeallocateA(T * _ptr, size_t _count);

  void Destroy(T & _value);

private:
  MemoryAllocator memory_allocator;
};

}

////////////////////////////////////////////////////////////////////////////////
// allocator
////////////////////////////////////////////////////////////////////////////////

namespace allocator {

/*******************************************************************************
** TypeAllocator
*******************************************************************************/

template<class T, class MemoryAllocator>
template<typename... Args>
inline TypeAllocator<T, MemoryAllocator>::TypeAllocator(Args &&... _args)
    : memory_allocator(forward<Args>(_args)...) {}

template<class T, class MemoryAllocator>
inline T * TypeAllocator<T, MemoryAllocator>::Allocate() {
  Allocation allocation = memory_allocator.Allocate(sizeof(T));
  CORE_LIB_ASSERT(allocation);
  return allocation.ReleaseAs<T *>();
}

template<class T, class MemoryAllocator>
inline T * TypeAllocator<T, MemoryAllocator>::AllocateA(size_t _count) {
  Allocation allocation = memory_allocator.Allocate(sizeof(T) * _count);
  CORE_LIB_ASSERT(allocation);
  return allocation.ReleaseAs<T *>();
}

template<class T, class MemoryAllocator>
template<typename... Args>
inline T & TypeAllocator<T, MemoryAllocator>::Create(Args &&... _args) {
  T * memory = Allocate();
  new (memory) T(forward<Args>(_args)...);
  return *memory;
}

template<class T, class MemoryAllocator>
inline void TypeAllocator<T, MemoryAllocator>::Deallocate(T * _ptr) {
  memory_allocator.Deallocate(Allocation(_ptr));
}

template<class T, class MemoryAllocator>
inline void TypeAllocator<T, MemoryAllocator>::DeallocateA(T * _ptr,
                                                           size_t _count) {
  memory_allocator.Deallocate(Allocation(_ptr, _count));
}

template<class T, class MemoryAllocator>
inline void TypeAllocator<T, MemoryAllocator>::Destroy(T & _value) {
  _value.~T();
  Deallocate(&_value);
}

}

#endif