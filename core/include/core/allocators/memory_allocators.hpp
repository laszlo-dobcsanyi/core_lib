#ifndef _CORE_LIB_ALLOCATORS_MEMORY_ALLOCATORS_HPP_
#define _CORE_LIB_ALLOCATORS_MEMORY_ALLOCATORS_HPP_

#ifndef CORE_LIB_ALLOCATORS
#  error
#endif

#include "core/memory/alloc.hpp"
#include "core/memory/aligned_alloc.hpp"

#if defined(CORE_LIB_OS_WINDOWS)
#  include <WinBase.h> // VirtualAlloc
#elif defined(CORE_LIB_OS_LINUX)
#  include <sys/mman.h> // mmap
#endif

////////////////////////////////////////////////////////////////////////////////
// allocator
////////////////////////////////////////////////////////////////////////////////

namespace allocator {

/*******************************************************************************
** NullAllocator
*******************************************************************************/

class NullAllocator final {
public:
  Allocation Allocate(size_t _size);
  void Deallocate(Allocation && _allocation);

  void Reset() {}
};

/*******************************************************************************
** InlineAllocator
*******************************************************************************/

template<size_t Size>
class InlineAllocator final {
public:
  InlineAllocator();
  InlineAllocator(InlineAllocator &&) = delete;
  InlineAllocator(InlineAllocator const &) = delete;
  InlineAllocator & operator=(InlineAllocator &&) = delete;
  InlineAllocator & operator=(InlineAllocator const &) = delete;
  ~InlineAllocator() = default;

  Allocation Allocate(size_t _size);
  void Deallocate(Allocation && _allocation);

  void Reset();

private:
  char memory[Size];
  uintptr_t free;
};

/*******************************************************************************
** HeapAllocator
*******************************************************************************/

class HeapAllocator final {
public:
  Allocation Allocate(size_t _size);
  void Deallocate(Allocation && _allocation);

  void Reset() {}
};

/*******************************************************************************
** AlignedHeapAllocator
*******************************************************************************/

template<size_t Alignment>
class AlignedHeapAllocator final {
public:
  Allocation Allocate(size_t _size);
  void Deallocate(Allocation && _allocation);

  void Reset() {}
};

/*******************************************************************************
** PageAllocator
*******************************************************************************/

template<size_t Size>
class PageAllocator final {
public:
  PageAllocator();
  PageAllocator(PageAllocator &&) = delete;
  PageAllocator(PageAllocator const &) = delete;
  PageAllocator & operator=(PageAllocator &&) = delete;
  PageAllocator & operator=(PageAllocator const &) = delete;
  ~PageAllocator();

  Allocation Allocate(size_t _size);
  void Deallocate(Allocation && _allocation);

  void Reset();

private:
  uintptr_t memory_base;
  uintptr_t free_memory;
  size_t commited_pages;
};

/*******************************************************************************
** AlignedPageAllocator
*******************************************************************************/

template<size_t Size>
class AlignedPageAllocator final {
  // TODO: Implement me!
};

/*******************************************************************************
** IntervalAllocator
*******************************************************************************/

template<size_t Bound, class LowerAllocator, class HigherAllocator>
class IntervalAllocator final {
public:
  IntervalAllocator() = default;
  IntervalAllocator(IntervalAllocator &&) = default;
  IntervalAllocator(IntervalAllocator const &) = default;
  IntervalAllocator & operator=(IntervalAllocator &&) = default;
  IntervalAllocator & operator=(IntervalAllocator const &) = default;
  ~IntervalAllocator() = default;

  Allocation Allocate(size_t _size);
  void Deallocate(Allocation && _allocation);

  void Reset();

private:
  LowerAllocator lower_allocator;
  HigherAllocator higher_allocator;
};

/*******************************************************************************
** ThreadLocalAllocator
*******************************************************************************/

template<class MemoryAllocator>
class ThreadLocalAllocator {
public:
  ThreadLocalAllocator() = default;
  ThreadLocalAllocator(ThreadLocalAllocator &&) = delete;
  ThreadLocalAllocator(ThreadLocalAllocator const &) = delete;
  ThreadLocalAllocator & operator=(ThreadLocalAllocator &&) = delete;
  ThreadLocalAllocator & operator=(ThreadLocalAllocator const &) = delete;
  ~ThreadLocalAllocator() = default;

  Allocation Allocate(size_t _size);
  void Deallocate(Allocation && _allocation);

private:
  static thread_local MemoryAllocator memory_allocator;
};

template<class MemoryAllocator>
thread_local MemoryAllocator
    ThreadLocalAllocator<MemoryAllocator>::memory_allocator;

}

////////////////////////////////////////////////////////////////////////////////
// allocator
////////////////////////////////////////////////////////////////////////////////

namespace allocator {

/*******************************************************************************
** NullAllocator
*******************************************************************************/

inline Allocation NullAllocator::Allocate(size_t /* _size */) {
  return Allocation();
}

inline void NullAllocator::Deallocate(Allocation && _allocation) {
  _allocation.Reset();
}

/*******************************************************************************
** InlineAllocator
*******************************************************************************/

template<size_t Size>
inline InlineAllocator<Size>::InlineAllocator()
    : free(reinterpret_cast<uintptr_t>(&memory)) {}

template<size_t Size>
inline Allocation InlineAllocator<Size>::Allocate(size_t _size) {
  auto memory_end = free + _size;
  auto allocation_end = reinterpret_cast<uintptr_t>(memory) + Size;
  if (memory_end <= allocation_end) {
    uintptr_t memory_start = free;
    free += _size;
    return Allocation(memory_start, _size);
  }
  return Allocation();
}

template<size_t Size>
inline void InlineAllocator<Size>::Deallocate(Allocation && _allocation) {
  if (free == _allocation.As<uintptr_t>() + _allocation.Size()) {
    free = _allocation.ReleaseAs<uintptr_t>();
    return;
  }
  CORE_LIB_ASSERT(false);
}

template<size_t Size>
inline void InlineAllocator<Size>::Reset() {
  free = reinterpret_cast<uintptr_t>(&memory);
}

/*******************************************************************************
** HeapAllocator
*******************************************************************************/

inline Allocation HeapAllocator::Allocate(size_t _size) {
  return Allocation(core::memory::alloc(_size), _size);
}

inline void HeapAllocator::Deallocate(Allocation && _allocation) {
  uintptr_t memory = _allocation.Release();
  core::memory::free(memory);
}

/*******************************************************************************
** AlignedHeapAllocator
*******************************************************************************/

template<size_t Alignment>
inline Allocation AlignedHeapAllocator<Alignment>::Allocate(size_t _size) {
  return Allocation(core::memory::aligned_alloc(Alignment, _size), _size);
}

template<size_t Alignment>
inline void
AlignedHeapAllocator<Alignment>::Deallocate(Allocation && _allocation) {
  uintptr_t memory = _allocation.Release();
  core::memory::aligned_free(memory);
}

/*******************************************************************************
** PageAllocator
*******************************************************************************/

template<size_t Size>
inline PageAllocator<Size>::PageAllocator()
    : memory_base(0u)
    , free_memory(0u)
    , commited_pages(0u) {
  CORE_LIB_ASSERT_TEXT(
      Size % platform::page_size() == 0,
      "PageAllocator requires Size(%ld) to be multiple of PageSize(%ld)", Size,
      platform::page_size());
  void * memory = nullptr;
#if defined(CORE_LIB_OS_WINDOWS)
  memory = (void *)::VirtualAlloc((LPVOID)NULL, (SIZE_T)Size,
                                  (DWORD)MEM_RESERVE, (DWORD)PAGE_NOACCESS);
#elif defined(CORE_LIB_OS_LINUX)
  memory = mmap(nullptr, Size, PROT_NONE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
#endif
  if (memory) {
    memory_base = reinterpret_cast<uintptr_t>(memory);
    free_memory = memory_base;
  } else {
    CORE_LIB_ASSERT_MESSAGE("PageAllocator failed to map memory!");
  }
}

template<size_t Size>
inline PageAllocator<Size>::~PageAllocator() {
  if (memory_base) {
#if defined(CORE_LIB_OS_WINDOWS)
    if (!::VirtualFree((LPVOID)memory_base,
                       /*(SIZE_T)(commited_pages * Size)*/ 0,
                       (DWORD)MEM_RELEASE)) {
      auto error = GetLastError();
      CORE_LIB_ASSERT_MESSAGE("PageAllocator failed to unmap memory: %ld!",
                              error);
    }
#elif defined(CORE_LIB_OS_LINUX)
    if (-1 == munmap(reinterpret_cast<void *>(memory_base), Size)) {
      CORE_LIB_ASSERT_MESSAGE("PageAllocator failed to unmap memory!");
    }
#endif
  }
}

template<size_t Size>
inline Allocation PageAllocator<Size>::Allocate(size_t _size) {
  CORE_LIB_ASSERT(_size <= Size);
  if (memory_base) {
    auto const memory_end = memory_base + Size;
    auto const allocation_end = free_memory + _size;
    if (allocation_end <= memory_end) {
      auto commited_end =
          memory_base + (commited_pages * platform::page_size());
      while (commited_end < allocation_end) {
        bool commited = false;
#if defined(CORE_LIB_OS_WINDOWS)
        if (::VirtualAlloc((LPVOID)commited_end, (SIZE_T)platform::page_size(),
                           (DWORD)MEM_COMMIT, (DWORD)PAGE_READWRITE)) {
          commited = true;
        }
#elif defined(CORE_LIB_OS_LINUX)
        if (-1 != mprotect(reinterpret_cast<void *>(commited_end),
                           platform::page_size(), PROT_READ | PROT_WRITE)) {
          commited = true;
        }
#endif
        if (commited) {
          ++commited_pages;
          commited_end = memory_base + (commited_pages * platform::page_size());
        } else {
          CORE_LIB_ASSERT_MESSAGE("PageAllocator failed to commit memory!");
          return Allocation();
        }
      }
      auto const allocated_memory = free_memory;
      free_memory = allocation_end;
      return Allocation(allocated_memory, _size);
    } else {
      CORE_LIB_ASSERT_MESSAGE("PageAllocator out of memory!");
      return Allocation();
    }
  } else {
    return Allocation();
  }
}

template<size_t Size>
inline void PageAllocator<Size>::Deallocate(Allocation && _allocation) {
  _allocation.Reset();
}

template<size_t Size>
inline void PageAllocator<Size>::Reset() {
  free_memory = memory_base;
  commited_pages = 0u;
}

/*******************************************************************************
** IntervalAllocator
*******************************************************************************/

template<size_t Bound, class LowerAllocator, class HigherAllocator>
inline Allocation
IntervalAllocator<Bound, LowerAllocator, HigherAllocator>::Allocate(
    size_t _size) {
  if (_size <= Bound) {
    return lower_allocator.Allocate(_size);
  } else {
    return higher_allocator.Allocate(_size);
  }
}

template<size_t Bound, class LowerAllocator, class HigherAllocator>
inline void
IntervalAllocator<Bound, LowerAllocator, HigherAllocator>::Deallocate(
    Allocation && _allocation) {
  if (_allocation.Size() <= Bound) {
    return lower_allocator.Deallocate(move(_allocation));
  } else {
    return higher_allocator.Deallocate(move(_allocation));
  }
}

template<size_t Bound, class LowerAllocator, class HigherAllocator>
inline void IntervalAllocator<Bound, LowerAllocator, HigherAllocator>::Reset() {
  lower_allocator.Reset();
  higher_allocator.Reset();
}

/*******************************************************************************
** ThreadLocalAllocator
*******************************************************************************/

template<class MemoryAllocator>
inline Allocation
ThreadLocalAllocator<MemoryAllocator>::Allocate(size_t _size) {
  return memory_allocator.Allocate(_size);
}

template<class MemoryAllocator>
inline void
ThreadLocalAllocator<MemoryAllocator>::Deallocate(Allocation && _allocation) {
  memory_allocator.Deallocate(move(_allocation));
}

}

#endif