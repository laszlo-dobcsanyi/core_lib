#ifndef _CORE_LIB_ALLOCATORS_POLICIES_HPP_
#define _CORE_LIB_ALLOCATORS_POLICIES_HPP_

#ifndef CORE_LIB_ALLOCATORS
#  error
#endif

////////////////////////////////////////////////////////////////////////////////
// allocator policy
////////////////////////////////////////////////////////////////////////////////

namespace allocator { namespace policy {

/*******************************************************************************
** Linear
*******************************************************************************/

template<size_t BaseSize>
struct Linear {
  static size_t Calculate(size_t _capacity) { return _capacity + BaseSize; }
};

/*******************************************************************************
** Exponential
*******************************************************************************/

template<size_t BaseSize, size_t Exponent>
struct Exponential {
  static size_t Calculate(size_t _capacity) {
    if (_capacity != 0)
      return _capacity * Exponent;
    return BaseSize;
  }
};

}}

#endif