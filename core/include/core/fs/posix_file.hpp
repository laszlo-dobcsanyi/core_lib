#ifndef _CORE_LIB_FS_POSIX_FILE_HPP_
#define _CORE_LIB_FS_POSIX_FILE_HPP_

#include "core/fs/base.h"

////////////////////////////////////////////////////////////////////////////////
// x detail
////////////////////////////////////////////////////////////////////////////////

namespace core { namespace fs { namespace posix { namespace detail {

template<class Transform>
class BinaryFileReader;
template<class Transform>
class BinaryFileWriter;

}}}}

////////////////////////////////////////////////////////////////////////////////
// s posix
////////////////////////////////////////////////////////////////////////////////

namespace core { namespace fs { namespace posix {

using BigEndianBinaryFileReader =
    detail::BinaryFileReader<memory::transform::ToBigEndian>;
using BigEndianBinaryFileWriter =
    detail::BinaryFileWriter<memory::transform::ToBigEndian>;

using LittleEndianBinaryFileReader =
    detail::BinaryFileReader<memory::transform::ToLittleEndian>;
using LittleEndianBinaryFileWriter =
    detail::BinaryFileWriter<memory::transform::ToLittleEndian>;

}}}

////////////////////////////////////////////////////////////////////////////////
// x detail
////////////////////////////////////////////////////////////////////////////////

namespace core { namespace fs { namespace posix { namespace detail {

/*******************************************************************************
** File
*******************************************************************************/

class File {
public:
  bool IsOpen() const;

  size_t Position();
  size_t Size() const;

  void Close();

protected:
  Value<FILE *, nullptr> file;
  size_t size = 0u;

  File() = default;
  File(File &&) = default;
  File(File const &) = delete;
  File & operator=(File &&) = default;
  File & operator=(File const &) = delete;
  ~File();

  bool Open(FilePath const & _file_path, char const * _mode);
};

}}}}

////////////////////////////////////////////////////////////////////////////////
// s posix
////////////////////////////////////////////////////////////////////////////////

namespace core { namespace fs { namespace posix {

#if 0

/*******************************************************************************
** TextFileReader
*******************************************************************************/

class TextFileReader final : public detail::File {
public:
  TextFileReader() = default;
  TextFileReader( TextFileReader && ) = default;
  TextFileReader( TextFileReader const & ) = delete;
  TextFileReader & operator=( TextFileReader && ) = default;
  TextFileReader & operator=( TextFileReader const & ) = delete;
  ~TextFileReader() = default;

  bool Open( FilePath const & _file_path );

  Optional< const_string > ReadLine();

  int ReadFormatted( char const * _format, ... );

  Optional< const_string > Read();

  bool Read( int8 & _value );
  bool Read( uint8 & _value );
  bool Read( int16 & _value );
  bool Read( uint16 & _value );
  bool Read( int32 & _value );
  bool Read( uint32 & _value );
  bool Read( int64 & _value );
  bool Read( uint64 & _value );
  bool Read( float & _value );
  bool Read( double & _value );
};

#endif

#if 0

/*******************************************************************************
** TextFileWriter
*******************************************************************************/

class TextFileWriter final : public detail::File {
public:
  TextFileWriter() = default;
  TextFileWriter( TextFileWriter && ) = default;
  TextFileWriter( TextFileWriter const & ) = delete;
  TextFileWriter & operator=( TextFileWriter && ) = default;
  TextFileWriter & operator=( TextFileWriter const & ) = delete;
  ~TextFileWriter() = default;

  bool Open( FilePath const & _file_path );

  bool WriteLine( const_string const & _line );

  int WriteFormatted( char const * _format, ... );

  bool Write( char const * _cstring );

  template< class StringStorage >
  bool Write( string::const_string< StringStorage > const & _string );

  bool Write( int8 _value );
  bool Write( uint8 _value );
  bool Write( int16 _value );
  bool Write( uint16 _value );
  bool Write( int32 _value );
  bool Write( uint32 _value );
  bool Write( int64 _value );
  bool Write( uint64 _value );
  bool Write( float _value );
  bool Write( double _value );
};

#endif

}}}

////////////////////////////////////////////////////////////////////////////////
// x detail
////////////////////////////////////////////////////////////////////////////////

namespace core { namespace fs { namespace posix { namespace detail {

/*******************************************************************************
** BinaryFileReader
*******************************************************************************/

template<class Transform>
class BinaryFileReader final : public File {
public:
  BinaryFileReader() = default;
  BinaryFileReader(BinaryFileReader &&) = default;
  BinaryFileReader(BinaryFileReader const &) = delete;
  BinaryFileReader & operator=(BinaryFileReader &&) = default;
  BinaryFileReader & operator=(BinaryFileReader const &) = delete;
  ~BinaryFileReader() = default;

  bool Open(FilePath const & _file_path);

  template<class StringStorage>
  bool Read(string::const_string<StringStorage> & _string);

  template<typename T>
  mpl::if_arithmetic_t<T, bool> Read(T & _value);
};

/*******************************************************************************
** BinaryFileWriter
*******************************************************************************/

template<class Transform>
class BinaryFileWriter final : public File {
public:
  BinaryFileWriter() = default;
  BinaryFileWriter(BinaryFileWriter &&) = default;
  BinaryFileWriter(BinaryFileWriter const &) = delete;
  BinaryFileWriter & operator=(BinaryFileWriter &&) = default;
  BinaryFileWriter & operator=(BinaryFileWriter const &) = delete;
  ~BinaryFileWriter() = default;

  bool Open(FilePath const & _file_path);

  bool Write(span<byte> _bytes);
  bool Write(span<byte const> _bytes);

  bool Write(char const * _cstring);
  template<class StringStorage>
  bool Write(string::const_string<StringStorage> const & _string);

  template<typename T>
  mpl::if_arithmetic_t<T, bool> Write(T _value);
};

}}}}

////////////////////////////////////////////////////////////////////////////////
// x detail
////////////////////////////////////////////////////////////////////////////////

namespace core { namespace fs { namespace posix { namespace detail {

/*******************************************************************************
** File
*******************************************************************************/

inline bool File::IsOpen() const { return file != nullptr; }

inline size_t File::Position() { return ::ftell(file); }

inline size_t File::Size() const { return size; }

inline File::~File() {
  if (IsOpen()) {
    Close();
  }
}

inline bool File::Open(FilePath const & _file_path, char const * _mode) {
  CORE_LIB_ASSERT(!file);
  CORE_LIB_ASSERT(_file_path);
  CORE_LIB_ASSERT(_mode);
#if defined(CORE_LIB_COMPILER_MSVC)
  auto error = ::fopen_s(&file, cstr(_file_path), _mode);
  if (IsOpen()) {
    ::fseek(file, 0u, SEEK_END);
    size = Position();
    ::fseek(file, 0u, SEEK_SET);
  }
  return !error;
#else
  file = ::fopen(cstr(_file_path), _mode);
  if (IsOpen()) {
    ::fseek(file, 0u, SEEK_END);
    size = Position();
    ::fseek(file, 0u, SEEK_SET);
  }
  return (file != nullptr);
#endif
}

inline void File::Close() {
  if (file) {
    ::fclose(file);
  }
}

}}}}

////////////////////////////////////////////////////////////////////////////////
// s posix
////////////////////////////////////////////////////////////////////////////////

namespace core { namespace fs { namespace posix {

#if 0

/*******************************************************************************
** TextFileReader
*******************************************************************************/

inline bool TextFileReader::Open( FilePath const & _file_path ) {
  return File::Open( _file_path, "r" );
}

inline Optional< const_string > TextFileReader::ReadLine() {
  CORE_LIB_UNIMPLEMENTED;
  return nullopt;
}

inline int TextFileReader::ReadFormatted( char const * _format, ... ) {
  CORE_LIB_ASSERT( IsOpen() );
  if ( file ) {
    va_list arguments;
    va_start( arguments, _format );
    auto count = vfscanf( file, _format, arguments );
    va_end( arguments );
    return count;
  }
  return -1;
}

inline Optional< const_string > TextFileReader::Read() {
  // TODO: This buffer size is for testing only!
  // Small string
  static const size_t kBufferSize = 4u;
  char cstring[ kBufferSize ];
  auto end = reinterpret_cast< char * >( reinterpret_cast< uintptr_t >( cstring ) + kBufferSize );
  auto current = cstring;
  while ( current < end ) {
    int result = ::fgetc( file );
    if ( result != EOF ) {
      auto ch = static_cast< char >( result );
      *current = ch;
      if ( ch != '\0' ) {
        ++current;
      } else {
        if ( current != cstring ) {
          auto const size =   reinterpret_cast< uintptr_t >( current )
                            - reinterpret_cast< uintptr_t >( cstring );
          return Optional< const_string >( cstring, static_cast< uint32 >( size ) );
        } else {
          return Optional< const_string >();
        }
      }
    } else {
      if ( auto error = ::ferror( file ) ) {
        // Error
        return nullopt;
      } else {
        // End of file
        CORE_LIB_ASSERT( ::feof( file ) );
        // TODO: Size
        return Optional< const_string >( cstring, 0u );
      }
    }
  }
  // Large string
  CORE_LIB_UNIMPLEMENTED;
  return nullopt;
}

inline bool TextFileReader::Read( int8 & _value ) {
  CORE_LIB_STATIC_ASSERT( sizeof( signed char ) == sizeof( int8 ) );
  return ::fscanf( file, "%hhi", &_value );
}

inline bool TextFileReader::Read( uint8 & _value ) {
  CORE_LIB_STATIC_ASSERT( sizeof( unsigned char ) == sizeof( uint8 ) );
  return ::fscanf( file, "%hhu", &_value );
}

inline bool TextFileReader::Read( int16 & _value ) {
  CORE_LIB_STATIC_ASSERT( sizeof( signed short int ) == sizeof( int16 ) );
  return ::fscanf( file, "%hi", &_value );
}

inline bool TextFileReader::Read( uint16 & _value ) {
  CORE_LIB_STATIC_ASSERT( sizeof( unsigned short int ) == sizeof( uint16 ) );
  return ::fscanf( file, "%hu", &_value );
}

inline bool TextFileReader::Read( int32 & _value ) {
  CORE_LIB_STATIC_ASSERT( sizeof( signed int ) == sizeof( int32 ) );
  return ::fscanf( file, "%i", &_value );
}

inline bool TextFileReader::Read( uint32 & _value ) {
  CORE_LIB_STATIC_ASSERT( sizeof( unsigned int ) == sizeof( uint32 ) );
  return ::fscanf( file, "%u", &_value );
}

inline bool TextFileReader::Read( int64 & _value ) {
  CORE_LIB_STATIC_ASSERT( sizeof( signed long int ) == sizeof( int64 ) );
  return ::fscanf( file, "%li", &_value );
}

inline bool TextFileReader::Read( uint64 & _value ) {
  CORE_LIB_STATIC_ASSERT( sizeof( unsigned long int ) == sizeof( uint64 ) );
  return ::fscanf( file, "%lu", &_value );
}

inline bool TextFileReader::Read( float & _value ) {
  return ::fscanf( file, "%f", &_value );
}

inline bool TextFileReader::Read( double & _value ) {
  return ::fscanf( file, "%lf", &_value );
}

#endif

#if 0

/*******************************************************************************
** TextFileWriter
*******************************************************************************/

inline bool TextFileWriter::Open( FilePath const & _file_path ) {
  return File::Open( _file_path, "w" );
}

inline bool TextFileWriter::WriteLine( const_string const & _line ) {
  CORE_LIB_UNIMPLEMENTED;
  return false;
}

inline int TextFileWriter::WriteFormatted( char const * _format, ... ) {
  if ( IsOpen() ) {
    va_list arguments;
    va_start( arguments, _format );
    auto count = fprintf( file, _format, arguments );
    va_end( arguments );
    return count;
  }
  return -1;
}

inline bool TextFileWriter::Write( char const * _cstring ) {
  if ( ::fputs( _cstring, file ) != EOF ) {
    if ( ::fputc( '\0', file ) != EOF ) {
      return true;
    }
  }
  return false;
}

template< class StringStorage >
inline bool TextFileWriter::Write( string::const_string< StringStorage > const & _string ) {
  return Write( cstr( _string ) );
}

inline bool TextFileWriter::Write( int8 _value ) {
  CORE_LIB_STATIC_ASSERT( sizeof( signed char ) == sizeof( int8 ) );
  return ::fprintf( file, "%hhi", _value );
}

inline bool TextFileWriter::Write( uint8 _value ) {
  CORE_LIB_STATIC_ASSERT( sizeof( unsigned char ) == sizeof( uint8 ) );
  return ::fprintf( file, "%hhu", _value );
}

inline bool TextFileWriter::Write( int16 _value ) {
  CORE_LIB_STATIC_ASSERT( sizeof( signed short int ) == sizeof( int16 ) );
  return ::fprintf( file, "%hi", _value );
}

inline bool TextFileWriter::Write( uint16 _value ) {
  CORE_LIB_STATIC_ASSERT( sizeof( unsigned short int ) == sizeof( uint16 ) );
  return ::fprintf( file, "%hu", _value );
}

inline bool TextFileWriter::Write( int32 _value ) {
  CORE_LIB_STATIC_ASSERT( sizeof( signed int ) == sizeof( int32 ) );
  return ::fprintf( file, "%i", _value );
}

inline bool TextFileWriter::Write( uint32 _value ) {
  CORE_LIB_STATIC_ASSERT( sizeof( unsigned int ) == sizeof( uint32 ) );
  return ::fprintf( file, "%u", _value );
}

inline bool TextFileWriter::Write( int64 _value ) {
  CORE_LIB_STATIC_ASSERT( sizeof( signed long int ) == sizeof( int64 ) );
  return ::fprintf( file, "%li", _value );
}

inline bool TextFileWriter::Write( uint64 _value ) {
  CORE_LIB_STATIC_ASSERT( sizeof( unsigned long int ) == sizeof( uint64 ) );
  return ::fprintf( file, "%lu", _value );
}

inline bool TextFileWriter::Write( float _value ) {
  return ::fprintf( file, "%f", _value );
}

inline bool TextFileWriter::Write( double _value ) {
  return ::fprintf( file, "%lf", _value );
}

#endif

}}}

////////////////////////////////////////////////////////////////////////////////
// x detail
////////////////////////////////////////////////////////////////////////////////

namespace core { namespace fs { namespace posix { namespace detail {

/*******************************************************************************
** BinaryFileReader
*******************************************************************************/

template<class Transform>
inline bool BinaryFileReader<Transform>::Open(FilePath const & _file_path) {
  return File::Open(_file_path, "rb");
}

template<class Transform>
template<class StringStorage>
inline bool BinaryFileReader<Transform>::Read(
    string::const_string<StringStorage> & _string) {
  char buffer[512u + 1u];
  auto const start_pos = File::Position();
  auto const buffer_size =
      ::fread(reinterpret_cast<void *>(&buffer), 1u, 512u, file);
  if (0u < buffer_size) {
    for (auto current = 0u; current < buffer_size; ++current) {
      auto const ch = buffer[current];
      auto end_pos = start_pos;
      if (ch == '\0') {
        end_pos = start_pos + current + 1u;
      } else if (start_pos + current == File::Size()) {
        buffer[current + 1u] = '\0';
        end_pos = start_pos + current + 1u;
      }
      if (end_pos != start_pos) {
        _string =
            string::const_string<StringStorage>(StringStorage(buffer, current));
        ::fseek(file, end_pos, SEEK_SET);
        return true;
      }
    }
    // TODO: Buffer too small.
    CORE_LIB_ASSERT(false);
    return false;
  }
  return false;
}

template<class Transform>
template<typename T>
inline mpl::if_arithmetic_t<T, bool>
BinaryFileReader<Transform>::Read(T & _value) {
  if (::fread(reinterpret_cast<void *>(&_value), sizeof(_value), 1u, file)) {
    Transform::Read(_value);
    return true;
  }
  return false;
}

/*******************************************************************************
** BinaryFileWriter
*******************************************************************************/

template<class Transform>
inline bool BinaryFileWriter<Transform>::Open(FilePath const & _file_path) {
  return File::Open(_file_path, "wb");
}

template<class Transform>
inline bool BinaryFileWriter<Transform>::Write(span<byte> _bytes) {
  if (!_bytes.empty()) {
    return ::fwrite(_bytes.data(), _bytes.size(), 1u, file);
  }
  return false;
}

template<class Transform>
inline bool BinaryFileWriter<Transform>::Write(span<byte const> _bytes) {
  if (!_bytes.empty()) {
    return ::fwrite(_bytes.data(), _bytes.size(), 1u, file);
  }
  return false;
}

template<class Transform>
inline bool BinaryFileWriter<Transform>::Write(char const * _cstring) {
  return ::fwrite(_cstring, strlen(_cstring) + 1u, 1u, file);
}

template<class Transform>
template<class StringStorage>
inline bool BinaryFileWriter<Transform>::Write(
    string::const_string<StringStorage> const & _string) {
  return ::fwrite(cstr(_string), strlen(_string) + 1u, 1u, file);
}

template<class Transform>
template<typename T>
inline mpl::if_arithmetic_t<T, bool>
BinaryFileWriter<Transform>::Write(T _value) {
  Transform::Write(_value);
  return ::fwrite(reinterpret_cast<void const *>(&_value), sizeof(_value), 1u,
                  file);
}

}}}}

#endif
