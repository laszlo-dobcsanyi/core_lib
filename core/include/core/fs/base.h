#ifndef _CORE_LIB_FS_BASE_H_
#define _CORE_LIB_FS_BASE_H_

#include "core/string.hpp"

#if defined(CORE_LIB_OS_LINUX)
#  include <sys/types.h>
#  include <sys/stat.h>
#  include <fcntl.h>
#  include <dirent.h>
#  include <stdio.h>
#  include <pwd.h>
#  include <unistd.h>
#endif

#define LOG_FS(__severity__, ...) CORE_LIB_LOG(__severity__, __VA_ARGS__)

// TODO: All functions should use NativeString parameters instead of char const * !

////////////////////////////////////////////////////////////////////////////////
// core fs
////////////////////////////////////////////////////////////////////////////////

namespace core { namespace fs {

// TODO: Implement a real NativeString!

/*******************************************************************************
** NativeString
*******************************************************************************/

class NativeString final {
public:
  NativeString() = delete;
  NativeString(char const * _cstring)
      : cstring(_cstring) {}
  NativeString(NativeString &&) = default;
  NativeString(NativeString const &) = delete;
  NativeString & operator=(NativeString &&) = default;
  NativeString & operator=(NativeString const &) = delete;
  ~NativeString() = default;

#if defined(CORE_LIB_OS_WINDOWS)
  operator char const *() const { return cstring; }
#else
  operator char const *() const { return cstring; }
#endif

private:
#if defined(CORE_LIB_OS_WINDOWS)
  char const * cstring;
#else
  char const * cstring;
#endif
};

/*******************************************************************************
** FileSize
*******************************************************************************/

#if defined(CORE_LIB_OS_WINDOWS)
using FileSize = int64;
static constexpr FileSize InvalidFileSize = -1;
#elif defined(CORE_LIB_OS_LINUX)
using FileSize = off_t;
static constexpr FileSize InvalidFileSize = -1;
#endif

/*******************************************************************************
** FileDescriptor
*******************************************************************************/

#if defined(CORE_LIB_OS_WINDOWS)
using FileDescriptorType = HANDLE;
static constexpr FileDescriptorType InvalidFileDescriptor = nullptr;
#elif defined(CORE_LIB_OS_LINUX)
using FileDescriptorType = int;
static constexpr FileDescriptorType InvalidFileDescriptor = -1;
#endif
using FileDescriptor = Value<FileDescriptorType, InvalidFileDescriptor>;

/*******************************************************************************
** FileMapping
*******************************************************************************/

#if defined(CORE_LIB_OS_WINDOWS)
using FileMappingType = HANDLE;
static constexpr FileMappingType InvalidFileMapping = nullptr;
using FileMapping = Value<FileMappingType, InvalidFileMapping>;
#endif

/*******************************************************************************
** PathSeparator
*******************************************************************************/

inline constexpr char const * PathSeparator() {
#if defined(CORE_LIB_OS_WINDOWS)
  return "\\";
#elif defined(CORE_LIB_OS_LINUX)
  return "/";
#endif
}

/*******************************************************************************
** OpenMode
*******************************************************************************/

enum class OpenMode {
  ReadOnly,
  WriteOnly,
};

/*******************************************************************************
** cwd
*******************************************************************************/

inline const_string cwd() {
#if defined(CORE_LIB_OS_WINDOWS)
  static const DWORD DefaultPathSize = 1024;
  char buffer[DefaultPathSize];
  DWORD result = ::GetCurrentDirectory(MAX_PATH, buffer);
  if (0 == result) {
    LOG_FS(ERROR, "Failed to get current directory!\n");
    return const_string();
  }
  if (result <= DefaultPathSize) {
    return const_string(buffer);
  } else {
    // TODO: Create const_string storage instead of malloc
    if (char * allocated_buffer = reinterpret_cast<char *>(::malloc(result))) {
      if (0 == ::GetCurrentDirectory(result, allocated_buffer)) {
        LOG_FS(ERROR, "[FileSystem] Failed to get current directory!\n");
        return const_string();
      }
      const_string path =
          const_string(allocated_buffer, static_cast<size_t>(result) - 1);
      ::free(allocated_buffer);
      return path;
    } else {
      LOG_FS(ERROR, "Failed to allocate memory!\n");
      return const_string();
    }
  }
#elif defined(CORE_LIB_OS_LINUX)
  const long path_max = ::pathconf(".", _PC_PATH_MAX);
  size_t path_size;
  if (-1 == path_max)
    path_size = 1024;
  else if (10240 < path_max)
    path_size = 10240;
  else
    path_size = path_max;

  char * ptr = nullptr;
  char * buffer = nullptr;
  while (ptr == nullptr) {
    // TODO: Create const_string storage instead of realloc
    if ((buffer = reinterpret_cast<char *>(::realloc(buffer, path_size))) ==
        nullptr) {
      LOG_FS(ERROR, "Failed to allocate memory!\n");
      return const_string();
    }

    ptr = ::getcwd(buffer, path_size);
    if (ptr == NULL && errno != ERANGE) {
      LOG_FS(ERROR, "Failed to get current working directory!\n");
      ::free(buffer);
      return const_string();
    }
    path_size *= 2;
  }
  const_string path = const_string(buffer);
  ::free(buffer);
  return path;
#endif
}

/*******************************************************************************
** executable_path
*******************************************************************************/

inline const_string executable_path() {
#if defined(CORE_LIB_OS_WINDOWS)
  DWORD storage_size = 1024;
  string::storage::UniqueStorage storage(storage_size);
  DWORD written = ::GetModuleFileName(nullptr, storage.Get(), storage_size);
  if (written == 0) {
    LOG_FS(ERROR, "::GetModuleFileName failed!\n");
    return const_string();
  }
  return const_string(move(storage));
#elif defined(CORE_LIB_OS_LINUX)
  size_t storage_size = 1024;
  string::storage::UniqueStorage storage(storage_size);
  ssize_t written =
      ::readlink("/proc/self/exe", storage.Get(), storage_size - 1);
  if (written == -1) {
    LOG_FS(ERROR, "::readlink failed!\n");
    return const_string();
  }
  storage.Get()[written] = '\0';
  return const_string(move(storage));
#endif
}

/*******************************************************************************
** file_exists
*******************************************************************************/

inline bool file_exists(const_string const & _name) {
#if defined(CORE_LIB_OS_WINDOWS)
  DWORD attributes = ::GetFileAttributes(cstr(_name));
  if (attributes == INVALID_FILE_ATTRIBUTES)
    return false;
  return (attributes & FILE_ATTRIBUTE_DIRECTORY) == 0;
#elif defined(CORE_LIB_OS_LINUX)
  struct stat buffer;
  return (::stat(cstr(_name), &buffer) == 0);
#endif
}

/*******************************************************************************
** directory_exists
*******************************************************************************/

inline bool directory_exists(const_string const & _name) {
#if defined(CORE_LIB_OS_WINDOWS)
  DWORD attributes = ::GetFileAttributes(cstr(_name));
  if (attributes == INVALID_FILE_ATTRIBUTES)
    return false;
  return (attributes & FILE_ATTRIBUTE_DIRECTORY) != 0;
#elif defined(CORE_LIB_OS_LINUX)
  struct stat buffer;
  return (::stat(cstr(_name), &buffer) == 0) &&
         ((buffer.st_mode & S_IFMT) == S_IFDIR);
#endif
}

/*******************************************************************************
** remove
*******************************************************************************/

inline bool remove(NativeString _native_string) {
#if defined(CORE_LIB_OS_WINDOWS)
  return ::DeleteFile(_native_string);
#else
  return ::remove(_native_string) == 0;
#endif
}

// TODO: Implement an exist function, which checks if a file or directory.

/*******************************************************************************
** file_size_by_name
*******************************************************************************/

inline FileSize file_size_by_name(const_string const & _file_path) {
#if defined(CORE_LIB_OS_WINDOWS)
  WIN32_FIND_DATAA data;
  FileDescriptor descriptor = ::FindFirstFileA(cstr(_file_path), &data);
  if (InvalidFileDescriptor == descriptor) {
    return InvalidFileSize;
  }
  ::FindClose(descriptor);
  int64 file_size_high = static_cast<int64>(data.nFileSizeHigh);
  int64 file_size_low = static_cast<int64>(data.nFileSizeLow);
  return file_size_low | (file_size_high << 32u);
#elif defined(CORE_LIB_OS_LINUX)
  struct stat buffer;
  if (::stat(cstr(_file_path), &buffer) == -1) {
    return InvalidFileSize;
  }
  return static_cast<FileSize>(buffer.st_size);
#endif
}

/*******************************************************************************
** file_size_by_descriptor
*******************************************************************************/

inline FileSize file_size_by_descriptor(FileDescriptor _descriptor) {
#if defined(CORE_LIB_OS_WINDOWS)
  DWORD file_size_high;
  int64 file_size_low =
      static_cast<int64>(::GetFileSize(_descriptor, &file_size_high));
  return file_size_low | (static_cast<int64>(file_size_high) << 32u);
#elif defined(CORE_LIB_OS_LINUX)
  struct stat buffer;
  if (fstat(_descriptor, &buffer) == -1) {
    return InvalidFileSize;
  }
  return static_cast<FileSize>(buffer.st_size);
#endif
}

namespace {

inline size_t ScanBackward(const_string const & _name,
                           char const * _separator) {
  char const * cstring = cstr(_name);
  size_t start = reinterpret_cast<size_t>(cstring);
  size_t result = _name.Length() + 1u;
  while ((cstring = std::strstr(cstring, _separator))) {
    result = reinterpret_cast<size_t>(cstring) - start;
    ++cstring;
  }
  return result;
}

}

/*******************************************************************************
** file_name_with_extension
*******************************************************************************/

inline const_string file_name_with_extension(const_string const & _file_path) {
  auto separator_position = ScanBackward(_file_path, PathSeparator());
  if (separator_position + 1u < _file_path.Length()) {
    auto cstring = cstr(_file_path) + separator_position + 1u;
    auto length = _file_path.Length() - (separator_position + 1u);
    using Storage = typename const_string::storage_type;
    Storage storage(length);
    std::memcpy(storage.Get(), cstring, length);
    return const_string(move(storage));
  }
  return const_string();
}

/*******************************************************************************
** file_name
*******************************************************************************/

inline const_string file_name(const_string const & _file_path) {
  auto full_name = file_name_with_extension(_file_path);
  auto dot_position = ScanBackward(full_name, ".");
  if (0u < dot_position) {
    if (dot_position < full_name.Length() + 1u) {
      using Storage = typename const_string::storage_type;
      Storage storage(dot_position);
      std::memcpy(storage.Get(), cstr(full_name), dot_position);
      return const_string(move(storage));
    }
    return _file_path;
  }
  return const_string();
}

/*******************************************************************************
** file_extension
*******************************************************************************/

inline const_string file_extension(const_string const & _file_path) {
  auto full_name = file_name_with_extension(_file_path);
  auto dot_position = ScanBackward(full_name, ".");
  if (dot_position < full_name.Length() + 1u) {
    auto cstring = cstr(full_name) + dot_position + 1u;
    auto length = full_name.Length() - (dot_position + 1u);
    using Storage = typename const_string::storage_type;
    Storage storage(length);
    std::memcpy(storage.Get(), cstring, length);
    return const_string(move(storage));
  }
  return const_string();
}

/*******************************************************************************
** path_parent
*******************************************************************************/

inline const_string path_parent(const_string const & _path) {
  auto separator_position = ScanBackward(_path, PathSeparator());
  if (separator_position < _path.Length() + 1u) {
    auto cstring = cstr(_path);
    auto length = separator_position;
    using Storage = typename const_string::storage_type;
    Storage storage(length);
    std::memcpy(storage.Get(), cstring, length);
    return const_string(move(storage));
  }
  return _path;
}

/*******************************************************************************
** getenv
*******************************************************************************/

inline const_string getenv(const_string const & _variable_name) {
  CORE_LIB_MSVC_WARNING_PUSH
  CORE_LIB_MSVC_WARNING_DISABLE(4996)
  return const_string(::getenv(cstr(_variable_name)));
  CORE_LIB_MSVC_WARNING_POP
}

/*******************************************************************************
** DirectoryType
*******************************************************************************/

enum class DirectoryType {
  Current,
  Home,
  Executable,
#if 0
  Desktop,
#endif
};

/*******************************************************************************
** get_directory_type_path
*******************************************************************************/

inline const_string get_directory_type_path(DirectoryType _directory_type) {
  switch (_directory_type) {
    case DirectoryType::Current: {
      return cwd();
    } break;
    case DirectoryType::Home: {
#if defined(CORE_LIB_OS_WINDOWS)
      if (auto user_profile = getenv(const_string("USERPROFILE"))) {
        return user_profile;
      } else {
        auto home_drive = getenv(const_string("HOMEDRIVE"));
        auto home_path = getenv(const_string("HOMEPATH"));
        string::Stream stream;
        stream << home_drive << home_path;
        return stream.Get();
      }
#elif defined(CORE_LIB_OS_LINUX)
      if (struct passwd * pwd = getpwuid(getuid())) {
        return pwd->pw_dir;
      } else {
        return getenv(const_string("HOME"));
      }
#endif
    } break;
    case DirectoryType::Executable: {
      return path_parent(executable_path());
    } break;
#if 0
    case DirectoryType::Desktop: {
      CORE_LIB_UNIMPLEMENTED;
    } break;
#endif
  };
  return const_string();
}

}}

#endif
