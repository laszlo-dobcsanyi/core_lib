#ifndef _CORE_LIB_FS_MAPPED_FILE_HPP_
#define _CORE_LIB_FS_MAPPED_FILE_HPP_

#include "core/byte_stream.hpp"
#include "core/optional.hpp"
#include "core/fs/basic_file.h"
#include "core/fs/filepath.h"

#define LOG_MAPPED_FILE(__severity__, ...)                                     \
  CORE_LIB_LOG(__severity__, __VA_ARGS__)

////////////////////////////////////////////////////////////////////////////////
// core fs
////////////////////////////////////////////////////////////////////////////////

namespace core { namespace fs {

/*******************************************************************************
** MemoryMappingReader
*******************************************************************************/

class MemoryMappingReader : public ByteStreamReader {
public:
  FileSize offset;

  MemoryMappingReader() = default;
  MemoryMappingReader(const char * _memory, FileSize _size, FileSize _offset);
  MemoryMappingReader(MemoryMappingReader && _other);
  MemoryMappingReader(MemoryMappingReader const &) = delete;
  MemoryMappingReader & operator=(MemoryMappingReader && _other);
  MemoryMappingReader & operator=(MemoryMappingReader const &) = delete;
  ~MemoryMappingReader();
};

/*******************************************************************************
** MappedFile
*******************************************************************************/

class MappedFile {
public:
  MappedFile() = default;
  MappedFile(MappedFile &&) = default;
  MappedFile(MappedFile const &) = delete;
  MappedFile & operator=(MappedFile &&) = default;
  MappedFile & operator=(MappedFile const &) = delete;
  ~MappedFile();

  bool IsOpen() const;
  explicit operator bool() const { return IsOpen(); }

  FileSize GetSize() const { return basic_file.GetSize(); }

  bool Open(FilePath const & _file_path, OpenMode _open_mode);
  void Close();

  MemoryMappingReader GetMappingReader(FileSize _offset, FileSize _size);
  // TODO: Implement GetMapping!

protected:
  BasicFile basic_file;
#if defined(CORE_LIB_OS_WINDOWS)
  FileMapping file_mapping = InvalidFileMapping;
#endif
};

/*******************************************************************************
** MappedFileReader
*******************************************************************************/

class MappedFileReader : public MemoryMappingReader {
public:
  MappedFileReader() = default;
  MappedFileReader(MappedFile _mapped_file,
                   MemoryMappingReader _mapping_reader);
  MappedFileReader(MappedFileReader &&) = default;
  MappedFileReader(MappedFileReader const &) = delete;
  MappedFileReader & operator=(MappedFileReader &&) = default;
  MappedFileReader & operator=(MappedFileReader const &) = delete;
  ~MappedFileReader() = default;

private:
  MappedFile mapped_file;
};

/*******************************************************************************
** mapped_file_reader
*******************************************************************************/

Optional<MappedFileReader> mapped_file_reader(FilePath const & _file_path);

}}

////////////////////////////////////////////////////////////////////////////////
// core fs
////////////////////////////////////////////////////////////////////////////////

namespace core { namespace fs {

/*******************************************************************************
** MemoryMappingReader
*******************************************************************************/

inline MemoryMappingReader::MemoryMappingReader(const char * _memory,
                                                FileSize _size,
                                                FileSize _offset)
    : ByteStreamReader(make_span(reinterpret_cast<byte const *>(_memory),
                                 static_cast<size_t>(_size)))
    , offset(_offset) {}

inline MemoryMappingReader::MemoryMappingReader(MemoryMappingReader && _other)
    : ByteStreamReader(move(_other))
    , offset(_other.offset) {
  _other.offset = 0;
}

inline MemoryMappingReader &
MemoryMappingReader::operator=(MemoryMappingReader && _other) {
  if (this != &_other) {
    static_cast<ByteStreamReader>(*this) = move(_other);
    offset = _other.offset;
    _other.offset = 0;
  }
  return *this;
}

inline MemoryMappingReader::~MemoryMappingReader() {
  if (!storage.empty()) {
    auto memory =
        const_cast<void *>(reinterpret_cast<const void *>(storage.data()));
#if defined(CORE_LIB_OS_WINDOWS)
    if (::UnmapViewOfFile(memory) == 0) {
      LOG_MAPPED_FILE(ERROR, "Error while unmapping!\n");
    }
#elif defined(CORE_LIB_OS_LINUX)
    if (::munmap(memory, storage.size()) == -1) {
      LOG_MAPPED_FILE(ERROR, "Error while unmapping!\n");
    }
#endif
  }
}

/*******************************************************************************
** MappedFile
*******************************************************************************/

inline MappedFile::~MappedFile() {
  if (IsOpen()) {
    Close();
  }
}

inline bool MappedFile::Open(FilePath const & _file_path, OpenMode _open_mode) {
  if (!basic_file.Open(_file_path, _open_mode)) {
    LOG_MAPPED_FILE(ERROR, "Could not open basic file '%s'!\n",
                    cstr(_file_path));
    return false;
  }
  FileSize file_size = file_size_by_descriptor(basic_file.file_descriptor);
  if (InvalidFileSize == file_size) {
    LOG_MAPPED_FILE(ERROR, "File size is invalid for '%s'!\n",
                    cstr(_file_path));
    return false;
  }
  if (OpenMode::ReadOnly == _open_mode) {
#if defined(CORE_LIB_OS_WINDOWS)
    file_mapping = ::CreateFileMapping(basic_file.file_descriptor, NULL,
                                       PAGE_READONLY, 0, 0, NULL);
    if (InvalidFileMapping == file_mapping) {
      LOG_MAPPED_FILE(ERROR, "Could not map basic file '%s'!\n",
                      cstr(_file_path));
      return false;
    }
#endif
  } else if (OpenMode::WriteOnly == _open_mode) {
    CORE_LIB_ASSERT(false);
    return false;
  }
  return true;
}

inline bool MappedFile::IsOpen() const {
#if defined(CORE_LIB_OS_WINDOWS)
  return basic_file.IsOpen() && file_mapping.HasValue();
#elif defined(CORE_LIB_OS_LINUX)
  return basic_file.IsOpen();
#endif
}

inline MemoryMappingReader MappedFile::GetMappingReader(FileSize _offset,
                                                        FileSize _size) {
  // TODO: We should assert that the BasicFile was opened for read!
  if (IsOpen()) {
    void * memory;
#if defined(CORE_LIB_OS_WINDOWS)
    // TODO: Use offset!
    memory = ::MapViewOfFile(file_mapping.Get(), FILE_MAP_READ, 0, 0,
                             static_cast<DWORD>(_size));
    if (!memory) {
      DWORD error = GetLastError();
      LOG_MAPPED_FILE(ERROR, "Failed to map file view of size %d. Error: %d!\n",
                      static_cast<DWORD>(_size), error);
      return MemoryMappingReader();
    }
#elif defined(CORE_LIB_OS_LINUX)
    memory = ::mmap(0, _size, PROT_READ, MAP_SHARED, basic_file.file_descriptor,
                    _offset);
    if (!memory) {
      LOG_MAPPED_FILE(ERROR, "Failed to map memory!\n");
      return MemoryMappingReader();
    }
#endif
    CORE_LIB_ASSERT(memory);
    return MemoryMappingReader(reinterpret_cast<const char *>(memory), _size,
                               _offset);
  }
  return MemoryMappingReader();
}

inline void MappedFile::Close() {
  CORE_LIB_ASSERT(IsOpen());
#if defined(CORE_LIB_OS_WINDOWS)
  if (file_mapping.HasValue()) {
    ::CloseHandle(file_mapping);
  }
#endif
  if (basic_file.IsOpen()) {
    basic_file.Close();
  }
}

/*******************************************************************************
** MappedFileReader
*******************************************************************************/

inline MappedFileReader::MappedFileReader(MappedFile _mapped_file,
                                          MemoryMappingReader _mapping_reader)
    : MemoryMappingReader(move(_mapping_reader))
    , mapped_file(move(_mapped_file)) {}

/*******************************************************************************
** mapped_file_reader
*******************************************************************************/

inline Optional<MappedFileReader>
mapped_file_reader(FilePath const & _file_path) {
  MappedFile file;
  if (!file.Open(_file_path, fs::OpenMode::ReadOnly)) {
    LOG_MAPPED_FILE(ERROR, "Failed to open '%s'!\n", cstr(_file_path));
    return nullopt;
  }
  auto file_size = file.GetSize();
  if (file_size == fs::InvalidFileSize) {
    LOG_MAPPED_FILE(ERROR, "File '%s' has invalid size!\n", cstr(_file_path));
    return nullopt;
  }
  auto mapping_reader = file.GetMappingReader(0u, file_size);
  if (mapping_reader.Empty()) {
    LOG_MAPPED_FILE(ERROR, "Failed to map file '%s' into memory!\n",
                    cstr(_file_path));
    return nullopt;
  }
  return Optional<MappedFileReader>(move(file), move(mapping_reader));
}

}}

#endif