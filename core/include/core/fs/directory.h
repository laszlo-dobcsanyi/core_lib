#ifndef _CORE_LIB_FS_DIRECTORY_H_
#define _CORE_LIB_FS_DIRECTORY_H_

#include "core/optional.hpp"
#include "core/variant.hpp"
#include "core/fs/filepath.h"

////////////////////////////////////////////////////////////////////////////////
// core fs
////////////////////////////////////////////////////////////////////////////////

namespace core { namespace fs {

class DirectoryIterator;

/*******************************************************************************
** Directory
*******************************************************************************/

class Directory final {
public:
  Directory() = default;
  explicit Directory(DirectoryType _directory_type)
      : path(get_directory_type_path(_directory_type)) {}
  explicit Directory(const_string _path)
      : path(move(_path)) {}
  explicit Directory(FilePath _path)
      : Directory(str(_path)) {}
  Directory(Directory &&) = default;
  Directory(Directory const &) = default;
  Directory & operator=(Directory &&) = default;
  Directory & operator=(Directory const &) = default;
  ~Directory() = default;

  bool IsValid() const { return !path.IsEmpty(); }
  explicit operator bool() const { return IsValid(); }

  const_string const & Path() const { return path; }

  FilePath GetPath(const_string const & _file_name) const;

  Optional<FilePath> GetFilePath(const_string const & _full_name) const;

  Optional<Directory>
  GetSubDirectory(const_string const & _directory_name) const;

  Optional<Directory> GetParentDirectory() const;

  bool Exists() const;

  bool Exists(const_string const & _file_name) const;

  DirectoryIterator begin() const;
  DirectoryIterator end() const;

private:
  const_string path;
};

inline FilePath operator/(Directory const & _directory,
                          const_string const & _path) {
  return _directory.GetPath(_path);
}

inline char const * cstr(Directory const & _directory) {
  return cstr(_directory.Path());
}

inline const_string str(Directory const & _directory) {
  return _directory.Path();
}

/*******************************************************************************
** DirectoryIterator
*******************************************************************************/

class DirectoryIterator final {
public:
  DirectoryIterator() = delete;
  explicit DirectoryIterator(nullptr_t);
  explicit DirectoryIterator(Directory const & _directory);
  DirectoryIterator(DirectoryIterator &&) = default;
  DirectoryIterator(DirectoryIterator const &) = delete;
  DirectoryIterator & operator=(DirectoryIterator &&) = default;
  DirectoryIterator & operator=(DirectoryIterator const &) = delete;
  ~DirectoryIterator();

  Variant<Directory, FilePath> operator*() {
    CORE_LIB_ASSERT(current.HasValue());
    return current.Value();
  }

  DirectoryIterator & operator++() {
    CORE_LIB_ASSERT(current.HasValue());
    current = Forward();
    return *this;
  }

  bool operator==(DirectoryIterator const & _other) {
    return !current.HasValue() && !_other.current.HasValue();
  }

  bool operator!=(DirectoryIterator const & _other) {
    return !(*this == _other);
  }

private:
  Directory const * directory = nullptr;
  Optional<Variant<Directory, FilePath>> current;
#if defined(CORE_LIB_OS_WINDOWS)
  Value<HANDLE, INVALID_HANDLE_VALUE> find;
  Value<bool, true> first;
#elif defined(CORE_LIB_OS_LINUX)
  Value<DIR *, nullptr> dir;
#endif

  Optional<Variant<Directory, FilePath>> Forward();
};

}}

////////////////////////////////////////////////////////////////////////////////
// core fs
////////////////////////////////////////////////////////////////////////////////

namespace core { namespace fs {

/*******************************************************************************
** Directory
*******************************************************************************/

inline FilePath Directory::GetPath(const_string const & _file_name) const {
  if (path) {
    string::Stream stream;
    stream << path << PathSeparator() << _file_name;
    return FilePath(stream.Get());
  }
  return FilePath(_file_name);
}

inline Optional<FilePath>
Directory::GetFilePath(const_string const & _full_name) const {
  auto file_path = GetPath(_full_name);
  if (file_exists(str(file_path))) {
    return Optional<FilePath>(str(file_path));
  }
  return nullopt;
}

inline Optional<Directory>
Directory::GetSubDirectory(const_string const & _directory_name) const {
  auto sub_directory_path = GetPath(_directory_name);
  if (directory_exists(str(sub_directory_path))) {
    return Optional<Directory>(str(sub_directory_path));
  }
  return nullopt;
}

inline Optional<Directory> Directory::GetParentDirectory() const {
  const_string parent_path = path_parent(path);
  if (directory_exists(parent_path)) {
    return Optional<Directory>(move(parent_path));
  }
  return nullopt;
}

inline bool Directory::Exists() const { return directory_exists(path); }

inline bool Directory::Exists(const_string const & _file_name) const {
  return file_exists(str(GetPath(_file_name)));
}

inline DirectoryIterator Directory::begin() const {
  return DirectoryIterator(*this);
}

inline DirectoryIterator Directory::end() const {
  return DirectoryIterator(nullptr);
}

/*******************************************************************************
** DirectoryIterator
*******************************************************************************/

inline DirectoryIterator::DirectoryIterator(nullptr_t) {}

inline DirectoryIterator::DirectoryIterator(Directory const & _directory)
    : directory(&_directory) {
#if defined(CORE_LIB_OS_LINUX)
  dir = ::opendir(cstr(_directory));
#endif
  current = Forward();
}

inline DirectoryIterator::~DirectoryIterator() {
#if defined(CORE_LIB_OS_WINDOWS)
  ::FindClose(find);
#elif defined(CORE_LIB_OS_LINUX)
  ::closedir(dir);
#endif
}

#if defined(CORE_LIB_OS_WINDOWS)

namespace {

inline bool IsFileOrDirectory(WIN32_FIND_DATA const & _data) {
  return (_data.dwFileAttributes &
          (FILE_ATTRIBUTE_ARCHIVE | FILE_ATTRIBUTE_NORMAL |
           FILE_ATTRIBUTE_DIRECTORY)) != 0;
}

inline Optional<Variant<Directory, FilePath>>
ProcessFindData(WIN32_FIND_DATA const & _data, Directory const & _directory) {
  using ResultValue = Variant<Directory, FilePath>;
  using OptionalResult = Optional<ResultValue>;

  CORE_LIB_ASSERT(IsFileOrDirectory(_data));
  if ((_data.dwFileAttributes &
       (FILE_ATTRIBUTE_NORMAL | FILE_ATTRIBUTE_ARCHIVE)) != 0) {
    return OptionalResult(in_place_t<FilePath>(),
                          _directory.GetPath(_data.cFileName));
  } else if ((_data.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) != 0) {
    return OptionalResult(in_place_t<Directory>(),
                          _directory.GetPath(_data.cFileName));
  }
  return nullopt;
}

}

#endif

inline Optional<Variant<Directory, FilePath>> DirectoryIterator::Forward() {
  using ResultValue = Variant<Directory, FilePath>;
  using OptionalResult = Optional<ResultValue>;

  CORE_LIB_ASSERT(directory);

#if defined(CORE_LIB_OS_WINDOWS)
  WIN32_FIND_DATA data;
  if (first) {
    first = false;

    string::Stream stream;
    stream << cstr(*directory) << "\\*";
    find = ::FindFirstFileA(cstr(stream.Get()), &data);
    if (find == INVALID_HANDLE_VALUE) {
      return nullopt;
    }

    if (IsFileOrDirectory(data)) {
      return ProcessFindData(data, *directory);
    }
  }
  while (::FindNextFileA(find, &data)) {
    if (IsFileOrDirectory(data)) {
      return ProcessFindData(data, *directory);
    }
  }
#elif defined(CORE_LIB_OS_LINUX)
  struct dirent * entry;
  while ((entry = readdir(dir)) != nullptr) {
    if (entry->d_name[0u] != '.') {
      if (entry->d_type == DT_REG) {
        return OptionalResult(in_place_t<FilePath>(),
                              directory->GetPath(entry->d_name));
      } else if (entry->d_type == DT_DIR) {
        return OptionalResult(in_place_t<Directory>(),
                              directory->GetPath(entry->d_name));
      }
    }
  }
#endif
  return nullopt;
}

}}

#endif
