#ifndef _CORE_LIB_FS_FILE_PATH_H_
#define _CORE_LIB_FS_FILE_PATH_H_

#include "core/fs/base.h"

////////////////////////////////////////////////////////////////////////////////
// core fs
////////////////////////////////////////////////////////////////////////////////

namespace core { namespace fs {

/*******************************************************************************
** FilePath
*******************************************************************************/

class FilePath final {
public:
  FilePath() = default;
  template<typename T>
  explicit inline FilePath(T && _argument)
      : file_path(forward<T>(_argument)) {}
  FilePath(FilePath &&) = default;
  FilePath(FilePath const &) = default;
  FilePath & operator=(FilePath &&) = default;
  FilePath & operator=(FilePath const &) = default;
  ~FilePath() = default;

  bool IsValid() const { return !file_path.IsEmpty(); }
  explicit operator bool() const { return IsValid(); }

  const_string const & Path() const { return file_path; }

  const_string NameWithExtension() const {
    return file_name_with_extension(file_path);
  }

  const_string Name() const { return file_name(file_path); }

  const_string Extension() const { return file_extension(file_path); }

  bool Exists() const { return file_exists(file_path); }

private:
  const_string file_path;
};

inline FilePath operator/(FilePath const & _path,
                          const_string const & _sub_path) {
  string::Stream stream;
  stream << cstr(_path.Path()) << PathSeparator() << cstr(_sub_path);
  return FilePath(stream.Get());
}

inline char const * cstr(FilePath const & _file_path) {
  return string::cstr(_file_path.Path());
}

inline const_string str(FilePath const & _file_path) {
  return _file_path.Path();
}

}}

#endif
