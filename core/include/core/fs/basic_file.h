#ifndef _CORE_LIB_FS_BASIC_FILE_H_
#define _CORE_LIB_FS_BASIC_FILE_H_

#include "core/fs/filepath.h"

#define LOG_BASIC_FILE(__severity__, ...) LOG_FS(__severity__, __VA_ARGS__)

////////////////////////////////////////////////////////////////////////////////
// core fs
////////////////////////////////////////////////////////////////////////////////

namespace core { namespace fs {

/*******************************************************************************
** BasicFile
*******************************************************************************/

class BasicFile {
public:
  FileDescriptor file_descriptor;

  BasicFile() = default;
  BasicFile(BasicFile &&) = default;
  BasicFile(BasicFile const &) = default;
  BasicFile & operator=(BasicFile &&) = default;
  BasicFile & operator=(BasicFile const &) = default;
  ~BasicFile();

  bool IsOpen() const { return file_descriptor.HasValue(); }
  explicit operator bool() const { return IsOpen(); }

  FileSize GetSize() const;

  bool Open(FilePath const & _file_path, OpenMode _mode);
  void Close();
};

}}

////////////////////////////////////////////////////////////////////////////////
// core fs
////////////////////////////////////////////////////////////////////////////////

namespace core { namespace fs {

/*******************************************************************************
** BasicFile
*******************************************************************************/

inline BasicFile::~BasicFile() {
  if (IsOpen()) {
    Close();
  }
}

inline bool BasicFile::Open(FilePath const & _file_path, OpenMode _mode) {
  if (OpenMode::ReadOnly == _mode) {
#if defined(CORE_LIB_OS_WINDOWS)
    file_descriptor =
        ::CreateFile(cstr(_file_path), GENERIC_READ, FILE_SHARE_READ, nullptr,
                     OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, nullptr);
#elif defined(CORE_LIB_OS_LINUX)
    file_descriptor = ::open(cstr(_file_path), O_RDONLY);
#endif
  } else if (OpenMode::WriteOnly == _mode) {
    CORE_LIB_UNIMPLEMENTED;
    return false;
  }
  if (file_descriptor == InvalidFileDescriptor) {
#if defined(CORE_LIB_OS_WINDOWS)
    LOG_BASIC_FILE(ERROR, "Error while opening file '%s'!\n", cstr(_file_path));
#elif defined(CORE_LIB_OS_LINUX)
    LOG_BASIC_FILE(ERROR, "Error while opening file '%s' : '%s' (%i)\n",
                   cstr(_file_path), strerror(errno), errno);
#endif
    return false;
  }
  return true;
}

inline FileSize BasicFile::GetSize() const {
  if (IsOpen()) {
    return file_size_by_descriptor(file_descriptor);
  }
  return InvalidFileSize;
}

inline void BasicFile::Close() {
  CORE_LIB_ASSERT(IsOpen());
#if defined(CORE_LIB_OS_WINDOWS)
  ::CloseHandle(file_descriptor);
#elif defined(CORE_LIB_OS_LINUX)
  ::close(file_descriptor);
#endif
  file_descriptor = InvalidFileDescriptor;
}

}}

#undef LOG_BASIC_FILE

#endif
