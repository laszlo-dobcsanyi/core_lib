#ifndef _CORE_LIB_QUERIES_LISTS_HPP_
#define _CORE_LIB_QUERIES_LISTS_HPP_

#ifndef _CORE_LIB_QUERIES_HPP_
#  error
#endif

////////////////////////////////////////////////////////////////////////////////
// query detail
////////////////////////////////////////////////////////////////////////////////

namespace query { namespace detail {

/*******************************************************************************
** SingleListQueryBase
*******************************************************************************/

template<typename T, class NodeType, class ListType>
class SingleListQueryBase {
public:
  SingleListQueryBase() = default;
  SingleListQueryBase(ListType & _list, NodeType * _node);
  SingleListQueryBase(SingleListQueryBase &&) = default;
  SingleListQueryBase(SingleListQueryBase const &) = default;
  SingleListQueryBase & operator=(SingleListQueryBase &&) = default;
  SingleListQueryBase & operator=(SingleListQueryBase const &) = default;
  ~SingleListQueryBase() = default;

  bool IsValid() const;
  explicit operator bool() const;
  T & Current();
  T & Forward();
  T * Next();
  void ToHead();

protected:
  ListType * list = nullptr;
  NodeType * current = nullptr;
};

/*******************************************************************************
** DoubleListQueryBase
*******************************************************************************/

template<typename T, class NodeType, class ListType>
class DoubleListQueryBase : public SingleListQueryBase<T, NodeType, ListType> {
public:
  using SingleListQueryBase<T, NodeType, ListType>::SingleListQueryBase;

  T const & Backward();
  T const * Previous();
  void ToTail();
};

}}

////////////////////////////////////////////////////////////////////////////////
// query
////////////////////////////////////////////////////////////////////////////////

namespace query {

/*******************************************************************************
** Query< SingleList >
*******************************************************************************/

template<typename T, template<typename> class TypeAllocator>
class Query<container::SingleList<T, TypeAllocator>> final
    : public detail::SingleListQueryBase<
          T, container::detail::SingleList_Node<T>,
          container::SingleList<T, TypeAllocator>> {
public:
  using detail::SingleListQueryBase<
      T, container::detail::SingleList_Node<T>,
      container::SingleList<T, TypeAllocator>>::SingleListQueryBase;
  explicit Query(container::SingleList<T, TypeAllocator> & _single_list);
};

/*******************************************************************************
** ConstQuery< SingleList >
*******************************************************************************/

template<typename T, template<typename> class TypeAllocator>
class ConstQuery<container::SingleList<T, TypeAllocator>> final
    : public detail::SingleListQueryBase<
          const T, const container::detail::SingleList_Node<T>,
          const container::SingleList<T, TypeAllocator>> {
public:
  using detail::SingleListQueryBase<
      const T, const container::detail::SingleList_Node<T>,
      const container::SingleList<T, TypeAllocator>>::SingleListQueryBase;
  explicit ConstQuery(
      container::SingleList<T, TypeAllocator> const & _single_list);
};

/*******************************************************************************
** Query< DoubleList >
*******************************************************************************/

template<typename T, template<typename> class TypeAllocator>
class Query<container::DoubleList<T, TypeAllocator>> final
    : public detail::DoubleListQueryBase<
          T, container::detail::DoubleList_Node<T>,
          container::DoubleList<T, TypeAllocator>> {
public:
  using detail::DoubleListQueryBase<
      T, container::detail::DoubleList_Node<T>,
      container::DoubleList<T, TypeAllocator>>::DoubleListQueryBase;
  explicit Query(container::DoubleList<T, TypeAllocator> & _double_list);

  template<typename... Args>
  T & LinkAsNext(Args &&... _args);
  template<typename... Args>
  T & LinkAsPrevious(Args &&... _args);
  void UnlinkForward();
  void UnlinkBackward();
  void UnlinkNext();
  void UnlinkPrevious();
};

/*******************************************************************************
** ConstQuery< DoubleList >
*******************************************************************************/

template<typename T, template<typename> class TypeAllocator>
class ConstQuery<container::DoubleList<T, TypeAllocator>> final
    : public detail::DoubleListQueryBase<
          const T, const container::detail::DoubleList_Node<T>,
          const container::DoubleList<T, TypeAllocator>> {
public:
  using detail::DoubleListQueryBase<
      const T, const container::detail::DoubleList_Node<T>,
      const container::DoubleList<T, TypeAllocator>>::DoubleListQueryBase;
  explicit ConstQuery(
      container::DoubleList<T, TypeAllocator> const & _double_list);
};

}

////////////////////////////////////////////////////////////////////////////////
// query detail
////////////////////////////////////////////////////////////////////////////////

namespace query { namespace detail {

/*******************************************************************************
** SingleListQueryBase
*******************************************************************************/

template<typename T, class NodeType, class ListType>
inline SingleListQueryBase<T, NodeType, ListType>::SingleListQueryBase(
    ListType & _list, NodeType * _node)
    : list(&_list)
    , current(_node) {}

template<typename T, class NodeType, class ListType>
inline bool SingleListQueryBase<T, NodeType, ListType>::IsValid() const {
  return list != nullptr && current != nullptr;
}

template<typename T, class NodeType, class ListType>
inline SingleListQueryBase<T, NodeType, ListType>::operator bool() const {
  return IsValid();
}

template<typename T, class NodeType, class ListType>
inline T & SingleListQueryBase<T, NodeType, ListType>::Current() {
  CORE_LIB_ASSERT(IsValid());
  return current->data;
}

template<typename T, class NodeType, class ListType>
inline T & SingleListQueryBase<T, NodeType, ListType>::Forward() {
  CORE_LIB_ASSERT(IsValid());
  T & result = current->data;
  current = current->next;
  return result;
}

template<typename T, class NodeType, class ListType>
inline T * SingleListQueryBase<T, NodeType, ListType>::Next() {
  CORE_LIB_ASSERT(IsValid());
  if (current->next) {
    return current->next->data;
  }
  return nullptr;
}

template<typename T, class NodeType, class ListType>
inline void SingleListQueryBase<T, NodeType, ListType>::ToHead() {
  CORE_LIB_ASSERT(IsValid());
  current = list->head;
}

/*******************************************************************************
** DoubleListQueryBase
*******************************************************************************/

template<typename T, class NodeType, class ListType>
inline T const & DoubleListQueryBase<T, NodeType, ListType>::Backward() {
  CORE_LIB_ASSERT(this->IsValid());
  T & value = this->current->data;
  this->current = this->current->prev;
  return value;
}

template<typename T, class NodeType, class ListType>
inline T const * DoubleListQueryBase<T, NodeType, ListType>::Previous() {
  CORE_LIB_ASSERT(this->IsValid());
  if (this->current->prev) {
    return this->current->prev->data;
  }
  return nullptr;
}

template<typename T, class NodeType, class ListType>
inline void DoubleListQueryBase<T, NodeType, ListType>::ToTail() {
  CORE_LIB_ASSERT(this->IsValid());
  this->current = this->list->tail;
}

}}

////////////////////////////////////////////////////////////////////////////////
// query
////////////////////////////////////////////////////////////////////////////////

namespace query {

/*******************************************************************************
** Query< SingleList >
*******************************************************************************/

template<typename T, template<typename> class TypeAllocator>
inline Query<container::SingleList<T, TypeAllocator>>::Query(
    container::SingleList<T, TypeAllocator> & _single_list)
    : detail::SingleListQueryBase<T, container::detail::SingleList_Node<T>,
                                  container::SingleList<T, TypeAllocator>>(
          _single_list, _single_list.head) {}

/*******************************************************************************
** ConstQuery< SingleList >
*******************************************************************************/

template<typename T, template<typename> class TypeAllocator>
inline ConstQuery<container::SingleList<T, TypeAllocator>>::ConstQuery(
    container::SingleList<T, TypeAllocator> const & _single_list)
    : detail::SingleListQueryBase<
          const T, const container::detail::SingleList_Node<T>,
          const container::SingleList<T, TypeAllocator>>(_single_list,
                                                         _single_list.head) {}

/*******************************************************************************
** Query< DoubleList >
*******************************************************************************/

template<typename T, template<typename> class TypeAllocator>
inline Query<container::DoubleList<T, TypeAllocator>>::Query(
    container::DoubleList<T, TypeAllocator> & _double_list)
    : detail::DoubleListQueryBase<T, container::detail::DoubleList_Node<T>,
                                  container::DoubleList<T, TypeAllocator>>(
          _double_list, _double_list.head) {}

template<typename T, template<typename> class TypeAllocator>
template<typename... Args>
inline T &
Query<container::DoubleList<T, TypeAllocator>>::LinkAsNext(Args &&... _args) {
  CORE_LIB_ASSERT(this->IsValid());
  auto & node =
      this->list->LinkNodeToPrevious(*this->current, forward<Args>(_args)...);
  return node.data;
}

template<typename T, template<typename> class TypeAllocator>
template<typename... Args>
inline T & Query<container::DoubleList<T, TypeAllocator>>::LinkAsPrevious(
    Args &&... _args) {
  CORE_LIB_ASSERT(this->current);
  auto & node =
      this->list->LinkNodeToNext(*this->current, forward<Args>(_args)...);
  return node.data;
}

template<typename T, template<typename> class TypeAllocator>
inline void Query<container::DoubleList<T, TypeAllocator>>::UnlinkForward() {
  CORE_LIB_ASSERT(this->IsValid());
  auto next = this->current->next;
  this->list->UnlinkNode(*this->current);
  this->current = next;
}

template<typename T, template<typename> class TypeAllocator>
inline void Query<container::DoubleList<T, TypeAllocator>>::UnlinkBackward() {
  CORE_LIB_ASSERT(this->IsValid());
  auto previous = this->current->prev;
  this->list->UnlinkNode(*this->current);
  this->current = previous;
}

template<typename T, template<typename> class TypeAllocator>
inline void Query<container::DoubleList<T, TypeAllocator>>::UnlinkNext() {
  CORE_LIB_ASSERT(this->IsValid() && this->current->next);
  this->list->UnlinkNode(*this->current->next);
}

template<typename T, template<typename> class TypeAllocator>
inline void Query<container::DoubleList<T, TypeAllocator>>::UnlinkPrevious() {
  CORE_LIB_ASSERT(this->IsValid() && this->current->prev);
  this->list->UnlinkNode(*this->current->prev);
}

/*******************************************************************************
** ConstQuery< DoubleList >
*******************************************************************************/

template<typename T, template<typename> class TypeAllocator>
inline ConstQuery<container::DoubleList<T, TypeAllocator>>::ConstQuery(
    container::DoubleList<T, TypeAllocator> const & _double_list)
    : detail::DoubleListQueryBase<
          const T, const container::detail::DoubleList_Node<T>,
          const container::DoubleList<T, TypeAllocator>>(_double_list,
                                                         _double_list.head) {}

}

////////////////////////////////////////////////////////////////////////////////
// container
////////////////////////////////////////////////////////////////////////////////

namespace container {

/*******************************************************************************
** make_query
*******************************************************************************/

template<typename T, template<typename> class TypeAllocator>
inline query::Query<SingleList<T, TypeAllocator>>
make_query(SingleList<T, TypeAllocator> & _list) {
  return query::Query<SingleList<T, TypeAllocator>>(_list);
}

template<typename T, template<typename> class TypeAllocator>
inline query::ConstQuery<SingleList<T, TypeAllocator>>
make_query(SingleList<T, TypeAllocator> const & _list) {
  return query::ConstQuery<SingleList<T, TypeAllocator>>(_list);
}

template<typename T, template<typename> class TypeAllocator>
inline query::Query<DoubleList<T, TypeAllocator>>
make_query(DoubleList<T, TypeAllocator> & _list) {
  return query::Query<DoubleList<T, TypeAllocator>>(_list);
}

template<typename T, template<typename> class TypeAllocator>
inline query::ConstQuery<DoubleList<T, TypeAllocator>>
make_query(DoubleList<T, TypeAllocator> const & _list) {
  return query::ConstQuery<DoubleList<T, TypeAllocator>>(_list);
}

}

#endif
