#ifndef _CORE_LIB_QUERIES_DETAIL_HPP_
#define _CORE_LIB_QUERIES_DETAIL_HPP_

#ifndef _CORE_LIB_QUERIES_HPP_
#  error
#endif

////////////////////////////////////////////////////////////////////////////////
// query detail
////////////////////////////////////////////////////////////////////////////////

namespace query { namespace detail {

template<typename ContainerType>
struct query_of {
  using type = Query<ContainerType>;
};

template<typename ContainerType>
struct query_of<const ContainerType> {
  using type = ConstQuery<ContainerType>;
};

template<typename ContainerType>
using query_of_t = typename query_of<ContainerType>::type;

}}

////////////////////////////////////////////////////////////////////////////////
// query
////////////////////////////////////////////////////////////////////////////////

namespace query {

template<typename ContainerType>
detail::query_of_t<ContainerType> make_query(ContainerType & _container) {
  return Query<ContainerType>(_container);
}

template<typename ContainerType>
detail::query_of_t<const ContainerType>
make_query(ContainerType const & _container) {
  return ConstQuery<ContainerType>(_container);
}

}

#endif
