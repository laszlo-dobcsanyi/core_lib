#ifndef _CORE_LIB_QUERIES_CHAIN_HPP_
#define _CORE_LIB_QUERIES_CHAIN_HPP_

#ifndef _CORE_LIB_QUERIES_HPP_
#  error
#endif

// TODO: Add support for const chains!

////////////////////////////////////////////////////////////////////////////////
// query
////////////////////////////////////////////////////////////////////////////////

namespace query {

/*******************************************************************************
** Chain_Query
*******************************************************************************/

template<class DerivedType, template<class> class ChainElementType>
class Chain_Query final {
public:
  using ChainType = ::container::Chain<DerivedType, ChainElementType>;

  static Chain_Query FromFirst(ChainType & _chain) {
    return Chain_Query(_chain, _chain.first);
  }
  static Chain_Query FromLast(ChainType & _chain) {
    return Chain_Query(_chain, _chain.last);
  }
  static Chain_Query FromElement(ChainType & _chain, DerivedType & _element) {
    return Chain_Query(_chain, &_element);
  }

  Chain_Query(Chain_Query && _other)
      : element(_other.element) {
    _other.element = nullptr;
  }
  Chain_Query(Chain_Query const &) = delete;
  Chain_Query & operator=(Chain_Query && _other) {
    if (this != &_other) {
      element = _other.element;
      _other.element = nullptr;
    }
    return *this;
  }
  Chain_Query & operator=(Chain_Query const &) = delete;
  ~Chain_Query() = default;

  DerivedType * Forward() {
    if (element) {
      DerivedType * result = element;
      element = element->ChainElementType<DerivedType>::next;
      return result;
    }
    return nullptr;
  }

  template<typename LinkageType>
  typename container::if_head_t<LinkageType, DerivedType *> CircularForward() {
    if (element) {
      DerivedType * result = element;
      if (auto previous = element->ChainElementType<DerivedType>::prev) {
        element = previous;
      } else {
        element = chain->last;
      }
      return result;
    }
    return nullptr;
  }

  template<typename LinkageType>
  typename container::if_tail_t<LinkageType, DerivedType *> CircularForward() {
    if (element) {
      DerivedType * result = element;
      if (auto next = element->ChainElementType<DerivedType>::next) {
        element = next;
      } else {
        element = chain->first;
      }
      return result;
    }
    return nullptr;
  }

  DerivedType * Backward() {
    if (element) {
      DerivedType * result = element;
      element = element->ChainElementType<DerivedType>::prev;
      return result;
    }
    return nullptr;
  }

  DerivedType * UnlinkForward() {
    if (element) {
      DerivedType * result = element;
      element = element->ChainElementType<DerivedType>::next;
      chain->template Unlink<container::Tail>(*result);
      return result;
    }
    return nullptr;
  }

  DerivedType * UnlinkBackward() {
    if (element) {
      DerivedType * result = element;
      element = element->ChainElementType<DerivedType>::prev;
      chain->template Unlink<container::Head>(*result);
      return result;
    }
    return nullptr;
  }

private:
  ChainType * chain;
  DerivedType * element;

  Chain_Query(ChainType & _chain, DerivedType * _element)
      : chain(&_chain)
      , element(_element) {}
};

}

////////////////////////////////////////////////////////////////////////////////
// query
////////////////////////////////////////////////////////////////////////////////

namespace query {
}

#endif
