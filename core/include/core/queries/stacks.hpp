#ifndef _CORE_LIB_QUERIES_STACKS_HPP_
#define _CORE_LIB_QUERIES_STACKS_HPP_

#ifndef _CORE_LIB_QUERIES_HPP_
#  error
#endif

////////////////////////////////////////////////////////////////////////////////
// query
////////////////////////////////////////////////////////////////////////////////

namespace query {

/*******************************************************************************
** Query< BoundedStack >
*******************************************************************************/

template<typename T, size_t Bound>
class Query<container::BoundedStack<T, Bound>> final {
public:
  using StackType = container::BoundedStack<T, Bound>;

  Query() = default;
  explicit Query(StackType & _stack);
  Query(Query &&) = default;
  Query(Query const &) = default;
  Query & operator=(Query &&) = default;
  Query & operator=(Query const &) = default;
  ~Query() = default;

  bool IsValid() const;
  explicit operator bool() const;
  T & Current();
  T & Forward();
  T & Backward();
  void ToHead();
  void ToTail();

private:
  StackType * stack = nullptr;
  size_t current;
};

/*******************************************************************************
** ConstQuery< BoundedStack >
*******************************************************************************/

template<typename T, size_t Bound>
class ConstQuery<container::BoundedStack<T, Bound>> final {
public:
  using StackType = container::BoundedStack<T, Bound>;

  ConstQuery() = default;
  explicit ConstQuery(StackType const & _stack);
  ConstQuery(ConstQuery &&) = default;
  ConstQuery(ConstQuery const &) = delete;
  ConstQuery & operator=(ConstQuery &&) = default;
  ConstQuery & operator=(ConstQuery const &) = delete;
  ~ConstQuery() = default;

  bool IsValid() const;
  explicit operator bool() const;
  T const & Current();
  T const & Forward();
  T const & Backward();
  void ToHead();
  void ToTail();

private:
  StackType const * stack = nullptr;
  size_t current;
};

/*******************************************************************************
** Query< UnboundedStack >
*******************************************************************************/

template<typename T, template<typename> class TypeAllocator>
class Query<container::UnboundedStack<T, TypeAllocator>> final {
public:
  using UnboundedStackType = container::UnboundedStack<T, TypeAllocator>;

  Query() = default;
  explicit Query(UnboundedStackType & _unbounded_stack);
  Query(Query &&) = default;
  Query(Query const &) = delete;
  Query & operator=(Query &&) = default;
  Query & operator=(Query const &) = delete;
  ~Query() = default;

  bool IsValid() const;
  explicit operator bool() const;
  T & Current();
  T & Forward();
  T & Backward();
  void ToHead();
  void ToTail();

private:
  using BoundedStackListType = typename UnboundedStackType::StackListType;
  using BoundedStackType = typename UnboundedStackType::StackType;

  UnboundedStackType * unbounded_stack = nullptr;
  Query<BoundedStackListType> bounded_stack_list_query;
  BoundedStackType * bounded_stack = nullptr;
  Query<BoundedStackType> bounded_stack_query;
};

/*******************************************************************************
** ConstQuery< UnboundedStack >
*******************************************************************************/

template<typename T, template<typename> class TypeAllocator>
class ConstQuery<container::UnboundedStack<T, TypeAllocator>> final {
public:
  using UnboundedStackType = container::UnboundedStack<T, TypeAllocator>;

  ConstQuery() = default;
  explicit ConstQuery(UnboundedStackType & _unbounded_stack);
  ConstQuery(ConstQuery &&) = default;
  ConstQuery(ConstQuery const &) = delete;
  ConstQuery & operator=(ConstQuery &&) = default;
  ConstQuery & operator=(ConstQuery const &) = delete;
  ~ConstQuery() = default;

  bool IsValid() const;
  explicit operator bool() const;
  T const & Current();
  T const & Forward();
  T const & Backward();
  void ToHead();
  void ToTail();

private:
  using BoundedStackListType = typename UnboundedStackType::StackListType;
  using BoundedStackType = typename UnboundedStackType::StackType;

  UnboundedStackType * unbounded_stack = nullptr;
  ConstQuery<BoundedStackListType> bounded_stack_list_query;
  ConstQuery<BoundedStackType> bounded_stack_query;
};

}

////////////////////////////////////////////////////////////////////////////////
// query
////////////////////////////////////////////////////////////////////////////////

namespace query {

/*******************************************************************************
** Query< BoundedStack >
*******************************************************************************/

template<typename T, size_t Bound>
inline Query<container::BoundedStack<T, Bound>>::Query(
    container::BoundedStack<T, Bound> & _stack)
    : stack(&_stack)
    , current(0u) {}

template<typename T, size_t Bound>
inline bool Query<container::BoundedStack<T, Bound>>::IsValid() const {
  return stack && stack->Size() > current;
}

template<typename T, size_t Bound>
inline Query<container::BoundedStack<T, Bound>>::operator bool() const {
  return IsValid();
}

template<typename T, size_t Bound>
inline T & Query<container::BoundedStack<T, Bound>>::Current() {
  CORE_LIB_ASSERT(IsValid());
  return stack->data[current];
}

template<typename T, size_t Bound>
inline T & Query<container::BoundedStack<T, Bound>>::Forward() {
  CORE_LIB_ASSERT(IsValid());
  return stack->data[current++];
}

template<typename T, size_t Bound>
inline T & Query<container::BoundedStack<T, Bound>>::Backward() {
  CORE_LIB_ASSERT(IsValid());
  return stack->data[current--];
}

template<typename T, size_t Bound>
inline void Query<container::BoundedStack<T, Bound>>::ToHead() {
  CORE_LIB_ASSERT(IsValid());
  current = 0u;
}

template<typename T, size_t Bound>
inline void Query<container::BoundedStack<T, Bound>>::ToTail() {
  CORE_LIB_ASSERT(IsValid());
  current = stack->Size() - 1u;
}

/*******************************************************************************
** ConstQuery< BoundedStack >
*******************************************************************************/

template<typename T, size_t Bound>
inline ConstQuery<container::BoundedStack<T, Bound>>::ConstQuery(
    container::BoundedStack<T, Bound> const & _stack)
    : stack(&_stack)
    , current(0u) {}

template<typename T, size_t Bound>
inline bool ConstQuery<container::BoundedStack<T, Bound>>::IsValid() const {
  return stack && stack->Size() > current;
}

template<typename T, size_t Bound>
inline ConstQuery<container::BoundedStack<T, Bound>>::operator bool() const {
  return IsValid();
}

template<typename T, size_t Bound>
inline T const & ConstQuery<container::BoundedStack<T, Bound>>::Current() {
  CORE_LIB_ASSERT(IsValid());
  return stack->data[current];
}

template<typename T, size_t Bound>
inline T const & ConstQuery<container::BoundedStack<T, Bound>>::Forward() {
  CORE_LIB_ASSERT(IsValid());
  return stack->data[current++];
}

template<typename T, size_t Bound>
inline T const & ConstQuery<container::BoundedStack<T, Bound>>::Backward() {
  CORE_LIB_ASSERT(IsValid());
  return stack->data[current--];
}

template<typename T, size_t Bound>
inline void ConstQuery<container::BoundedStack<T, Bound>>::ToHead() {
  CORE_LIB_ASSERT(IsValid());
  current = 0u;
}

template<typename T, size_t Bound>
inline void ConstQuery<container::BoundedStack<T, Bound>>::ToTail() {
  CORE_LIB_ASSERT(IsValid());
  current = stack->Size() - 1u;
}

/*******************************************************************************
** Query< UnboundedStack >
*******************************************************************************/

template<typename T, template<typename> class TypeAllocator>
inline Query<container::UnboundedStack<T, TypeAllocator>>::Query(
    container::UnboundedStack<T, TypeAllocator> & _unbounded_stack)
    : unbounded_stack(&_unbounded_stack)
    , bounded_stack_list_query(_unbounded_stack.stack_list)
    , bounded_stack(_unbounded_stack.stack_list.IsEmpty()
                        ? nullptr
                        : &_unbounded_stack.stack_list.Head()) {}

template<typename T, template<typename> class TypeAllocator>
inline bool
Query<container::UnboundedStack<T, TypeAllocator>>::IsValid() const {
  return unbounded_stack != nullptr &&
         (bounded_stack_list_query.IsValid() ||
          (bounded_stack && bounded_stack_query.IsValid()));
}

template<typename T, template<typename> class TypeAllocator>
inline Query<container::UnboundedStack<T, TypeAllocator>>::operator bool()
    const {
  return IsValid();
}

template<typename T, template<typename> class TypeAllocator>
inline T & Query<container::UnboundedStack<T, TypeAllocator>>::Current() {
  CORE_LIB_ASSERT(IsValid());
  if (!bounded_stack_query.IsValid()) {
    bounded_stack = &bounded_stack_list_query.Forward();
    bounded_stack_query = Query<BoundedStackType>(*bounded_stack);
  }
  return bounded_stack_query.Current();
}

template<typename T, template<typename> class TypeAllocator>
inline T & Query<container::UnboundedStack<T, TypeAllocator>>::Forward() {
  CORE_LIB_ASSERT(IsValid());
  if (!bounded_stack_query.IsValid()) {
    bounded_stack = &bounded_stack_list_query.Forward();
    bounded_stack_query = Query<BoundedStackType>(*bounded_stack);
  }
  return bounded_stack_query.Forward();
}

template<typename T, template<typename> class TypeAllocator>
inline T & Query<container::UnboundedStack<T, TypeAllocator>>::Backward() {
  CORE_LIB_ASSERT(IsValid());
  if (!bounded_stack_query.IsValid()) {
    bounded_stack = &bounded_stack_list_query.Backward();
    bounded_stack_query = Query<BoundedStackType>(*bounded_stack);
  }
  return bounded_stack_query.Backward();
}

template<typename T, template<typename> class TypeAllocator>
inline void Query<container::UnboundedStack<T, TypeAllocator>>::ToHead() {
  if (unbounded_stack) {
    bounded_stack_list_query.ToHead();
    if (bounded_stack_list_query.IsValid()) {
      bounded_stack = &bounded_stack_list_query.Forward();
      bounded_stack_query = Query<BoundedStackType>(*bounded_stack);
    }
  }
}

template<typename T, template<typename> class TypeAllocator>
inline void Query<container::UnboundedStack<T, TypeAllocator>>::ToTail() {
  if (unbounded_stack) {
    bounded_stack_list_query.ToTail();
    if (bounded_stack_list_query.IsValid()) {
      bounded_stack = &bounded_stack_list_query.Backward();
      bounded_stack_query = Query<BoundedStackType>(*bounded_stack);
      bounded_stack_query.ToTail();
    }
  }
}

/*******************************************************************************
** ConstQuery< UnboundedStack >
*******************************************************************************/

// TODO: Implement these!

#if 0

template< typename T, template< typename > class TypeAllocator >
inline ConstQuery< container::UnboundedStack< T, TypeAllocator > >::ConstQuery
( container::UnboundedStack< T, TypeAllocator > const & _unbounded )
    : unbounded_stack( &_unbounded_stack ) {}

template< typename T, template< typename > class TypeAllocator >
inline bool ConstQuery< container::UnboundedStack< T, TypeAllocator > >::IsValid() const {
}

template< typename T, template< typename > class TypeAllocator >
inline ConstQuery< container::UnboundedStack< T, TypeAllocator > >::operator bool() const {
  return IsValid();
}

template< typename T, template< typename > class TypeAllocator >
inline T const & ConstQuery< container::UnboundedStack< T, TypeAllocator > >::Current() {
  CORE_LIB_ASSERT( IsValid() );
}

template< typename T, template< typename > class TypeAllocator >
inline T const & ConstQuery< container::UnboundedStack< T, TypeAllocator > >::Forward() {
  CORE_LIB_ASSERT( IsValid() );
}

template< typename T, template< typename > class TypeAllocator >
inline T const & ConstQuery< container::UnboundedStack< T, TypeAllocator > >::Backward() {
  CORE_LIB_ASSERT( IsValid() );
}

template< typename T, template< typename > class TypeAllocator >
inline void ConstQuery< container::UnboundedStack< T, TypeAllocator > >::ToHead() {
}

template< typename T, template< typename > class TypeAllocator >
inline void ConstQuery< container::UnboundedStack< T, TypeAllocator > >::ToTail() {
}

#endif

}

////////////////////////////////////////////////////////////////////////////////
// container
////////////////////////////////////////////////////////////////////////////////

namespace container {

template<typename T, size_t Bound>
inline query::Query<BoundedStack<T, Bound>>
make_query(BoundedStack<T, Bound> & _stack) {
  return query::Query<BoundedStack<T, Bound>>(_stack);
}

template<typename T, size_t Bound>
inline query::ConstQuery<BoundedStack<T, Bound>>
make_query(BoundedStack<T, Bound> const & _stack) {
  return query::ConstQuery<BoundedStack<T, Bound>>(_stack);
}

template<typename T, template<typename> class TypeAllocator>
inline query::Query<UnboundedStack<T, TypeAllocator>>
make_query(UnboundedStack<T, TypeAllocator> & _stack) {
  return query::Query<UnboundedStack<T, TypeAllocator>>(_stack);
}

template<typename T, template<typename> class TypeAllocator>
inline query::ConstQuery<UnboundedStack<T, TypeAllocator>>
make_query(UnboundedStack<T, TypeAllocator> const & _stack) {
  return query::ConstQuery<UnboundedStack<T, TypeAllocator>>(_stack);
}

}

#endif
