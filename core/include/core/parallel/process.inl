#ifndef _CORE_LIB_PARALLEL_PROCESS_INL_
#define _CORE_LIB_PARALLEL_PROCESS_INL_

#include "core/queries.hpp"

#define LOG_PARALLEL(__severity__, ...) CORE_LIB_LOG(__severity__, __VA_ARGS__)

////////////////////////////////////////////////////////////////////////////////
// l implementation
////////////////////////////////////////////////////////////////////////////////

namespace parallel { namespace detail {

/*******************************************************************************
** CallAllocation
*******************************************************************************/

template<typename State>
inline void CallAllocation<State>::operator()(State & _state) {
  using VirtualCallableType =
      core::VirtualCallable<void, mpl::TypePack<State &>>;
  auto & virtual_callable =
      *reinterpret_cast<VirtualCallableType *>(&this->storage);
  virtual_callable(_state);
}

/*******************************************************************************
** CallAllocationRef
*******************************************************************************/

template<typename State>
inline CallAllocationRef<State>::CallAllocationRef()
    : call_allocation(nullptr) {}

template<typename State>
inline CallAllocationRef<State>::CallAllocationRef(
    CallAllocation<State> & _call_allocation)
    : call_allocation(&_call_allocation) {}

template<typename State>
inline CallAllocationRef<State>::CallAllocationRef(
    CallAllocationRef && _other) {
  call_allocation = _other.call_allocation;
  _other.call_allocation = nullptr;
}

template<typename State>
inline CallAllocationRef<State> &
CallAllocationRef<State>::operator=(CallAllocationRef && _other) {
  if (this != &_other) {
    call_allocation = _other.call_allocation;
    _other.call_allocation = nullptr;
  }
  return *this;
}

template<typename State>
inline CallAllocationRef<State>::~CallAllocationRef() {
  if (call_allocation) {
    auto & allocation = *call_allocation;
    auto & allocator =
        *reinterpret_cast<CallAllocator<State> *>(allocation.allocator);
    allocator.Destroy(allocation);
  }
}

template<typename State>
inline CallAllocationRef<State>::operator bool() const {
  return (call_allocation != nullptr);
}

template<typename State>
inline void CallAllocationRef<State>::operator()(State & _state) {
  CORE_LIB_ASSERT(call_allocation);
  (*call_allocation)(_state);
}

/*******************************************************************************
** CallAllocator
*******************************************************************************/

template<typename State>
template<typename CallableType, typename... Args>
inline CallAllocation<State> * CallAllocator<State>::Create(Args &&... _args) {
  using VirtualCallableType =
      core::VirtualCallable<void, mpl::TypePack<State &>>;
  CORE_LIB_STATIC_ASSERT(
      mpl::is_base_of<VirtualCallableType, CallableType>::value);
  using CallAllocationType = CallAllocationBase<CallableType>;
  auto allocation = allocator.Allocate(sizeof(CallAllocationType));
  if (allocation) {
    LOG_PARALLEL(VERBOSE, "Allocated @ %p!\n",
                 allocation.template As<void *>());
    CORE_LIB_ASSERT(allocation.Size() == sizeof(CallAllocationType));
    size_t call_allocation_size = allocation.Size();
    auto call_allocation =
        allocation.template ReleaseAs<CallAllocation<State> *>();
    call_allocation->size = call_allocation_size;
    call_allocation->allocator = reinterpret_cast<void *>(this);
    auto stored_call =
        reinterpret_cast<CallableType *>(&call_allocation->storage);
    new (stored_call) CallableType(forward<Args>(_args)...);
    return call_allocation;
  } else {
    LOG_PARALLEL(WARNING, "Failed to allocate memory!\n");
    return nullptr;
  }
}

template<typename State>
inline void
CallAllocator<State>::Destroy(CallAllocation<State> & _call_allocation) {
  CORE_LIB_ASSERT(_call_allocation.allocator == reinterpret_cast<void *>(this));
  using VirtualCallableType =
      core::VirtualCallable<void, mpl::TypePack<State &>>;
  auto & callable =
      *reinterpret_cast<VirtualCallableType *>(&_call_allocation.storage);
  callable.~VirtualCallableType();
  LOG_PARALLEL(VERBOSE, "Destroying allocation @ %p!\n",
               reinterpret_cast<void const *>(&_call_allocation));
  allocator.Deallocate(allocator::Allocation(
      reinterpret_cast<uintptr_t>(&_call_allocation), _call_allocation.size));
}

/*******************************************************************************
** CallQueue
*******************************************************************************/

template<typename State>
inline void CallQueue<State>::Push(CallAllocation<State> & _call_allocation) {
  call_queue.Push(_call_allocation);
  LOG_PARALLEL(VERBOSE, "Pushed call @ %p!\n",
               reinterpret_cast<void *>(&_call_allocation));
}

template<typename State>
inline CallAllocation<State> * CallQueue<State>::Pop() {
  auto call_allocation = call_queue.Pop();
  if (call_allocation) {
    LOG_PARALLEL(VERBOSE, "Allocation @ %p!\n",
                 reinterpret_cast<void *>(call_allocation));
  } else {
    // LOG_PARALLEL( VERBOSE, "Empty!\n" );
  }
  return call_allocation;
}

/*******************************************************************************
** TaskAllocator
*******************************************************************************/

template<typename State>
template<typename Callback, typename... Args>
inline Task<State, Callback> * TaskAllocator<State>::Create(Args &&... _args) {
  auto allocation = allocator.Allocate(sizeof(Task<State, Callback>));
  if (allocation) {
    LOG_PARALLEL(VERBOSE, "Allocated @ %p!\n",
                 allocation.template As<void *>());
    auto task = allocation.template ReleaseAs<Task<State, Callback> *>();
    new (task) Task<State, Callback>(forward<Args>(_args)...);
    return task;
  } else {
    LOG_PARALLEL(WARNING, "Failed to allocate memory!\n");
    return nullptr;
  }
}

template<typename State>
template<typename Callback>
void TaskAllocator<State>::Destroy(Task<State, Callback> const & _task) {
  auto task = reinterpret_cast<uintptr_t>(&_task);
  auto task_size = sizeof(_task);
  allocator.Deallocate(allocator::Allocation(task, task_size));
  LOG_PARALLEL(VERBOSE, "Destroyed allocation @ %p!\n",
               reinterpret_cast<void const *>(task));
}

/*******************************************************************************
** TaskCall
*******************************************************************************/

template<typename F, typename State, typename Callback>
template<typename... Args>
inline TaskCall<F, State, Callback>::TaskCall(Task<State, Callback> & _task,
                                              Args &&... _args)
    : closure(forward<Args>(_args)...)
    , task(&_task) {}

template<typename F, typename State, typename Callback>
inline void TaskCall<F, State, Callback>::operator()(State & _state) {
  closure(_state);
  auto task_finalizer = TaskFinalizer<State, Callback>();
  task_finalizer(_state, task);
}

/*******************************************************************************
** CallContext
*******************************************************************************/

template<typename State>
template<typename F, typename... Args>
inline bool CallContext<State>::Post(core::CallableWrapper<F> _function,
                                     Args &&... _args) {
  return this->template PostCallable<core::CallableClosure<F, State &>>(
      move(_function), forward<Args>(_args)...);
}

template<typename State>
template<typename F, typename... Args>
inline bool CallContext<State>::PostCallable(Args &&... _args) {
  using CallableType = F;
  auto call_allocation =
      call_allocator.template Create<CallableType>(forward<Args>(_args)...);
  if (call_allocation) {
    call_queue.Push(*call_allocation);
    LOG_PARALLEL(VERBOSE, "Posted call @ %p!\n",
                 reinterpret_cast<void *>(call_allocation));
    return true;
  } else {
    return false;
  }
}

template<typename State>
inline CallAllocationRef<State> CallContext<State>::Acquire() {
  auto call_allocation = call_queue.Pop();
  if (call_allocation) {
    LOG_PARALLEL(VERBOSE, "Acquired call @ %p!\n",
                 reinterpret_cast<void *>(call_allocation));
    return CallAllocationRef<State>(*call_allocation);
  }
  return CallAllocationRef<State>();
}

template<typename State>
template<typename Callback, typename... Args>
inline Task<State, Callback> *
CallContext<State>::CreateTask(size_t _call_count,
                               core::CallableWrapper<Callback> _callback,
                               Args &&... _args) {
  return task_allocator.template Create<Callback>(*this, _call_count, _callback,
                                                  forward<Args>(_args)...);
}

template<typename State>
template<typename Callback>
inline void
CallContext<State>::DestroyTask(Task<State, Callback> const & _task) {
  task_allocator.Destroy(_task);
}

/*******************************************************************************
** ProcessThread
*******************************************************************************/

template<typename State>
template<typename... Args>
inline ProcessThread<State>::ProcessThread(Thread _thread,
                                           Process<State> & _process,
                                           State & _state)
    : thread(move(_thread))
    , state(_process, *this, _state) {}

template<typename State>
template<typename F, typename... Args>
inline bool ProcessThread<State>::Post(core::CallableWrapper<F> _function,
                                       Args &&... _args) {
  return call_context.Post(move(_function), forward<Args>(_args)...);
}

template<typename State>
template<typename Callback, typename... Args>
inline Task<State, Callback> *
ProcessThread<State>::CreateTask(size_t _call_count,
                                 core::CallableWrapper<Callback> _callback,
                                 Args &&... _args) {
  return call_context.CreateTask(_call_count, move(_callback),
                                 forward<Args>(_args)...);
}

template<typename State>
inline bool ProcessThread<State>::Launch() {
  return thread.Run(core::wrap_callable(&ProcessThread::Run, *this));
}

template<typename State>
inline bool ProcessThread<State>::IsJoinable() const {
  return thread.IsJoinable();
}

template<typename State>
inline void ProcessThread<State>::Join() {
  LOG_PARALLEL(VERBOSE, "Join..\n");
  thread.Join();
  LOG_PARALLEL(VERBOSE, "Joined!\n");
}

template<typename State>
inline void ProcessThread<State>::Run() {
  auto & process = *state.process;

  LOG_PARALLEL(VERBOSE, "Started @ CPU#%zu!\n", Thread::GetCurrentThreadId());

  for (;;) {
    // Local call
    LOG_PARALLEL(VERBOSE, "CPU#%zu Acquiring local call..\n",
                 Thread::GetCurrentThreadId());
    if (auto call_reference = call_context.Acquire()) {
      LOG_PARALLEL(VERBOSE, "Local call on CPU#%zu..\n",
                   Thread::GetCurrentThreadId());
      call_reference(state);
      LOG_PARALLEL(VERBOSE, "Local call on CPU#%zu finished..\n",
                   Thread::GetCurrentThreadId());
      continue;
    }
    // Shared call
    LOG_PARALLEL(VERBOSE, "CPU#%zu Acquiring shared call..\n",
                 Thread::GetCurrentThreadId());
    if (auto call_reference = process.shared_call_context.Acquire()) {
      LOG_PARALLEL(VERBOSE, "Shared call on CPU#%zu..\n",
                   Thread::GetCurrentThreadId());
      call_reference(state);
      LOG_PARALLEL(VERBOSE, "Shared call on CPU#%zu finished..\n",
                   Thread::GetCurrentThreadId());
      continue;
    }
    // Stolen call
    bool called = false;
    using ProcessThreadQuery =
        typename Process<State>::ProcessThreadChain::QueryType;
    auto query = ProcessThreadQuery::FromElement(process.thread_chain, *this);
    query.template CircularForward<container::Head>();
    while (auto process_thread =
               query.template CircularForward<container::Head>()) {
      if (process_thread != this) {
        LOG_PARALLEL(VERBOSE, "CPU#%zu Acquiring stolen call from %p..\n",
                     Thread::GetCurrentThreadId(),
                     static_cast<void *>(process_thread));
        if (auto call_reference = process_thread->call_context.Acquire()) {
          LOG_PARALLEL(VERBOSE, "Stolen call on CPU#%zu..\n",
                       Thread::GetCurrentThreadId());
          call_reference(state);
          LOG_PARALLEL(VERBOSE, "Stolen call on CPU#%zu finished..\n",
                       Thread::GetCurrentThreadId());
          called = true;
          break;
        }
      } else {
        LOG_PARALLEL(VERBOSE, "CPU#%zu Skip..\n", Thread::GetCurrentThreadId());
        break;
      }
    }
    // No calls
    if (!called) {
      if (process.IsTerminated()) {
        LOG_PARALLEL(VERBOSE, "CPU#%zu Terminating..\n",
                     Thread::GetCurrentThreadId());
        return;
      } else {
        LOG_PARALLEL(VERBOSE, "CPU#%zu Locking sleep mutex..\n",
                     Thread::GetCurrentThreadId());
        UniqueLock<Mutex> lock(process.sleep_mutex);
        if (process.sleep_please) {
          LOG_PARALLEL(VERBOSE, "CPU#%zu Sleeping..\n",
                       Thread::GetCurrentThreadId());
          process.sleep_condition.wait(
              lock, [&process] { return !process.sleep_please; });
          LOG_PARALLEL(VERBOSE, "CPU#%zu Awakening..\n",
                       Thread::GetCurrentThreadId());
          lock.unlock();
        } else {
          LOG_PARALLEL(VERBOSE, "CPU#%zu Relaxing..\n",
                       Thread::GetCurrentThreadId());
          lock.unlock();
          // TODO: Find a good relax strategy!
#if 0
          std::this_thread::yield();
#else
          const std::chrono::milliseconds relax_duration(5);
          std::this_thread::sleep_for(relax_duration);
#endif
        }
      }
    }
  }
}

}}

////////////////////////////////////////////////////////////////////////////////
// parallel implementation
////////////////////////////////////////////////////////////////////////////////

namespace parallel {

/*******************************************************************************
** Process
*******************************************************************************/

template<typename State>
template<typename... Args>
inline Process<State>::Process(container::Array<Thread> _threads,
                               Args &&... _args)
    : state(forward<Args>(_args)...)
    , terminated(false) {
  for (Thread & thread : _threads) {
    auto & thread_process = thread_array.Create(move(thread), *this, state);
    thread_chain.template Link<container::Tail>(thread_process);
  }
}

template<typename State>
inline Process<State>::~Process() {
  if (IsJoinable()) {
    if (!IsTerminated()) {
      Terminate();
    }
    Join();
  }
  for (auto & process_thread : thread_array) {
    thread_chain.template Unlink<container::Tail>(process_thread);
  }
}

template<typename State>
template<typename F, typename... Args>
inline bool Process<State>::Post(core::CallableWrapper<F> _function,
                                 Args &&... _args) {
  return shared_call_context.Post(move(_function), forward<Args>(_args)...);
}

template<typename State>
template<typename Callback, typename... Args>
inline Task<ProcessState<State>, Callback> *
Process<State>::CreateTask(size_t _call_count,
                           core::CallableWrapper<Callback> _callback,
                           Args &&... _args) {
  return shared_call_context.CreateTask(_call_count, move(_callback),
                                        forward<Args>(_args)...);
}

template<typename State>
inline void Process<State>::Launch() {
  for (auto & process_thread : thread_array) {
    process_thread.Launch();
  }
}

template<typename State>
inline void Process<State>::Sleep() {
  LOG_PARALLEL(VERBOSE, "Sleeping..\n");
  {
    LockGuard<Mutex> lock(sleep_mutex);
    sleep_please = true;
    sleep_condition.notify_all();
  }
}

template<typename State>
inline void Process<State>::Wake() {
  LOG_PARALLEL(VERBOSE, "Wake..\n");
  {
    LockGuard<Mutex> lock(sleep_mutex);
    sleep_please = false;
    sleep_condition.notify_all();
  }
}

template<typename State>
inline bool Process<State>::IsJoinable() const {
  for (auto & process_thread : thread_array) {
    if (process_thread.IsJoinable()) {
      return true;
    }
  }
  return false;
}

template<typename State>
inline void Process<State>::Join() {
  for (auto & process_thread : thread_array) {
    process_thread.Join();
  }
}

template<typename State>
inline bool Process<State>::IsTerminated() const {
  return terminated;
}

template<typename State>
inline void Process<State>::Terminate() {
  LOG_PARALLEL(VERBOSE, "Terminating..\n");
  terminated = true;
}

/*******************************************************************************
** ProcessState
*******************************************************************************/

template<typename State>
inline ProcessState<State>::ProcessState(Process<State> & _process,
                                         detail::ProcessThread<State> & _thread,
                                         State & _state)
    : process(&_process)
    , thread(&_thread)
    , state(_state) {}

template<typename State>
template<typename F, typename... Args>
inline void ProcessState<State>::PostShared(core::CallableWrapper<F> _function,
                                            Args &&... _args) {
  process->Post(move(_function), forward<Args>(_args)...);
}

template<typename State>
template<typename F, typename... Args>
inline void ProcessState<State>::PostLocal(core::CallableWrapper<F> _function,
                                           Args &&... _args) {
  thread->Post(move(_function), forward<Args>(_args)...);
}

template<typename State>
template<typename Callback, typename... Args>
inline Task<ProcessState<State>, Callback> *
ProcessState<State>::CreateSharedTask(size_t _call_count,
                                      core::CallableWrapper<Callback> _callback,
                                      Args &&... _args) {
  return process->CreateTask(_call_count, move(_callback),
                             forward<Args>(_args)...);
}

template<typename State>
template<typename Callback, typename... Args>
inline Task<ProcessState<State>, Callback> *
ProcessState<State>::CreateLocalTask(size_t _call_count,
                                     core::CallableWrapper<Callback> _callback,
                                     Args &&... _args) {
  return thread->CreateTask(_call_count, move(_callback),
                            forward<Args>(_args)...);
}

template<typename State>
inline void ProcessState<State>::Sleep() {
  process->Sleep();
}

template<typename State>
inline void ProcessState<State>::Wake() {
  process->Wake();
}

template<typename State>
inline bool ProcessState<State>::IsTerminated() const {
  return process->IsTerminated();
}

template<typename State>
inline void ProcessState<State>::Terminate() {
  process->Terminate();
}

/*******************************************************************************
** Task
*******************************************************************************/

template<typename State, typename Callback>
template<typename... Args>
inline Task<State, Callback>::Task(detail::CallContext<State> & _call_context,
                                   size_t _call_count, Args &&... _args)
    : callback(forward<Args>(_args)...)
    , call_count(_call_count)
    , call_context(&_call_context) {}

template<typename State, typename Callback>
inline Task<State, Callback>::~Task() {
  CORE_LIB_ASSERT(call_count == 0u);
}

template<typename State, typename Callback>
template<typename F, typename... Args>
inline bool Task<State, Callback>::Post(core::CallableWrapper<F> _f,
                                        Args &&... _args) {
  using CallableType = detail::TaskCall<F, State, Callback>;
  return call_context->template PostCallable<CallableType>(
      *this, move(_f), forward<Args>(_args)...);
}

template<typename State, typename Callback>
inline void Task<State, Callback>::Finish(State & _state) {
  LOG_PARALLEL(VERBOSE, "Finish @ %p\n", this);
  if (call_count.fetch_sub(1, std::memory_order_release) == 1u) {
    std::atomic_thread_fence(std::memory_order_acquire);
    LOG_PARALLEL(VERBOSE, "Callback..\n");
    callback(_state);
    call_context->DestroyTask(*this);
  }
}

/*******************************************************************************
** TaskFinalizer
*******************************************************************************/

template<typename State, typename Callback>
inline void
TaskFinalizer<State, Callback>::operator()(State & _state,
                                           Task<State, Callback> * _task) {
  CORE_LIB_ASSERT(_task);
  LOG_PARALLEL(VERBOSE, "Fnishing @ %p -> %p\n", this, _task);
  _task->Finish(_state);
}

}

#endif
