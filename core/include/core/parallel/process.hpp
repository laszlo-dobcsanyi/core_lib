#ifndef _CORE_LIB_PARALLEL_PROCESS_HPP_
#define _CORE_LIB_PARALLEL_PROCESS_HPP_

#include "core/allocators.hpp"
#include "core/containers.hpp"
#include "core/ranges/span.hpp"
#include "core/callable/closure.hpp"
#include "core/callable/virtual_callable.hpp"
#include "core/parallel/thread.hpp"

////////////////////////////////////////////////////////////////////////////////
// parallel
////////////////////////////////////////////////////////////////////////////////

namespace parallel {

class ThreadAttributes;
class Thread;

template<typename State>
class Process;
template<typename State>
class ProcessState;
template<typename State, typename Callback>
class Task;
template<typename State, typename Callback>
class TaskFinalizer;
class ProcessContext;

}

////////////////////////////////////////////////////////////////////////////////
// parallel detail
////////////////////////////////////////////////////////////////////////////////

namespace parallel { namespace detail {

/*******************************************************************************
** CallAllocationBase
*******************************************************************************/

template<typename CallableType>
struct CallAllocationBase {
  size_t size;
  void * allocator;
  core::memory::AlignedStorage<alignof(CallableType), sizeof(CallableType)>
      storage;
};

/*******************************************************************************
** CallAllocation
*******************************************************************************/

template<typename State>
struct CallAllocation final
    : public CallAllocationBase<
          core::VirtualCallable<void, mpl::TypePack<State &>>> {
  void operator()(State & _state);
};

/*******************************************************************************
** CallAllocationRef
*******************************************************************************/

template<typename State>
class CallAllocationRef final {
public:
  CallAllocationRef();
  CallAllocationRef(CallAllocation<State> & _call_allocation);
  CallAllocationRef(CallAllocationRef && _other);
  CallAllocationRef(CallAllocationRef const &) = delete;
  CallAllocationRef & operator=(CallAllocationRef && _other);
  CallAllocationRef & operator=(CallAllocationRef const &) = delete;
  ~CallAllocationRef();

  explicit operator bool() const;
  void operator()(State & _state);

private:
  CallAllocation<State> * call_allocation;
};

/*******************************************************************************
** CallAllocator
*******************************************************************************/

template<typename State>
class CallAllocator final {
public:
  CallAllocator() = default;
  CallAllocator(CallAllocator &&) = default;
  CallAllocator(CallAllocator const &) = delete;
  CallAllocator & operator=(CallAllocator &&) = default;
  CallAllocator & operator=(CallAllocator const &) = delete;
  ~CallAllocator() = default;

  template<typename CallableType, typename... Args>
  CallAllocation<State> * Create(Args &&... _args);

  void Destroy(CallAllocation<State> & _call_allocation);

private:
  allocator::HeapAllocator allocator;
};

/*******************************************************************************
** CallQueue
*******************************************************************************/

template<typename State>
class CallQueue final {
public:
  CallQueue() = default;
  CallQueue(CallQueue &&) = default;
  CallQueue(CallQueue const &) = delete;
  CallQueue & operator=(CallQueue &&) = default;
  CallQueue & operator=(CallQueue const &) = delete;
  ~CallQueue() = default;

  void Push(CallAllocation<State> & _call_allocation);
  CallAllocation<State> * Pop();

private:
  template<typename T>
  using NodeAllocator = allocator::TypeAllocator<T, allocator::HeapAllocator>;
  container::LockedQueue<CallAllocation<State>, NodeAllocator> call_queue;
};

/*******************************************************************************
** TaskAllocator
*******************************************************************************/

template<typename State>
class TaskAllocator final {
public:
  TaskAllocator() = default;
  TaskAllocator(TaskAllocator &&) = default;
  TaskAllocator(TaskAllocator const &) = delete;
  TaskAllocator & operator=(TaskAllocator &&) = default;
  TaskAllocator & operator=(TaskAllocator const &) = delete;
  ~TaskAllocator() = default;

  template<typename Callback, typename... Args>
  Task<State, Callback> * Create(Args &&... _args);

  template<typename Callback>
  void Destroy(Task<State, Callback> const & _task);

private:
  allocator::HeapAllocator allocator;
};

/*******************************************************************************
** TaskCall
*******************************************************************************/

template<typename F, typename State, typename Callback>
class TaskCall : public core::VirtualCallable<void, mpl::TypePack<State &>> {
public:
  template<typename... Args>
  TaskCall(Task<State, Callback> & _task, Args &&... _args);
  TaskCall(TaskCall &&) = default;
  TaskCall(TaskCall const &) = default;
  TaskCall & operator=(TaskCall &&) = default;
  TaskCall & operator=(TaskCall const &) = default;
  virtual ~TaskCall() = default;

  virtual void operator()(State & _state) override;

protected:
  core::CallableClosure<F, State> closure;
  Task<State, Callback> * task;
};

/*******************************************************************************
** CallContext
*******************************************************************************/

template<typename State>
class CallContext final {
public:
  CallContext() = default;
  CallContext(CallContext &&) = default;
  CallContext(CallContext const &) = delete;
  CallContext & operator=(CallContext &&) = default;
  CallContext & operator=(CallContext const &) = delete;
  ~CallContext() = default;

  template<typename F, typename... Args>
  bool Post(core::CallableWrapper<F> _function, Args &&... _args);

  template<typename F, typename... Args>
  bool PostCallable(Args &&... _args);

  CallAllocationRef<State> Acquire();

  template<typename Callback, typename... Args>
  Task<State, Callback> * CreateTask(size_t _call_count,
                                     core::CallableWrapper<Callback> _callback,
                                     Args &&... _args);

  template<typename Callback>
  void DestroyTask(Task<State, Callback> const & _task);

private:
  CallAllocator<State> call_allocator;
  CallQueue<State> call_queue;
  TaskAllocator<State> task_allocator;
};

/*******************************************************************************
** ProcessThread
*******************************************************************************/

template<typename State>
class ProcessThread final
    : public ::container::ChainElement<ProcessThread<State>> {
public:
  template<typename... Args>
  ProcessThread(Thread _thread, Process<State> & _process, State & _state);
  ProcessThread(ProcessThread &&) = default;
  ProcessThread(ProcessThread const &) = delete;
  ProcessThread & operator=(ProcessThread &&) = default;
  ProcessThread & operator=(ProcessThread const &) = delete;
  ~ProcessThread() = default;

  template<typename F, typename... Args>
  bool Post(core::CallableWrapper<F> _function, Args &&... _args);

  template<typename Callback, typename... Args>
  Task<State, Callback> * CreateTask(size_t _call_count,
                                     core::CallableWrapper<Callback> _callback,
                                     Args &&... _args);

  bool Launch();

  bool IsJoinable() const;
  void Join();

private:
  Thread thread;
  CallContext<ProcessState<State>> call_context;
  ProcessState<State> state;

  void Run();
};

}}

////////////////////////////////////////////////////////////////////////////////
// parallel
////////////////////////////////////////////////////////////////////////////////

namespace parallel {

/*******************************************************************************
** Process
*******************************************************************************/

template<typename State>
class Process final {
public:
  friend detail::ProcessThread<State>;

  using ProcessThreadArray = container::PoolArray<detail::ProcessThread<State>>;
  using ProcessThreadChain = container::Chain<detail::ProcessThread<State>>;

  State state;

  template<typename... Args>
  Process(container::Array<Thread> _threads, Args &&... _args);
  Process(Process &&) = delete;
  Process(Process const &) = delete;
  Process & operator=(Process &&) = delete;
  Process & operator=(Process const &) = delete;
  ~Process();

  template<typename F, typename... Args>
  bool Post(core::CallableWrapper<F> _function, Args &&... _args);

  template<typename Callback, typename... Args>
  Task<ProcessState<State>, Callback> *
  CreateTask(size_t _call_count, core::CallableWrapper<Callback> _callback,
             Args &&... _args);

  void Launch();

  void Sleep();
  void Wake();

  bool IsJoinable() const;
  void Join();

  bool IsTerminated() const;
  void Terminate();

private:
  detail::CallContext<ProcessState<State>> shared_call_context;
  ProcessThreadArray thread_array;
  ProcessThreadChain thread_chain;

  Atomic<bool> terminated;
  Mutex sleep_mutex;
  ConditionVariable sleep_condition;
  bool sleep_please = false;
};

/*******************************************************************************
** ProcessState
*******************************************************************************/

template<typename State>
class ProcessState {
public:
  friend class detail::ProcessThread<State>;

  ProcessState(Process<State> & _process,
               detail::ProcessThread<State> & _thread, State & _state);
  ProcessState(ProcessState &&) = default;
  ProcessState(ProcessState const &) = default;
  ProcessState & operator=(ProcessState &&) = default;
  ProcessState & operator=(ProcessState const &) = default;
  ~ProcessState() = default;

  template<typename F, typename... Args>
  void PostShared(core::CallableWrapper<F> _function, Args &&... _args);
  template<typename F, typename... Args>
  void PostLocal(core::CallableWrapper<F> _function, Args &&... _args);

  template<typename Callback, typename... Args>
  Task<ProcessState<State>, Callback> *
  CreateSharedTask(size_t _call_count,
                   core::CallableWrapper<Callback> _callback, Args &&... _args);
  template<typename Callback, typename... Args>
  Task<ProcessState<State>, Callback> *
  CreateLocalTask(size_t _call_count, core::CallableWrapper<Callback> _callback,
                  Args &&... _args);

  void Sleep();
  void Wake();

  bool IsTerminated() const;
  void Terminate();

  State * operator->() { return &state; }
  State const * operator->() const { return &state; }

protected:
  Process<State> * process;
  detail::ProcessThread<State> * thread;
  State & state;
};

/*******************************************************************************
** Task
*******************************************************************************/

template<typename State, typename Callback>
class Task {
public:
  template<typename... Args>
  Task(detail::CallContext<State> & _call_context, size_t _call_count,
       Args &&... _args);
  Task(Task &&) = delete;
  Task(Task const &) = delete;
  Task & operator=(Task &&) = delete;
  Task & operator=(Task const &) = delete;
  ~Task();

  void Finish(State & _state);

  template<typename F, typename... Args>
  bool Post(core::CallableWrapper<F> _f, Args &&... _args);

private:
  core::Closure<Callback, State> callback;
  Atomic<size_t> call_count;
  detail::CallContext<State> * call_context;
};

/*******************************************************************************
** TaskFinalizer
*******************************************************************************/

template<typename State, typename Callback>
class TaskFinalizer {
public:
  TaskFinalizer() = default;
  TaskFinalizer(TaskFinalizer &&) = default;
  TaskFinalizer(TaskFinalizer const &) = default;
  TaskFinalizer & operator=(TaskFinalizer &&) = default;
  TaskFinalizer & operator=(TaskFinalizer const &) = default;
  ~TaskFinalizer() = default;

  void operator()(State & _state, Task<State, Callback> * _task);
};

}

#include "core/parallel/process.inl"

#endif
