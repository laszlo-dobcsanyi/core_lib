#ifndef _CORE_LIB_PARALLEL_THREAD_HPP_
#define _CORE_LIB_PARALLEL_THREAD_HPP_

#include "core/callable/closure.hpp"
#include "core/callable/wrapped_callable.hpp"
#include "core/callable/virtual_callable.hpp"

#if defined(CORE_LIB_OS_WINDOWS)
#  include <WinBase.h>
#elif defined(CORE_LIB_OS_LINUX)
#  include <pthread.h>
#endif

#define THREAD_LOG(__severity__, ...) CORE_LIB_LOG(__severity__, __VA_ARGS__)

////////////////////////////////////////////////////////////////////////////////
// parallel detail
////////////////////////////////////////////////////////////////////////////////

namespace parallel { namespace detail {

/*******************************************************************************
** ThreadHandle
*******************************************************************************/

class ThreadHandle final {
public:
#if defined(CORE_LIB_OS_WINDOWS)
  std::atomic<HANDLE> handle;
#elif defined(CORE_LIB_OS_LINUX)
  // TODO: Should we use a mutex and a simple bool instead of this atomic bool?
  std::atomic<bool> valid;
  pthread_t handle;
#endif

  ThreadHandle();
  ThreadHandle(ThreadHandle && _other);
  ThreadHandle(ThreadHandle const &) = delete;
  ThreadHandle & operator=(ThreadHandle && _other);
  ThreadHandle & operator=(ThreadHandle const &) = delete;
  ~ThreadHandle() = default;

  bool IsValid() const;
  void Invalidate();
};

/*******************************************************************************
** StaticRunArguments
*******************************************************************************/

class StaticRunArguments final {
public:
  ThreadHandle * thread_handle;
  core::VirtualCallable<void, mpl::TypePack<>> * callable;

  StaticRunArguments(ThreadHandle * _thread_handle,
                     core::VirtualCallable<void, mpl::TypePack<>> * _callable);
  StaticRunArguments(StaticRunArguments &&) = delete;
  StaticRunArguments(StaticRunArguments const &) = delete;
  StaticRunArguments & operator=(StaticRunArguments &&) = delete;
  StaticRunArguments & operator=(StaticRunArguments const &) = delete;
  ~StaticRunArguments() = default;
};

/*******************************************************************************
** StaticRun
*******************************************************************************/

#if defined(CORE_LIB_OS_WINDOWS)
static DWORD WINAPI StaticRun(LPVOID _arguments);
#elif defined(CORE_LIB_OS_LINUX)
static void StaticRunCleanup(void * _arguments);
static void * StaticRun(void * _arguments);
#endif

}}

////////////////////////////////////////////////////////////////////////////////
// parallel
////////////////////////////////////////////////////////////////////////////////

namespace parallel {

/*******************************************************************************
** ThreadAttributes
*******************************************************************************/

class ThreadAttributes {
public:
  size_t CPU;
  size_t stack_size;
#if defined(CORE_LIB_OS_LINUX)
  pthread_attr_t attributes;
#endif

  ThreadAttributes();
  ThreadAttributes(size_t _CPU, size_t _stack_size);
  ~ThreadAttributes() = default;
};

/*******************************************************************************
** Thread
*******************************************************************************/

class Thread final {
public:
  Thread();
  Thread(ThreadAttributes const & _attributes);
  Thread(Thread && _other);
  Thread(Thread const &) = delete;
  Thread & operator=(Thread && _other);
  Thread & operator=(Thread const &) = delete;
  ~Thread();

  static size_t GetCurrentThreadId();

  void SetAttributes(ThreadAttributes const & _attributes);

  template<typename F, typename... Args>
  bool Run(core::CallableWrapper<F> _function_wrapper, Args &&... _args);
  bool IsJoinable() const;
  bool Join();
  void Interrupt();

private:
  ThreadAttributes thread_attributes;
  detail::ThreadHandle thread_handle;
  void * arguments = nullptr;

  template<typename F, typename... Args>
  bool InitializeArguments(core::CallableWrapper<F> _function_wrapper,
                           Args &&... _args);
  bool FinalizeArguments();
};

////////////////////////////////////////////////////////////////////////////////
// detail implementation
////////////////////////////////////////////////////////////////////////////////

namespace detail {

/*******************************************************************************
** ThreadHandle
*******************************************************************************/

#if defined(CORE_LIB_OS_WINDOWS)
inline ThreadHandle::ThreadHandle()
    : handle(INVALID_HANDLE_VALUE) {}

inline ThreadHandle::ThreadHandle(ThreadHandle && _other)
    : handle(_other.handle.load()) {}

inline ThreadHandle & ThreadHandle::operator=(ThreadHandle && _other) {
  if (this != &_other) {
    handle = _other.handle.load();
  }
  return *this;
}
#elif defined(CORE_LIB_OS_LINUX)
inline ThreadHandle::ThreadHandle()
    : valid(false) {}

inline ThreadHandle::ThreadHandle(ThreadHandle && _other)
    : valid(_other.valid.load()) {}

inline ThreadHandle & ThreadHandle::operator=(ThreadHandle && _other) {
  if (this != &_other) {
    valid = _other.valid.load();
  }
  return *this;
}
#endif

inline bool ThreadHandle::IsValid() const {
#if defined(CORE_LIB_OS_WINDOWS)
  return handle != INVALID_HANDLE_VALUE;
#elif defined(CORE_LIB_OS_LINUX)
  return valid;
#endif
}

inline void ThreadHandle::Invalidate() {
#if defined(CORE_LIB_OS_WINDOWS)
  handle = INVALID_HANDLE_VALUE;
#elif defined(CORE_LIB_OS_LINUX)
  valid = false;
#endif
}

/*******************************************************************************
** StaticRunArguments
*******************************************************************************/

inline StaticRunArguments::StaticRunArguments(
    ThreadHandle * _thread_handle,
    core::VirtualCallable<void, mpl::TypePack<>> * _callable)
    : thread_handle(_thread_handle)
    , callable(_callable) {}

/*******************************************************************************
** StaticRun
*******************************************************************************/

#if defined(CORE_LIB_OS_WINDOWS)
inline DWORD WINAPI StaticRun(LPVOID _arguments) {
  auto & arguments = *reinterpret_cast<StaticRunArguments *>(_arguments);
  application::run(*arguments.callable);
  CORE_LIB_ASSERT(arguments.thread_handle);
  arguments.thread_handle->handle = INVALID_HANDLE_VALUE;
  return 1;
}
#elif defined(CORE_LIB_OS_LINUX)
inline void StaticRunCleanup(void * _arguments) {
  auto & arguments = *reinterpret_cast<StaticRunArguments *>(_arguments);
  arguments.thread_handle->Invalidate();
}

inline void * StaticRun(void * _arguments) {
  pthread_cleanup_push(StaticRunCleanup, _arguments);

  {
    auto & arguments = *reinterpret_cast<StaticRunArguments *>(_arguments);
    (*arguments.callable)();
  }

  pthread_cleanup_pop(true);
  return nullptr;
}
#endif

}

////////////////////////////////////////////////////////////////////////////////
// implementation
////////////////////////////////////////////////////////////////////////////////

/*******************************************************************************
** ThreadAttributes
*******************************************************************************/

inline ThreadAttributes::ThreadAttributes()
    : ThreadAttributes(0u, 0u) {}

inline ThreadAttributes::ThreadAttributes(size_t _CPU, size_t _stack_size)
    : CPU(_CPU)
    , stack_size(_stack_size) {}

/*******************************************************************************
** Thread
*******************************************************************************/

inline Thread::Thread()
    : Thread(ThreadAttributes()) {}

inline Thread::Thread(ThreadAttributes const & _attributes)
    : thread_attributes(_attributes) {}

inline Thread::Thread(Thread && _other)
    : thread_attributes(move(_other.thread_attributes))
    , thread_handle(move(_other.thread_handle)) {}

inline Thread & Thread::operator=(Thread && _other) {
  if (this != &_other) {
    thread_attributes = move(_other.thread_attributes);
    thread_handle = move(_other.thread_handle);

    _other.thread_handle.Invalidate();
  }
  return *this;
}

inline Thread::~Thread() {
  CORE_LIB_ASSERT(!IsJoinable());
  FinalizeArguments();
}

inline size_t Thread::GetCurrentThreadId() {
#if defined(CORE_LIB_OS_WINDOWS)
  return ::GetCurrentThreadId();
#elif defined(CORE_LIB_OS_LINUX)
  return ::sched_getcpu();
#endif
}

inline void Thread::SetAttributes(ThreadAttributes const & _attributes) {
  thread_attributes = _attributes;
}

template<typename F, typename... Args>
inline bool Thread::Run(core::CallableWrapper<F> _function_wrapper,
                        Args &&... _args) {
  if (!InitializeArguments(move(_function_wrapper), forward<Args>(_args)...)) {
    return false;
  }
#if defined(CORE_LIB_OS_WINDOWS)
  thread_handle.handle =
      ::CreateThread(NULL, thread_attributes.stack_size, &detail::StaticRun,
                     arguments, CREATE_SUSPENDED, NULL);
  if (!thread_handle.handle) {
    THREAD_LOG(ERROR, "Failed to create thread handle!\n");
    return false;
  }

  uint32 cpu_index = U32(1) << thread_attributes.CPU;
  if (!::SetThreadAffinityMask(thread_handle.handle, cpu_index)) {
    THREAD_LOG(ERROR, "Failed to set CPU affinity to %" PRIu32 "!\n",
               cpu_index);
    return false;
  }

  if (!::ResumeThread(thread_handle.handle)) {
    THREAD_LOG(ERROR, "Failed to launch thread handle!\n");
    return false;
  }
#elif defined(CORE_LIB_OS_LINUX)
  auto & attributes = thread_attributes.attributes;
  if (int error = ::pthread_attr_init(&attributes)) {
    THREAD_LOG(ERROR, "Failed to initialize pthread attributes!\n");
    return false;
  }

  if (0 < thread_attributes.stack_size) {
    if (int error = ::pthread_attr_setstacksize(&attributes,
                                                thread_attributes.stack_size)) {
      THREAD_LOG(ERROR, "Failed to set stack size to %i!\n",
                 thread_attributes.stack_size);
      return false;
    }
  }

  int cpu_index = static_cast<int>(thread_attributes.CPU);
  cpu_set_t CPUs;
  CPU_ZERO(&CPUs);
  CPU_SET(cpu_index, &CPUs);
  if (int error = ::pthread_attr_setaffinity_np(&attributes, sizeof(cpu_set_t),
                                                &CPUs)) {
    THREAD_LOG(ERROR, "Failed to set CPU affinity to %d!\n", cpu_index);
    return false;
  }

  if (int error = ::pthread_create(&thread_handle.handle, &attributes,
                                   &detail::StaticRun, arguments)) {
    THREAD_LOG(ERROR, "Failed to create thread handle!\n");
    return false;
  }

  if (int error = ::pthread_attr_destroy(&attributes)) {
    THREAD_LOG(ERROR, "Failed to finalize attributes!\n");
    return false;
  }

  thread_handle.valid = true;
#endif
  return true;
}

inline bool Thread::IsJoinable() const {
#if defined(CORE_LIB_OS_WINDOWS)
  return (thread_handle.handle != INVALID_HANDLE_VALUE);
#elif defined(CORE_LIB_OS_LINUX)
  if (!thread_handle.IsValid()) {
    return false;
  }
  int detach_state;
  if (int error = ::pthread_attr_getdetachstate(&thread_attributes.attributes,
                                                &detach_state)) {
    THREAD_LOG(ERROR, "Failed to get detach state! Error value: %d\n", error);
    return false;
  }
  if (detach_state != PTHREAD_CREATE_JOINABLE) {
    // TODO: Check result!
    return false;
  }
  return true;
#endif
}

inline bool Thread::Join() {
#if defined(CORE_LIB_OS_WINDOWS)
  auto handle = thread_handle.handle.load();
  if (handle != INVALID_HANDLE_VALUE) {
    auto result = ::WaitForSingleObject(handle, INFINITE);
    if (result == WAIT_FAILED) {
      // TODO: Check result!
      return false;
    }
  }
  return true;
#elif defined(CORE_LIB_OS_LINUX)
  if (thread_handle.IsValid()) {
    if (int error = ::pthread_join(thread_handle.handle, nullptr)) {
      THREAD_LOG(ERROR, "Failed join! Error value: %d\n", error);
      return false;
    }
  }
  return true;
#endif
}

inline void Thread::Interrupt() { CORE_LIB_UNIMPLEMENTED; }

template<typename F, typename... Args>
inline bool
Thread::InitializeArguments(core::CallableWrapper<F> _function_wrapper,
                            Args &&... _args) {
  CORE_LIB_STATIC_ASSERT(mpl::is_same<mpl::result_of_t<F>, void>::value);
  CORE_LIB_ASSERT(!arguments);
  arguments = ::malloc(sizeof(detail::StaticRunArguments));
  if (arguments) {
    using CallableClosureType = core::CallableClosure<F>;
    if (auto closure_allocation = ::malloc(sizeof(CallableClosureType))) {
      auto closure =
          reinterpret_cast<CallableClosureType *>(closure_allocation);
      new (closure)
          CallableClosureType(_function_wrapper, forward<Args>(_args)...);
      auto static_run_arguments =
          reinterpret_cast<detail::StaticRunArguments *>(arguments);
      new (static_run_arguments)
          detail::StaticRunArguments(&thread_handle, closure);
      return true;
    } else {
      THREAD_LOG(ERROR, "Failed to allocate StoredCall!\n");
      ::free(arguments);
      return false;
    }
  } else {
    THREAD_LOG(ERROR, "Failed to allocate StaticRunArgument!\n");
    return false;
  }
}

inline bool Thread::FinalizeArguments() {
  if (arguments) {
    auto static_run_arguments =
        reinterpret_cast<detail::StaticRunArguments *>(arguments);
    using CallableType = core::VirtualCallable<void, mpl::TypePack<>>;
    static_run_arguments->callable->~CallableType();
    ::free(static_run_arguments->callable);
    ::free(static_run_arguments);
  }
  return true;
}

}

#undef THREAD_MESSAGE

#endif
