#ifndef _CORE_LIB_MEMORY_OS_ALLOC_HPP_
#define _CORE_LIB_MEMORY_OS_ALLOC_HPP_

#include "core/base.h"

#if defined(CORE_LIB_OS_WINDOWS)
#  include <Memoryapi.h> // ::VirtualAlloc, ::VirtualFree
#elif defined(CORE_LIB_OS_LINUX)
#  include <sys/mman.h> // mmap
#endif

////////////////////////////////////////////////////////////////////////////////
// core memory
////////////////////////////////////////////////////////////////////////////////

namespace core { namespace memory {

/*******************************************************************************
** os_alloc
*******************************************************************************/

inline uintptr_t os_alloc(size_t _size) {
  CORE_LIB_ASSERT_TEXT(_size % platform::page_size() == 0,
                       "_size(%ld) must be multiple of page size (%ld)!\n",
                       Size, platform::page_size());
#if defined(CORE_LIB_OS_WINDOWS)
  return static_cast<uintptr_t>(
      ::VirtualAlloc(nullptr, static_cast<SIZE_T>(_size),
                     MEM_RESERVE | MEM_COMMIT, PAGE_READWRITE));
#elif defined(CORE_LIB_OS_LINUX)
  return static_cast<uintptr_t>(
      ::mmap(nullptr, Size, PROT_NONE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0));
#endif
}

/*******************************************************************************
** os_free
*******************************************************************************/

inline uintptr_t os_free(uintptr_t _ptr) {
#if defined(CORE_LIB_OS_WINDOWS)
  if (!::VirtualFree(_ptr, 0, MEM_RELEASE)) {
    CORE_LIB_ASSERT_MESSAGE("Failed to free memory: %ld!", ::GetLastError());
  }
#elif defined(CORE_LIB_OS_LINUX)
  if (::munmap(reinterpret_cast<void *>(_ptr), Size) == -1) {
    CORE_LIB_ASSERT_MESSAGE("Failed to free memory!");
  }
#endif
}

}}

#endif
