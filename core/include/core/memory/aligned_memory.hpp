#ifndef _CORE_LIB_MEMORY_ALIGNED_MEMORY_HPP_
#define _CORE_LIB_MEMORY_ALIGNED_MEMORY_HPP_

#include "core/memory/deleters.hpp"

////////////////////////////////////////////////////////////////////////////////
// core memory
////////////////////////////////////////////////////////////////////////////////

namespace core { namespace memory {

/*******************************************************************************
** AlignedMemory
*******************************************************************************/

template<size_t Alignment, class Deleter>
class AlignedMemory {
public:
  using pointer_type = void *;
  using const_pointer_type = void const *;

  AlignedMemory() = default;
  AlignedMemory(std::nullptr_t);
  AlignedMemory(void * _ptr, size_t _size);
  AlignedMemory(AlignedMemory && _other);
  AlignedMemory(AlignedMemory const &) = delete;
  AlignedMemory & operator=(AlignedMemory && _other);
  AlignedMemory & operator=(AlignedMemory const &) = delete;
  ~AlignedMemory();

  bool IsNull() const { return ptr == nullptr; }
  explicit operator bool() const { return !IsNull(); }

  size_t Size() const { return size; }

  pointer_type Get() { return ptr; }
  const_pointer_type Get() const { return ptr; }

  span<byte> Bytes();
  span<byte const> Bytes() const;

  void Reset();
  pointer_type Release();

protected:
  void * ptr = nullptr;
  size_t size = 0u;
};

}}

////////////////////////////////////////////////////////////////////////////////
// core memory
////////////////////////////////////////////////////////////////////////////////

namespace core { namespace memory {

/*******************************************************************************
** AlignedMemory
*******************************************************************************/

template<size_t Alignment, class Deleter>
inline AlignedMemory<Alignment, Deleter>::AlignedMemory(std::nullptr_t)
    : AlignedMemory(std::nullptr_t(), 0) {}

template<size_t Alignment, class Deleter>
inline AlignedMemory<Alignment, Deleter>::AlignedMemory(void * _ptr,
                                                        size_t _size)
    : ptr(_ptr)
    , size(_size) {
  CORE_LIB_ASSERT(
      memory::is_aligned(reinterpret_cast<uintptr_t>(_ptr), Alignment));
}

template<size_t Alignment, class Deleter>
inline AlignedMemory<Alignment, Deleter>::AlignedMemory(
    AlignedMemory && _other) {
  CORE_LIB_ASSERT(this != &_other);
  ptr = _other.ptr;
  size = _other.size;
  _other.ptr = nullptr;
  _other.size = 0u;
}

template<size_t Alignment, class Deleter>
inline AlignedMemory<Alignment, Deleter> &
AlignedMemory<Alignment, Deleter>::operator=(AlignedMemory && _other) {
  if (this != &_other) {
    if (ptr != nullptr) {
      Deleter deleter;
      deleter(ptr);
    }
    ptr = _other.ptr;
    size = _other.size;
    _other.ptr = nullptr;
    _other.size = 0u;
  }
  return *this;
}

template<size_t Alignment, class Deleter>
inline AlignedMemory<Alignment, Deleter>::~AlignedMemory() {
  if (ptr != nullptr) {
    Deleter deleter;
    deleter(ptr);
  }
}

template<size_t Alignment, class Deleter>
inline span<byte> AlignedMemory<Alignment, Deleter>::Bytes() {
  return make_span(reinterpret_cast<byte *>(ptr), size);
}

template<size_t Alignment, class Deleter>
inline span<byte const> AlignedMemory<Alignment, Deleter>::Bytes() const {
  return make_span(reinterpret_cast<byte const *>(ptr), size);
}

template<size_t Alignment, class Deleter>
inline void AlignedMemory<Alignment, Deleter>::Reset() {
  if (ptr != nullptr) {
    Deleter deleter;
    deleter(ptr);
    ptr = nullptr;
    size = 0u;
  }
}

template<size_t Alignment, class Deleter>
inline typename AlignedMemory<Alignment, Deleter>::pointer_type
AlignedMemory<Alignment, Deleter>::Release() {
  auto result = ptr;
  ptr = nullptr;
  size = 0u;
  return result;
}

}}

////////////////////////////////////////////////////////////////////////////////
// core
////////////////////////////////////////////////////////////////////////////////

namespace core {

/*******************************************************************************
** AlignedMemory
*******************************************************************************/

template<size_t Alignment>
using AlignedMemory =
    memory::AlignedMemory<Alignment, memory::AlignedFree<void>>;

/*******************************************************************************
** make_aligned_memory
*******************************************************************************/

template<size_t Alignment>
inline AlignedMemory<Alignment> make_aligned_memory(size_t _size) {
  auto ptr = reinterpret_cast<void *>(memory::aligned_alloc(Alignment, _size));
  // TODO: Check allocation result?
  return AlignedMemory<Alignment>(ptr, _size);
}

}

#endif
