#ifndef _CORE_LIB_MEMORY_ALIGNED_STORAGE_HPP_
#define _CORE_LIB_MEMORY_ALIGNED_STORAGE_HPP_

#include "core/memory/base.hpp"

////////////////////////////////////////////////////////////////////////////////
// core memory
////////////////////////////////////////////////////////////////////////////////

namespace core { namespace memory {

/*******************************************************************************
** AlignedStorage
*******************************************************************************/

template<size_t Alignment, size_t Size>
class AlignedStorage {
public:
  AlignedStorage() = default;
  AlignedStorage(AlignedStorage &&) = delete;
  AlignedStorage(AlignedStorage const &) = delete;
  AlignedStorage & operator=(AlignedStorage &&) = delete;
  AlignedStorage & operator=(AlignedStorage const &) = delete;
  ~AlignedStorage() = default;

  template<typename T>
  mpl::pointer_of_t<T, mpl::Mutable> Get() {
    CORE_LIB_STATIC_ASSERT(alignof(T) <= Alignment && sizeof(T) <= Size);
    return reinterpret_cast<T *>(memory);
  }

  template<typename T>
  mpl::pointer_of_t<T, mpl::Const> Get() const {
    CORE_LIB_STATIC_ASSERT(alignof(T) <= Alignment && sizeof(T) <= Size);
    return reinterpret_cast<T const *>(memory);
  }

  template<typename T>
  mpl::reference_of_t<T, mpl::Mutable> As() {
    CORE_LIB_STATIC_ASSERT(alignof(T) <= Alignment && sizeof(T) <= Size);
    return *reinterpret_cast<T *>(memory);
  }

  template<typename T>
  mpl::reference_of_t<T, mpl::Const> As() const {
    CORE_LIB_STATIC_ASSERT(alignof(T) <= Alignment && sizeof(T) <= Size);
    return *reinterpret_cast<T const *>(memory);
  }

private:
  alignas(Alignment) unsigned char memory[Size];
};

}}

#endif
