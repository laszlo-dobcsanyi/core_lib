#ifndef _CORE_LIB_MEMORY_OBJECT_HPP_
#define _CORE_LIB_MEMORY_OBJECT_HPP_

#include "core/base.h"
#include "core/collections/intrusive_list.hpp"

////////////////////////////////////////////////////////////////////////////////
// core
////////////////////////////////////////////////////////////////////////////////

namespace core {

template<class ObjectType>
class WeakCounter;

template<class ObjectType>
class Ref;

template<class ObjectType>
class Ptr;

template<class T, template<class> class CounterType>
class ObjectWrapper;

template<class T>
using WeakObjectWrapper = ObjectWrapper<T, WeakCounter>;

/*******************************************************************************
** CounterBase
*******************************************************************************/

class CounterBase {
protected:
  template<class ObjectType>
  void Rebind(Ref<ObjectType> & _ref, ObjectType * _object) {
    _ref.object = _object;
  }

  template<class ObjectType>
  void Rebind(Ptr<ObjectType> & _ptr, ObjectType * _object) {
    _ptr.object = _object;
  }
};

/*******************************************************************************
** WeakCounter
*******************************************************************************/

template<class ObjectType>
class WeakCounter : public CounterBase {
public:
  WeakCounter() = default;
  WeakCounter(WeakCounter && _other)
      : refs(move(_other.refs))
      , ptrs(move(_other.ptrs)) {
    for (Ref<ObjectType> & ref : refs) {
      CounterBase::Rebind(ref, static_cast<ObjectType *>(this));
    }
    for (Ptr<ObjectType> & ptr : ptrs) {
      CounterBase::Rebind(ptr, static_cast<ObjectType *>(this));
    }
  }
  WeakCounter(WeakCounter const &)
      : refs()
      , ptrs() {}
  WeakCounter & operator=(WeakCounter && _other) {
    CORE_LIB_ASSERT(this != &_other);
    CORE_LIB_ASSERT(refs.empty());
    for (Ptr<ObjectType> & ptr : ptrs) {
      CounterBase::Rebind(ptr, static_cast<ObjectType *>(nullptr));
    }
    refs = move(_other.refs);
    ptrs = move(_other.ptrs);
    for (Ref<ObjectType> & ref : refs) {
      CounterBase::Rebind(ref, static_cast<ObjectType *>(this));
    }
    for (Ptr<ObjectType> & ptr : ptrs) {
      CounterBase::Rebind(ptr, static_cast<ObjectType *>(this));
    }
    return *this;
  }
  WeakCounter & operator=(WeakCounter const & _other) {
    if (this != &_other) {
      Clear();
    }
    return *this;
  }
  ~WeakCounter() { Clear(); }

private:
  friend class Ref<ObjectType>;
  friend class Ptr<ObjectType>;

  void AddReference(Ref<ObjectType> & _ref) { refs.push_front(_ref); }

  void RemoveReference(Ref<ObjectType> & _ref) { refs.erase(_ref); }

  void AddReference(Ptr<ObjectType> & _ptr) { ptrs.push_front(_ptr); }

  void RemoveReference(Ptr<ObjectType> & _ptr) { ptrs.erase(_ptr); }

  void Clear() {
    CORE_LIB_ASSERT(refs.empty());
    for (Ptr<ObjectType> & ptr : ptrs) {
      CounterBase::Rebind(ptr, static_cast<ObjectType *>(nullptr));
    }
  }

  intrusive_list<Ref<ObjectType>> refs;
  intrusive_list<Ptr<ObjectType>> ptrs;
};

/*******************************************************************************
** Ref
*******************************************************************************/

template<class ObjectType>
class Ref : public intrusive_list_node {
public:
  friend CounterBase;

  using value_type = ObjectType;
  using pointer_type = value_type *;
  using const_pointer_type = value_type const *;
  using reference_type = value_type &;
  using const_reference_type = value_type const &;

  Ref(ObjectType & _object)
      : object(&_object) {
    AddReference();
  }
  Ref(Ref && _other)
      : intrusive_list_node(move(_other)) {
    object = _other.object;
    _other.object = nullptr;
  }
  Ref(Ref const & _other)
      : intrusive_list_node(_other) {
    CORE_LIB_ASSERT(this != &_other);
    object = _other.object;
    AddReference();
  }
  Ref & operator=(Ref && _other) {
    CORE_LIB_ASSERT(this != &_other);
    RemoveReference();
    object = _other.object;
    _other.object = nullptr;
    return *this;
  }
  Ref & operator=(Ref const & _other) {
    if (this != &_other) {
      RemoveReference();
      object = _other.object;
      AddReference();
    }
    return *this;
  }
  ~Ref() { RemoveReference(); }

  bool IsBound() const { return object != nullptr; }
  explicit operator bool() const { return IsBound(); }

  void Rebind(ObjectType & _object) {
    RemoveReference();
    object = &_object;
    AddReference();
  }

  reference_type Unbind() {
    CORE_LIB_ASSERT(IsBound());
    reference_type result = *object;
    RemoveReference();
    object = nullptr;
    return result;
  }

  reference_type operator*() {
    CORE_LIB_ASSERT(IsBound());
    return *object;
  }
  const_reference_type operator*() const {
    CORE_LIB_ASSERT(IsBound());
    return *object;
  }

  pointer_type operator->() {
    CORE_LIB_ASSERT(IsBound());
    return object;
  }
  const_pointer_type operator->() const {
    CORE_LIB_ASSERT(IsBound());
    return object;
  }

  pointer_type GetObject() { return object; }
  const_pointer_type GetObject() const { return object; }

private:
  ObjectType * object;

  void AddReference() {
    if (object) {
      object->AddReference(*this);
    }
  }

  void RemoveReference() {
    if (object) {
      object->RemoveReference(*this);
    }
  }
};

/*******************************************************************************
** make_ref
*******************************************************************************/

template<class ObjectType>
inline Ref<ObjectType> make_ref(ObjectType & _object) {
  return Ref<ObjectType>(_object);
}

/*******************************************************************************
** Ptr
*******************************************************************************/

template<class ObjectType>
class Ptr : public intrusive_list_node {
public:
  friend CounterBase;

  using value_type = ObjectType;
  using pointer_type = value_type *;
  using const_pointer_type = value_type const *;
  using reference_type = value_type &;
  using const_reference_type = value_type const &;

  Ptr() = default;
  Ptr(ObjectType * _object)
      : object(_object) {
    AddReference();
  }
  Ptr(ObjectType & _object)
      : Ptr(&_object) {}
  Ptr(Ref<ObjectType> _ref)
      : Ptr(_ref.GetObject()) {}
  Ptr(Ptr && _other)
      : intrusive_list_node(move(_other))
      , object(_other.object) {
    CORE_LIB_ASSERT(this != &_other);
    _other.object = nullptr;
  }
  Ptr(Ptr const & _other)
      : intrusive_list_node(_other)
      , object(_other.object) {
    CORE_LIB_ASSERT(this != &_other);
    AddReference();
  }
  Ptr & operator=(Ptr && _other) {
    if (this != &_other) {
      intrusive_list_node::operator=(move(_other));
      object = _other.object;
      _other.object = nullptr;
    }
    return *this;
  }
  Ptr & operator=(Ptr const & _other) {
    if (this != &_other) {
      RemoveReference();
      object = _other.object;
      AddReference();
    }
    return *this;
  }
  ~Ptr() = default;

  bool IsBound() const { return object != nullptr; }
  explicit operator bool() const { return IsBound(); }

  void Rebind(ObjectType & _object) {
    RemoveReference();
    object = _object;
    AddReference();
  }

  pointer_type Unbind() {
    pointer_type result = object;
    RemoveReference();
    object = nullptr;
    return result;
  }

  reference_type operator*() {
    CORE_LIB_ASSERT(IsBound());
    return *object;
  }
  const_reference_type operator*() const {
    CORE_LIB_ASSERT(IsBound());
    return *object;
  }

  pointer_type operator->() {
    CORE_LIB_ASSERT(IsBound());
    return object;
  }
  const_pointer_type operator->() const {
    CORE_LIB_ASSERT(IsBound());
    return object;
  }

  ObjectType * GetObject() { return object; }
  ObjectType const * GetObject() const { return object; }

private:
  ObjectType * object = nullptr;

  void AddReference() {
    if (object) {
      object->AddReference(*this);
    }
  }

  void RemoveReference() {
    if (object) {
      object->RemoveReference(*this);
    }
  }
};

/*******************************************************************************
** ObjectWrapper
*******************************************************************************/

template<class T, template<class> class CounterType>
class ObjectWrapper
    : public T
    , public CounterType<ObjectWrapper<T, CounterType>> {
public:
  template<typename... Args>
  ObjectWrapper(Args &&... _args)
      : T(forward<Args>(_args)...) {}
  ObjectWrapper(ObjectWrapper &&) = default;
  ObjectWrapper(ObjectWrapper const &) = default;
  ObjectWrapper & operator=(ObjectWrapper &&) = default;
  ObjectWrapper & operator=(ObjectWrapper const &) = default;
  ~ObjectWrapper() = default;
};

}

#endif
