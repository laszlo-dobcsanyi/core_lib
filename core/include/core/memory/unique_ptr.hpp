#ifndef _CORE_LIB_MEMORY_UNIQUE_PTR_HPP_
#define _CORE_LIB_MEMORY_UNIQUE_PTR_HPP_

#include "core/memory/deleters.hpp"

////////////////////////////////////////////////////////////////////////////////
// core memory
////////////////////////////////////////////////////////////////////////////////

namespace core { namespace memory {

/*******************************************************************************
** UniquePtr<T>
*******************************************************************************/

template<class T, class Deleter>
class UniquePtr {
public:
  CORE_LIB_STATIC_ASSERT(!mpl::is_same<T, void>::value);

  using value_type = T;
  using pointer_type = mpl::add_pointer_t<value_type>;
  using const_pointer_type = mpl::add_pointer_t<value_type const>;
  using reference_type = mpl::add_reference_t<value_type>;
  using const_reference_type = mpl::add_reference_t<value_type const>;

  UniquePtr() = default;
  UniquePtr(std::nullptr_t);
  explicit UniquePtr(pointer_type _ptr);
  UniquePtr(UniquePtr && _other);
  UniquePtr(UniquePtr const &) = delete;
  UniquePtr & operator=(UniquePtr && _other);
  UniquePtr & operator=(UniquePtr const &) = delete;
  ~UniquePtr();

  bool IsNull() const { return ptr == nullptr; }
  explicit operator bool() const { return !IsNull(); }

  pointer_type Get() { return ptr; }
  const_pointer_type Get() const { return ptr; }

  pointer_type operator->() { return ptr; }
  const_pointer_type operator->() const { return ptr; }

  reference_type operator*() {
    CORE_LIB_ASSERT(ptr);
    return *ptr;
  }
  const_reference_type operator*() const {
    CORE_LIB_ASSERT(ptr);
    return *ptr;
  }

  void Reset();
  pointer_type Release();

protected:
  pointer_type ptr = nullptr;
};

/*******************************************************************************
** UniquePtr<T[]>
*******************************************************************************/

template<class T, class Deleter>
class UniquePtr<T[], Deleter> {
public:
  CORE_LIB_STATIC_ASSERT(!mpl::is_same<T, void>::value);
  CORE_LIB_STATIC_ASSERT(std::is_trivially_destructible<T>::value);

  using value_type = mpl::remove_extent_t<T>;
  using pointer_type = mpl::add_pointer_t<value_type>;
  using const_pointer_type = mpl::add_pointer_t<value_type const>;
  using reference_type = mpl::add_reference_t<value_type>;
  using const_reference_type = mpl::add_reference_t<value_type const>;

  UniquePtr() = default;
  UniquePtr(std::nullptr_t);
  explicit UniquePtr(pointer_type _ptr);
  UniquePtr(UniquePtr && _other);
  UniquePtr(UniquePtr const &) = delete;
  UniquePtr & operator=(UniquePtr && _other);
  UniquePtr & operator=(UniquePtr const &) = delete;
  ~UniquePtr();

  bool IsNull() const { return ptr == nullptr; }
  explicit operator bool() const { return !IsNull(); }

  pointer_type Get() { return ptr; }
  const_pointer_type Get() const { return ptr; }

  reference_type operator[](size_t _index) { return ptr[_index]; }
  const_reference_type operator[](size_t _index) const { return ptr[_index]; }

  void Reset();
  pointer_type Release();

protected:
  pointer_type ptr = nullptr;
};

}}

////////////////////////////////////////////////////////////////////////////////
// core memory
////////////////////////////////////////////////////////////////////////////////

namespace core { namespace memory {

/*******************************************************************************
** UniquePtr<T>
*******************************************************************************/

template<class T, class Deleter>
inline UniquePtr<T, Deleter>::UniquePtr(std::nullptr_t)
    : UniquePtr(std::nullptr_t()) {}

template<class T, class Deleter>
inline UniquePtr<T, Deleter>::UniquePtr(pointer_type _ptr)
    : ptr(_ptr) {
  CORE_LIB_ASSERT(
      memory::is_aligned(reinterpret_cast<uintptr_t>(_ptr), alignof(T)));
}

template<class T, class Deleter>
inline UniquePtr<T, Deleter>::UniquePtr(UniquePtr && _other) {
  CORE_LIB_ASSERT(this != &_other);
  ptr = _other.ptr;
  _other.ptr = nullptr;
}

template<class T, class Deleter>
inline UniquePtr<T, Deleter> &
UniquePtr<T, Deleter>::operator=(UniquePtr && _other) {
  if (this != &_other) {
    if (ptr != nullptr) {
      ptr->~T();
      Deleter deleter;
      deleter(ptr);
    }
    ptr = _other.ptr;
    _other.ptr = nullptr;
  }
  return *this;
}

template<class T, class Deleter>
inline UniquePtr<T, Deleter>::~UniquePtr() {
  if (ptr != nullptr) {
    ptr->~T();
    Deleter deleter;
    deleter(ptr);
  }
}

template<class T, class Deleter>
inline void UniquePtr<T, Deleter>::Reset() {
  if (ptr != nullptr) {
    ptr->~T();
    Deleter deleter;
    deleter(ptr);
    ptr = nullptr;
  }
}

template<class T, class Deleter>
inline typename UniquePtr<T, Deleter>::pointer_type
UniquePtr<T, Deleter>::Release() {
  auto result = ptr;
  ptr = nullptr;
  return result;
}

/*******************************************************************************
** UniquePtr<T[]>
*******************************************************************************/

template<class T, class Deleter>
inline UniquePtr<T[], Deleter>::UniquePtr(std::nullptr_t)
    : UniquePtr(std::nullptr_t()) {}

template<class T, class Deleter>
inline UniquePtr<T[], Deleter>::UniquePtr(pointer_type _ptr)
    : ptr(_ptr) {
  CORE_LIB_ASSERT(
      memory::is_aligned(reinterpret_cast<uintptr_t>(_ptr), alignof(T)));
}

template<class T, class Deleter>
inline UniquePtr<T[], Deleter>::UniquePtr(UniquePtr && _other) {
  CORE_LIB_ASSERT(this != &_other);
  ptr = _other.ptr;
  _other.ptr = nullptr;
}

template<class T, class Deleter>
inline UniquePtr<T[], Deleter> &
UniquePtr<T[], Deleter>::operator=(UniquePtr && _other) {
  if (this != &_other) {
    if (ptr != nullptr) {
      Deleter deleter;
      deleter(ptr);
    }
    ptr = _other.ptr;
    _other.ptr = nullptr;
  }
  return *this;
}

template<class T, class Deleter>
inline UniquePtr<T[], Deleter>::~UniquePtr() {
  if (ptr != nullptr) {
    Deleter deleter;
    deleter(ptr);
  }
}

template<class T, class Deleter>
inline void UniquePtr<T[], Deleter>::Reset() {
  if (ptr != nullptr) {
    Deleter deleter;
    deleter(ptr);
    ptr = nullptr;
  }
}

template<class T, class Deleter>
inline typename UniquePtr<T[], Deleter>::pointer_type
UniquePtr<T[], Deleter>::Release() {
  auto result = ptr;
  ptr = nullptr;
  return result;
}

}}

////////////////////////////////////////////////////////////////////////////////
// core
////////////////////////////////////////////////////////////////////////////////

namespace core {

/*******************************************************************************
** DefaultUniquePtr
*******************************************************************************/

template<class T>
using DefaultUniquePtr = memory::UniquePtr<T, memory::Free<T>>;

/*******************************************************************************
** AlignedUniquePtr
*******************************************************************************/

template<class T>
using AlignedUniquePtr = memory::UniquePtr<T, memory::AlignedFree<T>>;

/*******************************************************************************
** UniquePtr
*******************************************************************************/

template<class T>
using UniquePtr = mpl::conditional_t<memory::is_standard_aligned<T>::value,
                                     DefaultUniquePtr<T>, AlignedUniquePtr<T>>;

/*******************************************************************************
** make_unique
*******************************************************************************/

template<typename T, typename... Args>
inline mpl::enable_if_t<!mpl::is_array<T>::value &&
                            memory::is_standard_aligned<T>::value,
                        DefaultUniquePtr<T>>
make_unique(Args &&... _args) {
  auto ptr = reinterpret_cast<T *>(memory::alloc(sizeof(T)));
  // TODO: Check return value of allocation?
  new (ptr) T(forward<Args>(_args)...);
  return DefaultUniquePtr<T>(ptr);
}

template<typename T, typename... Args>
inline mpl::enable_if_t<!mpl::is_array<T>::value &&
                            !memory::is_standard_aligned<T>::value,
                        AlignedUniquePtr<T>>
make_unique(Args &&... _args) {
  auto ptr =
      reinterpret_cast<T *>(memory::aligned_alloc(alignof(T), sizeof(T)));
  // TODO: Check return value of allocation?
  new (ptr) T(forward<Args>(_args)...);
  return AlignedUniquePtr<T>(ptr);
}

}

#endif
