#ifndef _CORE_LIB_MEMORY_ALLOC_HPP_
#define _CORE_LIB_MEMORY_ALLOC_HPP_

#include "core/memory/base.hpp"

////////////////////////////////////////////////////////////////////////////////
// core memory
////////////////////////////////////////////////////////////////////////////////

namespace core { namespace memory {

/*******************************************************************************
** alloc
*******************************************************************************/

inline uintptr_t alloc(size_t _size) {
  if (_size == 0u) {
    return 0u;
  }
  return reinterpret_cast<uintptr_t>(std::malloc(_size));
}

/*******************************************************************************
** free
*******************************************************************************/

inline void free(uintptr_t _ptr) { std::free(reinterpret_cast<void *>(_ptr)); }

}}

#endif
