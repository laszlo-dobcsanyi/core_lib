#ifndef _CORE_LIB_MEMORY_UNINITIALIZED_ARRAY_HPP_
#define _CORE_LIB_MEMORY_UNINITIALIZED_ARRAY_HPP_

#include "core/iterator/contiguous_iterator.hpp"
#include "core/memory/aligned_storage.hpp"

////////////////////////////////////////////////////////////////////////////////
// core memory
////////////////////////////////////////////////////////////////////////////////

namespace core { namespace memory {

/*******************************************************************************
** UninitializedArray
*******************************************************************************/

template<typename T, size_t Rank>
class UninitializedArray final {
public:
  CORE_LIB_STATIC_ASSERT(0u < Rank);

  using value_type = T;
  using pointer_type = mpl::add_pointer_t<value_type>;
  using const_pointer_type = mpl::add_pointer_t<value_type const>;
  using reference_type = mpl::add_reference_t<value_type>;
  using const_reference_type = mpl::add_reference_t<value_type const>;
  using iterator_type = contiguous_iterator<value_type>;
  using const_iterator_type = contiguous_iterator<value_type const>;

  UninitializedArray();
  UninitializedArray(UninitializedArray &&) = delete;
  UninitializedArray(UninitializedArray const &) = delete;
  UninitializedArray & operator=(UninitializedArray &&) = delete;
  UninitializedArray & operator=(UninitializedArray const &) = delete;
  ~UninitializedArray();

  size_t Size() const;

  bool Contains(T const & _element) const;
  bool Contains(T const * _element) const;
  size_t IndexOf(T const & _element) const;
  size_t IndexOf(T const * _element) const;
  T * Get(size_t _index);
  T const * Get(size_t _index) const;
  T & At(size_t _index);
  T const & At(size_t _index) const;
  T & operator[](size_t _index);
  T const & operator[](size_t _index) const;

  iterator_type begin();
  const_iterator_type begin() const;
  iterator_type end();
  const_iterator_type end() const;

private:
  AlignedStorage<alignof(T), sizeof(T) * Rank> storage;

  ptrdiff_t OffsetOf(uintptr_t _memory) const;
};

}}

////////////////////////////////////////////////////////////////////////////////
// core memory
////////////////////////////////////////////////////////////////////////////////

namespace core { namespace memory {

/*******************************************************************************
** UninitializedArray
*******************************************************************************/

template<typename T, size_t Rank>
inline UninitializedArray<T, Rank>::UninitializedArray() {}

template<typename T, size_t Rank>
inline UninitializedArray<T, Rank>::~UninitializedArray() {}

template<typename T, size_t Rank>
inline size_t UninitializedArray<T, Rank>::Size() const {
  return Rank;
}

template<typename T, size_t Rank>
inline bool UninitializedArray<T, Rank>::Contains(T const & _element) const {
  return Contains(memory::address_of(_element));
}

template<typename T, size_t Rank>
inline bool UninitializedArray<T, Rank>::Contains(T const * _element) const {
  auto const offset = OffsetOf(reinterpret_cast<uintptr_t>(_element));
  if (offset % sizeof(T) != 0u) {
    return false;
  }
  auto const index = offset / sizeof(T);
  return index < Rank;
}

template<typename T, size_t Rank>
inline size_t UninitializedArray<T, Rank>::IndexOf(T const & _element) const {
  return IndexOf(memory::address_of(_element));
}

template<typename T, size_t Rank>
inline size_t UninitializedArray<T, Rank>::IndexOf(T const * _element) const {
  CORE_LIB_ASSERT(Contains(_element));
  auto const offset = OffsetOf(reinterpret_cast<uintptr_t>(_element));
  return static_cast<size_t>(offset / sizeof(T));
}

template<typename T, size_t Rank>
inline T * UninitializedArray<T, Rank>::Get(size_t _index) {
  if (_index < Rank) {
    auto first_element = storage.template Get<T>();
    return first_element + _index;
  }
  return nullptr;
}

template<typename T, size_t Rank>
inline T const * UninitializedArray<T, Rank>::Get(size_t _index) const {
  if (_index < Rank) {
    auto const first_element = storage.template Get<T const>();
    return first_element + _index;
  }
  return nullptr;
}

template<typename T, size_t Rank>
inline T & UninitializedArray<T, Rank>::At(size_t _index) {
  return *Get(_index);
}

template<typename T, size_t Rank>
inline T const & UninitializedArray<T, Rank>::At(size_t _index) const {
  return *Get(_index);
}

template<typename T, size_t Rank>
inline T & UninitializedArray<T, Rank>::operator[](size_t _index) {
  return At(_index);
}

template<typename T, size_t Rank>
inline T const & UninitializedArray<T, Rank>::operator[](size_t _index) const {
  return At(_index);
}

template<typename T, size_t Rank>
inline typename UninitializedArray<T, Rank>::iterator_type
UninitializedArray<T, Rank>::begin() {
  return iterator_type(storage.template Get<T>());
}

template<typename T, size_t Rank>
inline typename UninitializedArray<T, Rank>::const_iterator_type
UninitializedArray<T, Rank>::begin() const {
  return const_iterator_type(storage.template Get<T const>());
}

template<typename T, size_t Rank>
inline typename UninitializedArray<T, Rank>::iterator_type
UninitializedArray<T, Rank>::end() {
  return iterator_type(storage.template Get<T>() + Rank);
}

template<typename T, size_t Rank>
inline typename UninitializedArray<T, Rank>::const_iterator_type
UninitializedArray<T, Rank>::end() const {
  return const_iterator_type(storage.template Get<T const>() + Rank);
}

template<typename T, size_t Rank>
inline ptrdiff_t
UninitializedArray<T, Rank>::OffsetOf(uintptr_t _memory) const {
  uintptr_t first_element =
      reinterpret_cast<uintptr_t>(storage.template Get<T const>());
  return static_cast<ptrdiff_t>(_memory - first_element);
}

}}

#endif
