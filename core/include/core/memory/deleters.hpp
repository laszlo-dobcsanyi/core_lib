#ifndef _CORE_LIB_MEMORY_DELETERS_HPP_
#define _CORE_LIB_MEMORY_DELETERS_HPP_

#include "core/memory/alloc.hpp"
#include "core/memory/aligned_alloc.hpp"

////////////////////////////////////////////////////////////////////////////////
// core memory
////////////////////////////////////////////////////////////////////////////////

namespace core { namespace memory {

/*******************************************************************************
** Free
*******************************************************************************/

template<typename T>
class Free final {
public:
  void operator()(T * _ptr) { memory::free(reinterpret_cast<uintptr_t>(_ptr)); }
};

template<typename T>
class Free<T[]> final {
public:
  void operator()(T * _ptr) { memory::free(reinterpret_cast<uintptr_t>(_ptr)); }
};

/*******************************************************************************
** AlignedFree
*******************************************************************************/

template<typename T>
class AlignedFree final {
public:
  void operator()(T * _ptr) {
    memory::aligned_free(reinterpret_cast<uintptr_t>(_ptr));
  }
};

template<typename T>
class AlignedFree<T[]> final {
public:
  void operator()(T * _ptr) {
    memory::aligned_free(reinterpret_cast<uintptr_t>(_ptr));
  }
};

}}

#endif
