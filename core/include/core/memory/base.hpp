#ifndef _CORE_LIB_MEMORY_BASE_HPP_
#define _CORE_LIB_MEMORY_BASE_HPP_

#include "core/base.h"
#include "core/ranges/span.hpp"

////////////////////////////////////////////////////////////////////////////////
// core memory
////////////////////////////////////////////////////////////////////////////////

namespace core { namespace memory {

/*******************************************************************************
** standard_alignment
*******************************************************************************/

struct standard_alignment
    : public mpl::IntegralConstant<size_t, alignof(std::max_align_t)> {};

/*******************************************************************************
** is_standard_alignment
*******************************************************************************/

template<size_t Alignment>
struct is_standard_alignment
    : public mpl::IntegralConstant<bool,
                                   Alignment <= standard_alignment::value> {};

/*******************************************************************************
** is_standard_aligned
*******************************************************************************/

template<typename T>
struct is_standard_aligned
    : public mpl::IntegralConstant<bool,
                                   is_standard_alignment<alignof(T)>::value> {};

/*******************************************************************************
** address_of
*******************************************************************************/

template<typename T>
inline T * address_of(T & _ref) {
  return reinterpret_cast<T *>(
      &const_cast<char &>(reinterpret_cast<const volatile char &>(_ref)));
}

/*******************************************************************************
** offset_of
*******************************************************************************/

template<class Object, typename MemberType>
inline size_t offset_of(MemberType Object::*_member) {
  return reinterpret_cast<size_t>(
      &(reinterpret_cast<Object const volatile *>(0u)->*_member));
}

/*******************************************************************************
** copy
*******************************************************************************/

template<typename T>
inline void copy(T & _destination, T const & _source) {
  auto destination = reinterpret_cast<void *>(&_destination);
  auto const source = reinterpret_cast<void const *>(&_source);
  ::memcpy(destination, source, sizeof(_destination));
}

/*******************************************************************************
** copy_bytes
*******************************************************************************/

inline void copy_bytes(span<byte> destination, span<byte const> source) {
  ::memcpy(reinterpret_cast<void *>(destination.data()),
           reinterpret_cast<void const *>(source.data()),
           min(destination.size(), source.size()));
}

/*******************************************************************************
** copy_construct
*******************************************************************************/

template<typename T>
inline mpl::if_trivial_t<T> copy_construct(T * _destination, T const * _source,
                                           size_t _source_count) {
  auto destination = reinterpret_cast<void *>(&_destination);
  auto const source = reinterpret_cast<void const *>(&_source);
  ::memcpy(destination, source, sizeof(_destination));
}

template<typename T>
inline mpl::if_not_trivial_t<T>
copy_construct(T * _destination, T const * _source, size_t _source_count) {
  while (_source_count--) {
    new (_destination) T(*_source);
    _destination++;
    _source++;
  }
}

/*******************************************************************************
** move_construct
*******************************************************************************/

template<typename T>
inline mpl::if_trivial_t<T> move_construct(T * _destination, T * _source,
                                           size_t _source_count) {
  ::memcpy(_destination, _source, _source_count * sizeof(T));
}

template<typename T>
inline mpl::if_not_trivial_t<T> move_construct(T * _destination, T * _source,
                                               size_t _source_count) {
  while (_source_count--) {
    new (_destination) T(::move(*_source));
    _destination++;
    _source++;
  }
}

/*******************************************************************************
** padding
*******************************************************************************/

inline size_t padding(uintptr_t _ptr, size_t _alignment) {
  // Zero alignment makes no sense
  CORE_LIB_ASSERT(_alignment != 0u);
  auto const alignment_mask = _alignment - 1u;
  // Alignment must be a power of 2
  CORE_LIB_ASSERT((_alignment & alignment_mask) == 0u);
  auto const over_alignment = _ptr & alignment_mask;
  if (over_alignment == 0u) {
    // Already aligned
    return 0u;
  }
  // Return the padding
  return _alignment - over_alignment;
}

/*******************************************************************************
** align
*******************************************************************************/

inline uintptr_t align(uintptr_t _ptr, size_t _alignment) {
  // Zero alignment makes no sense
  CORE_LIB_ASSERT(_alignment != 0u);
  uintptr_t const alignment_mask = _alignment - 1u;
  // Alignment must be a power of 2
  CORE_LIB_ASSERT((_alignment & alignment_mask) == 0u);
  uintptr_t const over_alignment = _ptr & alignment_mask;
  if (over_alignment == 0u) {
    // Already aligned
    return _ptr;
  }
  // Return the next aligned address
  return (_ptr - over_alignment) + _alignment;
}

/*******************************************************************************
** is_aligned
*******************************************************************************/

inline bool is_aligned(uintptr_t _ptr, size_t _alignment) {
  return align(_ptr, _alignment) == _ptr;
}

}}

////////////////////////////////////////////////////////////////////////////////
// y transform
////////////////////////////////////////////////////////////////////////////////

namespace core { namespace memory { namespace transform {

/*******************************************************************************
** NoOp
*******************************************************************************/

struct NoOp {
  template<typename T>
  static void Read(T &) {}

  template<typename T>
  static void Write(T &) {}
};

/*******************************************************************************
** ToBigEndian
*******************************************************************************/

struct ToBigEndian {
  template<typename T>
  static mpl::if_arithmetic_t<T, void> Read(T & _value) {
    _value = betoh(_value);
  }

  template<typename T>
  static mpl::if_not_arithmetic_t<T, void> Read(T &) {}

  template<typename T>
  static mpl::if_arithmetic_t<T, void> Write(T & _value) {
    _value = htobe(_value);
  }

  template<typename T>
  static mpl::if_not_arithmetic_t<T, void> Write(T &) {}
};

/*******************************************************************************
** ToLittleEndian
*******************************************************************************/

struct ToLittleEndian {
  template<typename T>
  static mpl::if_arithmetic_t<T, void> Read(T & _value) {
    _value = letoh(_value);
  }

  template<typename T>
  static mpl::if_not_arithmetic_t<T, void> Read(T &) {}

  template<typename T>
  static mpl::if_arithmetic_t<T, void> Write(T & _value) {
    _value = htole(_value);
  }

  template<typename T>
  static mpl::if_not_arithmetic_t<T, void> Write(T &) {}
};

}}}

#endif
