#ifndef _CORE_LIB_MEMORY_ALIGNED_ALLOC_HPP_
#define _CORE_LIB_MEMORY_ALIGNED_ALLOC_HPP_

#include "core/memory/base.hpp"

////////////////////////////////////////////////////////////////////////////////
// core memory
////////////////////////////////////////////////////////////////////////////////

namespace core { namespace memory {

/*******************************************************************************
** aligned_alloc
*******************************************************************************/

inline uintptr_t aligned_alloc(size_t _alignment, size_t _size) {
#if defined(CORE_LIB_COMPILER_MSVC)
  return reinterpret_cast<uintptr_t>(::_aligned_malloc(_size, _alignment));
#elif defined(CORE_LIB_COMPILER_GCC) || defined(CORE_LIB_COMPILER_CLANG)
  return reinterpret_cast<uintptr_t>(::aligned_alloc(_alignment, _size));
#endif
}

/*******************************************************************************
** aligned_free
*******************************************************************************/

inline void aligned_free(uintptr_t _ptr) {
#if defined(CORE_LIB_COMPILER_MSVC)
  ::_aligned_free(reinterpret_cast<void *>(_ptr));
#elif defined(CORE_LIB_COMPILER_GCC) || defined(CORE_LIB_COMPILER_CLANG)
  ::free(reinterpret_cast<void *>(_ptr));
#endif
}

}}

#endif