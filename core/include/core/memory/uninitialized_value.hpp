#ifndef _CORE_LIB_MEMORY_UNINITIALIZED_VALUE_HPP_
#define _CORE_LIB_MEMORY_UNINITIALIZED_VALUE_HPP_

#include "core/memory/aligned_storage.hpp"

////////////////////////////////////////////////////////////////////////////////
// core memory
////////////////////////////////////////////////////////////////////////////////

namespace core { namespace memory {

/*******************************************************************************
** UninitializedValue
*******************************************************************************/

template<typename T>
class UninitializedValue final {
public:
  UninitializedValue();
  UninitializedValue(UninitializedValue &&) = delete;
  UninitializedValue(UninitializedValue const &) = delete;
  UninitializedValue & operator=(UninitializedValue &&) = delete;
  UninitializedValue & operator=(UninitializedValue const &) = delete;
  ~UninitializedValue();

  T * Get();
  T const * Get() const;

  T * operator->();
  T const * operator->() const;

  T & Value();
  T const & Value() const;

  operator T &();
  operator T const &() const;
  T & operator*();
  T const & operator*() const;

private:
  union {
    AlignedStorage<alignof(T), sizeof(T)> storage;
    T value;
  };
};

}}

////////////////////////////////////////////////////////////////////////////////
// core memory
////////////////////////////////////////////////////////////////////////////////

namespace core { namespace memory {

/*******************************************************************************
** UninitializedValue
*******************************************************************************/

template<typename T>
inline UninitializedValue<T>::UninitializedValue() {}

template<typename T>
inline UninitializedValue<T>::~UninitializedValue() {}

template<typename T>
inline T * UninitializedValue<T>::Get() {
  return storage.template Get<T>();
}

template<typename T>
inline T const * UninitializedValue<T>::Get() const {
  return storage.template Get<T const>();
}

template<typename T>
inline T * UninitializedValue<T>::operator->() {
  return Get();
}

template<typename T>
inline T const * UninitializedValue<T>::operator->() const {
  return Get();
}

template<typename T>
inline T & UninitializedValue<T>::Value() {
  return storage.template As<T>();
}

template<typename T>
inline T const & UninitializedValue<T>::Value() const {
  return storage.template As<T const>();
}

template<typename T>
inline UninitializedValue<T>::operator T &() {
  return Value();
}

template<typename T>
inline UninitializedValue<T>::operator T const &() const {
  return Value();
}

template<typename T>
inline T & UninitializedValue<T>::operator*() {
  return Value();
}

template<typename T>
inline T const & UninitializedValue<T>::operator*() const {
  return Value();
}

}}

#endif