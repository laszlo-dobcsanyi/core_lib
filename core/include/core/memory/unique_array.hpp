#ifndef _CORE_LIB_MEMORY_UNIQUE_ARRAY_HPP_
#define _CORE_LIB_MEMORY_UNIQUE_ARRAY_HPP_

#include "core/iterator/contiguous_iterator.hpp"
#include "core/memory/deleters.hpp"

////////////////////////////////////////////////////////////////////////////////
// core memory
////////////////////////////////////////////////////////////////////////////////

namespace core { namespace memory {

/*******************************************************************************
** UniqueArray
*******************************************************************************/

template<class, class>
class UniqueArray;

/*******************************************************************************
** UniqueArray< T >
*******************************************************************************/

template<class T, class Deleter>
class UniqueArray<T[], Deleter> {
public:
  using value_type = mpl::remove_extent_t<T>;
  using pointer_type = mpl::add_pointer_t<value_type>;
  using const_pointer_type = mpl::add_pointer_t<value_type const>;
  using reference_type = mpl::add_reference_t<value_type>;
  using const_reference_type = mpl::add_reference_t<value_type const>;
  using iterator_type = contiguous_iterator<value_type>;
  using const_iterator_type = contiguous_iterator<value_type const>;

  UniqueArray() = default;
  UniqueArray(std::nullptr_t);
  UniqueArray(T * _ptr, size_t _size);
  UniqueArray(UniqueArray && _other);
  UniqueArray(UniqueArray const &) = delete;
  UniqueArray & operator=(UniqueArray && _other);
  UniqueArray & operator=(UniqueArray const &) = delete;
  ~UniqueArray();

  size_t Size() const { return size; }
  bool IsEmpty() const { return 0u == size; }

  bool IsNull() const { return ptr == nullptr; }
  explicit operator bool() const { return !IsNull(); }

  pointer_type Get() { return ptr; }
  const_pointer_type Get() const { return ptr; }

  reference_type operator[](size_t _index);
  const_reference_type operator[](size_t _index) const;

  iterator_type begin();
  const_iterator_type begin() const;
  iterator_type end();
  const_iterator_type end() const;

  void Reset();
  pointer_type Release();

protected:
  pointer_type ptr = nullptr;
  size_t size = 0u;
};

}}

////////////////////////////////////////////////////////////////////////////////
// core memory
////////////////////////////////////////////////////////////////////////////////

namespace core { namespace memory {

/*******************************************************************************
** UniqueArray
*******************************************************************************/

template<class T, class Deleter>
inline UniqueArray<T[], Deleter>::UniqueArray(std::nullptr_t)
    : UniqueArray(std::nullptr_t(), 0) {}

template<class T, class Deleter>
inline UniqueArray<T[], Deleter>::UniqueArray(T * _ptr, size_t _size)
    : ptr(_ptr)
    , size(_size) {
  CORE_LIB_ASSERT(
      memory::is_aligned(reinterpret_cast<uintptr_t>(_ptr), alignof(T)));
}

template<class T, class Deleter>
inline UniqueArray<T[], Deleter>::UniqueArray(UniqueArray && _other) {
  CORE_LIB_ASSERT(this != &_other);
  ptr = _other.ptr;
  size = _other.size;
  _other.ptr = nullptr;
  _other.size = 0u;
}

template<class T, class Deleter>
inline UniqueArray<T[], Deleter> &
UniqueArray<T[], Deleter>::operator=(UniqueArray && _other) {
  if (this != &_other) {
    if (ptr != nullptr) {
      for (auto current = 0u; current < size; ++current) {
        ptr[current].~T();
      }
      Deleter deleter;
      deleter(ptr);
    }
    ptr = _other.ptr;
    size = _other.size;
    _other.ptr = nullptr;
    _other.size = 0u;
  }
  return *this;
}

template<class T, class Deleter>
inline UniqueArray<T[], Deleter>::~UniqueArray() {
  if (ptr != nullptr) {
    for (auto current = 0u; current < size; ++current) {
      ptr[current].~T();
    }
    Deleter deleter;
    deleter(ptr);
  }
}

template<class T, class Deleter>
inline typename UniqueArray<T[], Deleter>::reference_type
UniqueArray<T[], Deleter>::operator[](size_t _index) {
  CORE_LIB_ASSERT(_index < size);
  return ptr[_index];
}

template<class T, class Deleter>
inline typename UniqueArray<T[], Deleter>::const_reference_type
UniqueArray<T[], Deleter>::operator[](size_t _index) const {
  CORE_LIB_ASSERT(_index < size);
  return ptr[_index];
}

template<class T, class Deleter>
inline typename UniqueArray<T[], Deleter>::iterator_type
UniqueArray<T[], Deleter>::begin() {
  return iterator_type(ptr);
}

template<class T, class Deleter>
inline typename UniqueArray<T[], Deleter>::const_iterator_type
UniqueArray<T[], Deleter>::begin() const {
  return const_iterator_type(ptr);
}

template<class T, class Deleter>
inline typename UniqueArray<T[], Deleter>::iterator_type
UniqueArray<T[], Deleter>::end() {
  return iterator_type(ptr + size);
}

template<class T, class Deleter>
inline typename UniqueArray<T[], Deleter>::const_iterator_type
UniqueArray<T[], Deleter>::end() const {
  return const_iterator_type(ptr + size);
}

template<class T, class Deleter>
inline void UniqueArray<T[], Deleter>::Reset() {
  if (ptr != nullptr) {
    for (auto current = 0u; current < size; ++current) {
      ptr[current].~T();
    }
    Deleter deleter;
    deleter(ptr);
    ptr = nullptr;
    size = 0u;
  }
}

template<class T, class Deleter>
inline typename UniqueArray<T[], Deleter>::pointer_type
UniqueArray<T[], Deleter>::Release() {
  auto result = ptr;
  ptr = nullptr;
  size = 0u;
  return result;
}

}}

////////////////////////////////////////////////////////////////////////////////
// core
////////////////////////////////////////////////////////////////////////////////

namespace core {

/*******************************************************************************
** DefaultUniqueArray
*******************************************************************************/

template<class T>
using DefaultUniqueArray = memory::UniqueArray<T[], memory::Free<T>>;

/*******************************************************************************
** AlignedUniqueArray
*******************************************************************************/

template<class T>
using AlignedUniqueArray = memory::UniqueArray<T[], memory::AlignedFree<T>>;

/*******************************************************************************
** UniqueArray
*******************************************************************************/

template<class T>
using UniqueArray =
    mpl::conditional_t<memory::is_standard_aligned<T>::value,
                       DefaultUniqueArray<T>, AlignedUniqueArray<T>>;

/*******************************************************************************
** make_default_unique_array
*******************************************************************************/

template<class T, typename... Args>
inline DefaultUniqueArray<T> make_default_unique_array(size_t _size,
                                                       Args &&... _args) {
  if (0u < _size) {
    auto const allocation_size = sizeof(T) * _size;
    // TODO: Check overflow?
    auto allocation = reinterpret_cast<T *>(memory::alloc(allocation_size));
    // TODO: Check allocation result?
    for (auto current = 0u; current < _size; ++current) {
      auto ptr = allocation + current;
      new (ptr) T(forward<Args>(_args)...);
    }
    return DefaultUniqueArray<T>(allocation, _size);
  }
  return DefaultUniqueArray<T>();
}

/*******************************************************************************
** make_aligned_unique_array
*******************************************************************************/

template<class T, typename... Args>
inline AlignedUniqueArray<T> make_aligned_unique_array(size_t _size,
                                                       Args &&... _args) {
  if (0u < _size) {
    auto const allocation_size = sizeof(T) * _size;
    // TODO: Check overflow?
    auto allocation = reinterpret_cast<T *>(
        memory::aligned_alloc(alignof(T), allocation_size));
    // TODO: Check allocation result?
    for (auto current = 0u; current < _size; ++current) {
      auto ptr = allocation + current;
      new (ptr) T(forward<Args>(_args)...);
    }
    return AlignedUniqueArray<T>(allocation, _size);
  }
  return AlignedUniqueArray<T>();
}

/*******************************************************************************
** make_unique_array
*******************************************************************************/

template<class T, typename... Args>
inline mpl::enable_if_t<memory::is_standard_aligned<T>::value,
                        DefaultUniqueArray<T>>
make_unique_array(size_t _size, Args &&... _args) {
  return make_default_unique_array<T>(_size, forward<Args>(_args)...);
}

template<class T, typename... Args>
inline mpl::enable_if_t<!memory::is_standard_aligned<T>::value,
                        AlignedUniqueArray<T>>
make_unique_array(size_t _size, Args &&... _args) {
  return make_aligned_unique_array<T>(_size, forward<Args>(_args)...);
}

}

#endif
