#ifndef _CORE_LIB_VARIANT_HPP_
#define _CORE_LIB_VARIANT_HPP_

#include "core/base.h"
#include "core/memory/aligned_storage.hpp"

////////////////////////////////////////////////////////////////////////////////
// core
////////////////////////////////////////////////////////////////////////////////

namespace core {

/*******************************************************************************
** Variant
*******************************************************************************/

template<typename... Variants>
class Variant {
public:
  // TODO: Store type index in smaller type
  using IndexType = uint8;
  CORE_LIB_STATIC_ASSERT(sizeof...(Variants) <= 255);
  using VariantsPack = mpl::TypePack<Variants...>;

private:
  CORE_LIB_STATIC_ASSERT(mpl::is_typepack_unique<VariantsPack>::value);
  CORE_LIB_STATIC_ASSERT(1 < mpl::sizeof_typepack<VariantsPack>::value);

  memory::AlignedStorage<mpl::alignment_of_union_typepack<VariantsPack>::value,
                         mpl::sizeof_union_typepack<VariantsPack>::value>
      storage;
  IndexType type_index;

  Variant() = delete;

public:
  template<typename Type, typename... Args>
  static Variant Create(Args &&... _args);

  template<typename Type, typename... Args>
  Variant(in_place_t<Type>, Args &&... _args);
  Variant(Variant && _other);
  Variant(Variant const & _other);
  Variant & operator=(Variant && _other);
  Variant & operator=(Variant const & _other);
  ~Variant();

  template<typename Type>
  bool Is() const;

  template<typename Type>
  Type * Get();
  template<typename Type>
  Type const * Get() const;
  template<typename Type>
  Type & As();
  template<typename Type>
  Type const & As() const;

  template<typename Visitor, typename... Args>
  decltype(auto) Apply(Visitor && _visitor, Args &&... _args);
  template<typename Visitor, typename... Args>
  decltype(auto) Apply(Visitor && _visitor, Args &&... _args) const;
};

}

////////////////////////////////////////////////////////////////////////////////
// core
////////////////////////////////////////////////////////////////////////////////

namespace core {

/*******************************************************************************
** Visit
*******************************************************************************/

template<typename Visitor, typename T, typename... Args>
inline decltype(auto) call_visitor(Visitor && _visitor, void * _value,
                                   Args &&... _args) {
  return _visitor(*reinterpret_cast<T *>(_value), forward<Args>(_args)...);
}

template<typename Visitor, typename T, typename... Args>
inline decltype(auto)
call_const_visitor(Visitor && _visitor, void const * _value, Args &&... _args) {
  return _visitor(*reinterpret_cast<T const *>(_value),
                  forward<Args>(_args)...);
}

template<typename... Types>
struct Visit {
  template<typename Visitor, typename... Args>
  inline decltype(auto) operator()(size_t _type_index, void * _storage,
                                   Visitor && _visitor, Args &&... _args) {
    // TODO: Remove this  std::common_type!
    using VisitorResult = typename std::common_type<decltype(
        call_visitor<Visitor, Types, Args &&...>(
            forward<Visitor>(_visitor), _storage,
            forward<Args>(_args)...))...>::type;
    using VisitorSignature = VisitorResult (*)(Visitor &&, void *, Args &&...);
    static VisitorSignature
        visitor_stubs[mpl::sizeof_typepack<mpl::TypePack<Types...>>::value] = {
            &call_visitor<Visitor, Types, Args &&...>...};
    CORE_LIB_ASSERT(_type_index <
                    mpl::sizeof_typepack<mpl::TypePack<Types...>>::value);
    return (*visitor_stubs[_type_index])(forward<Visitor>(_visitor), _storage,
                                         forward<Args>(_args)...);
  }
};

template<typename... Types>
struct ConstVisit {
  template<typename Visitor, typename... Args>
  inline decltype(auto) operator()(size_t _type_index, void const * _storage,
                                   Visitor && _visitor, Args &&... _args) {
    // TODO: Remove this  std::common_type!
    using ConstVisitorResult = typename std::common_type<decltype(
        call_const_visitor<Visitor, Types, Args &&...>(
            forward<Visitor>(_visitor), _storage,
            forward<Args>(_args)...))...>::type;
    using ConstVisitorSignature =
        ConstVisitorResult (*)(Visitor &&, void const *, Args &&...);
    static ConstVisitorSignature const_visitor_stubs
        [mpl::sizeof_typepack<mpl::TypePack<Types...>>::value] = {
            &call_const_visitor<Visitor, Types, Args &&...>...};
    CORE_LIB_ASSERT(_type_index <
                    mpl::sizeof_typepack<mpl::TypePack<Types...>>::value);
    return (*const_visitor_stubs[_type_index])(
        forward<Visitor>(_visitor), _storage, forward<Args>(_args)...);
  }
};

struct default_construct {
  template<typename Type>
  inline void operator()(Type & _value) {
    new (&_value) Type();
  }
};

struct rvalue_construct {
  template<typename Type>
  inline void operator()(Type & _value, void * _other_storage) {
    new (&_value) Type(move(*reinterpret_cast<Type *>(_other_storage)));
  }
};

struct lvalue_construct {
  template<typename Type>
  inline void operator()(Type & _value, void const * _other_storage) {
    new (&_value) Type(*reinterpret_cast<Type const *>(_other_storage));
  }
};

struct rvalue_assign {
  template<typename Type>
  inline void operator()(Type & _value, void * _other_storage) {
    _value = move(*reinterpret_cast<Type *>(_other_storage));
  }
};

struct lvalue_assign {
  template<typename Type>
  inline void operator()(Type & _value, void const * _other_storage) {
    _value = *reinterpret_cast<const Type *>(_other_storage);
  }
};

struct destructor {
  template<typename Type>
  inline void operator()(Type & _value) {
    _value.~Type();
  }
};

/*******************************************************************************
** Variant
*******************************************************************************/

template<typename... Variants>
template<typename Type, typename... Args>
inline Variant<Variants...> Variant<Variants...>::Create(Args &&... _args) {
  return Variant<Variants...>(in_place_t<Type>(forward<Args>(_args)...));
}

template<typename... Variants>
template<typename Type, typename... Args>
inline Variant<Variants...>::Variant(in_place_t<Type>, Args &&... _args) {
  auto index = mpl::index_of_type_in_pack<Type, VariantsPack>::value;
  type_index = static_cast<IndexType>(index);
  auto typed_storage = storage.template Get<Type>();
  new (typed_storage) Type(forward<Args>(_args)...);
}

template<typename... Variants>
inline Variant<Variants...>::Variant(Variant && _other) {
  CORE_LIB_ASSERT(this != &_other);
  type_index = _other.type_index;
  Apply(rvalue_construct(), reinterpret_cast<void *>(&_other.storage));
}

template<typename... Variants>
inline Variant<Variants...>::Variant(Variant const & _other) {
  CORE_LIB_ASSERT(this != &_other);
  type_index = _other.type_index;
  Apply(lvalue_construct(), reinterpret_cast<void const *>(&_other.storage));
}

template<typename... Variants>
inline Variant<Variants...> &
Variant<Variants...>::operator=(Variant && _other) {
  CORE_LIB_ASSERT(this != &_other);
  if (_other.type_index != type_index) {
    Apply(destructor());
    type_index = _other.type_index;
    // TODO: What if we can't default construct T?
    Apply(default_construct());
  }
  Apply(rvalue_assign(), reinterpret_cast<void *>(&_other.storage));
  return *this;
}

template<typename... Variants>
inline Variant<Variants...> &
Variant<Variants...>::operator=(Variant const & _other) {
  if (this != &_other) {
    if (_other.type_index != type_index) {
      Apply(destructor());
      type_index = _other.type_index;
      // TODO: What if we can't default construct T?
      Apply(default_construct());
    }
    Apply(lvalue_assign(), reinterpret_cast<void const *>(&_other.storage));
  }
  return *this;
}

template<typename... Variants>
inline Variant<Variants...>::~Variant() {
  Apply(destructor());
}

template<typename... Variants>
template<typename Type>
inline bool Variant<Variants...>::Is() const {
  return mpl::index_of_type_in_pack<Type, VariantsPack>::value == type_index;
}

template<typename... Variants>
template<typename Type>
inline Type * Variant<Variants...>::Get() {
  if (Is<Type>()) {
    return storage.template Get<Type>();
  }
  return nullptr;
}

template<typename... Variants>
template<typename Type>
inline Type const * Variant<Variants...>::Get() const {
  if (Is<Type>()) {
    return storage.template Get<Type>();
  }
  return nullptr;
}

template<typename... Variants>
template<typename Type>
inline Type & Variant<Variants...>::As() {
  CORE_LIB_ASSERT(Is<Type>());
  return storage.template As<Type>();
}

template<typename... Variants>
template<typename Type>
inline Type const & Variant<Variants...>::As() const {
  CORE_LIB_ASSERT(Is<Type>());
  return storage.template As<Type>();
}

template<typename... Variants>
template<typename Visitor, typename... Args>
inline decltype(auto) Variant<Variants...>::Apply(Visitor && _visitor,
                                                  Args &&... _arguments) {
  return Visit<Variants...>()(type_index, reinterpret_cast<void *>(&storage),
                              forward<Visitor>(_visitor),
                              forward<Args>(_arguments)...);
}

template<typename... Variants>
template<typename Visitor, typename... Args>
inline decltype(auto) Variant<Variants...>::Apply(Visitor && _visitor,
                                                  Args &&... _arguments) const {
  return ConstVisit<Variants...>()(
      type_index, reinterpret_cast<void const *>(&storage),
      forward<Visitor>(_visitor), forward<Args>(_arguments)...);
}

}

#endif
