#ifndef _CORE_LIB_ENDIAN_H_
#define _CORE_LIB_ENDIAN_H_

inline int8 byte_swap(int8 _value) { return _value; }
inline uint8 byte_swap(uint8 _value) { return _value; }

#if defined(CORE_LIB_COMPILER_MSVC)
#  include <stdlib.h>
#  define htobe16(__value__) _byteswap_ushort(__value__)
#  define htole16(__value__) (__value__)
#  define be16toh(__value__) _byteswap_ushort(__value__)
#  define le16toh(__value__) (__value__)

#  define htobe32(__value__) _byteswap_ulong(__value__)
#  define htole32(__value__) (__value__)
#  define be32toh(__value__) _byteswap_ulong(__value__)
#  define le32toh(__value__) (__value__)

#  define htobe64(__value__) _byteswap_uint64(__value__)
#  define htole64(__value__) (__value__)
#  define be64toh(__value__) _byteswap_uint64(__value__)
#  define le64toh(__value__) (__value__)

inline int16 byte_swap(int16 _value) { return _byteswap_ushort(_value); }
inline uint16 byte_swap(uint16 _value) { return _byteswap_ushort(_value); }
inline int32 byte_swap(int32 _value) { return _byteswap_ulong(_value); }
inline uint32 byte_swap(uint32 _value) { return _byteswap_ulong(_value); }
inline int64 byte_swap(int64 _value) { return _byteswap_uint64(_value); }
inline uint64 byte_swap(uint64 _value) { return _byteswap_uint64(_value); }
#else
#  include "endian.h"
#  include <arpa/inet.h>

inline int16 byte_swap(int16 _value) { return __builtin_bswap16(_value); }
inline uint16 byte_swap(uint16 _value) { return __builtin_bswap16(_value); }
inline int32 byte_swap(int32 _value) { return __builtin_bswap32(_value); }
inline uint32 byte_swap(uint32 _value) { return __builtin_bswap32(_value); }
inline int64 byte_swap(int64 _value) { return __builtin_bswap64(_value); }
inline uint64 byte_swap(uint64 _value) { return __builtin_bswap64(_value); }
#endif

inline int8 htobe(int8 _value) { return _value; }
inline uint8 htobe(uint8 _value) { return _value; }
inline int16 htobe(int16 _value) { return htobe16(_value); }
inline uint16 htobe(uint16 _value) { return htobe16(_value); }
inline int32 htobe(int32 _value) { return htobe32(_value); }
inline uint32 htobe(uint32 _value) { return htobe32(_value); }
inline int64 htobe(int64 _value) { return htobe64(_value); }
inline uint64 htobe(uint64 _value) { return htobe64(_value); }
inline float htobe(float _value) {
  CORE_LIB_GCC_WARNING_PUSH
  CORE_LIB_GCC_WARNING_DISABLE("-Wstrict-aliasing")
  //  This violation of strict aliasing is fine on x86
  auto result = htobe32(*reinterpret_cast<int32 *>(&_value));
  return *reinterpret_cast<float *>(&result);
  CORE_LIB_GCC_WARNING_POP
}
inline double htobe(double _value) {
  CORE_LIB_GCC_WARNING_PUSH
  CORE_LIB_GCC_WARNING_DISABLE("-Wstrict-aliasing")
  //  This violation of strict aliasing is fine on x86
  auto result = htobe64(*reinterpret_cast<int64 *>(&_value));
  return *reinterpret_cast<double *>(&result);
  CORE_LIB_GCC_WARNING_POP
}

inline int8 betoh(int8 _value) { return _value; }
inline uint8 betoh(uint8 _value) { return _value; }
inline int16 betoh(int16 _value) { return be16toh(_value); }
inline uint16 betoh(uint16 _value) { return be16toh(_value); }
inline int32 betoh(int32 _value) { return be32toh(_value); }
inline uint32 betoh(uint32 _value) { return be32toh(_value); }
inline int64 betoh(int64 _value) { return be64toh(_value); }
inline uint64 betoh(uint64 _value) { return be64toh(_value); }
inline float betoh(float _value) {
  CORE_LIB_GCC_WARNING_PUSH
  CORE_LIB_GCC_WARNING_DISABLE("-Wstrict-aliasing")
  //  This violation of strict aliasing is fine on x86
  auto result = be32toh(*reinterpret_cast<int32 *>(&_value));
  return *reinterpret_cast<float *>(&result);
  CORE_LIB_GCC_WARNING_POP
}
inline double betoh(double _value) {
  CORE_LIB_GCC_WARNING_PUSH
  CORE_LIB_GCC_WARNING_DISABLE("-Wstrict-aliasing")
  //  This violation of strict aliasing is fine on x86
  auto result = be64toh(*reinterpret_cast<int64 *>(&_value));
  return *reinterpret_cast<double *>(&result);
  CORE_LIB_GCC_WARNING_POP
}

inline int8 htole(int8 _value) { return _value; }
inline uint8 htole(uint8 _value) { return _value; }
inline int16 htole(int16 _value) { return htole16(_value); }
inline uint16 htole(uint16 _value) { return htole16(_value); }
inline int32 htole(int32 _value) { return htole32(_value); }
inline uint32 htole(uint32 _value) { return htole32(_value); }
inline int64 htole(int64 _value) { return htole64(_value); }
inline uint64 htole(uint64 _value) { return htole64(_value); }
inline float htole(float _value) {
  CORE_LIB_GCC_WARNING_PUSH
  CORE_LIB_GCC_WARNING_DISABLE("-Wstrict-aliasing")
  //  This violation of strict aliasing is fine on x86
  auto result = htole32(*reinterpret_cast<int32 *>(&_value));
  return *reinterpret_cast<float *>(&result);
  CORE_LIB_GCC_WARNING_POP
}
inline double htole(double _value) {
  CORE_LIB_GCC_WARNING_PUSH
  CORE_LIB_GCC_WARNING_DISABLE("-Wstrict-aliasing")
  //  This violation of strict aliasing is fine on x86
  auto result = htole64(*reinterpret_cast<int64 *>(&_value));
  return *reinterpret_cast<double *>(&result);
  CORE_LIB_GCC_WARNING_POP
}

inline int8 letoh(int8 _value) { return _value; }
inline uint8 letoh(uint8 _value) { return _value; }
inline int16 letoh(int16 _value) { return le16toh(_value); }
inline uint16 letoh(uint16 _value) { return le16toh(_value); }
inline int32 letoh(int32 _value) { return le32toh(_value); }
inline uint32 letoh(uint32 _value) { return le32toh(_value); }
inline int64 letoh(int64 _value) { return le64toh(_value); }
inline uint64 letoh(uint64 _value) { return le64toh(_value); }
inline float letoh(float _value) {
  CORE_LIB_GCC_WARNING_PUSH
  CORE_LIB_GCC_WARNING_DISABLE("-Wstrict-aliasing")
  //  This violation of strict aliasing is fine on x86
  auto result = le32toh(*reinterpret_cast<int32 *>(&_value));
  return *reinterpret_cast<float *>(&result);
  CORE_LIB_GCC_WARNING_POP
}
inline double letoh(double _value) {
  CORE_LIB_GCC_WARNING_PUSH
  CORE_LIB_GCC_WARNING_DISABLE("-Wstrict-aliasing")
  //  This violation of strict aliasing is fine on x86
  auto result = le64toh(*reinterpret_cast<int64 *>(&_value));
  return *reinterpret_cast<double *>(&result);
  CORE_LIB_GCC_WARNING_POP
}

#endif
