#ifndef _CORE_LIB_DEPENDENCIES_H_
#define _CORE_LIB_DEPENDENCIES_H_

// OS Specific dependencies
#if defined(CORE_LIB_OS_LINUX)
#  include <unistd.h>
#  include <strings.h>     // ffs
#  include <sys/syscall.h> // ::syscall( SYS_gettid )
#elif defined(CORE_LIB_OS_WINDOWS)
#  define WIN32_LEAN_AND_MEAN 1
#  define NOMINMAX
#  define _WIN32_WINNT _WIN32_WINNT_WIN7
#  include <windows.h>
#  undef GetObject
#  undef ERROR
#endif

// Standard dependencies
#include <cstdio>
#include <cstring>
#include <cstddef>
#include <cstdint>
#include <cinttypes>
#include <cstdarg>
#include <cstdlib>
#include <thread>
#include <chrono>
#include <type_traits>
#include <tuple>

using nullptr_t = std::nullptr_t;

#include <memory>
template<typename T>
using SharedPtr = std::shared_ptr<T>;

#include <atomic>
template<typename T>
using Atomic = std::atomic<T>;

#include <mutex>
using Mutex = std::mutex;

template<typename T = Mutex>
using LockGuard = std::lock_guard<T>;
template<typename T = Mutex>
using UniqueLock = std::unique_lock<T>;

#include <condition_variable>
using ConditionVariable = std::condition_variable;

#endif
