#ifndef _CORE_LIB_TYPES_H_
#define _CORE_LIB_TYPES_H_

using int8 = int_least8_t;
using int16 = int_least16_t;
using int32 = int_least32_t;
using int64 = int_least64_t;

using uint8 = uint_least8_t;
using uint16 = uint_least16_t;
using uint32 = uint_least32_t;
using uint64 = uint_least64_t;

#define I8(value) static_cast<int8>(value)
#define U8(value) static_cast<uint8>(value)
#define I16(value) static_cast<int16>(value)
#define U16(value) static_cast<uint16>(value)
#define I32(value) static_cast<int32>(value)
#define U32(value) static_cast<uint32>(value)
#define I64(value) static_cast<int64>(value)
#define U64(value) static_cast<uint64>(value)

using byte = uint8;

/*******************************************************************************
** in_place_t
*******************************************************************************/

template<typename T = void>
struct in_place_t {
  constexpr explicit in_place_t() = default;
};

constexpr in_place_t<void> in_place;

#endif
