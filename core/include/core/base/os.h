#ifndef _CORE_LIB_OS_H_
#define _CORE_LIB_OS_H_

#if defined(_WIN64) || defined(_WIN32)
#  define CORE_LIB_OS_WINDOWS
#elif defined(__linux)
#  define CORE_LIB_OS_LINUX
#else
#  error Unknown OS.
#endif

#endif
