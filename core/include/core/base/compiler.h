#ifndef _CORE_LIB_COMPILER_H_
#define _CORE_LIB_COMPILER_H_

// Compiler
#if defined(__clang__)
#  define CORE_LIB_COMPILER_CLANG
#elif defined(__GNUC__) || defined(__GNUG__)
#  define CORE_LIB_COMPILER_GCC
#elif defined(_MSC_VER)
#  define CORE_LIB_COMPILER_MSVC
#else
#  error Unknown compiler.
#endif

// Arch
#if defined(CORE_LIB_COMPILER_CLANG)
#  if defined(__x86_64__) || defined(__ppc64__)
#    define CORE_LIB_ARCH_64
#  else
#    define CORE_LIB_ARCH_32
#  endif
#elif defined(CORE_LIB_COMPILER_GCC)
#  if defined(__x86_64__) || defined(__ppc64__)
#    define CORE_LIB_ARCH_64
#  else
#    define CORE_LIB_ARCH_32
#  endif
#elif defined(CORE_LIB_COMPILER_MSVC)
#  if defined(_M_X64)
#    define CORE_LIB_ARCH_64
#  else
#    define CORE_LIB_ARCH_32
#  endif
#endif

#endif
