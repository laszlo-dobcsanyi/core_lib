#ifndef _CORE_LIB_BASE_MPL_HPP_
#define _CORE_LIB_BASE_MPL_HPP_

////////////////////////////////////////////////////////////////////////////////
// mpl
////////////////////////////////////////////////////////////////////////////////

namespace mpl {

// IntegralConstant
template<class T, T _value>
struct IntegralConstant {
  static const T value = _value;
  using ValueType = T;
  operator ValueType() const noexcept { return value; }
};

using false_t = IntegralConstant<bool, false>;
using true_t = IntegralConstant<bool, true>;

// type
template<typename T>
struct type {};

// void_t
template<typename...>
using void_t = void;

// enable_if
template<bool, class T = void>
struct enable_if {};

template<typename ResultType>
struct enable_if<true, ResultType> {
  using type = ResultType;
};

template<bool Condition, class ResultType = void>
using enable_if_t = typename enable_if<Condition, ResultType>::type;

// disable_if
template<bool, class T = void>
struct disable_if {};

template<typename ResultType>
struct disable_if<false, ResultType> {
  using type = ResultType;
};

template<bool Condition, class ResultType = void>
using disable_if_t = typename disable_if<Condition, ResultType>::type;

// conditional
template<bool, typename, typename>
struct conditional;

template<typename TrueType, typename FalseType>
struct conditional<true, TrueType, FalseType> {
  using type = TrueType;
};

template<typename TrueType, typename FalseType>
struct conditional<false, TrueType, FalseType> {
  using type = FalseType;
};

template<bool Condition, typename TrueType, typename FalseType>
using conditional_t =
    typename conditional<Condition, TrueType, FalseType>::type;

// is_same
template<class T1, class T2>
struct is_same : public false_t {};

template<class T1>
struct is_same<T1, T1> : public true_t {};

// if_same_t
template<class T1, class T2, typename ResultType = void>
using if_same_t = enable_if_t<is_same<T1, T2>::value, ResultType>;

// if_not_same_t
template<class T1, class T2, typename ResultType = void>
using if_not_same_t = enable_if_t<!is_same<T1, T2>::value, ResultType>;

/*******************************************************************************
** Void
*******************************************************************************/

// is_void
template<typename T>
struct is_void : public false_t {};

template<>
struct is_void<void> : public true_t {};

// if_void
template<typename T, typename ResultType = void>
using if_void = enable_if<is_void<T>::value, ResultType>;

// if_void_t
template<typename T, typename ResultType = void>
using if_void_t = enable_if_t<is_void<T>::value, ResultType>;

// if_not_void
template<typename T, typename ResultType = void>
using if_not_void = enable_if<!is_void<T>::value, ResultType>;

// if_not_void_t
template<typename T, typename ResultType = void>
using if_not_void_t = enable_if_t<!is_void<T>::value, ResultType>;

/*******************************************************************************
** Const
*******************************************************************************/

// is_const
template<typename T>
struct is_const : public false_t {};

template<typename T>
struct is_const<T const> : public true_t {};

// if_const
template<typename T, typename ResultType = void>
using if_const = enable_if_t<is_const<T>::value, ResultType>;

// if_not_const
template<typename T, typename ResultType = void>
using if_not_const = enable_if_t<!is_const<T>::value, ResultType>;

// add_const_t
template<typename T>
using add_const_t = T const;

// remove_const
template<typename T>
struct remove_const {
  using type = T;
};

template<typename T>
struct remove_const<T const> {
  using type = T;
};

template<typename T>
using remove_const_t = typename remove_const<T>::type;

/*******************************************************************************
** Pointer
*******************************************************************************/

// is_pointer
template<typename T>
struct is_pointer : public false_t {};

template<typename T>
struct is_pointer<T *> : public true_t {};

// if_pointer_t
template<typename T, typename ResultType = void>
using if_pointer_t = enable_if_t<is_pointer<T>::value, ResultType>;

// if_not_pointer_t
template<typename T, typename ResultType = void>
using if_not_pointer_t = enable_if_t<!is_pointer<T>::value, ResultType>;

// add_pointer
template<typename T>
struct add_pointer {
  using type = T *;
};

// add_pointer_t
template<typename T>
using add_pointer_t = typename add_pointer<T>::type;

// remove_pointer
template<class T>
struct remove_pointer {
  using type = T;
};

template<class T>
struct remove_pointer<T *> {
  using type = T;
};

template<class T>
using remove_pointer_t = typename remove_pointer<T>::type;

/*******************************************************************************
** Reference
*******************************************************************************/

// is_lvalue_reference
template<class T>
struct is_lvalue_reference : public false_t {};

template<class T>
struct is_lvalue_reference<T &> : public true_t {};

// is_rvalue_reference
template<class T>
struct is_rvalue_reference : public false_t {};

template<class T>
struct is_rvalue_reference<T &&> : public true_t {};

// is_reference
template<typename T>
using is_reference = is_lvalue_reference<T>;

// if_reference_t
template<typename T, typename ResultType = void>
using if_reference_t = enable_if_t<is_reference<T>::value, ResultType>;

// if_not_reference_t
template<typename T, typename ResultType = void>
using if_not_reference_t = enable_if_t<!is_reference<T>::value, ResultType>;

// add_reference_t
template<typename T>
using add_reference_t = T &;

// remove_reference
template<class T>
struct remove_reference {
  using type = T;
};

template<class T>
struct remove_reference<T &> {
  using type = T;
};

template<class T>
struct remove_reference<T &&> {
  using type = T;
};

template<class T>
using remove_reference_t = typename remove_reference<T>::type;

/*******************************************************************************
** Array
*******************************************************************************/

// is_array
template<class T>
struct is_array : public false_t {};

template<class T>
struct is_array<T[]> : public true_t {};

template<class T, size_t Size>
struct is_array<T[Size]> : public true_t {};

// if_array_t
template<typename T, typename ResultType = void>
using if_array_t = enable_if_t<is_array<T>::value, ResultType>;

// if_not_array_t
template<typename T, typename ResultType = void>
using if_not_array_t = enable_if_t<!is_array<T>::value, ResultType>;

// remove_extent_t
namespace detail {

template<class T>
struct RemoveExtent {
  using type = T;
};

template<class T>
struct RemoveExtent<T[]> {
  using type = T;
};

template<class T, size_t Size>
struct RemoveExtent<T[Size]> {
  using type = T;
};

}

template<typename T>
using remove_extent_t = typename detail::RemoveExtent<T>::type;

// array_elements
template<typename>
struct array_elements;

template<typename T, size_t N>
struct array_elements<T[N]> : public IntegralConstant<size_t, N> {};

/*******************************************************************************
** Type traits
*******************************************************************************/

// is_integral
template<typename T>
struct is_integral : public false_t {};
template<>
struct is_integral<bool> : public true_t {};
template<>
struct is_integral<char> : public true_t {};
template<>
struct is_integral<int8> : public true_t {};
template<>
struct is_integral<uint8> : public true_t {};
template<>
struct is_integral<int16> : public true_t {};
template<>
struct is_integral<uint16> : public true_t {};
template<>
struct is_integral<int32> : public true_t {};
template<>
struct is_integral<uint32> : public true_t {};
template<>
struct is_integral<int64> : public true_t {};
template<>
struct is_integral<uint64> : public true_t {};

// if_integral
template<typename T, typename ResultType = void>
using if_integral = enable_if<is_integral<T>::value, ResultType>;

template<typename T, typename ResultType = void>
using if_integral_t = typename if_integral<T, ResultType>::type;

// is_floating
template<typename T>
struct is_floating : public false_t {};
template<>
struct is_floating<float> : public true_t {};
template<>
struct is_floating<double> : public true_t {};
template<>
struct is_floating<long double> : public true_t {};

// if_floating
template<typename T, typename ResultType = void>
using if_floating = enable_if<is_floating<T>::value, ResultType>;

template<typename T, typename ResultType = void>
using if_floating_t = typename if_floating<T, ResultType>::type;

// is_arithmetic
template<typename T>
struct is_arithmetic
    : public IntegralConstant<bool, is_integral<T>::value ||
                                        is_floating<T>::value> {};

// if_arithmetic
template<typename T, typename ResultType = void>
using if_arithmetic = enable_if<is_arithmetic<T>::value, ResultType>;

template<typename T, typename ResultType = void>
using if_arithmetic_t = typename if_arithmetic<T, ResultType>::type;

// if_not_arithmetic
template<typename T, typename ResultType = void>
using if_not_arithmetic = enable_if<!is_arithmetic<T>::value, ResultType>;

template<typename T, typename ResultType = void>
using if_not_arithmetic_t = typename if_not_arithmetic<T, ResultType>::type;

// is_signed
template<typename T>
struct is_signed : public false_t {};
template<>
struct is_signed<int8> : public true_t {};
template<>
struct is_signed<int16> : public true_t {};
template<>
struct is_signed<int32> : public true_t {};
template<>
struct is_signed<int64> : public true_t {};
template<>
struct is_signed<float> : public true_t {};
template<>
struct is_signed<double> : public true_t {};

// if_signed
template<typename T, typename ResultType = void>
using if_signed = enable_if<is_signed<T>::value, ResultType>;

template<typename T, typename ResultType = void>
using if_signed_t = typename if_signed<T, ResultType>::type;

// is_unsigned
template<typename T>
struct is_unsigned : public false_t {};
template<>
struct is_unsigned<uint8> : public true_t {};
template<>
struct is_unsigned<uint16> : public true_t {};
template<>
struct is_unsigned<uint32> : public true_t {};
template<>
struct is_unsigned<uint64> : public true_t {};

// if_unsigned
template<typename T, typename ResultType = void>
using if_unsigned = enable_if<is_unsigned<T>::value, ResultType>;

template<typename T, typename ResultType = void>
using if_unsigned_t = typename if_unsigned<T, ResultType>::type;

// if_trivial
template<typename T, typename ResultType = void>
using if_trivial_t = enable_if_t<std::is_trivial<T>::value, ResultType>;

// if_not_trivial
template<typename T, typename ResultType = void>
using if_not_trivial_t = enable_if_t<!std::is_trivial<T>::value, ResultType>;

// min
template<typename T, T Value1, T Value2>
struct min : public IntegralConstant<T, (Value1 < Value2 ? Value1 : Value2)> {};

// max
template<typename T, T Value1, T Value2>
struct max : public IntegralConstant<T, (Value2 < Value1 ? Value1 : Value2)> {};

// less
template<typename Type, Type Value1, Type Value2>
struct less : public IntegralConstant<bool, (Value1 < Value2)> {};

// less_equal
template<typename Type, Type Value1, Type Value2>
struct less_equal : public IntegralConstant<bool, (Value1 <= Value2)> {};

// equal
template<typename Type, Type Value1, Type Value2>
struct equal : public IntegralConstant<bool, (Value1 == Value2)> {};

// enum_value
template<typename T>
constexpr size_t enum_value(T _value) {
  return static_cast<const size_t>(_value);
}

// bit_count
template<typename T>
struct bit_count : public IntegralConstant<size_t, sizeof(T) * 8u> {};

// power_of_2
template<size_t Rank>
struct power_of_2
    : public IntegralConstant<size_t,
                              (::mpl::power_of_2<Rank - 1>::value << 1)> {
  CORE_LIB_STATIC_ASSERT_TEXT(Rank < ::mpl::bit_count<size_t>::value,
                              "Storage limit reached!");
};

template<>
struct power_of_2<0u> : public IntegralConstant<size_t, 1u> {};

// base_of_2
template<size_t Value>
struct base_of_2
    : public IntegralConstant<size_t,
                              (::mpl::base_of_2<(Value >> 1)>::value + 1)> {};

template<>
struct base_of_2<1u> : public IntegralConstant<size_t, 0u> {};

// full_bit_field
template<size_t Bits, class Enabled = void>
struct full_bit_field;

template<size_t Bits>
struct full_bit_field<Bits, enable_if_t<equal<size_t, Bits, 0>::value>>
    : public IntegralConstant<size_t, 0> {};

template<size_t Bits>
struct full_bit_field<Bits, enable_if_t<less_equal<size_t, Bits, 32>::value &&
                                        !equal<size_t, Bits, 0>::value>>
    : public IntegralConstant<
          size_t, (::mpl::full_bit_field<Bits - 1>::value << 1) + 1u> {};

template<size_t Bits>
struct full_bit_field<Bits, enable_if_t<!less_equal<size_t, Bits, 32>::value &&
                                        less_equal<size_t, Bits, 64>::value>>
    : public IntegralConstant<
          unsigned long long,
          ((::mpl::full_bit_field<Bits - 1, void>::value) << 1) + 1u> {};

// is_standard_layout
template<typename T>
using is_standard_layout = typename std::is_standard_layout<T>::type;

// is_base_of
namespace detail {

using yes = char (&)[1];
using no = char (&)[2];

template<typename B, typename D>
struct Host {
  operator B *() const;
  operator D *();
};

}

template<typename B, typename D>
struct is_base_of {
  template<typename T>
  static detail::yes check(D *, T);
  static detail::no check(B *, int);

  static const bool value =
      sizeof(check(detail::Host<B, D>(), int())) == sizeof(detail::yes);
};

/*******************************************************************************
** Conversion
*******************************************************************************/

class Mutable;
class Const;

// pointer_of
template<typename, class>
struct pointer_of;

template<typename T>
struct pointer_of<T, Mutable> {
  using type = T *;
};

template<typename T>
struct pointer_of<T, Const> {
  using type = T const *;
};

template<typename T, class Mutability>
using pointer_of_t = typename pointer_of<T, Mutability>::type;

// reference_of
template<typename, class>
struct reference_of;

template<typename T>
struct reference_of<T, Mutable> {
  using type = T &;
};

template<typename T>
struct reference_of<T, Const> {
  using type = T const &;
};

template<typename T, class Mutability>
using reference_of_t = typename reference_of<T, Mutability>::type;

/*******************************************************************************
** TypePack
*******************************************************************************/

// TypePack
template<typename... Args>
struct TypePack {};

// inject_typepack
template<template<typename...> class, typename>
struct inject_typepack;

template<template<typename...> class T, typename... Args>
struct inject_typepack<T, TypePack<Args...>> {
  using type = T<Args...>;
};

template<template<typename...> class T, typename Pack>
using inject_typepack_t = typename inject_typepack<T, Pack>::type;

// join_typepacks
namespace detail {

template<typename, typename>
struct JoinedTypePacks;

template<typename... Args1, typename... Args2>
struct JoinedTypePacks<TypePack<Args1...>, TypePack<Args2...>> {
  using type = TypePack<Args1..., Args2...>;
};

}

template<typename TypePack1, typename TypePack2>
using join_typepacks =
    typename detail::JoinedTypePacks<TypePack1, TypePack2>::type;

// typepack_difference
namespace detail {

template<typename, typename>
struct TypePackDifference;

template<typename... Args>
struct TypePackDifference<TypePack<Args...>, TypePack<>> {
  using type = TypePack<Args...>;
};

template<typename Head, typename... Args1, typename... Args2>
struct TypePackDifference<TypePack<Head, Args1...>, TypePack<Head, Args2...>> {
  using type =
      typename TypePackDifference<TypePack<Args1...>, TypePack<Args2...>>::type;
};

}

template<typename TypePack1, typename TypePack2>
using typepack_difference =
    typename detail::TypePackDifference<TypePack1, TypePack2>::type;

// typepack_contains
template<typename, typename>
struct typepack_contains;

template<typename Type>
struct typepack_contains<Type, TypePack<>> : public false_t {};

template<typename Type, typename Head, typename... Tail>
struct typepack_contains<Type, TypePack<Head, Tail...>>
    : public IntegralConstant<
          bool, is_same<Type, Head>::value
                    ? true
                    : typepack_contains<Type, TypePack<Tail...>>::value> {};

// sizeof_typepack
template<typename>
struct sizeof_typepack;

template<typename... Args>
struct sizeof_typepack<TypePack<Args...>>
    : public IntegralConstant<size_t, sizeof...(Args)> {};

// alignment_of_union_typepack
namespace detail {

template<size_t, typename>
struct AlignmentOfTypeUnion;

template<size_t Alignment>
struct AlignmentOfTypeUnion<Alignment, TypePack<>>
    : public IntegralConstant<size_t, Alignment> {};

template<size_t Alignment, typename Type>
struct AlignmentOfTypeUnion<Alignment, TypePack<Type>>
    : public IntegralConstant<size_t,
                              max<size_t, Alignment, alignof(Type)>::value> {};

template<size_t Alignment, typename Type, typename... Types>
struct AlignmentOfTypeUnion<Alignment, TypePack<Type, Types...>>
    : public IntegralConstant<
          size_t, AlignmentOfTypeUnion<
                      AlignmentOfTypeUnion<Alignment, TypePack<Type>>::value,
                      TypePack<Types...>>::value> {};

}

template<typename>
struct alignment_of_union_typepack;

template<typename... Types>
struct alignment_of_union_typepack<TypePack<Types...>>
    : public IntegralConstant<
          size_t, detail::AlignmentOfTypeUnion<0u, TypePack<Types...>>::value> {
};

// sizeof_union_typepack
namespace detail {

template<size_t, typename>
struct SizeOfTypeUnion;

template<size_t Size>
struct SizeOfTypeUnion<Size, TypePack<>>
    : public IntegralConstant<size_t, Size> {};

template<size_t Size, typename Type>
struct SizeOfTypeUnion<Size, TypePack<Type>>
    : public IntegralConstant<size_t, max<size_t, Size, sizeof(Type)>::value> {
};

template<size_t Size, typename Type, typename... Types>
struct SizeOfTypeUnion<Size, TypePack<Type, Types...>>
    : public IntegralConstant<
          size_t, SizeOfTypeUnion<SizeOfTypeUnion<Size, TypePack<Type>>::value,
                                  TypePack<Types...>>::value> {};

}

template<typename>
struct sizeof_union_typepack;

template<typename... Types>
struct sizeof_union_typepack<TypePack<Types...>>
    : public IntegralConstant<
          size_t, detail::SizeOfTypeUnion<0u, TypePack<Types...>>::value> {};

// is_typepack_unique
template<typename>
struct is_typepack_unique;

template<>
struct is_typepack_unique<TypePack<>> : public true_t {};

template<typename Head, typename... Tail>
struct is_typepack_unique<TypePack<Head, Tail...>>
    : public IntegralConstant<
          bool, !typepack_contains<Head, TypePack<Tail...>>::value &&
                    is_typepack_unique<TypePack<Tail...>>::value> {};

// index_of_type_in_pack
namespace detail {

template<size_t, typename, typename>
struct IndexIfSameInPack;

template<size_t Index, typename Type>
struct IndexIfSameInPack<Index, Type, TypePack<>>
    : public IntegralConstant<size_t, 0u> {};

template<size_t Index, typename Type, typename Head, typename... Tail>
struct IndexIfSameInPack<Index, Type, TypePack<Head, Tail...>>
    : public IntegralConstant<
          size_t,
          is_same<Type, Head>::value
              ? Index
              : IndexIfSameInPack<Index + 1, Type, TypePack<Tail...>>::value> {
};

template<bool Condition>
struct StaticAssert {
  CORE_LIB_STATIC_ASSERT_TEXT(Condition, "Static Assertion failed!");
};

}
template<typename, typename>
struct index_of_type_in_pack;

template<typename Type, typename... Types>
struct index_of_type_in_pack<Type, TypePack<Types...>>
    : public IntegralConstant<size_t, detail::IndexIfSameInPack<
                                          0u, Type, TypePack<Types...>>::value>
    , public detail::StaticAssert<
          typepack_contains<Type, TypePack<Types...>>::value> {};

// type_at_index_t
namespace detail {

template<size_t, typename>
struct TypeAtIndex;

template<typename Head, typename... Types>
struct TypeAtIndex<0u, TypePack<Head, Types...>> {
  using type = Head;
};

template<size_t Index, typename Head, typename... Types>
struct TypeAtIndex<Index, TypePack<Head, Types...>> {
  using type = typename TypeAtIndex<Index - 1u, TypePack<Types...>>::type;
};

}

template<size_t Index, typename... Types>
using type_at_index_t =
    typename detail::TypeAtIndex<Index, TypePack<Types...>>::type;

// head_of_typepack
namespace detail {

template<typename>
struct HeadOfTypePack;

template<typename Head>
struct HeadOfTypePack<TypePack<Head>> {
  using type = Head;
};

template<typename Head, typename... Tail>
struct HeadOfTypePack<TypePack<Head, Tail...>> {
  using type = Head;
};

}

template<typename TypePack>
using head_of_typepack = typename detail::HeadOfTypePack<TypePack>::type;

// tail_of_typepack
namespace detail {

template<typename>
struct TailOfTypePack;

template<typename Tail>
struct TailOfTypePack<TypePack<Tail>> {
  using type = Tail;
};

template<typename Head, typename... Tail>
struct TailOfTypePack<TypePack<Head, Tail...>> {
  using type = typename TailOfTypePack<Tail...>::type;
};

}

template<typename TypePack>
using tail_of_typepack = typename detail::TailOfTypePack<TypePack>::type;

// insert_head_t
namespace detail {

template<typename, typename>
struct InsertHead;

template<typename Type, typename... Types>
struct InsertHead<Type, TypePack<Types...>> {
  using type = TypePack<Type, Types...>;
};

}

template<typename Type, typename TypePack>
using insert_head_t = typename detail::InsertHead<Type, TypePack>::type;

// insert_tail_t
namespace detail {

template<typename, typename>
struct InsertTail;

template<typename Type, typename... Types>
struct InsertTail<Type, TypePack<Types...>> {
  using type = TypePack<Types..., Type>;
};

}

template<typename Type, typename TypePack>
using insert_tail_t = typename detail::InsertTail<Type, TypePack>::type;

// remove_head_t
namespace detail {

template<typename>
struct RemoveFirstFromTypePack;

template<typename Head>
struct RemoveFirstFromTypePack<TypePack<Head>> {
  using type = TypePack<>;
};

template<typename Head, typename... Tail>
struct RemoveFirstFromTypePack<TypePack<Head, Tail...>> {
  using type = TypePack<Tail...>;
};

}

template<typename TypePack>
using remove_head_t = typename detail::RemoveFirstFromTypePack<TypePack>::type;

// remove_from_tail_t
namespace detail {

template<typename, typename, size_t>
struct RemoveTail;

template<typename... Head, typename... Types>
struct RemoveTail<TypePack<Head...>, TypePack<Types...>, 0u> {
  using type = TypePack<Head...>;
};

template<typename... Head, typename... Types, size_t Count>
struct RemoveTail<TypePack<Head...>, TypePack<Types...>, Count> {
  using type = typename RemoveTail<
      TypePack<Head..., head_of_typepack<TypePack<Types...>>>,
      remove_head_t<TypePack<Types...>>, Count - 1u>::type;
};

}

template<typename TypePack, size_t Count>
using remove_from_tail_t =
    typename detail::RemoveTail<mpl::TypePack<>, TypePack, Count>::type;

// remove_from_head_t
namespace detail {

template<typename, size_t>
struct RemoveHead;

template<>
struct RemoveHead<TypePack<>, 0u> {
  using type = TypePack<>;
};

template<typename... Types>
struct RemoveHead<TypePack<Types...>, 0u> {
  using type = TypePack<Types...>;
};

template<typename... Types, size_t Count>
struct RemoveHead<TypePack<Types...>, Count> {
  using type =
      typename RemoveHead<remove_head_t<TypePack<Types...>>, Count - 1u>::type;
};

}

template<typename TypePack, size_t Count>
using remove_from_head_t = typename detail::RemoveHead<TypePack, Count>::type;

//
namespace detail {

template<typename, typename, size_t>
struct SplitFirst;

template<typename... Types1, typename... Types2>
struct SplitFirst<TypePack<Types1...>, TypePack<Types2...>, 0u> {
  using Head = TypePack<Types1...>;
  using Tail = TypePack<Types2...>;
};

template<typename... Types1, typename... Types2, size_t Count>
struct SplitFirst<TypePack<Types1...>, TypePack<Types2...>, Count> {
  CORE_LIB_STATIC_ASSERT(Count <= sizeof...(Types2));
  using NewHead = TypePack<Types1..., head_of_typepack<TypePack<Types2...>>>;
  using NewTail = remove_head_t<TypePack<Types2...>>;
  using Next = SplitFirst<NewHead, NewTail, Count - 1u>;
  using Head = typename Next::Head;
  using Tail = typename Next::Tail;
};

}

// first_t
template<typename TypePack, size_t Count>
using first_t =
    typename detail::SplitFirst<mpl::TypePack<>, TypePack, Count>::Head;

// last_t
template<typename TypePack, size_t Count>
using last_t =
    typename detail::SplitFirst<mpl::TypePack<>, TypePack,
                                sizeof_typepack<TypePack>::value - Count>::Tail;

// apply_t
namespace detail {

template<template<typename> class Operator, typename Operand>
struct Apply {
  using type = typename Operator<Operand>::type;
};

}

template<template<typename> class Operator, typename Operand>
using apply_t = typename detail::Apply<Operator, Operand>::type;

// for_each_t
namespace detail {

template<template<typename> class, typename>
struct ForEach;

template<template<typename> class Operator>
struct ForEach<Operator, TypePack<>> {
  using type = TypePack<>;
};

template<template<typename> class Operator, typename Head>
struct ForEach<Operator, TypePack<Head>> {
  using type = TypePack<apply_t<Operator, Head>>;
};

template<template<typename> class Operator, typename Head, typename... Tail>
struct ForEach<Operator, TypePack<Head, Tail...>> {
  using type =
      insert_head_t<apply_t<Operator, Head>,
                    typename ForEach<Operator, TypePack<Tail...>>::type>;
};

}

template<template<typename> class Operator, typename Operand>
using for_each_t = typename detail::ForEach<Operator, Operand>::type;

// has_call_operator
template<typename, typename = void>
struct has_call_operator : public false_t {};

template<typename F>
struct has_call_operator<F, void_t<decltype(&F::operator())>>
    : public true_t {};

// if_has_call_operator_t
template<typename F, typename Result = void>
using if_has_call_operator_t = enable_if_t<has_call_operator<F>::value, Result>;

// parameters_of_t
namespace detail {

template<typename F>
struct ParametersOf {
  using type = typename ParametersOf<decltype(&F::operator())>::type;
};

#if defined(CORE_LIB_COMPILER_MSVC)

#  if defined(CORE_LIB_ARCH_32)

template<typename R, typename... Args>
struct ParametersOf<R(__stdcall *)(Args...)> {
  using type = TypePack<Args...>;
};

template<typename R, typename... Args>
struct ParametersOf<R(__cdecl *)(Args...)> {
  using type = TypePack<Args...>;
};

template<typename R, typename... Args>
struct ParametersOf<R(__fastcall *)(Args...)> {
  using type = TypePack<Args...>;
};

template<typename R, typename... Args>
struct ParametersOf<R(__thiscall *)(Args...)> {
  using type = TypePack<Args...>;
};

#  else

template<typename R, typename... Args>
struct ParametersOf<R (*)(Args...)> {
  using type = TypePack<Args...>;
};

#  endif

#elif defined(CORE_LIB_COMPILER_GCC) || defined(CORE_LIB_COMPILER_CLANG)

template<typename R, typename... Args>
struct ParametersOf<R(Args...)> {
  using type = TypePack<Args...>;
};

template<typename R, typename... Args>
struct ParametersOf<R (&)(Args...)> {
  using type = TypePack<Args...>;
};

template<typename R, typename... Args>
struct ParametersOf<R (*)(Args...)> {
  using type = TypePack<Args...>;
};

#endif

template<typename R, class C, typename... Args>
struct ParametersOf<R (C::*)(Args...)> {
  using type = TypePack<Args...>;
};

template<typename R, class C, typename... Args>
struct ParametersOf<R (C::*)(Args...) const> {
  using type = TypePack<Args...>;
};

}

template<typename F>
using parameters_of_t = typename detail::ParametersOf<F>::type;

// result_of_t
namespace detail {

template<typename F>
struct ResultOf;

template<typename F>
struct ResultOf {
  using type = typename ResultOf<decltype(&F::operator())>::type;
};

#if defined(CORE_LIB_COMPILER_MSVC)

#  if defined(CORE_LIB_ARCH_32)

template<typename R, typename... Args>
struct ResultOf<R(__stdcall *)(Args...)> {
  using type = R;
};

template<typename R, typename... Args>
struct ResultOf<R(__cdecl *)(Args...)> {
  using type = R;
};

template<typename R, typename... Args>
struct ResultOf<R(__fastcall *)(Args...)> {
  using type = R;
};

template<typename R, typename... Args>
struct ResultOf<R(__thiscall *)(Args...)> {
  using type = R;
};

#  else

template<typename R, typename... Args>
struct ResultOf<R(Args...)> {
  using type = R;
};

template<typename R, typename... Args>
struct ResultOf<R (&)(Args...)> {
  using type = R;
};

template<typename R, typename... Args>
struct ResultOf<R (*)(Args...)> {
  using type = R;
};

#  endif

#elif defined(CORE_LIB_COMPILER_GCC) || defined(CORE_LIB_COMPILER_CLANG)

template<typename R, typename... Args>
struct ResultOf<R(Args...)> {
  using type = R;
};

template<typename R, typename... Args>
struct ResultOf<R (&)(Args...)> {
  using type = R;
};

template<typename R, typename... Args>
struct ResultOf<R (*)(Args...)> {
  using type = R;
};

#endif

template<typename R, class C, typename... Args>
struct ResultOf<R (C::*)(Args...)> {
  using type = R;
};

template<typename R, class C, typename... Args>
struct ResultOf<R (C::*)(Args...) const> {
  using type = R;
};

}

template<typename F>
using result_of_t = typename detail::ResultOf<F>::type;

// Signature
template<typename R, typename... Args>
struct Signature {};

// signature_of_t
namespace detail {

template<typename, typename>
struct SignatureFrom;

template<typename R, typename... Args>
struct SignatureFrom<R, TypePack<Args...>> {
  using type = Signature<R, Args...>;
};

template<typename F>
struct SignatureOf {
  using type = typename SignatureFrom<result_of_t<F>, parameters_of_t<F>>::type;
};

template<typename R, typename... Args>
struct SignatureOf<R (*)(Args...)> {
  using type = Signature<R, Args...>;
};

template<typename C, typename R, typename... Args>
struct SignatureOf<R (C::*)(Args...)> {
  using type = Signature<R, Args...>;
};

}

template<typename F>
using signature_of_t = typename detail::SignatureOf<F>::type;

// TupleOfTypePack
namespace detail {

template<typename T>
struct Not_Reference {
  using type = T;
};

template<typename T>
struct Not_Reference<T *> {
  using type = T *;
};

template<typename T>
struct Not_Reference<T &&> {
  using type = T &&;
};

template<typename T>
struct Not_Reference<T &> {
  using type = T &;
};

template<typename>
struct ReferencelessTypePack;

template<typename... Args>
struct ReferencelessTypePack<TypePack<Args...>> {
  using type = apply_t<Not_Reference, TypePack<Args...>>;
};

template<typename T>
using NoReferenceTypePack = typename ReferencelessTypePack<T>::type;

template<typename T>
struct TupleOfTypePack {
  using type = std::tuple<T>;
};

template<>
struct TupleOfTypePack<TypePack<void>>;

template<typename... Args>
struct TupleOfTypePack<TypePack<Args...>> {
  using type = std::tuple<Args...>;
};

}

template<typename Pack>
using TupleOfTypePack =
    apply_t<detail::TupleOfTypePack, detail::NoReferenceTypePack<Pack>>;

// Sequence
template<size_t...>
struct Sequence {};

// GenerateSequence
namespace detail {

template<size_t N, size_t... S>
struct GenerateSequence : GenerateSequence<N - 1, N - 1, S...> {};

template<size_t... S>
struct GenerateSequence<0, S...> {
  using type = Sequence<S...>;
};

}

template<size_t Size>
using GenerateSequence = typename detail::GenerateSequence<Size>::type;

////////////////////////////////////////////////////////////////////////////////
//                                                                                                //
////////////////////////////////////////////////////////////////////////////////

template<typename T, T V>
struct Value {};

template<typename T1, typename T2>
struct Pair {};

template<class C, typename T, T C::*Pointer>
using Member = Value<T C::*, Pointer>;

// zip_t
namespace detail {

template<typename T1, typename T2>
struct Zip {
  using type = Pair<T1, T2>;
};

template<typename T>
struct Zip<T, TypePack<>> {
  using type = T;
};

template<typename Head, typename... Tail>
struct Zip<TypePack<>, TypePack<Head, Tail...>> {
  using type = typename Zip<Head, TypePack<Tail...>>::type;
};

template<typename T, typename Head, typename... Tail>
struct Zip<T, TypePack<Head, Tail...>> {
  using type = Pair<T, typename Zip<Head, TypePack<Tail...>>::type>;
};

}

template<typename T1, typename T2>
using zip_t = typename detail::Zip<T1, T2>::type;

}

// forward
template<class T>
constexpr T && forward(mpl::remove_reference_t<T> & _value) noexcept {
  return static_cast<T &&>(_value);
}

template<class T>
constexpr T && forward(mpl::remove_reference_t<T> && _value) noexcept {
  STATIC_ASSERT(!mpl::is_lvalue_reference<T>::value);
  return static_cast<T &&>(_value);
}

// move
template<class T>
constexpr mpl::remove_reference_t<T> && move(T && _value) noexcept {
  return static_cast<mpl::remove_reference_t<T> &&>(_value);
}

#endif
