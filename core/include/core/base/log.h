#ifndef _CORE_LIB_LOG_H_
#define _CORE_LIB_LOG_H_

#if !defined(CORE_LIB_LOG_ENABLED)
#  if defined(CORE_LIB_CONFIGURATION_DEBUG)
#    define CORE_LIB_LOG_ENABLED
#  endif
#endif

/*******************************************************************************
** CORE_LIB_LOG
*******************************************************************************/

#if defined(CORE_LIB_LOG_ENABLED)

////////////////////////////////////////////////////////////////////////////////
// logging
////////////////////////////////////////////////////////////////////////////////

namespace logging {

#  if defined(CORE_LIB_OS_WINDOWS)

/*******************************************************************************
** Console
*******************************************************************************/

class Console {
public:
  Console() {
    crt_std_conin = ::GetStdHandle(STD_INPUT_HANDLE);
    crt_std_conout = ::GetStdHandle(STD_OUTPUT_HANDLE);
  }

  void Create() {
    if (::AllocConsole()) {
      allocated = true;
      std_conin = ::GetStdHandle(STD_INPUT_HANDLE);
      std_conout = ::GetStdHandle(STD_OUTPUT_HANDLE);
    }
  }

  void Destroy() {
    if (allocated) {
      std_conin = nullptr;
      ::SetStdHandle(STD_INPUT_HANDLE, crt_std_conin);
      std_conout = nullptr;
      ::SetStdHandle(STD_OUTPUT_HANDLE, crt_std_conout);
      ::FreeConsole();
    }
  }

  HANDLE StdConin() const { return allocated ? std_conin : crt_std_conin; }
  HANDLE StdConout() const { return allocated ? std_conout : crt_std_conout; }

private:
  bool allocated = false;
  HANDLE crt_std_conin = nullptr;
  HANDLE crt_std_conout = nullptr;
  HANDLE std_conin = nullptr;
  HANDLE std_conout = nullptr;
};

inline Console & GetConsole() {
  static Console console;
  return console;
}

#  endif

/*******************************************************************************
** FunctionName
*******************************************************************************/

class FunctionName final {
public:
  FunctionName(char const * _signature)
      : signature(_signature) {}
  FunctionName(FunctionName &&) = delete;
  FunctionName(FunctionName const &) = delete;
  FunctionName & operator=(FunctionName &&) = delete;
  FunctionName & operator=(FunctionName const &) = delete;
  ~FunctionName() = default;

  char const * Get() const {
    // Search forward for end (argument list)
    auto name_end = signature;
    while (((*name_end) != '(') && ((*name_end) != '\0')) {
      ++name_end;
    }
    // Search backward for start (space before name)
    auto name_start = name_end;
    while (((name_start != signature) && (*(name_start - 1)) != ' ')) {
      --name_start;
    }
    auto start_position = reinterpret_cast<uintptr_t>(name_start);
    auto end_position = reinterpret_cast<uintptr_t>(name_end);
    size_t length = static_cast<size_t>(end_position - start_position);
    if (kBufferSize < length) {
      length = kBufferSize;
    }
    CORE_LIB_MSVC_WARNING_PUSH
    CORE_LIB_MSVC_WARNING_DISABLE(4996)
    std::strncpy(buffer, name_start, length);
    CORE_LIB_MSVC_WARNING_POP
    *(buffer + length) = '\0';
    return buffer;
  }

private:
  enum { kBufferSize = 256 };
  char const * signature;
  mutable char buffer[kBufferSize + 1u];
};

/*******************************************************************************
** LOG_SOURCE
*******************************************************************************/

#  if defined(CORE_LIB_COMPILER_MSVC)
#    define LOG_SOURCE logging::FunctionName(__FUNCSIG__)
#  else
#    define LOG_SOURCE logging::FunctionName(__PRETTY_FUNCTION__)
#  endif

/*******************************************************************************
** display_console
*******************************************************************************/

inline void display_console(char const * _message) {
#  if defined(CORE_LIB_OS_WINDOWS)
#    if defined(CORE_LIB_CONFIGURATION_DEBUG)
  if (::IsDebuggerPresent()) {
    ::OutputDebugString(_message);
  }
#    endif
  ::WriteConsoleA(GetConsole().StdConout(), _message, ::strlen(_message),
                  nullptr, nullptr);
#  else
  std::printf("%s", _message);
#  endif
}

/*******************************************************************************
** display_messagebox
*******************************************************************************/

inline bool display_messagebox(char const * _title, char const * _message) {
#  if defined(CORE_LIB_OS_WINDOWS)
  display_console(_message);
  auto const result = ::MessageBox(NULL, (LPCTSTR)_message, (LPCTSTR)_title,
                                   MB_ICONERROR | MB_OKCANCEL);
  return (result == IDCANCEL);
#  else
  display_console(_message);
  char ch;
  if (std::scanf(" %c", &ch)) {
  }
  return true;
#  endif
}

/*******************************************************************************
** format_message
*******************************************************************************/

inline char const * format_message(FunctionName const & _function_name,
                                   char const * _format, va_list _args) {
  static const size_t kSourceSize = 256u;
  static const size_t kMessageSize = 1024u;
  static const size_t kResultSize = kSourceSize + kMessageSize;
  static thread_local char message[kMessageSize + 1u];
  static thread_local char result[kResultSize + 1u];
#  if defined(CORE_LIB_OS_WINDOWS)
  char const * source_format = "[%*" PRIu32 "] %s: ";
  uint32 thread_id = ::GetCurrentThreadId();
#  elif defined(CORE_LIB_OS_LINUX)
  char const * source_format = "[%*d] %s: ";
#    if defined(SYS_gettid)
  pid_t thread_id = ::syscall(SYS_gettid);
#    else
#      error "SYS_gettid unavailable!"
#    endif
#  endif
  size_t source_size = ::snprintf(result, kResultSize, source_format, 6,
                                  thread_id, _function_name.Get());
  if (source_size < kResultSize) {
    if (::vsnprintf(message, kMessageSize, _format, _args)) {
      auto source_end = reinterpret_cast<uintptr_t>(result) + source_size;
      auto message_space = kResultSize - source_size;
      CORE_LIB_MSVC_WARNING_PUSH
      CORE_LIB_MSVC_WARNING_DISABLE(4996)
      ::strncpy(reinterpret_cast<char *>(source_end), message, message_space);
      CORE_LIB_MSVC_WARNING_POP
    }
  }
  return result;
}

/*******************************************************************************
** display_assert
*******************************************************************************/

enum class Severity {
  VERBOSE,
  MESSAGE,
  STAGE,
  LOG,
  WARNING,
  ERROR,
  FATAL,
};

constexpr bool is_severity_logged(Severity _severity) {
  if (static_cast<size_t>(Severity::VERBOSE) < static_cast<size_t>(_severity))
    return true;
  return false;
}

constexpr bool is_severity_critical(Severity _severity) {
  if (static_cast<size_t>(Severity::ERROR) < static_cast<size_t>(_severity))
    return true;
  return false;
}

inline void log(Severity _severity, FunctionName const & _function_name,
                char const * _format, ...) {
  va_list args;
  va_start(args, _format);
  if (is_severity_logged(_severity)) {
    if (!is_severity_critical(_severity)) {
      display_console(format_message(_function_name, _format, args));
    } else {
      display_messagebox("Critical error!",
                         format_message(_function_name, _format, args));
    }
  }
  va_end(args);
}

inline bool log_assertion_failed(FunctionName const & _function_name,
                                 char const * _format, ...) {
  ::fflush(stdout);

  va_list args;
  va_start(args, _format);
  auto result = display_messagebox(
      "Assertion failed!", format_message(_function_name, _format, args));
  va_end(args);

  ::fflush(stdout);
  return result;
}

inline bool log_exception(char const * _format, ...) {
  ::fflush(stdout);

  va_list args;
  va_start(args, _format);
  auto result = display_messagebox("Exception!",
                                   format_message(LOG_SOURCE, _format, args));
  va_end(args);

  ::fflush(stdout);
  return result;
}

}

#  define CORE_LIB_LOG(__severity__, ...)                                      \
    logging::log(logging::Severity::__severity__, LOG_SOURCE, __VA_ARGS__);

#else
#  define CORE_LIB_LOG(__severity__, ...)
#endif // CORE_LIB_LOG_ENABLED

////////////////////////////////////////////////////////////////////////////////
// logging
////////////////////////////////////////////////////////////////////////////////

namespace logging {

inline void Initialize() {
#if defined(CORE_LIB_LOG_ENABLED)
#  if defined(CORE_LIB_OS_WINDOWS)
  GetConsole().Create();
#  endif
#endif
}

inline void Finalize() {
#if defined(CORE_LIB_LOG_ENABLED)
#  if defined(CORE_LIB_OS_WINDOWS)
  GetConsole().Destroy();
#  endif
#endif
}

}

#endif
