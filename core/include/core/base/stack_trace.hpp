#ifndef _CORE_LIB_STACK_TRACE_HPP_
#define _CORE_LIB_STACK_TRACE_HPP_

#include "core/base.h"

#if defined(CORE_LIB_OS_WINDOWS)
#  include "dbghelp.h"
#  pragma comment(lib, "dbghelp.lib")
#elif defined(CORE_LIB_OS_LINUX)
#  include <dlfcn.h>
#  include <execinfo.h>
#  include <cxxabi.h>
#endif

#define LOG_STACK_TRACE(severity, ...) CORE_LIB_LOG(severity, __VA_ARGS__)

////////////////////////////////////////////////////////////////////////////////
// stack_trace
////////////////////////////////////////////////////////////////////////////////

namespace stack_trace {

inline bool Initialize() {
  LOG_STACK_TRACE(STAGE, "Initializing..\n");
#if defined(CORE_LIB_OS_WINDOWS)
  // Initialize symbol handler
  ::SymSetOptions(SYMOPT_UNDNAME | SYMOPT_DEFERRED_LOADS);

  if (!::SymInitialize(::GetCurrentProcess(), NULL, TRUE)) {
    uint32 error = ::GetLastError();
    LOG_STACK_TRACE(ERROR, "SymInitialize failed with %" PRIu32 "!\n", error);
    return false;
  }
#endif
  return true;
}

inline void Finalize() {
  LOG_STACK_TRACE(STAGE, "Finalizing..\n");
#if defined(CORE_LIB_OS_WINDOWS)
  // Finalize symbol handler
  if (!::SymCleanup(::GetCurrentProcess())) {
    uint32 error = ::GetLastError();
    LOG_STACK_TRACE(WARNING, "SymCleanup failed with %" PRIu32 "!\n", error);
  }
#endif
}

/*******************************************************************************
** dump
*******************************************************************************/

#if defined(CORE_LIB_OS_WINDOWS)
inline void dump(PCONTEXT _context) {
  STACKFRAME64 stack;
  memset(&stack, 0, sizeof(stack));
  stack.AddrPC.Mode = AddrModeFlat;
  stack.AddrStack.Mode = AddrModeFlat;
  stack.AddrFrame.Mode = AddrModeFlat;
#  if defined(CORE_LIB_ARCH_32)
  stack.AddrPC.Offset = _context->Eip;
  stack.AddrStack.Offset = _context->Esp;
  stack.AddrFrame.Offset = _context->Ebp;
  uint32 machine_type = IMAGE_FILE_MACHINE_I386;
#  elif defined(CORE_LIB_ARCH_64)
  stack.AddrPC.Offset = _context->Rip;
  stack.AddrStack.Offset = _context->Rsp;
  stack.AddrFrame.Offset = _context->Rbp;
  uint32 machine_type = IMAGE_FILE_MACHINE_AMD64;
#  endif

  for (uint32 frame = 0;; ++frame) {
    if (!::StackWalk64(machine_type, ::GetCurrentProcess(),
                       ::GetCurrentThread(), &stack, _context, nullptr,
                       ::SymFunctionTableAccess64, ::SymGetModuleBase64,
                       nullptr)) {
      break;
    }

    DWORD64 displacement = 0;
    const size_t kMaxSymNameLength = 1024;
    char buffer[sizeof(SYMBOL_INFO) + kMaxSymNameLength * sizeof(TCHAR)];
    PSYMBOL_INFO pSymbol = (PSYMBOL_INFO)buffer;
    pSymbol->SizeOfStruct = sizeof(SYMBOL_INFO);
    pSymbol->MaxNameLen = kMaxSymNameLength;

    if (!::SymFromAddr(::GetCurrentProcess(), (ULONG64)stack.AddrPC.Offset,
                       &displacement, pSymbol)) {
      uint32 error = ::GetLastError();
      LOG_STACK_TRACE(ERROR, "SymFromAddr failed with %" PRIu32 "!\n", error);
      continue;
    }

    // TODO: Zero last character of name!

    LOG_STACK_TRACE(LOG, "Frame %" PRIu32 "\tSymbol name: '%s'\n", frame,
                    pSymbol->Name);
  }
}
#endif

inline void dump() {
#if defined(CORE_LIB_OS_WINDOWS)
  CONTEXT context;
  ::RtlCaptureContext(&context);
  dump(&context);
#elif defined(CORE_LIB_OS_LINUX)
  constexpr int kMaxFrames = 64;
  void * frames[kMaxFrames];
  int frames_count = ::backtrace(frames, kMaxFrames);
  char ** symbols = ::backtrace_symbols(frames, frames_count);
  for (int frame = 0; frame < frames_count; ++frame) {
    Dl_info info;
    if (::dladdr(frames[frame], &info) && info.dli_sname) {
      int status = -1;
      char * demangled = nullptr;
      if (info.dli_sname[0] == '_') {
        demangled = abi::__cxa_demangle(info.dli_sname, nullptr, 0, &status);
      }
      LOG_STACK_TRACE(LOG, "Frame %d\t%p '%s' + %" PRIxPTR "\n", frame,
                      frames[frame],
                      status == 0                 ? demangled
                      : info.dli_sname == nullptr ? symbols[frame]
                                                  : info.dli_sname,
                      (uintptr_t)frames[frame] - (uintptr_t)info.dli_saddr);
      free(demangled);
    } else {
      LOG_STACK_TRACE(LOG, "Frame %d\t%p '%s'\n", frame, frames[frame],
                      symbols[frame]);
    }
  }
#endif
}

}

#endif
