#ifndef _CORE_LIB_EXTENSIONS_H_
#define _CORE_LIB_EXTENSIONS_H_

// String
#define CORE_LIB_AS_STRING(x) #x
#define CORE_LIB_TO_STRING(x) CORE_LIB_AS_STRING(x)

// Concat
#define CORE_LIB_CONCAT_(__token1__, __token2__) __token1__##__token2__
#define CORE_LIB_CONCAT(__token1__, __token2__)                                \
  CORE_LIB_CONCAT_(__token1__, __token2__)

// Warning
#if defined(CORE_LIB_COMPILER_MSVC)
#  define CORE_LIB_MSVC_WARNING_PUSH __pragma(warning(push))
#  define CORE_LIB_MSVC_WARNING_DISABLE(__warning__)                           \
    __pragma(warning(disable :##__warning__))
#  define CORE_LIB_MSVC_WARNING_POP __pragma(warning(pop))
#else
#  define CORE_LIB_MSVC_WARNING_PUSH
#  define CORE_LIB_MSVC_WARNING_DISABLE(__warning__)
#  define CORE_LIB_MSVC_WARNING_POP
#endif

#if defined(CORE_LIB_COMPILER_GCC) || defined(CORE_LIB_COMPILER_CLANG)
#  define CORE_LIB_GCC_WARNING_PUSH _Pragma("GCC diagnostic push")
#  define CORE_LIB_GCC_WARNING_DISABLE(__warning__)                            \
    _Pragma(CORE_LIB_AS_STRING(GCC diagnostic ignored __warning__))
#  define CORE_LIB_GCC_WARNING_POP _Pragma("GCC diagnostic pop")
#else
#  define CORE_LIB_GCC_WARNING_PUSH
#  define CORE_LIB_GCC_WARNING_DISABLE(__warning__)
#  define CORE_LIB_GCC_WARNING_POP
#endif

// No return
#if defined(CORE_LIB_COMPILER_MSVC)
#  define CORE_LIB_ASSUME_TRUE(arg) (arg)
#elif defined(CORE_LIB_COMPILER_GCC)
#  define CORE_LIB_ASSUME_TRUE(arg) (arg)
#elif defined(CORE_LIB_COMPILER_CLANG)
inline constexpr bool AnalyzerNoReturn() __attribute__((analyzer_noreturn)) {
  return false;
}
inline constexpr bool AnalyzerAssumeTrue(bool _value) {
  return _value || AnalyzerNoReturn();
}
#  define CORE_LIB_ASSUME_TRUE(arg) ::AnalyzerAssumeTrue(!!(arg))
#endif

// Unused parameter
#define CORE_LIB_UNUSED_PARAMETER(__parameter__) (__parameter__)

// Unreachable
#if defined(CORE_LIB_COMPILER_MSVC)
#  define CORE_LIB_UNREACHABLE
#elif defined(CORE_LIB_COMPILER_GCC) || defined(CORE_LIB_COMPILER_CLANG)
#  define CORE_LIB_UNREACHABLE __builtin_unreachable()
#endif

// Alignment
#if defined(CORE_LIB_COMPILER_MSVC)
#  define CORE_LIB_ALIGNED_(type_name) __declspec(align(type_name))
#elif defined(CORE_LIB_COMPILER_GCC) || defined(CORE_LIB_COMPILER_CLANG)
#  define CORE_LIB_ALIGNED_(type_name) __attribute__((aligned(type_name)))
#endif

#define CORE_LIB_ALIGNED(aligned_name, type_name)                              \
  using aligned_name = CORE_LIB_ALIGNED_(type_name)

// Line & File & Function
#define CORE_LIB__LINE__ __LINE__
#define CORE_LIB__FILE__ __FILE__
#define CORE_LIB__FUNCTION__ __FUNCTION__

// Static assert
#define CORE_LIB_STATIC_ASSERT(...) static_assert(__VA_ARGS__, #__VA_ARGS__)
#define CORE_LIB_STATIC_ASSERT_TEXT(...) static_assert(__VA_ARGS__)

// ForceInline
#if defined(CORE_LIB_COMPILER_MSVC)
#  define CORE_LIB_FORCE_INLINE __forceinline
#elif defined(CORE_LIB_COMPILER_GCC) || defined(CORE_LIB_COMPILER_CLANG)
#  define CORE_LIB_FORCE_INLINE __attribute__((always_inline)) inline
#endif

////////////////////////////////////////////////////////////////////////////////
// core
////////////////////////////////////////////////////////////////////////////////

namespace core {

template<typename T>
void swap(T & _value1, T & _value2) {
  T temp(static_cast<T &&>(_value1));
  _value1 = static_cast<T &&>(_value2);
  _value2 = static_cast<T &&>(temp);
}

template<typename T>
T max(T const & _first, T const & _second) {
  return _first < _second ? _second : _first;
}

template<typename T>
T min(T const & _first, T const & _second) {
  return _first < _second ? _first : _second;
}

template<typename T>
T abs(T const & _value) {
  return _value < 0 ? -_value : _value;
}

/*******************************************************************************
** Value
*******************************************************************************/

template<typename T, T DefaultValue>
class Value {
public:
  using ValueType = T;

  Value()
      : value(DefaultValue) {}
  Value(T _value)
      : value(_value) {}
  Value(Value && _other) {
    new (&value) T(static_cast<T &&>(_other.value));
    _other.value = DefaultValue;
  }
  Value(Value const &) = default;
  Value & operator=(Value && _other) {
    Value * other_ptr = reinterpret_cast<Value *>(
        &const_cast<char &>(reinterpret_cast<const volatile char &>(_other)));
    if (this != other_ptr) {
      value = static_cast<T &&>(_other.value);
      _other.value = DefaultValue;
    }
    return *this;
  }
  Value & operator=(Value const & _other) = default;
  ~Value() = default;

  bool HasValue() const { return value != DefaultValue; }

  T & Get() { return value; }
  operator T &() { return value; }
  T const & Get() const { return value; }
  operator T const &() const { return value; }

  T * operator&() { return &value; }
  T const * operator&() const { return &value; }

  void Reset() { value = DefaultValue; }

protected:
  T value;
};

}

#endif
