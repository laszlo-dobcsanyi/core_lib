#ifndef _CORE_LIB_DEBUG_H_
#define _CORE_LIB_DEBUG_H_

#if defined(CORE_LIB_CONFIGURATION_DEBUG)

/*******************************************************************************
** CORE_LIB_ASSERT_MESSAGE
*******************************************************************************/

#  if defined(CORE_LIB_OS_WINDOWS)

#    define CORE_LIB_ASSERT_MESSAGE(...)                                       \
      do {                                                                     \
        stack_trace::dump();                                                   \
        if (logging::log_assertion_failed(LOG_SOURCE, __VA_ARGS__)) {          \
          __debugbreak();                                                      \
        }                                                                      \
      } while (0)

#  elif defined(CORE_LIB_OS_LINUX)

#    define CORE_LIB_ASSERT_MESSAGE(...)                                       \
      do {                                                                     \
        stack_trace::dump();                                                   \
        if (logging::log_assertion_failed(LOG_SOURCE, __VA_ARGS__)) {          \
          __builtin_trap();                                                    \
        }                                                                      \
      } while (0)

#  endif

/*******************************************************************************
** CORE_LIB_ASSERT_TEXT
*******************************************************************************/

#  define CORE_LIB_ASSERT_TEXT(__condition__, ...)                             \
    do {                                                                       \
      if (CORE_LIB_ASSUME_TRUE(!(__condition__))) {                            \
        CORE_LIB_ASSERT_MESSAGE(__VA_ARGS__);                                  \
      }                                                                        \
    } while (0)

/*******************************************************************************
** CORE_LIB_ASSERT
*******************************************************************************/

#  define CORE_LIB_ASSERT(__condition__)                                       \
    CORE_LIB_ASSERT_TEXT(                                                      \
        __condition__,                                                         \
        "Assertion failed! Expression: %s \nFile: %s Line: %s\n",              \
        CORE_LIB_TO_STRING(__condition__),                                     \
        CORE_LIB_TO_STRING(CORE_LIB__FILE__),                                  \
        CORE_LIB_TO_STRING(CORE_LIB__LINE__))

/*******************************************************************************
** CORE_LIB_EXCEPTION
*******************************************************************************/

#  if defined(CORE_LIB_OS_WINDOWS)

#    define CORE_LIB_EXCEPTION(...)                                            \
      do {                                                                     \
        if (logging::log_exception(__VA_ARGS__)) {                             \
          __debugbreak();                                                      \
        }                                                                      \
      } while (0)

#  elif defined(CORE_LIB_OS_LINUX)

#    define CORE_LIB_EXCEPTION(...)                                            \
      do {                                                                     \
        if (logging::log_exception(__VA_ARGS__)) {                             \
          __builtin_trap();                                                    \
        }                                                                      \
      } while (0)

#  endif

/*******************************************************************************
** CORE_LIB_UNIMPLEMENTED
*******************************************************************************/

#  define CORE_LIB_UNIMPLEMENTED                                               \
    CORE_LIB_ASSERT_MESSAGE("Unimplemented function %s!\n",                    \
                            CORE_LIB__FUNCTION__);                             \
    CORE_LIB_UNREACHABLE

#else

#  define CORE_LIB_ASSERT_MESSAGE(...)
#  define CORE_LIB_ASSERT_TEXT(__condition__, ...)
#  define CORE_LIB_ASSERT(__condition__)
#  define CORE_LIB_EXCEPTION(...)
#  define CORE_LIB_UNIMPLEMENTED // #error Unimplemented function!

#endif

#endif
