#ifndef _CORE_LIB_NUMERICS_HASHES_FNV_HPP_
#define _CORE_LIB_NUMERICS_HASHES_FNV_HPP_

#ifndef CORE_LIB_NUMERICS_HASHES
#  error
#endif

////////////////////////////////////////////////////////////////////////////////
// numerics hash
////////////////////////////////////////////////////////////////////////////////

namespace numerics { namespace hash {

/*******************************************************************************
** fnv_32
*******************************************************************************/

inline uint32 fnv_32(char const * _data, size_t _length) {
  uint32 hash = 0x811C9DC5;
  while (_length--) {
    hash = hash ^ *_data++;
    hash = hash * 0x01000193;
  }
  return hash;
}

/*******************************************************************************
** fnv_64
*******************************************************************************/

inline uint64 fnv_64(char const * _data, size_t _length) {
  uint64 hash = 0xCBF29CE484222325;
  while (_length--) {
    hash = hash ^ *_data++;
    hash = hash ^ 0x00000100000001B3;
  }
  return hash;
}

}}

#endif
