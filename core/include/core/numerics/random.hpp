#ifndef _CORE_LIB_NUMERICS_RANDOM_HPP_
#define _CORE_LIB_NUMERICS_RANDOM_HPP_

#ifndef CORE_LIB_NUMERICS
#  error
#endif

////////////////////////////////////////////////////////////////////////////////
// m detail
////////////////////////////////////////////////////////////////////////////////

namespace numerics { namespace random { namespace detail {

template<typename T>
inline void xor_shift(T & _value) {
  _value ^= _value << 1u;
  _value ^= _value >> 3u;
  _value ^= _value << 10u;
}

}}}

////////////////////////////////////////////////////////////////////////////////
// numerics random
////////////////////////////////////////////////////////////////////////////////

namespace numerics { namespace random {

class XorShift32 final {
public:
  XorShift32()
      : XorShift32(2463534242) {}
  XorShift32(uint32 _seed)
      : state(_seed) {}
  XorShift32(XorShift32 &&) = default;
  XorShift32(XorShift32 const &) = default;
  XorShift32 & operator=(XorShift32 &&) = default;
  XorShift32 & operator=(XorShift32 const &) = default;
  ~XorShift32() = default;

  uint32 Next() {
    detail::xor_shift(state);
    return state;
  }

private:
  uint32 state;
};

}}

////////////////////////////////////////////////////////////////////////////////
// numerics
////////////////////////////////////////////////////////////////////////////////

namespace numerics {

/*******************************************************************************
** ShiftType
*******************************************************************************/

enum class ShiftType {
  Left,
  Right,
};

class ShiftParameters final {
public:
  uint32 A;
  uint32 B;
  uint32 C;

  ShiftParameters(size_t _index) {
    static const uint32 parameters[81u][3u] = {
        {1, 3, 10},   {1, 5, 16},   {1, 5, 19},  {1, 9, 29},   {1, 11, 6},
        {1, 11, 16},  {1, 19, 3},   {1, 21, 20}, {1, 27, 27},  {2, 5, 15},
        {2, 5, 21},   {2, 7, 7},    {2, 7, 9},   {2, 7, 25},   {2, 9, 15},
        {2, 15, 17},  {2, 15, 25},  {2, 21, 9},  {3, 1, 14},   {3, 3, 26},
        {3, 3, 28},   {3, 3, 29},   {3, 5, 20},  {3, 5, 22},   {3, 5, 25},
        {3, 7, 29},   {3, 13, 7},   {3, 23, 25}, {3, 25, 24},  {3, 27, 11},
        {4, 3, 17},   {4, 3, 27},   {4, 5, 15},  {5, 3, 21},   {5, 7, 22},
        {5, 9, 7},    {5, 9, 28},   {5, 9, 31},  {5, 13, 6},   {5, 15, 17},
        {5, 17, 13},  {5, 21, 12},  {5, 27, 8},  {5, 27, 21},  {5, 27, 25},
        {5, 27, 28},  {6, 1, 11},   {6, 3, 17},  {6, 17, 9},   {6, 21, 7},
        {6, 21, 13},  {7, 1, 9},    {7, 1, 18},  {7, 1, 25},   {7, 13, 25},
        {7, 17, 21},  {7, 25, 12},  {7, 25, 20}, {8, 7, 23},   {8, 9, 23},
        {9, 5, 1},    {9, 5, 25},   {9, 11, 19}, {9, 21, 16},  {10, 9, 21},
        {10, 9, 25},  {11, 7, 12},  {11, 7, 16}, {11, 17, 13}, {11, 21, 13},
        {12, 9, 23},  {13, 3, 17},  {13, 3, 27}, {13, 5, 19},  {13, 17, 15},
        {14, 1, 15},  {14, 13, 15}, {15, 1, 29}, {17, 15, 20}, {17, 15, 23},
        {17, 15, 26},
    };
    A = parameters[_index][0u];
    B = parameters[_index][1u];
    C = parameters[_index][2u];
  }
  ~ShiftParameters() = default;
};

/*******************************************************************************
** XorShiftGenerator_32
*******************************************************************************/

class XorShiftGenerator_32 final {
public:
  const ShiftParameters Parameters;
  const ShiftType Shift1;
  const ShiftType Shift2;
  const ShiftType Shift3;

  XorShiftGenerator_32()
      : XorShiftGenerator_32(ShiftType::Left, ShiftType::Right, ShiftType::Left,
                             0u, U32(2463534242)) {}
  XorShiftGenerator_32(uint32 _seed)
      : XorShiftGenerator_32(ShiftType::Left, ShiftType::Right, ShiftType::Left,
                             0u, _seed) {}
  XorShiftGenerator_32(ShiftType _shift_1, ShiftType _shift_2,
                       ShiftType _shift_3, size_t _paramters_index,
                       uint32 _seed)
      : Parameters(_paramters_index)
      , Shift1(_shift_1)
      , Shift2(_shift_2)
      , Shift3(_shift_3)
      , state(_seed) {}
  ~XorShiftGenerator_32() = default;

  uint32 Next() {
    if (Shift1 == ShiftType::Left) {
      state ^= state << Parameters.A;
    } else {
      state ^= state >> Parameters.A;
    }
    if (Shift2 == ShiftType::Left) {
      state ^= state << Parameters.B;
    } else {
      state ^= state >> Parameters.B;
    }
    if (Shift3 == ShiftType::Left) {
      state ^= state << Parameters.C;
    } else {
      state ^= state >> Parameters.C;
    }
    return state;
  }

private:
  uint32 state;
};

/*******************************************************************************
** XorShiftGenerator_64
*******************************************************************************/

class XorShiftGenerator_64 final {};

}

#endif
