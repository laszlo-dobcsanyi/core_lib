#ifndef _CORE_LIB_NUMERICS_MATH_HPP_
#define _CORE_LIB_NUMERICS_MATH_HPP_

#ifndef CORE_LIB_NUMERICS
#  error
#endif

#define _USE_MATH_DEFINES
#include <math.h>
#include <cmath>

#ifndef CORE_LIB_EPSILON
#  define CORE_LIB_EPSILON 1.e-5
#endif

////////////////////////////////////////////////////////////////////////////////
// numerics
////////////////////////////////////////////////////////////////////////////////

namespace numerics {

/*******************************************************************************
** sqr
*******************************************************************************/

template<typename ValueType>
inline ValueType sqr(ValueType _value) {
  return _value * _value;
}

/*******************************************************************************
** sqrt
*******************************************************************************/

inline float sqrt(float _value) { return std::sqrt(_value); }
inline double sqrt(double _value) { return std::sqrt(_value); }

/*******************************************************************************
** cos
*******************************************************************************/

inline float cos(float _value) { return std::cos(_value); }
inline double cos(double _value) { return std::cos(_value); }

/*******************************************************************************
** acos
*******************************************************************************/

inline float acos(float _value) { return std::acos(_value); }
inline double acos(double _value) { return std::acos(_value); }

/*******************************************************************************
** sin
*******************************************************************************/

inline float sin(float _value) { return std::sin(_value); }
inline double sin(double _value) { return std::sin(_value); }

/*******************************************************************************
** asin
*******************************************************************************/

inline float asin(float _value) { return std::asin(_value); }
inline double asin(double _value) { return std::asin(_value); }

/*******************************************************************************
** tan
*******************************************************************************/

inline float tan(float _value) { return std::tan(_value); }
inline double tan(double _value) { return std::tan(_value); }

/*******************************************************************************
** atan
*******************************************************************************/

inline float atan(float _value) { return std::atan(_value); }
inline double atan(double _value) { return std::atan(_value); }

/*******************************************************************************
** atan2
*******************************************************************************/

inline float atan2(float _y, float _x) { return std::atan2(_y, _x); }
inline double atan2(double _y, double _x) { return std::atan2(_y, _x); }

/*******************************************************************************
** to_radians
*******************************************************************************/

template<typename ValueType>
inline ValueType to_radians(ValueType _degrees) {
  return _degrees * static_cast<ValueType>(0.01745329251994329576923690768489);
}

/*******************************************************************************
** to_degrees
*******************************************************************************/

template<typename ValueType>
inline ValueType to_degrees(ValueType _radians) {
  return _radians * static_cast<ValueType>(57.295779513082320876798154814105);
}

/*******************************************************************************
** floor
*******************************************************************************/

template<typename ValueType>
inline ValueType floor(ValueType _value) {
  return std::floor(_value);
}

/*******************************************************************************
** ceil
*******************************************************************************/

template<typename ValueType>
inline ValueType ceil(ValueType _value) {
  return std::ceil(_value);
}

/*******************************************************************************
** clamp
*******************************************************************************/

template<typename ValueType, typename BoundType>
inline ValueType clamp(ValueType _value, BoundType _floor, BoundType _ceil) {
  return core::max(static_cast<ValueType>(_floor),
                   core::min(_value, static_cast<ValueType>(_ceil)));
}

/*******************************************************************************
** distance
*******************************************************************************/

template<typename T>
inline mpl::if_signed_t<T, T> distance(T _value1, T _value2) {
  return _value1 - _value2;
}

template<typename T>
inline mpl::if_unsigned_t<T, T> distance(T _value1, T _value2) {
  return _value1 < _value2 ? _value2 - _value1 : _value1 - _value2;
}

/*******************************************************************************
** is_nearly_zero
*******************************************************************************/

inline bool is_nearly_zero(int8 _value) { return _value == I8(0); }
inline bool is_nearly_zero(uint8 _value) { return _value == U8(0); }
inline bool is_nearly_zero(int16 _value) { return _value == I16(0); }
inline bool is_nearly_zero(uint16 _value) { return _value == U16(0); }
inline bool is_nearly_zero(int32 _value) { return _value == I32(0); }
inline bool is_nearly_zero(uint32 _value) { return _value == U32(0); }
inline bool is_nearly_zero(int64 _value) { return _value == I64(0); }
inline bool is_nearly_zero(uint64 _value) { return _value == U64(0); }
inline bool is_nearly_zero(float _value) {
  return std::abs(_value) < static_cast<float>(CORE_LIB_EPSILON);
}
inline bool is_nearly_zero(double _value) {
  return std::abs(_value) < static_cast<double>(CORE_LIB_EPSILON);
}

/*******************************************************************************
** are_nearly_equal
*******************************************************************************/

template<typename T>
inline bool are_nearly_equal(T _value1, T _value2) {
  return is_nearly_zero(distance(_value1, _value2));
}

/*******************************************************************************
** lerp
*******************************************************************************/

template<typename ValueType, typename AlphaType,
         class = mpl::if_arithmetic_t<ValueType>>
inline ValueType lerp(ValueType _min, ValueType _max, AlphaType _alpha) {
  return _min +
         static_cast<ValueType>((static_cast<AlphaType>(_max - _min) * _alpha));
}

}

#endif
