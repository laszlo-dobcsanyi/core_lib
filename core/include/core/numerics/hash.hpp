#ifndef _CORE_LIB_NUMERICS_HASH_HPP_
#define _CORE_LIB_NUMERICS_HASH_HPP_

#ifndef CORE_LIB_NUMERICS
#  error
#endif

#define CORE_LIB_NUMERICS_HASHES
#include "core/numerics/hashes/fnv.hpp"
#include "core/numerics/hashes/crc.hpp"
#undef CORE_LIB_NUMERICS_HASHES

#endif
