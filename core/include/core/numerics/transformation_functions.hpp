#ifndef _CORE_LIB_NUMERICS_TRANSFORMATION_FUNCTIONS_HPP_
#define _CORE_LIB_NUMERICS_TRANSFORMATION_FUNCTIONS_HPP_

#ifndef CORE_LIB_NUMERICS
#  error
#endif

////////////////////////////////////////////////////////////////////////////////
// numerics
////////////////////////////////////////////////////////////////////////////////

namespace numerics {

/*******************************************************************************
** set_zero
*******************************************************************************/

inline void set_zero(Transformation & _transformation) {
  _transformation.translation = Vector3f(0.f, 0.f, 0.f);
  _transformation.rotation = Quaternionf(0.f, 0.f, 0.f, 0.f);
}

/*******************************************************************************
** set_identity
*******************************************************************************/

inline void set_identity(Transformation & _transformation) {
  _transformation.translation = Vector3f(0.f, 0.f, 0.f);
  _transformation.rotation = Quaternionf(0.f, 0.f, 0.f, 1.f);
}

/*******************************************************************************
** set_translation
*******************************************************************************/

inline void set_translation(Transformation & _transformation,
                            Vector3f const & _translation) {
  _transformation.translation = _translation;
}

/*******************************************************************************
** to_translation
*******************************************************************************/

inline Vector3f to_translation(Transformation const & _transformation) {
  return _transformation.translation;
}

/*******************************************************************************
** set_rotation
*******************************************************************************/

inline void set_rotation(Transformation & _transformation,
                         Quaternionf const & _rotation) {
  _transformation.rotation = _rotation;
}

/*******************************************************************************
** to_rotation
*******************************************************************************/

inline Quaternionf to_rotation(Transformation const & _transformation) {
  return _transformation.rotation;
}

/*******************************************************************************
** set_matrix
*******************************************************************************/

inline void set_matrix(RowMajorMatrix4x4f & _matrix,
                       Transformation const & _transformation) {
  set_identity(_matrix);
  set_translation(_matrix, _transformation.translation);
  set_rotation(_matrix, _transformation.rotation);
}

inline void set_matrix(ColumnMajorMatrix4x4f & _matrix,
                       Transformation const & _transformation) {
  set_identity(_matrix);
  set_translation(_matrix, _transformation.translation);
  set_rotation(_matrix, _transformation.rotation);
}

/*******************************************************************************
** set_transformation
*******************************************************************************/

inline void set_transformation(Transformation & _transformation,
                               RowMajorMatrix4x4f const & _matrix) {
  _transformation.translation = get_translation(_matrix);
  _transformation.rotation = get_rotation(_matrix);
}

inline void set_transformation(Transformation & _transformation,
                               ColumnMajorMatrix4x4f const & _matrix) {
  _transformation.translation = get_translation(_matrix);
  _transformation.rotation = get_rotation(_matrix);
}

/*******************************************************************************
** add
*******************************************************************************/

inline Transformation operator+(Transformation const & _lhs,
                                RowMajorMatrix4x4f const & _rhs) {
  RowMajorMatrix4x4f matrix;
  set_matrix(matrix, _lhs);
  Transformation result;
  set_transformation(result, matrix + _rhs);
  return result;
}

inline Transformation operator+(Transformation const & _lhs,
                                ColumnMajorMatrix4x4f const & _rhs) {
  ColumnMajorMatrix4x4f matrix;
  set_matrix(matrix, _lhs);
  Transformation result;
  set_transformation(result, matrix + _rhs);
  return result;
}

/*******************************************************************************
** mul
*******************************************************************************/

inline RowMajorMatrix4x4f operator*(Transformation const & _lhs,
                                    RowMajorMatrix4x4f const & _rhs) {
  RowMajorMatrix4x4f matrix;
  set_matrix(matrix, _lhs);
  return matrix * _rhs;
}

inline RowMajorMatrix4x4f operator*(RowMajorMatrix4x4f const & _lhs,
                                    Transformation const & _rhs) {
  RowMajorMatrix4x4f matrix;
  set_matrix(matrix, _rhs);
  return _lhs * matrix;
}

inline ColumnMajorMatrix4x4f operator*(Transformation const & _lhs,
                                       ColumnMajorMatrix4x4f const & _rhs) {
  ColumnMajorMatrix4x4f matrix;
  set_matrix(matrix, _lhs);
  return matrix * _rhs;
}

inline ColumnMajorMatrix4x4f operator*(ColumnMajorMatrix4x4f const & _lhs,
                                       Transformation const & _rhs) {
  ColumnMajorMatrix4x4f matrix;
  set_matrix(matrix, _rhs);
  return _lhs * matrix;
}

}

#endif