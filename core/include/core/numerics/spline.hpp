#ifndef _CORE_LIB_NUMERICS_SPLINE_HPP_
#define _CORE_LIB_NUMERICS_SPLINE_HPP_

#include "core/containers.hpp"
#include "core/ranges/span.hpp"

#ifndef CORE_LIB_NUMERICS
#  error
#endif

////////////////////////////////////////////////////////////////////////////////
// numerics
////////////////////////////////////////////////////////////////////////////////

namespace numerics {

/*******************************************************************************
** de_boor
*******************************************************************************/

namespace detail {

inline Vector2f de_boor_generic(float _parameter,
                                core::span<const Vector2f> _control_points,
                                size_t _order, size_t _degree,
                                size_t _interval) {
  CORE_LIB_ASSERT(_degree <= _interval && _interval < _control_points.size());
  if (_degree != 0u) {
    const auto weight = (_parameter - static_cast<float>(_interval)) /
                        static_cast<float>(_order - _degree + 1u);
    CORE_LIB_ASSERT(0.f <= weight && weight <= 1.f);
    const auto operand1 = de_boor_generic(_parameter, _control_points, _order,
                                          _degree - 1u, _interval - 1u);
    const auto operand2 = de_boor_generic(_parameter, _control_points, _order,
                                          _degree - 1u, _interval);
    return operand1 * (1.f - weight) + operand2 * weight;
  }
  return _control_points[_interval];
}

}

inline Vector2f de_boor(float _parameter,
                        core::span<const Vector2f> _control_points) {
  const auto interval = static_cast<size_t>(_parameter);
  CORE_LIB_ASSERT(3u <= interval);
  return detail::de_boor_generic(_parameter, _control_points, 3u, 3u, interval);
}

/*******************************************************************************
** Spline
*******************************************************************************/

class Spline {
public:
  Spline() = default;
  Spline(Spline &&) = default;
  Spline(Spline const &) = delete;
  Spline & operator=(Spline &&) = default;
  Spline & operator=(Spline const &) = delete;
  ~Spline() = default;

  void Add(Vector2f const & _control_point) {
    control_points.Create(_control_point);
  }

  Vector2f operator()(float _parameter) const {
    return de_boor(_parameter, core::make_span(control_points));
  }

  container::Array<Vector2f> const & ControlPoints() const {
    return control_points;
  }

private:
  container::Array<Vector2f> control_points;
};

}

#endif
