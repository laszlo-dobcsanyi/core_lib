#ifndef _CORE_LIB_NUMERICS_MATRIX_HPP_
#define _CORE_LIB_NUMERICS_MATRIX_HPP_

#ifndef CORE_LIB_NUMERICS
#  error
#endif

////////////////////////////////////////////////////////////////////////////////
// numerics
////////////////////////////////////////////////////////////////////////////////

namespace numerics {

/*******************************************************************************
** RowMajorMatrix
*******************************************************************************/

template<typename Storage>
struct RowMajorMatrix {
  using StorageType = Storage;
  using ValueType = typename Storage::ValueType;
  using ColumnVector = typename Storage::ColumnVector;
  using RowVector = typename Storage::RowVector;
  using RowVectors = RowVector[Storage::Columns];

  Storage storage;

  RowMajorMatrix() = default;
  RowMajorMatrix(typename Storage::Array _components);
  RowMajorMatrix(RowVectors _vectors);
  RowMajorMatrix(RowMajorMatrix &&) = default;
  RowMajorMatrix(RowMajorMatrix const &) = default;
  RowMajorMatrix & operator=(RowMajorMatrix &&) = default;
  RowMajorMatrix & operator=(RowMajorMatrix const &) = default;
  ~RowMajorMatrix() = default;

  ColumnVector Column(size_t _column_index) const;

  RowVector & operator[](size_t _row_index);
  RowVector const & operator[](size_t _row_index) const;

  ValueType & operator()(size_t _row_index, size_t _column_index);
  ValueType operator()(size_t _row_index, size_t _column_index) const;
};

/*******************************************************************************
** ColumnMajorMatrix
*******************************************************************************/

template<typename Storage>
struct ColumnMajorMatrix {
  using StorageType = Storage;
  using ValueType = typename Storage::ValueType;
  using ColumnVector = typename Storage::ColumnVector;
  using RowVector = typename Storage::RowVector;
  using ColumnVectors = ColumnVector[Storage::Rows];

  Storage storage;

  ColumnMajorMatrix() = default;
  ColumnMajorMatrix(typename Storage::Array _components);
  ColumnMajorMatrix(ColumnVectors _vectors);
  ColumnMajorMatrix(ColumnMajorMatrix &&) = default;
  ColumnMajorMatrix(ColumnMajorMatrix const &) = default;
  ColumnMajorMatrix & operator=(ColumnMajorMatrix &&) = default;
  ColumnMajorMatrix & operator=(ColumnMajorMatrix const &) = default;
  ~ColumnMajorMatrix() = default;

  RowVector Row(size_t _row_index) const;

  ColumnVector & operator[](size_t _column_index);
  ColumnVector const & operator[](size_t _column_index) const;

  ValueType & operator()(size_t _row_index, size_t _column_index);
  ValueType operator()(size_t _row_index, size_t _column_index) const;
};

}

#endif
