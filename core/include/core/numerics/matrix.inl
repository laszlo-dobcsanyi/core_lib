#ifndef _CORE_LIB_NUMERICS_MATRIX_INL_
#define _CORE_LIB_NUMERICS_MATRIX_INL_

#ifndef CORE_LIB_NUMERICS
#  error
#endif

////////////////////////////////////////////////////////////////////////////////
// numerics
////////////////////////////////////////////////////////////////////////////////

namespace numerics {

/*******************************************************************************
** RowMajorMatrix
*******************************************************************************/

template<typename Storage>
inline RowMajorMatrix<Storage>::RowMajorMatrix(
    typename Storage::Array _components)
    : storage(_components) {}

template<typename Storage>
inline typename Storage::ColumnVector
RowMajorMatrix<Storage>::Column(size_t _column_index) const {
  typename Storage::ColumnVector result;
  for (auto row = 0u; row < Storage::Rows; ++row) {
    result[row] = storage[row * Storage::Columns + _column_index];
  }
  return result;
}

template<typename Storage>
inline typename Storage::RowVector &
RowMajorMatrix<Storage>::operator[](size_t _row_index) {
  auto & component = storage[_row_index * Storage::Columns];
  // TODO: Assert that storage.operator[] return sequential locations!
  return *reinterpret_cast<typename Storage::RowVector *>(&component);
}

template<typename Storage>
inline typename Storage::RowVector const &
RowMajorMatrix<Storage>::operator[](size_t _row_index) const {
  auto const & component = storage[_row_index * Storage::Columns];
  // TODO: Assert that storage.operator[] return sequential locations!
  return *reinterpret_cast<typename Storage::RowVector const *>(&component);
}

template<typename Storage>
inline typename Storage::ValueType &
RowMajorMatrix<Storage>::operator()(size_t _row_index, size_t _column_index) {
  return storage[_row_index * Storage::Columns + _column_index];
}

template<typename Storage>
inline typename Storage::ValueType
RowMajorMatrix<Storage>::operator()(size_t _row_index,
                                    size_t _column_index) const {
  return storage[_row_index * Storage::Columns + _column_index];
}

/*******************************************************************************
** ColumnMajorMatrix
*******************************************************************************/

template<typename Storage>
inline ColumnMajorMatrix<Storage>::ColumnMajorMatrix(
    typename Storage::Array _components)
    : storage(_components) {}

template<typename Storage>
inline ColumnMajorMatrix<Storage>::ColumnMajorMatrix(ColumnVectors _vectors)
    : ColumnMajorMatrix(reinterpret_cast<typename Storage::Array>(_vectors)) {}

template<typename Storage>
inline typename Storage::RowVector
ColumnMajorMatrix<Storage>::Row(size_t _row_index) const {
  typename Storage::RowVector result;
  for (auto column = 0u; column < Storage::Columns; ++column) {
    result[column] = storage[column * Storage::Rows + _row_index];
  }
  return result;
}

template<typename Storage>
inline typename Storage::ColumnVector &
ColumnMajorMatrix<Storage>::operator[](size_t _column_index) {
  auto & component = storage[_column_index * Storage::Rows];
  // TODO: Assert that storage.operator[] return sequential locations!
  return *reinterpret_cast<typename Storage::ColumnVector *>(&component);
}

template<typename Storage>
inline typename Storage::ColumnVector const &
ColumnMajorMatrix<Storage>::operator[](size_t _column_index) const {
  auto const & component = storage[_column_index * Storage::Rows];
  // TODO: Assert that storage.operator[] return sequential locations!
  return *reinterpret_cast<typename Storage::ColumnVector const *>(&component);
}

template<typename Storage>
inline typename Storage::ValueType &
ColumnMajorMatrix<Storage>::operator()(size_t _row_index,
                                       size_t _column_index) {
  return storage[_column_index * Storage::Rows + _row_index];
}

template<typename Storage>
inline typename Storage::ValueType
ColumnMajorMatrix<Storage>::operator()(size_t _row_index,
                                       size_t _column_index) const {
  return storage[_column_index * Storage::Rows + _row_index];
}

}

#endif
