#ifndef _CORE_LIB_NUMERICS_QUATERNION_HPP_
#define _CORE_LIB_NUMERICS_QUATERNION_HPP_

#ifndef CORE_LIB_NUMERICS
#  error
#endif

////////////////////////////////////////////////////////////////////////////////
// numerics detail
////////////////////////////////////////////////////////////////////////////////

namespace numerics { namespace detail {

/*******************************************************************************
** Quaternion
*******************************************************************************/

template<typename ValueType>
struct Quaternion {
  ValueType x;
  ValueType y;
  ValueType z;
  ValueType w;

  Quaternion();
  Quaternion(ValueType _x, ValueType _y, ValueType _z, ValueType _w);
  Quaternion(Vector4<ValueType> const & _vector);
  Quaternion(Vector3<ValueType> const & _vector, ValueType _scalar);
  Quaternion(Quaternion &&) = default;
  Quaternion(Quaternion const &) = default;
  Quaternion & operator=(Quaternion &&) = default;
  Quaternion & operator=(Quaternion const &) = default;
  ~Quaternion() = default;

  Vector3<ValueType> GetVector() const;
  ValueType GetScalar() const;
};

/*******************************************************************************
** Quaternion
*******************************************************************************/

template<typename ValueType>
inline Quaternion<ValueType>::Quaternion() {}

template<typename ValueType>
inline Quaternion<ValueType>::Quaternion(ValueType _x, ValueType _y,
                                         ValueType _z, ValueType _w)
    : x(_x)
    , y(_y)
    , z(_z)
    , w(_w) {}

template<typename ValueType>
inline Quaternion<ValueType>::Quaternion(Vector4<ValueType> const & _vector)
    : x(_vector.x)
    , y(_vector.y)
    , z(_vector.z)
    , w(_vector.w) {}

template<typename ValueType>
inline Quaternion<ValueType>::Quaternion(Vector3<ValueType> const & _vector,
                                         ValueType _scalar)
    : x(_vector.x)
    , y(_vector.y)
    , z(_vector.z)
    , w(_scalar) {}

template<typename ValueType>
inline Vector3<ValueType> Quaternion<ValueType>::GetVector() const {
  return Vector3<ValueType>(x, y, z);
}

template<typename ValueType>
inline ValueType Quaternion<ValueType>::GetScalar() const {
  return w;
}

template<typename ValueType>
inline Quaternion<ValueType> & operator+=(Quaternion<ValueType> & _lhs,
                                          Quaternion<ValueType> const & _rhs) {
  _lhs.x += _rhs.x;
  _lhs.y += _rhs.y;
  _lhs.z += _rhs.z;
  _lhs.w += _rhs.w;
  return _lhs;
}

template<typename ValueType>
inline Quaternion<ValueType> operator+(Quaternion<ValueType> const & _lhs,
                                       Quaternion<ValueType> const & _rhs) {
  return Quaternion<ValueType>(_lhs.x + _rhs.x, _lhs.y + _rhs.y,
                               _lhs.z + _rhs.z, _lhs.w + _rhs.w);
}

template<typename ValueType>
inline Quaternion<ValueType> & operator-=(Quaternion<ValueType> & _lhs,
                                          Quaternion<ValueType> const & _rhs) {
  _lhs.x -= _rhs.x;
  _lhs.y -= _rhs.y;
  _lhs.z -= _rhs.z;
  _lhs.w -= _rhs.w;
  return _lhs;
}

template<typename ValueType>
inline Quaternion<ValueType> operator-(Quaternion<ValueType> const & _lhs,
                                       Quaternion<ValueType> const & _rhs) {
  return Quaternion<ValueType>(_lhs.x - _rhs.x, _lhs.y - _rhs.y,
                               _lhs.z - _rhs.z, _lhs.w - _rhs.w);
}

template<typename ValueType>
inline Quaternion<ValueType>
operator-(Quaternion<ValueType> const & _quaternion) {
  return Quaternion<ValueType>(-_quaternion.x, -_quaternion.y, -_quaternion.z,
                               -_quaternion.w);
}

template<typename ValueType>
inline Quaternion<ValueType> & operator*=(Quaternion<ValueType> & _quaternion,
                                          ValueType _scalar) {
  _quaternion.x *= _scalar;
  _quaternion.y *= _scalar;
  _quaternion.z *= _scalar;
  _quaternion.w *= _scalar;
  return _quaternion;
}

template<typename ValueType>
inline Quaternion<ValueType> operator*(Quaternion<ValueType> const & _p,
                                       Quaternion<ValueType> const & _q) {
  return Quaternion<ValueType>(
      _p.w * _q.x + _p.x * _q.w + _p.y * _q.z - _p.z * _q.y,
      _p.w * _q.y + _p.y * _q.w + _p.z * _q.x - _p.x * _q.z,
      _p.w * _q.z + _p.z * _q.w + _p.x * _q.y - _p.y * _q.x,
      _p.w * _q.w - _p.x * _q.x - _p.y * _q.y - _p.z * _q.z);
}

template<typename ValueType>
inline Quaternion<ValueType>
operator*(Quaternion<ValueType> const & _quaternion, ValueType _scalar) {
  return Quaternion<ValueType>(_quaternion.x * _scalar, _quaternion.y * _scalar,
                               _quaternion.z * _scalar,
                               _quaternion.w * _scalar);
}

template<typename ValueType>
inline Quaternion<ValueType>
operator*(ValueType _scalar, Quaternion<ValueType> const & _quaternion) {
  return _quaternion * _scalar;
}

}}

////////////////////////////////////////////////////////////////////////////////
// numerics
////////////////////////////////////////////////////////////////////////////////

namespace numerics {

using Quaternion = detail::Quaternion<float>;
using Quaternionf = detail::Quaternion<float>;
using Quaterniond = detail::Quaternion<double>;

}

#endif
