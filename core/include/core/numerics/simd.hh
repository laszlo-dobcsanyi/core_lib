#ifndef _CORE_LIB_SIMD_HH_
#define _CORE_LIB_SIMD_HH_

#if !defined(CORE_LIB_NO_SIMD)
#  if defined(CORE_LIB_COMPILER_CLANG) || defined(CORE_LIB_COMPILER_GCC)
#    if defined(__AVX2__)
#      define CORE_LIB_AVX2
#    endif
#    if defined(__AVX__)
#      define CORE_LIB_AVX
#    endif
#    if defined(__SSE4_2__)
#      define CORE_LIB_SSE4_2
#    endif
#    if defined(__SSE4_1__)
#      define CORE_LIB_SSE4_1
#    endif
#    if defined(__SSE3__)
#      define CORE_LIB_SSE3
#    endif
#    if defined(__SSE2__)
#      define CORE_LIB_SSE2
#    endif
#    if defined(__SSE__)
#      define CORE_LIB_SSE
#    endif
#  elif defined(CORE_LIB_COMPILER_MSVC)
#    if defined(__AVX2__)
#      define CORE_LIB_AVX2
#      define CORE_LIB_AVX
#      define CORE_LIB_SSE2 // x64
#      define CORE_LIB_SSE2 // x32
#      define CORE_LIB_SSE
#    elif defined(__AVX__)
#      define CORE_LIB_AVX
#      define CORE_LIB_SSE2 // x64
#      define CORE_LIB_SSE2 // x32
#      define CORE_LIB_SSE
#    elif defined(_M_AMD64) || defined(_M_X64)
#      define CORE_LIB_SSE2 // x64
#      define CORE_LIB_SSE2 // x32
#      define CORE_LIB_SSE
#    elif _M_IX86_FP == 2
#      define CORE_LIB_SSE2 // x32
#      define CORE_LIB_SSE
#    elif _M_IX86_FP == 1
#      define CORE_LIB_SSE
#    endif
#  endif

#  if !defined(CORE_LIB_SSE)
#    define CORE_LIB_NO_SIMD
#  endif
#endif

#endif
