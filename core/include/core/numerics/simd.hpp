#ifndef _CORE_LIB_SIMD_HPP_
#define _CORE_LIB_SIMD_HPP_

#include "core/numerics/simd.hh"

#ifndef CORE_LIB_NO_SIMD
#  if defined(CORE_LIB_COMPILER_MSVC)
#    include <intrin.h>
#  elif defined(CORE_LIB_COMPILER_CLANG) || defined(CORE_LIB_COMPILER_GCC)
#    include <x86intrin.h>
#  endif
#endif

// TODO: Add scalar vector
// TODO: Add hardware vectors

#endif
