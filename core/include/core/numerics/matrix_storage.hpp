#ifndef _CORE_LIB_NUMERICS_MATRIX_STORAGE_HPP_
#define _CORE_LIB_NUMERICS_MATRIX_STORAGE_HPP_

#ifndef CORE_LIB_NUMERICS
#  error
#endif

////////////////////////////////////////////////////////////////////////////////
// numerics matrix
////////////////////////////////////////////////////////////////////////////////

namespace numerics { namespace matrix {

/*******************************************************************************
** Order
*******************************************************************************/

enum class Order {
  RowMajor,
  ColumnMajor,
};

/*******************************************************************************
** StorageType
*******************************************************************************/

template<typename _ValueType, size_t _Rows, size_t _Columns>
struct StorageType {
  using ValueType = _ValueType;
  static const size_t Rows = _Rows;
  static const size_t Columns = _Columns;
  static const size_t Elements = Rows * Columns;
  using Row = ValueType[Rows];
  using Column = ValueType[Columns];
  using Array = ValueType[Rows * Columns];
  using RowVector = numerics::detail::Vector<ValueType, Columns>;
  using ColumnVector = numerics::detail::Vector<ValueType, Rows>;
};

}}

#if defined(CORE_LIB_AVX2)
#  include "core/numerics/storage/avx2.hpp"
#elif defined(CORE_LIB_AVX)
#  include "core/numerics/storage/avx.hpp"
#elif defined(CORE_LIB_SSE2)
#  include "core/numerics/storage/sse2.hpp"
#elif defined(CORE_LIB_SSE)
#  include "core/numerics/storage/sse.hpp"
#else
#  include "core/numerics/storage/fpu.hpp"
#endif

////////////////////////////////////////////////////////////////////////////////
// numerics matrix
////////////////////////////////////////////////////////////////////////////////

namespace numerics { namespace matrix {

/*******************************************************************************
** DefaultStorage
*******************************************************************************/

#if defined(CORE_LIB_AVX2)
template<typename ValueType, size_t Rows, size_t Columns>
using DefaultStorage = matrix::storage::AVX2<ValueType, Rows, Columns>;
#elif defined(CORE_LIB_AVX)
template<typename ValueType, size_t Rows, size_t Columns>
using DefaultStorage = matrix::storage::AVX<ValueType, Rows, Columns>;
#elif defined(CORE_LIB_SSE2)
template<typename ValueType, size_t Rows, size_t Columns>
using DefaultStorage = matrix::storage::SSE2<ValueType, Rows, Columns>;
#elif defined(CORE_LIB_SSE)
template<typename ValueType, size_t Rows, size_t Columns>
using DefaultStorage = matrix::storage::SSE<ValueType, Rows, Columns>;
#else
template<typename ValueType, size_t Rows, size_t Columns>
using DefaultStorage = matrix::storage::FPU<ValueType, Rows, Columns>;
#endif

}}

#endif
