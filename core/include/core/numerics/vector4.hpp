#ifndef _CORE_NUMERICS_VECTOR4_HPP_
#define _CORE_NUMERICS_VECTOR4_HPP_

#ifndef CORE_LIB_NUMERICS
#  error
#endif

////////////////////////////////////////////////////////////////////////////////
// numerics detail
////////////////////////////////////////////////////////////////////////////////

namespace numerics { namespace detail {

/*******************************************************************************
** Vector4
*******************************************************************************/

template<typename ValueType>
struct Vector<ValueType, 4u> {
  ValueType x;
  ValueType y;
  ValueType z;
  ValueType w;

  Vector();
  Vector(ValueType _x, ValueType _y, ValueType _z, ValueType _w);
  Vector(Vector<ValueType, 2u> const & _vector, ValueType _z, ValueType _w);
  Vector(Vector<ValueType, 3u> const & _vector, ValueType _w);
  Vector(Vector &&) = default;
  Vector(Vector const &) = default;
  Vector & operator=(Vector &&) = default;
  Vector & operator=(Vector const &) = default;
  ~Vector() = default;

  ValueType & operator[](size_t _index);
  ValueType const & operator[](size_t _index) const;
};

template<typename ValueType>
inline Vector<ValueType, 4u>::Vector() {}

template<typename ValueType>
inline Vector<ValueType, 4u>::Vector(ValueType _x, ValueType _y, ValueType _z,
                                     ValueType _w)
    : x(_x)
    , y(_y)
    , z(_z)
    , w(_w) {}

template<typename ValueType>
inline Vector<ValueType, 4u>::Vector(Vector<ValueType, 2u> const & _vector,
                                     ValueType _z, ValueType _w)
    : x(_vector.x)
    , y(_vector.y)
    , z(_z)
    , w(_w) {}
template<typename ValueType>
inline Vector<ValueType, 4u>::Vector(Vector<ValueType, 3u> const & _vector,
                                     ValueType _w)
    : x(_vector.x)
    , y(_vector.y)
    , z(_vector.z)
    , w(_w) {}

template<typename ValueType>
ValueType & Vector<ValueType, 4u>::operator[](size_t _index) {
  CORE_LIB_ASSERT(_index < 4u);
  return _index == 0 ? x : _index == 1 ? y : _index == 2 ? z : w;
}

template<typename ValueType>
ValueType const & Vector<ValueType, 4u>::operator[](size_t _index) const {
  CORE_LIB_ASSERT(_index < 4u);
  return _index == 0 ? x : _index == 1 ? y : _index == 2 ? z : w;
}

template<typename ValueType1, typename ValueType2>
inline Vector<ValueType1, 4u> &
operator+=(Vector<ValueType1, 4u> & _lhs, Vector<ValueType2, 4u> const & _rhs) {
  _lhs.x += _rhs.x;
  _lhs.y += _rhs.y;
  _lhs.z += _rhs.z;
  _lhs.w += _rhs.w;
  return _lhs;
}

template<typename ValueType>
inline Vector<ValueType, 4u> & operator+=(Vector<ValueType, 4u> & _lhs,
                                          ValueType _rhs) {
  _lhs.x += _rhs;
  _lhs.y += _rhs;
  _lhs.z += _rhs;
  _lhs.w += _rhs;
  return _lhs;
}

template<typename ValueType>
inline Vector<ValueType, 4u> operator+(Vector<ValueType, 4u> const & _lhs,
                                       Vector<ValueType, 4u> const & _rhs) {
  return Vector<ValueType, 4u>(_lhs.x + _rhs.x, _lhs.y + _rhs.y,
                               _lhs.z + _rhs.z, _lhs.w + _rhs.w);
}

template<typename ValueType>
inline Vector<ValueType, 4u> operator+(Vector<ValueType, 4u> const & _lhs,
                                       ValueType _rhs) {
  return Vector<ValueType, 4u>(_lhs.x + _rhs, _lhs.y + _rhs, _lhs.z + _rhs,
                               _lhs.w + _rhs);
}

template<typename ValueType>
inline Vector<ValueType, 4u> operator+(ValueType _lhs,
                                       Vector<ValueType, 4u> const & _rhs) {
  return _rhs + _lhs;
}

template<typename ValueType1, typename ValueType2>
inline Vector<ValueType1, 4u> &
operator-=(Vector<ValueType1, 4u> & _lhs, Vector<ValueType2, 4u> const & _rhs) {
  _lhs.x -= _rhs.x;
  _lhs.y -= _rhs.y;
  _lhs.z -= _rhs.z;
  _lhs.w -= _rhs.w;
  return _lhs;
}

template<typename ValueType>
inline Vector<ValueType, 4u> & operator-=(Vector<ValueType, 4u> & _lhs,
                                          ValueType _rhs) {
  _lhs.x -= _rhs;
  _lhs.y -= _rhs;
  _lhs.z -= _rhs;
  _lhs.w -= _rhs;
  return _lhs;
}

template<typename ValueType>
inline Vector<ValueType, 4u> operator-(Vector<ValueType, 4u> const & _lhs,
                                       Vector<ValueType, 4u> const & _rhs) {
  return Vector<ValueType, 4u>(_lhs.x - _rhs.x, _lhs.y - _rhs.y,
                               _lhs.z - _rhs.z, _lhs.w - _rhs.w);
}

template<typename ValueType>
inline Vector<ValueType, 4u> operator-(Vector<ValueType, 4u> const & _lhs,
                                       ValueType _rhs) {
  return Vector<ValueType, 4u>(_lhs.x - _rhs, _lhs.y - _rhs, _lhs.z - _rhs,
                               _lhs.w - _rhs);
}

template<typename ValueType>
inline Vector<ValueType, 4u> operator-(ValueType _lhs,
                                       Vector<ValueType, 4u> const & _rhs) {
  return _rhs - _lhs;
}

template<typename ValueType1, typename ValueType2>
inline Vector<ValueType1, 4u> &
operator*=(Vector<ValueType1, 4u> & _lhs, Vector<ValueType2, 4u> const & _rhs) {
  _lhs.x *= _rhs.x;
  _lhs.y *= _rhs.y;
  _lhs.z *= _rhs.z;
  _lhs.w *= _rhs.w;
  return _lhs;
}

template<typename ValueType>
inline Vector<ValueType, 4u> & operator*=(Vector<ValueType, 4u> & _lhs,
                                          ValueType _rhs) {
  _lhs.x *= _rhs;
  _lhs.y *= _rhs;
  _lhs.z *= _rhs;
  _lhs.w *= _rhs;
  return _lhs;
}

template<typename ValueType>
inline Vector<ValueType, 4u> operator*(Vector<ValueType, 4u> const & _lhs,
                                       Vector<ValueType, 4u> const & _rhs) {
  return Vector<ValueType, 4u>(_lhs.x * _rhs.x, _lhs.y * _rhs.y,
                               _lhs.z * _rhs.z, _lhs.w * _rhs.w);
}

template<typename ValueType>
inline Vector<ValueType, 4u> operator*(Vector<ValueType, 4u> const & _lhs,
                                       ValueType _rhs) {
  return Vector<ValueType, 4u>(_lhs.x * _rhs, _lhs.y * _rhs, _lhs.z * _rhs,
                               _lhs.w * _rhs);
}

template<typename ValueType>
inline Vector<ValueType, 4u> operator*(ValueType _lhs,
                                       Vector<ValueType, 4u> const & _rhs) {
  return _rhs * _lhs;
}

template<typename ValueType1, typename ValueType2>
inline Vector<ValueType1, 4u> &
operator/=(Vector<ValueType1, 4u> & _lhs, Vector<ValueType2, 4u> const & _rhs) {
  if (!is_nearly_zero(_rhs.x) && !is_nearly_zero(_rhs.y) &&
      !is_nearly_zero(_rhs.z) && !is_nearly_zero(_rhs.w)) {
    _lhs.x /= _rhs.x;
    _lhs.y /= _rhs.y;
    _lhs.z /= _rhs.z;
    _lhs.w /= _rhs.w;
  }
  return _lhs;
}

template<typename ValueType>
inline Vector<ValueType, 4u> & operator/=(Vector<ValueType, 4u> & _lhs,
                                          ValueType _rhs) {
  if (!is_nearly_zero(_rhs)) {
    _lhs.x /= _rhs;
    _lhs.y /= _rhs;
    _lhs.z /= _rhs;
    _lhs.w /= _rhs;
  }
  return _lhs;
}

template<typename ValueType>
inline Vector<ValueType, 4u> operator/(Vector<ValueType, 4u> const & _lhs,
                                       Vector<ValueType, 4u> const & _rhs) {
  if (!is_nearly_zero(_rhs.x) && !is_nearly_zero(_rhs.y) &&
      !is_nearly_zero(_rhs.z) && !is_nearly_zero(_rhs.w)) {
    return Vector<ValueType, 4u>(_lhs.x / _rhs.x, _lhs.y / _rhs.y,
                                 _lhs.z / _rhs.z, _lhs.w / _rhs.w);
  }
  return _lhs;
}

template<typename ValueType>
inline Vector<ValueType, 4u> operator/(Vector<ValueType, 4u> const & _lhs,
                                       ValueType _rhs) {
  if (!is_nearly_zero(_rhs)) {
    return Vector<ValueType, 4u>(_lhs.x / _rhs, _lhs.y / _rhs, _lhs.z / _rhs,
                                 _lhs.w / _rhs);
  }
  return _lhs;
}

template<typename ValueType>
inline bool operator==(Vector<ValueType, 4u> const & _lhs,
                       Vector<ValueType, 4u> const & _rhs) {
  return _lhs.x == _rhs.x && _lhs.y == _rhs.y && _lhs.z == _rhs.z &&
         _lhs.w == _rhs.w;
}

template<typename ValueType>
inline bool operator!=(Vector<ValueType, 4u> const & _lhs,
                       Vector<ValueType, 4u> const & _rhs) {
  return !(_lhs == _rhs);
}

}}

////////////////////////////////////////////////////////////////////////////////
// numerics
////////////////////////////////////////////////////////////////////////////////

namespace numerics {

template<typename ValueType>
using Vector4 = detail::Vector<ValueType, 4u>;

using Vector4i8 = Vector4<int8>;
using Vector4u8 = Vector4<uint8>;
using Vector4i16 = Vector4<int16>;
using Vector4u16 = Vector4<uint16>;
using Vector4i32 = Vector4<int32>;
using Vector4u32 = Vector4<uint32>;
using Vector4i64 = Vector4<int64>;
using Vector4u64 = Vector4<uint64>;
using Vector4f = Vector4<float>;
using Vector4d = Vector4<double>;

}

#endif
