#ifndef _CORE_LIB_NUMERICS_QUATERNION_FUNCTIONS_HPP_
#define _CORE_LIB_NUMERICS_QUATERNION_FUNCTIONS_HPP_

#ifndef CORE_LIB_NUMERICS
#  error
#endif

////////////////////////////////////////////////////////////////////////////////
// numerics
////////////////////////////////////////////////////////////////////////////////

namespace numerics {

/*******************************************************************************
** dot
*******************************************************************************/

template<typename ValueType>
inline ValueType dot(detail::Quaternion<ValueType> const & _quaternion1,
                     detail::Quaternion<ValueType> const & _quaternion2) {
  return _quaternion1.x * _quaternion2.x + _quaternion1.y * _quaternion2.y +
         _quaternion1.z * _quaternion2.z + _quaternion1.w * _quaternion2.w;
}

/*******************************************************************************
** length2
*******************************************************************************/

template<typename ValueType>
inline ValueType length2(detail::Quaternion<ValueType> const & _quaternion) {
  return dot(_quaternion, _quaternion);
}

/*******************************************************************************
** length
*******************************************************************************/

template<typename ValueType>
inline ValueType length(detail::Quaternion<ValueType> const & _quaternion) {
  return sqrt(length2(_quaternion));
}

/*******************************************************************************
** negate
*******************************************************************************/

template<typename ValueType>
inline void negate(detail::Quaternion<ValueType> & _quaternion) {
  _quaternion[0u] = -((*_quaternion)[0u]);
  _quaternion[1u] = -((*_quaternion)[1u]);
  _quaternion[2u] = -((*_quaternion)[2u]);
  _quaternion[3u] = -((*_quaternion)[3u]);
}

/*******************************************************************************
** normalize
*******************************************************************************/

template<typename ValueType>
inline void normalize(detail::Quaternion<ValueType> & _quaternion) {
  auto const l = length(*_quaternion);
  if (!is_nearly_zero(l)) {
    _quaternion[0u] /= l;
    _quaternion[1u] /= l;
    _quaternion[2u] /= l;
    _quaternion[3u] /= l;
  }
}

/*******************************************************************************
** normalized
*******************************************************************************/

template<typename ValueType>
inline detail::Quaternion<ValueType>
normalized(detail::Quaternion<ValueType> const & _quaternion) {
  auto const l = length(*_quaternion);
  CORE_LIB_ASSERT(!is_nearly_zero(l));
  return detail::Quaternion<ValueType>(_quaternion.x / l, _quaternion.y / l,
                                       _quaternion.y / l, _quaternion.z / l);
}

/*******************************************************************************
** is_normalized
*******************************************************************************/

template<typename ValueType>
inline bool is_normalized(detail::Quaternion<ValueType> const & _quaternion) {
  return are_nearly_equal(length(_quaternion), ValueType(1));
}

/*******************************************************************************
** rotate
*******************************************************************************/

template<typename ValueType>
inline void rotate(detail::Vector<ValueType, 3u> & _result,
                   detail::Vector<ValueType, 3u> const & _vector,
                   detail::Quaternion<ValueType> const & _quaternion) {
  auto u = _quaternion.GetVector();
  auto s = _quaternion.GetScalar();
  _result = (2 * dot(u, _vector) * u) + ((s * s - dot(u, u)) * _vector) +
            (2 * s * cross(u, _vector));
}

template<typename ValueType>
inline detail::Vector<ValueType, 3u>
rotate(detail::Vector<ValueType, 3u> const & _vector,
       detail::Quaternion<ValueType> const & _quaternion) {
  detail::Vector<ValueType, 3u> result;
  rotate(result, _vector, _quaternion);
  return result;
}

/*******************************************************************************
** lerp
*******************************************************************************/

template<typename ValueType>
inline detail::Quaternion<ValueType>
lerp(detail::Quaternion<ValueType> const & _quaternion1,
     detail::Quaternion<ValueType> const & _quaternion2, ValueType _alpha) {
  detail::Quaternion<ValueType> result =
      _quaternion1 + _alpha * (_quaternion2 - _quaternion1);
  normalize(result);
  return result;
}

/*******************************************************************************
** slerp
*******************************************************************************/

template<typename ValueType>
inline detail::Quaternion<ValueType>
slerp(detail::Quaternion<ValueType> _quaternion1,
      detail::Quaternion<ValueType> _quaternion2, ValueType _alpha) {
  CORE_LIB_ASSERT(is_normalized(_quaternion1));
  CORE_LIB_ASSERT(is_normalized(_quaternion2));
  auto d = dot(_quaternion1, _quaternion2);
  if (d < static_cast<ValueType>(0)) {
    // Take the shorter path by negating
    negate(_quaternion1);
    d = -d;
  }
  if (static_cast<ValueType>(0.9995) < d) {
    // Just interpolate linearly
    return lerp(_quaternion1, _quaternion2, _alpha);
  }
  d = clamp(d, -1, 1);
  auto const theta0 = acos(d);
  auto const theta1 = acos(d) * _alpha;
  auto const scale1 = cos(theta1) - dot * sin(theta1) / sin(theta0);
  auto const scale2 = sin(theta1) / sin(theta0);
  return (_quaternion1 * scale1) + (_quaternion2 * scale2);
}

/*******************************************************************************
** from_euler
*******************************************************************************/

template<typename ValueType>
inline detail::Quaternion<ValueType>
from_euler(Vector3<ValueType> const & _euler_angles) {
  auto c = cos(_euler_angles * static_cast<ValueType>(0.5));
  auto s = sin(_euler_angles * static_cast<ValueType>(0.5));
  return detail::Quaternion<ValueType>(
      s.x * c.y * c.z - c.x * s.y * s.z, c.x * s.y * c.z + s.x * c.y * s.z,
      c.x * c.y * s.z - s.x * s.y * c.z, c.x * c.y * c.z + s.x * s.y * s.z);
}

/*******************************************************************************
** to_rotation
*******************************************************************************/

template<typename ValueType>
inline detail::Quaternion<ValueType>
to_rotation(Vector3<ValueType> const & _rotation) {
  Vector3<ValueType> euler_angles(to_radians(_rotation[0u]),
                                  to_radians(_rotation[1u]),
                                  to_radians(_rotation[2u]));
  return from_euler(euler_angles);
}

/*******************************************************************************
** equal
*******************************************************************************/

template<typename ValueType>
inline bool operator==(detail::Quaternion<ValueType> const & _lhs,
                       detail::Quaternion<ValueType> const & _rhs) {
  ValueType epsilon = (ValueType)CORE_LIB_EPSILON;
  if (!are_nearly_equal(_lhs.x, _rhs.x))
    return false;
  if (!are_nearly_equal(_lhs.y, _rhs.y))
    return false;
  if (!are_nearly_equal(_lhs.z, _rhs.z))
    return false;
  if (!are_nearly_equal(_lhs.w, _rhs.w))
    return false;
  return true;
}

/*******************************************************************************
** equivalent
*******************************************************************************/

template<typename ValueType>
inline bool are_equivalent(detail::Quaternion<ValueType> const & _lhs,
                           detail::Quaternion<ValueType> const & _rhs) {
  return are_nearly_equal(std::abs(dot(_lhs, _rhs)), ValueType(1));
}

}

#endif
