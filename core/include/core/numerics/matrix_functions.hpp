#ifndef _CORE_LIB_NUMERICS_MATRIX_FUNCTIONS_HPP_
#define _CORE_LIB_NUMERICS_MATRIX_FUNCTIONS_HPP_

#ifndef CORE_LIB_NUMERICS
#  error
#endif

////////////////////////////////////////////////////////////////////////////////
// numerics detail
////////////////////////////////////////////////////////////////////////////////

namespace numerics { namespace detail {

template<typename Storage>
struct is_4x4_matrix
    : public mpl::IntegralConstant<bool, Storage::Rows == 4u &&
                                             Storage::Columns == 4u> {};

template<typename Storage, typename Result = void>
using if_4x4_matrix_t = mpl::enable_if_t<is_4x4_matrix<Storage>::value, Result>;
}}

////////////////////////////////////////////////////////////////////////////////
// numerics
////////////////////////////////////////////////////////////////////////////////

namespace numerics {

template<typename Storage>
inline bool is_row_major(GenericMatrix<Storage> const & _matrix) {
  return _matrix.template Is<RowMajorMatrix<Storage>>();
}

template<typename Storage>
inline bool is_column_major(GenericMatrix<Storage> const & _matrix) {
  return _matrix.template Is<ColumnMajorMatrix<Storage>>();
}

template<typename Storage>
inline RowMajorMatrix<Storage> &
as_row_major(GenericMatrix<Storage> & _matrix) {
  return _matrix.template As<RowMajorMatrix<Storage>>();
}

template<typename Storage>
inline RowMajorMatrix<Storage> const &
as_row_major(GenericMatrix<Storage> const & _matrix) {
  return _matrix.template As<RowMajorMatrix<Storage>>();
}

template<typename Storage>
inline ColumnMajorMatrix<Storage> const &
as_column_major(GenericMatrix<Storage> const & _matrix) {
  return _matrix.template As<ColumnMajorMatrix<Storage>>();
}

template<typename ValueType, typename... Args>
inline Matrix4x4<ValueType> make_matrix_4x4(matrix::Order _order,
                                            Args &&... _args) {
  if (_order == matrix::Order::RowMajor) {
    return Matrix4x4<ValueType>(in_place_t<RowMajorMatrix4x4<ValueType>>(),
                                forward<Args>(_args)...);
  } else {
    return Matrix4x4<ValueType>(in_place_t<ColumnMajorMatrix4x4<ValueType>>(),
                                forward<Args>(_args)...);
  }
}

/*******************************************************************************
** set_zero
*******************************************************************************/

template<class Storage>
inline detail::if_4x4_matrix_t<Storage>
set_zero(RowMajorMatrix<Storage> & _matrix) {
  _matrix[0] = {0, 0, 0, 0};
  _matrix[1] = {0, 0, 0, 0};
  _matrix[2] = {0, 0, 0, 0};
  _matrix[3] = {0, 0, 0, 0};
}

template<class Storage>
inline detail::if_4x4_matrix_t<Storage>
set_zero(ColumnMajorMatrix<Storage> & _matrix) {
  _matrix[0] = {0, 0, 0, 0};
  _matrix[1] = {0, 0, 0, 0};
  _matrix[2] = {0, 0, 0, 0};
  _matrix[3] = {0, 0, 0, 0};
}

/*******************************************************************************
** set_identity
*******************************************************************************/

template<class Storage>
inline detail::if_4x4_matrix_t<Storage>
set_identity(RowMajorMatrix<Storage> & _matrix) {
  _matrix[0] = {1, 0, 0, 0};
  _matrix[1] = {0, 1, 0, 0};
  _matrix[2] = {0, 0, 1, 0};
  _matrix[3] = {0, 0, 0, 1};
}

template<class Storage>
inline detail::if_4x4_matrix_t<Storage>
set_identity(ColumnMajorMatrix<Storage> & _matrix) {
  _matrix[0] = {1, 0, 0, 0};
  _matrix[1] = {0, 1, 0, 0};
  _matrix[2] = {0, 0, 1, 0};
  _matrix[3] = {0, 0, 0, 1};
}

/*******************************************************************************
** set_translate
*******************************************************************************/

template<class Storage>
inline detail::if_4x4_matrix_t<Storage>
set_translate(RowMajorMatrix<Storage> & _matrix,
              Vector3<typename Storage::ValueType> const & _translation) {
  set_identity(_matrix);
  set_translation(_matrix, _translation);
}

template<class Storage>
inline detail::if_4x4_matrix_t<Storage>
set_translate(ColumnMajorMatrix<Storage> & _matrix,
              Vector3<typename Storage::ValueType> const & _translation) {
  set_identity(_matrix);
  set_translation(_matrix, _translation);
}

/*******************************************************************************
** set_translation
*******************************************************************************/

template<class Storage>
inline detail::if_4x4_matrix_t<Storage>
set_translation(RowMajorMatrix<Storage> & _matrix,
                Vector3<typename Storage::ValueType> const & _translation) {
  _matrix[0][3] = _translation[0];
  _matrix[1][3] = _translation[1];
  _matrix[2][3] = _translation[2];
}

template<class Storage>
inline detail::if_4x4_matrix_t<Storage>
set_translation(ColumnMajorMatrix<Storage> & _matrix,
                Vector3<typename Storage::ValueType> const & _translation) {
  _matrix[3][0] = _translation[0];
  _matrix[3][1] = _translation[1];
  _matrix[3][2] = _translation[2];
}

/*******************************************************************************
** get_translation
*******************************************************************************/

template<class Storage>
inline detail::if_4x4_matrix_t<Storage, Vector3<typename Storage::ValueType>>
get_translation(RowMajorMatrix<Storage> const & _matrix) {
  return Vector3<typename Storage::ValueType>(_matrix[0][3], _matrix[1][3],
                                              _matrix[2][3]);
}

template<class Storage>
inline detail::if_4x4_matrix_t<Storage, Vector3<typename Storage::ValueType>>
get_translation(ColumnMajorMatrix<Storage> const & _matrix) {
  return Vector3<typename Storage::ValueType>(_matrix[3][0], _matrix[3][1],
                                              _matrix[3][2]);
}

/*******************************************************************************
** to_translation
*******************************************************************************/

template<class Storage>
inline detail::if_4x4_matrix_t<Storage, RowMajorMatrix<Storage>>
to_translation(RowMajorMatrix<Storage> const & _matrix) {
  RowMajorMatrix<Storage> result;
  result[0] = {1, 0, 0, _matrix[0][3]};
  result[1] = {0, 1, 0, _matrix[1][3]};
  result[2] = {0, 0, 1, _matrix[2][3]};
  result[3] = {0, 0, 0, 1};
  return result;
}

template<class Storage>
inline detail::if_4x4_matrix_t<Storage, ColumnMajorMatrix<Storage>>
to_translation(ColumnMajorMatrix<Storage> const & _matrix) {
  ColumnMajorMatrix<Storage> result;
  result[0] = {1, 0, 0, 0};
  result[1] = {0, 1, 0, 0};
  result[2] = {0, 0, 1, 0};
  result[3] = {_matrix[3][0], _matrix[3][1], _matrix[3][2], 1};
  return result;
}

/*******************************************************************************
** set_rotate
*******************************************************************************/

template<class Storage>
inline detail::if_4x4_matrix_t<Storage>
set_rotate(RowMajorMatrix<Storage> & _matrix,
           detail::Quaternion<typename Storage::ValueType> const & _rotation) {
  set_identity(_matrix);
  set_rotation(_matrix, _rotation);
}

template<class Storage>
inline detail::if_4x4_matrix_t<Storage>
set_rotate(ColumnMajorMatrix<Storage> & _matrix,
           detail::Quaternion<typename Storage::ValueType> const & _rotation) {
  set_identity(_matrix);
  set_rotation(_matrix, _rotation);
}

/*******************************************************************************
** set_rotation
*******************************************************************************/

template<class Storage>
inline detail::if_4x4_matrix_t<Storage> set_rotation(
    RowMajorMatrix<Storage> & _matrix,
    detail::Quaternion<typename Storage::ValueType> const & _rotation) {
  using ValueType = typename Storage::ValueType;
  ValueType xx = _rotation.x * _rotation.x;
  ValueType yy = _rotation.y * _rotation.y;
  ValueType zz = _rotation.z * _rotation.z;
  ValueType xz = _rotation.x * _rotation.z;
  ValueType xy = _rotation.x * _rotation.y;
  ValueType yz = _rotation.y * _rotation.z;
  ValueType wx = _rotation.w * _rotation.x;
  ValueType wy = _rotation.w * _rotation.y;
  ValueType wz = _rotation.w * _rotation.z;

  _matrix[0][0] = ValueType(1) - ValueType(2) * (yy + zz);
  _matrix[1][1] = ValueType(2) * (xy + wz);
  _matrix[2][0] = ValueType(2) * (xz - wy);

  _matrix[0][1] = ValueType(2) * (xy - wz);
  _matrix[1][1] = ValueType(1) - ValueType(2) * (xx + zz);
  _matrix[2][1] = ValueType(2) * (yz + wx);

  _matrix[0][2] = ValueType(2) * (xz + wy);
  _matrix[1][2] = ValueType(2) * (yz - wx);
  _matrix[2][2] = ValueType(1) - ValueType(2) * (xx + yy);
}

template<class Storage>
inline detail::if_4x4_matrix_t<Storage> set_rotation(
    ColumnMajorMatrix<Storage> & _matrix,
    detail::Quaternion<typename Storage::ValueType> const & _rotation) {
  using ValueType = typename Storage::ValueType;
  ValueType xx = _rotation.x * _rotation.x;
  ValueType yy = _rotation.y * _rotation.y;
  ValueType zz = _rotation.z * _rotation.z;
  ValueType xz = _rotation.x * _rotation.z;
  ValueType xy = _rotation.x * _rotation.y;
  ValueType yz = _rotation.y * _rotation.z;
  ValueType wx = _rotation.w * _rotation.x;
  ValueType wy = _rotation.w * _rotation.y;
  ValueType wz = _rotation.w * _rotation.z;

  _matrix[0][0] = ValueType(1) - ValueType(2) * (yy + zz);
  _matrix[0][1] = ValueType(2) * (xy + wz);
  _matrix[0][2] = ValueType(2) * (xz - wy);

  _matrix[1][0] = ValueType(2) * (xy - wz);
  _matrix[1][1] = ValueType(1) - ValueType(2) * (xx + zz);
  _matrix[1][2] = ValueType(2) * (yz + wx);

  _matrix[2][0] = ValueType(2) * (xz + wy);
  _matrix[2][1] = ValueType(2) * (yz - wx);
  _matrix[2][2] = ValueType(1) - ValueType(2) * (xx + yy);
}

/*******************************************************************************
** get_rotation
*******************************************************************************/

template<class Storage>
inline detail::if_4x4_matrix_t<Storage,
                               detail::Quaternion<typename Storage::ValueType>>
get_rotation(RowMajorMatrix<Storage> const & _matrix) {
  using ValueType = typename Storage::ValueType;
  ValueType sub_trace = _matrix[0][0] + _matrix[1][1] + _matrix[2][2];
  if (ValueType(0) < sub_trace) {
    ValueType s = sqrt(sub_trace + ValueType(1));
    ValueType w = ValueType(0.5) * s;
    s = ValueType(0.5) / s;
    ValueType x = (_matrix[2][1] - _matrix[1][2]) * s;
    ValueType y = (_matrix[0][2] - _matrix[2][0]) * s;
    ValueType z = (_matrix[1][0] - _matrix[0][1]) * s;
    return detail::Quaternion<ValueType>(x, y, z, w);
  }
  if (_matrix[0][0] > _matrix[1][1] && _matrix[0][0] > _matrix[2][2]) {
    ValueType s = ValueType(2) * sqrt(ValueType(1) + _matrix[0][0] -
                                      _matrix[1][1] - _matrix[2][2]);
    ValueType x = ValueType(0.25) * s;
    ValueType y = (_matrix[0][1] + _matrix[1][0]) / s;
    ValueType z = (_matrix[0][2] + _matrix[2][0]) / s;
    ValueType w = (_matrix[2][1] - _matrix[1][2]) / s;
    return detail::Quaternion<ValueType>(x, y, z, w);
  } else if (_matrix[1][1] > _matrix[2][2]) {
    ValueType s = ValueType(2) * sqrt(ValueType(1) + _matrix[1][1] -
                                      _matrix[0][0] - _matrix[2][2]);
    ValueType x = (_matrix[0][1] + _matrix[1][0]) / s;
    ValueType y = ValueType(0.25) * s;
    ValueType z = (_matrix[1][2] + _matrix[2][1]) / s;
    ValueType w = (_matrix[0][2] - _matrix[2][0]) / s;
    return detail::Quaternion<ValueType>(x, y, z, w);
  }
  ValueType s = ValueType(2) * sqrt(ValueType(1) + _matrix[2][2] -
                                    _matrix[0][0] - _matrix[1][1]);
  ValueType x = (_matrix[0][2] + _matrix[2][0]) / s;
  ValueType y = (_matrix[1][2] + _matrix[2][1]) / s;
  ValueType z = ValueType(0.25) * s;
  ValueType w = (_matrix[1][0] - _matrix[0][1]) / s;
  return detail::Quaternion<ValueType>(x, y, z, w);
}

template<class Storage>
inline detail::if_4x4_matrix_t<Storage,
                               detail::Quaternion<typename Storage::ValueType>>
get_rotation(ColumnMajorMatrix<Storage> const & _matrix) {
  using ValueType = typename Storage::ValueType;
  ValueType sub_trace = _matrix[0][0] + _matrix[1][1] + _matrix[2][2];
  if (ValueType(0) < sub_trace) {
    ValueType s = sqrt(sub_trace + ValueType(1));
    ValueType w = ValueType(0.5) * s;
    s = ValueType(0.5) / s;
    ValueType x = (_matrix[1][2] - _matrix[2][1]) * s;
    ValueType y = (_matrix[2][0] - _matrix[0][2]) * s;
    ValueType z = (_matrix[0][1] - _matrix[1][0]) * s;
    return detail::Quaternion<ValueType>(x, y, z, w);
  }
  if (_matrix[0][0] > _matrix[1][1] && _matrix[0][0] > _matrix[2][2]) {
    ValueType s = ValueType(2) * sqrt(ValueType(1) + _matrix[0][0] -
                                      _matrix[1][1] - _matrix[2][2]);
    ValueType x = ValueType(0.25) * s;
    ValueType y = (_matrix[1][0] + _matrix[0][1]) / s;
    ValueType z = (_matrix[2][0] + _matrix[0][2]) / s;
    ValueType w = (_matrix[1][2] - _matrix[2][1]) / s;
    return detail::Quaternion<ValueType>(x, y, z, w);
  } else if (_matrix[1][1] > _matrix[2][2]) {
    ValueType s = ValueType(2) * sqrt(ValueType(1) + _matrix[1][1] -
                                      _matrix[0][0] - _matrix[2][2]);
    ValueType x = (_matrix[1][0] + _matrix[0][1]) / s;
    ValueType y = ValueType(0.25) * s;
    ValueType z = (_matrix[2][1] + _matrix[1][2]) / s;
    ValueType w = (_matrix[2][0] - _matrix[0][2]) / s;
    return detail::Quaternion<ValueType>(x, y, z, w);
  }
  ValueType s = ValueType(2) * sqrt(ValueType(1) + _matrix[2][2] -
                                    _matrix[0][0] - _matrix[1][1]);
  ValueType x = (_matrix[2][0] + _matrix[0][2]) / s;
  ValueType y = (_matrix[2][1] + _matrix[1][2]) / s;
  ValueType z = ValueType(0.25) * s;
  ValueType w = (_matrix[0][1] - _matrix[1][0]) / s;
  return detail::Quaternion<ValueType>(x, y, z, w);
}

/*******************************************************************************
** to_rotation
*******************************************************************************/

template<class Storage>
inline detail::if_4x4_matrix_t<Storage, RowMajorMatrix<Storage>>
to_rotation(RowMajorMatrix<Storage> const & _matrix) {
  RowMajorMatrix<Storage> result;
  result[0] = {_matrix[0][0], _matrix[0][1], _matrix[0][2], 0};
  result[1] = {_matrix[1][0], _matrix[1][1], _matrix[1][2], 0};
  result[2] = {_matrix[2][0], _matrix[2][1], _matrix[2][2], 0};
  result[3] = {0, 0, 0, 1};
  return result;
}

template<class Storage>
inline detail::if_4x4_matrix_t<Storage, ColumnMajorMatrix<Storage>>
to_rotation(ColumnMajorMatrix<Storage> const & _matrix) {
  ColumnMajorMatrix<Storage> result;
  result[0] = {_matrix[0][0], _matrix[0][1], _matrix[0][2], 0};
  result[1] = {_matrix[1][0], _matrix[1][1], _matrix[1][2], 0};
  result[2] = {_matrix[2][0], _matrix[2][1], _matrix[2][2], 0};
  result[3] = {0, 0, 0, 1};
  return result;
}

/*******************************************************************************
** set_scale
*******************************************************************************/

template<class Storage>
inline detail::if_4x4_matrix_t<Storage>
set_scale(RowMajorMatrix<Storage> & _matrix,
          Vector3<typename Storage::ValueType> const & _scale) {
  _matrix[0] = {_scale[0], 0, 0, 0};
  _matrix[1] = {0, _scale[1], 0, 0};
  _matrix[2] = {0, 0, _scale[2], 0};
  _matrix[3] = {0, 0, 0, 1};
}

template<class Storage>
inline detail::if_4x4_matrix_t<Storage>
set_scale(ColumnMajorMatrix<Storage> & _matrix,
          Vector3<typename Storage::ValueType> const & _scale) {
  _matrix[0] = {_scale[0], 0, 0, 0};
  _matrix[1] = {0, _scale[1], 0, 0};
  _matrix[2] = {0, 0, _scale[2], 0};
  _matrix[3] = {0, 0, 0, 1};
}

/*******************************************************************************
** set_ortho
*******************************************************************************/

template<class Storage>
inline detail::if_4x4_matrix_t<Storage>
set_ortho(RowMajorMatrix<Storage> & _matrix, typename Storage::ValueType _left,
          typename Storage::ValueType _right,
          typename Storage::ValueType _bottom, typename Storage::ValueType _top,
          typename Storage::ValueType _near, typename Storage::ValueType _far) {
  using ValueType = typename Storage::ValueType;
  auto const m00 = static_cast<ValueType>(2) / (_right - _left);
  auto const m03 = -(_right + _left) / (_right - _left);
  auto const m11 = static_cast<ValueType>(2) / (_top - _bottom);
  auto const m13 = -(_top + _bottom) / (_top - _bottom);
  auto const m22 = static_cast<ValueType>(-2) / (_far - _near);
  auto const m23 = -(_far + _near) / (_far - _near);

  _matrix[0] = {m00, 0, 0, m03};
  _matrix[1] = {0, m11, 0, m13};
  _matrix[2] = {0, 0, m22, m23};
  _matrix[3] = {0, 0, 0, 1};
}

template<class Storage>
inline detail::if_4x4_matrix_t<Storage>
set_ortho(ColumnMajorMatrix<Storage> & _matrix,
          typename Storage::ValueType _left, typename Storage::ValueType _right,
          typename Storage::ValueType _bottom, typename Storage::ValueType _top,
          typename Storage::ValueType _near, typename Storage::ValueType _far) {
  using ValueType = typename Storage::ValueType;
  auto const m00 = static_cast<ValueType>(2) / (_right - _left);
  auto const m30 = -(_right + _left) / (_right - _left);
  auto const m11 = static_cast<ValueType>(2) / (_top - _bottom);
  auto const m31 = -(_top + _bottom) / (_top - _bottom);
  auto const m22 = static_cast<ValueType>(-2) / (_far - _near);
  auto const m32 = -(_far + _near) / (_far - _near);

  _matrix[0] = {m00, 0, 0, 0};
  _matrix[1] = {0, m11, 0, 0};
  _matrix[2] = {0, 0, m22, 0};
  _matrix[3] = {m30, m31, m32, 1};
}

/*******************************************************************************
** set_perspective
*******************************************************************************/

template<class Storage>
inline detail::if_4x4_matrix_t<Storage> set_perspective(
    RowMajorMatrix<Storage> & _matrix, typename Storage::ValueType _fov,
    typename Storage::ValueType _aspect, typename Storage::ValueType _near,
    typename Storage::ValueType _far) {
  // TODO: Implement this!
  CORE_LIB_UNIMPLEMENTED;

  using ValueType = typename Storage::ValueType;
  ValueType const depth = _far - _near;
  ValueType const depth_inv = static_cast<ValueType>(1) / depth;

  ValueType const fov_tg =
      tan(static_cast<ValueType>(_fov / static_cast<ValueType>(2)));
  ValueType const m22 = static_cast<ValueType>(1) / fov_tg;
  ValueType const m11 = m22 / _aspect;
  ValueType const m33 = _far * depth_inv;
  ValueType const m43 = -(_far * _near) * depth_inv;

  _matrix[0] = {m11, 0, 0, 0};
  _matrix[1] = {0, m22, 0, 0};
  _matrix[2] = {0, 0, m33, 1};
  _matrix[3] = {0, 0, m43, 0};
}

template<class Storage>
inline detail::if_4x4_matrix_t<Storage> set_perspective(
    ColumnMajorMatrix<Storage> & _matrix, typename Storage::ValueType _fov,
    typename Storage::ValueType _aspect, typename Storage::ValueType _near,
    typename Storage::ValueType _far) {
  using ValueType = typename Storage::ValueType;
  ValueType const fov = _fov * static_cast<float>(M_PI) / 180.f;
  ValueType const tan_half_fov = tanf(fov / static_cast<ValueType>(2));
  ValueType const m11 = static_cast<ValueType>(1) / (_aspect * tan_half_fov);
  ValueType const m22 = static_cast<ValueType>(1) / tan_half_fov;
  ValueType const m43 = -static_cast<ValueType>(1);
  ValueType const m33 = -(_far + _near) / (_far - _near);
  ValueType const m34 =
      -(static_cast<ValueType>(2) * _far * _near) / (_far - _near);
  _matrix[0] = {m11, 0, 0, 0};
  _matrix[1] = {0, m22, 0, 0};
  _matrix[2] = {0, 0, m33, m43};
  _matrix[3] = {0, 0, m34, 0};
}

/*******************************************************************************
** set_look_at
*******************************************************************************/

//          [ Rx, Ry, Rz, 0 ]   [ 1, 0, 0, -Px ]
//          [ Ux, Uy, Uz, 0 ]   [ 0, 1, 0, -Py ]
// LookAt = [ Dx, Dy, Dz, 0 ] * [ 0, 0, 1, -Pz ]
//          [  0,  0,  0, 1 ]   [ 0, 0, 0,  1  ]
// R = Right, U = Up, D = Direction, P = Position

template<class Storage>
inline detail::if_4x4_matrix_t<Storage>
set_look_at(RowMajorMatrix<Storage> & _matrix,
            Vector3<typename Storage::ValueType> const & _eye,
            Vector3<typename Storage::ValueType> const & _center,
            Vector3<typename Storage::ValueType> const & _up) {
  // TODO: Implement this!
  CORE_LIB_UNIMPLEMENTED;

  auto const direction(normalized(_center - _eye));
  auto const right(normalized(cross(direction, _up)));
  auto const up(cross(right, direction));

  auto const m41 = -dot(right, _eye);
  auto const m42 = -dot(up, _eye);
  auto const m43 = dot(direction, _eye);

  _matrix[0] = {right.x, right.y, right.z, 0};
  _matrix[1] = {up.x, up.y, up.z, 0};
  _matrix[2] = {-direction.x, -direction.y, -direction.z, 0};
  _matrix[3] = {m41, m42, m43, 1};
}

template<class Storage>
inline detail::if_4x4_matrix_t<Storage>
set_look_at(ColumnMajorMatrix<Storage> & _matrix,
            Vector3<typename Storage::ValueType> const & _eye,
            Vector3<typename Storage::ValueType> const & _center,
            Vector3<typename Storage::ValueType> const & _up) {

  auto const direction(normalized(_center - _eye));
  auto const right(normalized(cross(direction, _up)));
  auto const up(cross(right, direction));

  auto const m41 = -dot(right, _eye);
  auto const m42 = -dot(up, _eye);
  auto const m43 = dot(direction, _eye);

  _matrix[0] = {right.x, up.x, -direction.x, 0};
  _matrix[1] = {right.y, up.y, -direction.y, 0};
  _matrix[2] = {right.z, up.z, -direction.z, 0};
  _matrix[3] = {m41, m42, m43, 1};
}

/*******************************************************************************
** determinant
*******************************************************************************/

template<class Storage>
inline detail::if_4x4_matrix_t<Storage, typename Storage::ValueType>
determinant(RowMajorMatrix<Storage> const & /*_matrix*/) {
  // TODO: Implement this!
  CORE_LIB_UNIMPLEMENTED;
  return static_cast<typename Storage::ValueType>(0);
}

template<class Storage>
inline detail::if_4x4_matrix_t<Storage, typename Storage::ValueType>
determinant(ColumnMajorMatrix<Storage> const & _m) {
  auto const sf_0 = _m[2][2] * _m[3][3] - _m[3][2] * _m[2][3];
  auto const sf_1 = _m[2][1] * _m[3][3] - _m[3][1] * _m[2][3];
  auto const sf_2 = _m[2][1] * _m[3][2] - _m[3][1] * _m[2][2];
  auto const sf_3 = _m[2][0] * _m[3][3] - _m[3][0] * _m[2][3];
  auto const sf_4 = _m[2][0] * _m[3][2] - _m[3][0] * _m[2][2];
  auto const sf_5 = _m[2][0] * _m[3][1] - _m[3][0] * _m[2][1];

  auto const dc_0 = +(_m[1][1] * sf_0 - _m[1][2] * sf_1 + _m[1][3] * sf_2);
  auto const dc_1 = -(_m[1][0] * sf_0 - _m[1][2] * sf_3 + _m[1][3] * sf_4);
  auto const dc_2 = +(_m[1][0] * sf_1 - _m[1][1] * sf_3 + _m[1][3] * sf_5);
  auto const dc_3 = -(_m[1][0] * sf_2 - _m[1][1] * sf_4 + _m[1][2] * sf_5);

  return _m[0][0] * dc_0 + _m[0][1] * dc_1 + _m[0][2] * dc_2 + _m[0][3] * dc_3;
}

/*******************************************************************************
** inverse
*******************************************************************************/

template<class Storage>
inline detail::if_4x4_matrix_t<Storage, RowMajorMatrix<Storage>>
inverse(RowMajorMatrix<Storage> const & /*_matrix*/) {
  // TODO: Implement this!
  CORE_LIB_UNIMPLEMENTED;
  return RowMajorMatrix<Storage>();
}

template<class Storage>
inline detail::if_4x4_matrix_t<Storage, ColumnMajorMatrix<Storage>>
inverse(ColumnMajorMatrix<Storage> const & _m) {
  using ValueType = typename Storage::ValueType;

  auto const c_00 = _m[2][2] * _m[3][3] - _m[3][2] * _m[2][3];
  auto const c_02 = _m[1][2] * _m[3][3] - _m[3][2] * _m[1][3];
  auto const c_03 = _m[1][2] * _m[2][3] - _m[2][2] * _m[1][3];

  auto const c_04 = _m[2][1] * _m[3][3] - _m[3][1] * _m[2][3];
  auto const c_06 = _m[1][1] * _m[3][3] - _m[3][1] * _m[1][3];
  auto const c_07 = _m[1][1] * _m[2][3] - _m[2][1] * _m[1][3];

  auto const c_08 = _m[2][1] * _m[3][2] - _m[3][1] * _m[2][2];
  auto const c_10 = _m[1][1] * _m[3][2] - _m[3][1] * _m[1][2];
  auto const c_11 = _m[1][1] * _m[2][2] - _m[2][1] * _m[1][2];

  auto const c_12 = _m[2][0] * _m[3][3] - _m[3][0] * _m[2][3];
  auto const c_14 = _m[1][0] * _m[3][3] - _m[3][0] * _m[1][3];
  auto const c_15 = _m[1][0] * _m[2][3] - _m[2][0] * _m[1][3];

  auto const c_16 = _m[2][0] * _m[3][2] - _m[3][0] * _m[2][2];
  auto const c_18 = _m[1][0] * _m[3][2] - _m[3][0] * _m[1][2];
  auto const c_19 = _m[1][0] * _m[2][2] - _m[2][0] * _m[1][2];

  auto const c_20 = _m[2][0] * _m[3][1] - _m[3][0] * _m[2][1];
  auto const c_22 = _m[1][0] * _m[3][1] - _m[3][0] * _m[1][1];
  auto const c_23 = _m[1][0] * _m[2][1] - _m[2][0] * _m[1][1];

  Vector4<ValueType> f_0(c_00, c_00, c_02, c_03);
  Vector4<ValueType> f_1(c_04, c_04, c_06, c_07);
  Vector4<ValueType> f_2(c_08, c_08, c_10, c_11);
  Vector4<ValueType> f_3(c_12, c_12, c_14, c_15);
  Vector4<ValueType> f_4(c_16, c_16, c_18, c_19);
  Vector4<ValueType> f_5(c_20, c_20, c_22, c_23);

  Vector4<ValueType> v_0(_m[1][0], _m[0][0], _m[0][0], _m[0][0]);
  Vector4<ValueType> v_1(_m[1][1], _m[0][1], _m[0][1], _m[0][1]);
  Vector4<ValueType> v_2(_m[1][2], _m[0][2], _m[0][2], _m[0][2]);
  Vector4<ValueType> v_3(_m[1][3], _m[0][3], _m[0][3], _m[0][3]);

  Vector4<ValueType> i_0(v_1 * f_0 - v_2 * f_1 + v_3 * f_2);
  Vector4<ValueType> i_1(v_0 * f_0 - v_2 * f_3 + v_3 * f_4);
  Vector4<ValueType> i_2(v_0 * f_1 - v_1 * f_3 + v_3 * f_5);
  Vector4<ValueType> i_3(v_0 * f_2 - v_1 * f_4 + v_2 * f_5);

  Vector4<ValueType> sign_0(+1, -1, +1, -1);
  Vector4<ValueType> sign_1(-1, +1, -1, +1);
  ColumnMajorMatrix<Storage> inverse;
  inverse[0] = i_0 * sign_0;
  inverse[1] = i_1 * sign_1;
  inverse[2] = i_2 * sign_0;
  inverse[3] = i_3 * sign_1;
  Vector4<ValueType> row_0(inverse[0][0], inverse[1][0], inverse[2][0],
                           inverse[3][0]);
  Vector4<ValueType> dot_0(_m[0] * row_0);
  auto const d = dot_0.x + dot_0.y + dot_0.z + dot_0.w;
  auto const inv_d = static_cast<ValueType>(1) / d;

  return inverse * inv_d;
}

/*******************************************************************************
** add
*******************************************************************************/

template<typename Storage>
inline RowMajorMatrix<Storage> operator+(RowMajorMatrix<Storage> const & _lhs,
                                         RowMajorMatrix<Storage> const & _rhs) {
  using namespace matrix::storage;
  RowMajorMatrix<Storage> result;
  result.storage = add(_lhs.storage, _rhs.storage);
  return result;
}

template<typename Storage>
inline ColumnMajorMatrix<Storage>
operator+(ColumnMajorMatrix<Storage> const & _lhs,
          ColumnMajorMatrix<Storage> const & _rhs) {
  using namespace matrix::storage;
  ColumnMajorMatrix<Storage> result;
  result.storage = add(_lhs.storage, _rhs.storage);
  return result;
}

/*******************************************************************************
** sub
*******************************************************************************/

template<typename Storage>
inline Storage sub(Storage const & _lhs, Storage const & _rhs) {
  return _lhs - _rhs;
}

template<typename Storage>
inline RowMajorMatrix<Storage> operator-(RowMajorMatrix<Storage> const & _lhs,
                                         RowMajorMatrix<Storage> const & _rhs) {
  RowMajorMatrix<Storage> result;
  result.storage = sub(_lhs.storage, _rhs.storage);
  return result;
}

template<typename Storage>
inline ColumnMajorMatrix<Storage>
operator-(ColumnMajorMatrix<Storage> const & _lhs,
          ColumnMajorMatrix<Storage> const & _rhs) {
  ColumnMajorMatrix<Storage> result;
  result.storage = sub(_lhs.storage - _rhs.storage);
  return result;
}

/*******************************************************************************
** mul
*******************************************************************************/

template<typename Storage>
inline RowMajorMatrix<Storage> operator*(RowMajorMatrix<Storage> const & _lhs,
                                         RowMajorMatrix<Storage> const & _rhs) {
  RowMajorMatrix<Storage> result;
  multiply_row_major(_lhs.storage, _rhs.storage, result.storage);
  return result;
}

template<typename Storage>
inline ColumnMajorMatrix<Storage>
operator*(ColumnMajorMatrix<Storage> const & _lhs,
          ColumnMajorMatrix<Storage> const & _rhs) {
  ColumnMajorMatrix<Storage> result;
  multiply_column_major(_lhs.storage, _rhs.storage, result.storage);
  return result;
}

template<typename Storage>
inline typename Storage::ColumnVector
operator*(RowMajorMatrix<Storage> const & _lhs,
          typename Storage::RowVector const & _rhs) {
  // TODO: Implement this!
  CORE_LIB_UNIMPLEMENTED;
  return Storage::ColumnVector();
}

template<typename Storage>
inline typename Storage::ColumnVector
operator*(ColumnMajorMatrix<Storage> const & _lhs,
          typename Storage::RowVector const & _rhs) {
  using RowVector = typename Storage::RowVector;
  RowVector v0 = RowVector(_rhs[0], _rhs[0], _rhs[0], _rhs[0]);
  RowVector v1 = RowVector(_rhs[1], _rhs[1], _rhs[1], _rhs[1]);
  RowVector v2 = RowVector(_rhs[2], _rhs[2], _rhs[2], _rhs[2]);
  RowVector v3 = RowVector(_rhs[3], _rhs[3], _rhs[3], _rhs[3]);
  return _lhs[0] * v0 + _lhs[1] * v1 + _lhs[2] * v2 + _lhs[3] * v3;
}

template<typename Storage>
inline typename Storage::RowVector
operator*(typename Storage::RowVector const & _lhs,
          RowMajorMatrix<Storage> const & _rhs) {
  // TODO: Implement this!
  CORE_LIB_UNIMPLEMENTED;
  return Storage::RowVector();
}

template<typename Storage>
inline typename Storage::RowVector
operator*(typename Storage::ColumnVector const & _lhs,
          ColumnMajorMatrix<Storage> const & _rhs) {
  // TODO: Implement this!
  CORE_LIB_UNIMPLEMENTED;
  return Storage::ColumnVector();
}

template<typename Storage>
inline RowMajorMatrix<Storage> & operator*=(RowMajorMatrix<Storage> & _lhs,
                                            typename Storage::ValueType _rhs) {
  _lhs[0] *= _rhs;
  _lhs[1] *= _rhs;
  _lhs[2] *= _rhs;
  _lhs[3] *= _rhs;
  return _lhs;
}

template<typename Storage>
inline RowMajorMatrix<Storage> operator*(RowMajorMatrix<Storage> const & _lhs,
                                         typename Storage::ValueType _rhs) {
  RowMajorMatrix<Storage> result = _lhs;
  result *= _rhs;
  return result;
}

template<typename Storage>
inline ColumnMajorMatrix<Storage> &
operator*=(ColumnMajorMatrix<Storage> & _lhs,
           typename Storage::ValueType _rhs) {
  _lhs[0] *= _rhs;
  _lhs[1] *= _rhs;
  _lhs[2] *= _rhs;
  _lhs[3] *= _rhs;
  return _lhs;
}

template<typename Storage>
inline ColumnMajorMatrix<Storage>
operator*(ColumnMajorMatrix<Storage> const & _lhs,
          typename Storage::ValueType _rhs) {
  ColumnMajorMatrix<Storage> result = _lhs;
  result *= _rhs;
  return result;
}

/*******************************************************************************
** equal
*******************************************************************************/

template<typename Storage>
inline bool equal(Storage const & _lhs, Storage const & _rhs,
                  typename Storage::ValueType _epsilon) {
  for (auto index = 0u; index < Storage::Elements; ++index) {
    if (_epsilon < distance(_lhs[index], _rhs[index])) {
      return false;
    }
  }
  return true;
}

template<typename Storage>
inline bool equal(Storage const & _lhs, Storage const & _rhs) {
  return equal(_lhs, _rhs,
               static_cast<typename Storage::ValueType>(CORE_LIB_EPSILON));
}

template<typename Storage>
inline bool operator==(RowMajorMatrix<Storage> const & _lhs,
                       RowMajorMatrix<Storage> const & _rhs) {
  return equal(_lhs.storage, _rhs.storage);
}

template<typename Storage>
inline bool operator==(ColumnMajorMatrix<Storage> const & _lhs,
                       ColumnMajorMatrix<Storage> const & _rhs) {
  return equal(_lhs.storage, _rhs.storage);
}

}

#endif
