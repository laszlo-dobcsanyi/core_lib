#ifndef _CORE_NUMERICS_VECTOR2_HPP_
#define _CORE_NUMERICS_VECTOR2_HPP_

#ifndef CORE_LIB_NUMERICS
#  error
#endif

////////////////////////////////////////////////////////////////////////////////
// numerics detail
////////////////////////////////////////////////////////////////////////////////

namespace numerics { namespace detail {

/*******************************************************************************
** Vector2
*******************************************************************************/

template<typename ValueType>
struct Vector<ValueType, 2u> {
  ValueType x;
  ValueType y;

  Vector();
  Vector(ValueType _x, ValueType _y);
  explicit Vector(Vector<ValueType, 3u> const & _vector);
  explicit Vector(Vector<ValueType, 4u> const & _vector);
  Vector(Vector &&) = default;
  Vector(Vector const &) = default;
  Vector & operator=(Vector &&) = default;
  Vector & operator=(Vector const &) = default;
  ~Vector() = default;

  ValueType & operator[](size_t _index);
  ValueType const & operator[](size_t _index) const;
};

template<typename ValueType>
inline Vector<ValueType, 2u>::Vector() {}

template<typename ValueType>
inline Vector<ValueType, 2u>::Vector(ValueType _x, ValueType _y)
    : x(_x)
    , y(_y) {}

template<typename ValueType>
inline Vector<ValueType, 2u>::Vector(Vector<ValueType, 3u> const & _vector)
    : x(_vector.x)
    , y(_vector.y) {}

template<typename ValueType>
inline Vector<ValueType, 2u>::Vector(Vector<ValueType, 4u> const & _vector)
    : x(_vector.x)
    , y(_vector.y) {}

template<typename ValueType>
ValueType & Vector<ValueType, 2u>::operator[](size_t _index) {
  CORE_LIB_ASSERT(_index < 2u);
  return _index == 0 ? x : y;
}

template<typename ValueType>
ValueType const & Vector<ValueType, 2u>::operator[](size_t _index) const {
  CORE_LIB_ASSERT(_index < 2u);
  return _index == 0 ? x : y;
}

template<typename ValueType1, typename ValueType2>
inline Vector<ValueType1, 2u> &
operator+=(Vector<ValueType1, 2u> & _lhs, Vector<ValueType2, 2u> const & _rhs) {
  _lhs.x += _rhs.x;
  _lhs.y += _rhs.y;
  return _lhs;
}

template<typename ValueType>
inline Vector<ValueType, 2u> & operator+=(Vector<ValueType, 2u> & _lhs,
                                          ValueType _rhs) {
  _lhs.x += _rhs;
  _lhs.y += _rhs;
  return _lhs;
}

template<typename ValueType>
inline Vector<ValueType, 2u> operator+(Vector<ValueType, 2u> const & _lhs,
                                       Vector<ValueType, 2u> const & _rhs) {
  return Vector<ValueType, 2u>(_lhs.x + _rhs.x, _lhs.y + _rhs.y);
}

template<typename ValueType>
inline Vector<ValueType, 2u> operator+(Vector<ValueType, 2u> const & _lhs,
                                       ValueType _rhs) {
  return Vector<ValueType, 2u>(_lhs.x + _rhs, _lhs.y + _rhs);
}

template<typename ValueType>
inline Vector<ValueType, 2u> operator+(ValueType _lhs,
                                       Vector<ValueType, 2u> const & _rhs) {
  return _rhs + _lhs;
}

template<typename ValueType1, typename ValueType2>
inline Vector<ValueType1, 2u> &
operator-=(Vector<ValueType1, 2u> & _lhs, Vector<ValueType2, 2u> const & _rhs) {
  _lhs.x -= _rhs.x;
  _lhs.y -= _rhs.y;
  return _lhs;
}

template<typename ValueType>
inline Vector<ValueType, 2u> & operator-=(Vector<ValueType, 2u> & _lhs,
                                          ValueType _rhs) {
  _lhs.x -= _rhs;
  _lhs.y -= _rhs;
  return _lhs;
}

template<typename ValueType>
inline Vector<ValueType, 2u> operator-(Vector<ValueType, 2u> const & _lhs,
                                       Vector<ValueType, 2u> const & _rhs) {
  return Vector<ValueType, 2u>(_lhs.x - _rhs.x, _lhs.y - _rhs.y);
}

template<typename ValueType>
inline Vector<ValueType, 2u> operator-(Vector<ValueType, 2u> const & _lhs,
                                       ValueType _rhs) {
  return Vector<ValueType, 2u>(_lhs.x - _rhs, _lhs.y - _rhs);
}

template<typename ValueType>
inline Vector<ValueType, 2u> operator-(ValueType _lhs,
                                       Vector<ValueType, 2u> const & _rhs) {
  return _rhs - _lhs;
}

template<typename ValueType1, typename ValueType2>
inline Vector<ValueType1, 2u> &
operator*=(Vector<ValueType1, 2u> & _lhs, Vector<ValueType2, 2u> const & _rhs) {
  _lhs.x *= _rhs.x;
  _lhs.y *= _rhs.y;
  return _lhs;
}

template<typename ValueType>
inline Vector<ValueType, 2u> & operator*=(Vector<ValueType, 2u> & _lhs,
                                          ValueType _rhs) {
  _lhs.x *= _rhs;
  _lhs.y *= _rhs;
  return _lhs;
}

template<typename ValueType>
inline Vector<ValueType, 2u> operator*(Vector<ValueType, 2u> const & _lhs,
                                       Vector<ValueType, 2u> const & _rhs) {
  return Vector<ValueType, 2u>(_lhs.coordiante.x * _rhs.x, _lhs.y * _rhs.y);
}

template<typename ValueType>
inline Vector<ValueType, 2u> operator*(Vector<ValueType, 2u> const & _lhs,
                                       ValueType _rhs) {
  return Vector<ValueType, 2u>(_lhs.x * _rhs, _lhs.y * _rhs);
}

template<typename ValueType>
inline Vector<ValueType, 2u> operator*(ValueType _lhs,
                                       Vector<ValueType, 2u> const & _rhs) {
  return _rhs * _lhs;
}

template<typename ValueType1, typename ValueType2>
inline Vector<ValueType1, 2u> &
operator/=(Vector<ValueType1, 2u> & _lhs, Vector<ValueType2, 2u> const & _rhs) {
  if (!is_nearly_zero(_rhs.x) && !is_nearly_zero(_rhs.y)) {
    _lhs.x /= _rhs.x;
    _lhs.y /= _rhs.y;
  }
  return _lhs;
}

template<typename ValueType>
inline Vector<ValueType, 2u> & operator/=(Vector<ValueType, 2u> & _lhs,
                                          ValueType _rhs) {
  if (!is_nearly_zero(_rhs)) {
    _lhs.x /= _rhs;
    _lhs.y /= _rhs;
  }
  return _lhs;
}

template<typename ValueType>
inline Vector<ValueType, 2u> operator/(Vector<ValueType, 2u> const & _lhs,
                                       Vector<ValueType, 2u> const & _rhs) {
  if (!is_nearly_zero(_rhs.x) && !is_nearly_zero(_rhs.y)) {
    Vector<ValueType, 2u>(_lhs.x / _rhs.x, _lhs.y / _rhs.y);
  }
  return _lhs;
}

template<typename ValueType>
inline Vector<ValueType, 2u> operator/(Vector<ValueType, 2u> const & _lhs,
                                       ValueType _rhs) {
  if (!is_nearly_zero(_rhs)) {
    return Vector<ValueType, 2u>(_lhs.x / _rhs, _lhs.y / _rhs);
  }
  return _lhs;
}

template<typename ValueType>
inline bool operator==(Vector<ValueType, 2u> const & _lhs,
                       Vector<ValueType, 2u> const & _rhs) {
  return _lhs.x == _rhs.x && _lhs.y == _rhs.y;
}

template<typename ValueType>
inline bool operator!=(Vector<ValueType, 2u> const & _lhs,
                       Vector<ValueType, 2u> const & _rhs) {
  return !(_lhs == _rhs);
}

}}

////////////////////////////////////////////////////////////////////////////////
// numerics
////////////////////////////////////////////////////////////////////////////////

namespace numerics {

template<typename ValueType>
using Vector2 = detail::Vector<ValueType, 2u>;

using Vector2i8 = Vector2<int8>;
using Vector2u8 = Vector2<uint8>;
using Vector2i16 = Vector2<int16>;
using Vector2u16 = Vector2<uint16>;
using Vector2i32 = Vector2<int32>;
using Vector2u32 = Vector2<uint32>;
using Vector2i64 = Vector2<int64>;
using Vector2u64 = Vector2<uint64>;
using Vector2f = Vector2<float>;
using Vector2d = Vector2<double>;

}

#endif
