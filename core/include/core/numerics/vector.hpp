#ifndef _CORE_NUMERICS_VECTOR_HPP_
#define _CORE_NUMERICS_VECTOR_HPP_

#ifndef CORE_LIB_NUMERICS
#  error
#endif

////////////////////////////////////////////////////////////////////////////////
// numerics detail
////////////////////////////////////////////////////////////////////////////////

namespace numerics { namespace detail {

/*******************************************************************************
** Vector
*******************************************************************************/

template<typename ValueType, size_t Size>
struct Vector;

}}

#endif
