#ifndef _CORE_NUMERICS_TRANSFORMATION_HPP_
#define _CORE_NUMERICS_TRANSFORMATION_HPP_

#ifndef CORE_LIB_NUMERICS
#  error
#endif

////////////////////////////////////////////////////////////////////////////////
// numerics
////////////////////////////////////////////////////////////////////////////////

namespace numerics {

/*******************************************************************************
** Transformation
*******************************************************************************/

struct Transformation {
  Vector3f translation;
  Quaternionf rotation;

  Transformation();
  Transformation(Vector3f translation, Quaternionf _rotation);
  Transformation(Transformation &&) = default;
  Transformation(Transformation const &) = default;
  Transformation & operator=(Transformation &&) = default;
  Transformation & operator=(Transformation const &) = default;
  ~Transformation() = default;
};

inline Transformation::Transformation() {}

inline Transformation::Transformation(Vector3f _translation,
                                      Quaternionf _rotation)
    : translation(_translation)
    , rotation(_rotation) {}
}

#endif