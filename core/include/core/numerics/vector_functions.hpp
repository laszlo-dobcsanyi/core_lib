#ifndef _CORE_LIB_NUMERICS_VECTOR_FUNCTIONS_HPP_
#define _CORE_LIB_NUMERICS_VECTOR_FUNCTIONS_HPP_

#ifndef CORE_LIB_NUMERICS
#  error
#endif

////////////////////////////////////////////////////////////////////////////////
// numerics
////////////////////////////////////////////////////////////////////////////////

namespace numerics {

/*******************************************************************************
** is_nearly_zero
*******************************************************************************/

template<typename ValueType>
inline bool is_nearly_zero(Vector2<ValueType> const & _vector) {
  return is_nearly_zero(_vector.x) && is_nearly_zero(_vector.y);
}

template<typename ValueType>
inline bool is_nearly_zero(Vector3<ValueType> const & _vector) {
  return is_nearly_zero(_vector.x) && is_nearly_zero(_vector.y) &&
         is_nearly_zero(_vector.z);
}

template<typename ValueType>
inline bool is_nearly_zero(Vector4<ValueType> const & _vector) {
  return is_nearly_zero(_vector.x) && is_nearly_zero(_vector.y) &&
         is_nearly_zero(_vector.z) && is_nearly_zero(_vector.w);
}

/*******************************************************************************
** cos
*******************************************************************************/

template<typename ValueType>
inline Vector2<ValueType> cos(Vector2<ValueType> const & _vector) {
  return Vector2<ValueType>(cos(_vector.x), cos(_vector.y));
}

template<typename ValueType>
inline Vector3<ValueType> cos(Vector3<ValueType> const & _vector) {
  return Vector3<ValueType>(cos(_vector.x), cos(_vector.y), cos(_vector.z));
}

template<typename ValueType>
inline Vector4<ValueType> cos(Vector4<ValueType> const & _vector) {
  return Vector4<ValueType>(cos(_vector.x), cos(_vector.y), cos(_vector.z),
                            cos(_vector.w));
}

/*******************************************************************************
** sin
*******************************************************************************/

template<typename ValueType>
inline Vector2<ValueType> sin(Vector2<ValueType> const & _vector) {
  return Vector2<ValueType>(sin(_vector.x), sin(_vector.y));
}

template<typename ValueType>
inline Vector3<ValueType> sin(Vector3<ValueType> const & _vector) {
  return Vector3<ValueType>(sin(_vector.x), sin(_vector.y), sin(_vector.z));
}

template<typename ValueType>
inline Vector4<ValueType> sin(Vector4<ValueType> const & _vector) {
  return Vector4<ValueType>(sin(_vector.x), sin(_vector.y), sin(_vector.z),
                            sin(_vector.w));
}

/*******************************************************************************
** cross
*******************************************************************************/

template<typename ValueType>
inline detail::Vector<ValueType, 3u>
cross(detail::Vector<ValueType, 3u> const & _vector1,
      detail::Vector<ValueType, 3u> const & _vector2) {
  return detail::Vector<ValueType, 3u>(
      _vector1.y * _vector2.z - _vector1.z * _vector2.y,
      _vector1.z * _vector2.x - _vector1.x * _vector2.z,
      _vector1.x * _vector2.y - _vector1.y * _vector2.x);
}

/*******************************************************************************
** dot
*******************************************************************************/

template<typename ValueType>
inline ValueType dot(detail::Vector<ValueType, 2u> const & _vector1,
                     detail::Vector<ValueType, 2u> const & _vector2) {
  return _vector1.x * _vector2.x + _vector1.y * _vector2.y;
}

template<typename ValueType>
inline ValueType dot(detail::Vector<ValueType, 3u> const & _vector1,
                     detail::Vector<ValueType, 3u> const & _vector2) {
  return _vector1.x * _vector2.x + _vector1.y * _vector2.y +
         _vector1.z * _vector2.z;
}

template<typename ValueType>
inline ValueType dot(detail::Vector<ValueType, 4u> const & _vector1,
                     detail::Vector<ValueType, 4u> const & _vector2) {
  return _vector1.x * _vector2.x + _vector1.y * _vector2.y +
         _vector1.z * _vector2.z + _vector1.w * _vector2.w;
}

/*******************************************************************************
** length2
*******************************************************************************/

template<typename ValueType>
inline ValueType length2(detail::Vector<ValueType, 2u> const & _vector) {
  return sqr(_vector.x) + sqr(_vector.y);
}

template<typename ValueType>
inline ValueType length2(detail::Vector<ValueType, 3u> const & _vector) {
  return sqr(_vector.x) + sqr(_vector.y) + sqr(_vector.z);
}

template<typename ValueType>
inline ValueType length2(detail::Vector<ValueType, 4u> const & _vector) {
  return sqr(_vector.x) + sqr(_vector.y) + sqr(_vector.z) + sqr(_vector.w);
}

/*******************************************************************************
** length
*******************************************************************************/

template<typename ValueType, size_t Size>
inline ValueType length(detail::Vector<ValueType, Size> const & _vector) {
  return sqrt(length2(_vector));
}

/*******************************************************************************
** is_normalized
*******************************************************************************/

template<typename ValueType, size_t Size>
inline bool is_normalized(detail::Vector<ValueType, Size> const & _vector) {
  return are_nearly_equal(static_cast<ValueType>(1), length2(_vector));
}

/*******************************************************************************
** normalize
*******************************************************************************/

template<typename ValueType, size_t Size>
inline void normalize(detail::Vector<ValueType, Size> & _vector) {
  CORE_LIB_ASSERT(!is_nearly_zero(_vector));
  _vector /= length(_vector);
  CORE_LIB_ASSERT(is_normalized(_vector));
}

/*******************************************************************************
** normalized
*******************************************************************************/

template<typename ValueType, size_t Size>
inline detail::Vector<ValueType, Size>
normalized(detail::Vector<ValueType, Size> const & _vector) {
  CORE_LIB_ASSERT(!is_nearly_zero(_vector));
  auto result = _vector / length(_vector);
  CORE_LIB_ASSERT(is_normalized(result));
  return result;
}

/*******************************************************************************
** lerp
*******************************************************************************/

template<typename ValueType, size_t Size, typename AlphaType>
inline detail::Vector<ValueType, Size>
lerp(detail::Vector<ValueType, Size> const & _vector1,
     detail::Vector<ValueType, Size> const & _vector2, AlphaType _alpha) {
  return _vector1 + static_cast<ValueType>(_alpha) * (_vector2 - _vector1);
}

/*******************************************************************************
** slerp
*******************************************************************************/

template<typename ValueType, size_t Size, typename AlphaType>
inline detail::Vector<ValueType, Size>
slerp(detail::Vector<ValueType, Size> const & _vector1,
      detail::Vector<ValueType, Size> const & _vector2, AlphaType _alpha) {
  CORE_LIB_ASSERT(is_normalized(_vector1));
  CORE_LIB_ASSERT(is_normalized(_vector2));
  ValueType cosine = dot(_vector1, _vector2);
  CORE_LIB_ASSERT(static_cast<ValueType>(-1) <= cosine &&
                  cosine <= static_cast<ValueType>(1));
  ValueType theta = acos(cosine) * _alpha;
  detail::Vector<ValueType, Size> relative = _vector2 - _vector1 * cosine;
  normalize(relative);
  return _vector1 * cos(theta) + relative * sin(theta);
}

/*******************************************************************************
** distance
*******************************************************************************/

template<typename ValueType, size_t Size>
inline ValueType distance(detail::Vector<ValueType, Size> _vector1,
                          detail::Vector<ValueType, Size> _vector2) {
  return length(_vector2 - _vector1);
}

/*******************************************************************************
** point_line_distance
*******************************************************************************/

template<typename ValueType>
inline ValueType point_line_distance(detail::Vector<ValueType, 3u> _point,
                                     detail::Vector<ValueType, 3u> _line1,
                                     detail::Vector<ValueType, 3u> _line2) {
  detail::Vector<ValueType, 3u> v = _line2 - _line1;
  detail::Vector<ValueType, 3u> w = _point - _line1;

  ValueType c1 = dot(w, v);
  if (c1 <= static_cast<ValueType>(0)) {
    return distance(_point, _line1);
  }

  ValueType c2 = dot(v, v);
  if (c2 <= c1) {
    return distance(_point, _line2);
  }

  return distance(_point, _line1 + (c1 / c2) * v);
}

}

#endif
