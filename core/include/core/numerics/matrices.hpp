#ifndef _CORE_LIB_NUMERICS_MATRICES_HPP_
#define _CORE_LIB_NUMERICS_MATRICES_HPP_

#ifndef CORE_LIB_NUMERICS
#  error
#endif

////////////////////////////////////////////////////////////////////////////////
// numerics
////////////////////////////////////////////////////////////////////////////////

namespace numerics {

/*******************************************************************************
** GenericMatrix
*******************************************************************************/

template<typename Storage>
using GenericMatrix =
    core::Variant<RowMajorMatrix<Storage>, ColumnMajorMatrix<Storage>>;

#if 0
/*******************************************************************************
** Matrix2x2
*******************************************************************************/

template< typename ValueType >
using Matrix2x2Storage = matrix::DefaultStorage< ValueType, 2u, 2u >;
template< typename ValueType >
using RowMajorMatrix2x2 = RowMajorMatrix< Matrix2x2Storage< ValueType > >;
template< typename ValueType >
using ColumnMajorMatrix2x2 = ColumnMajorMatrix< Matrix2x2Storage< ValueType > >;
template< typename ValueType >
using Matrix2x2 = GenericMatrix< Matrix2x2Storage< ValueType > >;

/*******************************************************************************
** Matrix3x3
*******************************************************************************/

template< typename ValueType >
using Matrix3x3Storage = matrix::DefaultStorage< ValueType, 3u, 3u >;
template< typename ValueType >
using RowMajorMatrix3x3 = RowMajorMatrix< Matrix3x3Storage< ValueType > >;
template< typename ValueType >
using ColumnMajorMatrix3x3 = ColumnMajorMatrix< Matrix3x3Storage< ValueType > >;
template< typename ValueType >
using Matrix3x3 = GenericMatrix< Matrix3x3Storage< ValueType > >;
#endif

/*******************************************************************************
** Matrix4x4
*******************************************************************************/

template<typename ValueType>
using Matrix4x4Storage = matrix::DefaultStorage<ValueType, 4u, 4u>;
template<typename ValueType>
using RowMajorMatrix4x4 = RowMajorMatrix<Matrix4x4Storage<ValueType>>;
template<typename ValueType>
using ColumnMajorMatrix4x4 = ColumnMajorMatrix<Matrix4x4Storage<ValueType>>;
template<typename ValueType>
using Matrix4x4 = GenericMatrix<Matrix4x4Storage<ValueType>>;

using RowMajorMatrix4x4f = RowMajorMatrix4x4<float>;
using ColumnMajorMatrix4x4f = ColumnMajorMatrix4x4<float>;
using Matrix4x4f = Matrix4x4<float>;

}

#endif
