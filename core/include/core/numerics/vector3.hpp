#ifndef _CORE_NUMERICS_VECTOR3_HPP_
#define _CORE_NUMERICS_VECTOR3_HPP_

#ifndef CORE_LIB_NUMERICS
#  error
#endif

////////////////////////////////////////////////////////////////////////////////
// numerics detail
////////////////////////////////////////////////////////////////////////////////

namespace numerics { namespace detail {

/*******************************************************************************
** Vector3
*******************************************************************************/

template<typename ValueType>
struct Vector<ValueType, 3u> {
  ValueType x;
  ValueType y;
  ValueType z;

  Vector();
  Vector(ValueType _x, ValueType _y, ValueType _z);
  Vector(Vector<ValueType, 2u> const & _vector, ValueType _z);
  explicit Vector(Vector<ValueType, 4u> const & _vector);
  Vector(Vector &&) = default;
  Vector(Vector const &) = default;
  Vector & operator=(Vector &&) = default;
  Vector & operator=(Vector const &) = default;
  ~Vector() = default;

  ValueType & operator[](size_t _index);
  ValueType const & operator[](size_t _index) const;
};

template<typename ValueType>
inline Vector<ValueType, 3u>::Vector() {}

template<typename ValueType>
inline Vector<ValueType, 3u>::Vector(ValueType _x, ValueType _y, ValueType _z)
    : x(_x)
    , y(_y)
    , z(_z) {}

template<typename ValueType>
inline Vector<ValueType, 3u>::Vector(Vector<ValueType, 2u> const & _vector,
                                     ValueType _z)
    : x(_vector.x)
    , y(_vector.y)
    , z(_z) {}

template<typename ValueType>
inline Vector<ValueType, 3u>::Vector(Vector<ValueType, 4u> const & _vector)
    : x(_vector.x)
    , y(_vector.y)
    , z(_vector.z) {}

template<typename ValueType>
ValueType & Vector<ValueType, 3u>::operator[](size_t _index) {
  CORE_LIB_ASSERT(_index < 3u);
  return _index == 0 ? x : _index == 1 ? y : z;
}

template<typename ValueType>
ValueType const & Vector<ValueType, 3u>::operator[](size_t _index) const {
  CORE_LIB_ASSERT(_index < 3u);
  return _index == 0 ? x : _index == 1 ? y : z;
}

template<typename ValueType1, typename ValueType2>
inline Vector<ValueType1, 3u> &
operator+=(Vector<ValueType1, 3u> & _lhs, Vector<ValueType2, 3u> const & _rhs) {
  _lhs.x += _rhs.x;
  _lhs.y += _rhs.y;
  _lhs.z += _rhs.z;
  return _lhs;
}

template<typename ValueType>
inline Vector<ValueType, 3u> & operator+=(Vector<ValueType, 3u> & _lhs,
                                          ValueType _rhs) {
  _lhs.x += _rhs;
  _lhs.y += _rhs;
  _lhs.z += _rhs;
  return _lhs;
}

template<typename ValueType>
inline Vector<ValueType, 3u> operator+(Vector<ValueType, 3u> const & _lhs,
                                       Vector<ValueType, 3u> const & _rhs) {
  return Vector<ValueType, 3u>(_lhs.x + _rhs.x, _lhs.y + _rhs.y,
                               _lhs.z + _rhs.z);
}

template<typename ValueType>
inline Vector<ValueType, 3u> operator+(Vector<ValueType, 3u> const & _lhs,
                                       ValueType _rhs) {
  return Vector<ValueType, 3u>(_lhs.x + _rhs, _lhs.y + _rhs, _lhs.z + _rhs);
}

template<typename ValueType>
inline Vector<ValueType, 3u> operator+(ValueType _lhs,
                                       Vector<ValueType, 3u> const & _rhs) {
  return _rhs + _lhs;
}

template<typename ValueType1, typename ValueType2>
inline Vector<ValueType1, 3u> &
operator-=(Vector<ValueType1, 3u> & _lhs, Vector<ValueType2, 3u> const & _rhs) {
  _lhs.x -= _rhs.x;
  _lhs.y -= _rhs.y;
  _lhs.z -= _rhs.z;
  return _lhs;
}

template<typename ValueType>
inline Vector<ValueType, 3u> & operator-=(Vector<ValueType, 3u> & _lhs,
                                          ValueType _rhs) {
  _lhs.x -= _rhs;
  _lhs.y -= _rhs;
  _lhs.z -= _rhs;
  return _lhs;
}

template<typename ValueType>
inline Vector<ValueType, 3u> operator-(Vector<ValueType, 3u> const & _lhs,
                                       Vector<ValueType, 3u> const & _rhs) {
  return Vector<ValueType, 3u>(_lhs.x - _rhs.x, _lhs.y - _rhs.y,
                               _lhs.z - _rhs.z);
}

template<typename ValueType>
inline Vector<ValueType, 3u> operator-(Vector<ValueType, 3u> const & _lhs,
                                       ValueType _rhs) {
  return Vector<ValueType, 3u>(_lhs.x - _rhs, _lhs.y - _rhs, _lhs.z - _rhs);
}

template<typename ValueType>
inline Vector<ValueType, 3u> operator-(ValueType _lhs,
                                       Vector<ValueType, 3u> const & _rhs) {
  return _rhs - _lhs;
}

template<typename ValueType1, typename ValueType2>
inline Vector<ValueType1, 3u> &
operator*=(Vector<ValueType1, 3u> & _lhs, Vector<ValueType2, 3u> const & _rhs) {
  _lhs.x *= _rhs.x;
  _lhs.y *= _rhs.y;
  _lhs.z *= _rhs.z;
  return _lhs;
}

template<typename ValueType>
inline Vector<ValueType, 3u> & operator*=(Vector<ValueType, 3u> & _lhs,
                                          ValueType _rhs) {
  _lhs.x *= _rhs;
  _lhs.y *= _rhs;
  _lhs.z *= _rhs;
  return _lhs;
}

template<typename ValueType>
inline Vector<ValueType, 3u> operator*(Vector<ValueType, 3u> const & _lhs,
                                       Vector<ValueType, 3u> const & _rhs) {
  return Vector<ValueType, 3u>(_lhs.x * _rhs.x, _lhs.y * _rhs.y,
                               _lhs.z * _rhs.z);
}

template<typename ValueType>
inline Vector<ValueType, 3u> operator*(Vector<ValueType, 3u> const & _lhs,
                                       ValueType _rhs) {
  return Vector<ValueType, 3u>(_lhs.x * _rhs, _lhs.y * _rhs, _lhs.z * _rhs);
}

template<typename ValueType>
inline Vector<ValueType, 3u> operator*(ValueType _lhs,
                                       Vector<ValueType, 3u> const & _rhs) {
  return _rhs * _lhs;
}

template<typename ValueType1, typename ValueType2>
inline Vector<ValueType1, 3u> &
operator/=(Vector<ValueType1, 3u> & _lhs, Vector<ValueType2, 3u> const & _rhs) {
  if (!is_nearly_zero(_rhs.x) && !is_nearly_zero(_rhs.y) &&
      !is_nearly_zero(_rhs.z)) {
    _lhs.x /= _rhs.x;
    _lhs.y /= _rhs.y;
    _lhs.z /= _rhs.z;
  }
  return _lhs;
}

template<typename ValueType>
inline Vector<ValueType, 3u> & operator/=(Vector<ValueType, 3u> & _lhs,
                                          ValueType _rhs) {
  if (!is_nearly_zero(_rhs)) {
    _lhs.x /= _rhs;
    _lhs.y /= _rhs;
    _lhs.z /= _rhs;
  }
  return _lhs;
}

template<typename ValueType>
inline Vector<ValueType, 3u> operator/(Vector<ValueType, 3u> const & _lhs,
                                       Vector<ValueType, 3u> const & _rhs) {
  if (!is_nearly_zero(_rhs.x) && !is_nearly_zero(_rhs.y) &&
      !is_nearly_zero(_rhs.z)) {
    return Vector<ValueType, 3u>(_lhs.x / _rhs.x, _lhs.y / _rhs.y,
                                 _lhs.z / _rhs.z);
  }
  return _lhs;
}

template<typename ValueType>
inline Vector<ValueType, 3u> operator/(Vector<ValueType, 3u> const & _lhs,
                                       ValueType _rhs) {
  if (!numerics::is_nearly_zero(_rhs)) {
    return Vector<ValueType, 3u>(_lhs.x / _rhs, _lhs.y / _rhs, _lhs.z / _rhs);
  }
  return _lhs;
}

template<typename ValueType>
inline bool operator==(Vector<ValueType, 3u> const & _lhs,
                       Vector<ValueType, 3u> const & _rhs) {
  return _lhs.x == _rhs.x && _lhs.y == _rhs.y && _lhs.z == _rhs.z;
}

template<typename ValueType>
inline bool operator!=(Vector<ValueType, 3u> const & _lhs,
                       Vector<ValueType, 3u> const & _rhs) {
  return !(_lhs == _rhs);
}

}}

////////////////////////////////////////////////////////////////////////////////
// numerics
////////////////////////////////////////////////////////////////////////////////

namespace numerics {

template<typename ValueType>
using Vector3 = detail::Vector<ValueType, 3u>;

using Vector3i8 = Vector3<int8>;
using Vector3u8 = Vector3<uint8>;
using Vector3i16 = Vector3<int16>;
using Vector3u16 = Vector3<uint16>;
using Vector3i32 = Vector3<int32>;
using Vector3u32 = Vector3<uint32>;
using Vector3i64 = Vector3<int64>;
using Vector3u64 = Vector3<uint64>;
using Vector3f = Vector3<float>;
using Vector3d = Vector3<double>;

}

#endif
