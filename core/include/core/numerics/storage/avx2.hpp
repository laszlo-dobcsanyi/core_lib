#ifndef _CORE_LIB_NUMERICS_STORAGE_AVX2_HPP_
#define _CORE_LIB_NUMERICS_STORAGE_AVX2_HPP_

#ifndef CORE_LIB_NUMERICS
#  error
#endif

// TODO: Implement this, as the current implementation is only a copy of SSE2

////////////////////////////////////////////////////////////////////////////////
// x storage
////////////////////////////////////////////////////////////////////////////////

namespace numerics { namespace matrix { namespace storage {

/*******************************************************************************
** AVX2
*******************************************************************************/

template<typename ValueType, size_t Row, size_t Columns>
struct AVX2;

/*******************************************************************************
** AVX2 ( float, 4x4 )
*******************************************************************************/

template<>
struct AVX2<float, 4u, 4u> : public StorageType<float, 4u, 4u> {
  alignas(16u) Array components;

  AVX2();
  AVX2(Array _components);
  AVX2(AVX2 &&) = default;
  AVX2(AVX2 const &) = default;
  AVX2 & operator=(AVX2 &&) = default;
  AVX2 & operator=(AVX2 const &) = default;
  ~AVX2() = default;

  float & operator[](size_t _index);
  float const & operator[](size_t _index) const;

  void Copy(size_t _index, core::span<float const> _values);
};

inline AVX2<float, 4u, 4u>::AVX2() {}

inline AVX2<float, 4u, 4u>::AVX2(Array _components) {
  // TODO
  ::memcpy(components, _components, sizeof(components));
}

inline float & AVX2<float, 4u, 4u>::operator[](size_t _index) {
  CORE_LIB_ASSERT(_index < 4u * 4u);
  return components[_index];
}

inline float const & AVX2<float, 4u, 4u>::operator[](size_t _index) const {
  CORE_LIB_ASSERT(_index < 4u * 4u);
  return components[_index];
}

inline void AVX2<float, 4u, 4u>::Copy(size_t _index,
                                      core::span<float const> _values) {
  // TODO
  CORE_LIB_ASSERT(!_values.empty());
  uintptr_t const start =
      reinterpret_cast<uintptr_t>(components) + _index * sizeof(float);
  CORE_LIB_ASSERT(reinterpret_cast<uintptr_t>(components) <= start);
  uintptr_t const end = start + _values.size() * sizeof(float);
  CORE_LIB_ASSERT(start <= end);
  uintptr_t const components_end =
      reinterpret_cast<uintptr_t>(components) + 16u * sizeof(float);
  CORE_LIB_ASSERT(end <= components_end);
  ::memcpy(reinterpret_cast<void *>(start), _values.data(),
           _values.size() * sizeof(float));
}

inline AVX2<float, 4u, 4u> add(AVX2<float, 4u, 4u> const & _left,
                               AVX2<float, 4u, 4u> const & _right) {
  AVX2<float, 4u, 4u> result;
  for (auto current = 0u; current < 16u; ++current) {
    result[current] = _left[current] + _right[current];
  }
  return result;
}

inline void multiply_row_major(AVX2<float, 4u, 4u> const & _left,
                               AVX2<float, 4u, 4u> const & _right,
                               AVX2<float, 4u, 4u> & _result) {
  // TODO
  CORE_LIB_UNIMPLEMENTED;
}

inline void multiply_column_major(AVX2<float, 4u, 4u> const & _left,
                                  AVX2<float, 4u, 4u> const & _right,
                                  AVX2<float, 4u, 4u> & _result) {
  __m128 left_line, right_line, r_line;
  for (int i = 0; i < 16; i += 4) {
    // unroll the first step of the loop to avoid having to initialize r_line to zero
    left_line = _mm_load_ps(_left.components); // a_line = vec4(column(a, 0))
    right_line = _mm_set1_ps(_right.components[i]); // b_line = vec4(b[i][0])
    r_line = _mm_mul_ps(left_line, right_line);     // r_line = a_line * b_line
    for (int j = 1; j < 4; j++) {
      left_line =
          _mm_load_ps(&_left.components[j * 4]); // a_line = vec4(column(a, j))
      right_line =
          _mm_set1_ps(_right.components[i + j]); // b_line = vec4(b[i][j])
                                                 // r_line += a_line * b_line
      r_line = _mm_add_ps(_mm_mul_ps(left_line, right_line), r_line);
    }
    _mm_store_ps(&(_result.components[i]), r_line); // r[i] = r_line
  }
}

}}}

#endif
