#ifndef _CORE_LIB_NUMERICS_STORAGE_SSE2_HPP_
#define _CORE_LIB_NUMERICS_STORAGE_SSE2_HPP_

#ifndef CORE_LIB_NUMERICS
#  error
#endif

////////////////////////////////////////////////////////////////////////////////
// x storage
////////////////////////////////////////////////////////////////////////////////

namespace numerics { namespace matrix { namespace storage {

/*******************************************************************************
** SSE2
*******************************************************************************/

template<typename ValueType, size_t Row, size_t Columns>
struct SSE2;

/*******************************************************************************
** SSE2 ( float, 4x4 )
*******************************************************************************/

template<>
struct SSE2<float, 4u, 4u> : public StorageType<float, 4u, 4u> {
  alignas(16u) Array components;

  SSE2();
  SSE2(Array _components);
  SSE2(SSE2 &&) = default;
  SSE2(SSE2 const &) = default;
  SSE2 & operator=(SSE2 &&) = default;
  SSE2 & operator=(SSE2 const &) = default;
  ~SSE2() = default;

  float & operator[](size_t _index);
  float const & operator[](size_t _index) const;

  void Copy(size_t _index, core::span<float const> _values);
};

inline SSE2<float, 4u, 4u>::SSE2() {}

inline SSE2<float, 4u, 4u>::SSE2(Array _components) {
  // TODO
  ::memcpy(components, _components, sizeof(components));
}

inline float & SSE2<float, 4u, 4u>::operator[](size_t _index) {
  CORE_LIB_ASSERT(_index < 4u * 4u);
  return components[_index];
}

inline float const & SSE2<float, 4u, 4u>::operator[](size_t _index) const {
  CORE_LIB_ASSERT(_index < 4u * 4u);
  return components[_index];
}

inline void SSE2<float, 4u, 4u>::Copy(size_t _index,
                                      core::span<float const> _values) {
  // TODO
  CORE_LIB_ASSERT(!_values.empty());
  uintptr_t const start =
      reinterpret_cast<uintptr_t>(components) + _index * sizeof(float);
  CORE_LIB_ASSERT(reinterpret_cast<uintptr_t>(components) <= start);
  CORE_LIB_ASSERT(start + _values.size() * 4u <=
                  reinterpret_cast<uintptr_t>(components) + 64u);
  ::memcpy(reinterpret_cast<void *>(start), _values.data(),
           _values.size() * sizeof(float));
}

inline SSE2<float, 4u, 4u> add(SSE2<float, 4u, 4u> const & _left,
                               SSE2<float, 4u, 4u> const & _right) {
  SSE2<float, 4u, 4u> result;
  for (auto current = 0u; current < 16u; ++current) {
    result[current] = _left[current] + _right[current];
  }
  return result;
}

inline void multiply_row_major(SSE2<float, 4u, 4u> const & _left,
                               SSE2<float, 4u, 4u> const & _right,
                               SSE2<float, 4u, 4u> & _result) {
  // TODO
  CORE_LIB_UNIMPLEMENTED;
}

inline void multiply_column_major(SSE2<float, 4u, 4u> const & _left,
                                  SSE2<float, 4u, 4u> const & _right,
                                  SSE2<float, 4u, 4u> & _result) {
  __m128 left_line, right_line, r_line;
  for (int i = 0; i < 16; i += 4) {
    // unroll the first step of the loop to avoid having to initialize r_line to zero
    left_line = _mm_load_ps(_left.components); // a_line = vec4(column(a, 0))
    right_line = _mm_set1_ps(_right.components[i]); // b_line = vec4(b[i][0])
    r_line = _mm_mul_ps(left_line, right_line);     // r_line = a_line * b_line
    for (int j = 1; j < 4; j++) {
      left_line =
          _mm_load_ps(&_left.components[j * 4]); // a_line = vec4(column(a, j))
      right_line =
          _mm_set1_ps(_right.components[i + j]); // b_line = vec4(b[i][j])
                                                 // r_line += a_line * b_line
      r_line = _mm_add_ps(_mm_mul_ps(left_line, right_line), r_line);
    }
    _mm_store_ps(&(_result.components[i]), r_line); // r[i] = r_line
  }
}

/*******************************************************************************
** SSE2 ( double, 4x4 )
*******************************************************************************/

template<>
struct SSE2<double, 4u, 4u> : public StorageType<double, 4u, 4u> {
  alignas(16u) Array components;

  SSE2();
  SSE2(Array _components);
  SSE2(SSE2 &&) = default;
  SSE2(SSE2 const &) = default;
  SSE2 & operator=(SSE2 &&) = default;
  SSE2 & operator=(SSE2 const &) = default;
  ~SSE2() = default;

  double & operator[](size_t _index);
  double const & operator[](size_t _index) const;

  void Copy(size_t _index, core::span<double const> _values);
};

inline SSE2<double, 4u, 4u>::SSE2() {}

inline SSE2<double, 4u, 4u>::SSE2(Array _components) {
  // TODO
  ::memcpy(components, _components, sizeof(components));
}

inline double & SSE2<double, 4u, 4u>::operator[](size_t _index) {
  CORE_LIB_ASSERT(_index < 4u * 4u);
  return components[_index];
}

inline double const & SSE2<double, 4u, 4u>::operator[](size_t _index) const {
  CORE_LIB_ASSERT(_index < 4u * 4u);
  return components[_index];
}

inline void SSE2<double, 4u, 4u>::Copy(size_t _index,
                                       core::span<double const> _values) {
  // TODO
  CORE_LIB_ASSERT(!_values.empty());
  uintptr_t const start =
      reinterpret_cast<uintptr_t>(components) + _index * sizeof(double);
  CORE_LIB_ASSERT(reinterpret_cast<uintptr_t>(components) <= start);
  CORE_LIB_ASSERT(start + _values.size() * 8u <=
                  reinterpret_cast<uintptr_t>(components) + 128u);
  ::memcpy(reinterpret_cast<void *>(start), _values.data(),
           _values.size() * sizeof(double));
}

inline SSE2<double, 4u, 4u> add(SSE2<double, 4u, 4u> const & _left,
                                SSE2<double, 4u, 4u> const & _right) {
  SSE2<double, 4u, 4u> result;
  for (auto current = 0u; current < 16u; ++current) {
    result[current] = _left[current] + _right[current];
  }
  return result;
}

inline void multiply_row_major(SSE2<double, 4u, 4u> const & _left,
                               SSE2<double, 4u, 4u> const & _right,
                               SSE2<double, 4u, 4u> & _result) {
  // TODO
  CORE_LIB_UNIMPLEMENTED;
}

inline void multiply_column_major(SSE2<double, 4u, 4u> const & _left,
                                  SSE2<double, 4u, 4u> const & _right,
                                  SSE2<double, 4u, 4u> & _result) {
  // TODO
  CORE_LIB_UNIMPLEMENTED;
}

}}}

#endif
