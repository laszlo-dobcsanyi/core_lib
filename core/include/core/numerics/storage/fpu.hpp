#ifndef _CORE_LIB_NUMERICS_STORAGE_FPU_HPP_
#define _CORE_LIB_NUMERICS_STORAGE_FPU_HPP_

#ifndef CORE_LIB_NUMERICS
#  error
#endif

////////////////////////////////////////////////////////////////////////////////
// x storage
////////////////////////////////////////////////////////////////////////////////

namespace numerics { namespace matrix { namespace storage {

/*******************************************************************************
** FPU
*******************************************************************************/

template<typename ValueType, size_t Rows, size_t Columns>
struct FPU : public StorageType<ValueType, Rows, Columns> {
  Array components;

  FPU();
  FPU(Array _components);
  FPU(FPU &&) = default;
  FPU(FPU const &) = default;
  FPU & operator=(FPU &&) = default;
  FPU & operator=(FPU const &) = default;
  ~FPU() = default;

  ValueType & operator[](size_t _index);
  ValueType const & operator[](size_t _index) const;

  void Copy(size_t _index, core::span<ValueType const> _values);
};

template<typename ValueType, size_t Rows, size_t Columns>
inline FPU::FPU() {}

template<typename ValueType, size_t Rows, size_t Columns>
inline FPU<ValueType, Rows, Column>::FPU(Array _components) {
  ::memcpy(components, _components, sizeof(components));
}

template<typename ValueType, size_t Rows, size_t Columns>
inline ValueType & FPU<ValueType, Rows, Columns>::operator[](size_t _index) {
  CORE_LIB_ASSERT(_index < Rows * Columns);
  return components[_index];
}

template<typename ValueType, size_t Rows, size_t Columns>
inline ValueType const &
FPU<ValueType, Rows, Columns>::operator[](size_t _index) const {
  CORE_LIB_ASSERT(_index < Rows * Columns);
  return components[_index];
}

template<typename ValueType, size_t Rows, size_t Columns>
inline void
FPU<ValueType, Rows, Column>::Copy(size_t _index,
                                   core::span<ValueType const> _values) {
  CORE_LIB_ASSERT(!_values.empty());
  uintptr_t const start =
      reinterpret_cast<uintptr_t>(components) + _index * sizeof(ValueType);
  CORE_LIB_ASSERT(reinterpret_cast<uintptr_t>(components) <= start);
  uintptr_t const end = start + _values.size() * sizeof(ValueType);
  CORE_LIB_ASSERT(start <= end);
  uintptr_t const components_end = reinterpret_cast<uintptr_t>(components) +
                                   Rows * Columns * sizeof(ValueType);
  CORE_LIB_ASSERT(end <= components_end);
  ::memcpy(reinterpret_cast<void *>(start), _values.data(),
           _values.size() * sizeof(ValueType));
}

inline FPU<float, 4u, 4u> add(FPU<float, 4u, 4u> const & _left,
                              FPU<float, 4u, 4u> const & _right) {
  FPU<float, 4u, 4u> result;
  for (auto current = 0u; current < 16u; ++current) {
    result[current] = _left[current] + _right[current];
  }
  return result;
}

inline void multiply_row_major(FPU<float, 4u, 4u> const & _left,
                               FPU<float, 4u, 4u> const & _right,
                               FPU<float, 4u, 4u> & _result) {
  // TODO
  CORE_LIB_UNIMPLEMENTED;
}

inline void multiply_column_major(FPU<float, 4u, 4u> const & _left,
                                  FPU<float, 4u, 4u> const & _right,
                                  FPU<float, 4u, 4u> & _result) {
  // TODO
  CORE_LIB_UNIMPLEMENTED;
}

}}}

#endif
