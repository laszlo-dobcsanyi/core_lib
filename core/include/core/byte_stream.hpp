#ifndef _CORE_LIB_MEMORY_BYTE_STREAM_HPP_
#define _CORE_LIB_MEMORY_BYTE_STREAM_HPP_

#include "core/base.h"
#include "core/string.hpp"
#include "core/ranges/span.hpp"

#include "core/byte_stream.hh"

////////////////////////////////////////////////////////////////////////////////
// core detail
////////////////////////////////////////////////////////////////////////////////

namespace core { namespace detail {

/*******************************************************************************
** ByteStream
*******************************************************************************/

template<typename Storage>
class ByteStream {
public:
  ByteStream() = default;
  explicit ByteStream(span<Storage> _bytes);
  ByteStream(ByteStream && _other);
  ByteStream(ByteStream const &) = default;
  ByteStream & operator=(ByteStream && _other);
  ByteStream & operator=(ByteStream const &) = default;
  ~ByteStream() = default;

  bool Empty() const;
  size_t Location() const;
  size_t Available() const;
  span<Storage> Bytes() const;

  void Advance(size_t _offset);
  void Seek(size_t _offset);
  void Reset();

protected:
  span<Storage> storage;
  size_t location = 0u;
};

/*******************************************************************************
** ByteStreamReader
*******************************************************************************/

template<typename Transform>
class ByteStreamReader : public ByteStream<byte const> {
public:
  ByteStreamReader() = default;
  explicit ByteStreamReader(span<byte const> _bytes);
  ByteStreamReader(ByteStreamReader &&) = default;
  ByteStreamReader(ByteStreamReader const &) = default;
  ByteStreamReader & operator=(ByteStreamReader &&) = default;
  ByteStreamReader & operator=(ByteStreamReader const &) = default;
  ~ByteStreamReader() = default;

  size_t Read(char * _cstring, uint32 _storage_size);
  template<class StringStorage>
  bool Read(string::const_string<StringStorage> & _string);

  template<typename T>
  mpl::if_arithmetic_t<T, bool> Read(T & _value);

  int8 ReadInt8();
  uint8 ReadUint8();
  int16 ReadInt16();
  uint16 ReadUint16();
  int32 ReadInt32();
  uint32 ReadUint32();
  int64 ReadInt64();
  uint64 ReadUint64();
  float ReadFloat();
  double ReadDouble();

  span<byte const> RemainingBytes() const;
  span<byte const> RemainingBytes(size_t _max_size) const;

protected:
  size_t NextNullTerminator(size_t _available);

  template<typename T>
  void ReadUnsafe(T & _value);
};

/*******************************************************************************
** ByteStreamWriter
*******************************************************************************/

template<typename Transform>
class ByteStreamWriter : public ByteStream<byte> {
public:
  ByteStreamWriter() = default;
  template<typename... Args>
  ByteStreamWriter(Args &&... _args);
  explicit ByteStreamWriter(span<byte> _bytes);
  ByteStreamWriter(ByteStreamWriter &&) = default;
  ByteStreamWriter(ByteStreamWriter const &) = default;
  ByteStreamWriter & operator=(ByteStreamWriter &&) = default;
  ByteStreamWriter & operator=(ByteStreamWriter const &) = default;
  ~ByteStreamWriter() = default;

  template<typename T>
  mpl::if_arithmetic_t<T, bool> Write(T _value);

  span<byte const> WrittenBytes() const;
};

}}

////////////////////////////////////////////////////////////////////////////////
// core detail
////////////////////////////////////////////////////////////////////////////////

namespace core { namespace detail {

/*******************************************************************************
** ByteStream
*******************************************************************************/

template<typename Storage>
inline ByteStream<Storage>::ByteStream(span<Storage> _bytes)
    : storage(_bytes) {}

template<typename Storage>
inline ByteStream<Storage>::ByteStream(ByteStream && _other)
    : storage(_other.storage)
    , location(_other.location) {
  _other.storage = span<Storage>();
  _other.location = 0u;
}

template<typename Storage>
inline ByteStream<Storage> &
ByteStream<Storage>::operator=(ByteStream && _other) {
  if (this != &_other) {
    storage = _other.storage;
    _other.storage = span<Storage>();
    location = _other.location;
    _other.location = 0u;
  }
  return *this;
}

template<typename Storage>
inline bool ByteStream<Storage>::Empty() const {
  return storage.empty();
}

template<typename Storage>
inline size_t ByteStream<Storage>::Location() const {
  return location;
}

template<typename Storage>
inline size_t ByteStream<Storage>::Available() const {
  CORE_LIB_ASSERT(location <= storage.size());
  return storage.size() - location;
}

template<typename Storage>
inline span<Storage> ByteStream<Storage>::Bytes() const {
  return storage;
}

template<typename Storage>
inline void ByteStream<Storage>::Advance(size_t _offset) {
  location += _offset;
  CORE_LIB_ASSERT(location <= storage.size());
}

template<typename Storage>
inline void ByteStream<Storage>::Seek(size_t _offset) {
  location = _offset;
  CORE_LIB_ASSERT(location <= storage.size());
}

template<typename Storage>
inline void ByteStream<Storage>::Reset() {
  location = 0u;
}

/*******************************************************************************
** ByteStreamReader
*******************************************************************************/

template<typename Transform>
inline ByteStreamReader<Transform>::ByteStreamReader(span<byte const> _bytes)
    : ByteStream<byte const>(_bytes) {}

template<typename Transform>
inline size_t ByteStreamReader<Transform>::Read(char * _cstring,
                                                uint32 _storage_size) {
  auto const length =
      NextNullTerminator(min(Available(), static_cast<size_t>(_storage_size)));
  ::memcpy(_cstring, storage.data(), length);
  *(_cstring + length) = '\0';
  Advance(length);
  return length;
}

template<typename Transform>
template<class StringStorage>
inline bool ByteStreamReader<Transform>::Read(
    string::const_string<StringStorage> & _string) {
  auto const length = NextNullTerminator(Available());
  if (1u < length) {
    void const * cstr =
        reinterpret_cast<char const *>(storage.data() + location);
    _string = string::const_string<StringStorage>(cstr, length);
  } else {
    _string = string::const_string<StringStorage>();
  }
  Advance(length + 1u);
  return length;
}

template<typename Transform>
template<typename T>
inline mpl::if_arithmetic_t<T, bool>
ByteStreamReader<Transform>::Read(T & _value) {
  if (sizeof(_value) <= Available()) {
    ReadUnsafe(_value);
    return true;
  }
  return false;
}

template<typename Transform>
inline int8 ByteStreamReader<Transform>::ReadInt8() {
  int8 value;
  ReadUnsafe(value);
  return value;
}

template<typename Transform>
inline uint8 ByteStreamReader<Transform>::ReadUint8() {
  int8 value;
  ReadUnsafe(value);
  return value;
}

template<typename Transform>
inline int16 ByteStreamReader<Transform>::ReadInt16() {
  int16 value;
  ReadUnsafe(value);
  return value;
}

template<typename Transform>
inline uint16 ByteStreamReader<Transform>::ReadUint16() {
  uint16 value;
  ReadUnsafe(value);
  return value;
}

template<typename Transform>
inline int32 ByteStreamReader<Transform>::ReadInt32() {
  int32 value;
  ReadUnsafe(value);
  return value;
}

template<typename Transform>
inline uint32 ByteStreamReader<Transform>::ReadUint32() {
  uint32 value;
  ReadUnsafe(value);
  return value;
}

template<typename Transform>
inline int64 ByteStreamReader<Transform>::ReadInt64() {
  int64 value;
  ReadUnsafe(value);
  return value;
}

template<typename Transform>
inline uint64 ByteStreamReader<Transform>::ReadUint64() {
  uint64 value;
  ReadUnsafe(value);
  return value;
}

template<typename Transform>
inline float ByteStreamReader<Transform>::ReadFloat() {
  float value;
  ReadUnsafe(value);
  return value;
}

template<typename Transform>
inline double ByteStreamReader<Transform>::ReadDouble() {
  double value;
  ReadUnsafe(value);
  return value;
}

template<typename Transform>
inline span<byte const> ByteStreamReader<Transform>::RemainingBytes() const {
  return make_span(storage.data() + this->location, Available());
}

template<typename Transform>
inline span<byte const>
ByteStreamReader<Transform>::RemainingBytes(size_t _max_size) const {
  return make_span(storage.data() + this->location,
                   min(Available(), _max_size));
}

template<typename Transform>
template<typename T>
inline void ByteStreamReader<Transform>::ReadUnsafe(T & _value) {
  CORE_LIB_ASSERT(sizeof(_value) <= Available());
  T const & source = *reinterpret_cast<T const *>(&storage[location]);
  memory::copy(_value, source);
  Transform::Read(_value);
  Advance(sizeof(_value));
}

template<typename Transform>
inline size_t
ByteStreamReader<Transform>::NextNullTerminator(size_t _available) {
  CORE_LIB_ASSERT(_available <= Available());
  size_t length = 0u;
  auto end = reinterpret_cast<uintptr_t>(&storage[location]);
  while (length < _available) {
    if (*reinterpret_cast<char *>(end) != '\0') {
      length++;
      end++;
    } else {
      break;
    }
  }
  return length;
}

/*******************************************************************************
** ByteStreamWriter
*******************************************************************************/

template<typename Transform>
inline ByteStreamWriter<Transform>::ByteStreamWriter(span<byte> _bytes)
    : ByteStream<byte>(_bytes) {}

template<typename Transform>
template<typename T>
inline mpl::if_arithmetic_t<T, bool>
ByteStreamWriter<Transform>::Write(T _value) {
  if (sizeof(_value) <= Available()) {
    T & destination = *reinterpret_cast<T *>(&storage[location]);
    memory::copy(destination, _value);
    Transform::Write(destination);
    Advance(sizeof(_value));
    return true;
  }
  return false;
}

template<typename Transform>
inline span<byte const> ByteStreamWriter<Transform>::WrittenBytes() const {
  return make_span(reinterpret_cast<byte const *>(storage.data()), location);
}

}}

#endif
