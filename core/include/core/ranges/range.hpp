#ifndef _CORE_RANGES_RANGE_HPP_
#define _CORE_RANGES_RANGE_HPP_

#include "core/iterator/iterators.hpp"

////////////////////////////////////////////////////////////////////////////////
// core ranges
////////////////////////////////////////////////////////////////////////////////

namespace core { namespace ranges {

/*******************************************************************************
** range
*******************************************************************************/

template<typename Iterator>
class range {
public:
  using value_type = typename Iterator::value_type;
  using pointer_type = typename Iterator::pointer_type;
  using reference_type = typename Iterator::reference_type;

  range() = default;
  range(Iterator _begin, Iterator _end)
      : begin_iterator(_begin)
      , end_iterator(_end) {}
  template<typename U>
  range(range<U> const & _other)
      : begin_iterator(_other.begin())
      , end_iterator(_other.end()) {}
  range(range &&) = default;
  range(range const &) = default;
  range & operator=(range &&) = default;
  range & operator=(range const &) = default;
  ~range() = default;

  template<typename It = Iterator>
  if_contiguous_iterator_t<It, size_t> size() const {
    return static_cast<size_t>(static_cast<pointer_type>(end_iterator) -
                               static_cast<pointer_type>(begin_iterator));
  }

  template<typename It = Iterator>
  if_contiguous_iterator_t<It, bool> empty() const {
    return size() == 0;
  }

  template<typename It = Iterator>
  explicit operator if_contiguous_iterator_t<It, bool>() const {
    return !empty();
  }

  template<typename It = Iterator>
  if_contiguous_iterator_t<It, pointer_type> data() const {
    return static_cast<pointer_type>(begin_iterator);
  }

  template<typename It = Iterator>
  if_contiguous_iterator_t<It, reference_type> operator[](size_t _index) const {
    CORE_LIB_ASSERT(_index < size());
    return data()[_index];
  }

  Iterator begin() const { return begin_iterator; }
  Iterator end() const { return end_iterator; }

private:
  Iterator begin_iterator;
  Iterator end_iterator;
};

}}

////////////////////////////////////////////////////////////////////////////////
// core
////////////////////////////////////////////////////////////////////////////////

namespace core {

/*******************************************************************************
** range
*******************************************************************************/

template<typename Iterator>
using range = ranges::range<Iterator>;

}

#endif
