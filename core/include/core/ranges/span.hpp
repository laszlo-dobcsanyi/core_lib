#ifndef _CORE_LIB_RANGES_SPAN_HPP_
#define _CORE_LIB_RANGES_SPAN_HPP_

#include "core/ranges/range.hpp"
#include "core/iterator/contiguous_iterator.hpp"

////////////////////////////////////////////////////////////////////////////////
// core
////////////////////////////////////////////////////////////////////////////////

namespace core {

/*******************************************************************************
** span
*******************************************************************************/

template<typename T>
using span = range<contiguous_iterator<T>>;

/*******************************************************************************
** value_type_t
*******************************************************************************/

template<class T>
using value_type_t = typename T::value_type;

/*******************************************************************************
** iterator_type_t
*******************************************************************************/

template<class T>
using iterator_type_t = typename T::iterator_type;

/*******************************************************************************
** const_iterator_type_t
*******************************************************************************/

template<class T>
using const_iterator_type_t = typename T::const_iterator_type;

/*******************************************************************************
** make_span
*******************************************************************************/

template<typename T>
inline if_contiguous_iterator_t<iterator_type_t<T>, span<value_type_t<T>>>
make_span(T & object) {
  return span<value_type_t<T>>(object.begin(), object.end());
}

template<typename T>
inline if_contiguous_iterator_t<iterator_type_t<T>, span<value_type_t<T> const>>
make_span(T const & object) {
  return span<value_type_t<T> const>(object.begin(), object.end());
}

template<typename T, size_t N>
inline span<T> make_span(T (&_array)[N]) {
  return span<T>(contiguous_iterator<T>(_array),
                 contiguous_iterator<T>(_array + N));
}

template<typename T>
inline span<T> make_span(T * _array, size_t _size) {
  return span<T>(contiguous_iterator<T>(_array),
                 contiguous_iterator<T>(_array + _size));
}

}

#endif
