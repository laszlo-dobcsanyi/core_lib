#ifndef _CORE_LIB_TIME_HPP_
#define _CORE_LIB_TIME_HPP_

#include "core/base.h"

#if defined(CORE_LIB_OS_LINUX)
#  include <time.h>
#endif

////////////////////////////////////////////////////////////////////////////////
// core
////////////////////////////////////////////////////////////////////////////////

namespace core {

/*******************************************************************************
** TimeStamp
*******************************************************************************/

struct TimeStamp {
#if defined(CORE_LIB_OS_LINUX)
  timespec value;
#elif defined(CORE_LIB_OS_WINDOWS)
  LARGE_INTEGER value;
#endif
};

/*******************************************************************************
** now
*******************************************************************************/

CORE_LIB_FORCE_INLINE TimeStamp now() {
  TimeStamp result;
#if defined(CORE_LIB_OS_LINUX)
  ::clock_gettime(CLOCK_MONOTONIC_RAW, &result.value);
#elif defined(CORE_LIB_OS_WINDOWS)
  ::QueryPerformanceCounter(&result.value);
#endif
  return result;
}

/*******************************************************************************
** TimeSpan
*******************************************************************************/

struct TimeSpan {
#if defined(CORE_LIB_OS_LINUX)
  timespec elapsed;
#elif defined(CORE_LIB_OS_WINDOWS)
  LARGE_INTEGER elapsed;
#endif
};

CORE_LIB_FORCE_INLINE TimeSpan operator-(TimeStamp const & _start,
                                         TimeStamp const & _end) {
  TimeSpan result;
#if defined(CORE_LIB_OS_LINUX)
  result.elapsed.tv_sec = _start.value.tv_sec - _end.value.tv_sec;
  result.elapsed.tv_nsec = _start.value.tv_nsec - _end.value.tv_nsec;
#elif defined(CORE_LIB_OS_WINDOWS)
  result.elapsed.QuadPart = _start.value.QuadPart - _end.value.QuadPart;
#endif
  return result;
}

CORE_LIB_FORCE_INLINE uint64 to_nanoseconds(TimeSpan _time_span) {
  uint64 result;
#if defined(CORE_LIB_OS_LINUX)
  result =
      _time_span.elapsed.tv_sec * I64(1000000000) + _time_span.elapsed.tv_nsec;
#elif defined(CORE_LIB_OS_WINDOWS)
  LARGE_INTEGER frequency;
  ::QueryPerformanceFrequency(&frequency);

  result = _time_span.elapsed.QuadPart;
  result *= 1000000000;
  result /= frequency.QuadPart;
#endif
  return result;
}

CORE_LIB_FORCE_INLINE double to_seconds(TimeSpan _time_span) {
  double result;
#if defined(CORE_LIB_OS_LINUX)
  result = static_cast<double>(_time_span.elapsed.tv_sec);
  result =
      result + static_cast<double>(_time_span.elapsed.tv_nsec) / 1000000000.;
#elif defined(CORE_LIB_OS_WINDOWS)
  LARGE_INTEGER frequency;
  ::QueryPerformanceFrequency(&frequency);

  result = static_cast<double>(_time_span.elapsed.QuadPart);
  result /= static_cast<double>(frequency.QuadPart);
#endif
  return result;
}

/*******************************************************************************
** HighResolutionClock
*******************************************************************************/

class HighResolutionClock final {
public:
  HighResolutionClock();
  HighResolutionClock(HighResolutionClock &&) = delete;
  HighResolutionClock(HighResolutionClock const &) = delete;
  HighResolutionClock & operator=(HighResolutionClock &&) = delete;
  HighResolutionClock & operator=(HighResolutionClock const &) = delete;
  ~HighResolutionClock() = default;

  TimeSpan Elapsed() const;
  TimeSpan Reset();

private:
  TimeStamp start;
};

CORE_LIB_FORCE_INLINE HighResolutionClock::HighResolutionClock()
    : start(now()) {}

CORE_LIB_FORCE_INLINE TimeSpan HighResolutionClock::Elapsed() const {
  return now() - start;
}

CORE_LIB_FORCE_INLINE TimeSpan HighResolutionClock::Reset() {
  TimeStamp end = now();
  TimeSpan elapsed = end - start;
  start = end;
  return elapsed;
}

}

#endif
