#ifndef _CORE_LIB_CALLABLE_VIRTUAL_CALLABLE_HPP_
#define _CORE_LIB_CALLABLE_VIRTUAL_CALLABLE_HPP_

#include "core/base.h"

////////////////////////////////////////////////////////////////////////////////
// core
////////////////////////////////////////////////////////////////////////////////

namespace core {

template<typename Result, typename Arguments>
class VirtualCallable;

/*******************************************************************************
** VirtualCallable
*******************************************************************************/

template<typename Result>
class VirtualCallable<Result, mpl::TypePack<void>> {
public:
  VirtualCallable() = default;
  VirtualCallable(VirtualCallable &&) = delete;
  VirtualCallable(VirtualCallable const &) = default;
  VirtualCallable & operator=(VirtualCallable &&) = default;
  VirtualCallable & operator=(VirtualCallable const &) = default;
  virtual ~VirtualCallable() = default;

  virtual Result operator()() = 0;
};

template<typename Result, typename... Args>
class VirtualCallable<Result, mpl::TypePack<Args...>> {
public:
  VirtualCallable() = default;
  VirtualCallable(VirtualCallable &&) = default;
  VirtualCallable(VirtualCallable const &) = default;
  VirtualCallable & operator=(VirtualCallable &&) = default;
  VirtualCallable & operator=(VirtualCallable const &) = default;
  virtual ~VirtualCallable() = default;

  virtual Result operator()(Args &&...) = 0;
};

}

#endif
