#ifndef _CORE_LIB_CALLABLE_DELEGATE_HPP_
#define _CORE_LIB_CALLABLE_DELEGATE_HPP_

#include "core/base.h"

////////////////////////////////////////////////////////////////////////////////
// core
////////////////////////////////////////////////////////////////////////////////

namespace core {

/*******************************************************************************
** Delegate
*******************************************************************************/

template<typename>
class Delegate;

template<typename R, typename... Args>
class Delegate<R(Args...)> final {
private:
  R (*invoker)(void *, Args &&...);
  void * object;

  Delegate(R (*_invoker)(void *, Args &&...))
      : Delegate(_invoker, nullptr) {}
  Delegate(R (*_invoker)(void *, Args &&...), void * _object)
      : invoker(_invoker)
      , object(_object) {}

  template<typename Functor>
  static R invoke_functor(void * _object, Args &&... _args) {
    auto object = reinterpret_cast<Functor *>(_object);
    return (*object)(forward<Args>(_args)...);
  }

  static R invoke_function(void * _object, Args &&... _args) {
    auto fptr = reinterpret_cast<R (*)(Args...)>(_object);
    return fptr(forward<Args>(_args)...);
  }

  template<class C, R (C::*Method)(Args...)>
  static R invoke_method(void * _object, Args &&... _args) {
    auto object = reinterpret_cast<C *>(_object);
    return (object->*Method)(forward<Args>(_args)...);
  }

  template<class C, R (C::*Method)(Args...) const>
  static R invoke_const_method(void * _object, Args &&... _args) {
    auto object = reinterpret_cast<C *>(_object);
    return (object->*Method)(forward<Args>(_args)...);
  }

public:
  Delegate()
      : invoker(nullptr)
      , object(nullptr) {}
  ~Delegate() = default;

  template<typename Functor>
  static Delegate Bind(Functor && _functor) {
    return Delegate(&invoke_functor<Functor>, &_functor);
  }

  static Delegate Bind(R (*_fptr)(Args...)) {
    return Delegate(&invoke_function, reinterpret_cast<void *>(_fptr));
  }

  template<class C>
  class Binder {
  public:
    template<R (C::*Method)(Args...)>
    Delegate Method() {
      return Delegate(&invoke_method<C, Method>, instance);
    }

    template<R (C::*Method)(Args...) const>
    Delegate Method() {
      return Delegate(&invoke_const_method<C, Method>, instance);
    }

  private:
    friend Delegate;

    C * instance = nullptr;

    explicit Binder(C * _instance)
        : instance(_instance) {}
  };

  template<class C>
  static Binder<C> Bind(C * _instance) {
    return Binder<C>(_instance);
  }

  explicit operator bool() const { return invoker != nullptr; }

  R operator()(Args... _args) {
    return (*invoker)(object, forward<Args>(_args)...);
  }
};

}

#endif
