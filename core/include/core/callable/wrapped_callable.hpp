#ifndef _CORE_LIB_CALLABLE_STATIC_FUNCTION_HPP_
#define _CORE_LIB_CALLABLE_STATIC_FUNCTION_HPP_

#include "core/base.h"

////////////////////////////////////////////////////////////////////////////////
// core
////////////////////////////////////////////////////////////////////////////////

namespace core {

namespace detail {

template<typename, typename>
class CallableWrapperImpl;

template<typename F, typename R, typename... Args>
class CallableWrapperImpl<F, mpl::Signature<R, Args...>> {
public:
  CallableWrapperImpl(F _f)
      : f(move(_f)) {}

  R operator()(Args &&... _args) { return f(forward<Args>(_args)...); }

private:
  F f;
};

template<typename R, typename... Args>
class CallableWrapperImpl<R (*)(Args...), mpl::Signature<R, Args...>> {
public:
  CallableWrapperImpl(R (*_fptr)(Args...))
      : fptr(_fptr) {}

  R operator()(Args... _args) { return (*fptr)(_args...); }

private:
  R (*fptr)(Args...);
};

template<class C, typename R, typename... Args>
class CallableWrapperImpl<R (C::*)(Args...), mpl::Signature<R, Args...>> {
public:
  CallableWrapperImpl(R (C::*_mptr)(Args...), C & _instance)
      : mptr(_mptr)
      , instance(&_instance) {}

  R operator()(Args... _args) { return (instance->*mptr)(_args...); }

private:
  R (C::*mptr)(Args...);
  C * instance;
};

}

/*******************************************************************************
** wrap_callable
*******************************************************************************/

template<typename F>
inline mpl::if_has_call_operator_t<
    F, detail::CallableWrapperImpl<F, mpl::signature_of_t<F>>>
wrap_callable(F && _f) {
  return detail::CallableWrapperImpl<F, mpl::signature_of_t<F>>(forward<F>(_f));
}

template<typename R, typename... Args>
inline detail::CallableWrapperImpl<R (*)(Args...), mpl::Signature<R, Args...>>
wrap_callable(R (*_fptr)(Args...)) {
  return detail::CallableWrapperImpl<R (*)(Args...),
                                     mpl::Signature<R, Args...>>(_fptr);
}

template<class C, typename R, typename... Args>
inline detail::CallableWrapperImpl<R (C::*)(Args...),
                                   mpl::Signature<R, Args...>>
wrap_callable(R (C::*_fptr)(Args...), C & _instance) {
  return detail::CallableWrapperImpl<R (C::*)(Args...),
                                     mpl::Signature<R, Args...>>(_fptr,
                                                                 _instance);
}

/*******************************************************************************
** CallableWrapper
*******************************************************************************/

template<typename F>
using CallableWrapper = detail::CallableWrapperImpl<F, mpl::signature_of_t<F>>;

}

#endif
