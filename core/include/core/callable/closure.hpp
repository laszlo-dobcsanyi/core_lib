#ifndef _CORE_LIB_CALLABLE_CLOSURE_HPP_
#define _CORE_LIB_CALLABLE_CLOSURE_HPP_

#include "core/callable/wrapped_callable.hpp"
#include "core/callable/virtual_callable.hpp"

////////////////////////////////////////////////////////////////////////////////
// core detail
////////////////////////////////////////////////////////////////////////////////

namespace core { namespace detail {

template<typename F, typename Result, typename ParameterArgs, typename CallArgs>
class FunctionClosure;
template<typename F, typename Result, typename ParameterArgs, typename CallArgs>
class CallableFunctionClosure;

template<typename F, typename ParameterCounts>
struct ClosureTypes {
  using ResultType = mpl::result_of_t<F>;
  using ParametersType = mpl::parameters_of_t<F>;
  using CallParametersType =
      mpl::first_t<ParametersType, ParameterCounts::CallParametersCount>;
  using StoredParametersType =
      mpl::last_t<ParametersType, ParameterCounts::StoredParametersCount>;
  CORE_LIB_STATIC_ASSERT(
      mpl::is_same<ParametersType,
                   mpl::join_typepacks<CallParametersType,
                                       StoredParametersType>>::value);

  using ClosureType =
      FunctionClosure<F, ResultType, StoredParametersType, CallParametersType>;
  using CallableClosureType =
      CallableFunctionClosure<F, ResultType, StoredParametersType,
                              CallParametersType>;
};

template<typename F, size_t CallParameterCount>
struct ClosureParameterCounts {
  static constexpr size_t ParametersCount =
      mpl::sizeof_typepack<mpl::parameters_of_t<F>>::value;
  static constexpr size_t CallParametersCount = CallParameterCount;
  static constexpr size_t StoredParametersCount =
      ParametersCount - CallParametersCount;
};

template<typename F, typename... CallArgs>
using ClosureTypesFromCallArgs = ClosureTypes<
    F, ClosureParameterCounts<
           F, mpl::sizeof_typepack<mpl::TypePack<CallArgs...>>::value>>;
template<typename F, typename... StoredArgs>
using ClosureTypesFromStoredArgs = ClosureTypes<
    F, ClosureParameterCounts<
           F, mpl::sizeof_typepack<mpl::parameters_of_t<F>>::value -
                  mpl::sizeof_typepack<mpl::TypePack<StoredArgs...>>::value>>;

}}

////////////////////////////////////////////////////////////////////////////////
// core
////////////////////////////////////////////////////////////////////////////////

namespace core {

template<typename F, typename... CallArguments>
using Closure =
    typename detail::ClosureTypesFromCallArgs<F, CallArguments...>::ClosureType;
template<typename F, typename... CallArguments>
using CallableClosure = typename detail::ClosureTypesFromCallArgs<
    F, CallArguments...>::CallableClosureType;
template<typename F, typename... StoredArguments>
using BoundClosure = typename detail::ClosureTypesFromStoredArgs<
    F, StoredArguments...>::ClosureType;
template<typename F, typename... StoredArguments>
using BoundCallableClosure = typename detail::ClosureTypesFromStoredArgs<
    F, StoredArguments...>::CallableClosureType;

}

////////////////////////////////////////////////////////////////////////////////
// core detail
////////////////////////////////////////////////////////////////////////////////

namespace core { namespace detail {

/*******************************************************************************
** FunctionClosure ( No Parameters )
*******************************************************************************/

template<typename F, typename R, typename... CallArgs>
class FunctionClosure<F, R, mpl::TypePack<>, mpl::TypePack<CallArgs...>> {
public:
  FunctionClosure(CallableWrapper<F> _callable_wrapper);
  FunctionClosure(FunctionClosure &&) = default;
  FunctionClosure(FunctionClosure const &) = default;
  FunctionClosure & operator=(FunctionClosure &&) = default;
  FunctionClosure & operator=(FunctionClosure const &) = default;
  ~FunctionClosure() = default;

  R operator()(CallArgs &&... _call_args);

private:
  CallableWrapper<F> callable_wrapper;
};

/*******************************************************************************
** FunctionClosure ( With Parameters )
*******************************************************************************/

template<typename F, typename R, typename... ParameterArgs,
         typename... CallArgs>
class FunctionClosure<F, R, mpl::TypePack<ParameterArgs...>,
                      mpl::TypePack<CallArgs...>> {
public:
  FunctionClosure(CallableWrapper<F> _callable_wrapper,
                  ParameterArgs... _parameter_args);
  FunctionClosure(FunctionClosure &&) = default;
  FunctionClosure(FunctionClosure const &) = default;
  FunctionClosure & operator=(FunctionClosure &&) = default;
  FunctionClosure & operator=(FunctionClosure const &) = default;
  ~FunctionClosure() = default;

  R operator()(CallArgs &&... _call_args);

private:
  CallableWrapper<F> callable_wrapper;

  using ParameterPack = mpl::TypePack<ParameterArgs...>;
  using ParameterTuple = mpl::TupleOfTypePack<ParameterPack>;
  ParameterTuple parameters;

  template<size_t... S>
  R Call(CallArgs &&... _call_args, mpl::Sequence<S...>);
};

/*******************************************************************************
** CallableFunctionClosure
*******************************************************************************/

template<typename F, typename R, typename... ParameterArgs,
         typename... CallArgs>
class CallableFunctionClosure<F, R, mpl::TypePack<ParameterArgs...>,
                              mpl::TypePack<CallArgs...>>
    : public FunctionClosure<F, R, mpl::TypePack<ParameterArgs...>,
                             mpl::TypePack<CallArgs...>>
    , public VirtualCallable<R, mpl::TypePack<CallArgs...>> {
public:
  using FunctionClosureType =
      FunctionClosure<F, R, mpl::TypePack<ParameterArgs...>,
                      mpl::TypePack<CallArgs...>>;
  using FunctionClosureType::FunctionClosure;
  virtual ~CallableFunctionClosure() override = default;

  virtual R operator()(CallArgs &&... _call_args) override;
};

}}

////////////////////////////////////////////////////////////////////////////////
// core
////////////////////////////////////////////////////////////////////////////////

namespace core {

/*******************************************************************************
** bind_closure
*******************************************************************************/

template<typename F, typename... Args>
inline BoundClosure<F, Args...>
bind_closure(CallableWrapper<F> _callable_wrapper, Args &&... _args) {
  return BoundClosure<F, Args...>(move(_callable_wrapper),
                                  forward<Args>(_args)...);
}

/*******************************************************************************
** bind_callable_closure
*******************************************************************************/

template<typename F, typename... Args>
inline BoundCallableClosure<F, Args...>
bind_callable_closure(CallableWrapper<F> _callable_wrapper, Args &&... _args) {
  return BoundCallableClosure<F, Args...>(move(_callable_wrapper),
                                          forward<Args>(_args)...);
}

}

////////////////////////////////////////////////////////////////////////////////
// core detail
////////////////////////////////////////////////////////////////////////////////

namespace core { namespace detail {

/*******************************************************************************
** FunctionClosure ( No Parameters )
*******************************************************************************/

template<typename F, typename R, typename... CallArgs>
inline FunctionClosure<F, R, mpl::TypePack<>, mpl::TypePack<CallArgs...>>::
    FunctionClosure(CallableWrapper<F> _callable_wrapper)
    : callable_wrapper(move(_callable_wrapper)) {}

template<typename F, typename R, typename... CallArgs>
inline R
FunctionClosure<F, R, mpl::TypePack<>, mpl::TypePack<CallArgs...>>::operator()(
    CallArgs &&... _call_args) {
  return callable_wrapper(forward<CallArgs>(_call_args)...);
}

/*******************************************************************************
** FunctionClosure ( With Parameters )
*******************************************************************************/

template<typename F, typename R, typename... ParameterArgs,
         typename... CallArgs>
inline FunctionClosure<F, R, mpl::TypePack<ParameterArgs...>,
                       mpl::TypePack<CallArgs...>>::
    FunctionClosure(CallableWrapper<F> _callable_wrapper,
                    ParameterArgs... _parameter_args)
    : callable_wrapper(move(_callable_wrapper))
    , parameters(forward<ParameterArgs>(_parameter_args)...) {}

template<typename F, typename R, typename... ParameterArgs,
         typename... CallArgs>
inline R FunctionClosure<
    F, R, mpl::TypePack<ParameterArgs...>,
    mpl::TypePack<CallArgs...>>::operator()(CallArgs &&... _call_args) {
  using ParameterTypePack = mpl::TypePack<ParameterArgs...>;
  return Call(
      forward<CallArgs>(_call_args)...,
      mpl::GenerateSequence<mpl::sizeof_typepack<ParameterTypePack>::value>());
}

template<typename F, typename R, typename... ParameterArgs,
         typename... CallArgs>
template<size_t... S>
inline R
FunctionClosure<F, R, mpl::TypePack<ParameterArgs...>,
                mpl::TypePack<CallArgs...>>::Call(CallArgs &&... _call_args,
                                                  mpl::Sequence<S...>) {
  return callable_wrapper(forward<CallArgs>(_call_args)...,
                          move(std::get<S>(parameters))...);
}

/*******************************************************************************
** CallableFunctionClosure
*******************************************************************************/

template<typename F, typename R, typename... ParameterArgs,
         typename... CallArgs>
inline R CallableFunctionClosure<
    F, R, mpl::TypePack<ParameterArgs...>,
    mpl::TypePack<CallArgs...>>::operator()(CallArgs &&... _call_args) {
  return FunctionClosureType::operator()(forward<CallArgs>(_call_args)...);
}

}}

#endif
