#ifndef _CORE_LIB_APPLICATION_HPP_
#define _CORE_LIB_APPLICATION_HPP_

#include "core/base.h"
#include "core/string.hpp"
#include "core/optional.hpp"

#include "core/memory/unique_array.hpp"

////////////////////////////////////////////////////////////////////////////////
// application
////////////////////////////////////////////////////////////////////////////////

namespace application {

#if defined(CORE_LIB_OS_WINDOWS)
inline int filter(unsigned int _code,
                  struct _EXCEPTION_POINTERS * _exception_pointers) {
  stack_trace::dump(_exception_pointers->ContextRecord);
  CORE_LIB_EXCEPTION("Exception!\n");
  return EXCEPTION_EXECUTE_HANDLER;
}
#endif

/*******************************************************************************
** run
*******************************************************************************/

template<typename F, typename... Args>
inline void run(F && _f, Args &&... _args) {
#if defined(CORE_LIB_OS_WINDOWS)
  __try {
#endif
    _f(forward<Args>(_args)...);
#if defined(CORE_LIB_OS_WINDOWS)
  } __except (filter(GetExceptionCode(), GetExceptionInformation())) {
    ::exit(1);
  }
#endif
}

/*******************************************************************************
** program_main
*******************************************************************************/

template<typename F, typename... Args>
inline void program_main(F && _f, Args &&... _args) {
  logging::Initialize();
  stack_trace::Initialize();
  { run(_f, forward<Args>(_args)...); }
  stack_trace::Finalize();
  logging::Finalize();
}

/*******************************************************************************
** CommandLine
*******************************************************************************/

class CommandLine {
public:
  CommandLine() = default;
  CommandLine(char * _command_line);
  CommandLine(char ** _command_line);
  CommandLine(CommandLine &&) = default;
  CommandLine(CommandLine const &) = delete;
  CommandLine & operator=(CommandLine &&) = default;
  CommandLine & operator=(CommandLine const &) = delete;
  ~CommandLine() = default;

  bool Has(const_string const & _name) const;
  core::Optional<const_string> Get(const_string const & _name) const;

private:
  core::UniqueArray<const_string> arguments;
};

/*******************************************************************************
** CommandLine
*******************************************************************************/

inline CommandLine::CommandLine(char * _command_line) {
  size_t arguments_count = 0u;
  {
    char * current = _command_line;
    while (*current != '\0') {
      char * next = current + 1;
      if (*current != ' ') {
        if (*next == ' ' || *next == '\0') {
          ++arguments_count;
        }
      }
      ++current;
    }
  }
  if (arguments_count == 0u) {
    return;
  }
  arguments = core::make_unique_array<const_string>(arguments_count);
  {
    size_t argument_index = 0u;
    char * current = _command_line;
    char * argument = *current != ' ' && *current != '\0' ? current : nullptr;
    while (*current != '\0') {
      char * next = current + 1;
      if (*current == ' ') {
        if (*next != ' ') {
          argument = next;
        }
      } else if (*next == ' ' || *next == '\0') {
        CORE_LIB_ASSERT(argument);
        size_t argument_size = next - argument;
        typename const_string::storage_type storage(argument_size);
        ::memcpy(storage.Get(), argument, argument_size);
        arguments[argument_index] = const_string(move(storage));
        CORE_LIB_LOG(LOG, "Argument %zu: '%s'\n", argument_index,
                     cstr(arguments[argument_index]));
        ++argument_index;
        argument = nullptr;
      }
      ++current;
    }
    CORE_LIB_ASSERT(!argument);
    CORE_LIB_ASSERT(argument_index == arguments_count);
  }
}

inline CommandLine::CommandLine(char ** _command_line) {
  CORE_LIB_ASSERT(_command_line);
  size_t arguments_count = 0u;
  {
    char ** argument = _command_line;
    while (*argument != nullptr) {
      ++arguments_count;
      ++argument;
    }
    CORE_LIB_ASSERT(1 <= arguments_count);
  }
  if (arguments_count == 1u) {
    return;
  }
  arguments = core::make_unique_array<const_string>(arguments_count - 1);
  {
    for (size_t argument_index = 0; argument_index < arguments_count - 1;
         ++argument_index) {
      arguments[argument_index] = _command_line[argument_index + 1];
      CORE_LIB_LOG(LOG, "Argument %zu: '%s'\n", argument_index,
                   cstr(arguments[argument_index]));
    }
  }
}

inline bool CommandLine::Has(const_string const & _name) const {
  for (const_string const & argument : arguments) {
    if (argument == _name) {
      return true;
    }
  }
  return false;
}

inline core::Optional<const_string>
CommandLine::Get(const_string const & _name) const {
  for (const_string const & argument : arguments) {
    char const * current = cstr(argument);
    while (*current != '\0') {
      if (*current == '=') {
        return const_string(current + 1u);
      }
      ++current;
    }
  }
  return core::nullopt;
}

}

#endif