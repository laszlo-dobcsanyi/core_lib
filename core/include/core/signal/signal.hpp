#ifndef _CORE_LIB_SIGNAL_SIGNAL_HPP_
#define _CORE_LIB_SIGNAL_SIGNAL_HPP_

#include "core/containers.hpp"
#include "core/signal/slot.hpp"

////////////////////////////////////////////////////////////////////////////////
// core collector
////////////////////////////////////////////////////////////////////////////////

namespace core { namespace collector {

/*******************************************************************************
** First
*******************************************************************************/

template<typename T>
struct First {
  First() = default;
  First(T _value)
      : value(_value) {}

  template<typename U>
  void operator()(U && _value) {
    if (first) {
      value = forward(_value);
      first = false;
    }
  }

  T result() { return value; }

  T value;
  bool first = true;
};

/*******************************************************************************
** Last
*******************************************************************************/

template<typename T>
struct Last {
  Last() = default;
  Last(T _value)
      : value(_value) {}

  template<typename U>
  void operator()(U && _value) {
    value = forward<U>(_value);
  }

  T result() { return value; }

  T value;
};

}}

////////////////////////////////////////////////////////////////////////////////
// core
////////////////////////////////////////////////////////////////////////////////

namespace core {

/*******************************************************************************
** Signal (Interface)
*******************************************************************************/

template<class Interface>
class Signal {
public:
  using SlotType = Slot<Interface>;

  void connect(SlotType & _slot) { slot_list.push_front(_slot); }

  void disconnect(SlotType & _slot) { slot_list.erase(_slot); }

  template<typename R, typename... Args>
  mpl::if_void_t<R, void> operator()(R (Interface::*_fptr)(Args...),
                                     Args... _args) {
    invoke(_fptr, forward<Args>(_args)...);
  }

  template<typename R, typename... Args>
  mpl::if_void_t<R, void> invoke(R (Interface::*_fptr)(Args...),
                                 Args... _args) {
    for (SlotType & slot : slot_list) {
      auto * instance = static_cast<Interface *>(&slot);
      (instance->*_fptr)(forward<Args>(_args)...);
    }
  }

  template<typename R, typename... Args>
  mpl::if_not_void_t<R, R> operator()(R (Interface::*_fptr)(Args...),
                                      Args... _args) {
    return collect(collector::Last<R>(), _fptr, forward<Args>(_args)...);
  }

  template<typename R, typename... Args>
  mpl::if_not_void_t<R, R> invoke(R (Interface::*_fptr)(Args...),
                                  Args... _args) {
    return collect(collector::Last<R>(), _fptr, forward<Args>(_args)...);
  }

  template<typename Collector, typename R, typename... Args>
  mpl::if_not_void_t<R, R> collect(Collector && _collector,
                                   R (Interface::*_fptr)(Args...),
                                   Args... _args) {
    for (SlotType & slot : slot_list) {
      auto * instance = static_cast<Interface *>(&slot);
      _collector((instance->*_fptr)(forward<Args>(_args)...));
    }
    return _collector.result();
  }

private:
  intrusive_list<SlotType> slot_list;
};

/*******************************************************************************
** Signal (Signature)
*******************************************************************************/

template<typename R, typename... Args>
class Signal<R(Args...)> {
public:
  using DelegateType = Delegate<R(Args...)>;
  using SlotType = Slot<R(Args...)>;

  template<typename F>
  void connect(F && _f) {
    SlotType & slot = slots.Create(DelegateType::Bind(forward<F>(_f)));
    slot_list.push_front(slot);
  }

  void connect(DelegateType && _delegate) {
    SlotType & slot = slots.Create(forward<DelegateType>(_delegate));
    slot_list.push_front(slot);
  }

  void connect(SlotType & _slot) { slot_list.push_front(_slot); }

  void disconnect(SlotType & _slot) { slot_list.push_front(_slot); }

  template<typename U = R>
  mpl::if_void_t<U, U> operator()(Args... _args) {
    invoke(forward<Args>(_args)...);
  }

  template<typename U = R>
  mpl::if_void_t<U, U> invoke(Args... _args) {
    for (SlotType & slot : slot_list) {
      slot.delegate(forward<Args>(_args)...);
    }
  }

  template<typename U = R>
  mpl::if_not_void_t<U, U> operator()(Args... _args) {
    return collect(collector::Last<R>(), forward<Args>(_args)...);
  }

  template<typename U = R>
  mpl::if_not_void_t<U, U> invoke(Args... _args) {
    return collect(collector::Last<R>(), forward<Args>(_args)...);
  }

  template<typename U = R, typename Collector>
  mpl::if_not_void_t<U, U> collect(Collector && _collector, Args... _args) {
    for (SlotType & slot : slot_list) {
      _collector(slot.delegate(forward<Args>(_args)...));
    }
    return _collector.result();
  }

private:
  container::PoolArray<SlotType> slots;
  intrusive_list<SlotType> slot_list;
};

}

#endif
