#ifndef _CORE_LIB_SIGNAL_SLOT_HPP_
#define _CORE_LIB_SIGNAL_SLOT_HPP_

#include "core/callable/delegate.hpp"
#include "core/collections/intrusive_list.hpp"

////////////////////////////////////////////////////////////////////////////////
// core
////////////////////////////////////////////////////////////////////////////////

namespace core {

template<typename>
class Signal;

/*******************************************************************************
** Slot (Interface)
*******************************************************************************/

template<class Interface>
class Slot
    : public Interface
    , public intrusive_list_node {};

/*******************************************************************************
** Slot (Signature)
*******************************************************************************/

template<typename R, typename... Args>
class Slot<R(Args...)> : public intrusive_list_node {
public:
  friend Signal<R(Args...)>;

  using DelegateType = Delegate<R(Args...)>;

  Slot() = default;
  Slot(DelegateType && _delegate)
      : delegate(forward<DelegateType>(_delegate)) {}

private:
  Delegate<R(Args...)> delegate;
};

}

#endif
