#ifndef _CORE_LIB_CONTAINERS_LISTS_HPP_
#define _CORE_LIB_CONTAINERS_LISTS_HPP_

#ifndef _CORE_LIB_CONTAINERS_HPP_
#  error
#endif

////////////////////////////////////////////////////////////////////////////////
// container detail
////////////////////////////////////////////////////////////////////////////////

namespace container { namespace detail {

/*******************************************************************************
** SingleList_Node
*******************************************************************************/

template<typename T>
class SingleList_Node final {
public:
  T data;
  SingleList_Node<T> * next = nullptr;

  template<typename... Args>
  SingleList_Node(Args &&... _args);
  SingleList_Node(SingleList_Node &&) = delete;
  SingleList_Node(SingleList_Node const &) = delete;
  SingleList_Node & operator=(SingleList_Node &&) = delete;
  SingleList_Node & operator=(SingleList_Node const &) = delete;
  ~SingleList_Node() = delete;
};

/*******************************************************************************
** DoubleList_Node
*******************************************************************************/

template<typename T>
class DoubleList_Node final {
public:
  T data;
  DoubleList_Node<T> * prev = nullptr;
  DoubleList_Node<T> * next = nullptr;

  template<typename... Args>
  DoubleList_Node(Args &&... _args);
  DoubleList_Node(DoubleList_Node &&) = delete;
  DoubleList_Node(DoubleList_Node const &) = delete;
  DoubleList_Node & operator=(DoubleList_Node &&) = delete;
  DoubleList_Node & operator=(DoubleList_Node const &) = delete;
  ~DoubleList_Node() = default;
};

/*******************************************************************************
** ListBase
*******************************************************************************/

template<typename T, template<typename> class NodeType,
         template<typename> class TypeAllocator>
class ListBase {
public:
  bool IsEmpty() const;
  explicit operator bool() const;
  T & Head();
  T const & Head() const;
  T & Tail();
  T const & Tail() const;

protected:
  TypeAllocator<NodeType<T>> node_allocator;
  NodeType<T> * head = nullptr;
  NodeType<T> * tail = nullptr;

  ListBase() = default;
  ListBase(ListBase && _other);
  ListBase(ListBase const &) = delete;
  ListBase & operator=(ListBase && _other);
  ListBase & operator=(ListBase const &) = delete;
  ~ListBase() = default;
};

}}

////////////////////////////////////////////////////////////////////////////////
// container
////////////////////////////////////////////////////////////////////////////////

namespace container {

/*******************************************************************************
** SingleList
*******************************************************************************/

template<class T, template<class> class TypeAllocator>
class SingleList final
    : public detail::ListBase<T, detail::SingleList_Node, TypeAllocator> {
public:
  using NodeType = detail::SingleList_Node<T>;

  friend query::Query<SingleList<T, TypeAllocator>>;
  friend query::ConstQuery<SingleList<T, TypeAllocator>>;

  SingleList() = default;
  SingleList(SingleList &&) = default;
  SingleList(SingleList const &) = delete;
  SingleList & operator=(SingleList &&) = default;
  SingleList & operator=(SingleList const &) = delete;
  ~SingleList();

  template<typename... Args>
  T & LinkHead(Args &&... _args);
  template<typename... Args>
  T & LinkTail(Args &&... _args);
  void Unlink(T const & _value);
  void Clear();
};

/*******************************************************************************
** DoubleList
*******************************************************************************/

template<class T, template<class> class TypeAllocator>
class DoubleList final
    : public detail::ListBase<T, detail::DoubleList_Node, TypeAllocator> {
public:
  using NodeType = detail::DoubleList_Node<T>;

  friend query::Query<DoubleList<T, TypeAllocator>>;
  friend query::ConstQuery<DoubleList<T, TypeAllocator>>;

  DoubleList() = default;
  DoubleList(DoubleList &&) = default;
  DoubleList(DoubleList const &) = delete;
  DoubleList & operator=(DoubleList &&) = default;
  DoubleList & operator=(DoubleList const &) = delete;
  ~DoubleList();

  template<typename... Args>
  T & LinkHead(Args &&... _args);
  template<typename... Args>
  T & LinkTail(Args &&... _args);
  void UnlinkHead(T const & _value);
  void UnlinkTail(T const & _value);
  void Clear();

private:
  template<typename... Args>
  NodeType & LinkNodeToPrevious(NodeType & _previous, Args &&... _args);
  template<typename... Args>
  NodeType & LinkNodeToNext(NodeType & _next, Args &&... _args);
  void UnlinkNode(NodeType & _node);
};

}

////////////////////////////////////////////////////////////////////////////////
// container detail
////////////////////////////////////////////////////////////////////////////////

namespace container { namespace detail {

/*******************************************************************************
** SingleList_Node
*******************************************************************************/

template<typename T>
template<typename... Args>
inline SingleList_Node<T>::SingleList_Node(Args &&... _args)
    : data(forward<Args>(_args)...) {}

template<typename T>
template<typename... Args>
inline DoubleList_Node<T>::DoubleList_Node(Args &&... _args)
    : data(forward<Args>(_args)...) {}

/*******************************************************************************
** ListBase
*******************************************************************************/

#if 0
template< typename T
        , template< typename > class NodeType
        , template< typename > class TypeAllocator >
template< typename ... Args >
inline ListBase< T, NodeType, TypeAllocator >::ListBase( Args && ... _args )
    : node_allocator( forward< Args >( _args ) ... ) {}
#endif

template<typename T, template<typename> class NodeType,
         template<typename> class TypeAllocator>
inline ListBase<T, NodeType, TypeAllocator>::ListBase(ListBase && _other) {
  CORE_LIB_ASSERT(this != &_other);
  if (this != &_other) {
    new (&node_allocator)
        TypeAllocator<NodeType<T>>(move(_other.node_allocator));
    head = _other.head;
    tail = _other.tail;
    _other.tail = nullptr;
    _other.head = nullptr;
  }
}

template<typename T, template<typename> class NodeType,
         template<typename> class TypeAllocator>
inline ListBase<T, NodeType, TypeAllocator> &
ListBase<T, NodeType, TypeAllocator>::operator=(ListBase && _other) {
  if (this != &_other) {
    node_allocator = move(_other.node_allocator);
    head = _other.head;
    tail = _other.tail;
    _other.head = nullptr;
    _other.tail = nullptr;
  }
  return *this;
}

template<typename T, template<typename> class NodeType,
         template<typename> class TypeAllocator>
inline bool ListBase<T, NodeType, TypeAllocator>::IsEmpty() const {
  return head == nullptr;
}

template<typename T, template<typename> class NodeType,
         template<typename> class TypeAllocator>
inline ListBase<T, NodeType, TypeAllocator>::operator bool() const {
  return !IsEmpty();
}

template<typename T, template<typename> class NodeType,
         template<typename> class TypeAllocator>
inline T & ListBase<T, NodeType, TypeAllocator>::Head() {
  CORE_LIB_ASSERT(head != nullptr);
  return head->data;
}

template<typename T, template<typename> class NodeType,
         template<typename> class TypeAllocator>
inline T const & ListBase<T, NodeType, TypeAllocator>::Head() const {
  CORE_LIB_ASSERT(head != nullptr);
  return head->data;
}

template<typename T, template<typename> class NodeType,
         template<typename> class TypeAllocator>
inline T & ListBase<T, NodeType, TypeAllocator>::Tail() {
  CORE_LIB_ASSERT(tail != nullptr);
  return tail->data;
}

template<typename T, template<typename> class NodeType,
         template<typename> class TypeAllocator>
inline T const & ListBase<T, NodeType, TypeAllocator>::Tail() const {
  CORE_LIB_ASSERT(tail != nullptr);
  return tail->data;
}

}}

////////////////////////////////////////////////////////////////////////////////
// container
////////////////////////////////////////////////////////////////////////////////

namespace container {

/*******************************************************************************
** SingleList
*******************************************************************************/

template<typename T, template<typename> class TypeAllocator>
inline SingleList<T, TypeAllocator>::~SingleList() {
  Clear();
}

template<typename T, template<typename> class TypeAllocator>
template<typename... Args>
inline T & SingleList<T, TypeAllocator>::LinkHead(Args &&... _args) {
  auto & node = this->node_allocator.Create(forward<Args>(_args)...);
  if (this->head) {
    node.next = this->head;
    this->head = &node;
  } else {
    this->head = &node;
    this->tail = &node;
  }
  return node.data;
}

template<typename T, template<typename> class TypeAllocator>
template<typename... Args>
inline T & SingleList<T, TypeAllocator>::LinkTail(Args &&... _args) {
  auto & node = this->node_allocator.Create(forward<Args>(_args)...);
  if (this->tail) {
    this->tail->next = &node;
    this->tail = &node;
  } else {
    this->head = &node;
    this->tail = &node;
  }
  return node.data;
}

template<typename T, template<typename> class TypeAllocator>
inline void SingleList<T, TypeAllocator>::Unlink(T const & _value) {
  CORE_LIB_ASSERT(!this->IsEmpty());
  auto current = this->head;
  if (current->data == _value) {
    if (this->head == this->tail) {
      this->tail = nullptr;
    }
    this->head = this->head->next;
    this->node_allocator.Destroy(*current);
    return;
  }

  while (current->next) {
    if (current->next->data == _value) {
      if (current->next == this->tail) {
        this->tail = current;
      }
      auto node = current->next;
      current->next = node->next;
      this->node_allocator.Destroy(*node);
      return;
    }
  }

  CORE_LIB_ASSERT_MESSAGE("SingleList Unlink: Node not found!");
}

template<typename T, template<typename> class TypeAllocator>
inline void SingleList<T, TypeAllocator>::Clear() {
  auto current = this->head;
  auto next = current->next;
  while (current) {
    this->node_allocator.Destroy(*current);
    current = next;
    next = current->next;
  }
}

/*******************************************************************************
** DoubleList
*******************************************************************************/

template<typename T, template<typename> class TypeAllocator>
inline DoubleList<T, TypeAllocator>::~DoubleList() {
  Clear();
}

template<typename T, template<typename> class TypeAllocator>
template<typename... Args>
inline T & DoubleList<T, TypeAllocator>::LinkHead(Args &&... _args) {
  auto node = &this->node_allocator.Create(forward<Args>(_args)...);
  if (!this->IsEmpty()) {
    this->head->prev = node;
    node->next = this->head;
    this->head = node;
  } else {
    this->head = node;
    this->tail = node;
  }
  return node->data;
}

template<typename T, template<typename> class TypeAllocator>
template<typename... Args>
inline T & DoubleList<T, TypeAllocator>::LinkTail(Args &&... _args) {
  auto node = &this->node_allocator.Create(forward<Args>(_args)...);
  if (!this->IsEmpty()) {
    node->prev = this->tail;
    this->tail->next = node;
    this->tail = node;
  } else {
    this->head = node;
    this->tail = node;
  }
  return node->data;
}

template<typename T, template<typename> class TypeAllocator>
inline void DoubleList<T, TypeAllocator>::UnlinkHead(T const & _value) {
  CORE_LIB_ASSERT(!this->IsEmpty());
  auto current = this->head;
  if (this->head->data == _value) {
    if (this->head == this->tail) {
      this->tail = nullptr;
    }
    this->head = this->head->next;
    this->node_allocator.Destroy(*current);
    return;
  }

  while (current->next) {
    if (current->next->data == _value) {
      if (current->next == this->tail) {
        this->tail = current;
      }
      auto node = current->next;
      current->next = current->next->next;
      this->node_allocator.Destroy(*node);
      return;
    }
    current = current->next;
  }

  CORE_LIB_ASSERT(false);
}

template<typename T, template<typename> class TypeAllocator>
inline void DoubleList<T, TypeAllocator>::UnlinkTail(T const & _value) {
  // TODO: Implement me!
  UnlinkHead(_value);
}

template<typename T, template<typename> class TypeAllocator>
inline void DoubleList<T, TypeAllocator>::Clear() {
  auto current = this->head;
  while (current) {
    auto next = current->next;
    this->node_allocator.Destroy(*current);
    current = next;
  }
  this->head = nullptr;
  this->tail = nullptr;
}

template<typename T, template<typename> class TypeAllocator>
template<typename... Args>
inline detail::DoubleList_Node<T> &
DoubleList<T, TypeAllocator>::LinkNodeToPrevious(NodeType & _previous,
                                                 Args &&... _args) {
  auto node = &this->node_allocator.Create(forward<Args>(_args)...);
  node->prev = &_previous;
  node->next = _previous.next;
  if (_previous.next) {
    _previous.next->prev = node;
  }
  _previous.next = node;
  return *node;
}

template<typename T, template<typename> class TypeAllocator>
template<typename... Args>
inline detail::DoubleList_Node<T> &
DoubleList<T, TypeAllocator>::LinkNodeToNext(NodeType & _next,
                                             Args &&... _args) {
  auto node = &this->node_allocator.Create(forward<Args>(_args)...);
  node->prev = _next.prev;
  node->next = &_next;
  if (_next.prev) {
    _next.prev->next = node;
  }
  _next.prev = node;
  return *node;
}

template<typename T, template<typename> class TypeAllocator>
inline void DoubleList<T, TypeAllocator>::UnlinkNode(NodeType & _node) {
  if (&_node == this->head) {
    this->head = _node.next;
  } else {
    CORE_LIB_ASSERT(_node.prev);
    _node.prev->next = _node.next;
  }
  if (&_node == this->tail) {
    this->tail = _node.prev;
  } else {
    CORE_LIB_ASSERT(_node.next);
    _node.next->prev = _node.prev;
  }
  this->node_allocator.Destroy(_node);
}

}

#endif
