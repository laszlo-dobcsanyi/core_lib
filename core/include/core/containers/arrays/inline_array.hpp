#ifndef _CORE_LIB_CONTAINERS_ARRAYS_INLINE_ARRAY_HPP_
#define _CORE_LIB_CONTAINERS_ARRAYS_INLINE_ARRAY_HPP_

#ifndef CORE_LIB_CONTAINERS
#  error
#endif

#include "core/iterator/contiguous_iterator.hpp"

////////////////////////////////////////////////////////////////////////////////
// container
////////////////////////////////////////////////////////////////////////////////

namespace container {

/*******************************************************************************
** InlineArray
*******************************************************************************/

template<typename T, size_t Count>
class InlineArray final {
public:
  using value_type = T;
  using pointer_type = T *;
  using reference_type = T &;
  using const_reference_type = T const &;
  using iterator_type = core::contiguous_iterator<T>;
  using const_iterator_type = core::contiguous_iterator<T const>;

  InlineArray() = default;
  InlineArray(InlineArray &&) = default;
  InlineArray(InlineArray const &) = default;
  InlineArray & operator=(InlineArray &&) = default;
  InlineArray & operator=(InlineArray const &) = default;
  ~InlineArray() = default;

  size_t Size() const { return Count; }

  T & operator[](size_t _index);
  T const & operator[](size_t _index) const;

  iterator_type begin() { return iterator_type(this->data); }
  const_iterator_type begin() const { return const_iterator_type(this->data); }
  iterator_type end() { return iterator_type(this->data + Count); }
  const_iterator_type end() const {
    return const_iterator_type(this->data + Count);
  }

private:
  T data[Count];
};

/*******************************************************************************
** InlineArray
*******************************************************************************/

template<typename T, size_t Count>
inline T & InlineArray<T, Count>::operator[](size_t _index) {
  CORE_LIB_ASSERT(_index < Count);
  return this->data[_index];
}

template<typename T, size_t Count>
inline T const & InlineArray<T, Count>::operator[](size_t _index) const {
  CORE_LIB_ASSERT(_index < Count);
  return this->data[_index];
}

}

#endif
