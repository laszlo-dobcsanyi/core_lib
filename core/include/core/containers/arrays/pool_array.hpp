#ifndef _CORE_LIB_CONTAINERS_ARRAYS_POOL_ARRAY_HPP_
#define _CORE_LIB_CONTAINERS_ARRAYS_POOL_ARRAY_HPP_

#include "core/queries.hh"

#ifndef CORE_LIB_CONTAINERS
#  error
#endif

#include "core/memory/uninitialized_array.hpp"

////////////////////////////////////////////////////////////////////////////////
// container detail
////////////////////////////////////////////////////////////////////////////////

namespace container { namespace detail {

/*******************************************************************************
** Pool
*******************************************************************************/

template<class T, size_t PoolSize>
struct Pool final {
  BitSet<PoolSize, size_t> reservation_field;
  core::memory::UninitializedArray<T, PoolSize> data;
  Pool * next_pool = nullptr;
};

}}

////////////////////////////////////////////////////////////////////////////////
// container
////////////////////////////////////////////////////////////////////////////////

namespace container {

/*******************************************************************************
** PoolArray
*******************************************************************************/

template<class T, template<class> class TypeAllocator, size_t PoolSize>
class PoolArray final {
public:
  template<typename U>
  class Iterator {
  public:
    explicit Iterator(nullptr_t)
        : pool(nullptr)
        , index(0u) {}
    explicit Iterator(detail::Pool<T, PoolSize> * _pool)
        : pool(_pool)
        , index(0u) {
      if (pool) {
        if (!pool->reservation_field.IsAllUnset()) {
          index = pool->reservation_field.GetFirstSetIndex();
        } else {
          pool = nullptr;
          index = 0u;
        }
      }
    }

    U & operator*() {
      CORE_LIB_ASSERT(pool);
      return pool->data[index];
    }
    Iterator & operator++() {
      CORE_LIB_ASSERT(pool);
      ++index;
      while (pool) {
        while (index < PoolSize) {
          if (pool->reservation_field.IsSet(index)) {
            return *this;
          }
          ++index;
        }
        index = 0u;
        pool = pool->next_pool;
      }
      return *this;
    }
    bool operator==(Iterator const & _other) {
      return pool == _other.pool && index == _other.index;
    }
    bool operator!=(Iterator const & _other) { return !(*this == _other); }

  private:
    detail::Pool<T, PoolSize> * pool;
    size_t index;
  };

  using value_type = T;
  using pointer_type = value_type *;
  using reference_type = value_type &;
  using const_reference_type = T const &;
  using iterator_type = Iterator<T>;
  using const_iterator_type = Iterator<T const>;

  template<typename... Args>
  PoolArray(Args &&... _args);
  PoolArray(PoolArray && _other);
  PoolArray(PoolArray const &) = delete;
  PoolArray & operator=(PoolArray && _other);
  PoolArray & operator=(PoolArray const &) = delete;
  ~PoolArray();

  size_t Size() const { return size; }
  bool IsEmpty() const { return size == 0u; }

  T * Get(size_t _index) { return At(_index); }
  T const * Get(size_t _index) const { return At(_index); }

  T & operator[](size_t _index);
  T const & operator[](size_t _index) const;

  T & Allocate();
  void Deallocate(T & _element);

  template<typename... Args>
  T & Create(Args &&... _args);
  void Destroy(T & _element);

  void Clear();

  iterator_type begin() { return iterator_type(head); }
  const_iterator_type begin() const { return const_iterator_type(head); }
  iterator_type end() { return iterator_type(nullptr); }
  const_iterator_type end() const { return const_iterator_type(nullptr); }

private:
  TypeAllocator<detail::Pool<T, PoolSize>> pool_allocator;
  detail::Pool<T, PoolSize> * head = nullptr;
  size_t size = 0;

  T * At(size_t _index) const;
};

}

////////////////////////////////////////////////////////////////////////////////
// container
////////////////////////////////////////////////////////////////////////////////

namespace container {

/*******************************************************************************
** PoolArray
*******************************************************************************/

template<typename T, template<class> class TypeAllocator, size_t PoolSize>
template<typename... Args>
inline PoolArray<T, TypeAllocator, PoolSize>::PoolArray(Args &&... _args)
    : pool_allocator(forward<Args>(_args)...) {}

template<typename T, template<class> class TypeAllocator, size_t PoolSize>
inline PoolArray<T, TypeAllocator, PoolSize>::PoolArray(PoolArray && _other)
    : pool_allocator(move(_other)) {
  if (this != &_other) {
    head = _other.head;
    _other.head = nullptr;
  }
}

template<typename T, template<class> class TypeAllocator, size_t PoolSize>
inline PoolArray<T, TypeAllocator, PoolSize> &
PoolArray<T, TypeAllocator, PoolSize>::operator=(PoolArray && _other) {
  pool_allocator = move(_other);
  if (this != &_other) {
    if (head) {
      Clear();
    }

    head = _other.head;
    _other.head = nullptr;
  }
  return *this;
}

template<typename T, template<class> class TypeAllocator, size_t PoolSize>
inline PoolArray<T, TypeAllocator, PoolSize>::~PoolArray() {
  Clear();
}

template<typename T, template<class> class TypeAllocator, size_t PoolSize>
inline T & PoolArray<T, TypeAllocator, PoolSize>::operator[](size_t _index) {
  auto element = Get(_index);
  CORE_LIB_ASSERT(element);
  return *element;
}

template<typename T, template<class> class TypeAllocator, size_t PoolSize>
inline T const &
PoolArray<T, TypeAllocator, PoolSize>::operator[](size_t _index) const {
  auto element = Get(_index);
  CORE_LIB_ASSERT(element);
  return *element;
}

template<typename T, template<class> class TypeAllocator, size_t PoolSize>
inline T & PoolArray<T, TypeAllocator, PoolSize>::Allocate() {
  // If we don't have any allocated pools, allocate the first!
  if (head == nullptr) {
    head = &pool_allocator.Create();
  }
  auto pool = head;
  auto last_pool = head;
  while (pool != nullptr) {
    if (!pool->reservation_field.IsAllSet()) {
      // Current pool has an empty slot!
      auto index = pool->reservation_field.GetFirstUnsetIndex();
      pool->reservation_field.Set(index);
      ++size;
      return pool->data[index];
    } else {
      // Current pool is full, continue!
      last_pool = pool;
      pool = pool->next_pool;
    }
  }
  // All pools are full, so create a new!
  auto new_pool = &pool_allocator.Create();
  last_pool->next_pool = new_pool;
  new_pool->reservation_field.Set(0u);
  ++size;
  return new_pool->data[0u];
}

template<typename T, template<class> class TypeAllocator, size_t PoolSize>
inline void PoolArray<T, TypeAllocator, PoolSize>::Deallocate(T & _element) {
  auto pool = head;
  while (pool) {
    if (pool->data.Contains(_element)) {
      auto const index = pool->data.IndexOf(_element);
      pool->reservation_field.UnSet(index);
      --size;
      return;
    } else {
      pool = pool->next_pool;
    }
  }
  CORE_LIB_ASSERT(false);
}

template<typename T, template<class> class TypeAllocator, size_t PoolSize>
template<typename... Args>
inline T & PoolArray<T, TypeAllocator, PoolSize>::Create(Args &&... _args) {
  auto & allocated = Allocate();
  new (&allocated) T(forward<Args>(_args)...);
  return allocated;
}

template<typename T, template<class> class TypeAllocator, size_t PoolSize>
inline void PoolArray<T, TypeAllocator, PoolSize>::Destroy(T & _element) {
  _element.~T();
  Deallocate(_element);
}

template<typename T, template<class> class TypeAllocator, size_t PoolSize>
inline void PoolArray<T, TypeAllocator, PoolSize>::Clear() {
  while (head) {
    for (auto index = 0u; index < PoolSize; ++index) {
      if (head->reservation_field.IsSet(index)) {
        head->data[index].~T();
      }
    }
    auto next = head->next_pool;
    pool_allocator.Destroy(*head);
    head = next;
  }
  size = 0u;
}

template<typename T, template<class> class TypeAllocator, size_t PoolSize>
inline T * PoolArray<T, TypeAllocator, PoolSize>::At(size_t _index) const {
  auto pool = head;
  auto current = 0u;
  while (pool) {
    if (_index < current + PoolSize) {
      _index = _index % PoolSize;
      if (pool->reservation_field.IsSet(_index)) {
        return &(pool->data[_index]);
      } else {
        return nullptr;
      }
    }
    current += PoolSize;
    pool = pool->next_pool;
  }
  return nullptr;
}

}

#endif
