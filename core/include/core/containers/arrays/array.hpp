#ifndef _CORE_LIB_CONTAINERS_ARRAYS_ARRAY_HPP_
#define _CORE_LIB_CONTAINERS_ARRAYS_ARRAY_HPP_

#ifndef CORE_LIB_CONTAINERS
#  error
#endif

#include "core/memory/base.hpp"
#include "core/iterator/contiguous_iterator.hpp"

////////////////////////////////////////////////////////////////////////////////
// container
////////////////////////////////////////////////////////////////////////////////

namespace container {

/*******************************************************************************
** Array
*******************************************************************************/

template<class T, class AllocationPolicy, template<class> class TypeAllocator>
class Array final {
public:
  using value_type = T;
  using pointer_type = T *;
  using const_pointer_type = T const *;
  using reference_type = T &;
  using const_reference_type = T const &;
  using iterator_type = core::contiguous_iterator<T>;
  using const_iterator_type = core::contiguous_iterator<T const>;

  template<typename... Args>
  Array(Args &&... _args);
  Array(Array && _other);
  Array(Array const & _other);
  Array & operator=(Array && _other);
  Array & operator=(Array const & _other);
  ~Array();

  explicit operator bool() const { return data != nullptr; }
  size_t Size() const { return size; }
  size_t Capacity() const { return capacity; }
  bool IsEmpty() const { return size == 0u; }

  T & operator[](size_t _index);
  T const & operator[](size_t _index) const;

  template<typename... Args>
  T & Create(Args &&... _args);
  bool Destroy(T const & _element);
  void DestroyAt(size_t _index);

  void Clear();
  void Reset();

  iterator_type begin() { return iterator_type(data); }
  const_iterator_type begin() const { return const_iterator_type(data); }
  iterator_type end() { return iterator_type(data + size); }
  const_iterator_type end() const { return const_iterator_type(data + size); }

private:
  size_t size = 0;
  size_t capacity = 0;
  T * data = nullptr;
  TypeAllocator<T> type_allocator;
};

}

////////////////////////////////////////////////////////////////////////////////
// container
////////////////////////////////////////////////////////////////////////////////

namespace container {

/*******************************************************************************
** Array
*******************************************************************************/

template<class T, class AllocationPolicy, template<class> class TypeAllocator>
template<typename... Args>
inline Array<T, AllocationPolicy, TypeAllocator>::Array(Args &&... _args)
    : type_allocator(forward<Args>(_args)...) {}

template<class T, class AllocationPolicy, template<class> class TypeAllocator>
inline Array<T, AllocationPolicy, TypeAllocator>::Array(Array && _other) {
  CORE_LIB_ASSERT(this != &_other);
  type_allocator = move(_other.type_allocator);
  size = _other.size;
  capacity = _other.capacity;
  data = _other.data;

  _other.size = 0;
  _other.capacity = 0;
  _other.data = nullptr;
}

template<class T, class AllocationPolicy, template<class> class TypeAllocator>
inline Array<T, AllocationPolicy, TypeAllocator>::Array(Array const & _other) {
  CORE_LIB_ASSERT(this != &_other);
  size = _other.size;
  capacity = _other.capacity;
  type_allocator = _other.type_allocator;
  data = type_allocator.AllocateA(capacity);
  core::memory::copy_construct(data, _other.data, size);
}

template<class T, class AllocationPolicy, template<class> class TypeAllocator>
inline Array<T, AllocationPolicy, TypeAllocator> &
Array<T, AllocationPolicy, TypeAllocator>::operator=(Array && _other) {
  CORE_LIB_ASSERT(this != &_other);
  if (!IsEmpty()) {
    for (T & element : *this) {
      element.~T();
    }
    type_allocator.DeallocateA(data, capacity);
  }

  type_allocator = move(_other.type_allocator);
  size = _other.size;
  capacity = _other.capacity;
  data = _other.data;

  _other.size = 0;
  _other.capacity = 0;
  _other.data = nullptr;
  return *this;
}

template<class T, class AllocationPolicy, template<class> class TypeAllocator>
inline Array<T, AllocationPolicy, TypeAllocator> &
Array<T, AllocationPolicy, TypeAllocator>::operator=(Array const & _other) {
  if (this != &_other) {
    if (!IsEmpty()) {
      for (T & element : *this) {
        element.~T();
      }
      type_allocator.DeallocateA(data, capacity);
    }
    size = _other.size;
    capacity = _other.capacity;
    type_allocator = _other.type_allocator;
    data = type_allocator.AllocateA(capacity);
    core::memory::copy_construct(data, _other.data, size);
  }
  return *this;
}

template<class T, class AllocationPolicy, template<class> class TypeAllocator>
inline Array<T, AllocationPolicy, TypeAllocator>::~Array() {
  Reset();
}

template<class T, class AllocationPolicy, template<class> class TypeAllocator>
inline T &
Array<T, AllocationPolicy, TypeAllocator>::operator[](size_t _index) {
  CORE_LIB_ASSERT(_index < size);
  return data[_index];
}

template<class T, class AllocationPolicy, template<class> class TypeAllocator>
inline T const &
Array<T, AllocationPolicy, TypeAllocator>::operator[](size_t _index) const {
  CORE_LIB_ASSERT(_index < size);
  return data[_index];
}

template<class T, class AllocationPolicy, template<class> class TypeAllocator>
template<typename... Args>
inline T & Array<T, AllocationPolicy, TypeAllocator>::Create(Args &&... _args) {
  if (capacity == size) {
    if (data) {
      size_t new_capacity = AllocationPolicy::Calculate(capacity);
      T * expanded = type_allocator.AllocateA(new_capacity);
      core::memory::move_construct(expanded, data, capacity);
      for (T & element : *this) {
        element.~T();
      }
      type_allocator.DeallocateA(data, capacity);
      data = expanded;
      capacity = new_capacity;
    } else {
      capacity = AllocationPolicy::Calculate(capacity);
      data = type_allocator.AllocateA(capacity);
    }
    CORE_LIB_ASSERT(capacity != size);
  }

  T * element = data + size;
  new (element) T(forward<Args>(_args)...);
  size++;
  return *element;
}

template<class T, class AllocationPolicy, template<class> class TypeAllocator>
inline bool
Array<T, AllocationPolicy, TypeAllocator>::Destroy(T const & _element) {
  for (size_t index = 0u; index < size; ++index) {
    if (*(data + index) == _element) {
      DestroyAt(index);
      return true;
    }
  }
  return false;
}

template<class T, class AllocationPolicy, template<class> class TypeAllocator>
inline void
Array<T, AllocationPolicy, TypeAllocator>::DestroyAt(size_t _index) {
  CORE_LIB_ASSERT(_index < size);
  T * element = data + _index;
  element->~T();
  T * last = data + size;
  if (element != last)
    new (element) T(move(*last));
  last->~T();
  --size;
}

template<class T, class AllocationPolicy, template<class> class TypeAllocator>
inline void Array<T, AllocationPolicy, TypeAllocator>::Clear() {
  for (T & element : *this) {
    element.~T();
  }
  size = 0;
}

template<class T, class AllocationPolicy, template<class> class TypeAllocator>
inline void Array<T, AllocationPolicy, TypeAllocator>::Reset() {
  Clear();
  type_allocator.DeallocateA(data, capacity);
  capacity = 0;
  data = nullptr;
}

}

#endif
