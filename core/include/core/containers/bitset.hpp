#ifndef _CORE_LIB_BITSET_HPP_
#define _CORE_LIB_BITSET_HPP_

#ifndef CORE_LIB_CONTAINERS
#  error
#endif

////////////////////////////////////////////////////////////////////////////////
// container
////////////////////////////////////////////////////////////////////////////////

namespace container {

/*******************************************************************************
** BitSet ( Generic )
*******************************************************************************/

template<size_t Size, typename StorageType>
class BitSet final {
public:
  BitSet() { Clear(); }
  BitSet(BitSet &&) = default;
  BitSet(BitSet const &) = default;
  BitSet & operator=(BitSet &&) = default;
  BitSet & operator=(BitSet const &) = default;
  ~BitSet() = default;

  bool IsSet(size_t _index) const {
    CORE_LIB_ASSERT(_index < Size);
    const auto array_index = ArrayIndex(_index);
    const auto bit_index = BitIndex(_index);
    return (storage[array_index] & ((StorageType)1 << bit_index)) != 0;
  }

  bool IsAllSet() const {
    for (auto current = 0;
         current < BitArraySize<Size, sizeof(StorageType)>::value; ++current) {
      if (storage[current] != mpl::full_bit_field<sizeof(StorageType)>::value)
        return false;
    }
    return true;
  }

  bool IsAllUnset() const {
    for (auto current = 0;
         current < BitArraySize<Size, sizeof(StorageType)>::value; ++current) {
      if (storage[current] != (StorageType)0)
        return false;
    }
    return true;
  }

  void Set(size_t _index) {
    CORE_LIB_ASSERT(_index < Size);
    const auto array_index = ArrayIndex(_index);
    const auto bit_index = BitIndex(_index);
    storage[array_index] |= ((StorageType)1 << bit_index);
  }

  void UnSet(size_t _index) {
    CORE_LIB_ASSERT(_index < Size);
    const auto array_index = ArrayIndex(_index);
    const auto bit_index = BitIndex(_index);
    storage[array_index] &= ~((StorageType)1 << bit_index);
  }

  void Clear() { ::memset(storage, U8(0), sizeof(storage)); }

private:
  template<size_t FieldSize, size_t IntSize>
  struct BitArraySize {
    static const size_t value = (FieldSize - 1) / (IntSize << 3) + 1;
  };

  CORE_LIB_STATIC_ASSERT(BitArraySize<Size, sizeof(StorageType)>::value == 1);
  StorageType storage[BitArraySize<Size, sizeof(StorageType)>::value];

  size_t ArrayIndex(size_t _index) const {
    // CORE_LIB_ASSERT( BitArraySize< Size, sizeof( StorageType ) >::value < array_index );
    return _index >> mpl::base_of_2<sizeof(StorageType)>::value;
  }

  size_t BitIndex(size_t _index) const {
    CORE_LIB_ASSERT(_index < sizeof(StorageType));
    return _index & mpl::full_bit_field<
                        mpl::base_of_2<sizeof(StorageType)>::value>::value;
  }
};

/*******************************************************************************
** BitSet ( uint32 )
*******************************************************************************/

template<>
class BitSet<mpl::bit_count<uint32>::value, uint32> final {
public:
  static const auto FullBits =
      mpl::full_bit_field<mpl::bit_count<uint32>::value>::value;

  BitSet() = default;
  BitSet(BitSet &&) = default;
  BitSet(BitSet const &) = default;
  BitSet & operator=(BitSet &&) = default;
  BitSet & operator=(BitSet const &) = default;
  ~BitSet() = default;

  bool IsSet(uint32 _index) const {
    CORE_LIB_ASSERT(_index < mpl::bit_count<uint32>::value);
    return (storage & (U32(1) << _index)) != U32(0);
  }
  bool IsAllSet() const { return storage == FullBits; }
  bool IsAllUnset() const { return storage == U32(0); }
  void Set(uint32 _index) {
    CORE_LIB_ASSERT(_index < mpl::bit_count<uint32>::value);
    storage |= (U32(1) << _index);
  }
  void UnSet(uint32 _index) {
    CORE_LIB_ASSERT(_index < mpl::bit_count<uint32>::value);
    storage &= ~(U32(1) << _index);
  }
  uint32 GetFirstSetIndex() const {
    CORE_LIB_ASSERT(!IsAllUnset());
    unsigned long index;
#if defined(CORE_LIB_OS_WINDOWS)
    bool result = _BitScanForward(&index, storage);
    CORE_LIB_ASSERT(result);
    return index;
#elif defined(CORE_LIB_OS_LINUX)
    index = ffsl(storage);
    CORE_LIB_ASSERT(0 < index);
    return static_cast<uint32>(index - 1);
#endif
  }
  uint32 GetFirstUnsetIndex() const {
    CORE_LIB_ASSERT(!IsAllSet());
    unsigned long inverted = storage ^ FullBits;
    unsigned long index;
#if defined(CORE_LIB_OS_WINDOWS)
    bool result = _BitScanForward(&index, inverted);
    CORE_LIB_ASSERT(result);
    return index;
#elif defined(CORE_LIB_OS_LINUX)
    index = ffsl(inverted);
    CORE_LIB_ASSERT(0 < index);
    return static_cast<uint32>(index - 1);
#endif
  }

  void Clear() { storage = U32(0); }

private:
  uint32 storage = U32(0);
};

#if defined(CORE_LIB_ARCH_64)

/*******************************************************************************
** BitSet ( uint64 )
*******************************************************************************/

template<>
class BitSet<mpl::bit_count<uint64>::value, uint64> final {
public:
  static const auto FullBits =
      mpl::full_bit_field<mpl::bit_count<uint64>::value>::value;

  BitSet() = default;
  BitSet(BitSet &&) = default;
  BitSet(BitSet const &) = default;
  BitSet & operator=(BitSet &&) = default;
  BitSet & operator=(BitSet const &) = default;
  ~BitSet() = default;

  bool IsSet(uint64 _index) const {
    CORE_LIB_ASSERT(_index < mpl::bit_count<uint64>::value);
    return (storage & (U64(1) << _index)) != U64(0);
  }
  bool IsAllSet() const { return storage == FullBits; }
  bool IsAllUnset() const { return storage == U64(0); }
  void Set(uint64 _index) {
    CORE_LIB_ASSERT(_index < mpl::bit_count<uint64>::value);
    storage |= (U64(1) << _index);
  }
  void UnSet(uint64 _index) {
    CORE_LIB_ASSERT(_index < mpl::bit_count<uint64>::value);
    storage &= ~(U64(1) << _index);
  }
  uint64 GetFirstSetIndex() const {
    CORE_LIB_ASSERT(!IsAllUnset());
#  if defined(CORE_LIB_OS_WINDOWS)
    DWORD index;
    bool result = _BitScanForward64(&index, storage);
    CORE_LIB_ASSERT(result);
    return index;
#  else
    size_t index = ffsll(storage);
    CORE_LIB_ASSERT(index);
    return index - 1;
#  endif
  }
  uint64 GetFirstUnsetIndex() const {
    CORE_LIB_ASSERT(!IsAllSet());
    auto inverted = storage ^ FullBits;
#  if defined(CORE_LIB_OS_WINDOWS)
    DWORD index;
    bool result = _BitScanForward64(&index, inverted);
    CORE_LIB_ASSERT(result);
    return index;
#  else
    size_t index = ffsll(inverted);
    CORE_LIB_ASSERT(index);
    return index - 1;
#  endif
  }

  void Clear() { storage = U64(0); }

private:
  uint64 storage = U64(0);
};

#endif

}

#endif
