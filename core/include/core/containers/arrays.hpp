#ifndef _CORE_LIB_CONTAINERS_ARRAYS_HPP_
#define _CORE_LIB_CONTAINERS_ARRAYS_HPP_

#ifndef CORE_LIB_CONTAINERS
#  error
#endif

#include "core/containers/arrays/array.hpp"
#include "core/containers/arrays/inline_array.hpp"
#include "core/containers/arrays/pool_array.hpp"

#endif
