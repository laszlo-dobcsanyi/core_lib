#ifndef _CORE_LIB_CONTAINERS_STACKS_HPP_
#define _CORE_LIB_CONTAINERS_STACKS_HPP_

#ifndef _CORE_LIB_CONTAINERS_HPP_
#  error
#endif

#include "core/memory/uninitialized_array.hpp"
#include "core/memory/unique_array.hpp"

////////////////////////////////////////////////////////////////////////////////
// container
////////////////////////////////////////////////////////////////////////////////

namespace container {

/*******************************************************************************
** to_unique_array
*******************************************************************************/

template<typename T, template<typename> class TypeAllocator>
inline core::UniqueArray<T>
to_unique_array(UnboundedStack<T, TypeAllocator> && _stack) {
  auto result = core::make_unique_array<T>(_stack.Size());
  auto stack_list_query = make_query(_stack.stack_list);
  T * target = result.Get();
  while (stack_list_query.IsValid()) {
    auto & bounded_stack = stack_list_query.Current();
    for (T & source : bounded_stack) {
      (*target++) = move(source);
    }
    bounded_stack.size = 0u;
    stack_list_query.UnlinkForward();
  }
  return result;
}

template<typename T, template<typename> class TypeAllocator>
inline core::UniqueArray<T>
to_unique_array(UnboundedStack<T, TypeAllocator> const & _stack) {
  auto result = core::make_unique_array<T>(_stack.Size());
  auto stack_list_query = make_query(_stack.stack_list);
  T * target = result.Get();
  while (stack_list_query.IsValid()) {
    auto & bounded_stack = stack_list_query.Current();
    for (T const & source : bounded_stack) {
      (*target++) = source;
    }
    stack_list_query.Forward();
  }
  return result;
}

/*******************************************************************************
** BoundedStack
*******************************************************************************/

template<typename T, size_t Bound>
class BoundedStack final {
public:
  using value_type = T;
  using pointer_type = mpl::add_pointer_t<value_type>;
  using const_pointer_type = mpl::add_pointer_t<value_type const>;
  using reference_type = mpl::add_reference_t<value_type>;
  using const_reference_type = mpl::add_reference_t<value_type const>;
  using iterator_type = core::contiguous_iterator<value_type>;
  using const_iterator_type = core::contiguous_iterator<value_type const>;

  friend query::Query<BoundedStack<T, Bound>>;
  friend query::ConstQuery<BoundedStack<T, Bound>>;

  template<typename U, template<typename> class UA>
  friend core::UniqueArray<U> to_unique_array(UnboundedStack<U, UA> &&);
  template<typename U, template<typename> class UA>
  friend core::UniqueArray<U> to_unique_array(UnboundedStack<U, UA> const &);

  BoundedStack() = default;
  BoundedStack(BoundedStack && _other);
  BoundedStack(BoundedStack const & _other);
  BoundedStack & operator=(BoundedStack && _other);
  BoundedStack & operator=(BoundedStack const & _other);
  ~BoundedStack();

  size_t Size() const;
  bool IsFull() const;
  bool IsEmpty() const;
  template<typename... Args>
  T & Push(Args &&... _args);
  T & Last();
  T const & Last() const;
  void Pop();
  void Clear();

  iterator_type begin();
  const_iterator_type begin() const;
  iterator_type end();
  const_iterator_type end() const;

private:
  core::memory::UninitializedArray<T, Bound> data;
  size_t size = 0u;
};

/*******************************************************************************
** UnboundedStack
*******************************************************************************/

template<typename T, template<typename> class TypeAllocator>
class UnboundedStack final {
public:
  friend query::Query<UnboundedStack<T, TypeAllocator>>;
  friend query::ConstQuery<UnboundedStack<T, TypeAllocator>>;

  template<typename U, template<typename> class UA>
  friend core::UniqueArray<U> to_unique_array(UnboundedStack<U, UA> &&);
  template<typename U, template<typename> class UA>
  friend core::UniqueArray<U> to_unique_array(UnboundedStack<U, UA> const &);

  static const size_t Bound = 32u;

  UnboundedStack() = default;
  UnboundedStack(UnboundedStack && _other) = default;
  UnboundedStack(UnboundedStack const & _other) = default;
  UnboundedStack & operator=(UnboundedStack && _other) = default;
  UnboundedStack & operator=(UnboundedStack const & _other) = default;
  ~UnboundedStack() = default;

  size_t Size() const;
  bool IsEmpty() const;
  template<typename... Args>
  T & Push(Args &&... _args);
  T & Last();
  T const & Last() const;
  void Pop();
  void Clear();

private:
  using StackType = BoundedStack<T, Bound>;
  using StackListType = DoubleList<StackType, TypeAllocator>;
  StackListType stack_list;
  size_t size = 0u;
};

}

////////////////////////////////////////////////////////////////////////////////
// container
////////////////////////////////////////////////////////////////////////////////

namespace container {

/*******************************************************************************
** BoundedStack
*******************************************************************************/

template<typename T, size_t Bound>
inline BoundedStack<T, Bound>::BoundedStack(BoundedStack && _other) {
  CORE_LIB_ASSERT(this != &_other);
  size = _other.size;
  for (auto current = 0u; current < size; ++current) {
    new (data.Get(current)) T(move(_other.data[current]));
  }
  _other.size = 0u;
}

template<typename T, size_t Bound>
inline BoundedStack<T, Bound>::BoundedStack(BoundedStack const & _other) {
  CORE_LIB_ASSERT(this != &_other);
  size = _other.size;
  for (auto current = 0u; current < size; ++current) {
    new (data.Get(current)) T(_other.data[current]);
  }
}

template<typename T, size_t Bound>
inline BoundedStack<T, Bound> &
BoundedStack<T, Bound>::operator=(BoundedStack && _other) {
  if (this != &_other) {
    size = _other.size;
    for (auto current = 0u; current < size; ++current) {
      new (data.Get(current)) T(move(_other.data[current]));
    }
    _other.size = 0u;
  }
  return *this;
}

template<typename T, size_t Bound>
inline BoundedStack<T, Bound> &
BoundedStack<T, Bound>::operator=(BoundedStack const & _other) {
  if (this != &_other) {
    size = _other.size;
    for (auto current = 0u; current < size; ++current) {
      new (data.Get(current)) T(_other.data[current]);
    }
  }
  return *this;
}

template<typename T, size_t Bound>
inline BoundedStack<T, Bound>::~BoundedStack() {
  for (auto current = 0u; current < size; ++current) {
    data[current].~T();
  }
}

template<typename T, size_t Bound>
inline size_t BoundedStack<T, Bound>::Size() const {
  return size;
}

template<typename T, size_t Bound>
inline bool BoundedStack<T, Bound>::IsFull() const {
  return size == Bound;
}

template<typename T, size_t Bound>
inline bool BoundedStack<T, Bound>::IsEmpty() const {
  return size == 0u;
}

template<typename T, size_t Bound>
template<typename... Args>
inline T & BoundedStack<T, Bound>::Push(Args &&... _args) {
  CORE_LIB_ASSERT(size < Bound);
  new (data.Get(size)) T(forward<Args>(_args)...);
  return data[size++];
}

template<typename T, size_t Bound>
inline T & BoundedStack<T, Bound>::Last() {
  CORE_LIB_ASSERT(0 < size);
  return data[size - 1];
}

template<typename T, size_t Bound>
inline T const & BoundedStack<T, Bound>::Last() const {
  CORE_LIB_ASSERT(0 < size);
  return data[size - 1];
}

template<typename T, size_t Bound>
inline void BoundedStack<T, Bound>::Pop() {
  CORE_LIB_ASSERT(0 < size);
  --size;
  data[size].~T();
}

template<typename T, size_t Bound>
inline void BoundedStack<T, Bound>::Clear() {
  for (T & _value : *this) {
    _value.~T();
  }
  size = 0u;
}

template<typename T, size_t Bound>
inline typename BoundedStack<T, Bound>::iterator_type
BoundedStack<T, Bound>::begin() {
  return data.begin();
}

template<typename T, size_t Bound>
inline typename BoundedStack<T, Bound>::const_iterator_type
BoundedStack<T, Bound>::begin() const {
  return data.begin();
}

template<typename T, size_t Bound>
inline typename BoundedStack<T, Bound>::iterator_type
BoundedStack<T, Bound>::end() {
  return iterator_type(data.begin() + size);
}

template<typename T, size_t Bound>
inline typename BoundedStack<T, Bound>::const_iterator_type
BoundedStack<T, Bound>::end() const {
  return const_iterator_type(data.begin() + size);
}

/*******************************************************************************
** UnboundedStack
*******************************************************************************/

template<typename T, template<typename> class TypeAllocator>
inline size_t UnboundedStack<T, TypeAllocator>::Size() const {
  return size;
}

template<typename T, template<typename> class TypeAllocator>
inline bool UnboundedStack<T, TypeAllocator>::IsEmpty() const {
  return size == 0u;
}

template<typename T, template<typename> class TypeAllocator>
template<typename... Args>
inline T & UnboundedStack<T, TypeAllocator>::Push(Args &&... _args) {
  if (stack_list.IsEmpty() || stack_list.Tail().IsFull()) {
    stack_list.LinkTail();
  }
  ++size;
  return stack_list.Tail().Push(forward<Args>(_args)...);
}

template<typename T, template<typename> class TypeAllocator>
inline T & UnboundedStack<T, TypeAllocator>::Last() {
  CORE_LIB_ASSERT(!stack_list.IsEmpty());
  return stack_list.Tail().Last();
}

template<typename T, template<typename> class TypeAllocator>
inline T const & UnboundedStack<T, TypeAllocator>::Last() const {
  CORE_LIB_ASSERT(!stack_list.IsEmpty());
  return stack_list.Tail().Last();
}

template<typename T, template<typename> class TypeAllocator>
inline void UnboundedStack<T, TypeAllocator>::Pop() {
  CORE_LIB_ASSERT(!stack_list.IsEmpty());
  stack_list.Tail().Pop();
  --size;
  if (stack_list.Tail().IsEmpty()) {
    stack_list.UnlinkTail();
  }
}

template<typename T, template<typename> class TypeAllocator>
inline void UnboundedStack<T, TypeAllocator>::Clear() {
  stack_list.Clear();
  size = 0u;
}

}

////////////////////////////////////////////////////////////////////////////////
//                                                                                                //
////////////////////////////////////////////////////////////////////////////////

#endif
