#ifndef _CORE_LIB_CONTAINERS_SLOT_MAP_HPP_
#define _CORE_LIB_CONTAINERS_SLOT_MAP_HPP_

#include "core/memory/unique_array.hpp"

////////////////////////////////////////////////////////////////////////////////
// core container
////////////////////////////////////////////////////////////////////////////////

namespace core { namespace container {

template<typename T>
class SlotMap;

/*******************************************************************************
** SlotMapId
*******************************************************************************/

struct SlotMapId {
  SlotMapId() = default;
  SlotMapId(SlotMapId &&) = default;
  SlotMapId(SlotMapId const &) = default;
  SlotMapId & operator=(SlotMapId &&) = default;
  SlotMapId & operator=(SlotMapId const &) = default;
  ~SlotMapId() = default;

private:
  template<typename T>
  friend class SlotMap;

  uint16 version = 0u;
  uint16 index = 0u;

  SlotMapId(uint32 _version, uint32 _index)
      : version(_version)
      , index(_index) {}
};

/*******************************************************************************
** SlotMap
*******************************************************************************/

template<typename T>
class SlotMap {
public:
  using value_type = T;
  using pointer_type = T *;
  using const_pointer_type = T const *;
  using reference_type = T &;
  using const_reference_type = T const &;
  using iterator_type = core::contiguous_iterator<T>;
  using const_iterator_type = core::contiguous_iterator<T const>;

  SlotMap();
  explicit SlotMap(size_t _size);
  SlotMap(SlotMap &&) = default;
  SlotMap(SlotMap const &) = default;
  SlotMap & operator=(SlotMap &&) = default;
  SlotMap & operator=(SlotMap const &) = default;
  ~SlotMap() = default;

  template<typename... Args>
  SlotMapId Create(Args &&... _args);
  void Destroy(SlotMapId _id);

  bool IsValid(SlotMapId _id) const;

  pointer_type Get(SlotMapId _id);
  const_pointer_type Get(SlotMapId _id) const;

  reference_type operator[](SlotMapId _id);
  const_reference_type operator[](SlotMapId _id) const;

  iterator_type begin() { return iterator_type(data.Get()); }
  const_iterator_type begin() const { return const_iterator_type(data.Get()); }
  iterator_type end() { return iterator_type(data.Get() + data_size); }
  const_iterator_type end() const {
    return const_iterator_type(data.Get() + data_size);
  }

private:
  struct Slot {
    uint16 version = 0u;
    uint16 index = 0u;
  };
  UniqueArray<T> data;
  UniqueArray<Slot> slots;
  UniqueArray<uint16> data_to_slots;
  uint16 data_size = 0u;
  uint16 slots_size = 0u;
  uint16 free_slot = 0u;
};

}}

////////////////////////////////////////////////////////////////////////////////
// core container
////////////////////////////////////////////////////////////////////////////////

namespace core { namespace container {

/*******************************************************************************
** SlotMap
*******************************************************************************/

template<typename T>
inline SlotMap<T>::SlotMap()
    : SlotMap(2048u) {}

template<typename T>
inline SlotMap<T>::SlotMap(size_t _size) {
  data = make_unique_array<T>(_size);
  slots = make_unique_array<Slot>(_size);
  data_to_slots = make_unique_array<uint16>(_size);
}

template<typename T>
template<typename... Args>
inline SlotMapId SlotMap<T>::Create(Args &&... _args) {
  CORE_LIB_ASSERT(free_slot <= slots_size);

  uint16 new_slot_index = free_slot;
  Slot & slot = slots[free_slot];
  if (free_slot < slots_size) {
    free_slot = slot.index;
  } else {
    ++slots_size;
    free_slot = slots_size;
  }
  slot.index = data_size;
  SlotMapId id(slot.version, new_slot_index);
  data_to_slots[data_size] = new_slot_index;

  new (&data[data_size]) T(forward<Args>(_args)...);

  data_size = data_size + 1u;

  return id;
}

template<typename T>
inline void SlotMap<T>::Destroy(SlotMapId _id) {
  CORE_LIB_ASSERT(data_size != 0);
  CORE_LIB_ASSERT(IsValid(_id));

  Slot & slot = slots[_id.index];
  data[slot.index].~T();
  if (_id.index != data_to_slots[data_size - 1]) {
    Slot & last_slot = slots[data_to_slots[data_size - 1]];
    new (&data[slot.index]) T(move(data[last_slot.index]));
    last_slot.index = slot.index;
    data_to_slots[slot.index] = data_to_slots[data_size - 1];
  }

  slot.index = free_slot;
  ++slot.version;
  CORE_LIB_ASSERT(data_size > 0u);
  --data_size;
  free_slot = _id.index;
}

template<typename T>
inline bool SlotMap<T>::IsValid(SlotMapId _id) const {
  return slots[_id.index].version == _id.version;
}

template<typename T>
inline typename SlotMap<T>::pointer_type SlotMap<T>::Get(SlotMapId _id) {
  if (!IsValid(_id)) {
    return nullptr;
  }
  return &data[slots[_id.index].index];
}

template<typename T>
inline typename SlotMap<T>::const_pointer_type
SlotMap<T>::Get(SlotMapId _id) const {
  if (!IsValid(_id)) {
    return nullptr;
  }
  return &data[slots[_id.index].index];
}

template<typename T>
inline typename SlotMap<T>::reference_type
SlotMap<T>::operator[](SlotMapId _id) {
  CORE_LIB_ASSERT(IsValid(_id));
  return data[slots[_id.index].index];
}

template<typename T>
inline typename SlotMap<T>::const_reference_type
SlotMap<T>::operator[](SlotMapId _id) const {
  CORE_LIB_ASSERT(IsValid(_id));
  return data[slots[_id.index].index];
}

}}

#endif
