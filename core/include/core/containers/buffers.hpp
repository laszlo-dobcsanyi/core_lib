#ifndef _CORE_LIB_CONTAINERS_BUFFERS_HPP_
#define _CORE_LIB_CONTAINERS_BUFFERS_HPP_

#ifndef _CORE_LIB_CONTAINERS_HPP_
#  error
#endif

#include "core/memory/uninitialized_array.hpp"

////////////////////////////////////////////////////////////////////////////////
// container
////////////////////////////////////////////////////////////////////////////////

namespace container {

/*******************************************************************************
** RingBuffer
*******************************************************************************/

template<class T, size_t Bound>
class RingBuffer final {
public:
  RingBuffer() = default;
  RingBuffer(RingBuffer &&) = delete;
  RingBuffer(RingBuffer const &) = delete;
  RingBuffer & operator=(RingBuffer &&) = delete;
  RingBuffer & operator=(RingBuffer const &) = delete;
  ~RingBuffer() { Clear(); }

  size_t Occupied() const { return occupied; }
  bool IsEmpty() const { return occupied == 0u; }
  bool IsFull() const { return occupied == Bound; }

  template<class Tag, typename... Args>
  T & Push(Args &&... _args) {
    added++;
    CORE_LIB_ASSERT(!IsFull());
    auto member = &data[Forward_Point<Tag>()];
    new (member) T(forward<Args>(_args)...);
    return *member;
  }

  template<class Tag>
  void Pop() {
    CORE_LIB_ASSERT(!IsEmpty());
    auto member = &data[Backward_Point<Tag>()];
    member->~T();
    added--;
  }

  void Clear() {
    added = 0;
    if (!IsEmpty()) {
      if (!IsFull()) {
        while (occupied--) {
          auto member = &data[Head()];
          member->~T();
        }
      } else {
        for (auto current = 0; current < Bound; ++current) {
          auto member = &data[current];
          member->~T();
        }
      }
    }
  }

  template<class Tag>
  if_head_t<Tag, T &> & Get() {
    CORE_LIB_ASSERT(!IsEmpty() && occupied == added);
    return data[Head()];
  }

  template<class Tag>
  if_head_t<Tag, T const &> & Get() const {
    CORE_LIB_ASSERT(!IsEmpty() && occupied == added);
    return data[Head()];
  }

  template<class Tag>
  if_tail_t<Tag, T &> & Get() {
    CORE_LIB_ASSERT(!IsEmpty() && occupied == added);
    return data[tail];
  }

  template<class Tag>
  if_tail_t<Tag, T const &> & Get() const {
    CORE_LIB_ASSERT(!IsEmpty() && occupied == added);
    return data[tail];
  }

private:
  core::memory::UninitializedArray<T, Bound> data;
  size_t tail = 0u;
  size_t occupied = 0u;
  size_t added = 0u;

  size_t AsIndex(size_t _value) const { return _value % Bound; }
  size_t Next(size_t _index) const { return AsIndex(_index + 1); }
  T * Get(size_t _index) {
    CORE_LIB_ASSERT(_index < Bound);
    return &data[_index];
  }
  T * Get(size_t _index) const {
    CORE_LIB_ASSERT(_index < Bound);
    return &data[_index];
  }
  size_t Head() const { return AsIndex(tail + (occupied - 1)); }

  template<class Tag>
  if_head_t<Tag, size_t> Forward_Point() {
    auto result = AsIndex(tail + occupied);
    ++occupied;
    return result;
  }

  template<class Tag>
  if_tail_t<Tag, size_t> Forward_Point() {
    auto result = tail;
    ++occupied;
    tail = AsIndex(tail + (Bound - 1));
    return result;
  }

  template<class Tag>
  if_head_t<Tag, size_t> Backward_Point() {
    auto result = AsIndex(tail + occupied);
    --occupied;
    return result;
  }

  template<class Tag>
  if_tail_t<Tag, size_t> Backward_Point() {
    auto result = tail;
    --occupied;
    tail = RingBuffer::AsIndex(tail + 1);
    return result;
  }
};

}

////////////////////////////////////////////////////////////////////////////////
// container
////////////////////////////////////////////////////////////////////////////////

namespace container {
}

#endif
