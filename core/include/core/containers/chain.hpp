#ifndef _CORE_LIB_CONTAINERS_CHAIN_HPP_
#define _CORE_LIB_CONTAINERS_CHAIN_HPP_

#ifndef _CORE_LIB_CONTAINERS_HPP_
#  error
#endif

// TODO: Add support for const chains!

////////////////////////////////////////////////////////////////////////////////
// container
////////////////////////////////////////////////////////////////////////////////

namespace container {

/*******************************************************************************
** ChainElement
*******************************************************************************/

template<class DerivedType>
class ChainElement {
public:
  template<class, template<class> class>
  friend class Chain;
  template<class, template<class> class>
  friend class ::query::Chain_Query;
  template<class>
  friend class ChainElementIterator;

protected:
  ChainElement() = default;
  ChainElement(ChainElement &&) = delete;
  ChainElement(ChainElement const &) = delete;
  ChainElement & operator=(ChainElement &&) = delete;
  ChainElement & operator=(ChainElement const &) = delete;
  ~ChainElement() { CORE_LIB_ASSERT(!next && !prev); }

  bool IsInChain() const { return in_chain; }

private:
  DerivedType * next = nullptr;
  DerivedType * prev = nullptr;
  bool in_chain = false;
};

/*******************************************************************************
** ChainElementIterator
*******************************************************************************/

template<class DerivedType>
class ChainElementIterator {
public:
  ChainElementIterator(DerivedType * _element)
      : element(_element) {}

  DerivedType & operator*() {
    CORE_LIB_ASSERT(element);
    return *element;
  }

  ChainElementIterator & operator++() {
    CORE_LIB_ASSERT(element);
    element = element->next;
    return *this;
  }
  bool operator==(ChainElementIterator const & _other) {
    return element == _other.element;
  }
  bool operator!=(ChainElementIterator const & _other) {
    return !(*this == _other);
  }

private:
  DerivedType * element;
};

/*******************************************************************************
** Chain
*******************************************************************************/

template<class DerivedType, template<class> class ChainElement>
class Chain {
public:
  // CORE_LIB_STATIC_ASSERT_TEXT( ::mpl::Is_Base_Of< ChainElementType< DerivedType >
  //                                               , DerivedType >::value
  //  , "Template type ChainElementType is not base of template type DerivedType!" );
  // CORE_LIB_STATIC_ASSERT_TEXT( ::mpl::Is_Same< ChainElementType< DerivedType >
  //                                            , ::container::ChainElement< DerivedType > >::value
  //                              || ::mpl::Is_Base_Of< ChainElementType< DerivedType >
  //                                                  , ::container::ChainElement< DerivedType > >::value
  //  , "Template type ChainElementType is not equal or derived from ::containerChainElement!" );

  using ChainElementType = ChainElement<DerivedType>;
  using QueryType = ::query::Chain_Query<DerivedType, ChainElement>;
  friend QueryType;

  Chain() = default;
  Chain(Chain && _other) {
    first = _other.first;
    last = _other.last;
#if defined(CORE_LIB_CONFIGURATION_DEBUG)
    count = _other.count;
    Validate();
#endif

    _other.first = nullptr;
    _other.last = nullptr;
#if defined(CORE_LIB_CONFIGURATION_DEBUG)
    _other.count = 0u;
#endif
  }
  Chain(Chain const &) = delete;
  Chain & operator=(Chain && _other) {
    if (this != &_other) {
      Release();

      first = _other.first;
      last = _other.last;
#if defined(CORE_LIB_CONFIGURATION_DEBUG)
      count = _other.count;
      Validate();
#endif

      _other.first = nullptr;
      _other.last = nullptr;
#if defined(CORE_LIB_CONFIGURATION_DEBUG)
      _other.count = 0u;
#endif
    }
    return *this;
  }
  Chain & operator=(Chain const &) = delete;
  ~Chain() {
#if defined(CORE_LIB_CONFIGURATION_DEBUG)
    Validate();
#endif
    Reset();
  }

  template<typename LinkageType>
  if_head_t<LinkageType, void> Link(DerivedType & _enclosing) {
    CORE_LIB_ASSERT(!_enclosing.ChainElementType::next &&
                    !_enclosing.ChainElementType::prev &&
                    !_enclosing.ChainElementType::in_chain);
    if (last) {
      last->ChainElementType::prev = &_enclosing;
      _enclosing.ChainElementType::next = last;
      _enclosing.ChainElementType::prev = nullptr;
    } else {
      first = &_enclosing;
    }
    last = &_enclosing;
    _enclosing.ChainElementType::in_chain = true;
#if defined(CORE_LIB_CONFIGURATION_DEBUG)
    count++;
    Validate();
#endif
  }

  template<typename LinkageType>
  if_head_t<LinkageType, void> Unlink(DerivedType & _enclosing) {
    CORE_LIB_ASSERT(_enclosing.ChainElementType::in_chain);
    DerivedType * previous = _enclosing.ChainElementType::prev;
    if (_enclosing.ChainElementType::prev) {
      _enclosing.ChainElementType::prev->ChainElementType::next =
          _enclosing.ChainElementType::next;
      _enclosing.ChainElementType::prev = nullptr;
    } else {
      CORE_LIB_ASSERT(last == &_enclosing);
      last = _enclosing.ChainElementType::next;
    }
    if (_enclosing.ChainElementType::next) {
      _enclosing.ChainElementType::next->ChainElementType::prev = previous;
      _enclosing.ChainElementType::next = nullptr;
    } else {
      CORE_LIB_ASSERT(first == &_enclosing);
      first = previous;
    }
    _enclosing.ChainElementType::in_chain = true;
#if defined(CORE_LIB_CONFIGURATION_DEBUG)
    count--;
    Validate();
#endif
  }

  template<typename LinkageType>
  if_tail_t<LinkageType, void> Link(DerivedType & _enclosing) {
    CORE_LIB_ASSERT(!_enclosing.ChainElementType::next &&
                    !_enclosing.ChainElementType::prev &&
                    !_enclosing.ChainElementType::in_chain);
    if (last) {
      CORE_LIB_ASSERT(!last->ChainElementType::next);
      last->ChainElementType::next = &_enclosing;
      _enclosing.ChainElementType::prev = last;
    } else {
      CORE_LIB_ASSERT(!first);
      first = &_enclosing;
    }
    last = &_enclosing;
    _enclosing.ChainElementType::in_chain = true;
#if defined(CORE_LIB_CONFIGURATION_DEBUG)
    count++;
    Validate();
#endif
  }

  template<typename LinkageType>
  if_tail_t<LinkageType, void> Unlink(DerivedType & _enclosing) {
    CORE_LIB_ASSERT(_enclosing.ChainElementType::in_chain);
    DerivedType * next = _enclosing.ChainElementType::next;
    if (_enclosing.ChainElementType::next) {
      _enclosing.ChainElementType::next->ChainElementType::prev =
          _enclosing.ChainElementType::prev;
      _enclosing.ChainElementType::next = nullptr;
    } else {
      CORE_LIB_ASSERT(last == &_enclosing);
      last = _enclosing.ChainElementType::prev;
    }
    if (_enclosing.ChainElementType::prev) {
      _enclosing.ChainElementType::prev->ChainElementType::next = next;
      _enclosing.ChainElementType::prev = nullptr;
    } else {
      CORE_LIB_ASSERT(first == &_enclosing);
      first = next;
    }
    _enclosing.ChainElementType::in_chain = false;
#if defined(CORE_LIB_CONFIGURATION_DEBUG)
    count--;
    Validate();
#endif
  }

  void Reset() {
    Release();
    first = nullptr;
    last = nullptr;
#if defined(CORE_LIB_CONFIGURATION_DEBUG)
    count = 0u;
    Validate();
#endif
  }

  ChainElementIterator<DerivedType> begin() { return first; }
  ChainElementIterator<DerivedType const> begin() const { return first; }
  ChainElementIterator<DerivedType> end() { return nullptr; }
  ChainElementIterator<DerivedType const> end() const { return nullptr; }

private:
  DerivedType * first = nullptr;
  DerivedType * last = nullptr;
#if defined(CORE_LIB_CONFIGURATION_DEBUG)
  size_t count = 0u;
#endif

  void Release() {
    DerivedType * element = first;
    DerivedType * next = element;
    while (element) {
      next = element->ChainElementType::next;
      element->ChainElementType::next = nullptr;
      element->ChainElementType::prev = nullptr;
      element->ChainElementType::in_chain = false;
      element = next;
    }
  }

#if defined(CORE_LIB_CONFIGURATION_DEBUG)
  void Validate() {
    size_t elements = 0u;
    DerivedType * previous = nullptr;
    DerivedType * element = first;
    if (element) {
      for (;;) {
        ++elements;
        CORE_LIB_ASSERT(element->ChainElementType::prev == previous);
        CORE_LIB_ASSERT(element->ChainElementType::in_chain);

        if (!element->ChainElementType::next)
          break;
        previous = element;
        element = element->ChainElementType::next;
      }
    }
    CORE_LIB_ASSERT(element == last);
    CORE_LIB_ASSERT(elements == count);
  }
#endif
};

}

////////////////////////////////////////////////////////////////////////////////
// container
////////////////////////////////////////////////////////////////////////////////

namespace container {
}

#endif
