#ifndef _CORE_LIB_CONTAINERS_QUEUES_HPP_
#define _CORE_LIB_CONTAINERS_QUEUES_HPP_

#ifndef _CORE_LIB_CONTAINERS_HPP_
#  error
#endif

////////////////////////////////////////////////////////////////////////////////
// container detail
////////////////////////////////////////////////////////////////////////////////

namespace container { namespace detail {

/*******************************************************************************
** LockedQueueNode
*******************************************************************************/

template<typename T>
struct LockedQueueNode {
  T * ptr = nullptr;
  LockedQueueNode<T> * next = nullptr;
};

}}

////////////////////////////////////////////////////////////////////////////////
// container
////////////////////////////////////////////////////////////////////////////////

namespace container {

/*******************************************************************************
** BoundedQueue
*******************************************************************************/

template<typename T, size_t Bound>
class BoundedQueue final {
public:
  BoundedQueue() = default;
  BoundedQueue(BoundedQueue && _other) = delete;
  BoundedQueue(BoundedQueue const & _other) = delete;
  BoundedQueue & operator=(BoundedQueue && _other) = delete;
  BoundedQueue & operator=(BoundedQueue const & _other) = delete;
};

/*******************************************************************************
** UnboundedQueue
*******************************************************************************/

template<class T, template<typename> class TypeAllocator>
class UnboundedQueue final {
public:
  static const size_t StackBound = 32u;

  UnboundedQueue() = default;
  UnboundedQueue(UnboundedQueue &&) = default;
  UnboundedQueue(UnboundedQueue const &) = delete;
  UnboundedQueue & operator=(UnboundedQueue &&) = default;
  UnboundedQueue & operator=(UnboundedQueue const &) = delete;
  ~UnboundedQueue() = default;

  size_t Size() const;
  template<typename... Args>
  T & Push(Args &&... _args);
  T & Last();
  T const & Last() const;
  void Pop();

private:
  using StackType = BoundedStack<T, StackBound>;
  using StackListType = DoubleList<StackType, TypeAllocator>;
  StackListType stack_list;
  size_t size = 0u;
};

/*******************************************************************************
** LockedQueue
*******************************************************************************/

template<typename T, template<typename> class TypeAllocator>
class LockedQueue final {
public:
  using Node = detail::LockedQueueNode<T>;

  LockedQueue();
  LockedQueue(LockedQueue &&) = delete;
  LockedQueue(LockedQueue const &) = delete;
  LockedQueue & operator=(LockedQueue &&) = delete;
  LockedQueue & operator=(LockedQueue const &) = delete;
  ~LockedQueue();

  void Push(T & _ref);
  T * Pop();

private:
  TypeAllocator<Node> node_allocator;
  Mutex head_mutex;
  Mutex tail_mutex;
  Node * head;
  Node * tail;

  Node * GetTail();
  Node * PopHead();
};

}

////////////////////////////////////////////////////////////////////////////////
// container
////////////////////////////////////////////////////////////////////////////////

namespace container {

/*******************************************************************************
** UnboundedQueue
*******************************************************************************/

template<typename T, template<typename> class TypeAllocator>
inline size_t UnboundedQueue<T, TypeAllocator>::Size() const {
  return size;
}

template<typename T, template<typename> class TypeAllocator>
template<typename... Args>
inline T & UnboundedQueue<T, TypeAllocator>::Push(Args &&... /* _args */) {
  CORE_LIB_UNIMPLEMENTED;
#if 0
  auto tail = stack_list.Tail();
  if ( !tail ) {
    tail = &stack_list.Link_Tail();
  }
#endif
}

template<typename T, template<typename> class TypeAllocator>
inline T & UnboundedQueue<T, TypeAllocator>::Last() {
  CORE_LIB_UNIMPLEMENTED;
}

template<typename T, template<typename> class TypeAllocator>
inline T const & UnboundedQueue<T, TypeAllocator>::Last() const {
  CORE_LIB_UNIMPLEMENTED;
}

template<typename T, template<typename> class TypeAllocator>
inline void UnboundedQueue<T, TypeAllocator>::Pop() {
  CORE_LIB_UNIMPLEMENTED;
}

/*******************************************************************************
** LockedQueue
*******************************************************************************/

template<typename T, template<typename> class TypeAllocator>
inline LockedQueue<T, TypeAllocator>::LockedQueue()
    : head(&node_allocator.Create())
    , tail(head) {}

template<typename T, template<typename> class TypeAllocator>
inline LockedQueue<T, TypeAllocator>::~LockedQueue() {
  CORE_LIB_ASSERT(!head->next);
  node_allocator.Destroy(*head);
}

template<typename T, template<typename> class TypeAllocator>
inline void LockedQueue<T, TypeAllocator>::Push(T & _ref) {
  auto & node = node_allocator.Create();
  {
    LockGuard<Mutex> tail_lock(tail_mutex);
    tail->ptr = &_ref;
    tail->next = &node;
    // CORE_LIB_LOG( LOG, "LockedQueue pushed Node @ %p[%p]!\n"
    //                  , reinterpret_cast< void * >( tail )
    //                  , reinterpret_cast< void * >( &_ref ) );
    tail = &node;
  }
}

template<typename T, template<typename> class TypeAllocator>
inline T * LockedQueue<T, TypeAllocator>::Pop() {
  auto old_head = PopHead();
  if (old_head) {
    T * result = old_head->ptr;
    // COLLECTION_MESSAGE( "LockedQueue popped Node @ %p[%p]!\n"
    //                   , reinterpret_cast< void * >( old_head )
    //                   , reinterpret_cast< void * >( result ) );
    node_allocator.Destroy(*old_head);
    return result;
  } else {
    return nullptr;
  }
}

template<typename T, template<typename> class TypeAllocator>
inline detail::LockedQueueNode<T> * LockedQueue<T, TypeAllocator>::GetTail() {
  LockGuard<Mutex> tail_lock(tail_mutex);
  return tail;
}

template<typename T, template<typename> class TypeAllocator>
inline detail::LockedQueueNode<T> * LockedQueue<T, TypeAllocator>::PopHead() {
  {
    LockGuard<Mutex> head_lock(head_mutex);
    if (head != GetTail()) {
      Node * old_head = head;
      head = head->next;
      return old_head;
    } else {
      return nullptr;
    }
  }
}

}

#endif
