#ifndef _CORE_LIB_CONTAINERS_HASH_TABLE_HPP_
#define _CORE_LIB_CONTAINERS_HASH_TABLE_HPP_

#ifndef CORE_LIB_CONTAINERS
#  error
#endif

////////////////////////////////////////////////////////////////////////////////
// container detail
////////////////////////////////////////////////////////////////////////////////

namespace container { namespace detail {

/*******************************************************************************
** HashTable
*******************************************************************************/

template<typename Key, typename ValueType, class Allocator>
class HashTable final {
public:
  template<typename... Args>
  HashTable(Args &&... _args);
  HashTable(HashTable &&) = default;
  HashTable(HashTable const &) = delete;
  HashTable & operator=(HashTable &&) = default;
  HashTable & operator=(HashTable const &) = delete;
  ~HashTable();

  template<typename... Args>
  ValueType & Insert(Key _key, Args &&... _args);
  void Remove(Key _key);
  void Remove(ValueType const & _value);

  ValueType * Find(Key _key);
  ValueType * Find(ValueType const & _value);

private:
  struct Entry {
    Key key;
    ValueType value;
  };

  allocator::TypeAllocator<Entry, Allocator> allocator;
  allocator::Allocation table;
  size_t elements = 0u;

  allocator::Allocation CreateTable(size_t _table_size);
  void DestroyTable(allocator::Allocation && _table);
  void Rehash(allocator::Allocation & new_table);
  void Grow();
  Entry & FindFreeEntry(Key _key);
};

}}

////////////////////////////////////////////////////////////////////////////////
// container detail
////////////////////////////////////////////////////////////////////////////////

namespace container { namespace detail {

/*******************************************************************************
** HashTable
*******************************************************************************/

template<typename Key, typename ValueType, class Allocator>
template<typename... Args>
inline HashTable<Key, ValueType, Allocator>::HashTable(Args &&... _args)
    : allocator(forward<Args>(_args)...) {}

template<typename Key, typename ValueType, class Allocator>
inline HashTable<Key, ValueType, Allocator>::~HashTable() {
  if (table) {
    DestroyTable(move(table));
  }
}

template<typename Key, typename ValueType, class Allocator>
template<typename... Args>
inline ValueType &
HashTable<Key, ValueType, Allocator>::Insert(Key _key, Args &&... _args) {
  Grow();
  auto & entry = FindFreeEntry(_key);
  entry.key = _key;
  new (&entry.value) ValueType(forward(_args)...);
  return entry.value;
}

template<typename Key, typename ValueType, class Allocator>
inline void HashTable<Key, ValueType, Allocator>::Remove(Key /*_key */) {
  CORE_LIB_UNIMPLEMENTED;
}

template<typename Key, typename ValueType, class Allocator>
inline void
HashTable<Key, ValueType, Allocator>::Remove(ValueType const & /*_value */) {
  CORE_LIB_UNIMPLEMENTED;
}

template<typename Key, typename ValueType, class Allocator>
inline ValueType * HashTable<Key, ValueType, Allocator>::Find(Key /*_key*/) {
  CORE_LIB_UNIMPLEMENTED;
}

template<typename Key, typename ValueType, class Allocator>
inline ValueType *
HashTable<Key, ValueType, Allocator>::Find(ValueType const & /*_value*/) {
  CORE_LIB_UNIMPLEMENTED;
}

template<typename Key, typename ValueType, class Allocator>
inline allocator::Allocation
HashTable<Key, ValueType, Allocator>::CreateTable(size_t _table_size) {
  auto const memory =
      reinterpret_cast<uintptr_t>(allocator.AllocateA(_table_size));
  for (auto current = 0u; current < _table_size; ++current) {
    auto & entry = *(reinterpret_cast<Entry *>(memory) + current);
    entry.key = current + 1u;
  }
  return allocator::Allocation(memory, _table_size);
}

template<typename Key, typename ValueType, class Allocator>
inline void HashTable<Key, ValueType, Allocator>::DestroyTable(
    allocator::Allocation && _table) {
  allocator::Allocation allocation(move(_table));
  for (auto current = 0u; current < allocation.Size(); ++current) {
    auto & entry = *(allocation.As<Entry *>() + current);
    if (entry.key == current) {
      entry.~Entry();
    }
  }

  size_t count = allocation.Size() / sizeof(Entry);
  Entry * ptr = allocation.ReleaseAs<Entry *>();
  allocator.DeallocateA(ptr, count);
}

template<typename Key, typename ValueType, class Allocator>
inline void HashTable<Key, ValueType, Allocator>::Rehash(
    allocator::Allocation & /*_new_table*/) {
  CORE_LIB_UNIMPLEMENTED;
}

template<typename Key, typename ValueType, class Allocator>
inline void HashTable<Key, ValueType, Allocator>::Grow() {
  if (!table.IsEmpty()) {
    auto const load_factor =
        static_cast<float>(elements) / static_cast<float>(table.Size());
    if (load_factor < 0.7f) {
      return;
    }
  }
  // TODO: AllocationPolicy!
  auto new_table = CreateTable(table.IsEmpty() ? 512u : table.Size() << 1u);
  Rehash(new_table);
  DestroyTable(move(table));
  table = move(new_table);
}

template<typename Key, typename ValueType, class Allocator>
inline typename HashTable<Key, ValueType, Allocator>::Entry &
HashTable<Key, ValueType, Allocator>::FindFreeEntry(Key /*_key*/) {
  CORE_LIB_UNIMPLEMENTED;
}

}}

#endif
