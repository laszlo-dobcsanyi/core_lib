#ifndef _CORE_COLLECTIONS_INTRUSIVE_LIST_HPP_
#define _CORE_COLLECTIONS_INTRUSIVE_LIST_HPP_

#include "core/base.h"

////////////////////////////////////////////////////////////////////////////////
// core
////////////////////////////////////////////////////////////////////////////////

namespace core {

template<typename T, typename Policy>
class intrusive_list;

/*******************************************************************************
** intrusive_list_node
*******************************************************************************/

class intrusive_list_node {
public:
  intrusive_list_node() = default;
  intrusive_list_node(intrusive_list_node && _other) {
    if (_other.is_linked()) {
      prev = _other.prev;
      prev->next = this;
      _other.prev = nullptr;

      next = _other.next;
      next->prev = this;
      _other.next = nullptr;
    } else {
      prev = nullptr;
      next = nullptr;
    }
  }
  intrusive_list_node(intrusive_list_node const &)
      : intrusive_list_node() {}
  intrusive_list_node & operator=(intrusive_list_node && _other) {
    if (this != &_other) {
      unlink();

      if (_other.is_linked()) {
        prev = _other.prev;
        prev->next = this;
        _other.prev = nullptr;

        next = _other.next;
        next->prev = this;
        _other.next = nullptr;
      } else {
        prev = nullptr;
        next = nullptr;
      }
    }
    return *this;
  }
  intrusive_list_node & operator=(intrusive_list_node const & _other) {
    if (this != &_other) {
      remove();
    }
    return *this;
  }
  ~intrusive_list_node() { unlink(); }

  bool is_linked() const { return prev != nullptr; }

  void remove() {
    unlink();

    prev = nullptr;
    next = nullptr;
  }

private:
  template<typename T, typename Policy>
  friend class intrusive_list;

  intrusive_list_node * prev = nullptr;
  intrusive_list_node * next = nullptr;

  void unlink() {
    if (is_linked()) {
      prev->next = next;
      next->prev = prev;
    }
  }
};

/*******************************************************************************
** inherited_policy
*******************************************************************************/

template<typename T>
struct inherited_policy {
  CORE_LIB_STATIC_ASSERT(mpl::is_base_of<intrusive_list_node, T>::value);

  static intrusive_list_node & get_node(T & _value) {
    return static_cast<intrusive_list_node &>(_value);
  }

  static intrusive_list_node const & get_node(T const & _value) {
    return static_cast<intrusive_list_node const &>(_value);
  }

  static T & get_value(intrusive_list_node & _node) {
    return static_cast<T &>(_node);
  }

  static T const & get_value(intrusive_list_node const & _node) {
    return static_cast<T const &>(_node);
  }
};

/*******************************************************************************
** member_policy
*******************************************************************************/

template<typename T, intrusive_list_node T::*Member>
struct member_policy {
  static intrusive_list_node & get_node(T & _value) { return _value.*Member; }

  static intrusive_list_node const & get_node(T const & _value) {
    return _value.*Member;
  }

  static constexpr std::size_t MemberOffset() {
    return reinterpret_cast<std::size_t>(&(((T *)nullptr)->*Member));
  }

  static T & get_value(intrusive_list_node & _node) {
    return *reinterpret_cast<T *>(reinterpret_cast<std::uintptr_t>(&_node) -
                                  MemberOffset());
  }

  static T const & get_value(intrusive_list_node const & _node) {
    return *reinterpret_cast<T const *>(
        reinterpret_cast<std::uintptr_t>(&_node) - MemberOffset());
  }
};

/*******************************************************************************
** intrusive_list
*******************************************************************************/

template<typename T, typename Policy = inherited_policy<T>>
class intrusive_list {
public:
  template<typename ValueType, typename NodeType>
  class iterator_base {
  public:
    using value_type = ValueType;
    using pointer_type = value_type *;
    using reference_type = value_type &;
    using node_type = NodeType;

    iterator_base(node_type * _current)
        : current(_current)
        , next(current->next) {}

    iterator_base operator++() {
      CORE_LIB_ASSERT(next);
      current = next;
      next = current->next;
      return *this;
    }

    pointer_type operator->() const {
      CORE_LIB_ASSERT(current);
      return &Policy::get_value(*current);
    }

    reference_type operator*() const {
      CORE_LIB_ASSERT(current);
      return Policy::get_value(*current);
    }

    bool operator==(iterator_base const & _other) const {
      return current == _other.current;
    }

    bool operator!=(iterator_base const & _other) const {
      return !(*this == _other);
    }

  private:
    friend intrusive_list;

    node_type * current = nullptr;
    node_type * next = nullptr;
  };

  using value_type = T;
  using pointer_type = value_type *;
  using const_pointer_type = value_type const *;
  using reference_type = value_type &;
  using const_reference_type = value_type const &;
  using iterator_type = iterator_base<T, intrusive_list_node>;
  using const_iterator_type = iterator_base<T const, intrusive_list_node const>;

  intrusive_list() {
    head.next = &tail;
    tail.prev = &head;
  }
  intrusive_list(intrusive_list && _other) {
    head.next = _other.head.next;
    head.next->prev = &head;
    _other.head.next = &_other.tail;

    tail.prev = _other.tail.prev;
    tail.prev->next = &tail;
    _other.tail.prev = &_other.head;
  }
  intrusive_list(intrusive_list const &)
      : intrusive_list() {}
  intrusive_list & operator=(intrusive_list && _other) {
    if (this != &_other) {
      clear();

      head.next = _other.head.next;
      head.next->prev = &head;
      _other.head.next = &_other.tail;

      tail.prev = _other.tail.prev;
      tail.prev->next = &tail;
      _other.tail.prev = &_other.head;
    }
    return *this;
  }
  intrusive_list & operator=(intrusive_list const & _other) {
    if (this != &_other) {
      clear();
    }
    return *this;
  }
  ~intrusive_list() { clear(); }

  bool empty() const { return head.next == &tail; }

  bool contains(T const & _value) const {
    intrusive_list_node const & node = Policy::get_node(_value);
    if (!node.is_linked()) {
      return false;
    }
    for (T const & value : *this) {
      if (&value == &_value) {
        return true;
      }
    }
    return false;
  }

  void push_front(T & _value) {
    intrusive_list_node & node = Policy::get_node(_value);
    CORE_LIB_ASSERT(!node.is_linked());
    node.prev = &head;
    node.next = head.next;
    head.next->prev = &node;
    head.next = &node;
  }

  void push_back(T & _value) {
    intrusive_list_node & node = Policy::get_node(_value);
    CORE_LIB_ASSERT(!node.is_linked());
    node.prev = tail.prev;
    node.next = &tail;
    tail.prev->next = &node;
    tail.prev = &node;
  }

  T & pop_front() {
    CORE_LIB_ASSERT(!empty());
    intrusive_list_node & node = *head.next;
    node.remove();
    return Policy::get_value(node);
  }

  T * try_pop_front() {
    if (!empty()) {
      return &pop_front();
    }
    return nullptr;
  }

  T & pop_back() {
    CORE_LIB_ASSERT(!empty());
    intrusive_list_node & node = *tail.prev;
    node.remove();
    return Policy::get_value(node);
  }

  T * try_pop_back() {
    if (!empty()) {
      return &pop_back();
    }
    return nullptr;
  }

  iterator_type erase(T & _element) {
    intrusive_list_node & node = Policy::get_node(_element);
    return erase(iterator_type(&node));
  }

  iterator_type erase(iterator_type _iterator) {
    CORE_LIB_ASSERT(contains(Policy::get_value(*_iterator.current)));
    iterator_type next(_iterator.next);
    _iterator.current->remove();
    return next;
  }

  void clear() {
    for (intrusive_list_node * node = head.next; node != &tail;) {
      intrusive_list_node * next = node->next;
      node->prev = nullptr;
      node->next = nullptr;
      node = next;
    }
    head.next = nullptr;
    tail.prev = nullptr;
  }

  iterator_type begin() { return iterator_type(head.next); }
  const_iterator_type begin() const { return const_iterator_type(head.next); }
  iterator_type end() { return iterator_type(&tail); }
  const_iterator_type end() const { return const_iterator_type(&tail); }

private:
  intrusive_list_node head;
  intrusive_list_node tail;
};

}

#endif
