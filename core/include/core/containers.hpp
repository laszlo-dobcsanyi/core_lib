#ifndef _CORE_LIB_CONTAINERS_HPP_
#define _CORE_LIB_CONTAINERS_HPP_

#include "core/base.h"
#include "core/allocators.hpp"

#include "core/containers.hh"
#include "core/queries.hh"

#define CORE_LIB_CONTAINERS
#include "core/containers/bitset.hpp"
#include "core/containers/arrays.hpp"
#include "core/containers/lists.hpp"
#include "core/containers/chain.hpp"
#include "core/containers/stacks.hpp"
#include "core/containers/queues.hpp"
#include "core/containers/buffers.hpp"
#include "core/containers/hash_table.hpp"
#undef CORE_LIB_CONTAINERS

#endif
