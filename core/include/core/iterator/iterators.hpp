#ifndef _CORE_LIB_ITERATOR_ITERATORS_HPP_
#define _CORE_LIB_ITERATOR_ITERATORS_HPP_

#include "core/base.h"

////////////////////////////////////////////////////////////////////////////////
// core
////////////////////////////////////////////////////////////////////////////////

namespace core {

/*******************************************************************************
** is_iterator
*******************************************************************************/

template<typename Iterator, typename Enabled = void>
struct is_iterator : public mpl::false_t {};

template<typename Iterator>
struct is_iterator<Iterator, mpl::void_t<typename Iterator::iterator_category>>
    : public mpl::true_t {};

/*******************************************************************************
** is_iterator_category
*******************************************************************************/

template<typename Iterator, typename Category, typename Enable = void>
struct is_iterator_category : public mpl::false_t {};

template<typename Iterator, typename Category>
struct is_iterator_category<
    Iterator, Category,
    mpl::if_same_t<typename Iterator::iterator_category, Category>>
    : public mpl::true_t {};

/*******************************************************************************
** contiguous_iterator_tag
*******************************************************************************/

struct contiguous_iterator_tag {};

template<typename Iterator>
using is_contiguous_iterator =
    is_iterator_category<Iterator, contiguous_iterator_tag>;

template<typename Iterator, typename ResultType = void>
using if_contiguous_iterator_t =
    mpl::enable_if_t<is_contiguous_iterator<Iterator>::value, ResultType>;

}

#endif
