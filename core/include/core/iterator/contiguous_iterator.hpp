#ifndef _CORE_LIB_ITERATOR_ARRAY_ITERATOR_HPP_
#define _CORE_LIB_ITERATOR_ARRAY_ITERATOR_HPP_

#include "core/iterator/iterators.hpp"

////////////////////////////////////////////////////////////////////////////////
// core
////////////////////////////////////////////////////////////////////////////////

namespace core {

/*******************************************************************************
** contiguous_iterator
*******************************************************************************/

template<typename T>
class contiguous_iterator {
public:
  using iterator_category = contiguous_iterator_tag;

  using value_type = mpl::remove_const_t<T>;
  using pointer_type = T *;
  using reference_type = T &;

  contiguous_iterator() = default;
  explicit contiguous_iterator(pointer_type _ptr)
      : ptr(_ptr) {}
  template<typename U>
  contiguous_iterator(contiguous_iterator<U> const & _other)
      : ptr(_other) {}

  explicit operator pointer_type() const { return ptr; }

  contiguous_iterator & operator++() {
    ++ptr;
    return *this;
  }

  contiguous_iterator operator+(size_t _offset) const {
    return contiguous_iterator(ptr + _offset);
  }

  contiguous_iterator & operator--() {
    --ptr;
    return *this;
  }

  contiguous_iterator operator-(size_t _offset) const {
    return contiguous_iterator(ptr - _offset);
  }

  pointer_type operator->() const { return ptr; }

  reference_type operator*() const { return *ptr; }

  bool operator==(contiguous_iterator const & _other) const {
    return ptr == _other.ptr;
  }

  bool operator!=(contiguous_iterator const & _other) const {
    return ptr != _other.ptr;
  }

protected:
  pointer_type ptr = nullptr;
};

}

#endif
