#ifndef _CORE_LIB_PLATFORM_HPP_
#define _CORE_LIB_PLATFORM_HPP_

#include "core/base.h"

// TODO: Use a unified cpuid!

#if defined(CORE_LIB_COMPILER_CLANG) || defined(CORE_LIB_COMPILER_GCC)
#  include <cpuid.h>
#endif

////////////////////////////////////////////////////////////////////////////////
// platform
////////////////////////////////////////////////////////////////////////////////

namespace platform {

/*******************************************************************************
** CacheInformation
*******************************************************************************/

struct CacheInformation {
  size_t count = 0u;
  size_t line_size = 0u;
  size_t size = 0u;
};

/*******************************************************************************
** ProcessorInformation
*******************************************************************************/

struct ProcessorInformation {
  char vendor[13] = {'\0'};
  size_t core_count = 0u;
  size_t processor_count = 0u;
  size_t packages = 0u;
  CacheInformation caches[3u];
};

/*******************************************************************************
** SIMDInformation
*******************************************************************************/

struct SIMDInformation {
  //  Misc.
  bool has_MMX;
  bool has_x64;
  bool has_ABM; // Advanced Bit Manipulation
  bool has_RDRAND;
  bool has_BMI1;
  bool has_BMI2;
  bool has_ADX;
  bool has_PREFETCHWT1;

  //  SIMD: 128-bit
  bool has_SSE;
  bool has_SSE2;
  bool has_SSE3;
  bool has_SSSE3;
  bool has_SSE41;
  bool has_SSE42;
  bool has_SSE4a;
  bool has_AES;
  bool has_SHA;

  //  SIMD: 256-bit
  bool has_AVX;
  bool has_XOP;
  bool has_FMA3;
  bool has_FMA4;
  bool has_AVX2;

  //  SIMD: 512-bit
  bool has_AVX512F;    //  AVX512 Foundation
  bool has_AVX512CD;   //  AVX512 Conflict Detection
  bool has_AVX512PF;   //  AVX512 Prefetch
  bool has_AVX512ER;   //  AVX512 Exponential + Reciprocal
  bool has_AVX512VL;   //  AVX512 Vector Length Extensions
  bool has_AVX512BW;   //  AVX512 Byte + Word
  bool has_AVX512DQ;   //  AVX512 Doubleword + Quadword
  bool has_AVX512IFMA; //  AVX512 Integer 52-bit Fused Multiply-Add
  bool has_AVX512VBMI; //  AVX512 Vector Byte Manipulation Instructions
};

size_t page_size();
ProcessorInformation const & processor_information();
SIMDInformation const & simd_information();

bool has_mmx();
bool has_sse();
bool has_sse2();
bool has_sse3();
bool has_sse4_1();
bool has_sse4_2();
bool has_avx();
bool has_avx2();

}

////////////////////////////////////////////////////////////////////////////////
// platform
////////////////////////////////////////////////////////////////////////////////

namespace platform {

/*******************************************************************************
** page_size
*******************************************************************************/

inline size_t get_page_size() {
#if defined(CORE_LIB_OS_WINDOWS)
  SYSTEM_INFO system_information;
  GetSystemInfo(&system_information);
  return static_cast<size_t>(system_information.dwPageSize);
#elif defined(CORE_LIB_OS_LINUX)
  return static_cast<size_t>(getpagesize());
#endif
}

inline size_t page_size() {
  static size_t cached_page_size = get_page_size();
  return cached_page_size;
}

/*******************************************************************************
** processor_information
*******************************************************************************/

inline ProcessorInformation get_processor_information() {
  ProcessorInformation processor_information;
#if defined(CORE_LIB_OS_WINDOWS)
  typedef BOOL(WINAPI * LPFN_GLPI)(PSYSTEM_LOGICAL_PROCESSOR_INFORMATION,
                                   PDWORD);

  struct Local {
    // Helper Function to count set bits in the processor mask.
    static inline DWORD CountSetBits(ULONG_PTR bitMask) {
      DWORD LSHIFT = sizeof(ULONG_PTR) * 8 - 1;
      DWORD bitSetCount = 0;
      ULONG_PTR bitTest = (ULONG_PTR)1 << LSHIFT;
      DWORD i;

      for (i = 0; i <= LSHIFT; ++i) {
        bitSetCount += ((bitMask & bitTest) ? 1 : 0);
        bitTest /= 2;
      }

      return bitSetCount;
    }
  };

  LPFN_GLPI glpi;
  BOOL done = FALSE;
  PSYSTEM_LOGICAL_PROCESSOR_INFORMATION buffer = NULL;
  PSYSTEM_LOGICAL_PROCESSOR_INFORMATION ptr = NULL;
  DWORD returnLength = 0;

  DWORD byteOffset = 0;
  PCACHE_DESCRIPTOR Cache;

  glpi = (LPFN_GLPI)GetProcAddress(GetModuleHandle(TEXT("kernel32")),
                                   "GetLogicalProcessorInformation");
  CORE_LIB_ASSERT(glpi != nullptr);

  while (!done) {
    DWORD rc = glpi(buffer, &returnLength);

    if (FALSE == rc) {
      if (auto error = GetLastError() == ERROR_INSUFFICIENT_BUFFER) {
        if (buffer)
          free(buffer);

        buffer = (PSYSTEM_LOGICAL_PROCESSOR_INFORMATION)malloc(returnLength);
        CORE_LIB_ASSERT(buffer != nullptr);
      } else {
        CORE_LIB_LOG(ERROR, "GetProcessorInformation error %ld!", error);
      }
    } else {
      done = TRUE;
    }
  }

  ptr = buffer;

  while (byteOffset + sizeof(SYSTEM_LOGICAL_PROCESSOR_INFORMATION) <=
         returnLength) {
    switch (ptr->Relationship) {
      case RelationNumaNode:
        break;

      case RelationProcessorPackage:
        // Logical processors share a physical package.
        processor_information.packages++;
        break;

      case RelationProcessorCore:
        processor_information.core_count++;
        // A hyperthreaded core supplies more than one logical processor.
        processor_information.processor_count +=
            Local::CountSetBits(ptr->ProcessorMask);
        break;

      case RelationCache:
        // Cache data is in ptr->Cache, one CACHE_DESCRIPTOR structure for each cache.
        Cache = &ptr->Cache;
        processor_information.caches[Cache->Level - 1].count++;
        processor_information.caches[Cache->Level - 1].line_size =
            Cache->LineSize;
        processor_information.caches[Cache->Level - 1].size = Cache->Size;
        break;

      default:
        CORE_LIB_LOG(ERROR,
                     "Unsupported LOGICAL_PROCESSOR_RELATIONSHIP value!");
        break;
    }
    byteOffset += sizeof(SYSTEM_LOGICAL_PROCESSOR_INFORMATION);
    ptr++;
  }
#elif defined(CORE_LIB_OS_LINUX)
  struct Local {
    static inline void cpuinfo(int code, int * eax, int * ebx, int * ecx,
                               int * edx) {
      __asm__ volatile("cpuid;"
                       : "=a"(*eax), "=b"(*ebx), "=c"(*ecx), "=d"(*edx)
                       : "a"(code));
    }
  };
  int eax = 0, ebx = 0, ecx = 0, edx = 0;

  Local::cpuinfo(0x0, &eax, &ebx, &ecx, &edx);
  int identity_reg[3] = {ebx, ecx, edx};
  for (unsigned int i = 0; i < 3; i++) {
    for (unsigned int j = 0; j < 4; j++) {
      processor_information.vendor[i * 4 + j] =
          static_cast<char>(identity_reg[i] >> (j * 8));
    }
  }

  Local::cpuinfo(0x80000000, &eax, &ebx, &ecx, &edx);
  const char intel_extended = static_cast<char>(eax);
  if (intel_extended < 6) {
    // CORE_LIB_ASSERT_TEXT( 5 < intel_extended, "Cache extended feature not supported" );
    return processor_information;
  }

  Local::cpuinfo(0x80000006, &eax, &ebx, &ecx, &edx);
  const char cache_line_size = static_cast<char>(ecx); // TODO: This is L2 size!
  const int cache_size = ecx >> 16u;                   // TODO: This is L2 size!
  for (auto cache_index = 0; cache_index < 3; ++cache_index) {
    processor_information.caches[cache_index].size = cache_size << 10u;
    processor_information.caches[cache_index].line_size = cache_line_size;
  }
#endif
  return processor_information;
}

inline ProcessorInformation const & processor_information() {
  static ProcessorInformation cached_processor_information =
      get_processor_information();
  return cached_processor_information;
}

/*******************************************************************************
** simd_information
*******************************************************************************/

inline SIMDInformation get_simd_information() {
  struct Local {
    static void cpuid(int _cpu_info[4u], int _function_id) {
#if defined(CORE_LIB_COMPILER_MSVC)
      __cpuidex(_cpu_info, _function_id, 0);
#else
      __cpuid_count(_function_id, 0, _cpu_info[0u], _cpu_info[1u],
                    _cpu_info[2u], _cpu_info[3u]);
#endif
    }
  };

  SIMDInformation simd_information;
  int info[4u];

  Local::cpuid(info, 0);
  int nIds = info[0u];
  Local::cpuid(info, 0x80000000);
  unsigned nExIds = info[0u];

  if (0x00000001 <= nIds) {
    Local::cpuid(info, 0x00000001);
    simd_information.has_MMX = (info[3] & (1 << 23)) != 0;
    simd_information.has_SSE = (info[3] & (1 << 25)) != 0;
    simd_information.has_SSE2 = (info[3] & (1 << 26)) != 0;
    simd_information.has_SSE3 = (info[2] & (1 << 0)) != 0;

    simd_information.has_SSSE3 = (info[2] & (1 << 9)) != 0;
    simd_information.has_SSE41 = (info[2] & (1 << 19)) != 0;
    simd_information.has_SSE42 = (info[2] & (1 << 20)) != 0;
    simd_information.has_AES = (info[2] & (1 << 25)) != 0;

    simd_information.has_AVX = (info[2] & (1 << 28)) != 0;
    simd_information.has_FMA3 = (info[2] & (1 << 12)) != 0;

    simd_information.has_RDRAND = (info[2] & (1 << 30)) != 0;
  }
  if (0x00000007 <= nIds) {
    Local::cpuid(info, 0x00000007);
    simd_information.has_AVX2 = (info[1] & (1 << 5)) != 0;

    simd_information.has_BMI1 = (info[1] & (1 << 3)) != 0;
    simd_information.has_BMI2 = (info[1] & (1 << 8)) != 0;
    simd_information.has_ADX = (info[1] & (1 << 19)) != 0;
    simd_information.has_SHA = (info[1] & (1 << 29)) != 0;
    simd_information.has_PREFETCHWT1 = (info[2] & (1 << 0)) != 0;

    simd_information.has_AVX512F = (info[1] & (1 << 16)) != 0;
    simd_information.has_AVX512CD = (info[1] & (1 << 28)) != 0;
    simd_information.has_AVX512PF = (info[1] & (1 << 26)) != 0;
    simd_information.has_AVX512ER = (info[1] & (1 << 27)) != 0;
    simd_information.has_AVX512VL = (info[1] & (1 << 31)) != 0;
    simd_information.has_AVX512BW = (info[1] & (1 << 30)) != 0;
    simd_information.has_AVX512DQ = (info[1] & (1 << 17)) != 0;
    simd_information.has_AVX512IFMA = (info[1] & (1 << 21)) != 0;
    simd_information.has_AVX512VBMI = (info[2] & (1 << 1)) != 0;
  }
  if (0x80000001 <= nExIds) {
    Local::cpuid(info, 0x80000001);
    simd_information.has_x64 = (info[3] & (1 << 29)) != 0;
    simd_information.has_ABM = (info[2] & (1 << 5)) != 0;
    simd_information.has_SSE4a = (info[2] & (1 << 6)) != 0;
    simd_information.has_FMA4 = (info[2] & (1 << 16)) != 0;
    simd_information.has_XOP = (info[2] & (1 << 11)) != 0;
  }
  return simd_information;
}

inline SIMDInformation const & simd_information() {
  static SIMDInformation cached_simd_information = get_simd_information();
  return cached_simd_information;
}

inline bool has_mmx() { return simd_information().has_MMX; }
inline bool has_sse() { return simd_information().has_SSE; }
inline bool has_sse2() { return simd_information().has_SSE2; }
inline bool has_sse3() { return simd_information().has_SSE3; }
inline bool has_sse4_1() { return simd_information().has_SSE41; }
inline bool has_sse4_2() { return simd_information().has_SSE42; }
inline bool has_avx() { return simd_information().has_AVX; }
inline bool has_avx2() { return simd_information().has_AVX2; }

}

#endif
