#ifndef _CORE_LIB_ALLOCATORS_HH_
#define _CORE_LIB_ALLOCATORS_HH_

#include "core/memory/base.hpp" // core::memory::is_standard_aligned

////////////////////////////////////////////////////////////////////////////////
// allocator policy
////////////////////////////////////////////////////////////////////////////////

namespace allocator { namespace policy {

template<size_t BaseSize>
struct Linear;
template<size_t BaseSize, size_t Exponent>
struct Exponential;

}}

////////////////////////////////////////////////////////////////////////////////
// allocator
////////////////////////////////////////////////////////////////////////////////

namespace allocator {

struct Allocation;

class NullAllocator;
template<size_t Size>
class InlineAllocator;
class HeapAllocator;
template<size_t Alignment>
class AlignedHeapAllocator;
template<size_t Size>
class PageAllocator;
template<size_t Size>
class AlignedPageAllocator;

template<size_t Bound, class LowerAllocator, class HigherAllocator>
class IntervalAllocator;
template<class MemoryAllocator>
class ThreadLocalAllocator;

template<class T, class MemoryAllocator>
class TypeAllocator;

using DefaultMemoryAllocator = HeapAllocator;
template<typename T>
using DefaultTypeAllocator = TypeAllocator<
    T, mpl::conditional_t<core::memory::is_standard_aligned<T>::value,
                          HeapAllocator, AlignedHeapAllocator<alignof(T)>>>;
using DefaultThreadsafeAllocator = ThreadLocalAllocator<DefaultMemoryAllocator>;
template<typename T>
using DefaultThreadsafeTypeAllocator =
    TypeAllocator<T, DefaultThreadsafeAllocator>;

}

#endif
