#ifndef _CORE_LIB_QUERIES_HPP_
#define _CORE_LIB_QUERIES_HPP_

#include "core/base.h"
#include "core/containers.hpp"

#include "core/queries.hh"

#include "core/queries/detail.hpp"
#include "core/queries/lists.hpp"
#include "core/queries/chain.hpp"
#include "core/queries/stacks.hpp"

#endif
