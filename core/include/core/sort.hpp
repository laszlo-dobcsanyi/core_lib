#ifndef _CORE_LIB_SORT_HPP_
#define _CORE_LIB_SORT_HPP_

#include "core/ranges/span.hpp"

////////////////////////////////////////////////////////////////////////////////
// core detail
////////////////////////////////////////////////////////////////////////////////

namespace core { namespace detail {

struct Less {
  template<typename T>
  bool operator()(T const & _lhs, T const & _rhs) {
    return _lhs < _rhs;
  }
};

#if defined(CORE_LIB_DEBUG_SORT)
template<typename ValueType, class Predicate>
inline void check_predicate(span<ValueType> _span, Predicate _predicate) {
  for (auto current = 0u; current < _span.size() - 1u; ++current) {
    CORE_LIB_ASSERT(!_predicate(_span[current], _span[current]));
    if (_predicate(_span[current], _span[current + 1u])) {
      CORE_LIB_ASSERT(!_predicate(_span[current + 1u], _span[current]));
    } else {
      CORE_LIB_ASSERT(_predicate(_span[current + 1u], _span[current]));
    }
    // TODO: Check transitivity!
  }
}
#endif

template<typename ValueType, class Predicate>
inline int32 partition(span<ValueType> & _span, Predicate & _predicate,
                       int32 _start, int32 _end) {
  auto pivot = _start;
  auto i = _start - 1;
  auto j = _end + 1;
  for (;;) {
    do {
      ++i;
    } while (_predicate(_span[i], _span[pivot]));
    do {
      --j;
    } while (_predicate(_span[pivot], _span[j]));
    if (j <= i) {
      return j;
    }
    swap(_span[i], _span[j]);
#if defined(CORE_LIB_DEBUG_SORT)
    check_predicate(_span, _predicate);
#endif
  }
}

template<typename ValueType, class Predicate>
inline void sort_recursion(span<ValueType> & _span, Predicate & _predicate,
                           int32 _start, int32 _end) {
  if (_start < _end) {
    auto pivot = partition(_span, _predicate, _start, _end);
    sort_recursion(_span, _predicate, _start, pivot);
    sort_recursion(_span, _predicate, pivot + 1, _end);
  }
}

}}

////////////////////////////////////////////////////////////////////////////////
// core
////////////////////////////////////////////////////////////////////////////////

namespace core {

/*******************************************************************************
** is_sorted
*******************************************************************************/

template<typename ValueType, class Predicate>
inline bool is_sorted(span<ValueType> _span, Predicate _predicate) {
#if defined(CORE_LIB_DEBUG_SORT)
  check_predicate(_span, _predicate);
#endif
  for (auto current = 0u; current < _span.size() - 1u; ++current) {
    if (!_predicate(_span[current], _span[current + 1u])) {
      return false;
    }
  }
  return true;
}

template<typename ValueType>
inline bool is_sorted(span<ValueType> _span) {
  return is_sorted(_span, detail::Less());
}

/*******************************************************************************
** sort
*******************************************************************************/

template<typename ValueType, class Predicate>
inline void sort(span<ValueType> _span, Predicate _predicate) {
#if defined(CORE_LIB_DEBUG_SORT)
  check_predicate(_span, _predicate);
#endif
  detail::sort_recursion(_span, _predicate, 0,
                         static_cast<int32>(_span.size()) - 1);
#if defined(CORE_LIB_DEBUG_SORT)
  for (auto outer = 0u; outer < _span.size(); ++outer) {
    for (auto inner = outer + 1u; inner < _span.size(); ++inner) {
      CORE_LIB_ASSERT(_predicate(_span[outer], _span[inner]));
    }
  }
#endif
}

template<typename ValueType>
inline void sort(span<ValueType> _span) {
  sort(_span, detail::Less());
}

/*******************************************************************************
** find_in_sorted
*******************************************************************************/

template<typename ValueType>
inline ValueType * find_in_sorted(span<ValueType> _span, ValueType _element) {
#if defined(CORE_LIB_DEBUG_SORT)
  CORE_LIB_ASSERT(is_sorted(_span));
#endif
  if (_span.empty()) {
    return nullptr;
  }
  size_t start = 0u;
  size_t end = _span.size() - 1u;
  while (start <= end) {
    size_t middle = start + (end - start) / 2u;
    if (_span[middle] == _element) {
      return &_span[middle];
    } else if (_span[middle] < _element) {
      start = middle + 1u;
    } else {
      end = middle - 1u;
    }
  }
  return nullptr;
}

template<typename ValueType, typename ElementType, class Selector>
inline ValueType * find_in_sorted(span<ValueType> _span, ElementType _element,
                                  Selector _selector) {
#if defined(CORE_LIB_DEBUG_SORT)
  CORE_LIB_ASSERT(is_sorted(_span));
#endif
  if (_span.empty()) {
    return nullptr;
  }
  size_t start = 0u;
  size_t end = _span.size() - 1u;
  while (start <= end) {
    size_t middle = start + (end - start) / 2u;
    auto value = _selector(_span[middle]);
    if (value == _element) {
      return &_span[middle];
    } else if (value < _element) {
      start = middle + 1u;
    } else {
      end = middle - 1u;
    }
  }
  return nullptr;
}

}

#endif
