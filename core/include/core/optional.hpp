#ifndef _CORE_LIB_OPTIONAL_HPP_
#define _CORE_LIB_OPTIONAL_HPP_

#include "core/memory/uninitialized_value.hpp"
#include "core/iterator/contiguous_iterator.hpp"

////////////////////////////////////////////////////////////////////////////////
// core
////////////////////////////////////////////////////////////////////////////////

namespace core {

/*******************************************************************************
** nullopt_t
*******************************************************************************/

struct nullopt_t {
  constexpr explicit nullopt_t() = default;
};
constexpr nullopt_t nullopt;

/*******************************************************************************
** Optional
*******************************************************************************/

template<typename T>
class Optional final {
public:
  using value_type = T;
  using iterator_type = core::contiguous_iterator<T>;
  using const_iterator_type = core::contiguous_iterator<T const>;

  Optional();
  Optional(nullopt_t);
  Optional(in_place_t<void>);
  template<typename... Args>
  Optional(Args &&... _args);
  Optional(Optional && _other);
  Optional(Optional const & _other);
  Optional & operator=(Optional && _other);
  Optional & operator=(Optional const & _other);
  ~Optional();

  bool HasValue() const;
  explicit operator bool() const;

  T & Value();
  T const & Value() const;

  T * Get();
  T const * Get() const;

  T & operator*();
  T const & operator*() const;
  T * operator->();
  T const * operator->() const;

  iterator_type begin();
  const_iterator_type begin() const;
  iterator_type end();
  const_iterator_type end() const;

private:
  memory::UninitializedValue<T> storage;
  bool has_value;
};

}

////////////////////////////////////////////////////////////////////////////////
// core
////////////////////////////////////////////////////////////////////////////////

namespace core {

/*******************************************************************************
** Optional
*******************************************************************************/

template<typename T>
inline Optional<T>::Optional()
    : Optional(nullopt) {}

template<typename T>
inline Optional<T>::Optional(nullopt_t)
    : has_value(false) {}

template<typename T>
inline Optional<T>::Optional(in_place_t<void>)
    : has_value(true) {
  new (storage.Get()) T();
}

template<typename T>
template<typename... Args>
inline Optional<T>::Optional(Args &&... _args)
    : has_value(true) {
  new (storage.Get()) T(forward<Args>(_args)...);
}

template<typename T>
inline Optional<T>::Optional(Optional && _other) {
  CORE_LIB_ASSERT(this != &_other);
  if (_other.has_value) {
    new (storage.Get()) T(move(_other.storage.Value()));
  }
  has_value = _other.has_value;
}

template<typename T>
inline Optional<T>::Optional(Optional const & _other) {
  CORE_LIB_ASSERT(this != &_other);
  if (_other.has_value) {
    new (storage.Get()) T(_other.storage.Value());
  }
  has_value = _other.has_value;
}

template<typename T>
inline Optional<T> & Optional<T>::operator=(Optional && _other) {
  if (this != &_other) {
    if (_other.has_value) {
      if (has_value) {
        storage.Value().~T();
      }
      new (storage.Get()) T(move(_other.storage.Value()));
      has_value = true;
    } else {
      if (has_value) {
        storage.Value().~T();
        has_value = false;
      }
    }
  }
  return *this;
}

template<typename T>
inline Optional<T> & Optional<T>::operator=(Optional const & _other) {
  if (this != &_other) {
    if (has_value) {
      if (_other.has_value) {
        storage.Value() = _other.storage.Value();
      } else {
        storage.Value().~T();
      }
    } else if (_other.has_value) {
      storage.Value() = _other.storage.Value();
    }
    has_value = _other.has_value;
  }
  return *this;
}

template<typename T>
inline Optional<T>::~Optional() {
  if (HasValue()) {
    storage.Value().~T();
  }
}

template<typename T>
inline bool Optional<T>::HasValue() const {
  return has_value;
}

template<typename T>
inline Optional<T>::operator bool() const {
  return HasValue();
}

template<typename T>
inline T & Optional<T>::Value() {
  CORE_LIB_ASSERT(HasValue());
  return storage.Value();
}

template<typename T>
inline T const & Optional<T>::Value() const {
  CORE_LIB_ASSERT(HasValue());
  return storage.Value();
}

template<typename T>
inline T * Optional<T>::Get() {
  if (HasValue()) {
    return storage.Get();
  }
  return nullptr;
}

template<typename T>
inline T const * Optional<T>::Get() const {
  if (HasValue()) {
    return storage.Get();
  }
  return nullptr;
}

template<typename T>
inline T & Optional<T>::operator*() {
  return Value();
}

template<typename T>
inline T const & Optional<T>::operator*() const {
  return Value();
}

template<typename T>
inline T * Optional<T>::operator->() {
  CORE_LIB_ASSERT(HasValue());
  return storage.Get();
}

template<typename T>
inline T const * Optional<T>::operator->() const {
  CORE_LIB_ASSERT(HasValue());
  return storage.Get();
}

template<typename T>
inline typename Optional<T>::iterator_type Optional<T>::begin() {
  if (HasValue()) {
    return iterator_type(Get());
  }
  return end();
}

template<typename T>
inline typename Optional<T>::const_iterator_type Optional<T>::begin() const {
  if (HasValue()) {
    return const_iterator_type(Get());
  }
  return end();
}

template<typename T>
inline typename Optional<T>::iterator_type Optional<T>::end() {
  return iterator_type(Get() + 1u);
}

template<typename T>
inline typename Optional<T>::const_iterator_type Optional<T>::end() const {
  return const_iterator_type(Get() + 1u);
}

}

#endif
