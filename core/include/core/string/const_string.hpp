#ifndef _CORE_LIB_STRING_CONST_STRING_HPP_
#define _CORE_LIB_STRING_CONST_STRING_HPP_

#ifndef CORE_LIB_STRING
#  error
#endif

#include "core/memory/unique_ptr.hpp"

////////////////////////////////////////////////////////////////////////////////
// string storage
////////////////////////////////////////////////////////////////////////////////

namespace string { namespace storage {

/*******************************************************************************
** UniqueStorage
*******************************************************************************/

class UniqueStorage {
public:
  UniqueStorage() = default;
  UniqueStorage(char const * _cstring);
  UniqueStorage(char const * _cstring, size_t _length);
  UniqueStorage(size_t _size);
  UniqueStorage(UniqueStorage &&) = default;
  UniqueStorage(UniqueStorage const & _other);
  UniqueStorage & operator=(UniqueStorage &&) = default;
  UniqueStorage & operator=(UniqueStorage const & _other);
  ~UniqueStorage() = default;

  bool IsAllocated() const { return !!memory; }

  char * Get() { return memory.Get(); }
  char const * Get() const { return memory.Get(); }

private:
  core::UniquePtr<char[]> memory;

  void Allocate(size_t _size);
  void CopyString(char const * _cstring, size_t _length);
};

}}

////////////////////////////////////////////////////////////////////////////////
// string
////////////////////////////////////////////////////////////////////////////////

namespace string {

/*******************************************************************************
** const_string
*******************************************************************************/

template<class Storage>
class const_string final {
public:
  using storage_type = Storage;

  const_string();
  const_string(char const * _cstring);
  const_string(char const * _cstring, size_t _length);
  const_string(Storage && _storage);
  const_string(const_string && _other);
  const_string(const_string const & _other);
  const_string & operator=(const_string && _other);
  const_string & operator=(const_string const & _other);
  ~const_string() = default;

  size_t Length() const { return length; }

  explicit operator bool() const { return !IsEmpty(); }
  bool IsEmpty() const { return *cstring == '\0'; }

  char const * Cstr() const { return cstring; }

private:
  Storage storage;
  char const * cstring;
  size_t length;
};

}

////////////////////////////////////////////////////////////////////////////////
// string storage
////////////////////////////////////////////////////////////////////////////////

namespace string { namespace storage {

/*******************************************************************************
** UniqueStorage
*******************************************************************************/

inline UniqueStorage::UniqueStorage(char const * _cstring)
    : UniqueStorage(_cstring, cstring::strlen(_cstring)) {}

inline UniqueStorage::UniqueStorage(char const * _cstring, size_t _length) {
  CORE_LIB_ASSERT(*(_cstring + _length) == '\0');
  CORE_LIB_ASSERT(cstring::strlen(_cstring) == _length);
  Allocate(_length);
  CopyString(_cstring, _length);
}

inline UniqueStorage::UniqueStorage(size_t _length) {
  Allocate(_length);
  memory[_length] = '\0';
}

inline UniqueStorage::UniqueStorage(UniqueStorage const & _other) {
  if (_other.IsAllocated()) {
    auto const length = cstring::strlen(_other.memory.Get());
    Allocate(length);
    CopyString(_other.memory.Get(), length);
  }
}

inline UniqueStorage & UniqueStorage::operator=(UniqueStorage const & _other) {
  if (this != &_other) {
    memory.Reset();
    if (_other.IsAllocated()) {
      auto const length = cstring::strlen(_other.memory.Get());
      Allocate(length);
      CopyString(_other.memory.Get(), length);
    }
  }
  return *this;
}

inline void UniqueStorage::Allocate(size_t _length) {
  auto const allocation_size = core::memory::align(_length + 1u, 4u);
  CORE_LIB_ASSUME_TRUE(allocation_size > 0u);
  uintptr_t allocation = core::memory::alloc(allocation_size);
  CORE_LIB_ASSERT(allocation != 0u);
  CORE_LIB_ASSERT(!memory);
  new (&memory) core::UniquePtr<char[]>(reinterpret_cast<char *>(allocation));
#if defined(CORE_LIB_CONFIGURATION_DEBUG)
  ::memset(memory.Get(), 0xCD, allocation_size);
#endif
}

inline void UniqueStorage::CopyString(char const * _cstring, size_t _length) {
  CORE_LIB_ASSERT(_cstring[_length] == '\0');
  ::memcpy(memory.Get(), _cstring, _length + 1u);
  CORE_LIB_ASSERT(memory[_length] == '\0');
}

}}

////////////////////////////////////////////////////////////////////////////////
// string
////////////////////////////////////////////////////////////////////////////////

namespace string {

/*******************************************************************************
** const_string
*******************************************************************************/

template<class Storage>
inline const_string<Storage>::const_string()
    : const_string("\0", 0u) {}

template<class Storage>
inline const_string<Storage>::const_string(char const * _cstring)
    : const_string(_cstring, cstring::strlen(_cstring)) {}

template<class Storage>
inline const_string<Storage>::const_string(char const * _cstring,
                                           size_t _length)
    : storage(_cstring, _length)
    , cstring(storage.Get())
    , length(_length) {
  CORE_LIB_ASSERT(cstring[length] == '\0');
}

template<class Storage>
inline const_string<Storage>::const_string(Storage && _storage) {
  new (&storage) Storage(move(_storage));
  if (storage.IsAllocated()) {
    cstring = storage.Get();
    length = cstring::strlen(cstring);
  } else {
    cstring = "\0";
    length = 0u;
  }
}

template<class Storage>
inline const_string<Storage>::const_string(const_string && _other) {
  CORE_LIB_ASSERT(this != &_other);
  new (&storage) Storage(move(_other.storage));
  CORE_LIB_ASSERT(!_other.storage.IsAllocated());
  if (storage.IsAllocated()) {
    cstring = storage.Get();
    CORE_LIB_ASSERT(cstring != nullptr);
    length = _other.length;
  } else {
    cstring = "\0";
    length = 0u;
  }
  CORE_LIB_ASSERT(!_other.storage.IsAllocated());
  _other.cstring = "\0";
  _other.length = 0u;
}

template<class Storage>
inline const_string<Storage>::const_string(const_string const & _other) {
  if (this != &_other) {
    new (&storage) Storage(_other.storage);
    if (storage.IsAllocated()) {
      cstring = storage.Get();
      CORE_LIB_ASSERT(cstring != nullptr);
    } else {
      CORE_LIB_ASSERT(_other.cstring != nullptr);
      CORE_LIB_ASSERT(*_other.cstring == '\0');
      CORE_LIB_ASSERT(_other.length == 0u);
      cstring = "\0";
    }
    length = _other.length;
  }
}

template<class Storage>
inline const_string<Storage> &
const_string<Storage>::operator=(const_string && _other) {
  CORE_LIB_ASSERT(this != &_other);
  storage = move(_other.storage);
  CORE_LIB_ASSERT(!_other.storage.IsAllocated());
  if (storage.IsAllocated()) {
    cstring = storage.Get();
    CORE_LIB_ASSERT(cstring != nullptr);
    length = _other.length;
  } else {
    CORE_LIB_ASSERT(_other.cstring != nullptr);
    CORE_LIB_ASSERT(*_other.cstring == '\0');
    CORE_LIB_ASSERT(_other.length == 0u);
    cstring = "\0";
    length = 0u;
  }
  CORE_LIB_ASSERT(!_other.storage.IsAllocated());
  _other.cstring = "\0";
  _other.length = 0u;
  return *this;
}

template<class Storage>
inline const_string<Storage> &
const_string<Storage>::operator=(const_string const & _other) {
  if (this != &_other) {
    storage = _other.storage;
    if (storage.IsAllocated()) {
      cstring = storage.Get();
      CORE_LIB_ASSERT(cstring != nullptr);
    } else {
      CORE_LIB_ASSERT(_other.cstring != nullptr);
      CORE_LIB_ASSERT(*_other.cstring == '\0');
      CORE_LIB_ASSERT(_other.length == 0u);
      cstring = "\0";
    }
    length = _other.length;
  }
  return *this;
}

template<typename Storage>
inline bool operator==(const_string<Storage> const & _lhs,
                       const_string<Storage> const & _rhs) {
  if (_lhs.Length() != _rhs.Length())
    return false;
  return _lhs == cstr(_rhs);
}

template<typename Storage>
inline bool operator!=(const_string<Storage> const & _lhs,
                       const_string<Storage> const & _rhs) {
  return !(_lhs == _rhs);
}

template<typename Storage>
inline bool operator<(const_string<Storage> const & _lhs,
                      const_string<Storage> const & _rhs) {
  if (strlen(_lhs) == strlen(_rhs))
    return _lhs < cstr(_rhs);
  return strlen(_lhs) < strlen(_rhs);
}

template<typename Storage>
inline bool operator<=(const_string<Storage> const & _lhs,
                       const_string<Storage> const & _rhs) {
  if (strlen(_lhs) == strlen(_rhs))
    return _lhs <= cstr(_rhs);
  return strlen(_lhs) < strlen(_rhs);
}

template<typename Storage>
inline bool operator>(const_string<Storage> const & _lhs,
                      const_string<Storage> const & _rhs) {
  if (strlen(_lhs) == strlen(_rhs))
    return _lhs > cstr(_rhs);
  return strlen(_lhs) > strlen(_rhs);
}

template<typename Storage>
inline bool operator>=(const_string<Storage> const & _lhs,
                       const_string<Storage> const & _rhs) {
  if (strlen(_lhs) == strlen(_rhs))
    return _lhs >= cstr(_rhs);
  return strlen(_lhs) > strlen(_rhs);
}

template<typename Storage>
inline bool operator==(const_string<Storage> const & _lhs, char const * _rhs) {
  return ::strcmp(cstr(_lhs), _rhs) == 0;
}

template<typename Storage>
inline bool operator!=(const_string<Storage> const & _lhs, char const * _rhs) {
  return !(_lhs == _rhs);
}

template<typename Storage>
inline bool operator<(const_string<Storage> const & _lhs, char const * _rhs) {
  return ::strcmp(cstr(_lhs), _rhs) < 0;
}

template<typename Storage>
inline bool operator<=(const_string<Storage> const & _lhs, char const * _rhs) {
  return ::strcmp(cstr(_lhs), _rhs) <= 0;
}

template<typename Storage>
inline bool operator>(const_string<Storage> const & _lhs, char const * _rhs) {
  return ::strcmp(cstr(_lhs), _rhs) > 0;
}

template<typename Storage>
inline bool operator>=(const_string<Storage> const & _lhs, char const * _rhs) {
  return ::strcmp(cstr(_lhs), _rhs) >= 0;
}

template<class Storage>
inline char const * cstr(const_string<Storage> const & _const_string) {
  return _const_string.Cstr();
}

template<class Storage>
inline size_t strlen(const_string<Storage> const & _const_string) {
  return _const_string.Length();
}

}

using const_string = string::const_string<string::storage::UniqueStorage>;

#endif
