#ifndef _CORE_LIB_STRING_PARSER_HPP_
#define _CORE_LIB_STRING_PARSER_HPP_

#ifndef CORE_LIB_STRING
#  error
#endif

////////////////////////////////////////////////////////////////////////////////
// parser
////////////////////////////////////////////////////////////////////////////////

namespace parser {

inline bool is_whitespace(char _c) { return _c == ' ' || _c == '\t'; }

inline size_t parse_end_of_file(char const * _cstring) {
  return (*_cstring == '\0') ? 1 : 0;
}

inline size_t parse_line_end_windows(char const * _cstring) {
  return (*_cstring == '\r' && *(_cstring + 1) == '\n') ? 2 : 0;
}

inline size_t parse_line_end_unix(char const * _cstring) {
  return (*_cstring == '\n') ? 1 : 0;
}

inline size_t parse_line_end(char const * _cstring) {
  size_t parsed = 0;
  if ((parsed = parse_line_end_unix(_cstring))) {
    return parsed;
  } else if ((parsed = parse_line_end_windows(_cstring))) {
    return parsed;
  }
  return parsed;
}

inline size_t parse_whitespaces(char const * _cstring) {
  size_t parsed = 0;
  while (is_whitespace(*(_cstring++))) {
    ++parsed;
  }
  return parsed;
}

inline size_t parse_token(char const * _cstring) {
  size_t parsed = 0;
  for (;;) {
    if (is_whitespace(*_cstring)) {
      return parsed;
    } else if (0 < parse_line_end(_cstring)) {
      return parsed;
    } else if (0 < parse_end_of_file(_cstring)) {
      return parsed;
    }
    ++parsed;
    ++_cstring;
  }
}

inline size_t parse_line(char const * _cstring) {
  size_t parsed = 0;
  size_t sub_parsed;
  for (;;) {
    if ((sub_parsed = parse_line_end(_cstring))) {
      return parsed;
    } else if ((sub_parsed = parse_end_of_file(_cstring))) {
      return parsed;
    }
    ++parsed;
    ++_cstring;
  }
}

inline size_t parse_comment(char const * _cstring) {
  if (*_cstring == '#') {
    return parse_line(_cstring + 1) + 1;
  }
  return 0;
}

template<typename Floating>
inline size_t parse_float(char const * _cstring, Floating & _value) {
  size_t parsed = parse_whitespaces(_cstring);
  _cstring += parsed;
  return parsed + cstring::parse_floating(_cstring, _value);
}

template<typename Integral>
inline size_t parse_decimal(char const * _cstring, Integral & _value) {
  size_t parsed = parse_whitespaces(_cstring);
  _cstring += parsed;
  return parsed + cstring::parse_decimal(_cstring, _value);
}

}

#endif
