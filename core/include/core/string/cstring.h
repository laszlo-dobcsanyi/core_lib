#ifndef _CORE_LIB_CSTRING_CSTRING_H_
#define _CORE_LIB_CSTRING_CSTRING_H_

#include "core/base.h"

extern "C" {

#include <emmintrin.h>
#include <mmintrin.h>
}

// TODO: Rework this file entirely!

#define CORE_LIB_STD_CSTRING

////////////////////////////////////////////////////////////////////////////////
// cstring
////////////////////////////////////////////////////////////////////////////////

namespace cstring {

/*******************************************************************************
** constexpr_cstring
*******************************************************************************/

class constexpr_cstring final {
public:
  template<size_t N>
  constexpr constexpr_cstring(const char (&_cstring)[N])
      : cstring(_cstring)
      , size(N - 1u) {}

  constexpr operator const char *() const { return cstring; }

  const char * cstring;
  const size_t size;
};

/*******************************************************************************
** strlen
*******************************************************************************/

#if defined(CORE_LIB_STD_CSTRING)
inline size_t strlen(char const * _cstring) { return ::strlen(_cstring); }
#else
// TODO: Need padded strings for this!
inline size_t strlen(char const * _cstring) {
  char const * dbg = _cstring;
  size_t length = 0u;
  static const intptr_t alignment_mask = mpl::full_bit_field<4u>::value;
  while ((reinterpret_cast<intptr_t>(_cstring) & alignment_mask) != 0) {
    if (*(_cstring++) == 0) {
      return length;
    }
    ++length;
  }
  __m128i xmm0 = _mm_setzero_si128();
  __m128i xmm1;
  int mask = 0;
  for (;;) {
    xmm1 = _mm_load_si128(reinterpret_cast<__m128i const *>(_cstring));
    xmm1 = _mm_cmpeq_epi8(xmm1, xmm0);
    if ((mask = _mm_movemask_epi8(xmm1)) != 0) {
// TODO: Move this functionality somehow to bits.h
#  if defined(CORE_LIB_COMPILER_MSVC)
      unsigned long index;
      _BitScanForward(&index, mask);
      length += static_cast<size_t>(index);
#  elif defined(CORE_LIB_COMPILER_GCC) || defined(CORE_LIB_COMPILER_CLANG)
      length += __builtin_ctz(mask);
#  endif
      break;
    }
    _cstring += sizeof(__m128i);
    length += sizeof(__m128i);
  }
  return length;
}
#endif

/*******************************************************************************
** strcpy
*******************************************************************************/

#if defined(CORE_LIB_STD_CSTRING)
inline char * strcpy(char * _destination, char const * _source) {
  CORE_LIB_MSVC_WARNING_PUSH
  CORE_LIB_MSVC_WARNING_DISABLE(4996)
  return ::strcpy(_destination, _source);
  CORE_LIB_MSVC_WARNING_POP
}
#else
#  error
#endif

/*******************************************************************************
** strcmp
*******************************************************************************/

#if defined(CORE_LIB_STD_CSTRING)
inline int strcmp(char const * _cstring1, char const * _cstring2) {
  return ::strcmp(_cstring1, _cstring2);
}
#else
#  error
#endif

/*******************************************************************************
** parse_decimal
*******************************************************************************/

#if defined(CORE_LIB_STD_CSTRING) && 0
#  error
#else
inline bool is_digit(uint8 _c) { return _c <= 9; }
inline uint8 digit_value(char _c) { return static_cast<uint8>(_c - '0'); }

inline size_t parse_decimal(char const * const _cstring, int8 & _value) {
  CORE_LIB_UNIMPLEMENTED;
  return 0u;
}
inline size_t parse_decimal(char const * const _cstring, uint8 & _value) {
  char const * cstring = _cstring;
  _value = 0;
  uint8 digit;
  while (is_digit(digit = digit_value(*(cstring++)))) {
    _value = _value * 10 + digit;
  }
  return cstring - _cstring - 1;
}
inline size_t parse_decimal(char const * const _cstring, int16 & _value) {
  CORE_LIB_UNIMPLEMENTED;
  return 0u;
}
inline size_t parse_decimal(char const * const _cstring, uint16 & _value) {
  char const * cstring = _cstring;
  _value = 0;
  uint8 digit;
  while (is_digit(digit = digit_value(*(cstring++)))) {
    _value = _value * 10 + digit;
  }
  return cstring - _cstring - 1;
}
inline size_t parse_decimal(char const * const _cstring, int32 & _value) {
  char const * cstring = _cstring;
  bool negative = false;
  if (*cstring == '-') {
    negative = true;
    ++cstring;
  }
  _value = 0;
  uint8 digit;
  while (is_digit(digit = digit_value(*(cstring++)))) {
    _value = _value * 10 + digit;
  }
  if (negative) {
    _value = -_value;
  }
  return cstring - _cstring - 1;
}
inline size_t parse_decimal(char const * const _cstring, uint32 & _value) {
  char const * cstring = _cstring;
  _value = 0;
  uint8 digit;
  while (is_digit(digit = digit_value(*(cstring++)))) {
    _value = _value * 10 + digit;
  }
  return cstring - _cstring - 1;
}
inline size_t parse_decimal(char const * const _cstring, int64 & _value) {
  char const * cstring = _cstring;
  bool negative = false;
  if (*cstring == '-') {
    negative = true;
    ++cstring;
  }
  _value = 0;
  uint8 digit;
  while (is_digit(digit = digit_value(*(cstring++)))) {
    _value = _value * 10 + digit;
  }
  if (negative) {
    _value = -_value;
  }
  return cstring - _cstring - 1;
}
inline size_t parse_decimal(char const * const _cstring, uint64 & _value) {
  char const * cstring = _cstring;
  _value = 0;
  uint8 digit;
  while (is_digit(digit = digit_value(*(cstring++)))) {
    _value = _value * 10 + digit;
  }
  return cstring - _cstring - 1;
}
#endif

/*******************************************************************************
** parse_floating
*******************************************************************************/

#if defined(CORE_LIB_STD_CSTRING) && 0
#  error
#else
inline size_t parse_floating(char const * const _cstring, float & _value) {
  static const size_t fraction_digit_count = 11;
  static const float fraction_multipliers[fraction_digit_count] = {
      0.f,      1.f,       10.f,       100.f,       1000.f,      10000.f,
      100000.f, 1000000.f, 10000000.f, 100000000.f, 1000000000.f};
  static const float fraction_dividers[fraction_digit_count] = {
      0.f,       0.1f,       0.01f,       0.001f,       0.0001f,      0.00001f,
      0.000001f, 0.0000001f, 0.00000001f, 0.000000001f, 0.0000000001f};

  char const * cstring = _cstring;
  bool negative = false;
  if (*cstring == '-') {
    negative = true;
    ++cstring;
  }
  uint32 integral;
  uint32 fraction;
  uint32 exponential;
  size_t parsed_integral = parse_decimal(cstring, integral);
  cstring += parsed_integral;
  _value = static_cast<float>(integral);
  // CORE_LIB_LOG( VERBOSE, "Integral: %u\n", integral );
  if (*cstring == '.') {
    ++cstring;
    size_t parsed_fraction = parse_decimal(cstring, fraction);
    cstring += parsed_fraction;
    // CORE_LIB_LOG( VERBOSE, "Fraction: %u\n", fraction );
    CORE_LIB_ASSERT(parsed_fraction < fraction_digit_count);
    _value += static_cast<float>(fraction) * fraction_dividers[parsed_fraction];
    if (*cstring == 'e' || *cstring == 'E') {
      ++cstring;
      bool negative_exponential = false;
      if (*cstring == '-') {
        negative_exponential = true;
        ++cstring;
      }
      size_t parsed_exponential = parse_decimal(cstring, exponential);
      cstring += parsed_exponential;
      // CORE_LIB_LOG( VERBOSE, "Exponential: %u\n", exponential );
      CORE_LIB_ASSERT(exponential < fraction_digit_count);
      if (negative_exponential) {
        _value *= fraction_dividers[exponential];
      } else {
        _value *= fraction_multipliers[exponential];
      }
    }
  }
  if (negative) {
    _value = -_value;
  }
  return reinterpret_cast<uintptr_t>(cstring) -
         reinterpret_cast<uintptr_t>(_cstring);
}
inline size_t parse_floating(char const * const _cstring, double & _double) {
  CORE_LIB_UNIMPLEMENTED;
  return 0u;
}
#endif

/*******************************************************************************
** format_decimal
*******************************************************************************/

#if defined(CORE_LIB_STD_CSTRING)
inline int format_decimal(char * _cstring, int8 _value) {
  CORE_LIB_MSVC_WARNING_PUSH
  CORE_LIB_MSVC_WARNING_DISABLE(4996)
  return sprintf(_cstring, "%" PRIi8, _value);
  CORE_LIB_MSVC_WARNING_POP
}
inline int format_decimal(char * _cstring, uint8 _value) {
  CORE_LIB_MSVC_WARNING_PUSH
  CORE_LIB_MSVC_WARNING_DISABLE(4996)
  return sprintf(_cstring, "%" PRIu8, _value);
  CORE_LIB_MSVC_WARNING_POP
}
inline int format_decimal(char * _cstring, int16 _value) {
  CORE_LIB_MSVC_WARNING_PUSH
  CORE_LIB_MSVC_WARNING_DISABLE(4996)
  return sprintf(_cstring, "%" PRIi16, _value);
  CORE_LIB_MSVC_WARNING_POP
}
inline int format_decimal(char * _cstring, uint16 _value) {
  CORE_LIB_MSVC_WARNING_PUSH
  CORE_LIB_MSVC_WARNING_DISABLE(4996)
  return sprintf(_cstring, "%" PRIu8, _value);
  CORE_LIB_MSVC_WARNING_POP
}
inline int format_decimal(char * _cstring, int32 _value) {
  CORE_LIB_MSVC_WARNING_PUSH
  CORE_LIB_MSVC_WARNING_DISABLE(4996)
  return sprintf(_cstring, "%" PRIi32, _value);
  CORE_LIB_MSVC_WARNING_POP
}
inline int format_decimal(char * _cstring, uint32 _value) {
  CORE_LIB_MSVC_WARNING_PUSH
  CORE_LIB_MSVC_WARNING_DISABLE(4996)
  return sprintf(_cstring, "%" PRIu32, _value);
  CORE_LIB_MSVC_WARNING_POP
}
inline int format_decimal(char * _cstring, int64 _value) {
  CORE_LIB_MSVC_WARNING_PUSH
  CORE_LIB_MSVC_WARNING_DISABLE(4996)
  return sprintf(_cstring, "%" PRIi64, _value);
  CORE_LIB_MSVC_WARNING_POP
}
inline int format_decimal(char * _cstring, uint64 _value) {
  CORE_LIB_MSVC_WARNING_PUSH
  CORE_LIB_MSVC_WARNING_DISABLE(4996)
  return sprintf(_cstring, "%" PRIu64, _value);
  CORE_LIB_MSVC_WARNING_POP
}
#else
int format_decimal(char * _cstring, int8 _value, char * _cstring);
int format_decimal(char * _cstring, uint8 _value, char * _cstring);
int format_decimal(char * _cstring, int16 _value, char * _cstring);
int format_decimal(char * _cstring, uint16 _value, char * _cstring);
int format_decimal(char * _cstring, int32 _value, char * _cstring);
int format_decimal(char * _cstring, uint32 _value, char * _cstring);
int format_decimal(char * _cstring, int64 _value, char * _cstring);
int format_decimal(char * _cstring, uint64 _value, char * _cstring);
#endif

}

#endif
