#ifndef _CORE_LIB_STRING_STREAM_HPP_
#define _CORE_LIB_STRING_STREAM_HPP_

#ifndef CORE_LIB_STRING
#  error
#endif

#define STREAM_LOG(...) // CORE_LIB_LOG( LOG, __VA_ARGS__ )

////////////////////////////////////////////////////////////////////////////////
// string detail
////////////////////////////////////////////////////////////////////////////////

namespace string { namespace detail {

/*******************************************************************************
** Stream
*******************************************************************************/

template<class MemoryAllocator, class AllocationPolicy>
class Stream {
public:
  template<typename... Args>
  Stream(Args &&... _args);
  Stream(Stream &&) = default;
  Stream(Stream const &) = delete;
  Stream & operator=(Stream &&) = default;
  Stream & operator=(Stream const &) = delete;
  ~Stream();

  void Write(int8 _value);
  void Write(uint8 _value);
  void Write(int16 _value);
  void Write(uint16 _value);
  void Write(int32 _value);
  void Write(uint32 _value);
  void Write(int64 _value);
  void Write(uint64 _value);

  void Write(char const * _cstring, size_t _length);
  template<size_t Size>
  void Write(char const (&_cstring)[Size]);
  void Write(char const * _cstring);
  template<class Storage>
  void Append(const_string<Storage> const & _const_string);

  void Clear();

  template<typename T>
  Stream & operator<<(T _value);
  template<class Storage>
  Stream & operator<<(const_string<Storage> const & _const_string);
  template<class Storage>
  void operator>>(const_string<Storage> & _const_string);

  ::const_string Get() const;

private:
  MemoryAllocator memory_allocator;
  allocator::Allocation allocation;
  size_t length = 0u;

  char * ExpandFor(size_t _size);
  void ExtendWith(int _size);
  char const * Raw() const;
};

}}

////////////////////////////////////////////////////////////////////////////////
// l implementation
////////////////////////////////////////////////////////////////////////////////

namespace string { namespace detail {

template<class MemoryAllocator, class AllocationPolicy>
template<typename... Args>
inline Stream<MemoryAllocator, AllocationPolicy>::Stream(Args &&... _args)
    : memory_allocator(forward<Args>(_args)...) {}

template<class MemoryAllocator, class AllocationPolicy>
inline Stream<MemoryAllocator, AllocationPolicy>::~Stream() {
  if (allocation) {
    memory_allocator.Deallocate(move(allocation));
  }
}
template<class MemoryAllocator, class AllocationPolicy>
inline void Stream<MemoryAllocator, AllocationPolicy>::Write(int8 _value) {
  ExtendWith(cstring::format_decimal(ExpandFor(4u), _value));
}

template<class MemoryAllocator, class AllocationPolicy>
inline void Stream<MemoryAllocator, AllocationPolicy>::Write(uint8 _value) {
  ExtendWith(cstring::format_decimal(ExpandFor(3u), _value));
}

template<class MemoryAllocator, class AllocationPolicy>
inline void Stream<MemoryAllocator, AllocationPolicy>::Write(int16 _value) {
  ExtendWith(cstring::format_decimal(ExpandFor(6u), _value));
}

template<class MemoryAllocator, class AllocationPolicy>
inline void Stream<MemoryAllocator, AllocationPolicy>::Write(uint16 _value) {
  ExtendWith(cstring::format_decimal(ExpandFor(5u), _value));
}

template<class MemoryAllocator, class AllocationPolicy>
inline void Stream<MemoryAllocator, AllocationPolicy>::Write(int32 _value) {
  ExtendWith(cstring::format_decimal(ExpandFor(11u), _value));
}

template<class MemoryAllocator, class AllocationPolicy>
inline void Stream<MemoryAllocator, AllocationPolicy>::Write(uint32 _value) {
  ExtendWith(cstring::format_decimal(ExpandFor(10u), _value));
}

template<class MemoryAllocator, class AllocationPolicy>
inline void Stream<MemoryAllocator, AllocationPolicy>::Write(int64 _value) {
  ExtendWith(cstring::format_decimal(ExpandFor(21u), _value));
}

template<class MemoryAllocator, class AllocationPolicy>
inline void Stream<MemoryAllocator, AllocationPolicy>::Write(uint64 _value) {
  ExtendWith(cstring::format_decimal(ExpandFor(20u), _value));
}

template<class MemoryAllocator, class AllocationPolicy>
inline void
Stream<MemoryAllocator, AllocationPolicy>::Write(char const * _cstring,
                                                 size_t _length) {
  ExpandFor(_length + 1u);
  ::memcpy(allocation.As<char *>() + length, _cstring, _length + 1u);
  ExtendWith(static_cast<int>(_length));
}

template<class MemoryAllocator, class AllocationPolicy>
template<size_t Size>
inline void
Stream<MemoryAllocator, AllocationPolicy>::Write(char const (&_cstring)[Size]) {
  Write(_cstring, Size);
}

template<class MemoryAllocator, class AllocationPolicy>
inline void
Stream<MemoryAllocator, AllocationPolicy>::Write(char const * _cstring) {
  Write(_cstring, cstring::strlen(_cstring));
}

template<class MemoryAllocator, class AllocationPolicy>
template<class Storage>
inline void Stream<MemoryAllocator, AllocationPolicy>::Append(
    const_string<Storage> const & _const_string) {
  Write(cstr(_const_string), strlen(_const_string));
}

template<class MemoryAllocator, class AllocationPolicy>
inline void Stream<MemoryAllocator, AllocationPolicy>::Clear() {
  length = 0u;
}

template<class MemoryAllocator, class AllocationPolicy>
template<typename T>
inline Stream<MemoryAllocator, AllocationPolicy> &
Stream<MemoryAllocator, AllocationPolicy>::operator<<(T _value) {
  Write(_value);
  return *this;
}

template<class MemoryAllocator, class AllocationPolicy>
template<class Storage>
inline Stream<MemoryAllocator, AllocationPolicy> &
Stream<MemoryAllocator, AllocationPolicy>::operator<<(
    const_string<Storage> const & _const_string) {
  Append(_const_string);
  return *this;
}

template<class MemoryAllocator, class AllocationPolicy>
template<class Storage>
inline void Stream<MemoryAllocator, AllocationPolicy>::operator>>(
    const_string<Storage> & _const_string) {
  _const_string = const_string<Storage>(Raw(), length);
}

template<class MemoryAllocator, class AllocationPolicy>
inline ::const_string Stream<MemoryAllocator, AllocationPolicy>::Get() const {
  return ::const_string(Raw(), length);
}

template<class MemoryAllocator, class AllocationPolicy>
inline char const * Stream<MemoryAllocator, AllocationPolicy>::Raw() const {
  if (length != 0u) {
    return allocation.As<char const *>();
  }
  return "\0";
}

template<class MemoryAllocator, class AllocationPolicy>
inline char *
Stream<MemoryAllocator, AllocationPolicy>::ExpandFor(size_t _size) {
  const size_t new_length = length + _size;
  STREAM_LOG("Expanding for: %zu\n", new_length);
  if (allocation.Size() < new_length) {
    size_t new_size = allocation.Size();
    while (new_size < new_length) {
      new_size = AllocationPolicy::Calculate(new_size);
      STREAM_LOG("New allocation size: %zu\n", new_size);
    }
    allocator::Allocation new_allocation = memory_allocator.Allocate(new_size);
    CORE_LIB_ASSERT(new_allocation);
    if (length != 0u) {
      // TODO: memcpy from cstring/bits?
      ::memcpy(new_allocation.template As<void *>(),
               allocation.template As<void const *>(), length);
      memory_allocator.Deallocate(move(allocation));
    }
    allocation = move(new_allocation);
  }
  return allocation.As<char *>() + length;
}

template<class MemoryAllocator, class AllocationPolicy>
inline void Stream<MemoryAllocator, AllocationPolicy>::ExtendWith(int _size) {
  STREAM_LOG("Extending with: %i\n", _size);
  if (0 < _size) {
    CORE_LIB_ASSERT(length + _size < allocation.Size());
    length += static_cast<size_t>(_size);
    STREAM_LOG("New length: %zu\n", length);
  }
}

}}

#undef STREAM_LOG

#endif
