#ifndef _CORE_LIB_LIBRARY_HPP_
#define _CORE_LIB_LIBRARY_HPP_

#include "core/base.h"
#include "core/string.hpp"

#if defined(CORE_LIB_OS_WINDOWS)
#elif defined(CORE_LIB_OS_LINUX)
#  include <dlfcn.h>
#endif

#define LOG_LIBRARY(__severity__, ...) CORE_LIB_LOG(__severity__, __VA_ARGS__)

////////////////////////////////////////////////////////////////////////////////
// core
////////////////////////////////////////////////////////////////////////////////

namespace core {

template<class F, typename R, typename Args>
class DynamicFunction;

#if defined(CORE_LIB_OS_WINDOWS)
using LibraryHandleType = HMODULE;
static const LibraryHandleType InvalidLibraryHandle = nullptr;
#elif defined(CORE_LIB_OS_LINUX)
using LibraryHandleType = void *;
static constexpr LibraryHandleType InvalidLibraryHandle = nullptr;
#endif
using LibraryHandle = Value<LibraryHandleType, InvalidLibraryHandle>;

/*******************************************************************************
** Library
*******************************************************************************/

class Library final {
public:
  template<class F, typename R, typename Args>
  friend class DynamicFunction;

  Library() = default;
  Library(Library &&) = default;
  Library(Library const &) = delete;
  Library & operator=(Library &&) = default;
  Library & operator=(Library const &) = delete;
  ~Library() {
    if (IsLoaded()) {
      Unload();
    }
  }

  bool IsLoaded() const { return library != InvalidLibraryHandle; }
  explicit operator bool() const { return IsLoaded(); }

  bool Load(const_string const & _file_name) {
    CORE_LIB_ASSERT(!IsLoaded());
#if defined(CORE_LIB_OS_WINDOWS)
    library = ::LoadLibrary(cstr(_file_name));
    if (!IsLoaded()) {
      LOG_LIBRARY(WARNING, "Error while loading: %ld!\n", GetLastError());
      return false;
    }
#elif defined(CORE_LIB_OS_LINUX)
    library = ::dlopen(cstr(_file_name), RTLD_LAZY);
    if (!IsLoaded()) {
      LOG_LIBRARY(WARNING, "Error while loading: %s!\n", dlerror());
      return false;
    }
#endif
    return true;
  }

  void Unload() {
    if (IsLoaded()) {
#if defined(CORE_LIB_OS_WINDOWS)
      if (!::FreeLibrary(library)) {
        LOG_LIBRARY(WARNING, "Error while unloading: %ld!\n", GetLastError());
      }
#elif defined(CORE_LIB_OS_LINUX)
      if (::dlclose(library)) {
        LOG_LIBRARY(WARNING, "Error while unloading: %s!\n", dlerror());
      }
#endif
      library.Reset();
    }
  }

  LibraryHandle NativeHandle() const { return library; }

private:
  LibraryHandle library;
};

/*******************************************************************************
** DynamicFunction
*******************************************************************************/

template<class F, typename R, typename... Args>
class DynamicFunction<F, R, mpl::TypePack<Args...>> {
public:
  CORE_LIB_STATIC_ASSERT(mpl::is_same<R, mpl::result_of_t<F>>::value);
  CORE_LIB_STATIC_ASSERT(
      mpl::is_same<mpl::TypePack<Args...>, mpl::parameters_of_t<F>>::value);
  using FunctionPointer = F;

  DynamicFunction() = default;
  DynamicFunction(DynamicFunction &&) = default;
  DynamicFunction(DynamicFunction const &) = delete;
  DynamicFunction & operator=(DynamicFunction &&) = default;
  DynamicFunction & operator=(DynamicFunction const &) = delete;
  ~DynamicFunction() = default;

  DynamicFunction & operator=(FunctionPointer _fptr) {
    fptr = _fptr;
    return *this;
  }

  bool Load(Library const & _library, const_string const & _symbol) {
#if defined(CORE_LIB_OS_WINDOWS)
    fptr = reinterpret_cast<FunctionPointer>(
        ::GetProcAddress(_library.library, cstr(_symbol)));
    if (fptr == nullptr) {
      LOG_LIBRARY(WARNING, "Error while loading symbol '%s': %ld!\n",
                  cstr(_symbol), GetLastError());
      return false;
    }
#elif defined(CORE_LIB_OS_LINUX)
    ::dlerror();
    fptr = reinterpret_cast<FunctionPointer>(
        ::dlsym(_library.library, cstr(_symbol)));
    if (char * error = ::dlerror()) {
      LOG_LIBRARY(WARNING, "Error while loading symbol %s: %s\n", cstr(_symbol),
                  error);
      return false;
    }
#endif
    return true;
  }

  void Unload() { fptr = nullptr; }

  bool IsLoaded() const { return fptr != nullptr; }
  explicit operator bool() const { return IsLoaded(); }

  FunctionPointer Get() const { return fptr; }

  R operator()(Args... _args) {
    CORE_LIB_ASSERT(fptr);
    return (*fptr)(_args...);
  }

private:
  FunctionPointer fptr = nullptr;
};

template<typename F>
using LibraryFunction =
    DynamicFunction<F, mpl::result_of_t<F>, mpl::parameters_of_t<F>>;

}

#endif
