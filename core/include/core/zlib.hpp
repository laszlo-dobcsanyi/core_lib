#ifndef _CORE_LIB_ZLIB_HPP_
#define _CORE_LIB_ZLIB_HPP_

#include "core/base.h"
#include "core/ranges/span.hpp"
#include "core/memory/unique_array.hpp"

#include "zlib.h"

#define LOG_ZLIB_DECODER(__severity__, ...)                                    \
  CORE_LIB_LOG(__severity__, __VA_ARGS__)

////////////////////////////////////////////////////////////////////////////////
// zlib detail
////////////////////////////////////////////////////////////////////////////////

namespace zlib { namespace detail {

/*******************************************************************************
** Decoder
*******************************************************************************/

template<size_t BufferSize>
class Decoder final {
public:
  Decoder() = default;
  Decoder(Decoder &&) = default;
  Decoder(Decoder const &) = default;
  Decoder & operator=(Decoder &&) = default;
  Decoder & operator=(Decoder const &) = default;
  ~Decoder() = default;

  byte * Get() { return data.Get(); }
  byte const * Get() const { return data.Get(); }

  size_t Size() const { return data_size; }

  bool Decompress(core::span<byte const> _buffer);

  core::UniqueArray<byte> const & Data() const;
  core::UniqueArray<byte> Acquire();

private:
  byte buffer[BufferSize];
  size_t data_size = 0u;
  core::UniqueArray<byte> data;
};

/*******************************************************************************
** Decoder
*******************************************************************************/

template<size_t BufferSize>
inline bool Decoder<BufferSize>::Decompress(core::span<byte const> _buffer) {
  CORE_LIB_ASSERT(_buffer.size() != 0u);
  int result;
  size_t decompressed;
  z_stream stream;
  stream.zalloc = Z_NULL;
  stream.zfree = Z_NULL;
  stream.opaque = Z_NULL;
  stream.avail_in = 0;
  stream.next_in = Z_NULL;
  result = inflateInit(&stream);
  if (result != Z_OK) {
    LOG_ZLIB_DECODER(ERROR, "Failed to initialize inflate state: %d!\n",
                     result);
    return false;
  }
  auto memory = reinterpret_cast<uintptr_t>(_buffer.data());
  size_t memory_size = _buffer.size();
  do {
    stream.avail_in = static_cast<uInt>(core::min(memory_size, BufferSize));
    memory_size -= stream.avail_in;
    stream.next_in = reinterpret_cast<Bytef *>(memory);
    memory += stream.avail_in;
    do {
      stream.avail_out = BufferSize;
      stream.next_out = reinterpret_cast<Bytef *>(buffer);
      result = inflate(&stream, Z_NO_FLUSH);
      CORE_LIB_ASSERT(result != Z_STREAM_ERROR);
      if (result == Z_NEED_DICT || result == Z_DATA_ERROR ||
          result == Z_MEM_ERROR) {
        inflateEnd(&stream);
        LOG_ZLIB_DECODER(ERROR, "Failed to inflate data: %d!\n", result);
        return false;
      }
      decompressed = BufferSize - stream.avail_out;
      auto const new_size = data_size + decompressed;
      if (data.Size() < new_size) {
        auto new_data = core::make_unique_array<byte>(new_size);
        CORE_LIB_ASSERT(new_data);
        auto new_memory = reinterpret_cast<void *>(new_data.Get());
        auto const old_memory = reinterpret_cast<void const *>(data.Get());
        ::memcpy(new_memory, old_memory, data_size);
        data = move(new_data);
      }
      auto data_memory = reinterpret_cast<uintptr_t>(data.Get());
      auto data_end = reinterpret_cast<void *>(data_memory + data_size);
      ::memcpy(data_end, buffer, decompressed);
      data_size = new_size;
    } while (stream.avail_out == 0);
  } while (result != Z_STREAM_END);
  inflateEnd(&stream);
  return true;
}

template<size_t BufferSize>
inline core::UniqueArray<byte> const & Decoder<BufferSize>::Data() const {
  return data;
}
template<size_t BufferSize>
inline core::UniqueArray<byte> Decoder<BufferSize>::Acquire() {
  return core::UniqueArray<byte>(move(data));
}

}}

////////////////////////////////////////////////////////////////////////////////
// zlib
////////////////////////////////////////////////////////////////////////////////

namespace zlib {

using Decoder = detail::Decoder<1024u>;

}

#endif
