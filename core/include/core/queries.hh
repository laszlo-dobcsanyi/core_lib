#ifndef _CORE_LIB_QUERIES_HH_
#define _CORE_LIB_QUERIES_HH_

#include "core/base.h"

namespace query {

template<typename ContainerType>
class Query;
template<typename ContainerType>
class ConstQuery;

// Lists
template<typename T, template<typename> class TAllocator>
class SingleList_Query;
template<typename T, template<typename> class TAllocator>
class DoubleList_Query;
// Chain & Link
template<class DerivedType, template<class> class ChainElementType>
class Chain_Query;
// Stacks
// Queues

}

#endif
