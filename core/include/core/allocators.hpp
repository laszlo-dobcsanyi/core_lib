#ifndef _CORE_LIB_ALLOCATORS_HPP_
#define _CORE_LIB_ALLOCATORS_HPP_

#include "core/base.h"
#include "core/platform.hpp"

#include "core/allocators.hh"

#define CORE_LIB_ALLOCATORS
#include "core/allocators/allocation.hpp"
#include "core/allocators/policies.hpp"
#include "core/allocators/memory_allocators.hpp"
#include "core/allocators/type_allocator.hpp"
#undef CORE_LIB_ALLOCATORS

#endif
