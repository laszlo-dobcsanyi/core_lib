#ifndef _CORE_LIB_BYTE_STREAM_HH_
#define _CORE_LIB_BYTE_STREAM_HH_

#include "core/memory/base.hpp" // memory::transform

namespace core { namespace detail {

template<typename Transform>
class ByteStreamReader;
template<typename Transform>
class ByteStreamWriter;

}}

namespace core {

using ByteStreamReader = detail::ByteStreamReader<memory::transform::NoOp>;
using ByteStreamWriter = detail::ByteStreamWriter<memory::transform::NoOp>;

using BigEndianByteStreamReader =
    detail::ByteStreamReader<memory::transform::ToBigEndian>;
using BigEndianByteStreamWriter =
    detail::ByteStreamWriter<memory::transform::ToBigEndian>;

using LittleEndianByteStreamReader =
    detail::ByteStreamReader<memory::transform::ToLittleEndian>;
using LittleEndianByteStreamWriter =
    detail::ByteStreamWriter<memory::transform::ToLittleEndian>;

}

#endif